<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/defaultKeycode/Change.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Change Default Key Code</TITLE>
			<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

}</script><%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
								<br>
								<hx:scriptCollector id="scriptCollector1" preRender="#{pc_Change.onPageLoadBegin}"><h:form styleClass="form" id="form1">
												<h:messages styleClass="messages" id="messages1" layout="table" style="font-size: 14pt"></h:messages>
												<br>
												<h:outputText styleClass="outputText"
													id="textPublicationLabel" value="Publication:"
													style="margin-top: 10px; font-size: 14pt"></h:outputText>
												<h:outputText styleClass="outputText" id="text13" value="#{defaultKeycodeChangeHandler.currentProduct.product.name}" style="margin-left: 10px; font-size: 14pt"></h:outputText>

												<h:panelGrid styleClass="panelGrid" id="gridKeycode"
													columns="2" cellpadding="1" cellspacing="2"
													columnClasses="columnClass5"
													style="vertical-align: top; border-color: gray; border-style: dotted; border-width: thin">


													<h:panelGrid styleClass="panelGrid" id="gridInside11"
														columns="2">
														<h:outputText styleClass="outputText"
															id="textCurrentDefaultForPubLabel"
															value="Current Default Keycode:"
															style="font-weight: bold"></h:outputText>
														<h:inputText styleClass="inputText"
															id="textCurrentDefaultKeycode" readonly="true" size="10"
															value="#{defaultKeycodeChangeHandler.currentOffer.keyCode}" style="background-color: white" tabindex="90"></h:inputText>
													</h:panelGrid>

													<h:panelGrid styleClass="panelGrid" id="gridInside1111"
														columns="2">
														<h:outputText styleClass="outputText"
															id="textCurrentExpiredDefaultForPubLabel"
															value="Current Expired Offer Keycode:"
															style="font-weight: bold"></h:outputText>
														<h:inputText styleClass="inputText"
															id="textCurrentDefaultExpiredKeycode" readonly="true"
															size="10"
															value="#{defaultKeycodeChangeHandler.currentExpiredOffer.keyCode}" style="background-color: white"></h:inputText>

													</h:panelGrid>
													<hx:panelSection styleClass="panelSection"
														id="sectionCurrentTermsDetail" initClosed="false">

														<f:facet name="closed">
															<hx:jspPanel id="jspPanel2">
																<hx:graphicImageEx id="imageEx2"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																<h:outputText id="text2" styleClass="outputText"
																	value="Current Offer Terms"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<f:facet name="opened">
															<hx:jspPanel id="jspPanel1">
																<hx:graphicImageEx id="imageEx1"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																<h:outputText id="text1" styleClass="outputText"
																	value="Current Offer Terms"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
															columnClasses="columnClass1" headerClass="headerClass"
															footerClass="footerClass" rowClasses="rowClass1"
															styleClass="dataTableEx" id="tableExCurrentOfferTerms"
															value="#{defaultKeycodeChangeHandler.currentOfferTerms}"
															var="varCurrentOfferTerms" style="margin-top: 8px">
															<hx:columnEx id="columnEx1" style="" align="center">
																<f:facet name="header">
																	<h:outputText id="text3" styleClass="outputText"
																		value="Length"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="text7"
																	value="#{varCurrentOfferTerms.durationInWeeks}">
																</h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnEx2" align="center">
																<f:facet name="header">
																	<h:outputText value="Rate Code" styleClass="outputText"
																		id="text4"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="text8"
																	value="#{varCurrentOfferTerms.rateCode}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnEx3" nowrap="true">
																<f:facet name="header">
																	<h:outputText value="Display  String"
																		styleClass="outputText" id="text5"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="text9"
																	value="#{varCurrentOfferTerms.description}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnEx4" align="center">
																<f:facet name="header">
																	<h:outputText value="Requires EZ-Pay"
																		styleClass="outputText" id="text6"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="text10"
																	value="#{varCurrentOfferTerms.requiresEzpay}"></h:outputText>
															</hx:columnEx>
														</hx:dataTableEx>
													</hx:panelSection>

													<hx:panelSection styleClass="panelSection"
														id="sectionCurrentExpiredTermsDetail" initClosed="false">

														<f:facet name="closed">
															<hx:jspPanel id="jspPanelExpired2">
																<hx:graphicImageEx id="imageExExpired2"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																<h:outputText id="textExpired2" styleClass="outputText"
																	value="Current Expired Offer Terms"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<f:facet name="opened">
															<hx:jspPanel id="jspPanelExpired1">
																<hx:graphicImageEx id="imageExExpired1"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																<h:outputText id="textExpired1" styleClass="outputText"
																	value="Current Expired Offer Terms"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
															columnClasses="columnClass1" headerClass="headerClass"
															footerClass="footerClass" rowClasses="rowClass1"
															styleClass="dataTableEx"
															id="tableExCurrentExpiredOfferTerms"
															value="#{defaultKeycodeChangeHandler.currentExpiredOfferTerms}"
															var="varCurrentExpiredOfferTerms" style="margin-top: 8px">
															<hx:columnEx id="columnExExpired1" style=""
																align="center">
																<f:facet name="header">
																	<h:outputText id="textExpired3" styleClass="outputText"
																		value="Length"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textExpired7"
																	value="#{varCurrentExpiredOfferTerms.durationInWeeks}">
																</h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExExpired2" align="center">
																<f:facet name="header">
																	<h:outputText value="Rate Code" styleClass="outputText"
																		id="textExpired4"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textExpired8"
																	value="#{varCurrentExpiredOfferTerms.rateCode}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExExpired3" nowrap="true">
																<f:facet name="header">
																	<h:outputText value="Display  String"
																		styleClass="outputText" id="textExpired5"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textExpired9"
																	value="#{varCurrentExpiredOfferTerms.description}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExExpired4" align="center">
																<f:facet name="header">
																	<h:outputText value="Requires EZ-Pay"
																		styleClass="outputText" id="textExpired6"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textExpired10"
																	value="#{varCurrentExpiredOfferTerms.requiresEzpay}"></h:outputText>
															</hx:columnEx>
														</hx:dataTableEx>
													</hx:panelSection>


												</h:panelGrid>


												<br>
												<hx:panelFormBox helpPosition="under" labelPosition="left"
													styleClass="panelFormBox" id="formBoxNewKeycode"
													label="Enter New Keycodes" style="border-color: white"
													widthLabel="200">
													<hx:formItem styleClass="formItem" id="formItemNewKeycode1"
														label="New Offer Key Code"
														infoText="Leave blank to keep current key code">
														<h:inputText styleClass="inputText" id="text12NewKeycode"
															value="#{defaultKeycodeChangeHandler.newKeycode}"
															maxlength="5" tabindex="1"
															validatorMessage="Invalid Key Code Entered, must be 5 chars or blank.">
															<hx:behavior event="onchange" id="behavior2"
																behaviorAction="get" targetAction="gridNewOfferGrid"></hx:behavior>
															<f:validateLength minimum="5" maximum="5"></f:validateLength>
															<hx:validateConstraint regex="^[A-Za-z0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist2" />
														</h:inputText>
														
													</hx:formItem>


													
													<hx:formItem styleClass="formItem" id="formItemNewKeycode2"
														label="New Expired Offer Key Code"
														infoText="Leave blank to keep current key code">
														<h:inputText styleClass="inputText" id="text12"
															value="#{defaultKeycodeChangeHandler.newExpiredKeycode}"
															maxlength="5" tabindex="2"
															validatorMessage="Invalid Key Code Entered. Must be 5 chars or blank.">
															<hx:behavior event="onchange" id="behavior1"
																behaviorAction="get" targetAction="gridNewOfferGrid"></hx:behavior>
															<f:validateLength minimum="5" maximum="5"></f:validateLength>
															<hx:validateConstraint regex="^[A-Za-z0-9]+$" />
															<hx:inputHelperAssist errorClass="inputText_Error"
																autoTab="true" id="assist1" />
														</h:inputText>
														
													</hx:formItem>
													<f:facet name="right">
														<hx:commandExButton type="submit" value="Review Changes"
															styleClass="commandExButton" id="buttonSaveChange"
															style="margin-left: 15px"
															action="#{pc_Change.doButtonSaveChangeAction}"
															tabindex="3">
														</hx:commandExButton>
													</f:facet>
												</hx:panelFormBox>



												<h:panelGrid styleClass="panelGrid" id="gridNewOfferGrid"
													columns="2" cellpadding="2" cellspacing="2"
													columnClasses="columnClass5"
													style="border-color: gray; border-width: thin; border-style: dotted">


													<hx:panelSection styleClass="panelSection"
														id="sectionNewTermsDetail" initClosed="false">

														<f:facet name="closed">
															<hx:jspPanel id="jspPanelNew2">
																<hx:graphicImageEx id="imageExNew2"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																<h:outputText id="textNew2" styleClass="outputText"
																	value="New Offer Terms #{defaultKeycodeChangeHandler.newKeycode}"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<f:facet name="opened">
															<hx:jspPanel id="jspPanelNew1">
																<hx:graphicImageEx id="imageExNew1"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																<h:outputText id="text11" styleClass="outputText"
																	value="New Offer Terms #{defaultKeycodeChangeHandler.newKeycode}"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
															columnClasses="columnClass1" headerClass="headerClass"
															footerClass="footerClass" rowClasses="rowClass1"
															styleClass="dataTableEx" id="tableExNewOfferTerms"
															value="#{defaultKeycodeChangeHandler.newOfferTerms}"
															var="varNewOfferTerms" style="margin-top: 8px">
															<f:facet name="header">
																<hx:panelBox styleClass="panelBox" id="box1">
																	<h:message styleClass="message" id="message1" for="text12NewKeycode"></h:message>
																</hx:panelBox>
															</f:facet>
															<hx:columnEx id="columnEx5">
																<f:facet name="header">
																	<h:outputText value="Key Code" styleClass="outputText"
																		id="text15"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="text16" value="#{varNewOfferTerms.parentOfferKeycode}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExNew1" style="" align="center">
																<f:facet name="header">
																	<h:outputText id="textNew3" styleClass="outputText"
																		value="Length"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textNew7"
																	value="#{varNewOfferTerms.durationInWeeks}">
																</h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExNew2" align="center">
																<f:facet name="header">
																	<h:outputText value="Rate Code" styleClass="outputText"
																		id="textNew4"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textNew8"
																	value="#{varNewOfferTerms.rateCode}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExNew3" nowrap="true">
																<f:facet name="header">
																	<h:outputText value="Display  String"
																		styleClass="outputText" id="textNew5"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textNew9"
																	value="#{varNewOfferTerms.description}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExNew4" align="center">
																<f:facet name="header">
																	<h:outputText value="Requires EZ-Pay"
																		styleClass="outputText" id="textNew6"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="textNew10"
																	value="#{varNewOfferTerms.requiresEzpay}"></h:outputText>
															</hx:columnEx>
														</hx:dataTableEx>
													</hx:panelSection>

													<hx:panelSection styleClass="panelSection"
														id="sectionNewExpiredTermsDetail" initClosed="false">

														<f:facet name="closed">
															<hx:jspPanel id="jspPanelExpiredNew2">
																<hx:graphicImageEx id="imageExExpiredNew2"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																<h:outputText id="textExpiredNew2"
																	styleClass="outputText"
																	value="New Expired Offer Terms #{defaultKeycodeChangeHandler.newExpiredKeycode}"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<f:facet name="opened">
															<hx:jspPanel id="jspPanelExpiredNew1">
																<hx:graphicImageEx id="imageExExpiredNew1"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																<h:outputText id="textExpiredNew1"
																	styleClass="outputText"
																	value="New Expired Offer Terms #{defaultKeycodeChangeHandler.newExpiredKeycode}"
																	style="font-weight: bold; font-size: 12pt"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
															columnClasses="columnClass1" headerClass="headerClass"
															footerClass="footerClass" rowClasses="rowClass1"
															styleClass="dataTableEx" id="tableExNewExpiredOfferTerms"
															value="#{defaultKeycodeChangeHandler.newExpiredOfferTerms}"
															var="varNewExpiredOfferTerms" style="margin-top: 8px">
															<f:facet name="header">
																<hx:panelBox styleClass="panelBox" id="box2">
																	<h:message styleClass="message" id="message2"
																		for="text12"></h:message>
																</hx:panelBox>
															</f:facet>
															<hx:columnEx id="columnEx6">
																<f:facet name="header">
																	<h:outputText value="Key Code" styleClass="outputText"
																		id="text17"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText" id="text18" value="#{varNewExpiredOfferTerms.parentOfferKeycode}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExExpiredNew1" style=""
																align="center">
																<f:facet name="header">
																	<h:outputText id="textExpiredNew3"
																		styleClass="outputText" value="Length"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText"
																	id="textExpiredNew7"
																	value="#{varNewExpiredOfferTerms.durationInWeeks}">
																</h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExExpiredNew2" align="center">
																<f:facet name="header">
																	<h:outputText value="Rate Code" styleClass="outputText"
																		id="textExpiredNew4"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText"
																	id="textExpiredNew8"
																	value="#{varNewExpiredOfferTerms.rateCode}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExExpiredNew3" nowrap="true">
																<f:facet name="header">
																	<h:outputText value="Display  String"
																		styleClass="outputText" id="textExpiredNew5"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText"
																	id="textExpiredNew9"
																	value="#{varNewExpiredOfferTerms.description}"></h:outputText>
															</hx:columnEx>
															<hx:columnEx id="columnExExpiredNew4" align="center">
																<f:facet name="header">
																	<h:outputText value="Requires EZ-Pay"
																		styleClass="outputText" id="textExpiredNew6"></h:outputText>
																</f:facet>
																<h:outputText styleClass="outputText"
																	id="textExpiredNew10"
																	value="#{varNewExpiredOfferTerms.requiresEzpay}"></h:outputText>
															</hx:columnEx>
														</hx:dataTableEx>
													</hx:panelSection>
												</h:panelGrid>
												<hx:ajaxRefreshSubmit target="gridNewOfferGrid"
													id="ajaxRefreshSubmit1"></hx:ajaxRefreshSubmit>
												<hx:outputSeparator styleClass="outputSeparator"
											id="separator1"></hx:outputSeparator>
												<hx:inputHelperSetFocus id="setFocus1" target="text12NewKeycode"></hx:inputHelperSetFocus>
											</h:form>
									<br><br>
											
											
											</hx:scriptCollector>
							<%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>