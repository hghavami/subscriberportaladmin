<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/Campaign_details.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%-- tpl:insert page="/theme/NoNav_EditCampaigns_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
				<TITLE>Campaign Details</TITLE>
					<link rel="stylesheet" type="text/css"
						href="${pageContext.request.contextPath}/theme/dropdown_.css">
						

					<link rel="stylesheet" type="text/css"
						href="${pageContext.request.contextPath}/theme/vertical-text.css">
				<%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<link rel="stylesheet" type="text/css" 	href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/basic.css"	type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/stylesheet.css"	type="text/css">

</head>
	<body style="background-image: url('${pageContext.request.contextPath}/theme/1x1.gif'); background-color: white;" "bgcolor="white">
	
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
		value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>
			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav2">
		<table class="hsep_table" border="0" cellspacing="0" cellpadding="0" width="800">
			<tbody class="hsep_table_body">
				<tr class="hsep_table_row">
					<td style="hsep_cell_normal" nowrap="nowrap"><a class="hsep_item_normal" style=""
						href="/portaladmin/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Review Campaigns</a> :</td>
					<td nowrap="nowrap"><siteedit:navbar group="group5"
						spec="/portaladmin/secured/campaigns/details/horizontal-sep.jsp" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		

		<!-- start main content area -->
		<div class="mainWideBox2"><a name="navskip"><img border="0"
			src="/portaladmin/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"> 
		 </a> <hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="500px" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="/portaladmin/images/arrow_expanded.gif" alt="show/hide"
						border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="baseline" height="16" width="16"
							src="/portaladmin/images/Warning.gif">Changes have been
						made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_NoNav_EditCampaigns_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="SAVE" style="font-weight: bold; font-size: 14pt"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel> <%-- tpl:put name="bodyarea" --%><hx:scriptCollector id="scriptCollectorCampaignDetailsCollector"><h:form styleClass="form" id="formCampaignDetails">
										<hx:outputLinkEx
											value="/portaladmin/secured/campaigns/advancedsearch.faces"
											styleClass="outputLinkEx" id="linkExBackToSearchPage">
											<h:outputText id="text9" styleClass="outputText"
												value="&lt;&lt;...Back to Campaign Search" escape="false"></h:outputText>
										</hx:outputLinkEx>
										<br>
					<h:messages styleClass="messages" id="messages1" layout="table"></h:messages>
					<br>

										<hx:panelSection styleClass="panelSection"
											id="sectionCampaignsSection" initClosed="false">
											<f:facet name="closed">
												<hx:jspPanel id="jspPanel2">
								<hx:graphicImageEx id="imageExClosed"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif" align="middle"></hx:graphicImageEx>
								<h:outputText id="textSelectedCampaignsLabel1"
														styleClass="outputText"
														value="Selected Campaigns For Edit"></h:outputText>
												</hx:jspPanel>
											</f:facet>
											<f:facet name="opened">
												<hx:jspPanel id="jspPanel1">
								<hx:graphicImageEx id="imageExOpened"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif" align="middle"></hx:graphicImageEx>
								<h:outputText id="textSelectedCampaignsLabel"
														styleClass="outputText"
														value="Selected Campaigns For Edit"></h:outputText>
												</hx:jspPanel>
											</f:facet>
						<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
							columnClasses="columnClass1" headerClass="headerClass"
							footerClass="footerClass" rowClasses="rowClass1, rowClass2"
							styleClass="dataTableEx" id="tableExCampaigns"
							value="#{edittingCampaignsHandler.campaigns}" var="varcampaigns"
							style="margin-top: 10px" width="100%">
							<f:facet name="header">
								<hx:panelBox styleClass="panelBox" id="box1">
									<h:panelGrid styleClass="panelGrid"
										id="gridEditCampaignDTHeaderGrid" columns="3" cellpadding="1"
										cellspacing="1">
										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExInfoImage1"
											value="/portaladmin/images/Information.gif"></hx:graphicImageEx>
										<h:outputText styleClass="outputText"
											id="textSelCampHeaderInfoText"
											value="Click on an individual row to edit the individual campaign details (keycode, vanity, landing page) or choose a page to configure:"></h:outputText>
										<hx:jspPanel id="jspPanelCampEditJumpPanel">
											<siteedit:navbar group="group5" spec="dropdown.jsp"
												target="self" />
										</hx:jspPanel>
									</h:panelGrid>
								</hx:panelBox>
							</f:facet>
							<hx:columnEx id="columnEx11">
								<hx:requestRowAction id="rowActionEditItemSpecific"
									action="#{pc_Campaign_details.doRowAction1Action}">
									<f:param name="CID" value="#{varcampaigns.campaignID}"
										id="param2"></f:param>
								</hx:requestRowAction>
								<f:facet name="header"></f:facet>
							</hx:columnEx>
							<hx:columnEx id="columnEx1">
								<f:facet name="header">
									<h:outputText id="text3" styleClass="outputText"
										value="Campaign"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text6"
									value="#{varcampaigns.campaignName}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx2" align="center">
								<f:facet name="header">
									<h:outputText value="Pub" styleClass="outputText" id="text4"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text7"
									value="#{varcampaigns.pubcode}"></h:outputText>

							</hx:columnEx>
							<hx:columnEx id="columnEx3" align="center">
								<f:facet name="header">
									<h:outputText value="Keycode" styleClass="outputText"
										id="text5"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text8"
									value="#{varcampaigns.keycode}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx4">
								<f:facet name="header">
									<h:outputText value="Vanity" styleClass="outputText" id="text1"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text2"
									value="#{varcampaigns.vanityURL}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx5">
								<f:facet name="header">
									<h:outputText value="Type" styleClass="outputText" id="text10"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="textCampaignType"
									value="#{varcampaigns.campaignTypeAsString}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx6">
								<f:facet name="header">
									<h:outputText value="Landing Page" styleClass="outputText"
										id="text11"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText"
									id="textCampaignLandingPage"
									value="#{varcampaigns.campaignLandingPageDescription}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx12">
								<f:facet name="header">
									<h:outputText value="Publishing State" styleClass="outputText"
										id="text15"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText"
									id="textEditCampaignPubStatus"
									value="#{varcampaigns.campaignStatusString}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx9">
								<f:facet name="header">
									<h:outputText value="Created Time" styleClass="outputText"
										id="text14"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="textCreatedTime"
									value="#{edittingCampaignsHandler.campaigns[0].createdTime}">
									<hx:convertDateTime type="both" dateStyle="short" />
								</h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx7">
								<f:facet name="header">
									<h:outputText value="Created By" styleClass="outputText"
										id="text12"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="textCampaignCreator"
									value="#{varcampaigns.createdBy}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx8">
								<f:facet name="header">
									<h:outputText value="Updated By" styleClass="outputText"
										id="text13"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText"
									id="textCampaignLastUpdater" value="#{varcampaigns.updatedBy}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx10">
								<f:facet name="header">
									<h:outputText value="Update Time" styleClass="outputText"
										id="text20"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="textUpdateTime"
									value="#{edittingCampaignsHandler.campaigns[0].updatedTime}">
									<hx:convertDateTime type="both" dateStyle="short" />
								</h:outputText>
							</hx:columnEx>
						</hx:dataTableEx>
					</hx:panelSection>
										<br>
										<hx:commandExButton type="submit"
											value="Publish These Campaigns" styleClass="commandExButton"
											id="buttonPublishChanges" action="#{pc_Campaign_details.doButtonPublishChangesAction}"></hx:commandExButton>
					<hx:commandExButton type="submit" value="Clear All Unsaved Changes"
						styleClass="commandExButton" id="buttonClearAllChanges" action="#{pc_Campaign_details.doButtonClearAllChangesAction}" confirm="All unsaved changes will be cleared. Are you sure you want to continue?"></hx:commandExButton>
					<br>
										<br>
										<br>										
									</h:form></hx:scriptCollector><%-- /tpl:put --%> 
</div>
	<!-- end main content area -->
</body>
</html>
</f:view>
<%-- /tpl:insert --%>