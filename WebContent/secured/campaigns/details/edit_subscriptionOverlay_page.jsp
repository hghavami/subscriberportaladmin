<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/Edit_subscriptionOverlay_page.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%-- tpl:insert page="/theme/NoNav_EditCampaigns_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
<link rel="stylesheet" href="/portaladmin/theme/tabpanel.css"
				type="text/css">
<title>New Order Exit Pop Up</title>

			<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader(thisObj.value);

}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelNavigation(thisObj.value);

}

</script>

<script type="text/javascript">	
//	
function updateDisplayedInnerTabPanelHeader(radioBtnValue) {
 
 var selectedValue = radioBtnValue;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerTabHeaderImage');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelHeaderImageDefault');
	panelCtrl.restoreUIState(panel);	
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelHeaderImageNoImage');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelHeaderImageCustom');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelHeaderImageDefault');
	panelCtrl.restoreUIState(panel);
 }
}

function updateDisplayedInnerTabPanelNavigation(radioBtnValue) {
 
 var selectedValue = radioBtnValue;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerTabNavigationImage');

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelNavigationImageDefault');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNavigationImageNoImage');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelNavigationImageCustom');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelNavigationImageDefault');
	panelCtrl.restoreUIState(panel);
 }
}
</script>
<%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<link rel="stylesheet" type="text/css" 	href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/basic.css"	type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/stylesheet.css"	type="text/css">

</head>
	<body style="background-image: url('${pageContext.request.contextPath}/theme/1x1.gif'); background-color: white;" "bgcolor="white">
	
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
		value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>
			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav2">
		<table class="hsep_table" border="0" cellspacing="0" cellpadding="0" width="800">
			<tbody class="hsep_table_body">
				<tr class="hsep_table_row">
					<td style="hsep_cell_normal" nowrap="nowrap"><a class="hsep_item_normal" style=""
						href="/portaladmin/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Review Campaigns</a> :</td>
					<td nowrap="nowrap"><siteedit:navbar group="group5"
						spec="/portaladmin/secured/campaigns/details/horizontal-sep.jsp" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		

		<!-- start main content area -->
		<div class="mainWideBox2"><a name="navskip"><img border="0"
			src="/portaladmin/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"> 
		 </a> <hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="500px" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="/portaladmin/images/arrow_expanded.gif" alt="show/hide"
						border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="baseline" height="16" width="16"
							src="/portaladmin/images/Warning.gif">Changes have been
						made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_NoNav_EditCampaigns_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="SAVE" style="font-weight: bold; font-size: 14pt"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel> <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollectorCampaignDetails">
			<h:messages styleClass="messages" id="messages1"></h:messages>
			<h:form styleClass="form" id="formCampaignDetails">


					<h:panelGrid styleClass="panelGrid" id="gridMainGrid"
						cellpadding="2" cellspacing="2" columns="2"
						style="text-align: left">
						<hx:panelBox styleClass="panelBox" id="boxLeftBox" height="100%"
							valign="top">

							<odc:tabbedPanel slantActiveRight="4" showBackNextButton="false"
								slantInactiveRight="4" styleClass="tabbedPanel" width="630"
								showTabs="true" variableTabLength="false"
								id="tabbedPanelHeaderNavTabs" height="350">
								<odc:bfPanel id="bfpanelHeaderImage"
									name="Overlay Image Configuration"
									showFinishCancelButton="false">
									<hx:panelLayout styleClass="panelLayout"
										id="layoutHeaderImageLayout">
										<f:facet name="body">
											<odc:tabbedPanel slantActiveRight="4"
												showBackNextButton="false" slantInactiveRight="4"
												styleClass="tabbedPanel_2" width="575" showTabs="false"
												variableTabLength="true" id="tabbedPanelInnerTabHeaderImage">
												<odc:bfPanel id="bfpanelHeaderImageDefault" name=""
													showFinishCancelButton="false">
													<h:panelGrid styleClass="panelGrid"
														id="gridHeaderImageDefaultImage" cellpadding="2"
														cellspacing="2">
														<h:outputText styleClass="outputText_Med"
															id="textHeaderImageDefaultLabel"
															value="Below is the Default Overlay Image:"></h:outputText>
														<hx:outputSeparator styleClass="outputSeparator"
															id="separator1" width="100%"></hx:outputSeparator>

														<hx:graphicImageEx styleClass="graphicImageEx"
															id="imageExHeaderImageDefault" align="middle"
															hspace="10" vspace="10" border="0" value="#{defaultCampaignReadOnly.sourcePromosHandler.newOrderPathPopOverlayPath}"></hx:graphicImageEx>

													</h:panelGrid>
												</odc:bfPanel>
												<odc:bfPanel id="bfpanelHeaderImageNoImage" name=""
													showFinishCancelButton="false">
													<hx:jspPanel id="jspPanelInnerHeaderNoImage">
														<br>
														<br>
														<h:outputText styleClass="outputText_Med"
															id="textInnerHeaderNoImageLabel"
															value="No Pop Up Overlay will be displayed for this offer."
															style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
														<br>
														<br>
														<br>
													</hx:jspPanel>
												</odc:bfPanel>
												<odc:bfPanel id="bfpanelHeaderImageCustom" name=""
													showFinishCancelButton="false">
													<hx:panelLayout styleClass="panelLayout"
														id="layoutInnerHeaderImageCustomImageLayout" width="100%">
														<f:facet name="body">
															<hx:panelFormBox helpPosition="over" labelPosition="left"
																styleClass="panelFormBox" id="formBoxHeaderCustomImage"
																widthLabel="125">
																<hx:formItem styleClass="formItem"
																	id="formItemCustomImageFormItem" label="Upload Image:"
																	infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																	<hx:fileupload styleClass="fileupload"
																		id="fileuploadPromoImageSubsribePopOverlay" size="40"
																		accept="image/*" title="Custom Image Upload" value="#{campaignDetailEditsHandler.newOrderPopUpOverlayCustomImage.imageContents}">
																		<hx:fileProp name="fileName" value="#{campaignDetailEditsHandler.newOrderPopUpOverlayCustomImage.imageFileName}"/>
																		<hx:fileProp name="contentType" value="#{campaignDetailEditsHandler.newOrderPopUpOverlayCustomImage.imageFileType}"/>
																	</hx:fileupload>
																	<h:message for="fileuploadPromoImageSubsribePopOverlay" id="pupUpFileUploadErrMsg"></h:message>
																</hx:formItem>
																<hx:formItem styleClass="formItem"
																	id="formItemCustomHeaderOnClickURL"
																	label="On Click URL:"
																	infoText="Specify the URL to go to when this image is clicked.">
																	<h:inputText styleClass="inputText"
																		id="textCustomHeaderOnClickURL" size="45" value="#{campaignDetailEditsHandler.newOrderPopUpOverlayCustomImage.linkURL}"></h:inputText>
																</hx:formItem>
																<hx:formItem styleClass="formItem"
																	id="formItemCustomHeaderImageAltText"
																	label="Alternate Text:"
																	infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																	<h:inputText styleClass="inputText"
																		id="textCustomHeaderImageAltText" size="45" value="#{campaignDetailEditsHandler.newOrderPopUpOverlayCustomImage.alternateText}"></h:inputText>
																</hx:formItem>

															</hx:panelFormBox>
														</f:facet>
														<f:facet name="left"></f:facet>
														<f:facet name="right"></f:facet>
														<f:facet name="bottom"></f:facet>
														<f:facet name="top"></f:facet>
													</hx:panelLayout>
												</odc:bfPanel>
												<f:facet name="back">
													<hx:commandExButton type="submit" value="&lt; Back"
														id="tabbedPanelInnerTabHeaderImage_back"
														style="display:none"></hx:commandExButton>
												</f:facet>
												<f:facet name="next">
													<hx:commandExButton type="submit" value="Next &gt;"
														id="tabbedPanelInnerTabHeaderImage_next"
														style="display:none"></hx:commandExButton>
												</f:facet>
												<f:facet name="finish">
													<hx:commandExButton type="submit" value="Finish"
														id="tabbedPanelInnerTabHeaderImage_finish"
														style="display:none"></hx:commandExButton>
												</f:facet>
												<f:facet name="cancel">
													<hx:commandExButton type="submit" value="Cancel"
														id="tabbedPanelInnerTabHeaderImage_cancel"
														style="display:none"></hx:commandExButton>
												</f:facet>
											</odc:tabbedPanel>
										</f:facet>
										<f:facet name="left"></f:facet>
										<f:facet name="right"></f:facet>
										<f:facet name="bottom"></f:facet>
										<f:facet name="top">
											<h:panelGrid styleClass="panelGrid"
												id="gridMainHeaderImageOptionGrid" cellpadding="2"
												columns="1">
												<f:facet name="header">
													<h:panelGroup styleClass="panelGroup" id="group1">
														<h:outputText styleClass="outputText_Med"
															id="textHeaderImageHeader" value="Overlay Image"
															style="font-weight: bold; font-size: 14pt"></h:outputText>
													</h:panelGroup>
												</f:facet>

												<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
													enabledClass="selectOneRadio_Enabled"
													styleClass="selectOneRadio" id="radioOverlayImage"
													style="font-weight: bold"
													onclick="return func_1(this, event);" value="#{campaignDetailEditsHandler.newOrderPopUpOverlayImageOption}">
													<f:selectItem itemLabel="Default PopUp Overlay Image"
														itemValue="default" />
													<f:selectItem itemLabel="No Pop Up (Hide)" itemValue="none" />
													<f:selectItem itemLabel="Custom PopUp Overlay Image"
														itemValue="custom" />
												</h:selectOneRadio>

											</h:panelGrid>
										</f:facet>
									</hx:panelLayout>
								</odc:bfPanel>
								<f:facet name="back">
									<hx:commandExButton type="submit" value="&lt; Back"
										id="tabbedPanelHeaderNavTabs_back" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="next">
									<hx:commandExButton type="submit"
										id="tabbedPanelHeaderNavTabs_next" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="finish">
									<hx:commandExButton type="submit" value="Finish"
										id="tabbedPanelHeaderNavTabs_finish" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="cancel">
									<hx:commandExButton type="submit" value="Cancel"
										id="tabbedPanelHeaderNavTabs_cancel" style="display:none"></hx:commandExButton>
								</f:facet>
								<odc:buttonPanel alignContent="right" id="bpCustomButtonPanel">
									<hx:commandExButton type="submit" value="Apply"
										styleClass="commandExButton" id="buttonApplyChanges"
										action="#{pc_Edit_subscriptionOverlay_page.doButtonApplyChangesAction}"></hx:commandExButton>
								</odc:buttonPanel>
							</odc:tabbedPanel>
						</hx:panelBox>


						<hx:panelBox styleClass="panelBox" id="boxRightBox" valign="top"
							height="100%" layout="pageDirection" align="left">
							<h:panelGrid styleClass="panelGrid" id="gridCurrentSettings"
								columns="1" cellspacing="2" cellpadding="2"
								style="vertical-align: top">
								<f:facet name="header">
									<h:panelGroup styleClass="panelGroup" id="group3">
										<h:outputText styleClass="outputText_Med"
											id="textCurrentSettingLabel" value="Current Settings"
											style="text-decoration: underline overline; font-weight: bold;"></h:outputText>
									</h:panelGroup>
								</f:facet>
								<h:outputText styleClass="outputText"
									id="textCurrentHeaderImageLabel" value="Current Overlay Image:"
									style="font-weight: bold"></h:outputText>
								<hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExHeaderInMemoryVersion"
									rendered="#{not campaignDetailEditsHandler.currentNewOrderPopUpOverlayImage.shimImage }" value="#{campaignDetailEditsHandler.currentNewOrderPopUpOverlayImage.imageContents}" mimeType="#{campaignDetailEditsHandler.currentNewOrderPopUpOverlayImage.imageFileType}"></hx:graphicImageEx>

							</h:panelGrid>
						</hx:panelBox>

						<f:facet name="header">
							<h:panelGroup styleClass="panelGroup" id="groupHeaderGroup"
								style="line-height: 37px;height: 37px; width: 100%; left: 0px; text-align: left">
								<h:outputText styleClass="outputText_Large"
									id="textPageHeaderText"
									value="New Subscription Exit PopUp Overlay"></h:outputText>
								<hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExCampaignListImage" hspace="15"
									value="/portaladmin/images/icon_manage_campaigns_34x35.gif"
									width="34" height="35"
									title="Click to Show Current Campaigns Being Editted"
									align="bottom" vspace="0">
								</hx:graphicImageEx>
							</h:panelGroup>
						</f:facet>
					</h:panelGrid></h:form>
				<br>  
				 
				<h:form styleClass="form" id="formForm2">				
				<br>
				<hx:panelDialog type="modeless" styleClass="panelDialog"
						id="dialogSelectedCampaignsForEdit" for="imageExCampaignListImage" title="Campaigns Currently Being Editted" relativeTo="imageExCampaignListImage">
							<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
								columnClasses="columnClass1, columnClass3"
								headerClass="headerClass" footerClass="footerClass"
								rowClasses="rowClass1, rowClass2" styleClass="dataTableEx"
								id="tableExSelectedCampaignsForEditV2"
								value="#{edittingCampaignsHandler.campaigns}" var="varcampaigns">
								<f:facet name="header">
									<hx:panelBox styleClass="panelBox" id="box1">
										<h:outputText styleClass="outputText"
											id="textEditCampsDTHeader" style="font-weight: bold"
											value="#{edittingCampaignsHandler.dataTableHeaderString}"></h:outputText>
									</hx:panelBox>
								</f:facet>
							<hx:columnEx id="columnEx6" rendered="false">
								<hx:inputRowSelect styleClass="inputRowSelect"
									id="rowSelectRemoveCampaignFromEdit"
									value="#{varcampaigns.campaignID}">
									<f:param name="campID" value="#{varcampaigns.campaignID}"
										id="param1"></f:param>
								</hx:inputRowSelect>
								<f:facet name="header"></f:facet>
							</hx:columnEx>
							<hx:columnEx id="columnEx3">
									<f:facet name="header">
										<h:outputText id="textEditPubHeader" styleClass="outputText"
											value="Pub"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text7"
										value="#{varcampaigns.pubcode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx4">
									<f:facet name="header">
										<h:outputText value="Keycode" styleClass="outputText"
											id="textEditKeycodeHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text8"
										value="#{varcampaigns.keycode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx5">
									<f:facet name="header">
										<h:outputText value="Name" styleClass="outputText"
											id="textEditNameHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text9"
										value="#{varcampaigns.campaignName}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx1">
									<f:facet name="header">
										<h:outputText value="Type" styleClass="outputText"
											id="textEditTypeColHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="textEditCampaignType"
										value="#{varcampaigns.targetAudience}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx2">
									<f:facet name="header">
										<h:outputText value="Landing Page" styleClass="outputText"
											id="textEditLandingPageHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText"
										id="textEditCampaignLandingPageDes"
										value="#{varcampaigns.campaignLandingPageDescription}"></h:outputText>
								</hx:columnEx>
							</hx:dataTableEx>
						<h:panelGroup id="groupEditCampaignsFooterGroup"
							styleClass="panelDialog_Footer">
							<hx:commandExButton type="submit"
								value="Remove Campaigns From Edit List"
								styleClass="commandExButton"
								id="buttonRemoveSelectedCampaignsFromEditCollection"
								action="#{pc_EdittingCampaignsCollection.doButtonRemoveSelectedCampaignsFromEditCollectionAction}"
								confirm="Removing the selected campaigns only prevents them from being editted. It does not remove them from the system."
								rendered="false">
							</hx:commandExButton>
						</h:panelGroup>

			</hx:panelDialog>			
			</h:form>
			</hx:scriptCollector>
		<%-- /tpl:put --%> 
</div>
	<!-- end main content area -->
</body>
</html>
</f:view>
<%-- /tpl:insert --%>