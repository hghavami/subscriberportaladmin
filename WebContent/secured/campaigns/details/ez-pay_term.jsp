<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/Ezpay_term.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/NoNav_EditCampaigns_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
<title>ez-pay_term</title>
<%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<link rel="stylesheet" type="text/css" 	href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/basic.css"	type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/stylesheet.css"	type="text/css">

</head>
	<body style="background-image: url('${pageContext.request.contextPath}/theme/1x1.gif'); background-color: white;" "bgcolor="white">
	
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
		value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>
			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav2">
		<table class="hsep_table" border="0" cellspacing="0" cellpadding="0" width="800">
			<tbody class="hsep_table_body">
				<tr class="hsep_table_row">
					<td style="hsep_cell_normal" nowrap="nowrap"><a class="hsep_item_normal" style=""
						href="/portaladmin/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Review Campaigns</a> :</td>
					<td nowrap="nowrap"><siteedit:navbar group="group5"
						spec="/portaladmin/secured/campaigns/details/horizontal-sep.jsp" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		

		<!-- start main content area -->
		<div class="mainWideBox2"><a name="navskip"><img border="0"
			src="/portaladmin/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"> 
		 </a> <hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="500px" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="/portaladmin/images/arrow_expanded.gif" alt="show/hide"
						border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="baseline" height="16" width="16"
							src="/portaladmin/images/Warning.gif">Changes have been
						made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_NoNav_EditCampaigns_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="SAVE" style="font-weight: bold; font-size: 14pt"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel> <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollector1"><h:form styleClass="form" id="form1">
					<h:messages styleClass="messages" id="messages1" globalOnly="true"></h:messages>
					<br>
					<h:panelGrid styleClass="panelGrid" id="gridMainGrid" columns="2" cellspacing="10">
						<h:panelGrid styleClass="panelGrid" id="gridLeftGrid" columns="1">
							<f:facet name="header">
								<h:panelGroup styleClass="panelGroup" id="group1">
									<h:outputText styleClass="outputText"
										id="textOptionSelectionText"
										value="Choose Force EZ-PAY Override Option"
										style="font-weight: bold; font-size: 14pt"></h:outputText>
																			<hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExCampaignListImage" hspace="15"
									value="/portaladmin/images/icon_manage_campaigns_34x35.gif"
									width="34" height="35"
									title="Click to Show Current Campaigns Being Editted"
									align="bottom" vspace="0">
								</hx:graphicImageEx>
										
									
								</h:panelGroup>
							</f:facet>
							<hx:outputLinkEx styleClass="outputLinkEx"
										id="linkExShowDefaults">
										<h:outputText id="text8" styleClass="outputText"
											value="View Default Force EZPAY Settings"></h:outputText>
							</hx:outputLinkEx>
							<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
								enabledClass="selectOneRadio_Enabled"
								styleClass="selectOneRadio" id="radioOverrideOptionsRadio"
								layout="pageDirection" value="#{campaignDetailEditsHandler.forceEZPAYOverrideRateOption}">
								<f:selectItem itemLabel="Use Default Force EZ-PAY Rate "
									itemValue="default" id="selectItem1" />
								<f:selectItem itemLabel="Use Custom Force EZ-PAY Rate Override (#{campaignDetailEditsHandler.forceEZPAYRateCodeToLookUp})"
									itemValue="OVRD" id="selectItem2" />
								<f:selectItem itemLabel="Hide Force EZ-PAY Rate"
									itemValue="HIDE" id="selectItem3" />
							</h:selectOneRadio>
							<hx:commandExButton type="submit" value="Apply"
								styleClass="commandExButton" id="buttonApplySetting" action="#{pc_Ezpay_term.doButtonApplySettingAction}"></hx:commandExButton>

						</h:panelGrid>
						
						<h:panelGrid styleClass="panelGrid" id="gridRightGrid" columns="1">
							<hx:panelFormBox labelPosition="left"
								styleClass="panelFormBox" id="formBox1" widthLabel="100"
								label="Check a Rate">
								<hx:formItem styleClass="formItem" id="formItemRateCodeCheck"
									label="Rate Code:" infoText="Enter a rate code to look up.">
									<h:inputText styleClass="inputText" id="textRateCodeToCheck"
										size="5"
										value="#{campaignDetailEditsHandler.forceEZPAYRateCodeToLookUp}"></h:inputText>
								</hx:formItem>
								<f:facet name="right">
									<h:panelGrid styleClass="panelGrid" id="gridRateCodeInfoGrid"
										columns="2"
										rendered="#{campaignDetailEditsHandler.showForceEZPAYRateCodeResults}"
										style="border-left-color: navy; border-left-width: thin; border-left-style: solid"
										cellpadding="1" cellspacing="1">
										<f:facet name="footer">
											<h:panelGroup styleClass="panelGroup" id="group2">
												<hx:commandExButton type="submit"
													styleClass="commandExButton" id="buttonUseRate"
													value="#{campaignDetailEditsHandler.forceEZPAYUseButtonLabel}"
													action="#{pc_Ezpay_term.doButtonUseRateAction}"></hx:commandExButton>
											</h:panelGroup>
										</f:facet>
										<h:outputText styleClass="outputText" id="text7"
											value="Rate Code:" style="font-weight: bold"></h:outputText>
										<h:outputText styleClass="outputText" id="text2"
											value="#{campaignDetailEditsHandler.lastRateLookedUp.piaRateCode}"></h:outputText>
										<h:outputText styleClass="outputText" id="text4"
											value="Description:" style="font-weight: bold"></h:outputText>
										<h:outputText styleClass="outputText" id="text5"
											value="#{campaignDetailEditsHandler.lastRateLookedUp.relOfferDescription}"></h:outputText>
										<h:outputText styleClass="outputText" id="text3"
											value="Length:" style="font-weight: bold"></h:outputText>
										<h:outputText styleClass="outputText" id="text6"
											value="#{campaignDetailEditsHandler.lastRateLookedUp.relPeriodLength}"></h:outputText>

									</h:panelGrid>
								</f:facet>
								<f:facet name="bottom">
									<h:panelGrid styleClass="panelGrid" id="gridBottomeGrid"
										width="100%" columns="1" style="text-align: left">

										<hx:commandExButton type="submit" value="Lookup"
											styleClass="commandExButton" id="buttonCheckRate"
											style="margin-left: 100px"
											action="#{pc_Ezpay_term.doButtonCheckRateAction}"></hx:commandExButton>

									</h:panelGrid>


								</f:facet>
								<f:facet name="top">
								</f:facet>
							</hx:panelFormBox>
						</h:panelGrid>
					</h:panelGrid>
					<br>
					<br>
					<br>
					<hx:panelDialog type="modeless" styleClass="panelDialog"
						id="dialogDefaultForceEZPayDialog"
						relativeTo="radioOverrideOptionsRadio" valign="relative" align="center" for="linkExShowDefaults" title="Default Force EZ-PAY Settings">
						<hx:panelLayout styleClass="panelLayout" id="layout1">
							<f:facet name="body">
								<hx:dataTableEx border="0" cellpadding="2" cellspacing="1"
									columnClasses="columnClass1" headerClass="headerClass"
									footerClass="footerClass" rowClasses="rowClass1, rowClass2"
									styleClass="dataTableEx"
									id="tableExDefaultForceEzpayOptionsTable"
									value="#{portalApplicationSettings.forceEZPAYApplicationSettings}"
									var="varforceEZPAYApplicationSettings">
									<hx:columnEx id="columnEx1">
										<f:facet name="header">
											<h:outputText id="text9" styleClass="outputText"
												value="Setting"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text12"
											value="#{varforceEZPAYApplicationSettings.label}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx2">
										<f:facet name="header">
											<h:outputText value="Value" styleClass="outputText"
												id="text10"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text13"
											value="#{varforceEZPAYApplicationSettings.value}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx3" nowrap="false">
										<f:facet name="header">
											<h:outputText value="Description" styleClass="outputText"
												id="text11"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text14"
											value="#{varforceEZPAYApplicationSettings.description}"></h:outputText>
									</hx:columnEx>
								</hx:dataTableEx>
							</f:facet>
							<f:facet name="left"></f:facet>
							<f:facet name="right"></f:facet>
							<f:facet name="bottom"></f:facet>
							<f:facet name="top">
								<h:outputText styleClass="outputText" id="text15"
									value="These are the default settings for all publications.<br>If you choose the default override, then the corresponding default rate will be used (or hidden if no value present or the program is not enabled)"
									escape="false"></h:outputText>
							</f:facet>
						</hx:panelLayout>
						<h:panelGroup id="groupDefaultForceEZPAYFooterGroup"
							styleClass="panelDialog_Footer">
							<hx:commandExButton id="button2" styleClass="commandExButton"
								type="submit" value="Close">
								<hx:behavior event="onclick" behaviorAction="hide"
									targetAction="dialogDefaultForceEZPayDialog" id="behavior2"></hx:behavior>
							</hx:commandExButton>
							<hx:commandExButton id="button1" styleClass="commandExButton"
								type="reset" value="Cancel" rendered="false">
								<hx:behavior event="onclick" behaviorAction="hide;stop"
									id="behavior1" targetAction="dialog1"></hx:behavior>
							</hx:commandExButton>
						</h:panelGroup>
					</hx:panelDialog>
					<br>
					<br>
				</h:form>
				<br>
				<br>
					<h:form styleClass="form" id="formForm2">
					
					<br>
					<hx:panelDialog type="modeless" styleClass="panelDialog"
						id="dialogSelectedCampaignsForEdit" for="imageExCampaignListImage" title="Campaigns Currently Being Editted" relativeTo="imageExCampaignListImage">
							<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
								columnClasses="columnClass1, columnClass3"
								headerClass="headerClass" footerClass="footerClass"
								rowClasses="rowClass1, rowClass2" styleClass="dataTableEx"
								id="tableExSelectedCampaignsForEditV2"
								value="#{edittingCampaignsHandler.campaigns}" var="varcampaigns">
								<f:facet name="header">
									<hx:panelBox styleClass="panelBox" id="box1">
										<h:outputText styleClass="outputText"
											id="textEditCampsDTHeader" style="font-weight: bold"
											value="#{edittingCampaignsHandler.dataTableHeaderString}"></h:outputText>
									</hx:panelBox>
								</f:facet>
							<hx:columnEx id="columnEx6" rendered="false">
								<hx:inputRowSelect styleClass="inputRowSelect"
									id="rowSelectRemoveCampaignFromEdit"
									value="#{varcampaigns.campaignID}">
									<f:param name="campID" value="#{varcampaigns.campaignID}"
										id="param1"></f:param>
								</hx:inputRowSelect>
								<f:facet name="header"></f:facet>
							</hx:columnEx>
							<hx:columnEx id="columnEx3">
									<f:facet name="header">
										<h:outputText id="textEditPubHeader" styleClass="outputText"
											value="Pub"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text7"
										value="#{varcampaigns.pubcode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx4">
									<f:facet name="header">
										<h:outputText value="Keycode" styleClass="outputText"
											id="textEditKeycodeHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text8"
										value="#{varcampaigns.keycode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx5">
									<f:facet name="header">
										<h:outputText value="Name" styleClass="outputText"
											id="textEditNameHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text9"
										value="#{varcampaigns.campaignName}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx2">
									<f:facet name="header">
										<h:outputText value="Landing Page" styleClass="outputText"
											id="textEditLandingPageHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText"
										id="textEditCampaignLandingPageDes"
										value="#{varcampaigns.campaignLandingPageDescription}"></h:outputText>
								</hx:columnEx>
							</hx:dataTableEx>
						<h:panelGroup id="groupEditCampaignsFooterGroup"
							styleClass="panelDialog_Footer">
							<hx:commandExButton type="submit"
								value="Remove Campaigns From Edit List"
								styleClass="commandExButton"
								id="buttonRemoveSelectedCampaignsFromEditCollection"
								action="#{pc_EdittingCampaignsCollection.doButtonRemoveSelectedCampaignsFromEditCollectionAction}"
								confirm="Removing the selected campaigns only prevents them from being editted. It does not remove them from the system."
								rendered="false">
							</hx:commandExButton>
						</h:panelGroup>

				</hx:panelDialog>
				
				</h:form>				
				</hx:scriptCollector>
		<%-- /tpl:put --%> 
</div>
	<!-- end main content area -->
</body>
</html>
</f:view>
<%-- /tpl:insert --%>