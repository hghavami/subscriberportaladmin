<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/Standard_vanity.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%-- tpl:insert page="/theme/NoNav_EditCampaigns_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
			<link rel="stylesheet" href="${pageContext.request.contextPath}/theme/tabpanel.css"
				type="text/css">
<title>Vanity URL Campaign Edits</title>
<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
// Type Java code that runs when the component is clicked
	   
}</script><%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<link rel="stylesheet" type="text/css" 	href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/basic.css"	type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/stylesheet.css"	type="text/css">

</head>
	<body style="background-image: url('${pageContext.request.contextPath}/theme/1x1.gif'); background-color: white;" "bgcolor="white">
	
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
		value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>
			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav2">
		<table class="hsep_table" border="0" cellspacing="0" cellpadding="0" width="800">
			<tbody class="hsep_table_body">
				<tr class="hsep_table_row">
					<td style="hsep_cell_normal" nowrap="nowrap"><a class="hsep_item_normal" style=""
						href="/portaladmin/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Review Campaigns</a> :</td>
					<td nowrap="nowrap"><siteedit:navbar group="group5"
						spec="/portaladmin/secured/campaigns/details/horizontal-sep.jsp" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		

		<!-- start main content area -->
		<div class="mainWideBox2"><a name="navskip"><img border="0"
			src="/portaladmin/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"> 
		 </a> <hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="500px" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="/portaladmin/images/arrow_expanded.gif" alt="show/hide"
						border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="baseline" height="16" width="16"
							src="/portaladmin/images/Warning.gif">Changes have been
						made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_NoNav_EditCampaigns_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="SAVE" style="font-weight: bold; font-size: 14pt"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel> <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollector1"><h:form styleClass="form" id="formCampaignDetails">
					
					<table border="0" cellpadding="0" cellspacing="1" width="650">
						<tbody>
							<tr>
								<td><h:outputText styleClass="outputText"
									id="textPageInformationText"
									value="You are editting the campaign specific settings for the chosen camapaign. Changes to these settings must be saved before leaving this page. NOTICE: If you change the vanity, the old vanity will be deleted immediately from all servers that it has been previously published to. Until you re-publish the campaign, it will not exist on these tes or production servers."></h:outputText></td>
							</tr>
							<tr>
								<td><h:messages styleClass="messages" id="messages1"></h:messages></td>
							</tr>
						</tbody>
					</table><odc:tabbedPanel slantActiveRight="4" showBackNextButton="false"
						slantInactiveRight="4" styleClass="tabbedPanel" width="600"
						showTabs="true" variableTabLength="false" height="350"
						id="tabbedPanelVanityTabPanel">
						<odc:bfPanel id="bfpanelSinglePageSettings"
							name="Vanity URL Customizations" showFinishCancelButton="false">
							<hx:panelLayout styleClass="panelLayout" id="layout1"
								width="100%">
								<f:facet name="body">
									<hx:panelFormBox helpPosition="over" labelPosition="left"
										styleClass="panelFormBox" id="formBoxCampaignDetails">
										<hx:formItem styleClass="formItem" id="formItemKeyCode"
											label="Keycode:"
											infoText="Change the keycode associated with this camapaign">
											<h:inputText styleClass="inputText" id="text3"
												value="#{edittingCampaignsVanityHandler.campaigns[0].keycode}"></h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem" id="formItem2"
											label="Vanity URL:"
											infoText="Specify the desired vanity. http://service.usatoday.com/[vanity]">
											<h:inputText styleClass="inputText" id="textCampaignVanity"
												size="40"
												value="#{edittingCampaignsVanityHandler.campaigns[0].vanityURL}"></h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem" id="formItemLandingPage"
											label="Landing Page:"
											infoText="Select the page the user will land on.">
											<h:selectOneMenu styleClass="selectOneMenu" id="menu1"
												value="#{edittingCampaignsVanityHandler.campaigns[0].redirectToPage}">
												<f:selectItem itemLabel="One Page Order Form"
													itemValue="TERMS" />
												<f:selectItem itemLabel="Subscribe Page (deprecated)"
													itemValue="WELCOME" id="selectItem1" />
											</h:selectOneMenu>
										</hx:formItem>
										<f:facet name="top">
											<hx:jspPanel id="jspPanelTopPanel">
												<table>
													<tr>
														<td><h:outputText styleClass="outputText"
															id="textHeaderCampPub" value="Publication:"
															style="color: black; font-weight: bold"></h:outputText></td>
														<td><h:inputText styleClass="inputText"
															id="textCampaignPub"
															value="#{edittingCampaignsVanityHandler.campaigns[0].pubcode}"
															size="4" readonly="true" disabled="true"></h:inputText></td>
														<td><h:outputText styleClass="outputText"
															id="textCampaignName" value="Campaign Name:"
															style="color: black; font-weight: bold"></h:outputText></td>
														<td><h:inputText styleClass="inputText" id="textCampName"
															size="40" disabled="true" readonly="true"
															value="#{edittingCampaignsVanityHandler.campaigns[0].campaign.campaignName}"></h:inputText></td>
													</tr>
												</table>
											</hx:jspPanel>
										</f:facet>
									</hx:panelFormBox>
								</f:facet>
								<f:facet name="left"></f:facet>
								<f:facet name="right"></f:facet>
								<f:facet name="bottom"></f:facet>
								<f:facet name="top"></f:facet>
							</hx:panelLayout>
						</odc:bfPanel>
						<f:facet name="back">
							<hx:commandExButton type="submit" value="&lt; Back"
								id="tabbedPanel1_back" style="display:none"></hx:commandExButton>
						</f:facet>
						<f:facet name="next">
							<hx:commandExButton type="submit" value="Next &gt;"
								id="tabbedPanel1_next" style="display:none"></hx:commandExButton>
						</f:facet>
						<f:facet name="finish">
							<hx:commandExButton type="submit" value="Finish"
								id="tabbedPanel1_finish" style="display:none"></hx:commandExButton>
						</f:facet>
						<f:facet name="cancel">
							<hx:commandExButton type="submit" value="Cancel"
								id="tabbedPanel1_cancel" style="display:none"></hx:commandExButton>
						</f:facet>
						<odc:buttonPanel alignContent="right" id="bpCustomButtons">
							<h:panelGrid styleClass="panelGrid" id="gridFooterGrid" columns="2">
								<hx:commandExButton type="submit" value="Save"
									styleClass="commandExButton" id="buttonApplyChanges"
									action="#{pc_Standard_vanity.doButtonApplyChangesAction}"
									onclick="return func_1(this, event);"></hx:commandExButton>
								<hx:commandExButton type="submit" value="Cancel"
									styleClass="commandExButton" id="buttonCancelChanges"
									action="#{pc_Standard_vanity.doButtonCancelChangesAction}"
									accesskey="B" immediate="true"></hx:commandExButton>

							</h:panelGrid>						
						</odc:buttonPanel>
					</odc:tabbedPanel>
					<br>
					
					
					<br></h:form></hx:scriptCollector>
		<%-- /tpl:put --%> 
</div>
	<!-- end main content area -->
</body>
</html>
</f:view>
<%-- /tpl:insert --%>