<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/Onepage_order.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
	<%@taglib uri="http://www.ibm.com/jsf/rte" prefix="r"%>
<%-- tpl:insert page="/theme/NoNav_EditCampaigns_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
<link rel="stylesheet" href="/portaladmin/theme/tabpanel.css" type="text/css">
<link rel="stylesheet" href="http://service.usatoday.com/theme/themeV3/css/usatodayAddOn.css" type="text/css">
<title>One Page Order Entry Edits</title>
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">	

<script language="JavaScript">
Event.observe(window, 'load', function () {
	try {
		var outterField = $('form1:checkbox1');
		var oField = $('form1:checkbox2');
		if (outterField.checked == false) {
			oField.checked = false;
			oField.disabled = true;
		}
	}
	catch (e) {
		// todo: handle exception
	}
});

function func_1(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDisplayedInnerTabPanelCustom(thisObj);

}			
			
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
var oField = $('form1:checkbox2');
if (thisObj.checked == true) {
	oField.disabled = false;
}
else {
	oField.checked = false;
	oField.disabled = true;
}
}</script>
			<script language="JavaScript">
function updateDisplayedInnerTabPanelCustom(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerTabCustomImage');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelCustomImageDefault');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelCustomImageNoImage');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomImageCustom');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelCustomImageDefault');
	panelCtrl.restoreUIState(panel);
 }
}
			
</script>
			


			<link rel="stylesheet" type="text/css"
				href="/portaladmin/theme/horizontal-sep.css">

			<link rel="stylesheet" type="text/css" title="Style"
				href="/theme/rte_style.css">
		<%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<link rel="stylesheet" type="text/css" 	href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/basic.css"	type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/stylesheet.css"	type="text/css">

</head>
	<body style="background-image: url('${pageContext.request.contextPath}/theme/1x1.gif'); background-color: white;" "bgcolor="white">
	
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
		value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>
			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav2">
		<table class="hsep_table" border="0" cellspacing="0" cellpadding="0" width="800">
			<tbody class="hsep_table_body">
				<tr class="hsep_table_row">
					<td style="hsep_cell_normal" nowrap="nowrap"><a class="hsep_item_normal" style=""
						href="/portaladmin/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Review Campaigns</a> :</td>
					<td nowrap="nowrap"><siteedit:navbar group="group5"
						spec="/portaladmin/secured/campaigns/details/horizontal-sep.jsp" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		

		<!-- start main content area -->
		<div class="mainWideBox2"><a name="navskip"><img border="0"
			src="/portaladmin/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"> 
		 </a> <hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="500px" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="/portaladmin/images/arrow_expanded.gif" alt="show/hide"
						border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="baseline" height="16" width="16"
							src="/portaladmin/images/Warning.gif">Changes have been
						made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_NoNav_EditCampaigns_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="SAVE" style="font-weight: bold; font-size: 14pt"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel> <%-- tpl:put name="bodyarea" --%><hx:scriptCollector id="scriptCollector1">
				<h:messages styleClass="messages" id="messages1"></h:messages>
				<h:form styleClass="form" id="form1">
				
					<table border="0" cellpadding="0" cellspacing="0">
						<tbody>
							<tr>
								<td colspan="2">
								<h:panelGroup styleClass="panelGroup" id="groupHeaderGroup"
								style="line-height: 37px;height: 37px; width: 100%; left: 0px; text-align: left">
									<h:outputText styleClass="outputText_Large"
										id="textPageHeaderText"
										value="One Page Order Entry Page Text and Images"></h:outputText>
									<hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExCampaignListImage" hspace="15"
									value="/portaladmin/images/icon_manage_campaigns_34x35.gif"
									width="34" height="35"
									title="Click to Show Current Campaigns Being Editted"
									align="bottom" vspace="0">
								</hx:graphicImageEx>
								</h:panelGroup>
								
								</td>
							</tr>						
							<tr>
								<td valign="top">
<odc:tabbedPanel slantActiveRight="4" showBackNextButton="false"
								slantInactiveRight="4" styleClass="tabbedPanel" width="630"
								showTabs="true" variableTabLength="false" height="350"
								id="tabbedPanelTermsTextAndImages">
									<odc:bfPanel id="bfpanelHeaderPromoText"
										name="Top Promotional Text" showFinishCancelButton="false">
										<hx:panelLayout styleClass="panelLayout"
											id="layoutInnerPromotionalText" width="100%" height="255">
											<f:facet name="body">

												<r:inputRichText width="100%" height="205"
													id="richTextEditor1"
													value="#{campaignDetailEditsHandler.customOnePagePromoText1}"></r:inputRichText>
											</f:facet>
											<f:facet name="left"></f:facet>
											<f:facet name="right"></f:facet>
											<f:facet name="bottom">
												<h:commandLink styleClass="commandLink"
													id="linkClearPromoText" action="#{pc_Onepage_order.doLinkClearPromoTextAction}">
													<h:outputText id="textClearPromoTextLabel"
														styleClass="outputText" value="Clear Top Promo Text"></h:outputText>
												</h:commandLink>
											</f:facet>
											<f:facet name="top">
												<h:outputText styleClass="outputText" id="textPromoTextMsg"
													value="This text will appear above the subscription terms. It is the header text"
													style="font-weight: bold"></h:outputText>
											</f:facet>
										</hx:panelLayout>
									</odc:bfPanel>
									<odc:bfPanel id="bfpanelTermsAndConditions"
										name="Disclaimer Text" showFinishCancelButton="false">
										<hx:panelLayout styleClass="panelLayout"
											id="layoutInnerTermsandConditionsLayout" width="100%" height="255">
											<f:facet name="body">
												<r:inputRichText width="100%" height="205"
													id="richTextEditorTermsAndConditionsText"
													value="#{campaignDetailEditsHandler.customOnePageDisclaimerText}"></r:inputRichText>
											</f:facet>
											<f:facet name="left"></f:facet>
											<f:facet name="right"></f:facet>
											<f:facet name="bottom">
												<h:commandLink styleClass="commandLink"
													id="linkClearDisclaimerText" action="#{pc_Onepage_order.doLinkClearDisclaimerTextAction}">
													<h:outputText id="textClearDisclaimerLabel"
														styleClass="outputText" value="Clear Disclaimer Text"></h:outputText>
												</h:commandLink>
											</f:facet>
											<f:facet name="top">
												<h:outputText styleClass="outputText"
													id="textTermsConditionsTextMsg"
													value='This text will appear above the "Place Order" button on the One Page Order page.'
													style="font-weight: bold"></h:outputText>
											</f:facet>
										</hx:panelLayout>
									</odc:bfPanel>
									<odc:bfPanel id="bfpanelForceEZPayTermsText"
										name="Force EZPay Terms Text" showFinishCancelButton="false">
											<hx:panelLayout styleClass="panelLayout"
											id="layoutInnerForceEZPayTermsLayout" width="100%" height="255">
											<f:facet name="body">

												<h:inputTextarea styleClass="inputTextarea"
													id="textareaForceEZPayTermsText" cols="100" rows="13"
													style="color: navy; font-family: Arial; font-weight: bold; font-size: 10px"
													value="#{campaignDetailEditsHandler.customOnePageForceEZPayTerms}"></h:inputTextarea>
											</f:facet>
											<f:facet name="left"></f:facet>
											<f:facet name="right">
												
											</f:facet>
											<f:facet name="bottom">
												<h:commandLink styleClass="commandLink"
													id="linkClearForceEZPayTermsText" action="#{pc_Onepage_order.doLinkClearForceEZPayTermsTextAction}">
													<h:outputText id="textClearForceEZPayTermsLabel"
														styleClass="outputText" value="Clear Force EZ-PAY Terms Text"></h:outputText>
												</h:commandLink>
											</f:facet>
											<f:facet name="top">

												<h:panelGrid styleClass="panelGrid"
													id="gridForceEZPayTermsTextHeaderGrid" columns="1" cellpadding="1" cellspacing="1">
													<h:outputText styleClass="outputText"
														id="textForceEZPayTermsTextMsg"
														value="This text will appear under the displayed terms and under the standard EZ-PAY text shown below."
														style="font-weight: bold"></h:outputText>
													<h:outputText styleClass="outputText" id="text2"
														value='* Selection of this term will require the EZ-PAY renewal plan.  Learn More...'
														style="color: navy; font-family: Arial; font-weight: bold; font-size: 10px"
														escape="false"></h:outputText>
												</h:panelGrid>
											</f:facet>
										</hx:panelLayout>	
									</odc:bfPanel>
										<odc:bfPanel id="bfpanelOfferOverridesPanel"
											name="Offer Code Overrides" showFinishCancelButton="false">
											<h:panelGrid styleClass="panelGrid"
												id="gridKeycodeOverrideGrid" columns="2">
												<f:facet name="header">
													<h:panelGroup styleClass="panelGroup" id="group2">
														<h:outputText styleClass="outputText"
															id="textKeycodeOverrideGridHeaderLabel"
															value="#{campaignDetailEditsHandler.defaultAllowOverrideOfferDisplayString}"
															style="font-weight: bold; font-size: 14pt"></h:outputText>
													</h:panelGroup>
												</f:facet>
												<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
													id="checkbox1"
													value="#{campaignDetailEditsHandler.overrideDefaultAllowOfferOverride}"
													onclick="return func_2(this, event);"></h:selectBooleanCheckbox>
												<h:outputText styleClass="outputText"
													id="textOverrideDefaultLabel"
													value="Check this box to override the default Pub Code setting above"></h:outputText>
												<h:outputText styleClass="outputText" id="text4"></h:outputText>
												<h:panelGrid styleClass="panelGrid"
													id="gridKeycodeOverrideSetting" columns="2">
													<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
														id="checkbox2"
														value="#{campaignDetailEditsHandler.customAllowOfferOverrideSetting}"></h:selectBooleanCheckbox>
													<h:outputText styleClass="outputText"
														id="textKeycodeOverrideLabel"
														value="Allow Overrides (checked box will show 'Enter Offer Code', unckecked will hide it"></h:outputText>
												</h:panelGrid>
											</h:panelGrid>
										</odc:bfPanel>
										<f:facet name="back">
									<hx:commandExButton type="submit" value="&lt; Back"
										id="tabbedPanelTermsTextAndImages_back" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="next">
									<hx:commandExButton type="submit" value="Next &gt;"
										id="tabbedPanelTermsTextAndImages_next" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="finish">
									<hx:commandExButton type="submit" value="Finish"
										id="tabbedPanelTermsTextAndImages_finish" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="cancel">
									<hx:commandExButton type="submit" value="Cancel"
										id="tabbedPanelTermsTextAndImages_cancel" style="display:none"></hx:commandExButton>
								</f:facet>
								<odc:buttonPanel alignContent="left" id="bpCustomButtonPanel">
									<hx:commandExButton type="submit" value="Apply"
										styleClass="commandExButton" id="buttonApplyChanges" action="#{pc_Onepage_order.doButtonApplyChangesAction}"></hx:commandExButton>
								</odc:buttonPanel>
							</odc:tabbedPanel>								
								</td>
								<td valign="top">
							<h:panelGrid styleClass="panelGrid" id="gridCurrentSettings"
								columns="1" cellpadding="2" cellspacing="2"
								style="text-align: left">
								<f:facet name="header">
									<h:panelGroup styleClass="panelGroup" id="group1"
										style="text-align: left">
										<h:outputText styleClass="outputText_Med"
											id="textCurrentSettingLabel" value="Current Settings"
											style="text-decoration: underline overline; font-weight: bold;"></h:outputText>
									</h:panelGroup>
								</f:facet>
									<h:outputText styleClass="outputText"
										id="textDefaultPromotTextLabel"
										value="Default Top Promo Text Value:"
										style="font-weight: bold"></h:outputText>

									<h:outputText id="textDefaultTopPromoText" escape="false"
										value="#{campaignDetailEditsHandler.defaultOnePagePromoText1}"></h:outputText>

									<h:panelGrid styleClass="panelGrid"
									id="gridCurrentPromoLabelGrid" columns="2">
										
										<h:outputText styleClass="outputText"
											id="textPreviewPromoText" value="Promotional Text Preview:"
											style="font-weight: bold"></h:outputText>
										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExCurrentPromoText1DetailsImage"
											value="../../../images/icon_detailsmag14x14.gif" width="14"
											height="14" title="Click to View Raw HTML"></hx:graphicImageEx>

									</h:panelGrid>

									<hx:jspPanel id="jspPanel22222">
											<div id="headerPromoTextDiv">
												<h:outputText id="textCurrentPromoTextValue" escape="false" value="#{campaignDetailEditsHandler.currentOnePagePromoText1}"></h:outputText>
											</div>
									</hx:jspPanel>
										
									
									<hx:outputSeparator styleClass="outputSeparator"
										id="separator1"></hx:outputSeparator>
									<h:outputText styleClass="outputText"
										id="textDefaultForceEZPayTermsLabel" value="Default Force EZ-PAY Terms Text:" style="font-weight: bold"></h:outputText>
									<h:outputText id="textDefaultForceEZPayTermsText"
										value="#{campaignDetailEditsHandler.defaultOnePageForceEZPayTermsText}"
										style="color: navy; font-family: Arial; font-weight: bold; font-size: 10px"></h:outputText>

									<h:panelGrid styleClass="panelGrid"
									id="gridCurrentForceEZPayTermsGrid" columns="2">


										<h:outputText styleClass="outputText"
											id="textCurrentForceEZPayTermsLabel" style="font-weight: bold"
											value="Current Force EZ-PAY Terms Text:"></h:outputText>
									</h:panelGrid>
									<h:outputText styleClass="outputText"
										id="textCurrentForceEZPayTermsText" escape="false"
										value="#{campaignDetailEditsHandler.currentOnePageForceEZPayTermsText}"
										style="color: navy; font-family: Arial; font-weight: bold; font-size: 10px"></h:outputText>

									<hx:outputSeparator styleClass="outputSeparator"
										id="separator2"></hx:outputSeparator>
									<h:outputText styleClass="outputText"
										id="textDefaultDisclaimerTextLabel" value="Default Disclaimer Text:" style="font-weight: bold"></h:outputText>
									<h:outputText id="textDefaultDisclaimerText" value="#{campaignDetailEditsHandler.defaultCampaign.promotionSetHandler.onePageOrderEntryDisclaimer}" escape="false"></h:outputText>

									<h:panelGrid styleClass="panelGrid"
									id="gridCurrentTermsConditionGrid" columns="2">


										<h:outputText styleClass="outputText"
											id="textCurrentTermsAndCondLabel" style="font-weight: bold"
											value="Current Disclaimer Text:"></h:outputText>
										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExCurrentDisclaimerTextDetailsImage"
											value="../../../images/icon_detailsmag14x14.gif" width="14"
											height="14" title="Click to View Raw HTML"></hx:graphicImageEx>
									</h:panelGrid>
								<h:outputText styleClass="outputText"
									id="textCurrentTermsConditionsText" escape="false" value="#{campaignDetailEditsHandler.currentOnePageDisclaimerText}"></h:outputText>

							</h:panelGrid>								
								
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<br>
					<br>
					<hx:panelDialog type="modeless" styleClass="panelDialog"
						id="dialogCurrentPromoTextDialogue" saveState="false"
						relativeTo="textPreviewPromoText" showTitleCloseButton="true" 
						title="Raw HTML" for="imageExCurrentPromoText1DetailsImage">
						<hx:jspPanel id="jspPanel1">
							<table width="350">
								<tr>
									<td><h:outputText styleClass="outputText"
										id="textPromoText1NumChars"
										style="border-bottom-width: thin; border-bottom-style: solid; font-weight: bold; font-size: 12pt; border-bottom-color: black"
										value="Max 1000 characters: Current length: #{campaignDetailEditsHandler.currentOnePagePromoText1Length}"></h:outputText>
									</td>
								</tr>
								<tr>
									<td><h:outputText styleClass="outputText_Med"
										id="textCurrentPromoText1HTML" style="color: black"
										value="#{campaignDetailEditsHandler.currentOnePagePromoText1}"></h:outputText></td>
								</tr>
								<tr>
									<td>
										<h:inputTextarea styleClass="inputTextarea"
											value="#{campaignDetailEditsHandler.currentOnePagePromoText1Raw}"
											style="color: black" id="textareaRawHtmlHeaderPromo"
											cols="100" rows="10"></h:inputTextarea></td>
								</tr>
								<tr>
									<td>
										<hx:commandExButton type="submit" value="Apply"											
											styleClass="commandExButton" id="buttonApplyRawChanges" action="#{pc_Onepage_order.doButtonApplyRawChangesAction}"></hx:commandExButton>
									</td>
								</tr>								
							</table>
						</hx:jspPanel>
					</hx:panelDialog>
					<hx:panelDialog type="modeless" styleClass="panelDialog"
						id="dialogCurrentTermsConditionTextDialgue" saveState="false"
						for="imageExCurrentDisclaimerTextDetailsImage"
						relativeTo="textCurrentTermsAndCondLabel" title="Raw HTML">
						<hx:jspPanel id="jspPanelRawTermCondTextModalPanel">
							<table width="350">
								<tr>
									<td><h:outputText styleClass="outputText"
										id="textTermsCondTextNumChars"
										style="border-bottom-width: thin; border-bottom-style: solid; font-weight: bold; font-size: 12pt; border-bottom-color: black"
										value="Max 1000 characters: Current length: #{campaignDetailEditsHandler.currentOnePageDisclaimerTextLength}"></h:outputText>
									</td>
								</tr>
								<tr>
									<td><h:outputText styleClass="outputText_Med"
										id="textCurrentDisclaimerTextHTML"
										value="#{campaignDetailEditsHandler.currentOnePageDisclaimerText}"
										style="color: black"></h:outputText></td>
								</tr>
							</table>
						</hx:jspPanel>
					</hx:panelDialog>
					<br>
				
				
				</h:form><br>
				<br>
								<h:form styleClass="form" id="formForm2">
					
					<br>
					<hx:panelDialog type="modeless" styleClass="panelDialog"
						id="dialogSelectedCampaignsForEdit" for="imageExCampaignListImage" title="Campaigns Currently Being Editted" relativeTo="imageExCampaignListImage">
							<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
								columnClasses="columnClass1, columnClass3"
								headerClass="headerClass" footerClass="footerClass"
								rowClasses="rowClass1, rowClass2" styleClass="dataTableEx"
								id="tableExSelectedCampaignsForEditV2"
								value="#{edittingCampaignsHandler.campaigns}" var="varcampaigns">
								<f:facet name="header">
									<hx:panelBox styleClass="panelBox" id="box1">
										<h:outputText styleClass="outputText"
											id="textEditCampsDTHeader" style="font-weight: bold"
											value="#{edittingCampaignsHandler.dataTableHeaderString}"></h:outputText>
									</hx:panelBox>
								</f:facet>
							<hx:columnEx id="columnEx6" rendered="false">
								<hx:inputRowSelect styleClass="inputRowSelect"
									id="rowSelectRemoveCampaignFromEdit"
									value="#{varcampaigns.campaignID}">
									<f:param name="campID" value="#{varcampaigns.campaignID}"
										id="param1"></f:param>
								</hx:inputRowSelect>
								<f:facet name="header"></f:facet>
							</hx:columnEx>
							<hx:columnEx id="columnEx3">
									<f:facet name="header">
										<h:outputText id="textEditPubHeader" styleClass="outputText"
											value="Pub"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text7"
										value="#{varcampaigns.pubcode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx4">
									<f:facet name="header">
										<h:outputText value="Keycode" styleClass="outputText"
											id="textEditKeycodeHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text8"
										value="#{varcampaigns.keycode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx5">
									<f:facet name="header">
										<h:outputText value="Name" styleClass="outputText"
											id="textEditNameHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text9"
										value="#{varcampaigns.campaignName}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx2">
									<f:facet name="header">
										<h:outputText value="Landing Page" styleClass="outputText"
											id="textEditLandingPageHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText"
										id="textEditCampaignLandingPageDes"
										value="#{varcampaigns.campaignLandingPageDescription}"></h:outputText>
								</hx:columnEx>
							</hx:dataTableEx>
						<h:panelGroup id="groupEditCampaignsFooterGroup"
							styleClass="panelDialog_Footer">
							<hx:commandExButton type="submit"
								value="Remove Campaigns From Edit List"
								styleClass="commandExButton"
								id="buttonRemoveSelectedCampaignsFromEditCollection"
								action="#{pc_EdittingCampaignsCollection.doButtonRemoveSelectedCampaignsFromEditCollectionAction}"
								confirm="Removing the selected campaigns only prevents them from being editted. It does not remove them from the system."
								rendered="false">
							</hx:commandExButton>
						</h:panelGroup>

			</hx:panelDialog>
				
				</h:form>
				
			</hx:scriptCollector><%-- /tpl:put --%> 
</div>
	<!-- end main content area -->
</body>
</html>
</f:view>
<%-- /tpl:insert --%>