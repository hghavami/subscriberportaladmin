
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="sitenav" type="com.ibm.etools.siteedit.sitelib.core.SiteNavBean" scope="request"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
<title>dropdown</title>
<link rel="stylesheet" type="text/css" href="../../../theme/dropdown_.css">
</head>
<body>
<script language="JavaScript">
	function redirect(select) {
		var page = select.options[select.selectedIndex].value;
		if (page!="#") document.location.href = page;
	}
</script>
<select onchange="redirect(this)">
<c:forEach var="item" items="${sitenav.items}" begin="0" step="1" varStatus="status">
	<c:out value='<option value="${item.href}" class="dropdown1_item_normal">${item.label}</option>' escapeXml='false'/>
</c:forEach>
</select>
</body>
</html>
