<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/TestPage.java" --%><%-- /jsf:pagecode --%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/jsf/rte" prefix="r"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="../../../dojo/dijit/themes/dijit.css">
<link rel="stylesheet" type="text/css"
	href="../../../dojo/dijit/themes/nihilo/nihilo.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/portaladmin/theme/Master.css"
	type="text/css">
<title>testPage</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript"
	djconfig="isDebug: false, parseOnLoad: true"
	src="../../../dojo/dojo/dojo.js"></script>
<script type="text/javascript">
dojo.require("dojo.parser");
dojo.require("dijit.Editor");
dojo.require("dijit.InlineEditBox");
</script>
<link rel="stylesheet" type="text/css" title="Style"
	href="../../../theme/rte_style.css">
</head>
<f:view>
	<body class="nihilo">
		<hx:scriptCollector id="scriptCollector1">
			<textarea dojotype="dijit.Editor" name="field" rows="5" cols="80"></textarea>
			<hr />
			<p>
				<br>
				<span dojotype="dijit.InlineEditBox" editor="dijit.form.TextBox">Here
					is some text you can edit inline</span>
			</p>
			<p></p>
			<h:form styleClass="form" id="form1">
				<r:inputRichText width="702" height="352" id="richTextEditor1"></r:inputRichText>
			</h:form>
		</hx:scriptCollector>
	</body>
</f:view>
</html>