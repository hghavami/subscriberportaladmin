<%-- tpl:insert page="/theme/NoNav_EditCampaigns_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
<title>$Company Name: $page name</title>
<%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<link rel="stylesheet" type="text/css" 	href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/basic.css"	type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/stylesheet.css"	type="text/css">

</head>
	<body style="background-image: url('${pageContext.request.contextPath}/theme/1x1.gif'); background-color: white;" "bgcolor="white">
	
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
		value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>
			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav2">
		<table class="hsep_table" border="0" cellspacing="0" cellpadding="0" width="800">
			<tbody class="hsep_table_body">
				<tr class="hsep_table_row">
					<td style="hsep_cell_normal" nowrap="nowrap"><a class="hsep_item_normal" style=""
						href="/portaladmin/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Review Campaigns</a> :</td>
					<td nowrap="nowrap"><siteedit:navbar group="group5"
						spec="/portaladmin/secured/campaigns/details/horizontal-sep.jsp" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		

		<!-- start main content area -->
		<div class="mainWideBox2"><a name="navskip"><img border="0"
			src="/portaladmin/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"> 
		 </a> <hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="500px" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="/portaladmin/images/arrow_expanded.gif" alt="show/hide"
						border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="baseline" height="16" width="16"
							src="/portaladmin/images/Warning.gif">Changes have been
						made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_NoNav_EditCampaigns_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="SAVE" style="font-weight: bold; font-size: 14pt"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel> <%-- tpl:put name="bodyarea" --%>
			<br>
			FUTURE<br><%-- /tpl:put --%> 
</div>
	<!-- end main content area -->
</body>
</html>
</f:view>
<%-- /tpl:insert --%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/Onepage_complete_page.java" --%><%-- /jsf:pagecode --%>
