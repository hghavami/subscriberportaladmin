<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/details/Edit_payment_page.java" --%><%-- /jsf:pagecode --%>
	<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
	<%-- tpl:insert page="/theme/NoNav_EditCampaigns_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
			<link rel="stylesheet" href="/portaladmin/theme/tabpanel.css" type="text/css">
			<link rel="stylesheet" type="text/css"
				href="/portaladmin/theme/horizontal-sep.css">
			<title>Payment Page Campaign Details</title>
		<%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<link rel="stylesheet" type="text/css" 	href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/basic.css"	type="text/css">
<link rel="stylesheet" href="service.usatoday.com/theme/themeV2/css/stylesheet.css"	type="text/css">

</head>
	<body style="background-image: url('${pageContext.request.contextPath}/theme/1x1.gif'); background-color: white;" "bgcolor="white">
	
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
		value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>
			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav2">
		<table class="hsep_table" border="0" cellspacing="0" cellpadding="0" width="800">
			<tbody class="hsep_table_body">
				<tr class="hsep_table_row">
					<td style="hsep_cell_normal" nowrap="nowrap"><a class="hsep_item_normal" style=""
						href="/portaladmin/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Review Campaigns</a> :</td>
					<td nowrap="nowrap"><siteedit:navbar group="group5"
						spec="/portaladmin/secured/campaigns/details/horizontal-sep.jsp" />
					</td>
				</tr>
			</tbody>
		</table>
		</div>
		

		<!-- start main content area -->
		<div class="mainWideBox2"><a name="navskip"><img border="0"
			src="/portaladmin/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"> 
		 </a> <hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="500px" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="/portaladmin/images/arrow_expanded.gif" alt="show/hide"
						border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="baseline" height="16" width="16"
							src="/portaladmin/images/Warning.gif">Changes have been
						made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_NoNav_EditCampaigns_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="SAVE" style="font-weight: bold; font-size: 14pt"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel> <%-- tpl:put name="bodyarea" --%><hx:scriptCollector id="scriptCollectorCampaignDetails">
				<h:messages styleClass="messages" id="messages1" style="font-weight: bold; font-size: 12pt"></h:messages><h:form styleClass="form" id="formCampaignDetails">
					<h:panelGrid styleClass="panelGrid" id="gridMainGrid" columns="2"
						cellpadding="2" style="text-align: left">
						<hx:panelBox styleClass="panelBox" id="boxPanelLeft" height="100" valign="top">
							<odc:tabbedPanel slantActiveRight="4" showBackNextButton="false"
								slantInactiveRight="4" styleClass="tabbedPanel" width="650"
								showTabs="true" variableTabLength="false" height="350"
								id="tabbedPanelPaymentCustomizations">
								<odc:bfPanel id="bfpanelEZPayConfigsPanel" name="EZ-PAY"
									showFinishCancelButton="false" rendered="false">
									<hx:panelLayout styleClass="panelLayout"
										id="layoutEZPayConfigLayout1" width="100%">
										<f:facet name="body">
											<hx:jspPanel id="jspPanelEZPayBodyInnerPanel">
												<table align="center">
													<tr>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td align="right"><h:outputText styleClass="outputText"
															id="text3" value="Override EZ-PAY Text:"
															style="font-weight: bold"></h:outputText></td>
														<td><h:selectBooleanCheckbox
															styleClass="selectBooleanCheckbox"
															id="checkboxIncludeEZPayOffer"
															value="#{campaignDetailEditsHandler.overrideZPayPaymentPageText}"></h:selectBooleanCheckbox><h:outputText
															styleClass="outputText" id="text2"
															value="If checked the red EZ-PAY text above will be overridden with the text below."></h:outputText></td>
													</tr>
													<tr>
														<td valign="top"></td>
														<td><h:outputText styleClass="outputText"
															id="textPromoTextInfoTextLabel"
															value="Enter spaces to clear text."></h:outputText></td>
													</tr>
													<tr>
														<td valign="top" align="right"><h:outputText
															styleClass="outputText" id="text4"
															value="Custom EZ-PAY Promo Text:"
															style="font-weight: bold"></h:outputText></td>
														<td><h:inputTextarea styleClass="inputTextarea"
															id="textareaEZPayCustomText" cols="50" rows="3" value="#{campaignDetailEditsHandler.customEZPayPaymentPageText}"></h:inputTextarea></td>
													</tr>
												</table>
											</hx:jspPanel>
										</f:facet>
										<f:facet name="left"></f:facet>
										<f:facet name="right"></f:facet>
										<f:facet name="bottom">

											
										</f:facet>
										<f:facet name="top">
											<hx:panelLayout styleClass="panelLayout"
												id="layoutNorthEZPayLayout" width="500" align="center">
												<f:facet name="body">
													<hx:panelBox id="bodyHeaderEzPay" height="60" style="background-color: white" valign="top"
														cellpadding="0" cellspacing="0" width="460">

														<h:outputText styleClass="outputText"
															id="textDefaultEZPayPromoText"
															value="<font face=Arial size=2><b>Please sign me up for EZ-PAY. </b>My subscription will automatically renew to my credit card.* <font color=red>#{campaignDetailEditsHandler.defaultEZPAYPaymentPageText}</font><font color=blue> Learn More...</font></font>"
															escape="false"
															style="width: 460px; background-color: white"></h:outputText>
													</hx:panelBox>
												</f:facet>
												<f:facet name="left">

													<hx:panelBox styleClass="panelBox" id="boxleftSideHeader"
														height="60" style="background-color: white" valign="top"
														cellpadding="0" cellspacing="0" width="30">
														<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
															enabledClass="selectOneRadio_Enabled"
															styleClass="selectOneRadio" id="radioDummyRadioButton"
															value="0" style="background-color: white">
															<f:selectItem itemLabel=" " itemValue="0" />
														</h:selectOneRadio>
													</hx:panelBox>
												</f:facet>
												<f:facet name="right"></f:facet>
												<f:facet name="bottom">
													<hx:outputSeparator styleClass="outputSeparator"
														id="separator1" style="margin-bottom: 15px"></hx:outputSeparator>
												</f:facet>
												<f:facet name="top">
													<h:outputText styleClass="outputText"
														id="textDefaultEZPayHeaderLabel"
														value="Default EZ-PAY Text in RED"
														style="color: white; background-color: #0080c0; width: 100%; font-weight: bold; text-align: center"></h:outputText>
												</f:facet>
											</hx:panelLayout>
										</f:facet>
									</hx:panelLayout>
								</odc:bfPanel>
								<odc:bfPanel id="bfpanelCreditCardsConfigPanel"
									name="Payment Options" showFinishCancelButton="false">
									<hx:panelLayout styleClass="panelLayout"
										id="layoutPaymeyOptionsLayout" width="100%">
										<f:facet name="body">

											<hx:panelFormBox helpPosition="over" labelPosition="left"
												styleClass="panelFormBox" id="formBoxCreditCardFormBox" widthLabel="100">
												<hx:formItem styleClass="formItem"
													id="formItemCreditCardChoices" label="Credit Cards:" infoText="Select the cards that can be used with this promotion. Default is all.">
													<h:selectManyCheckbox
														disabledClass="selectManyCheckbox_Disabled"
														styleClass="selectManyCheckbox"
														id="checkboxPaymentMethods" layout="pageDirection" value="#{campaignDetailEditsHandler.paymentMethods}">
														<f:selectItem itemLabel="American Express" itemValue="0" />
														<f:selectItem itemLabel="Visa" itemValue="1" />
														<f:selectItem itemLabel="Master Card" itemValue="2" />
														<f:selectItem itemLabel="Discover" itemValue="3" />
														<f:selectItem itemLabel="Diner's" itemValue="4" />
													</h:selectManyCheckbox>
												</hx:formItem>
											</hx:panelFormBox>
										</f:facet>
										<f:facet name="left">
											<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx1"
												value="/portaladmin/theme/img/JSF_1x1.gif" width="1" height="1" hspace="35" border="0"></hx:graphicImageEx>
										</f:facet>
										<f:facet name="right">
											
										</f:facet>
										<f:facet name="bottom"></f:facet>
										<f:facet name="top">
											<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx2"
												value="/portaladmin/theme/img/JSF_1x1.gif" width="1"
												height="1" border="0" vspace="20"></hx:graphicImageEx>
										</f:facet>
									</hx:panelLayout>
									
								</odc:bfPanel>
								<f:facet name="back">
									<hx:commandExButton type="submit" value="&lt; Back"
										id="tabbedPanelPaymentCustomizations_back" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="next">
									<hx:commandExButton type="submit" value="Next &gt;"
										id="tabbedPanelPaymentCustomizations_next" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="finish">
									<hx:commandExButton type="submit" value="Finish"
										id="tabbedPanelPaymentCustomizations_finish" style="display:none"></hx:commandExButton>
								</f:facet>
								<f:facet name="cancel">
									<hx:commandExButton type="submit" value="Cancel"
										id="tabbedPanelPaymentCustomizations_cancel" style="display:none"></hx:commandExButton>
								</f:facet>
								<odc:buttonPanel alignContent="right" id="bpCustomButtonArea">
									<hx:commandExButton type="submit" value="Apply"
										styleClass="commandExButton" id="buttonApplyChanges" action="#{pc_Edit_payment_page.doButtonApplyChangesAction}"></hx:commandExButton>
								</odc:buttonPanel>
							</odc:tabbedPanel>
						</hx:panelBox>
						<hx:panelBox styleClass="panelBox" id="boxPanelRight"
							height="100%" valign="top">
							<h:panelGrid styleClass="panelGrid"
								id="gridRightSideCurrentSettings" cellpadding="2" cellspacing="2" columns="1">
								<f:facet name="header">
									<h:panelGroup styleClass="panelGroup" id="group1">
										<h:outputText styleClass="outputText_Med"
											id="textCurrentSettingLabel" value="Current Settings"
											style="position: relative; text-decoration: underline overline; font-weight: bold; left: -30%"></h:outputText>
									</h:panelGroup>
								</f:facet>



								<h:outputText styleClass="outputText"
									id="textCurrentEZPayPromoTextLabel" style="font-weight: bold"
									value="Current EZ-PAY Offer Text:"></h:outputText>
								<h:inputTextarea styleClass="inputTextarea"
									id="textareaCurrentEZPayCustomerText"
									value="#{campaignDetailEditsHandler.currentEZPayPaymentPageText}"
									rows="2" cols="70" disabled="true" readonly="true"></h:inputTextarea>

								<h:outputText styleClass="outputText"
									id="textCurrentPaymentOptionsLabel"
									value="Current Credit Cards Accepted:"
									style="text-decoration: overline; font-weight: bold"></h:outputText>

								<h:panelGrid styleClass="panelGrid" id="gridCreditCardImageGrid"
									columns="5" cellpadding="1" cellspacing="1">
									<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx3"
										value="../../../images/american_express_logo.gif" width="42" height="26" title="Amex" rendered="#{campaignDetailEditsHandler.acceptsAmexEx}"></hx:graphicImageEx>
									<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx7"
										value="../../../images/visa_logo.gif" width="42" height="26"
										title="VISA" rendered="#{campaignDetailEditsHandler.acceptsVisa}"></hx:graphicImageEx>
									<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx4"
										value="../../../images/mc_logo.gif" width="42" height="26"
										title="MasterCard" rendered="#{campaignDetailEditsHandler.acceptsMasterCard}"></hx:graphicImageEx>
									<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx5" value="../../../images/discover_logo.gif" width="44" height="28" title="Discover" rendered="#{campaignDetailEditsHandler.acceptsDiscovery}"></hx:graphicImageEx>
									
									
									<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx6" value="../../../images/Diners_logo.jpg" width="42" height="26" title="Diner's Club" rendered="#{campaignDetailEditsHandler.acceptsDiners}"></hx:graphicImageEx>
									
								</h:panelGrid>
							</h:panelGrid>
						</hx:panelBox>
						<f:facet name="header">
							<h:panelGroup styleClass="panelGroup" id="groupHeaderGroup"
								style="line-height: 37px;height: 37px; width: 100%; left: 0px; text-align: left">
								<h:outputText styleClass="outputText_Large"
									id="textPageHeaderText" value="Payment Page Text" style="position: relative; left: 0px"></h:outputText>
								<hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExCampaignListImage" hspace="15"
									value="/portaladmin/images/icon_manage_campaigns_34x35.gif"
									width="34" height="35"
									title="Click to Show Current Campaigns Being Editted"
									align="bottom" vspace="0">
								</hx:graphicImageEx>
							</h:panelGroup>
						</f:facet>
					</h:panelGrid>
					<br>
					<br>
					
				</h:form>
				<br>
				<br>
				<br>
				<br>
				<br>
									<h:form styleClass="form" id="formForm2">
						<hx:panelDialog type="modeless" styleClass="panelDialog"
						id="dialogSelectedCampaignsForEdit" for="imageExCampaignListImage" title="Campaigns Currently Being Editted" relativeTo="imageExCampaignListImage">
							<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
								columnClasses="columnClass1, columnClass3"
								headerClass="headerClass" footerClass="footerClass"
								rowClasses="rowClass1, rowClass2" styleClass="dataTableEx"
								id="tableExSelectedCampaignsForEditV2"
								value="#{edittingCampaignsHandler.campaigns}" var="varcampaigns">
								<f:facet name="header">
									<hx:panelBox styleClass="panelBox" id="box1">
										<h:outputText styleClass="outputText"
											id="textEditCampsDTHeader" style="font-weight: bold"
											value="#{edittingCampaignsHandler.dataTableHeaderString}"></h:outputText>
									</hx:panelBox>
								</f:facet>
							<hx:columnEx id="columnEx6" rendered="false">
								<hx:inputRowSelect styleClass="inputRowSelect"
									id="rowSelectRemoveCampaignFromEdit"
									value="#{varcampaigns.campaignID}">
									<f:param name="campID" value="#{varcampaigns.campaignID}"
										id="param1"></f:param>
								</hx:inputRowSelect>
								<f:facet name="header"></f:facet>
							</hx:columnEx>
							<hx:columnEx id="columnEx3">
									<f:facet name="header">
										<h:outputText id="textEditPubHeader" styleClass="outputText"
											value="Pub"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text7"
										value="#{varcampaigns.pubcode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx4">
									<f:facet name="header">
										<h:outputText value="Keycode" styleClass="outputText"
											id="textEditKeycodeHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text8"
										value="#{varcampaigns.keycode}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx5">
									<f:facet name="header">
										<h:outputText value="Name" styleClass="outputText"
											id="textEditNameHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="text9"
										value="#{varcampaigns.campaignName}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx1">
									<f:facet name="header">
										<h:outputText value="Type" styleClass="outputText"
											id="textEditTypeColHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText" id="textEditCampaignType"
										value="#{varcampaigns.targetAudience}"></h:outputText>
								</hx:columnEx>
								<hx:columnEx id="columnEx2">
									<f:facet name="header">
										<h:outputText value="Landing Page" styleClass="outputText"
											id="textEditLandingPageHeader"></h:outputText>
									</f:facet>
									<h:outputText styleClass="outputText"
										id="textEditCampaignLandingPageDes"
										value="#{varcampaigns.campaignLandingPageDescription}"></h:outputText>
								</hx:columnEx>
							</hx:dataTableEx>
						<h:panelGroup id="groupEditCampaignsFooterGroup"
							styleClass="panelDialog_Footer">
							<hx:commandExButton type="submit"
								value="Remove Campaigns From Edit List"
								styleClass="commandExButton"
								id="buttonRemoveSelectedCampaignsFromEditCollection"
								action="#{pc_EdittingCampaignsCollection.doButtonRemoveSelectedCampaignsFromEditCollectionAction}"
								confirm="Removing the selected campaigns only prevents them from being editted. It does not remove them from the system."
								rendered="false">
							</hx:commandExButton>
						</h:panelGroup>

			</hx:panelDialog>
				
				</h:form>
				<br>
				<br>
			</hx:scriptCollector><%-- /tpl:put --%> 
</div>
	<!-- end main content area -->
</body>
</html>
</f:view>
<%-- /tpl:insert --%>

