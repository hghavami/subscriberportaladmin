
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:useBean id="sitenav" type="com.ibm.etools.siteedit.sitelib.core.SiteNavBean" scope="request"/>
<html>
<head>
<title>horizontal-sep</title>
<link rel="stylesheet" type="text/css" href="/portaladmin/theme/horizontal-sep.css">
</head>
<body>
<table class="hsep_table" cellpadding="0" cellspacing="3">
	<tbody class="hsep_table_body">
		<tr class="hsep_table_row">
<c:forEach var="item" items="${sitenav.items}" begin="0" step="1" varStatus="status">
 <c:choose>
  <c:when test="${item.group}">
   <c:if test="${!status.first}">
			<td class="hsep_cell_separator"></td>
			<td class="hsep_cell_separator"></td>
   </c:if>
			<td class="hsep_cell_group">
			<span class="hsep_item_group"><c:out value='${item.label}' escapeXml='false'/></span>
			</td>
  </c:when>
  <c:otherwise>
   <c:if test="${!status.first}">
			<td class="hsep_cell_separator"></td>
   </c:if>
			<td class="hsep_cell_normal" nowrap>
			<c:out value='<a href="${item.href}" class="hsep_item_normal">${item.label}</a>' escapeXml='false'/>
			</td>
  </c:otherwise>
 </c:choose>
</c:forEach>
		</tr>
	</tbody>
</table>
</body>
</html>
