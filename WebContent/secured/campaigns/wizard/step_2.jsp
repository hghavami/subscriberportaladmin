<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/wizard/Step_2.java" --%><%-- /jsf:pagecode --%>
	<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
	<%@taglib uri="http://www.ibm.com/jsf/rte" prefix="r"%>
	<%-- tpl:insert page="/theme/primaryNoNave_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
			<title>New Campaign: Step 2</title>
		<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
var groupObj = $('formWizardForm:textGroupName');

if (thisObj.checked == true) {
	groupObj.enable();
	groupObj.removeClassName('inputText_Disabled');
}
else {
	groupObj.addClassName('inputText_Disabled');
	groupObj.disable();
}
}</script><%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">

<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">

</head>
	<body>
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
				value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr valign="top"> 
			<td><a class="" style="" href="${pageContext.request.contextPath}/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Campaign Details</a></td> 
</tr></tbody></table> </div>

		<!-- start main content area -->
		<div class="mainWideBox"><a name="navskip"><img border="0"
			src="${pageContext.request.contextPath}/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"></a> 
			<hx:scriptCollector id="scriptCollectorNoNavTemplateCollector">
		<hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="75%" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="${pageContext.request.contextPath}/images/arrow_expanded.gif"
						alt="show/hide" border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="bottom" height="16" width="16"
							src="${pageContext.request.contextPath}/images/Warning.gif">Changes
						have been made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_PrimaryNoNave_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="Save"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel>
	</hx:scriptCollector>
		
 <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollector1">
				<h:form styleClass="form" id="formWizardForm">
					<table border="0" cellpadding="0" cellspacing="1">
						<tbody>
							<tr>
								<td colspan="3"><h:messages styleClass="messages" id="messages1"
									layout="table" style="font-weight: bold; font-size: 12pt"></h:messages></td>
							</tr>
							<tr>
								<td colspan="3"><h:outputText styleClass="outputText" id="text2" value="New Campaign For Product:" style="font-weight: bold; font-size: 14pt"></h:outputText><h:outputText
									styleClass="outputText" id="text3"
									value="#{newCampaignWizard.product.description}"
									style="color: #004080; font-size: 14pt"></h:outputText></td>
							</tr>
							<tr>
								<td><hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExSWImage"
									value="/portaladmin/images/swPrimaryLogo32x27.jpg" width="32"
									height="27" rendered="#{not newCampaignWizard.UTCampaign }"></hx:graphicImageEx><hx:graphicImageEx
									styleClass="graphicImageEx" id="imageExUTLogoSmall"
									value="/portaladmin/images/utPrimaryLogo32x26.jpg" width="32"
									height="26" rendered="#{newCampaignWizard.UTCampaign}"></hx:graphicImageEx></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="2" rowspan="2" valign="bottom"><hx:panelFormBox
									helpPosition="over" labelPosition="left"
									styleClass="panelFormBox" id="formBoxLeftBox"
									label="Keycode Selection:">
									<hx:jspPanel id="jspPanel1">
										<hx:panelLayout styleClass="panelLayout" id="layout1"
											align="center">
											<f:facet name="body">

												<h:panelGrid styleClass="panelGrid" id="gridKeycodeMovers"
													cellpadding="0" width="45" cellspacing="2"
													style="text-align: center">
													<hx:outputSeparator styleClass="outputSeparator"
														id="separator1" height="2"></hx:outputSeparator>
													<hx:commandExButton type="submit" id="buttonAddKeycode"
														title="Add" image="/portaladmin/images/rightArrow.gif"
														style="height: 26px; width: 39px"
														action="#{pc_Step_2.doButtonAddKeycodeAction}"></hx:commandExButton>

													<hx:commandExButton type="submit" id="buttonRemoveKeycodes"
														title="Remove" image="/portaladmin/images/leftArrow.gif"
														style="height: 26px; width: 39px"
														action="#{pc_Step_2.doButtonRemoveKeycodesAction}">
													</hx:commandExButton>
													<hx:outputSeparator styleClass="outputSeparator"
														id="separator2" height="2"></hx:outputSeparator>
												</h:panelGrid>
											</f:facet>
											<f:facet name="left">
												<h:selectManyListbox styleClass="selectManyListbox"
													id="listboxAvailableKeycodes" size="10"
													value="#{newCampaignWizard.tempSelectedKeyCodesStrings}"
													style="width: 350px">
													<f:selectItems
														value="#{selectitems.newCampaignWizard.keyCodesForSelectedPub.description.description.toArray}" />
												</h:selectManyListbox>
											</f:facet>
											<f:facet name="right">
												<h:selectManyListbox styleClass="selectManyListbox"
													id="listboxSelectedKeycodes" size="10" value="#{newCampaignWizard.tempRemoveSelectedKeyCodeStrings}">
													<f:selectItems
														value="#{selectitems.newCampaignWizard.selectedKeyCodesForNewCampaigns.description.description.toArray}" />
												</h:selectManyListbox>
											</f:facet>
											<f:facet name="bottom">
											</f:facet>
											<f:facet name="top">
												<hx:jspPanel id="jspPanel2">
													<table border="0" cellpadding="1" cellspacing="1"
														bgcolor="white">
														<tbody>
															<tr>
																<td colspan="2" class="panelFormBox_Label-Cell"><h:outputText
																	styleClass="panelFormBox_Label formItem"
																	id="textKeycodeLabel" value="Choose Keycode(s):"
																	style="margin-left: 1px;"></h:outputText></td>
															</tr>
															<tr>
																<td><h:inputText styleClass="inputText"
																	id="textKeycodeFilter" size="5"
																	style="margin-left: 4px" value="#{newCampaignWizard.keyCodeFilter}"></h:inputText></td>
																<td><hx:commandExButton type="submit"
																	value="Apply Filter" styleClass="commandExButton"
																	id="buttonKeycodeFilter" style="margin-left: 5px" action="#{pc_Step_2.doButtonKeycodeFilterAction}"></hx:commandExButton><hx:commandExButton
																	type="submit" value="Clear"
																	styleClass="commandExButton"
																	id="buttonClearKeycodeFilter" style="margin-left: 5px"
																	action="#{pc_Step_2.doButtonClearKeycodeFilterAction}"></hx:commandExButton></td>
															</tr>
															<tr>
																<td></td>
																<td></td>
															</tr>
														</tbody>
													</table>
												</hx:jspPanel>
											</f:facet>
										</hx:panelLayout>
									</hx:jspPanel>
								</hx:panelFormBox></td><td align="right">
								
														
								</td>
							</tr>
							<tr><td valign="bottom"><hx:panelFormBox helpPosition="over"
									labelPosition="left" styleClass="panelFormBox"
									id="formBoxRight" label=" Grouping Options:"
									style="margin-left: 5px" widthLabel="130">
									<hx:formItem styleClass="formItem" id="formItemAddToGroupFlag"
										label="Include In Group:"
										infoText="(only if campaign includes more than one keycode)">
										<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
											id="checkboxIncludeInGroup"
											onclick="return func_1(this, event);"
											value="#{newCampaignWizard.includeInGroup}">
										</h:selectBooleanCheckbox>
									</hx:formItem>
									<hx:formItem styleClass="formItem" id="formItemUTGrouping"
										label="Campaign Group:"
										infoText="A campaign may optionally belong to a group. You may enter an existing group or enter a new group name.">
										<h:inputText styleClass="inputText" id="textGroupName"
											size="30" value="#{newCampaignWizard.groupName}">
											<hx:inputHelperTypeahead styleClass="inputText_Typeahead"
												id="typeaheadGroupNameSuggestions" startDelay="500"
												startCharacters="1" value="#{groupNameSuggestions}"
												size="10"></hx:inputHelperTypeahead>
										</h:inputText>
									</hx:formItem>

									<f:facet name="bottom">
										<hx:panelLayout styleClass="panelLayout"
											id="layoutFooterLayout" width="100%">
											<f:facet name="body">
												<h:outputText styleClass="outputText" id="textFiller1"></h:outputText>
											</f:facet>
											<f:facet name="left"></f:facet>
											<f:facet name="right">
												<h:panelGrid styleClass="panelGrid" id="gridFooterGrid"
													columns="2" cellpadding="4" cellspacing="1"
													style="text-align: right">
													<hx:commandExButton type="submit" id="buttonPrevStep"
														action="#{pc_Step_2.doButtonPrevStepAction}"
														image="/portaladmin/images/backButton.gif"
														style="height: 26px; width: 76px"></hx:commandExButton>
													<hx:commandExButton type="submit" id="buttonNextStep"
														action="#{pc_Step_2.doButtonNextStepAction}"
														image="/portaladmin/images/nextButton.gif"
														style="height: 26px; width: 76px"></hx:commandExButton>
												</h:panelGrid>
											</f:facet>
											<f:facet name="bottom"></f:facet>
											<f:facet name="top"></f:facet>
										</hx:panelLayout>
									</f:facet>
									<f:facet name="left">
										<h:outputText styleClass="outputText" id="text1filler"
											value=" "></h:outputText>
									</f:facet>
								</hx:panelFormBox></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</h:form>
				<br>
</hx:scriptCollector>
		<%-- /tpl:put --%> </div>
		<!-- end main content area -->
	</body>
</html>
</f:view>
<%-- /tpl:insert --%>

