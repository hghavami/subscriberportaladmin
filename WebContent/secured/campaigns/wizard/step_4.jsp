<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/wizard/Step_4.java" --%><%-- /jsf:pagecode --%>
	<%-- tpl:insert page="/theme/primaryNoNave_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
			<title>New Campaign Wizard - Step 4</title>
		<script type="text/javascript"></script><%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">

<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">

</head>
	<body>
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
				value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr valign="top"> 
			<td><a class="" style="" href="${pageContext.request.contextPath}/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Campaign Details</a></td> 
</tr></tbody></table> </div>

		<!-- start main content area -->
		<div class="mainWideBox"><a name="navskip"><img border="0"
			src="${pageContext.request.contextPath}/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"></a> 
			<hx:scriptCollector id="scriptCollectorNoNavTemplateCollector">
		<hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="75%" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="${pageContext.request.contextPath}/images/arrow_expanded.gif"
						alt="show/hide" border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="bottom" height="16" width="16"
							src="${pageContext.request.contextPath}/images/Warning.gif">Changes
						have been made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_PrimaryNoNave_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="Save"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel>
	</hx:scriptCollector>
		
 <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollector1">
				<h:form styleClass="form" id="formWizardForm">
					<table border="0" cellspacing="1" cellpadding="1">
						<tbody>
							<tr>
								<td>											<h:outputText styleClass="outputText" id="text21"
												value="New Campaign For Product:"
												style="font-weight: bold; font-size: 14pt"></h:outputText>
											<h:outputText styleClass="outputText" id="text3"
												value="#{newCampaignWizard.product.description}"
												style="font-size: 14pt"></h:outputText>
								</td>
								<td></td>
							</tr>
							<tr>
								<td><h:messages styleClass="messages" id="messages1"
									style="font-weight: bold; font-size: 12pt" layout="table"></h:messages></td>
								<td></td>
							</tr>
							<tr>
								<td><h:outputText styleClass="outputText, panelFormBox_Header"
									id="textHeaderText" value="Campaign Summary" style="margin-bottom: 2px; padding-bottom: 3px"></h:outputText></td>
								<td></td>
							</tr>
							<tr>
								<td><hx:dataTableEx id="tableExNewCampaigns"
									value="#{newCampaignWizard.newCampaigns}" var="varnewCampaigns"
									styleClass="dataTableEx" headerClass="headerClass"
									footerClass="footerClass" rowClasses="rowClass1, rowClass2"
									columnClasses="columnClass1" border="0" cellpadding="2"
									cellspacing="0">
									<hx:columnEx id="columnEx1">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Pubcode"
												id="text2"></h:outputText>
										</f:facet>
										<h:outputText id="textPubcode1"
											value="#{varnewCampaigns.pubcode}" styleClass="outputText">
										</h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx2">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Keycode"
												id="text3"></h:outputText>
										</f:facet>
										<h:outputText id="textKeycode1"
											value="#{varnewCampaigns.keycode}" styleClass="outputText">
										</h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx3">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Campaign Name"
												id="text4"></h:outputText>
										</f:facet>
										<h:outputText id="textCampaignName1"
											value="#{varnewCampaigns.campaignName}"
											styleClass="outputText">
										</h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx4" align="center">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Campaign Group"
												id="text5"></h:outputText>
										</f:facet>
										<h:outputText id="textCampaignGroup1"
											value="#{varnewCampaigns.campaignGroup}"
											styleClass="outputText">
										</h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx6">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Vanity URL"
												id="text7"></h:outputText>
										</f:facet>
										<h:outputText id="textVanityURL1"
											value="#{varnewCampaigns.vanityURL}" styleClass="outputText">
										</h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx5">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Landing Page"
												id="text6"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textCampaignType"
											value="#{varnewCampaigns.campaignLandingPageDescription}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx8" align="center">
										<f:facet name="header">
											<h:outputText styleClass="outputText"
												value="Campaign Status String" id="text9"></h:outputText>
										</f:facet>
										<h:outputText id="textCampaignStatusString1"
											value="#{varnewCampaigns.campaignStatusString}"
											styleClass="outputText">
										</h:outputText>
									</hx:columnEx>
								</hx:dataTableEx></td>
								<td></td>
							</tr>
							<tr>
								<td align="right">
								<table border="0" cellpadding="0" cellspacing="1">
									<tbody>
										<tr>
											<td colspan="2"><h:outputText styleClass="outputText_Med"
												id="textOptionsHeader" value="After Saving I want to: " style="font-weight: bold"></h:outputText><h:outputText
												styleClass="outputText" id="textAddtlinfo"
												value="(check all that apply)" style="font-size: 9pt"></h:outputText></td><td></td>
										</tr>
										<tr>
											<td valign="top"><h:selectBooleanCheckbox
												styleClass="selectBooleanCheckbox" id="checkboxEditDetails"
												value="#{newCampaignWizard.editDetails}"></h:selectBooleanCheckbox></td>
											<td><h:outputText styleClass="outputText"
												id="textEditDetailLabel" value="Edit Campaign Details"></h:outputText><br>
											<h:outputText styleClass="outputText" id="textUnderText"
												value="(Will apply to all keycodes above and any tied to the same group.)"
												style="font-size: 10px"></h:outputText>
											</td>
											<td></td>
										</tr>
										<tr>
											<td><h:selectBooleanCheckbox
												styleClass="selectBooleanCheckbox" id="checkboxAutoPublish" value="#{newCampaignWizard.autoPublish}"></h:selectBooleanCheckbox></td>
											<td><h:outputText styleClass="outputText"
												id="textPublishLabel" value="Automatically Publish Campaigns"></h:outputText></td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</td>
								<td></td>
							</tr>
							<tr>
								<td align="right"><hx:commandExButton type="submit"
									id="buttonPrevStep"
									action="#{pc_Step_4.doButtonPrevStepAction}"
									image="/portaladmin/images/backButton.gif"
									style="margin-right: 3px"></hx:commandExButton><hx:commandExButton
									type="submit" id="buttonSaveCampaigns"
									action="#{pc_Step_4.doButtonSaveCampaignsAction}"
									image="/portaladmin/images/saveCampaigns.gif"></hx:commandExButton></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<br>
					<br>
				</h:form>
				<br>
				<br>
				<br>
			</hx:scriptCollector>
		<%-- /tpl:put --%> </div>
		<!-- end main content area -->
	</body>
</html>
</f:view>
<%-- /tpl:insert --%>

