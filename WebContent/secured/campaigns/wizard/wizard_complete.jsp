<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/wizard/Wizard_complete.java" --%><%-- /jsf:pagecode --%>
	<%-- tpl:insert page="/theme/primaryNoNave_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
			<title>New Campaign Wizard Complete</title>
		<script type="text/javascript"></script><%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">

<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">

</head>
	<body>
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
				value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr valign="top"> 
			<td><a class="" style="" href="${pageContext.request.contextPath}/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Campaign Details</a></td> 
</tr></tbody></table> </div>

		<!-- start main content area -->
		<div class="mainWideBox"><a name="navskip"><img border="0"
			src="${pageContext.request.contextPath}/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"></a> 
			<hx:scriptCollector id="scriptCollectorNoNavTemplateCollector">
		<hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="75%" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="${pageContext.request.contextPath}/images/arrow_expanded.gif"
						alt="show/hide" border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="bottom" height="16" width="16"
							src="${pageContext.request.contextPath}/images/Warning.gif">Changes
						have been made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_PrimaryNoNave_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="Save"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel>
	</hx:scriptCollector>
		
 <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollector1">
				<h:form styleClass="form" id="formWizardForm">
					<table border="0" cellspacing="1" cellpadding="1">
						<tbody>
							<tr>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td><h:messages styleClass="messages" id="messages1" layout="table" style="font-weight: bold; font-size: 12pt"></h:messages></td>
								<td></td>
							</tr>
							<tr>
								<td><h:outputText styleClass="outputText, panelFormBox_Header"
									id="textHeaderText" value="Errors Occurerd with following:"
									style="margin-bottom: 2px; padding-bottom: 3px" rendered="#{newCampaignWizard.publishingFailed}"></h:outputText></td>
								<td></td>
							</tr>
							<tr>
								<td><hx:dataTableEx id="tableExNewCampaigns"
									value="#{newCampaignWizard.publishingFailures}"
									var="varpublishingFailures" styleClass="dataTableEx"
									headerClass="headerClass" footerClass="footerClass"
									rowClasses="rowClass1, rowClass2" columnClasses="columnClass1"
									border="0" cellpadding="2" cellspacing="0" rendered="#{newCampaignWizard.publishingFailed}">
									<hx:columnEx id="columnEx1">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Pubcode"
												id="text2"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textPubCode" value="#{newCampaignWizard.publishingFailures[0].campaign.pubCode}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx2">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Keycode"
												id="text3"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textKeyCode" value="#{newCampaignWizard.publishingFailures[0].campaign.keyCode}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx3">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Campaign Name"
												id="text4"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textCampName" value="#{newCampaignWizard.publishingFailures[0].campaign.campaignName}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx4">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Campaign Group"
												id="text5"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textCampGroupName" value="#{newCampaignWizard.publishingFailures[0].campaign.groupName}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx6">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Vanity URL"
												id="text7"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textVanity" value="#{varpublishingFailures.vanityURL}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx8">
										<f:facet name="header">
											<h:outputText styleClass="outputText" value="Campaign Status"
												id="text9"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textCampStatus"
											value="#{varpublishingFailures.campaignStateDescription}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx5">
										<f:facet name="header">
											<h:outputText value="Reason" styleClass="outputText"
												id="textErrorReasonLabel"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textErrorReason"
											value="#{varpublishingFailures.errorReason}" style="font-weight: bold"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx7">
										<f:facet name="header">
											<h:outputText value="Recommendation" styleClass="outputText"
												id="textRecommendedActionLabel"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text6" value="#{varpublishingFailures.recommendedAction}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx9">
										<f:facet name="header">
											<h:outputText value="Exception Detail (if available)"
												styleClass="outputText" id="textExceptionLabel"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="textExeptionDetail"
											value="#{newCampaignWizard.publishingFailures[0].underlyingException.message}"
											rendered=""></h:outputText>
									</hx:columnEx>
								</hx:dataTableEx></td>
								<td></td>
							</tr>
							<tr>
								<td align="left">
								
								<h:outputText styleClass="outputText, panelFormBox_Header"
									id="textAllCampaignStatusHeader"
									style="margin-bottom: 2px; padding-bottom: 3px" value="Summary:"></h:outputText></td>
								<td></td>
							</tr>
							<tr>
								<td align="left"><hx:dataTableEx border="0" cellpadding="2"
									cellspacing="0" columnClasses="columnClass1"
									headerClass="headerClass" footerClass="footerClass"
									rowClasses="rowClass1, rowClass2" styleClass="dataTableEx"
									id="tableExNewCampaignstable" value="#{newCampaignWizard.newCampaigns}" var="varnewCampaigns">
									<hx:columnEx id="columnEx10">
										<f:facet name="header">
											<h:outputText id="text8" styleClass="outputText"
												value="Pubcode"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text15" value="#{varnewCampaigns.pubcode}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx11">
										<f:facet name="header">
											<h:outputText value="Keycode" styleClass="outputText"
												id="text10"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text16" value="#{varnewCampaigns.keycode}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx12">
										<f:facet name="header">
											<h:outputText value="Campaign Name" styleClass="outputText"
												id="text11"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text17" value="#{varnewCampaigns.campaignName}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx13" align="center">
										<f:facet name="header">
											<h:outputText value="Campaign Group" styleClass="outputText"
												id="text12"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text18" value="#{varnewCampaigns.campaignGroup}"></h:outputText>
									</hx:columnEx>
									<hx:columnEx id="columnEx14">
										<f:facet name="header">
											<h:outputText value="Vanity URL" styleClass="outputText"
												id="text13"></h:outputText>
										</f:facet>
										<hx:outputLinkEx styleClass="outputLinkEx" id="linkExProdLink"
											value="#{varnewCampaigns.fullVanityURL}" target="_blank">
											<h:outputText id="text20" styleClass="outputText"
												value="#{varnewCampaigns.fullVanityURLText}"></h:outputText>
										</hx:outputLinkEx>
									</hx:columnEx>
									<hx:columnEx id="columnEx15">
										<f:facet name="header">
											<h:outputText value="Campaign Status" styleClass="outputText"
												id="text14"></h:outputText>
										</f:facet>
										<h:outputText styleClass="outputText" id="text19" value="#{varnewCampaigns.campaignStatusString}" style="font-weight: bold"></h:outputText>
									</hx:columnEx>
								</hx:dataTableEx></td>
								<td></td>
							</tr>
							<tr>
								<td align="center"><hx:commandExButton type="submit"
									value="Finish" styleClass="commandExButton"
									id="buttonAllFinished"
									action="#{pc_Wizard_failed.doButtonAllFinishedAction}"></hx:commandExButton></td>
								<td></td>
							</tr>
							<tr>
								<td align="left"></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<br>
					<br>
				</h:form>
				<br></hx:scriptCollector>
		<%-- /tpl:put --%> </div>
		<!-- end main content area -->
	</body>
</html>
</f:view>
<%-- /tpl:insert --%>

