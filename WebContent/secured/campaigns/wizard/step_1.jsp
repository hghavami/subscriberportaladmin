<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/wizard/Step_1.java" --%><%-- /jsf:pagecode --%>
	<%-- tpl:insert page="/theme/primaryNoNave_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
			<title>New Campaign: Step 1</title>
		<%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">

<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">

</head>
	<body>
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
				value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr valign="top"> 
			<td><a class="" style="" href="${pageContext.request.contextPath}/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Campaign Details</a></td> 
</tr></tbody></table> </div>

		<!-- start main content area -->
		<div class="mainWideBox"><a name="navskip"><img border="0"
			src="${pageContext.request.contextPath}/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"></a> 
			<hx:scriptCollector id="scriptCollectorNoNavTemplateCollector">
		<hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="75%" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="${pageContext.request.contextPath}/images/arrow_expanded.gif"
						alt="show/hide" border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="bottom" height="16" width="16"
							src="${pageContext.request.contextPath}/images/Warning.gif">Changes
						have been made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_PrimaryNoNave_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="Save"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel>
	</hx:scriptCollector>
		
 <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollectorStep1">
<h:form styleClass="form" id="formStep1">
					<hx:panelFormBox id="formBoxNewCampaign" styleClass="panelFormBox"
						helpPosition="over" label="Choose Publication:"
						labelPosition="left">
						<hx:formItem styleClass="formItem" id="formItemPublication"
							infoText="Select One:">
							<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
								enabledClass="selectOneRadio_Enabled"
								styleClass="selectOneRadio" id="radioPubChoice"
								value="#{newCampaignWizard.publication}" layout="pageDirection"
								style="font-size: 14pt">
								<f:selectItems
									value="#{productHandler.availableProductsSelectItems}"
									id="selectItems1" />
							</h:selectOneRadio>
						</hx:formItem>
						<f:facet name="top">

						</f:facet>
						<f:facet name="bottom">
							<hx:commandExButton type="submit" id="buttonNextStep"
								action="#{pc_Step_1.doButtonNextStepAction}"
								style="height: 26px; margin-right: 5px; margin-bottom: 3px; width: 76px"
								image="/portaladmin/images/nextButton.gif"></hx:commandExButton>
						</f:facet>
						<f:facet name="left">
							<h:panelGrid styleClass="panelGrid" id="grid1" columns="1">

								<hx:graphicImageEx styleClass="graphicImageEx"
									id="imageExUTLogo"
									value="/portaladmin/images/utPrimaryLogo.jpg" hspace="10"
									vspace="15"></hx:graphicImageEx>
								<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx1"
									value="/portaladmin/images/swPrimaryLogo.jpg" vspace="15"
									hspace="10"></hx:graphicImageEx>

							</h:panelGrid>
						</f:facet>
					</hx:panelFormBox>
				</h:form>
<br>

</hx:scriptCollector>
		<%-- /tpl:put --%> </div>
		<!-- end main content area -->
	</body>
</html>
</f:view>
<%-- /tpl:insert --%>

