<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/wizard/Step_3.java" --%><%-- /jsf:pagecode --%>
	<%-- tpl:insert page="/theme/primaryNoNave_JSP-C-03_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<f:view>
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
<%-- tpl:put name="headarea" --%>
			<title>New Campaign Wizard - Step 3</title>
		<script type="text/javascript">
</script><%-- /tpl:put --%>

<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">

<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" type="text/css">

</head>
	<body>
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white;font-size: 24px;text-decoration:none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif" alt="skip to page's main contents"
			border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">

			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
				value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>	
		</div>			
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><table border="0" cellspacing="0" cellpadding="0"><tbody><tr valign="top"> 
			<td><a class="" style="" href="${pageContext.request.contextPath}/secured/campaigns/details/campaign_details.faces">&#0187;&nbsp;Campaign Details</a></td> 
</tr></tbody></table> </div>

		<!-- start main content area -->
		<div class="mainWideBox"><a name="navskip"><img border="0"
			src="${pageContext.request.contextPath}/theme/1x1.gif" width="1" height="1"
			alt="Beginning of page content"></a> 
			<hx:scriptCollector id="scriptCollectorNoNavTemplateCollector">
		<hx:jspPanel id="jspPanelMessagesPanel"
			rendered="#{campaignDetailEditsHandler.changed}">
			<table class="messagePortlet" border="0" cellpadding="0"
				cellspacing="0" valign="top" width="75%" summary="Inline Messages">
				<tr valign="top">
					<td class="messageTitle"><a
						href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
						class="expand-task"><img id="com_ibm_ws_inlineMessagesImg"
						src="${pageContext.request.contextPath}/images/arrow_expanded.gif"
						alt="show/hide" border="0" align="middle"> Messages</a></td>
				</tr>
				<tbody id="com_ibm_ws_inlineMessages">
					<tr>
						<td class="complex-property"><span
							class="validation-warn-info"> <img alt="Warning"
							align="bottom" height="16" width="16"
							src="${pageContext.request.contextPath}/images/Warning.gif">Changes
						have been made. Click <hx:requestLink styleClass="requestLink"
							id="linkNoNavMessagesMasterSave"
							action="#{pc_PrimaryNoNave_JSPC03_blue.doLinkNoNavMessagesMasterSaveAction}">
							<h:outputText id="text1" styleClass="outputText" value="Save"></h:outputText>
						</hx:requestLink> to apply changes to the database.</span><br>
						<br>
						</td>
					</tr>
				</tbody>
			</table>
		</hx:jspPanel>
	</hx:scriptCollector>
		
 <%-- tpl:put name="bodyarea" --%>
			<hx:scriptCollector id="scriptCollectorStep3WizardForm">
				<h:form styleClass="form" id="formWizardForm">
					<table border="0" cellpadding="0" cellspacing="1">
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="2"><h:messages styleClass="messages" id="messages1" globalOnly="true" layout="table" style="font-weight: bold; font-size: 12pt"></h:messages></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="3">
												<h:outputText styleClass="outputText"
													id="textNewCampProdDesLabelHead1"
													value="New Campaign For Product:"
													style="font-weight: bold; font-size: 14pt"></h:outputText>
												<h:outputText styleClass="outputText"
									id="textNewCampProdDescHead"
									value="#{newCampaignWizard.product.description}"
									style="color: #004080; font-size: 14pt"></h:outputText>
								
								</td>
							</tr>
							<tr>
								<td colspan="2" class="panelFormBox"><hx:panelLayout
									styleClass="panelLayout" id="layoutNewCampaignsArea">
									<f:facet name="body">

										<hx:dataTableEx border="0" cellpadding="2" cellspacing="2"
											columnClasses="columnClass1" headerClass="headerClass"
											footerClass="footerClass" rowClasses="rowClass1,rowClass2"
											style="margin-top: 5px" styleClass="dataTableEx"
											id="tableExNewCampaigns"
											value="#{newCampaignWizard.newCampaigns}"
											var="varnewCampaigns">
											<hx:columnEx id="columnEx2">
												<f:facet name="header">
													<h:outputText value="Pub" styleClass="outputText"
														id="text2"></h:outputText>
												</f:facet>
												<h:outputText styleClass="outputText" id="textCampaignPub"
													value="#{varnewCampaigns.pubcode}"></h:outputText>
											</hx:columnEx>
											<hx:columnEx id="columnEx1">
												<f:facet name="header">
													<h:outputText id="text1" styleClass="outputText"
														value="Keycode"></h:outputText>
												</f:facet>
												<h:outputText styleClass="outputText"
													id="textCampaignKeycode" value="#{varnewCampaigns.keycode}"></h:outputText>
											</hx:columnEx>
											<hx:columnEx id="columnEx3" nowrap="true" align="center">
												<f:facet name="header">
													<h:outputText value="Group Name" styleClass="outputText"
														id="textGroupNameColLabel"></h:outputText>
												</f:facet>
												<h:outputText styleClass="outputText" id="textGroupName"
													value="#{newCampaignWizard.groupName}"></h:outputText>
											</hx:columnEx>
											<hx:columnEx id="columnEx4" nowrap="true">
												<f:facet name="header">
													<h:outputText value="Campaign Name" styleClass="outputText"
														id="text4"></h:outputText>
												</f:facet>
												<h:inputText styleClass="inputText" id="textCampaignName"
													value="#{varnewCampaigns.campaignName}" size="40"></h:inputText>
											</hx:columnEx>
											<hx:columnEx id="columnEx5">
												<f:facet name="header">
													<h:outputText value="Desired Vanity"
														styleClass="outputText" id="text5"></h:outputText>
												</f:facet>
												<hx:jspPanel id="jspPanelVanityDetailPanel">
												<table>
													<tr><td nowrap="nowrap"><h:inputText styleClass="inputText" id="textCampaignVanity"
													value="#{varnewCampaigns.vanityURL}" size="20"></h:inputText></td></tr>
													<tr><td><h:outputText styleClass="message"
															id="textVanityErrorMessage"
															style="color: red; font-weight: bold"
															value="#{varnewCampaigns.campaignErrorMessage}"
															escape="false"></h:outputText></td></tr>
												</table>
												</hx:jspPanel>

											</hx:columnEx>
											<hx:columnEx id="columnEx6" align="center">
												<f:facet name="header">
													<h:outputText value="Redirect To Page"
														styleClass="outputText" id="textRedirectToPage"></h:outputText>
												</f:facet>
												<h:selectOneMenu styleClass="selectOneMenu"
													id="menuCampaignType"
													value="#{varnewCampaigns.redirectToPage}">
													<f:selectItem itemLabel="One Page Order Form"
														itemValue="TERMS" />
													<f:selectItem itemLabel="Subscribe Page (deprecated)"
														itemValue="WELCOME" id="selectItem1" />
												</h:selectOneMenu>
											</hx:columnEx>

										</hx:dataTableEx>

									</f:facet>
									<f:facet name="left"></f:facet>
									<f:facet name="right"></f:facet>
									<f:facet name="bottom">
										<hx:panelLayout styleClass="panelLayout"
											id="layoutFooterLayout" width="100%"
											style="border-top-color: #809eba; border-top-style: solid; border-top-width: medium">
											<f:facet name="body">
												<h:outputText styleClass="outputText" id="text12Filler"></h:outputText>
											</f:facet>
											<h:outputText styleClass="outputText" id="textFiller11"></h:outputText>
											<f:facet name="left"></f:facet>
											<f:facet name="right">
												<h:panelGrid styleClass="panelGrid" id="gridPrevNextButtons"
													columns="2" cellspacing="2">
													<hx:commandExButton type="submit"
														id="buttonPrevStep"
														action="#{pc_Step_3.doButtonPrevStepAction}"
														image="/portaladmin/images/backButton.gif" style="height: 26px; width: 76px" confirm="If you go back you will lose any changes you have made to this page. Please confirm."></hx:commandExButton>
													<hx:commandExButton type="submit"
														id="buttonNextStep"
														action="#{pc_Step_3.doButtonNextStepAction}"
														image="/portaladmin/images/nextButton.gif" style="height: 26px; width: 76px"></hx:commandExButton>
												</h:panelGrid>
											</f:facet>
											<f:facet name="bottom">
											</f:facet>
											<f:facet name="top"></f:facet>
										</hx:panelLayout>
									</f:facet>
									<f:facet name="top">

										<hx:panelLayout styleClass="panelLayout"
											id="layoutHeaderLayoutPanel" width="100%">
											<f:facet name="body">
												<h:outputText styleClass="panelFormBox_Header"
													id="textHeaderText"
													value="Assign Campaign Name / Specify Desired Vanity / Landing Page"
													style="margin-bottom: 2px; padding-bottom: 3px"></h:outputText>
											</f:facet>
											<f:facet name="left"></f:facet>
											<f:facet name="right">
												<h:panelGrid styleClass="panelGrid"
													id="gridCampaignImageGrid" columns="2" cellpadding="1" cellspacing="1">
													<hx:graphicImageEx styleClass="graphicImageEx"
														id="imageExSWImageStep3"
														value="/portaladmin/images/swPrimaryLogo32x27.jpg"
														width="32" height="27" rendered="#{not newCampaignWizard.UTCampaign }"></hx:graphicImageEx>
													<hx:graphicImageEx styleClass="graphicImageEx"
														id="imageExUTLogoStep3"
														value="/portaladmin/images/utPrimaryLogo32x26.jpg"
														width="32" height="26" rendered="#{newCampaignWizard.UTCampaign }"></hx:graphicImageEx>
												</h:panelGrid>
											</f:facet>
											<f:facet name="bottom"></f:facet>
											<f:facet name="top">
											</f:facet>

										</hx:panelLayout>
									</f:facet>
									
								</hx:panelLayout></td><td></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>
								
								</td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
					
					<br>
					<br>
				</h:form>
			</hx:scriptCollector>
		<%-- /tpl:put --%> </div>
		<!-- end main content area -->
	</body>
</html>
</f:view>
<%-- /tpl:insert --%>

