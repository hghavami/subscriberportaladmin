<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/groups/Manage_group.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Manage Group Settings</TITLE>
<script language="JavaScript">
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
       }
   }
}
</script>
				
			<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
									<hx:scriptCollector id="scriptCollectorManageGroups" preRender="#{pc_Manage_group.onPageLoadBegin}">
										<h:form styleClass="form" id="formManageGroups">

											<hx:jspPanel id="jspPanelMessagesPanelSave"
												rendered="#{campaignGroupSettingsHandler.changed}">
												<table class="messagePortlet" border="0" cellpadding="0"
													cellspacing="0" valign="top" width="75%"
													summary="Inline Messages">
													<tr valign="top">
														<td class="messageTitle"><a
															href="javascript:showHideSection('com_ibm_ws_inlineMessages')"
															class="expand-task"><img
															id="com_ibm_ws_inlineMessagesImg"
															src="/portaladmin/images/arrow_expanded.gif"
															alt="show/hide" border="0" align="middle"> Messages</a></td>
													</tr>
													<tbody id="com_ibm_ws_inlineMessages">
														<tr>
															<td class="complex-property"><span
																class="validation-warn-info"> <img alt="Warning"
																align="baseline" height="16" width="16"
																src="/portaladmin/images/Warning.gif">Changes have
															been made. Click <hx:requestLink styleClass="requestLink"
																id="linkSaveGroupChanges" action="#{pc_Manage_group.doLinkSaveGroupChangesAction}">
																<h:outputText id="textSaveLinkLabel" styleClass="outputText"
																	value="Save"></h:outputText>
															</hx:requestLink> to apply changes to the database. <br>
															<br>
															Please Note: Setting a campaign's group name to an existing campaign group will not automatically copy all promotion settings. You must edit the details of the group using the campaign you want to use as the basis and then Save and Publish those campaigns.</span><br>
															<br>
															</td>
														</tr>
													</tbody>
												</table>
											</hx:jspPanel><h:messages styleClass="messages" id="messagesErrrorMessages"
												layout="table"></h:messages>
											<hx:panelFormBox helpPosition="over" labelPosition="left"
												styleClass="panelFormBox" id="formBoxGroupSettingsFormBox"
												widthLabel="110" widthContent="490">


												<hx:formItem styleClass="formItem" id="formItemNewGroupName"
													label="Group Name:"
													infoText="Enter a group name to apply to all selected campaigns. Campaigns with different publication codes cannot be grouped together.">
													<h:inputText styleClass="inputText" id="textNewGroupName"
														value="#{campaignGroupSettingsHandler.groupName}"
														size="30">
														<hx:inputHelperTypeahead styleClass="inputText_Typeahead"
															id="typeaheadGroupName" value="#{groupNameSuggestions}"
															size="10"></hx:inputHelperTypeahead>
													</h:inputText>
												</hx:formItem>

												<f:facet name="bottom">
													<hx:panelBox styleClass="panelBox" id="boxFooterBox"
														width="100%" align="center">
														<h:panelGrid styleClass="panelGrid" id="gridButtonGrid"
															columns="2" cellpadding="2" cellspacing="2" style="margin-left: 75px">
															<hx:commandExButton type="submit"
																value="Apply Group Name" styleClass="commandExButton"
																id="buttonApplyGroupName"
																confirm="All selected campaigns will have their group name set to the specified group name."
																action="#{pc_Manage_group.doButtonApplyGroupNameAction}"></hx:commandExButton>
															<hx:commandExButton type="submit" value="Remove Grouping"
																styleClass="commandExButton" id="buttonClearGroupNames"
																confirm="All selected campaigns will have their group associations removed."
																action="#{pc_Manage_group.doButtonClearGroupNamesAction}"></hx:commandExButton>
														</h:panelGrid>
													</hx:panelBox>
												</f:facet>
												<f:facet name="top">
												<hx:outputLinkEx
														value="/portaladmin/secured/campaigns/advancedsearch.faces"
														styleClass="outputLinkEx" id="linkExBackToSearchPage">
												<h:outputText id="text9" styleClass="outputText"
													value="&lt;&lt;...Campaign Search/Cancel Unsaved Changes"
													escape="false"></h:outputText>
												<hx:graphicImageEx styleClass="graphicImageEx"
													id="imageExSearchIcon"
													value="/portaladmin/images/icon_detailsmag14x14.gif"
													width="14" height="14" hspace="5" border="0" vspace="5"
													title="Advanced Search" alt="Advanced Search"
													align="middle"></hx:graphicImageEx>
											</hx:outputLinkEx>
												</f:facet>
											</hx:panelFormBox>
											<hx:dataTableEx border="0" cellpadding="2" cellspacing="1"
												columnClasses="columnClass1" headerClass="headerClass"
												footerClass="headerClass" rowClasses="rowClass1, rowClass3"
												styleClass="dataTableEx" id="tableExCampaignsToEditTable"
												value="#{edittingCampaignsHandler.campaigns}"
												var="varcampaigns">
												<f:facet name="footer">
													<hx:panelBox styleClass="panelBox"
														id="boxSelectionBoxesBottom">
														<hx:outputSelecticons styleClass="outputSelecticons"
															id="selecticonsBoxBottom"></hx:outputSelecticons>
													</hx:panelBox>
												</f:facet>
												<hx:columnEx id="columnEx5">
													<hx:inputRowSelect styleClass="inputRowSelect"
														id="rowSelectCampaign"
														value="#{varcampaigns.campaignSelected}"></hx:inputRowSelect>
													<f:facet name="header"></f:facet>
												</hx:columnEx>
												<f:facet name="header">
													<hx:panelBox styleClass="panelBox" id="box1">
														<hx:outputSelecticons styleClass="outputSelecticons"
															id="selecticonsBoxTop"></hx:outputSelecticons>
														<h:outputText styleClass="outputText"
															id="textDataTableHeaderText"
															value="#{edittingCampaignsHandler.dataTableHeaderString}"
															style="margin-left: 10px"></h:outputText>
													</hx:panelBox>
												</f:facet>
												<hx:columnEx id="columnExCampName">
													<h:panelGrid styleClass="panelGrid" id="gridNameGrid"
														columns="2" cellpadding="1">
														<h:outputText styleClass="outputText" id="textCampName"
															value="#{varcampaigns.campaignName}"></h:outputText>
														<h:outputText styleClass="outputText"
															id="textChangedIndicator" value="*"
															style="color: red; font-weight: bold; font-size: 12pt"
															rendered="#{varcampaigns.campaign.isChanged}"></h:outputText>
													</h:panelGrid>
													<f:facet name="header">
														<h:outputText id="textCampNameHeaderLabel"
															styleClass="outputText" value="Campaign Name"></h:outputText>
													</f:facet>

												</hx:columnEx>
												<hx:columnEx id="columnExGroupNameCol">
													<f:facet name="header">
														<h:outputText value="Group Name" styleClass="outputText"
															id="text1"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textGroupName"
														value="#{varcampaigns.campaignGroup}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnExPubCol" align="center">
													<f:facet name="header">
														<h:outputText value="Publication" styleClass="outputText"
															id="text2"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textPubCode"
														value="#{varcampaigns.pubcode}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx3">
													<f:facet name="header">
														<h:outputText value="Keycode" styleClass="outputText"
															id="text3"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textKeycode"
														value="#{varcampaigns.keycode}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx4">
													<f:facet name="header">
														<h:outputText value="Vanity" styleClass="outputText"
															id="text4"></h:outputText>
													</f:facet>
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExVanityLink"
														value="#{varcampaigns.fullVanityURL}" target="_blank">
														<h:outputText id="textProdVanityLinkText"
															styleClass="outputText"
															value="#{varcampaigns.fullVanityURLText}"></h:outputText>
													</hx:outputLinkEx>
												</hx:columnEx>
											</hx:dataTableEx>
											<h:outputText styleClass="outputText"
												id="textChangedIndicatorReadOnly" value="&nbsp; *"
												style="color: red; font-weight: bold; font-size: 12pt" escape="false"></h:outputText>
											<h:outputText styleClass="outputText" id="textInfoLabel" value="Denotes changed campaign" style="font-weight: bold"></h:outputText>
											
											<br>
											<br>
										
										
										
										</h:form>
										<br>
										<br>
										<br>
										<br></hx:scriptCollector><%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>