<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/Advancedsearch.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Advanced Search</TITLE>
				
				<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%><hx:scriptCollector id="scriptCollector1">
									<h:form styleClass="form" id="formAdvancedSearch"><h:messages styleClass="messages" id="messages1" style="font-weight: bold; font-size: 12pt"></h:messages>
										<table border="0" cellpadding="1" cellspacing="1">
											<tbody>
												<tr>
													<th align="left" nowrap colspan="4" class="headerClass" width="600">
														<a href="javascript:showHideSection('com_usat_seachFilterCriteria')" class="expand-task">
															<img id="com_usat_seachFilterCriteriaImg"
															src="${pageContext.request.contextPath}/images/arrow_expanded.gif" alt="show/hide"
															border="0"/>Search Filter Criteria:</a></th>
												</tr>
												<tr>
													<td nowrap colspan="4">
														<table border="0" cellpadding="1" cellspacing="1">
															<tbody id="com_usat_seachFilterCriteria">
																<tr>
																	<td align="center" colspan="4">
																	
																	<h:outputText styleClass="outputText"
																		id="textSearchHelpStr"
																		value="Enter as many or as few critera as you'd like. Enter a '%' (percent) sign for a wildcard."
																		style="color: black; font-weight: bold"></h:outputText></td>
																</tr>
																<tr>
																	<td align="right" nowrap><h:outputText
																		styleClass="outputText" id="textCriteriaLabelCampaignName"
																		value="Campaign Name Like:"></h:outputText></td>
																	<td nowrap align="left"><h:inputText
																	styleClass="inputText" id="textCampaignNameLike" value="#{advancedSearchCriteriaHandler.campaignNameLike}"></h:inputText></td>
																	<td align="right"><h:outputText styleClass="outputText"
																		id="textCriteriaLabelCampaignGroup"
																		value="Campaign Group Name Like:" style="margin-left: 4px"></h:outputText></td>
																	<td align="left"><h:inputText styleClass="inputText"
																	id="textCampaignGroupLike" value="#{advancedSearchCriteriaHandler.campaignGroupLike}"></h:inputText></td>
																</tr>
																<tr>
																	<td align="right" nowrap><h:outputText
																		styleClass="outputText" id="textCriteriaLabelKeycode"
																		value="Keycode Like:"></h:outputText></td>
																	<td nowrap align="left"><h:inputText
																	styleClass="inputText" id="textCampaignKeyCodeLike" value="#{advancedSearchCriteriaHandler.campaignKeyCodeLike}"></h:inputText></td>
																	<td align="right"><h:outputText styleClass="outputText"
																		id="textCriteriaLabelVanityURL" value="Vanity URL Like:"></h:outputText></td>
																	<td align="left"><h:inputText styleClass="inputText"
																	id="textCampaignVanityLike" value="#{advancedSearchCriteriaHandler.campaignVanityLike}"></h:inputText></td>
																</tr>
																<tr>
																	<td align="right" nowrap></td>
																	<td nowrap align="left"></td>
																	<td align="right"></td>
																	<td align="left"></td>
																</tr>
																<tr>
																	<td align="right" nowrap></td>
																	<td nowrap align="left"></td>
																	<td align="right"></td>
																	<td align="left"></td>
																</tr>
																<tr>
																	<td align="right" nowrap><h:outputText
																		styleClass="outputText" id="textCriteriaLabelCreatedDate"
																		value="Campaign created between:"></h:outputText></td>
																	<td nowrap align="left" colspan="2"><h:inputText styleClass="inputText"
																		id="textInsertTimeBegin" size="15" value="#{advancedSearchCriteriaHandler.insertedAfter}">
																	<hx:convertDateTime />
																	<hx:inputHelperDatePicker />
																	</h:inputText> And <h:inputText styleClass="inputText"
																		id="textInsertTimeEnd" size="15" value="#{advancedSearchCriteriaHandler.insertedBefore}">
																	<hx:convertDateTime />
																	<hx:inputHelperDatePicker />
																	</h:inputText></td><td align="left"></td>
																</tr>
																<tr>
																	<td align="right" nowrap></td>
																	<td nowrap align="left"></td>
																	<td align="right"></td>
																	<td align="left"></td>
																</tr>
																<tr>
																	<td align="right" nowrap><h:outputText
																		styleClass="outputText" id="textCriteriaLabelPublications"
																		value="Publication:"></h:outputText></td>
																	<td nowrap align="left"> <h:selectOneRadio
																	disabledClass="selectOneRadio_Disabled"
																	styleClass="selectOneRadio" id="radioPubSelection"
																	value="#{advancedSearchCriteriaHandler.publication}">
																	<f:selectItem itemValue="ALL" itemLabel="ALL" />
																		<f:selectItems
																			value="#{productHandler.availableProductsSelectItems}"
																			id="selectItems1" />
																	</h:selectOneRadio></td>
																	<td align="right"></td>
																	<td align="left"></td>
																</tr>
																<tr>
																	<td align="right" nowrap><h:outputText
																		styleClass="outputText"
																		id="textCriteriaLabelPublishingStatus"
																		value="Publishing Status:"></h:outputText></td>
																	<td nowrap align="left" colspan="2"><h:selectOneRadio
																	disabledClass="selectOneRadio_Disabled"
																	styleClass="selectOneRadio" id="radioCampaignPubStatus"
																	value="#{advancedSearchCriteriaHandler.publishingStatus}"
																	layout="lineDirection">
																	<f:selectItem itemValue="ALL" itemLabel="ALL" />
																		<f:selectItem itemLabel="New" itemValue="0" />
																		<f:selectItem itemValue="1" itemLabel="In Test" />
																		<f:selectItem itemValue="2" itemLabel="In Prod" />
																		<f:selectItem itemLabel="Republish" itemValue="3" />
																	</h:selectOneRadio></td>
																	<td align="right"><hx:commandExButton type="submit" value="Search"
																		styleClass="commandExButton" id="buttonSubmitSearch"
																		action="#{pc_Advancedsearch.doButtonSubmitSearchAction}"></hx:commandExButton></td>
																</tr>
																<tr>
																	<td align="right" nowrap></td>
																	<td nowrap align="left"></td>
																	<td align="right"></td>
																	<td></td>
																</tr>
														 </tbody>
														</table>													
													</td>
												</tr>
												<tr>
													<td align="right" nowrap colspan="4"><hx:outputSeparator
														styleClass="outputSeparator" id="separatorHR" width="100%"></hx:outputSeparator></td>
												</tr>
											</tbody>
										</table>

										<h:outputText styleClass="outputText_Med"
											id="textSeachResultsLabelText"
											value="#{currentSearchResults.lastSeachResultsString}" style="font-weight: bold"></h:outputText>

											<hx:dataTableEx border="0" cellpadding="2" cellspacing="1"
												columnClasses="columnClass1" headerClass="headerClass"
												footerClass="headerClass" rowClasses="rowClass1, rowClass3"
												styleClass="dataTableEx" id="tableExSearchResultsDataTable"
												value="#{currentSearchResults.lastSearchResults}"
												var="varlastSearchResults"
												rendered="#{currentSearchResults.hasSearchResults}"
												style="margin-top: 10px" rows="50">
												<f:facet name="header">
													<hx:panelBox styleClass="panelBox" id="boxHeaderBox">


														<hx:outputSelecticons styleClass="outputSelecticons"
															id="selecticons1"></hx:outputSelecticons>
														<hx:commandExButton type="submit" value="Edit Details"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonEditSelected"
															action="#{pc_Advancedsearch.doButtonEditSelectedAction}"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>
														<hx:commandExButton type="submit"
															value="Manage Group Settings"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonManageGroupSettings"
															action="#{pc_Advancedsearch.doButtonManageGroupSettingsAction}"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>
														<hx:commandExButton type="submit" value="Publish"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonPublishSelectedCampaigns"
															action="#{pc_Advancedsearch.doButtonPublishSelectedCampaignsAction}"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>


														<hx:commandExButton type="submit" value="Delete"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonDeleteSelected1"
															action="#{pc_Advancedsearch.doButtonDeleteSelectedCampaignsAction}"
															confirm="Are you sure you want to Delete selected items?"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>
														<h:panelGrid styleClass="panelGrid" id="gridNumRowsToShow"
															cellpadding="0" columns="3" cellspacing="2">
															<h:outputText styleClass="outputText"
																id="textNumRowsLabel" value="Rows Per Page:"
																style="margin-left: 50px"></h:outputText>
															<h:inputText styleClass="inputText"
																id="textRowsPerPageValue"
																value="#{currentSearchResults.numRecordsPerPage}"
																size="3">
																<f:validateLongRange minimum="1"></f:validateLongRange>
																<hx:convertNumber integerOnly="true"
																	maxFractionDigits="0" pattern="0" />
															</h:inputText>
															<hx:commandExButton type="submit" value="Refresh"
																styleClass="commandExButton" id="buttonUpdateRowToShow"
																style="padding: 0px; font-size: 9pt"
																action="#{pc_Advancedsearch.doButtonUpdateRowToShowAction}"></hx:commandExButton>
														</h:panelGrid>
													</hx:panelBox>
												</f:facet>
												<f:facet name="footer">
													<hx:panelBox styleClass="panelBox" id="boxFooterBox">

														<hx:outputSelecticons styleClass="outputSelecticons"
															id="selecticonsRowSelectionFooter"></hx:outputSelecticons>

														<hx:commandExButton type="submit" value="Edit Details"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonEditSelectedBottom"
															action="#{pc_Advancedsearch.doButtonEditSelectedAction}"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>
														<hx:commandExButton type="submit"
															value="Manage Group Settings"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonManageGroupSettingsBottom"
															action="#{pc_Advancedsearch.doButtonManageGroupSettingsAction}"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>
														<hx:commandExButton type="submit" value="Publish"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonPublishCampaignsBottom"
															action="#{pc_Advancedsearch.doButtonPublishSelectedCampaignsAction}"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>
														<hx:commandExButton type="submit" value="Delete"
															styleClass="commandExButton #{user.disabledButtonStyle}"
															id="buttonDeleteSelected"
															action="#{pc_Advancedsearch.doButtonDeleteSelectedCampaignsAction}"
															confirm="Are you sure you want to Delete selected items?"
															disabled="#{user.islimitedAccessOnly}"></hx:commandExButton>
														<h:panelGrid styleClass="panelGrid"
															id="gridBottomPagerGrid" width="260" columns="1"
															style="text-align: right">
															<hx:pagerWeb styleClass="pagerWeb" id="webBottomPager" />
														</h:panelGrid>


													</hx:panelBox>
												</f:facet>
												<hx:columnEx id="columnEx3">
													<hx:inputRowSelect styleClass="inputRowSelect"
														id="rowSelectSearchResults"
														value="#{varlastSearchResults.campaignSelected}"></hx:inputRowSelect>
													<f:facet name="header"></f:facet>
												</hx:columnEx>
												<hx:columnEx id="columnEx1" nowrap="false">
													<f:facet name="header">

														<hx:sortHeader styleClass="sortHeader"
															defaultSortOrder="sortbi" id="sortHeaderCName"
															action="#{pc_Advancedsearch.doSortHeaderCNameAction}">
															<h:outputText id="text5" styleClass="outputText"
																value="Campaign Name"></h:outputText>
														</hx:sortHeader>
													</f:facet>
													<h:outputText styleClass="outputText" id="textCampaignName"
														value="#{varlastSearchResults.campaignName}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx2" align="center">
													<f:facet name="header">
														<h:outputText value="Pub" styleClass="outputText"
															id="text6"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textCampaignPub"
														value="#{varlastSearchResults.pubcode}" style="font-weight: bold"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx6" align="center" nowrap="true">
													<f:facet name="header">

														<hx:sortHeader styleClass="sortHeader"
															id="sortHeaderByKeycode"
															action="#{pc_Advancedsearch.doSortHeaderByKeycodeAction}" defaultSortOrder="sortbi">
															<h:outputText value="Keycode" styleClass="outputText"
																id="text1"></h:outputText>
														</hx:sortHeader>
													</f:facet>
													<h:outputText styleClass="outputText"
														id="textCampaignKeycode"
														value="#{varlastSearchResults.keycode}" style="font-weight: bold"></h:outputText>
													
												</hx:columnEx>
												<hx:columnEx id="columnExKeycodeCol" align="center">
													<h:outputText styleClass="outputText"
														id="textCampaignBrandingPub"
														value="#{varlastSearchResults.campaign.product.brandingPubCode}"></h:outputText>
													<f:facet name="header">
														
															<h:outputText value="Branding Pub"
																styleClass="outputText" id="textKeycodeHeaderLabel"></h:outputText>
														
													</f:facet>
													
												</hx:columnEx>
												<hx:columnEx id="columnEx5">
													<f:facet name="header">
														<h:outputText value="Vanity" styleClass="outputText"
															id="text9"></h:outputText>
													</f:facet>
													<hx:outputLinkEx styleClass="outputLinkEx"
														id="linkExVanityLink"
														value="#{varlastSearchResults.fullVanityURL}"
														target="_blank">
														<h:outputText id="textProdVanityLinkText"
															styleClass="outputText"
															value="#{varlastSearchResults.fullVanityURLText}"></h:outputText>
													</hx:outputLinkEx>
												</hx:columnEx>
												<hx:columnEx id="columnEx10">
													<f:facet name="header">
														<h:outputText value="Landing Page" styleClass="outputText"
															id="text4"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text12"
														value="#{varlastSearchResults.campaignLandingPageDescription}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx14" align="center">
													<f:facet name="header">
														<h:outputText value="EZ-PAY Override"
															styleClass="outputText" id="text16"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text19"
														value="#{varlastSearchResults.promotionSetHandler.forceEZPAYOverridePromoStatusString}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx4" nowrap="false">
													<f:facet name="header">

														<hx:sortHeader styleClass="sortHeader"
															defaultSortOrder="sortbi" id="sortHeaderByCampGroup"
															action="#{pc_Advancedsearch.doSortHeaderByCampGroupAction}">
															<h:outputText value="Campaign Group"
																styleClass="outputText" id="text8"></h:outputText>
														</hx:sortHeader>
													</f:facet>
													<h:outputText styleClass="outputText"
														id="textCampaignGroup"
														value="#{varlastSearchResults.campaignGroup}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx8">
													<f:facet name="header">
														<h:outputText value="Publishing Status"
															styleClass="outputText" id="text11"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText"
														id="textCampaignStatus"
														value="#{varlastSearchResults.campaignStatusString}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx7" nowrap="true">
													<f:facet name="header">
														<h:outputText value="Created Date" styleClass="outputText"
															id="text10"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textCreatedDate"
														value="#{varlastSearchResults.createdTime}">
														<hx:convertDateTime />
													</h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx11">
													<f:facet name="header">
														<h:outputText value="Created By" styleClass="outputText"
															id="text7"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text13"
														value="#{varlastSearchResults.createdBy}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx12">
													<f:facet name="header">
														<h:outputText value="Updated Time" styleClass="outputText"
															id="text14"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text17" value="#{varlastSearchResults.updatedTime}">
														<hx:convertDateTime />
													</h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx13">
													<f:facet name="header">
														<h:outputText value="Updated By" styleClass="outputText"
															id="text15"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text18" value="#{varlastSearchResults.updatedBy}"></h:outputText>
												</hx:columnEx>
											</hx:dataTableEx>
											<br>
										<br>
										<br>
									</h:form>
									<br>
									<br>
									<br>
									<br>
								</hx:scriptCollector><%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>