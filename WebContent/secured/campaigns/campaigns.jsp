<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/Campaigns.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Custom Campaigns</TITLE>
			<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
								<div id="searchDivArea" class="topSearchRight">
									<f:subview id="subviewBasicSearch">
										<hx:scriptCollector id="scriptCollectorBasicSearchForm">
										
										<h:form styleClass="form" id="formBasicSearchForm">
										<TABLE border="0" cellpadding="0" cellspacing="1">
											<TBODY>
												<TR>
													<TD nowrap><h:outputText styleClass="outputText_Small"
													id="textBasicSearchLabel" value="Search Campaigns:"></h:outputText></TD>
												<TD nowrap><h:inputText styleClass="inputText"
															id="textBasicSearchCriteria" size="20" required="true"
															value="#{basicSearchCriteria.queryString}">
															<f:validateLength minimum="1"></f:validateLength>
														</h:inputText>&nbsp;<hx:commandExButton type="submit"
													styleClass="commandExButton" id="buttonBasicSearch"
													value="Search" action="#{pc_Campaigns.doButtonBasicSearchAction}"></hx:commandExButton><hx:outputLinkEx
													styleClass="outputLinkEx"
													value="/portaladmin/secured/campaigns/advancedsearch.faces"
													id="linkExAdvSearchLink">
													<h:outputText id="textAdvSearchLabel"
														styleClass="outputText_Small" value="Advanced Search"></h:outputText>
												</hx:outputLinkEx></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><h:message styleClass="message" id="message1"
													for="textBasicSearchCriteria"></h:message></TD>
													<TD></TD>
												</TR>
												<TR>
													<TD></TD>
													<TD></TD>
													<TD></TD>
												</TR>
											</TBODY>
										</TABLE>
												<hx:behaviorKeyPress key="Enter" id="behaviorKeyPress1" behaviorAction="click" targetAction="buttonBasicSearch"></hx:behaviorKeyPress>
												<hx:inputHelperSetFocus id="setFocus1" target="textBasicSearchCriteria"></hx:inputHelperSetFocus>
											</h:form>
										</hx:scriptCollector>
									</f:subview>								
								</div>
								<hx:scriptCollector id="scriptCollectorCampaignInfo">
								<h:form styleClass="form" id="formCampaignInfo">
									<table border="0" cellpadding="0" cellspacing="1" width="570">
										<tbody>
											<tr>
												<td></td>
												<td></td>
												<td></td>
											</tr>
												<tr>
													<td colspan="3"><h:messages styleClass="messages"
														id="messagesCampDetailsErrors" globalOnly="true"></h:messages></td>
												</tr>
												<tr>
													<td valign="middle" nowrap><hx:outputLinkEx
															styleClass="outputLinkEx" id="linkExNewCampaignWizLink"
															value="/portaladmin/secured/campaigns/wizard/step_1.faces">
															<hx:graphicImageEx styleClass="graphicImageEx"
																id="imageEx1"
																value="/portaladmin/images/icon_new_campaign_34x35.gif"
																vspace="0" hspace="2" width="34" height="35"
																align="absbottom" border="0"></hx:graphicImageEx>
															<h:outputText id="textNewCampaignLinkLabel"
																styleClass="outputText" value="Create New Campaign"
																style="height: 35px; vertical-align: top; font-weight: bold; font-size: 12pt; cursor: pointer"></h:outputText>
														</hx:outputLinkEx></td>
													<td width="20">&nbsp;</td>
													<td></td>
												</tr>											
												<tr>
													<td valign="top" width="250"><hx:panelSection
															styleClass="panelSection"
															id="sectionCampaignCountsSection" initClosed="false"
															style="width: 250px; margin-bottom: 10px">
															<hx:jspPanel id="jspPanelCampaignCountsJSPPanel">

																<hx:dataTableEx border="0" cellpadding="2"
																	cellspacing="0" columnClasses="columnClass1"
																	headerClass="headerClass" footerClass="footerClass"
																	rowClasses="rowClass1, rowClass2"
																	styleClass="dataTableEx"
																	id="tableExCampaignCountsDGrid"
																	value="#{campaignStatisticsHandler.campaignStatsCollection}"
																	var="varcampaignStatsCollection" width="100%"
																	style="border-bottom-width: 2px; border-bottom-style: solid">
																	<f:facet name="header">
																		<hx:panelBox styleClass="panelBox" id="box1">
																			<hx:commandExButton type="submit" value="Refresh"
																				styleClass="commandExButton"
																				id="buttonRefreshCampaignStats" action="#{pc_Campaigns.doButtonRefreshCampaignStatsAction}"></hx:commandExButton>
																		</hx:panelBox>
																	</f:facet>
																	<hx:columnEx id="columnExCampCountDGridCol1"
																		nowrap="true">
																		<f:facet name="header">
																			<h:outputText id="columnExCampCountDGridCol1Header"
																				styleClass="outputText" value="Product"></h:outputText>
																		</f:facet>
																		<h:outputText styleClass="outputText" id="text8"
																			value="#{varcampaignStatsCollection.product.name}"></h:outputText>
																	</hx:columnEx>
																	<hx:columnEx id="columnEx1" align="right">
																		<h:outputText styleClass="outputText" id="text1"
																			value="#{varcampaignStatsCollection.numCampaigns}"
																			></h:outputText>
																		<f:facet name="header">
																			<h:outputText value="# of Campaigns"
																				styleClass="outputText" id="text9"></h:outputText>
																		</f:facet>
																	</hx:columnEx>
																</hx:dataTableEx>

																<table border="0" cellpadding="0" cellspacing="1"
																	bgcolor="white" width="100%">
																	<tbody>

																		<tr>
																			<td align="left" nowrap><h:outputText
																				styleClass="outputText" id="textNumCampaignsLabel"
																				value="Total Campaigns in System:" style="font-weight: bold"></h:outputText></td>
																			<td width="10"></td>
																			<td align="right"><h:inputText styleClass="inputText"
																							id="textNumCampaigns"
																							value="#{campaignStatisticsHandler.totalCampaigns}"
																							size="4" readonly="true" disabled="false"
																							style="color: black; font-weight: bold; text-align: right"></h:inputText>
																					</td>
																		</tr>
																		<tr>
																			<td align="left" nowrap></td>
																			<td width="10"></td>
																			<td align="left"></td>
																		</tr>
																		<tr>
																			<td align="left" nowrap></td>
																			<td width="10"></td>
																			<td align="left"></td>
																		</tr>
																	</tbody>
																</table>
															</hx:jspPanel>
															<f:facet name="closed">
																<hx:jspPanel id="jspPanel2">
																	<hx:graphicImageEx id="imageExCampaignsSectionCol"
																		styleClass="graphicImageEx" align="middle"
																		value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																	<h:outputText id="text2" styleClass="outputText"
																		value="Campaigns:"></h:outputText>
																</hx:jspPanel>
															</f:facet>
															<f:facet name="opened">
																<hx:jspPanel id="jspPanel1">
																	<hx:graphicImageEx id="imageExCampaignsSectionExpanded"
																		styleClass="graphicImageEx" align="middle"
																		value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																	<h:outputText id="text1" styleClass="outputText"
																		value="Campaigns:"></h:outputText>
																</hx:jspPanel>
															</f:facet>
														</hx:panelSection>
													<hx:panelSection styleClass="panelSection"
														id="sectionExpiredCampaignsSection" style="width: 100%" initClosed="false" rendered="false">
														<hx:jspPanel id="jspPanelExpiredCampsPanel">
															<table border="0" cellpadding="0" cellspacing="1"
																bgcolor="white" width="100%">
																<tbody>
																	<tr>
																		<td align="left" nowrap><h:outputText
																			styleClass="outputText" id="textNumExpCampaignsLabel"
																			value="Total Expired Campaigns:"></h:outputText></td>
																		<td width="10"></td>
																		<td align="left"><h:inputText styleClass="inputText"
																			id="textNumExpCampaigns"
																			value="#{campaignStatisticsHandler.totalExpiredCampaigns}"
																			size="4"></h:inputText></td>
																	</tr>
																</tbody>
															</table>
														</hx:jspPanel>
														<f:facet name="closed">
															<hx:jspPanel id="jspPanel6">
																<hx:graphicImageEx id="imageExExpCampsCol"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																<h:outputText id="text7" styleClass="outputText"
																	value="Expired Campaigns:"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<f:facet name="opened">
															<hx:jspPanel id="jspPanel5">
																<hx:graphicImageEx id="imageExExpCampsExp"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																<h:outputText id="text6" styleClass="outputText"
																	value="Expired Campaigns:"></h:outputText>
															</hx:jspPanel>
														</f:facet>
													</hx:panelSection><br>
													<br>
													<br>
													
													
													
													</td>
													<td></td>
													<td valign="top"><hx:panelSection styleClass="panelSection"
														id="sectionDefaultKeycodesSection" initClosed="false" style="margin-left: 20px">
														<f:facet name="closed">
															<hx:jspPanel id="jspPanel4">
																<hx:graphicImageEx id="imageExDefKeycodesCol"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																<h:outputText id="text5" styleClass="outputText"
																	value="Default Keycodes:"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<f:facet name="opened">
															<hx:jspPanel id="jspPanel3">
																<hx:graphicImageEx id="imageExDefKeycodeExp"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																<h:outputText id="text4" styleClass="outputText"
																	value="Default Keycodes:"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<hx:jspPanel id="jspPanelDefaultKeycodesPanel">

																		<hx:dataTableEx border="0" cellpadding="2"
																			cellspacing="0" columnClasses="columnClass1"
																			headerClass="headerClass" footerClass="footerClass"
																			rowClasses="rowClass1, rowClass2"
																			styleClass="dataTableEx" id="productData" value="#{productHandler.products}" var="currentProd">
																			<hx:columnEx id="columnExProductName" align="left" nowrap="true">
																				<f:facet name="header">
																					<h:outputText id="text3" styleClass="outputText" value="Product"></h:outputText>
																				</f:facet>
																				<h:outputText styleClass="outputText"
																					id="textProdNameDT" value="#{currentProd.name} :"></h:outputText>
																			</hx:columnEx>
																			<hx:columnEx id="columnEx2" align="center" nowrap="true">
																				<f:facet name="header">
																				<h:outputText id="text444" styleClass="outputText" value="Default Offer Key Code"></h:outputText>
																				</f:facet>
																				<h:outputText styleClass="outputText" id="text10"
																					value="#{currentProd.defaultKeycode}"></h:outputText>
																			</hx:columnEx>
																			<hx:columnEx id="columnEx3" align="center" nowrap="true">
																				<f:facet name="header">
																					<h:outputText value="Expired Offer Key Code"
																						styleClass="outputText" id="text11"></h:outputText>
																				</f:facet>
																				<h:outputText styleClass="outputText" id="text12"
																					value="#{currentProd.expiredOfferKeycode}"></h:outputText>
																			</hx:columnEx>
																		</hx:dataTableEx>
																	</hx:jspPanel>
													</hx:panelSection></td>
												</tr>
												<tr>
												<td></td>
												<td width="20">&nbsp;</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</h:form>
								</hx:scriptCollector>
								<BR><%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>