<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/Confirm_selection.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Confirm Selections</TITLE>
			<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
								<hx:scriptCollector id="scriptCollector1"><h:form styleClass="form" id="formConfirmSelection">
										<h:panelGrid styleClass="panelGrid" id="gridMainLayoutGrid"
											cellpadding="1" cellspacing="1" columns="1" width="650">
											<hx:outputLinkEx styleClass="outputLinkEx"
												id="linkEx1BackToSearch"
												value="/portaladmin/secured/campaigns/advancedsearch.faces">
												<h:outputText id="textLinkDescription"
													styleClass="outputText"
													value="&lt;&lt;...Back to Campaign Search" escape="false"></h:outputText>
											</hx:outputLinkEx>
											<h:messages styleClass="messages" id="messages1"
												layout="table" style="font-weight: bold; font-size: 12pt"></h:messages>
												<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
													columnClasses="columnClass1" headerClass="headerClass"
													footerClass="headerClass" rowClasses="rowClass1, rowClass2"
													styleClass="dataTableEx"
													id="tableExSelectedCampaignsToEdit"
													value="#{edittingCampaignsHandler.campaigns}"
													var="varcampaigns">
													<f:facet name="footer">
														<hx:panelBox styleClass="panelBox" id="box1">
															<hx:commandExButton type="submit"
																value="Proceed to campaign details"
																styleClass="commandExButton" id="buttonConfirmSelection"
																confirm="All campaigns listed here will be editted. With the selected campaign used as the initial basis."
																action="#{pc_Confirm_selection.doButtonConfirmSelectionAction}"></hx:commandExButton>
														</hx:panelBox>
													</f:facet>
													<hx:columnEx id="columnEx5">
														<hx:inputRowSelect styleClass="inputRowSelect"
															id="rowSelect1" value="#{varcampaigns.campaignSelected}"
															selectOne="true"></hx:inputRowSelect>
														<f:facet name="header"></f:facet>
													</hx:columnEx>
													<hx:columnEx id="columnExGroup">

														<f:facet name="header">
															<h:outputText id="textGroupNameLabel"
																styleClass="outputText" value="Group Name"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText" id="textGroupName"
															value="#{varcampaigns.campaignGroup}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx1">
														<f:facet name="header">
															<h:outputText value="Campaign Name"
																styleClass="outputText" id="text1"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText" id="textCampName"
															value="#{varcampaigns.campaignName}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx2">
														<f:facet name="header">
															<h:outputText value="Publication" styleClass="outputText"
																id="text2"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText" id="textPub"
															value="#{varcampaigns.pubcode}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx3">
														<f:facet name="header">
															<h:outputText value="Keycode" styleClass="outputText"
																id="text3"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText" id="textKeycode"
															value="#{varcampaigns.keycode}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx6">
														<f:facet name="header">
															<h:outputText value="Vanity" styleClass="outputText"
																id="text5"></h:outputText>
														</f:facet>
														<hx:outputLinkEx styleClass="outputLinkEx"
															id="linkExVanLink" value="#{varcampaigns.fullVanityURL}"
															target="_blank">
															<h:outputText id="textVanLabel" styleClass="outputText"
																value="#{varcampaigns.fullVanityURLText}"></h:outputText>
														</hx:outputLinkEx>
													</hx:columnEx>
													<hx:columnEx id="columnEx4">
														<f:facet name="header">
															<h:outputText value="Publishing Status"
																styleClass="outputText" id="text4"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText" id="textPubStatus"
															value="#{varcampaigns.campaignStatusString}"></h:outputText>
													</hx:columnEx>
												</hx:dataTableEx>

											</h:panelGrid>
											<br>
										</h:form>
									<br>
									<br>
									<br>
									<br>
									<br></hx:scriptCollector>
							<%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>