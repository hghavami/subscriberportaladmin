<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/campaigns/Publish_campaigns.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Publish Campaigns</TITLE>
			<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
if (thisEvent.type == "onload") {
    // called due to 'pageload' event, put any ‘initial’ processing here
    return true;
}
}

var formSubmitted = false;
var publishClicked = true;

function func_ShowStatusBar(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	var divArea = $('stausBarDivArea');
	divArea.show();
	
	pb=hX.getComponentById('formPublishingForm:barPubStatusBar');
	pb.visible();
	pb.start('Please wait while publishing is being executed on the server...');
	return true;
}

function advancePB() { 
	pb=hX.getComponentById('formPublishingForm:barPubStatusBar'); 
	pb.upDatePercentage(pb.percentageDone + 5); 
	pb.redraw(); 
} 
function resetPB() { 
	pb=hX.getComponentById('formPublishingForm:barPubStatusBar'); 
	pb.upDatePercentage(0);
	pb.redraw(); 
} 

function stopProgressBar() {
	if (!formSubmitted) {
		var divArea = $('stausBarDivArea');
		
		pb=hX.getComponentById('formPublishingForm:barPubStatusBar');
		pb.stop('Request Cancelled.');
		pb.reset();
		pb.hide();
		divArea.hide();
	}
}

function func_OnSubmit(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
formSubmitted = true;
    if (publishClicked) {
		func_ShowStatusBar(thisObj, thisEvent)
	}
	return true;
}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
publishClicked = false;
return true;
}</script><%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
								<hx:outputLinkEx
											value="/portaladmin/secured/campaigns/advancedsearch.faces"
											styleClass="outputLinkEx" id="linkExBackToSearchPage">
									<h:outputText id="text9" styleClass="outputText"
										value="&lt;&lt;...Campaign Search" escape="false"></h:outputText>
										<hx:graphicImageEx styleClass="graphicImageEx"
											id="imageExSearchIcon"
											value="/portaladmin/images/icon_detailsmag14x14.gif"
											width="14" height="14" hspace="5" border="0" vspace="5"
											title="Search For Campaigns To Publish" alt="Advanced Search"
											align="middle"></hx:graphicImageEx>
									</hx:outputLinkEx>
									<hx:scriptCollector id="scriptCollectorPublishingCollector">
										<h:form styleClass="form" id="formPublishingForm"
											onsubmit="return func_OnSubmit(this, event);">
											<br>
											<h:messages styleClass="messages" id="messages1"
												style="font-weight: bold; font-size: 12pt"></h:messages>
											<div id="stausBarDivArea" style="display: none"><hx:progressBar
												auto="true" timeInterval="500" proportion="5"
												styleClass="progressBar" id="barPubStatusBar"
												initHidden="true"
												message="Please Wait. Publishing Campaigns..."
												outward="true" style="width: 600px"></hx:progressBar></div>
											<hx:panelSection styleClass="panelSection"
												id="sectionSelectedCampaignsForPublishing"
												initClosed="false">
												<f:facet name="closed">
													<hx:jspPanel id="jspPanel2">
														<hx:graphicImageEx id="imageExPublishingCCollapsed"
															styleClass="graphicImageEx" align="middle"
															value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
														<h:outputText id="textCollapsedheaderText"
															styleClass="outputText"
															value="#{publishingCampaignsHandler.numberOfCampaignsInCollection} Campaigns Selected For Publishing"></h:outputText>
													</hx:jspPanel>
												</f:facet>
												<f:facet name="opened">
													<hx:jspPanel id="jspPanel1">
														<hx:graphicImageEx id="imageExPublischCOpen"
															styleClass="graphicImageEx" align="middle"
															value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
														<h:outputText id="textexpandedTextHeaderLabel"
															styleClass="outputText"
															value="#{publishingCampaignsHandler.numberOfCampaignsInCollection} Campaigns Selected For Publishing"></h:outputText>
													</hx:jspPanel>
												</f:facet>
												<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
													columnClasses="columnClass1" headerClass="headerClass"
													footerClass="headerClass" rowClasses="rowClass1, rowClass3"
													styleClass="dataTableEx" id="tableExSearchResultsDataTable"
													value="#{publishingCampaignsHandler.lastSearchResults}"
													var="varlastSearchResults"
													rendered="#{publishingCampaignsHandler.hasSearchResults}"
													style="margin-top: 10px" rows="50">
													<f:facet name="header">
														<hx:panelBox styleClass="panelBox" id="boxHeaderBox">



															<hx:outputSelecticons styleClass="outputSelecticons"
																id="selecticons1"></hx:outputSelecticons>
															<hx:commandExButton type="submit" value="Publish To Test"
																styleClass="commandExButton" id="buttonPublishToTest"
																action="#{pc_Publish_campaigns.doButtonPublishToTestAction}"
																></hx:commandExButton>
															<hx:commandExButton type="submit"
																value="Publish To Production"
																styleClass="commandExButton" id="buttonPublishToProd"
																confirm="All selected campaigns with a publishing status of In Test or Republish will be processed."
																action="#{pc_Publish_campaigns.doButtonPublishToProdAction}"></hx:commandExButton>
															<h:panelGrid styleClass="panelGrid"
																id="gridNumRowsToShow" cellpadding="0" columns="3"
																cellspacing="2">
																<h:outputText styleClass="outputText"
																	id="textNumRowsLabel" value="Rows Per Page:"
																	style="margin-left: 125px"></h:outputText>
																<h:inputText styleClass="inputText"
																	id="textRowsPerPageValue"
																	value="#{currentSearchResults.numRecordsPerPage}"
																	size="3">
																	<f:validateLongRange minimum="1"></f:validateLongRange>
																	<hx:convertNumber integerOnly="true"
																		maxFractionDigits="0" pattern="0" />
																</h:inputText>
																<hx:commandExButton type="submit" value="Redisplay"
																	styleClass="commandExButton" id="buttonUpdateRowToShow"
																	style="padding: 0px; font-size: 9pt"
																	action="#{pc_Advancedsearch.doButtonUpdateRowToShowAction}" onclick="return func_2(this, event);"></hx:commandExButton>
															</h:panelGrid>
														</hx:panelBox>
													</f:facet>
													<f:facet name="footer">
														<hx:panelBox styleClass="panelBox" id="boxFooterBox">

															<hx:outputSelecticons styleClass="outputSelecticons"
																id="selecticonsRowSelectionFooter"></hx:outputSelecticons>
															<h:panelGrid styleClass="panelGrid"
																id="gridBottomPagerGrid" width="350" columns="1"
																style="text-align: right">
																<hx:pagerWeb styleClass="pagerWeb" id="webBottomPager" />
															</h:panelGrid>


														</hx:panelBox>
													</f:facet>
													<hx:columnEx id="columnEx3">
														<hx:inputRowSelect styleClass="inputRowSelect"
															id="rowSelectSearchResults"
															value="#{varlastSearchResults.campaignSelected}"></hx:inputRowSelect>
														<f:facet name="header"></f:facet>
													</hx:columnEx>
													<hx:columnEx id="columnEx1" nowrap="true">
														<f:facet name="header">
															<hx:sortHeader styleClass="sortHeader"
																defaultSortOrder="sortbi" id="sortHeaderCampaignName"
																action="#{pc_Publish_campaigns.doSortHeaderCampaignNameAction}">
																<h:outputText id="text5" styleClass="outputText"
																	value="Campaign Name"></h:outputText>
															</hx:sortHeader>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textCampaignName"
															value="#{varlastSearchResults.campaignName}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx4" nowrap="true">
														<f:facet name="header">
															<hx:sortHeader styleClass="sortHeader"
																defaultSortOrder="sortbi" id="sortHeaderCampaignGroup"
																action="#{pc_Publish_campaigns.doSortHeaderCampaignGroupAction}">
																<h:outputText value="Campaign Group"
																	styleClass="outputText" id="textCampGroupHeaderLabel"></h:outputText>
															</hx:sortHeader>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textCampaignGroup"
															value="#{varlastSearchResults.campaignGroup}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx2">
														<f:facet name="header">
															<h:outputText value="Pub" styleClass="outputText"
																id="text6"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText" id="textCampaignPub"
															value="#{varlastSearchResults.pubcode}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnExKeycodeCol">
														<f:facet name="header">
															<hx:sortHeader styleClass="sortHeader"
																defaultSortOrder="sortbi" id="sortHeaderKeycode"
																action="#{pc_Publish_campaigns.doSortHeaderKeycodeAction}">
																<h:outputText value="Keycode" styleClass="outputText"
																	id="textKeycodeHeaderLabel"></h:outputText>
															</hx:sortHeader>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textCampaignKeycode"
															value="#{varlastSearchResults.keycode}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx5">
														<f:facet name="header">
															<h:outputText value="Production Vanity"
																styleClass="outputText" id="text9"></h:outputText>
														</f:facet>
														<hx:outputLinkEx styleClass="outputLinkEx"
															id="linkExVanityLink"
															value="#{varlastSearchResults.fullVanityURL}"
															target="_blank">
															<h:outputText id="textProdVanityLinkText"
																styleClass="outputText"
																value="#{varlastSearchResults.fullVanityURLText}"></h:outputText>
														</hx:outputLinkEx>
													</hx:columnEx>
													<hx:columnEx id="columnEx7" nowrap="true">
														<f:facet name="header">
															<h:outputText value="Test Vanity" styleClass="outputText"
																id="text10"></h:outputText>
														</f:facet>
														<hx:outputLinkEx styleClass="outputLinkEx"
															id="linkExTestServerLink"
															value="#{varlastSearchResults.fullVanityTestURL}"
															target="_blank">
															<h:outputText id="textTestVanityLabel"
																styleClass="outputText"
																value="#{varlastSearchResults.fullVanityURLText}"></h:outputText>
														</hx:outputLinkEx>
													</hx:columnEx>
													<hx:columnEx id="columnEx8">
														<f:facet name="header">
															<h:outputText value="Status" styleClass="outputText"
																id="text11"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textCampaignStatus"
															value="#{varlastSearchResults.campaignStatusString}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnExLastErrMsg">
														<f:facet name="header">
															<h:outputText value="Publish Message"
																styleClass="outputText" id="text1"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textLastPublishMessage"
															value="#{varlastSearchResults.campaignErrorMessage}"></h:outputText>
													</hx:columnEx>
												</hx:dataTableEx>


											</hx:panelSection>
											<br>

											<br>
											<br>
											<br>
											<br>
										</h:form>
									</hx:scriptCollector><%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>