<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/users/powerUser/Add_user.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Add User</TITLE>
			<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

}</script><%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%><hx:scriptCollector id="scriptCollector1">
									<h:form styleClass="form" id="formAddUserForm">
										<h:messages styleClass="messages" id="messages1"
											globalOnly="false" style="font-size: 12pt"></h:messages>
										<hx:panelFormBox helpPosition="over" labelPosition="left"
											styleClass="panelFormBox" id="formBoxAddUser"
											label="New User Information">

											<hx:formItem styleClass="formItem"
												id="formItemNewUserEnabled" label="Enabled:">
												<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
													id="checkboxNewUserEnabled" value="#{newSubscriberPortalUser.enabled}" tabindex="1"></h:selectBooleanCheckbox>
											</hx:formItem>
											<hx:formItem styleClass="formItem" id="formItemUserID"
												label="*User ID:">
												<h:inputText styleClass="inputText" id="textUserID"
													size="25" value="#{newSubscriberPortalUser.userID}"
													required="true" tabindex="2">
													<f:validateLength minimum="2" maximum="32"></f:validateLength>
												</h:inputText>
												<h:message for="textUserID"></h:message>
											</hx:formItem>
											<hx:formItem styleClass="formItem" id="formItemFirstName"
												label="*First Name:">
												<h:inputText styleClass="inputText" id="textFirstName"
													size="25" value="#{newSubscriberPortalUser.firstName}"
													required="true" tabindex="3">
													<f:validateLength minimum="1" maximum="40"></f:validateLength>
												</h:inputText>
												<h:message for="textFirstName"></h:message>
											</hx:formItem>
											<hx:formItem styleClass="formItem" id="formItemLastName"
												label="*Last Name:">
												<h:inputText styleClass="inputText" id="textLastName"
													size="25" value="#{newSubscriberPortalUser.lastName}"
													required="true" tabindex="4">
													<f:validateLength minimum="1" maximum="40"></f:validateLength>
												</h:inputText>
												<h:message for="textLastName"></h:message>
											</hx:formItem>
											<hx:formItem styleClass="formItem" id="formItemPhone"
												label="Phone:"
												infoText="Extension or full phone number (no dashes or spaces)">
												<h:inputText styleClass="inputText" id="textPhone" size="25"
													value="#{newSubscriberPortalUser.phone}" tabindex="5">
													<f:validateLength maximum="10"></f:validateLength>
												</h:inputText>
												<h:message for="textPhone"></h:message>
											</hx:formItem>
											<hx:formItem styleClass="formItem" id="formItemEmail"
												label="*Email Address:">
												<h:inputText styleClass="inputText" id="textEmailAddress"
													size="25" value="#{newSubscriberPortalUser.emailAddress}"
													tabindex="6" required="true">
													<f:validateLength minimum="6" maximum="128"></f:validateLength>
												</h:inputText>
												<h:message for="textEmailAddress"></h:message>
											</hx:formItem>
											<hx:formItem styleClass="formItem" id="formItem1UserRole"
												label="*User Role:">
												<h:selectManyCheckbox
													disabledClass="selectManyCheckbox_Disabled"
													styleClass="selectManyCheckbox" id="checkboxNewRoles"
													layout="pageDirection"
													value="#{newSubscriberPortalUser.assignedRolesAsArray}"
													tabindex="7" required="true">
													<f:selectItem itemLabel="Limited User" itemValue="4" />
													<f:selectItem itemLabel="Normal User" itemValue="2" />
													<f:selectItem itemLabel="Power User" itemValue="3" />
												</h:selectManyCheckbox>
												<h:message for="checkboxNewRoles"></h:message>
											</hx:formItem>
											<f:facet name="right"></f:facet>
											<f:facet name="bottom">
												<h:panelGrid styleClass="panelGrid" id="gridFooterGrid"
													columns="2">
													<hx:commandExButton type="submit" value="Save"
														styleClass="commandExButton" id="buttonSaveUser"
														action="#{pc_Add_user.doButtonSaveUserAction}"></hx:commandExButton>
														<hx:commandExButton type="submit" value="Cancel"
															styleClass="commandExButton" id="buttonCancel"
															action="#{pc_Add_user.doButtonCancelAction}" immediate="true">
														</hx:commandExButton>
													</h:panelGrid>
											</f:facet>
										</hx:panelFormBox>
									</h:form>
									<br><br>
									
								</hx:scriptCollector><%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>