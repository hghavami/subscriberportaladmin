<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/users/powerUser/Administration.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>User Management</TITLE>
			<script type="text/javascript">

function func_4(thisObj, thisEvent) {
<!--
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
//actionBegin: com.ibm.sed.jseditor.script021
_JumpURL('/portaladmin/secured/users/powerUser/add_user.faces');
//actionEnd: com.ibm.sed.jseditor.script021

}

function _JumpURL(url) 
{
  if (url != "")
  {
    window.location = url;
  }
}

//-->
</script><%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
								<hx:scriptCollector id="scriptCollector1"><h:form styleClass="form" id="form1">
										<h:panelGrid styleClass="panelGrid" id="gridOptionsGrid" columns="4">
											<hx:commandExButton type="submit" value="Add New User"
												styleClass="commandExButton" id="buttonClick"
												action="#{pc_Administration.doButtonClickAction}">
											</hx:commandExButton>
										</h:panelGrid>
										<br>
										<h:messages styleClass="messages" id="messages1"></h:messages>
										<hx:panelSection styleClass="panelSection"
											id="sectionCurrentSystemUsers" initClosed="false">
											<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
												columnClasses="columnClass3" headerClass="headerClass"
												footerClass="footerClass" rowClasses="rowClass1, rowClass3"
												styleClass="dataTableEx" id="tableExCurrentUsers"
												value="#{adminSiteUsers.users}" var="varusers"
												style="margin-top: 10px">
												<f:facet name="header">
													<hx:panelBox styleClass="panelBox" id="box1">

														<hx:commandExButton type="submit" value="Reset Password"
															styleClass="commandExButton" id="buttonResetPassword"
															action="#{pc_Administration.doButtonResetPasswordAction}" confirm="This will reset all passwords for the selected users. They will be sent an email with their new password. Continue?"></hx:commandExButton>
														<hx:commandExButton type="submit" value="Delete Selected"
															styleClass="commandExButton"
															id="buttonDeleteSelectedUsers" action="#{pc_Administration.doButtonDeleteSelectedUsersAction}" confirm="All of the selected users will be deleted. Are you sure you want to continue?"></hx:commandExButton>

													</hx:panelBox>
												</f:facet>
												<hx:columnEx id="columnEx6">
													<hx:inputRowSelect styleClass="inputRowSelect"
														id="rowSelectedFlag" value="#{varusers.userSelected}"></hx:inputRowSelect>
													<f:facet name="header"></f:facet>
												</hx:columnEx>
												<hx:columnEx id="columnEx1">
													<f:facet name="header">
														<h:outputText id="text3" styleClass="outputText"
															value="Last Name"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textLastName"
														value="#{varusers.lastName}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx2">
													<f:facet name="header">
														<h:outputText value="First Name" styleClass="outputText"
															id="text4"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textFirstName"
														value="#{varusers.firstName}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx5">
													<f:facet name="header">
														<h:outputText value="User ID" styleClass="outputText"
															id="text7"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textUserID"
														value="#{varusers.userID}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx3">
													<f:facet name="header">
														<h:outputText value="Phone" styleClass="outputText"
															id="text5"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textPhone"
														value="#{varusers.phone}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx4">
													<f:facet name="header">
														<h:outputText value="Email" styleClass="outputText"
															id="text6"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textEmailAddr"
														value="#{varusers.emailAddress}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx8" nowrap="true">
													<f:facet name="header">
														<h:outputText value="Role" styleClass="outputText"
															id="text8"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText"
														id="textAssignedRoles"
														value="#{varusers.assignedRolesAsString}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx9">
													<f:facet name="header">
														<h:outputText value="Enabled" styleClass="outputText"
															id="text9"></h:outputText>
													</f:facet>
													<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
														id="checkboxUserCurrentEnabled" value="#{varusers.enabled}" readonly="true" disabled="true"></h:selectBooleanCheckbox>
												</hx:columnEx>
												<hx:columnEx id="columnEx7">
													<hx:commandExRowEdit styleClass="commandExRowEdit"
														id="rowEditUser" action="#{pc_Administration.doRowEditUserAction}">
														<hx:jspPanel id="jspPanel3">
															<hx:panelFormBox helpPosition="over" labelPosition="left"
																styleClass="panelFormBox" id="formBox1"
																label="Edit User">
																<hx:formItem styleClass="formItem"
																	id="formItemNewEnabledStatus" label="Enabled:">
																	<h:selectBooleanCheckbox
																		styleClass="selectBooleanCheckbox"
																		id="checkboxNewEnabledStatus" value="#{varusers.enabled}"></h:selectBooleanCheckbox>
																</hx:formItem>
																<hx:formItem styleClass="formItem" id="formItemLastName"
																	label="Last Name:">
																	<h:inputText styleClass="inputText"
																		id="textNewLastName" value="#{varusers.lastName}" size="40" required="true">
																		<f:validateLength minimum="1" maximum="40"></f:validateLength>
																	</h:inputText>
																	<h:message for="textNewLastName"></h:message>
																</hx:formItem>
																<hx:formItem styleClass="formItem"
																	id="formItemFirstName" label="First Name:">
																	<h:inputText styleClass="inputText"
																		id="textNewFirstName" value="#{varusers.firstName}" size="40" required="true">
																		<f:validateLength minimum="1" maximum="40"></f:validateLength>
																	</h:inputText>
																	<h:message for="textNewFirstName"></h:message>
																</hx:formItem>
																<hx:formItem styleClass="formItem" id="formItemPhone"
																	label="Phone:" infoText="Extension or full phone number (no dashes or spaces)">
																	<h:inputText styleClass="inputText" id="textNewPhone"
																		value="#{varusers.phone}">
																		<f:validateLength maximum="10"></f:validateLength>
																	</h:inputText>
																	<h:message for="textNewPhone"></h:message>
																</hx:formItem>
																<hx:formItem styleClass="formItem"
																	id="formItemEmailAddress" label="Email:">
																	<h:inputText styleClass="inputText" id="textNewEmail"
																		value="#{varusers.emailAddress}" size="40" required="true">
																		<f:validateLength minimum="4" maximum="128"></f:validateLength>
																	</h:inputText>
																	<h:message for="textNewEmail"></h:message>
																</hx:formItem>
																<hx:formItem styleClass="formItem"
																	id="formItemNewUserRole" label="User Role:">
																	<h:selectManyCheckbox
																		disabledClass="selectManyCheckbox_Disabled"
																		styleClass="selectManyCheckbox" id="checkboxNewRoles"
																		layout="pageDirection"
																		value="#{varusers.assignedRolesAsArray}" required="true">
																		<f:selectItem itemLabel="Limited User" itemValue="4" />
																		<f:selectItem itemLabel="Normal User" itemValue="2" />
																		<f:selectItem itemLabel="Power User" itemValue="3" />
																	</h:selectManyCheckbox>
																	<h:message for="checkboxNewRoles"></h:message>
																</hx:formItem>
																<f:facet name="top">
																	<hx:formMessagesArea id="formMessagesArea1"></hx:formMessagesArea>
																</f:facet>
															</hx:panelFormBox>
														</hx:jspPanel>
													</hx:commandExRowEdit>
													<f:facet name="header"></f:facet>
												</hx:columnEx>
											</hx:dataTableEx>
											<f:facet name="closed">
												<hx:jspPanel id="jspPanel2">
													<hx:graphicImageEx id="imageEx2"
														styleClass="graphicImageEx" align="middle"
														value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
													<h:outputText id="text2" styleClass="outputText"
														value="Current Admin Site Users"></h:outputText>
												</hx:jspPanel>
											</f:facet>
											<f:facet name="opened">
												<hx:jspPanel id="jspPanel1">
													<hx:graphicImageEx id="imageEx1"
														styleClass="graphicImageEx" align="middle"
														value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
													<h:outputText id="text1" styleClass="outputText"
														value="Current Admin Site Users"></h:outputText>
												</hx:jspPanel>
											</f:facet>
										</hx:panelSection>
										<br>
										<br>
									</h:form></hx:scriptCollector>
							<%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>