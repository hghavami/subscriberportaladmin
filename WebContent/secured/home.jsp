<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/Home.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Subscriber Portal Administration Site Home</TITLE>
				<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
				<META HTTP-EQUIV="Expires" CONTENT="-1">			
				
					<LINK rel="stylesheet" type="text/css"
						href="/portaladmin/theme/C_stylesheet_blue.css" title="Style">
				<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
														<div id="searchDivArea" class="topSearchRight">
									<f:subview id="subviewBasicSearch">
										<hx:scriptCollector id="scriptCollectorBasicSearchForm">
										
										<h:form styleClass="form" id="formBasicSearchForm">
										<TABLE border="0" cellpadding="0" cellspacing="1">
											<TBODY>
												<TR>
													<TD nowrap><h:outputText styleClass="outputText_Small"
													id="textBasicSearchLabel" value="Search Campaigns:"></h:outputText></TD>
												<TD nowrap><h:inputText styleClass="inputText"
															id="textBasicSearchCriteria" size="20" required="true"
															value="#{basicSearchCriteria.queryString}">
															<f:validateLength minimum="1"></f:validateLength>
														</h:inputText>&nbsp;<hx:commandExButton type="submit"
													styleClass="commandExButton" id="buttonBasicSearch"
													value="Search" action="#{pc_Home.doButtonBasicSearchAction}"></hx:commandExButton><hx:outputLinkEx
													styleClass="outputLinkEx"
													value="/portaladmin/secured/campaigns/advancedsearch.faces"
													id="linkExAdvSearchLink">
													<h:outputText id="textAdvSearchLabel"
														styleClass="outputText_Small" value="Advanced Search"></h:outputText>
												</hx:outputLinkEx></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD><h:message styleClass="message" id="message1"
													for="textBasicSearchCriteria"></h:message></TD>
													<TD></TD>
												</TR>
												<TR>
													<TD></TD>
													<TD></TD>
													<TD></TD>
												</TR>
											</TBODY>
										</TABLE>
												<hx:behaviorKeyPress key="Enter" id="behaviorKeyPress1" behaviorAction="click" targetAction="buttonBasicSearch"></hx:behaviorKeyPress>
												<hx:inputHelperSetFocus id="setFocus1" target="textBasicSearchCriteria"></hx:inputHelperSetFocus>
											</h:form>
										</hx:scriptCollector>
									</f:subview>								
								</div>
						
								<hx:scriptCollector id="scriptCollectorHomePage">
									<h:outputText styleClass="outputText_Large"
										id="textWelcomeText"
										value="USA TODAY Subscriber Portal Administration Site"></h:outputText><h:form styleClass="form" id="formCampaignHome">
									<TABLE border="0" cellpadding="0" cellspacing="1" style="margin-top: 8px">
										<TBODY>
											<TR>
												<TD width="200"></TD>
												<TD width="10"></TD>
													<TD></TD>
											</TR>
											<TR>
												<TD width="200" valign="top">
													<hx:panelSection styleClass="panelSection"
															id="sectionCampManagementSection"
															style="width: 250px; margin-bottom: 10px"
															initClosed="false">
															<hx:jspPanel id="jspPanelCampManagementPanel">

																<hx:dataTableEx border="0" cellpadding="2"
																	cellspacing="0" columnClasses="columnClass1"
																	headerClass="headerClass" footerClass="footerClass"
																	rowClasses="rowClass1, rowClass2"
																	styleClass="dataTableEx"
																	id="tableExCampaignCountsDGrid"
																	value="#{campaignStatisticsHandler.campaignStatsCollection}"
																	var="varcampaignStatsCollection" width="100%"
																	style="border-bottom-width: 2px; border-bottom-style: solid">
																	<f:facet name="header">
																		<hx:panelBox styleClass="panelBox" id="box1">
																			<hx:commandExButton type="submit" value="Refresh"
																				styleClass="commandExButton" id="buttonRefreshStats" action="#{pc_Home.doButtonRefreshStatsAction}"></hx:commandExButton>
																		</hx:panelBox>
																	</f:facet>
																	<hx:columnEx id="columnExCampCountDGridCol1"
																		nowrap="true">
																		<f:facet name="header">
																			<h:outputText id="columnExCampCountDGridCol1Header"
																				styleClass="outputText" value="Product"></h:outputText>
																		</f:facet>
																		<h:outputText styleClass="outputText" id="text8"
																			value="#{varcampaignStatsCollection.product.name}"></h:outputText>
																	</hx:columnEx>
																	<hx:columnEx id="columnEx1" align="right">
																		<h:inputText styleClass="inputText" id="text1"
																			value="#{varcampaignStatsCollection.numCampaigns}"
																			readonly="true" disabled="true"
																			style="color: black; font-weight: bold; text-align: right"
																			size="4"></h:inputText>
																		<f:facet name="header">
																			<h:outputText value="# of Campaigns"
																				styleClass="outputText" id="text9"></h:outputText>
																		</f:facet>
																	</hx:columnEx>
																</hx:dataTableEx>

																<TABLE border="0" cellpadding="0" cellspacing="1"
																	width="250">
																	<TBODY>
																		<TR>
																			<TD align="left" nowrap><h:outputText
																				styleClass="outputText" id="textNumCampaignsLabel"
																				value="Total Campaigns in System:" style="font-weight: bold"></h:outputText> <hx:outputLinkEx
																				styleClass="outputLinkEx"
																				id="linkExToAdvancedSearch"
																				value="/portaladmin/secured/campaigns/advancedsearch.faces">
																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageExSearchCampsImageOpen"
																					value="/portaladmin/images/icon_detailsmag14x14.gif"
																					width="14" height="14" title="Campaign Search"></hx:graphicImageEx>
																			</hx:outputLinkEx></TD>
																			<TD width="10"></TD>
																			<TD align="right"><h:inputText styleClass="inputText"
																				id="textNumCampaigns"
																				value="#{campaignStatisticsHandler.totalCampaigns}"
																				size="4" readonly="true"
																				style="font-weight: bold; text-align: right" disabled="true"></h:inputText></TD>
																		</TR>
																		
																	</TBODY>
																</TABLE>
															</hx:jspPanel>
															<f:facet name="closed">
																<hx:jspPanel id="jspPanel11">
																	<hx:graphicImageEx id="imageExCampManCol"
																		value="/portaladmin/images/arrow_collapsed.gif"
																		styleClass="graphicImageEx" align="middle"></hx:graphicImageEx>
																	<h:outputText id="text17" styleClass="outputText"
																		value="Campaign Management:"></h:outputText>
																</hx:jspPanel>
															</f:facet>
															<f:facet name="opened">
																<hx:jspPanel id="jspPanel10">
																	<hx:graphicImageEx id="imageExCampManExp"
																		value="/portaladmin/images/arrow_expanded.gif"
																		styleClass="graphicImageEx" align="middle"></hx:graphicImageEx>
																	<h:outputText id="text16" styleClass="outputText"
																		value="Campaign Management:"></h:outputText>
																</hx:jspPanel>
															</f:facet>
														</hx:panelSection>																									
												</TD>
												<TD width="10"></TD>
												<TD valign="top" align="center" width="330">
												</TD>
											</TR>
											<tr>
													<td width="200" valign="top"></td>
													<td width="10"></td>
													<td valign="top" align="center" ></td>
											</tr>
											<tr>
													<td width="200" valign="top"></td>
													<td width="10"></td>
													<td valign="top" align="center" ></td>
											</tr>
											<TR>
													<TD valign="top" colspan="3" width="100%"><hx:panelSection
														styleClass="panelSection" id="sectionCoverGraphics"
														initClosed="false" style="width: 100%; margin-top: 10px">
														<h:panelGrid styleClass="panelGrid" columnClasses="topAlign" id="gridInsideGrid"
															columns="2" width="100%">
															<hx:panelBox styleClass="panelBox" id="boxleftBoxGraphic"
																valign="top" height="100%">
																<h:panelGrid styleClass="panelGrid" id="gridLeftPanel" columnClasses="topAlign" 
																	columns="1">
																	<hx:jspPanel id="jspPanelUsatImgHeader">USA TODAY: <hx:requestLink
																				styleClass="requestLink" id="linkUploadNewUT" action="#{pc_Home.doLinkUploadNewUTAction}">
																				<h:outputText id="text5" styleClass="outputText"
																					value="Upload New"></h:outputText>
																			</hx:requestLink>
																		</hx:jspPanel>
																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageExUTCover"
																					value="#{utDefaultCampaign.sourcePromosHandler.productImage1Path}"
																					title="(#{utDefaultCampaign.sourcePromosHandler.productImage1Path})"></hx:graphicImageEx>

																			</h:panelGrid>
															</hx:panelBox>
															<hx:panelBox styleClass="panelBox topAlign"  
																id="boxPanelBoxRightCovGraphic" height="100%" valign="top">
																<h:panelGrid styleClass="panelGrid" columnClasses="topAlign"
																	id="gridRightPanelGrid" columns="1">
																	<hx:jspPanel id="jspPanel3">
																		Sports Weekly: <hx:requestLink
																				styleClass="requestLink"
																				id="linkSWCoverGraphicUpload" action="#{pc_Home.doLinkSWCoverGraphicUploadAction}">
																				<h:outputText id="text6" styleClass="outputText"
																					value="Upload New"></h:outputText>
																			</hx:requestLink>
																		</hx:jspPanel>
																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageExBWDefaultCover"
																					value="#{swDefaultCampaign.sourcePromosHandler.productImage1Path}"
																					title="(#{swDefaultCampaign.sourcePromosHandler.productImage1Path})"></hx:graphicImageEx>
																			</h:panelGrid>
															</hx:panelBox>
														</h:panelGrid>

														<f:facet name="closed">
															<hx:jspPanel id="jspPanel2">
																<hx:graphicImageEx id="imageExCoverGraphCol"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
																<h:outputText id="text9" styleClass="outputText"
																	value="Current Cover Graphics:"></h:outputText>
															</hx:jspPanel>
														</f:facet>
														<f:facet name="opened">
															<hx:jspPanel id="jspPanel1">
																<hx:graphicImageEx id="imageExCovGraphicExp"
																	styleClass="graphicImageEx" align="middle"
																	value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
																<h:outputText id="text8" styleClass="outputText"
																	value="Current Cover Graphics:"></h:outputText>
															</hx:jspPanel>
														</f:facet>
													</hx:panelSection>
													</TD>
												</TR>
												<TR>
													<TD width="200"></TD>
													<TD width="10"></TD>
													<TD valign="top" align="center" ></TD>
												</TR>
											</TBODY>
									</TABLE></h:form><BR><BR></hx:scriptCollector><%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>