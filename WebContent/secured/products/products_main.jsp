<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/products/Products_main.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>products_main</TITLE>
			<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
									<hx:scriptCollector id="scriptCollector1">
										<br>
										<h:messages styleClass="messages" id="messages1"></h:messages>
										<h:form styleClass="form" id="form1">
											<hx:dataTableEx border="0" cellpadding="2" cellspacing="1"
												columnClasses="columnClass1" headerClass="headerClass"
												footerClass="footerClass" rowClasses="rowClass1, rowClass2"
												styleClass="dataTableEx" id="tableEx1"
												value="#{productHandler.products}" var="varproducts">
												<f:facet name="header">
													<hx:panelBox styleClass="panelBox" id="box1">
														<hx:commandExButton type="submit" value="Refresh"
															styleClass="commandExButton" id="buttonRefreshProducts" action="#{pc_Products_main.doButtonRefreshProductsAction}"></hx:commandExButton>
													</hx:panelBox>
												</f:facet>
												<hx:columnEx id="columnEx1" align="center">
													<f:facet name="header">
														<h:outputText id="textProductCodeLabel"
															styleClass="outputText" value="Product Code"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="textProductCode"
														value="#{varproducts.productCode}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx2">
													<f:facet name="header">
														<h:outputText value="Name" styleClass="outputText"
															id="text1"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text3"
														value="#{varproducts.name}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx3">
													<f:facet name="header">
														<h:outputText value="Description" styleClass="outputText"
															id="text2"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text4"
														value="#{varproducts.description}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx4" align="center">
													<f:facet name="header">
														<h:outputText value="Branding Product Code"
															styleClass="outputText" id="text5"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text6"
														value="#{varproducts.brandingPubCode}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx5" align="center">
													<f:facet name="header">
														<h:outputText value="Default Keycode"
															styleClass="outputText" id="text7"></h:outputText>
													</f:facet>
														<h:panelGrid styleClass="panelGrid" id="grid1DefKeycodechange" columns="2" cellpadding="1">
															<h:outputText styleClass="outputText" id="text11" value="#{varproducts.defaultKeycode}"></h:outputText>
															<hx:outputLinkEx
																value="/portaladmin/secured/defaultKeycode/change.faces?product=#{varproducts.productCode}"
																styleClass="outputLinkEx" id="linkEx1">
																<h:outputText id="text15" styleClass="outputText"
																	value="[Edit]"></h:outputText>
															</hx:outputLinkEx>
														</h:panelGrid>
												</hx:columnEx>
												<hx:columnEx id="columnEx6" align="center">
													<f:facet name="header">
														<h:outputText value="Expired Offer Keycode"
															styleClass="outputText" id="text8"></h:outputText>
													</f:facet>
													<h:panelGrid styleClass="panelGrid" id="grid2DefKeycodechange" columns="2" cellpadding="1">
														<h:outputText styleClass="outputText" id="text12" value="#{varproducts.expiredOfferKeycode}"></h:outputText>
														<hx:outputLinkEx	value="/portaladmin/secured/defaultKeycode/change.faces?product=#{varproducts.productCode}"
																styleClass="outputLinkEx" id="linkEx111">
																<h:outputText id="text1511" styleClass="outputText"
																	value="[Edit]"></h:outputText>
															</hx:outputLinkEx>
													</h:panelGrid>
												</hx:columnEx>
												<hx:columnEx id="columnEx7" align="center">
													<f:facet name="header">
														<h:outputText value="Max Days For Future Start"
															styleClass="outputText" id="text9"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text13" value="#{varproducts.maxDaysInFutureBeforeFulfillment}"></h:outputText>
												</hx:columnEx>
												<hx:columnEx id="columnEx8" nowrap="true">
													<f:facet name="header">
														<h:outputText value="Long Description"
															styleClass="outputText" id="text10"></h:outputText>
													</f:facet>
													<h:outputText styleClass="outputText" id="text14" escape="false" value="#{varproducts.detailedDescription}"></h:outputText>
												</hx:columnEx>
											</hx:dataTableEx>
										</h:form>
										<br>
										<br></hx:scriptCollector>
								<%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>