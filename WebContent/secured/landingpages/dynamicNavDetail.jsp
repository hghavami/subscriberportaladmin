<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/landingpages/DynamicNavDetail.java" --%><%-- /jsf:pagecode --%>

<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%><%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Dynamic Navigation</TITLE>
				<link rel="stylesheet" type="text/css" title="Style"
					href="${pageContext.request.contextPath}/theme/tabpanel.css">
			<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
history.go(-1);
}

function func_3(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDisplayedInnerTabPanelHeader(thisObj);

}

function updateDisplayedInnerTabPanelHeader(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerLeftNavConfig4');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelLeftNavCurrent4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoLeftNav4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomLeftNav');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelLeftNavCurrent4');
	panelCtrl.restoreUIState(panel);
 }
}
</script>					
<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
									<hx:scriptCollector id="scriptCollector1"><h:form styleClass="form" id="form1">
											<hx:outputLinkEx styleClass="outputLinkEx" id="linkExback1" value="#" onclick="return func_2(this, event);">
												<h:outputText id="textBack1" styleClass="outputText"
													value="&lt;&lt;...Back" escape="false"></h:outputText>
											</hx:outputLinkEx><br/>
											<h:messages styleClass="messages" id="messages1"></h:messages>
											<h:panelGrid styleClass="panelGrid" id="gridProductGrid"
												columns="2">
												<h:outputText styleClass="outputText" id="text2" value="Editing Default Settings for Publication:" style="font-weight: bold"></h:outputText>
												<h:outputText styleClass="outputText" id="textProductName"
													value="#{currentDefaultCampaign.campaign.product.name}" style="color: blue"></h:outputText>
											</h:panelGrid>

											
											<hx:panelLayout styleClass="panelLayout" id="layout1">
												<f:facet name="body">
													<odc:tabbedPanel slantInactiveRight="4" width="400"
														styleClass="tabbedPanel" height="300" slantActiveRight="4"
														showBackNextButton="false" id="tabbedPanelLinksEditorTabs"
														showTabs="true" variableTabLength="false">
														<odc:bfPanel id="bfpanelOEPagesLinks" 
															name="Order Pages Navigation Links"
															showFinishCancelButton="false">

															<h:panelGrid styleClass="panelGrid"
																id="gridOrderPageInnerGrid" columns="1" width="100%" bgcolor="white">
																<hx:commandExButton type="button" value="Add New Link"
																	styleClass="commandExButton" id="buttonAddNewOELink"></hx:commandExButton>

																<hx:dataTableEx border="1" cellpadding="1"
																	cellspacing="0" headerClass="headerClass"
																	footerClass="footerClass" rowClasses="rowClass1"
																	styleClass="dataTableEx" id="tableExOrderPageNavLinks"
																	value="#{currentDefaultCampaign.promotionSetHandler.orderEntryDynamicLinks}"
																	var="varorderEntryDynamicLinks"
																	columnClasses="columnClass1, " width="300">
																	<hx:columnEx id="columnExDynamicLinks" align="left"
																		nowrap="true" width="160" valign="middle">
																		<f:facet name="header">
																			<h:outputText id="textLinkcolHeader"
																				styleClass="outputText"
																				value="Current Dynamic Links"></h:outputText>
																		</f:facet>
																		<hx:jspPanel id="jspPanelOEInnerPanel1">
																			<div id="nav">
																			<div id="leftNavList">
																			<ul>

																				<h:outputText styleClass="outputText"
																					id="textCurrentLinkeOE" escape="false"
																					value="#{varorderEntryDynamicLinks.link}"></h:outputText>

																			</ul>
																			</div>
																			</div>
																		</hx:jspPanel>
																	</hx:columnEx>
																	<hx:columnEx id="columnEx1" align="center">
																		<f:facet name="header">
																		</f:facet>
																		<hx:commandExButton type="submit" value="Delete Link"
																			styleClass="commandExButton" id="buttonOEDeleteLinks"
																			action="#{pc_DynamicNavDetail.doButtonOEDeleteLinksAction}"></hx:commandExButton>
																	</hx:columnEx>
																</hx:dataTableEx>
															</h:panelGrid>
														</odc:bfPanel>
														<odc:bfPanel id="bfpanelCSPagesLinks"
															name="Customer Service Navigation Links"
															showFinishCancelButton="false">
															<h:panelGrid styleClass="panelGrid"
																id="gridCSPageInnerGrid" columns="1" width="100%" bgcolor="white">
																<hx:commandExButton type="button" value="Add New Link"
																	styleClass="commandExButton" id="buttonAddNewCSLink"></hx:commandExButton>

																<hx:dataTableEx border="1" cellpadding="1"
																	cellspacing="0" headerClass="headerClass"
																	footerClass="footerClass" rowClasses="rowClass1"
																	styleClass="dataTableEx" id="tableExCSPageNavLinks"
																	value="#{currentDefaultCampaign.promotionSetHandler.custServiceDynamicLinks}"
																	var="varcsEntryDynamicLinks"
																	columnClasses="columnClass1, " width="300">
																	<hx:columnEx id="columnExDynamicLinks1" align="left"
																		nowrap="true" width="160" valign="middle">
																		<f:facet name="header">
																			<h:outputText id="textLinkcolHeader1"
																				styleClass="outputText"
																				value="Current Dynamic Links"></h:outputText>
																		</f:facet>
																		<hx:jspPanel id="jspPanelCSInnerPanel1">
																			<div id="nav">
																			<div id="leftNavList">
																			<ul>

																				<h:outputText styleClass="outputText"
																					id="textCurrentLinkeCS" escape="false"
																					value="#{varcsEntryDynamicLinks.link}"></h:outputText>

																			</ul>
																			</div>
																			</div>
																		</hx:jspPanel>
																	</hx:columnEx>
																	<hx:columnEx id="columnEx11" align="center">
																		<f:facet name="header">
																		</f:facet>
																		<hx:commandExButton type="submit" value="Delete Link"
																			styleClass="commandExButton" id="buttonCSDeleteLinks"
																			action="#{pc_DynamicNavDetail.doButtonCSDeleteLinksAction}"></hx:commandExButton>
																	</hx:columnEx>
																</hx:dataTableEx>
															</h:panelGrid>															
														</odc:bfPanel>
														<odc:bfPanel id="bfpanelNavImage" name="Left Nav Image"
															showFinishCancelButton="false">
															
															
															<h:panelGrid styleClass="panelGrid"
																id="gridLeftNavImageOptionGrid8" cellpadding="2"
																columns="1">
																<f:facet name="header">
																	<h:panelGroup styleClass="panelGroup" id="groupLeftNavImage18">
																		<h:outputText styleClass="outputText_Med"
																			id="textLeftNavImageHeader8"
																			value="Left Navigation Image (Recommend no more than 155 pixel wide, variable  height)"
																			style="font-weight: bold; font-size: 12pt"></h:outputText>
																	</h:panelGroup>
																</f:facet>

																<h:selectOneRadio
																	disabledClass="selectOneRadio_Disabled"
																	enabledClass="selectOneRadio_Enabled"
																	styleClass="selectOneRadio" id="radioLeftNavOpSelect"
																	style="font-weight: bold"
																	onclick="return func_3(this, event);" value="#{coverGraphicsHandler.currentGraphicImageOption}">
																	<f:selectItem itemLabel="Current Image Selection"
																		itemValue="default" />
																	<f:selectItem itemLabel="No Image" itemValue="none" />
																	<f:selectItem itemLabel="Upload Image"
																		itemValue="custom" />
																</h:selectOneRadio>

															</h:panelGrid>
															<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerLeftNavConfig4">
																		<odc:bfPanel id="bfpanelLeftNavCurrent4" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage4" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group7">

																				<hx:graphicImageEx styleClass="graphicImageEx"
																					id="imageLeftNav" align="middle"
																					value="#{currentDefaultCampaign.promotionSetHandler.templateNavigationImagePath}"
																					hspace="10" vspace="1" border="0"></hx:graphicImageEx>
																			</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft4" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomLeftNavImageDefaultLabel4"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoLeftNav4" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage4">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerLeftNavNoImageLabel4"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																<odc:bfPanel id="bfpanelCustomLeftNav" name=""
																	showFinishCancelButton="false">
																	<hx:panelLayout styleClass="panelLayout"
																		id="layoutInnerCustomUpSellImageImageLayout4"
																		width="100%">
																		<f:facet name="body">
																			<hx:panelFormBox helpPosition="over"
																				labelPosition="left" styleClass="panelFormBox"
																				id="formBoxCustomImage4" widthLabel="125">
																				<hx:formItem styleClass="formItem"
																					id="formItemCustomLeftNavImageFormItem4"
																					label="Upload Image:"
																					infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																					<hx:fileupload styleClass="fileupload"
																						id="fileuploadCustomImageUpload4" size="40"
																						accept="image/*" title="Custom Image Upload" value="#{coverGraphicsHandler.currentImageGraphics.imageContents}">
																						<hx:fileProp name="fileName"
																							value="#{coverGraphicsHandler.currentImageGraphics.imageFileName}" />
																						<hx:fileProp name="contentType"
																							value="#{coverGraphicsHandler.currentImageGraphics.imageFileType}" />
																					</hx:fileupload>
																					<h:message
																						for="formItemCustomLeftNavImageFormItem4"
																						id="errorMsgCustomFileUpload4"></h:message>
																				</hx:formItem>
																				<hx:formItem styleClass="formItem"
																					id="formItemCustomLeftNavOnClickURL4"
																					label="On Click URL:"
																					infoText="Specify the URL to go to when this image is clicked.">
																					<h:inputText styleClass="inputText"
																						id="textCustomLeftNavOnClickURL4" size="45" value="#{coverGraphicsHandler.currentImageGraphics.linkURL}"></h:inputText>
																				</hx:formItem>
																				<hx:formItem styleClass="formItem"
																					id="formItemCustomLeftNavImageAltText4"
																					label="Alternate Text:"
																					infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																					<h:inputText styleClass="inputText"
																						id="textCustomLeftNavImageAltText4" size="45" value="#{coverGraphicsHandler.currentImageGraphics.alternateText}"></h:inputText>
																				</hx:formItem>
																			</hx:panelFormBox>
																		</f:facet>
																		<f:facet name="left"></f:facet>
																		<f:facet name="right"></f:facet>
																		<f:facet name="bottom"></f:facet>
																		<f:facet name="top"></f:facet>
																	</hx:panelLayout>
																</odc:bfPanel>
																<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>															
														
														
														
														
														</odc:bfPanel>
														<f:facet name="back">
															<hx:commandExButton type="submit" value="&lt; Back"
																id="tabbedPanel1_back" style="display:none"></hx:commandExButton>
														</f:facet>
														<f:facet name="next">
															<hx:commandExButton type="submit" value="Next &gt;"
																id="tabbedPanel1_next" style="display:none"></hx:commandExButton>
														</f:facet>
														<f:facet name="finish">
															<hx:commandExButton type="submit" value="Finish"
																id="tabbedPanel1_finish" style="display:none"></hx:commandExButton>
														</f:facet>
														<f:facet name="cancel">
															<hx:commandExButton type="submit" value="Cancel"
																id="tabbedPanel1_cancel" style="display:none"></hx:commandExButton>
														</f:facet>
														<odc:buttonPanel alignContent="left" id="bp1">
															<hx:commandExButton type="submit"
																value="Publish Campaign" styleClass="commandExButton"
																id="buttonPublishCampaign" action="#{pc_DynamicNavDetail.doButtonPublishCampaignAction}"></hx:commandExButton>
															<hx:commandExButton type="submit" value="Save Changes"
																styleClass="commandExButton" id="buttonSaveChanges" action="#{pc_DynamicNavDetail.doButtonSaveChangesAction}"></hx:commandExButton>
														</odc:buttonPanel>
													</odc:tabbedPanel>
												</f:facet>
												<f:facet name="left"></f:facet>
												<f:facet name="right">
												</f:facet>
												<f:facet name="bottom"></f:facet>
												<f:facet name="top">
												</f:facet>
											</hx:panelLayout>
											<hx:panelDialog type="modeless" styleClass="panelDialog"
												id="dialogOEAddLink" for="buttonAddNewOELink"
												title="Add New Link" initiallyShow="false" valign="middle">
												<hx:panelFormBox helpPosition="over" labelPosition="left"
													styleClass="panelFormBox" id="formBox1">
													<hx:formItem styleClass="formItem" id="formItemOEUrl"
														label="URL:"
														infoText="May be a relative url beginning with a '/' or a full URL beginning with 'http'.">
														<h:inputText styleClass="inputText" id="text3" value="#{genericFormBean.valueString1}"></h:inputText>

													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formItemDisplayName"
														label="Display Name">
														<h:inputText styleClass="inputText" id="text1" value="#{genericFormBean.valueString2}"></h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formItemdisplayOptoin" label="Open Link In:">
														<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
															enabledClass="selectOneRadio_Enabled"
															styleClass="selectOneRadio" id="radioOpenInOptoin" value="#{genericFormBean.valueString3}">
															<f:selectItem itemLabel="New Window" itemValue="_blank"
																id="selectItem1" />
															<f:selectItem itemLabel="Same Window" itemValue="" id="selectItem2" />
														</h:selectOneRadio>
													</hx:formItem>
												</hx:panelFormBox>
												<h:panelGroup id="group1" styleClass="panelDialog_Footer">
													<hx:commandExButton id="buttonModalOE2"
														styleClass="commandExButton" type="submit" value="OK" action="#{pc_DynamicNavDetail.doButtonModalOE2Action}">
														<hx:behavior event="onclick" behaviorAction="hide;"
															targetAction="dialogOEAddLink" id="behavior2"></hx:behavior>
													</hx:commandExButton>
													<hx:commandExButton id="buttonModalOE1"
														styleClass="commandExButton" type="reset" value="Cancel">
														<hx:behavior event="onclick" behaviorAction="hide;stop"
															id="behavior1" targetAction="dialogOEAddLink"></hx:behavior>
													</hx:commandExButton>
												</h:panelGroup>
											</hx:panelDialog>
											<br>
											<br>
											<br>
											<hx:panelDialog type="modeless" styleClass="panelDialog"
												id="dialogCSAddLink" valign="middle" title="Add New Link" for="buttonAddNewCSLink">
												<hx:panelFormBox helpPosition="over" labelPosition="left"
													styleClass="panelFormBox" id="formBox2">
													<hx:formItem styleClass="formItem" id="formItemCSUrl"
														label="URL:"
														infoText="May be a relative url beginning with a '/' or a full URL beginning with 'http'.">
														<h:inputText styleClass="inputText" id="text33"
															value="#{genericFormBean2.valueString1}"></h:inputText>

													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formItemDisplayName2"
														label="Display Name">
														<h:inputText styleClass="inputText" id="text11"
															value="#{genericFormBean2.valueString2}"></h:inputText>
													</hx:formItem>
													<hx:formItem styleClass="formItem" id="formItemdisplayOptoin2" label="Open Link In:">
														<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
															enabledClass="selectOneRadio_Enabled"
															styleClass="selectOneRadio" id="radioOpenInOptoin1"
															value="#{genericFormBean2.valueString3}">
															<f:selectItem itemLabel="New Window" itemValue="_blank"
																id="selectItem11" />
															<f:selectItem itemLabel="Same Window" itemValue=""
																id="selectItem22" />
														</h:selectOneRadio>
													</hx:formItem>
												</hx:panelFormBox>
												<h:panelGroup id="group2" styleClass="panelDialog_Footer">
													<hx:commandExButton id="buttonModalCS2"
														styleClass="commandExButton" type="submit" value="OK" action="#{pc_DynamicNavDetail.doButtonModalCS2Action}">
														<hx:behavior event="onclick" behaviorAction="hide"
															targetAction="dialogCSAddLink" id="behavior4"></hx:behavior>
													</hx:commandExButton>
													<hx:commandExButton id="buttonModalCS1"
														styleClass="commandExButton" type="reset" value="Cancel">
														<hx:behavior event="onclick" behaviorAction="hide;stop"
															id="behavior3" targetAction="dialogCSAddLink"></hx:behavior>
													</hx:commandExButton>
												</h:panelGroup>
											</hx:panelDialog>
											<br>
											<br>
											<br>
										</h:form></hx:scriptCollector>
								<%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>