<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/landingpages/WelcomePage.java" --%><%-- /jsf:pagecode --%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@taglib uri="http://www.ibm.com/jsf/rte" prefix="r"%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
					<link rel="stylesheet" href="${pageContext.request.contextPath}/theme/tabpanel.css" type="text/css">
			<link rel="stylesheet" type="text/css"
				href="${pageContext.request.contextPath}/theme/horizontal-sep.css">
				
			<TITLE>USA TODAY:  Welcome Page</TITLE>
			
		<script language="JavaScript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader(thisObj);

}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader2(thisObj);

}


function func_3(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader3(thisObj);

}


function func_4(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader4(thisObj);

}


function func_6(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader6(thisObj);

}




function func_8(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader8(thisObj);

}



function func_21(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader21(thisObj);

}


function func_31(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader31(thisObj);

}



function func_41(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader41(thisObj);

}


function func_51(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader51(thisObj);

}


</script>
			<script language="JavaScript">
function updateDisplayedInnerTabPanelHeader(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell');
	panelCtrl.restoreUIState(panel);
 }
}

function updateDisplayedInnerTabPanelHeader2(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig2');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell2');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell2');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell2');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell2');
	panelCtrl.restoreUIState(panel);
 }
}
			
		
			
			
			
function updateDisplayedInnerTabPanelHeader3(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig3');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell3');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell3');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell3');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell3');
	panelCtrl.restoreUIState(panel);
 }
}
		
		
			
function updateDisplayedInnerTabPanelHeader4(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig4');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell4');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell4');
	panelCtrl.restoreUIState(panel);
 }
}			




			
function updateDisplayedInnerTabPanelHeader6(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig6');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell6');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell6');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell6');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell6');
	panelCtrl.restoreUIState(panel);
 }
}			



	
	
function updateDisplayedInnerTabPanelHeader8(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig8');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell8');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell8');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell8');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell8');
	panelCtrl.restoreUIState(panel);
 }
}	
	
	
	
function updateDisplayedInnerTabPanelHeader21(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig11');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell11');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell11');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell11');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell11');
	panelCtrl.restoreUIState(panel);
 }
}	
		
	
	
	
function updateDisplayedInnerTabPanelHeader31(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig31');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell31');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell31');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell31');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell31');
	panelCtrl.restoreUIState(panel);
 }
}	
			
	
	
function updateDisplayedInnerTabPanelHeader41(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig41');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell41');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell41');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell41');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell41');
	panelCtrl.restoreUIState(panel);
 }
}		

	
function updateDisplayedInnerTabPanelHeader51(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig51');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell51');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell51');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell51');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell51');
	panelCtrl.restoreUIState(panel);
 }
}	




			</script>			
			
			
			
			
				
			
			
			
			

			
			<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%><hx:scriptCollector id="scriptCollector1">
									<h:form styleClass="form" id="formWelcomePage"><h:panelGrid styleClass="panelGrid" id="gridMainGrid"
											columns="2" cellpadding="2" cellspacing="2">
											<hx:panelBox styleClass="panelBox" id="boxPanelLeft">
													<odc:tabbedPanel slantActiveRight="4"
														showBackNextButton="false" slantInactiveRight="4"
														styleClass="tabbedPanel" width="630" showTabs="true"
														variableTabLength="false" height="370" id="tabbedPanel1">
														<odc:bfPanel id="bfpanel1"
															name="USA TODAY (#{utLandingPages.keycode })"
															showFinishCancelButton="false">
															<odc:tabbedPanel slantActiveRight="4"
																showBackNextButton="false" slantInactiveRight="4"
																styleClass="tabbedPanel" width="100%" showTabs="true"
																variableTabLength="false" id="tabbedPanel2"
																height="100%">
																<odc:bfPanel id="bfpanel4" name="Header Text"
																	showFinishCancelButton="false">




																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid" cellpadding="2"
																		columns="1" width="100%">
																		<r:inputRichText width="100%" height="200"
																			id="richTextEditor1"
																			value="#{welcomePageHandler.utHeaderText}"></r:inputRichText>

																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group1">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader"
																					value="UT Header Text (Max 1000 Characters)"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																	</h:panelGrid>








																</odc:bfPanel>
																<odc:bfPanel id="bfpanel3" name="400x200 Image"
																	showFinishCancelButton="false">


																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrida" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group1a">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeadera"
																					value="400wx200h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioUt400x200"
																			value="#{welcomePageHandler.ut400x200ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_1(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>

																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig" height="100%">
																		<odc:bfPanel id="bfpanelDefaultUpSell" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group3">
																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageUt400x200" align="middle"
																							value="#{utLandingPages.campaign.promotionSet.largePromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{utLandingPages.campaign.promotionSet.largePromoImageLandingPage.imageFileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.ut400x200Image.imageContents}">
																								<hx:fileProp name="fileName"
																									value="#{welcomePageHandler.ut400x200Image.imageFileName}" />
																								<hx:fileProp name="contentType"
																									value="#{welcomePageHandler.ut400x200Image.imageFileType}" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL" size="45"
																								value="#{welcomePageHandler.ut400x200Image.linkURL}"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText" size="45"
																								value="#{welcomePageHandler.ut400x200Image.alternateText}"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>





																</odc:bfPanel>
																<odc:bfPanel id="bfpanel8" name="400x100 Image"
																	showFinishCancelButton="false">


																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid4" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group4">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader4"
																					value="400wx100h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioUt400x100"
																			value="#{welcomePageHandler.ut400x100ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_4(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>


																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig4">
																		<odc:bfPanel id="bfpanelDefaultUpSell4" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage4" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group7">

																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageUt400x100" align="middle"
																							value="#{utLandingPages.campaign.promotionSet.smallPromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{utLandingPages.campaign.promotionSet.smallPromoImageLandingPage.imageFileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft4" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel4"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell4" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage4">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel4"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell4" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout4"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage4" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem4"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload4" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.ut400x100Image.imageContents}">
																								<hx:fileProp name="fileName"
																									value="#{welcomePageHandler.ut400x100Image.imageFileName}" />
																								<hx:fileProp name="contentType"
																									value="#{welcomePageHandler.ut400x100Image.imageFileType}" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload4"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL4"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL4" size="45"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText4"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText4" size="45"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel4"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>
																</odc:bfPanel>
																<odc:bfPanel id="bfpanel7" name="180x90 Image"
																	showFinishCancelButton="false">




																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid6" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group16">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader6"
																					value="180wx90h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioUt180x90"
																			value="#{welcomePageHandler.ut180x90ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_6(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>







																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig6">
																		<odc:bfPanel id="bfpanelDefaultUpSell6" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage6" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group6">
																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageUt180x90" align="middle"
																							value="#{utLandingPages.campaign.promotionSet.rightColumnUpperPromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{utLandingPages.campaign.promotionSet.rightColumnUpperPromoImageLandingPage.imageContents}FileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft6" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel6"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell6" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage6">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel6"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell6" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout6"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage6" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem6"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload6" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.ut180x90Image.imageContents}">
																								<hx:fileProp name="fileName"
																									value="#{welcomePageHandler.ut180x90Image.imageFileName}" />
																								<hx:fileProp name="contentType"
																									value="#{welcomePageHandler.ut180x90Image.imageFileType}" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload6"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL6"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL6" size="45"
																								value="#{welcomePageHandler.ut180x90Image.linkURL}"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText6"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText6" size="45"
																								value="#{welcomePageHandler.ut180x90Image.alternateText}"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back6"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next6"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish6"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel6"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>









																</odc:bfPanel>
																<odc:bfPanel id="bfpanel6" name="180x120 Image"
																	showFinishCancelButton="false">




																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid8" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group18">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader8"
																					value="180wx120h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioUt180x120"
																			value="#{welcomePageHandler.ut180x120ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_8(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>


																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig8">
																		<odc:bfPanel id="bfpanelDefaultUpSell8" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage8" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group5">
																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageUt180x120" align="middle"
																							value="#{utLandingPages.campaign.promotionSet.rightColumnLowerPromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{utLandingPages.campaign.promotionSet.rightColumnLowerPromoImageLandingPage.imageContents}FileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft8" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel8"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell8" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage8">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel8"
																					value="No image will be displayed to the right of the order summary information."
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell8" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout8"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage8" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem8"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload8" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.ut180x120Image.imageContents}">
																								<hx:fileProp name="fileName"
																									value="#{welcomePageHandler.ut180x120Image.imageFileName}" />
																								<hx:fileProp name="contentType"
																									value="#{welcomePageHandler.ut180x120Image.imageFileType}" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload8"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL8"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL8" size="45"
																								value="#{welcomePageHandler.ut180x120Image.linkURL}"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText8"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText8" size="45"
																								value="#{welcomePageHandler.ut180x120Image.alternateText}"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back8"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next8"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish8"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel8"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>














																</odc:bfPanel>
																<f:facet name="back">
																	<hx:commandExButton type="submit" value="&lt; Back"
																		id="tabbedPanel2_back" style="display:none"></hx:commandExButton>
																</f:facet>
																<f:facet name="next">
																	<hx:commandExButton type="submit" value="Next &gt;"
																		id="tabbedPanel2_next" style="display:none"></hx:commandExButton>
																</f:facet>
																<f:facet name="finish">
																	<hx:commandExButton type="submit" value="Finish"
																		id="tabbedPanel2_finish" style="display:none"></hx:commandExButton>
																</f:facet>
																<f:facet name="cancel">
																	<hx:commandExButton type="submit" value="Cancel"
																		id="tabbedPanel2_cancel" style="display:none"></hx:commandExButton>
																</f:facet>
															</odc:tabbedPanel>

														</odc:bfPanel>
														<odc:bfPanel id="bfpanel2"
															name="SPORTS WEEKLY (#{swLandingPages.keycode })"
															showFinishCancelButton="false">
															<odc:tabbedPanel slantActiveRight="4"
																showBackNextButton="false" slantInactiveRight="4"
																styleClass="tabbedPanel" width="100%" showTabs="true"
																variableTabLength="false" height="100%"
																id="tabbedPanel3">
																<odc:bfPanel id="bfpanel5" name="Header Text"
																	showFinishCancelButton="false">




																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid71" cellpadding="2"
																		columns="1" width="100%">
																		<r:inputRichText width="100%" height="205"
																			id="richTextEditor2"
																			value="#{welcomePageHandler.swHeaderText}"></r:inputRichText>

																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group71">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader71"
																					value="SW Header Text (Max 1000 Characters)"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																	</h:panelGrid>







																</odc:bfPanel>
																<odc:bfPanel id="bfpanel9" name="400x200 Image"
																	showFinishCancelButton="false">





																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid11" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group111">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader11a"
																					value="400wx200h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioSw400x200"
																			value="#{welcomePageHandler.sw400x200ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_21(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>



																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig11">
																		<odc:bfPanel id="bfpanelDefaultUpSell11" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage11" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group8">
																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageSw400x200" align="middle"
																							value="#{swLandingPages.campaign.promotionSet.largePromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{swLandingPages.campaign.promotionSet.largePromoImageLandingPage.imageFileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft11" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel11"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell11" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage11">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel11"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell11" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout11"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage11" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem11"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload11" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.sw400x200Image.imageContents}">
																								<hx:fileProp name="fileName"
																									value="#{welcomePageHandler.sw400x200Image.imageFileName}" />
																								<hx:fileProp name="contentType"
																									value="#{welcomePageHandler.sw400x200Image.imageFileType}" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload11"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL11"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL11" size="45"
																								value="#{welcomePageHandler.sw400x200Image.linkURL}"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText11"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText11" size="45"
																								value="#{welcomePageHandler.sw400x200Image.alternateText}"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back11"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next11"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish11"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel11"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>










																</odc:bfPanel>
																<odc:bfPanel id="bfpanel10" name="400x100 Image"
																	showFinishCancelButton="false">



																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid31" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group131">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader31"
																					value="400wx100h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioSw400x100"
																			value="#{welcomePageHandler.sw400x100ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_31(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>


																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig31">
																		<odc:bfPanel id="bfpanelDefaultUpSell31" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage31" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group9">
																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageSw400x100" align="middle"
																							value="#{swLandingPages.campaign.promotionSet.smallPromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{swLandingPages.campaign.promotionSet.smallPromoImageLandingPage.imageFileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft31" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel31"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell31" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage31">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel31"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell31" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout31"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage31" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem31"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload31" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.sw400x100Image.imageContents}">
																								<hx:fileProp name="fileName" />
																								<hx:fileProp name="contentType" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload31"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL31"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL31" size="45"
																								value="#{welcomePageHandler.sw400x100Image.linkURL}"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText31"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText31" size="45"
																								value="#{welcomePageHandler.sw400x100Image.alternateText}"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back31"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next31"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish31"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel31"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>

















																</odc:bfPanel>
																<odc:bfPanel id="bfpanel11" name="180x90 Image"
																	showFinishCancelButton="false">



																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid41" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group41">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader41"
																					value="180wx90h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioSw180x90"
																			value="#{welcomePageHandler.sw180x90ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_41(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>






																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig41">
																		<odc:bfPanel id="bfpanelDefaultUpSell41" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage41" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group10">
																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageSw180x90" align="middle"
																							value="#{swLandingPages.campaign.promotionSet.rightColumnUpperPromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{swLandingPages.campaign.promotionSet.rightColumnUpperPromoImageLandingPage.imageFileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft41" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel41"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell41" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage41">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel41"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell41" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout41"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage41" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem41"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload41" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.sw180x90Image.imageContents}">
																								<hx:fileProp name="fileName"
																									value="#{welcomePageHandler.sw180x90Image.imageFileName}" />
																								<hx:fileProp name="contentType"
																									value="#{welcomePageHandler.sw180x90Image.imageFileType}" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload41"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL41"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL41" size="45"
																								value="#{welcomePageHandler.sw180x90Image.linkURL}"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText41"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText41" size="45"
																								value="#{welcomePageHandler.sw180x90Image.alternateText}"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back41"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next41"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish41"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel41"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>















																</odc:bfPanel>
																<odc:bfPanel id="bfpanel12" name="180x120 Image"
																	showFinishCancelButton="false">


																	<h:panelGrid styleClass="panelGrid"
																		id="gridMainUpSellImageOptionGrid51" cellpadding="2"
																		columns="1">
																		<f:facet name="header">
																			<h:panelGroup styleClass="panelGroup" id="group51a">
																				<h:outputText styleClass="outputText_Med"
																					id="textUpSellImageHeader51"
																					value="180wx120h Pixel Image"
																					style="font-weight: bold; font-size: 14pt"></h:outputText>
																			</h:panelGroup>
																		</f:facet>

																		<h:selectOneRadio
																			disabledClass="selectOneRadio_Disabled"
																			enabledClass="selectOneRadio_Enabled"
																			styleClass="selectOneRadio" id="radioSw180x120"
																			value="#{welcomePageHandler.sw180x120ImageOption}"
																			style="font-weight: bold"
																			onclick="return func_51(this, event);">
																			<f:selectItem itemLabel="Current Image Selection"
																				itemValue="default" />
																			<f:selectItem itemLabel="No Image" itemValue="none" />
																			<f:selectItem itemLabel="Upload Image"
																				itemValue="custom" />
																		</h:selectOneRadio>

																	</h:panelGrid>





																	<odc:tabbedPanel slantActiveRight="4"
																		showBackNextButton="false" slantInactiveRight="4"
																		styleClass="tabbedPanel_2" width="100%"
																		showTabs="false" variableTabLength="true"
																		id="tabbedPanelInnerUpSellConfig51">
																		<odc:bfPanel id="bfpanelDefaultUpSell51" name=""
																			showFinishCancelButton="false">
																			<h:panelGrid styleClass="panelGrid"
																				id="gridCustomImageDefaultImage51" cellpadding="1"
																				cellspacing="1" columns="2">
																				<f:facet name="footer">
																					<h:panelGroup styleClass="panelGroup" id="group11">
																						<hx:graphicImageEx styleClass="graphicImageEx"
																							id="imageSw180x120" align="middle"
																							value="#{swLandingPages.campaign.promotionSet.rightColumnLowerPromoImageLandingPage.imageContents}"
																							hspace="10" vspace="1" border="0"
																							mimeType="#{swLandingPages.campaign.promotionSet.rightColumnLowerPromoImageLandingPage.imageFileType}"></hx:graphicImageEx>
																					</h:panelGroup>
																				</f:facet>

																				<hx:panelBox styleClass="panelBox"
																					id="boxCurrentDefaultPanelBoxLeft51" height="100%"
																					valign="top">
																					<h:outputText styleClass="outputText_Med"
																						id="textCustomUpSellImageDefaultLabel51"
																						value="Current Image Selection:"></h:outputText>
																				</hx:panelBox>


																			</h:panelGrid>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelNoUpSell51" name=""
																			showFinishCancelButton="false">
																			<hx:jspPanel id="jspPanelInnerCustomrNoImage51">
																				<br>
																				<br>
																				<h:outputText styleClass="outputText_Med"
																					id="textInnerUpSellNoImageLabel51"
																					value="No image will be displayed. "
																					style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																				<br>
																				<br>
																				<br>
																			</hx:jspPanel>
																		</odc:bfPanel>
																		<odc:bfPanel id="bfpanelCustomUpSell51" name=""
																			showFinishCancelButton="false">
																			<hx:panelLayout styleClass="panelLayout"
																				id="layoutInnerCustomUpSellImageImageLayout51"
																				width="100%">
																				<f:facet name="body">
																					<hx:panelFormBox helpPosition="over"
																						labelPosition="left" styleClass="panelFormBox"
																						id="formBoxCustomImage51" widthLabel="125">
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageFormItem51"
																							label="Upload Image:"
																							infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																							<hx:fileupload styleClass="fileupload"
																								id="fileuploadCustomImageUpload51" size="40"
																								accept="image/*" title="Custom Image Upload"
																								value="#{welcomePageHandler.sw180x120Image.imageContents}">
																								<hx:fileProp name="fileName"
																									value="#{welcomePageHandler.sw180x120Image.imageFileName}" />
																								<hx:fileProp name="contentType"
																									value="#{welcomePageHandler.sw180x120Image.imageFileType}" />
																							</hx:fileupload>
																							<h:message
																								for="formItemCustomUpSellImageFormItem"
																								id="errorMsgCustomFileUpload51"></h:message>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellOnClickURL51"
																							label="On Click URL:"
																							infoText="Specify the URL to go to when this image is clicked.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellOnClickURL51" size="45"
																								value="#{welcomePageHandler.sw180x120Image.linkURL}"></h:inputText>
																						</hx:formItem>
																						<hx:formItem styleClass="formItem"
																							id="formItemCustomUpSellImageAltText51"
																							label="Alternate Text:"
																							infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																							<h:inputText styleClass="inputText"
																								id="textCustomUpSellImageAltText51" size="45"
																								value="#{welcomePageHandler.sw180x120Image.alternateText}"></h:inputText>
																						</hx:formItem>
																					</hx:panelFormBox>
																				</f:facet>
																				<f:facet name="left"></f:facet>
																				<f:facet name="right"></f:facet>
																				<f:facet name="bottom"></f:facet>
																				<f:facet name="top"></f:facet>
																			</hx:panelLayout>
																		</odc:bfPanel>
																		<f:facet name="back">
																			<hx:commandExButton type="submit" value="&lt; Back"
																				id="tabbedPanelInnerUpSellConfig_back51"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="next">
																			<hx:commandExButton type="submit" value="Next &gt;"
																				id="tabbedPanelInnerUpSellConfig_next51"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="finish">
																			<hx:commandExButton type="submit" value="Finish"
																				id="tabbedPanelInnerUpSellConfig_finish51"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																		<f:facet name="cancel">
																			<hx:commandExButton type="submit" value="Cancel"
																				id="tabbedPanelInnerUpSellConfig_cancel51"
																				style="display:none"></hx:commandExButton>
																		</f:facet>
																	</odc:tabbedPanel>






																</odc:bfPanel>
																<f:facet name="back">
																	<hx:commandExButton type="submit" value="&lt; Back"
																		id="tabbedPanel3_back" style="display:none"></hx:commandExButton>
																</f:facet>
																<f:facet name="next">
																	<hx:commandExButton type="submit" value="Next &gt;"
																		id="tabbedPanel3_next" style="display:none"></hx:commandExButton>
																</f:facet>
																<f:facet name="finish">
																	<hx:commandExButton type="submit" value="Finish"
																		id="tabbedPanel3_finish" style="display:none"></hx:commandExButton>
																</f:facet>
																<f:facet name="cancel">
																	<hx:commandExButton type="submit" value="Cancel"
																		id="tabbedPanel3_cancel" style="display:none"></hx:commandExButton>
																</f:facet>
															</odc:tabbedPanel>
														</odc:bfPanel>
														<f:facet name="back">
															<hx:commandExButton type="submit" value="&lt; Back"
																id="tabbedPanel1_back" style="display:none"></hx:commandExButton>
														</f:facet>
														<f:facet name="next">
															<hx:commandExButton type="submit" value="Next &gt;"
																id="tabbedPanel1_next" style="display:none"></hx:commandExButton>
														</f:facet>
														<f:facet name="finish">
															<hx:commandExButton type="submit" value="Finish"
																id="tabbedPanel1_finish" style="display:none"></hx:commandExButton>
														</f:facet>
														<f:facet name="cancel">
															<hx:commandExButton type="submit" value="Cancel"
																id="tabbedPanel1_cancel" style="display:none"></hx:commandExButton>
														</f:facet>
														<odc:buttonPanel alignContent="left" id="bp1">

															<hx:commandExButton type="submit"
																value="Publish These Campaigns"
																styleClass="commandExButton" id="buttonPublishChanges"
																action="#{pc_WelcomePage.doButtonPublishChangesAction}"></hx:commandExButton>
															<hx:commandExButton type="submit" value="Save"
																styleClass="commandExButton" id="button1"
																action="#{pc_WelcomePage.doButtonSaveChangesAction}"></hx:commandExButton>

														</odc:buttonPanel>
													</odc:tabbedPanel>





												</hx:panelBox>
											<hx:panelBox styleClass="panelBox" id="boxPanelRight">



													<h:panelGrid styleClass="panelGrid" id="grid1">
														<h:outputText styleClass="outputText_Small"
															id="textCurrentHeaderImageLabel" value="UT Header Text:"
															style="text-decoration: underline; font-weight: bold"></h:outputText>
														<f:facet name="footer">
															<h:panelGroup styleClass="panelGroup" id="group12">
																<h:panelGrid styleClass="panelGrid" id="grid2">
																	<h:outputText styleClass="outputText_Small"
																		id="textCurrentNavImageLabel" value="SW Header Text:"
																		style="background-color: white; text-decoration: underline; font-weight: bold; border-color: white; border-width: thin; border-style: none"></h:outputText>

																	<f:facet name="footer">
																		<h:panelGroup styleClass="panelGroup" id="group14">
																			<h:outputText styleClass="outputText" id="text2"
																				value="#{swLandingPages.campaign.promotionSet.promoLandingPageText.promotionalHTML}"
																				escape="false"
																				style="border-color: white; border-style: none; border-width: thin"></h:outputText>
																		</h:panelGroup>
																	</f:facet>
																	<f:facet name="header">
																		<h:panelGroup styleClass="panelGroup" id="group13">
																			<h:outputText styleClass="outputText" id="text1"
																				value="#{utLandingPages.campaign.promotionSet.promoLandingPageText.promotionalHTML}"
																				escape="false"></h:outputText>

																		</h:panelGroup>
																	</f:facet>
																</h:panelGrid>
															</h:panelGroup>
														</f:facet>
														<f:facet name="header">
															<h:panelGroup styleClass="panelGroup" id="group2">
																<h:outputText styleClass="outputText_Med"
																	id="textCurrentSettingLabel"
																	value="Current Header Text Settings"
																	style="text-decoration: underline overline; font-weight: bold;"></h:outputText>
															</h:panelGroup>
														</f:facet>
													</h:panelGrid>
											</hx:panelBox>
											<f:facet name="header">
												<h:panelGroup styleClass="panelGroup" id="group1A">
														<h:outputText styleClass="outputText_Large"
															id="textWelcomePage"
															value="Customize Welcome Page Images and Text              "></h:outputText>

														<hx:outputLinkEx styleClass="outputLinkEx" id="linkExMockUpLink"
											value="/portaladmin/secured/campaigns/details/mockup/index.htm"
											target="_blank">
											<h:outputText id="textMockUpLinkLabel" styleClass="outputText"
												value="            Open Subscribe Page Mockup" style="margin: 40px"></h:outputText>
										</hx:outputLinkEx>
												
												</h:panelGroup>
											</f:facet>
										</h:panelGrid></h:form>
								</hx:scriptCollector>
							<%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>