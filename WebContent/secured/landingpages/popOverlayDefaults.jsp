<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/landingpages/PopOverlayDefaults.java" --%><%-- /jsf:pagecode --%>
<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>
			<link rel="stylesheet"
				href="${pageContext.request.contextPath}/theme/tabpanel.css"
				type="text/css">
			<link rel="stylesheet" type="text/css"
				href="${pageContext.request.contextPath}/theme/horizontal-sep.css">

			<title>Subscription Path Overlay Defaults</title>
			<script language="JavaScript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader(thisObj.value);

}

function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
updateDisplayedInnerTabPanelHeader2(thisObj);

}


function func_3(thisObj, thisEvent) {
//Use 'thisObj' to refer directly to this component instead of keyword 'this'
//Use 'thisEvent' to refer to the event generated instead of keyword 'event'

//'thisObj' is the object that represents the tabbed panel component.
//This event handler will be called before displaying the tabbed panel.
//If the id of a panel is returned then that will be initial panel displayed, if not the first panel will be displayed.

}</script>
<script language="JavaScript">
function updateDisplayedInnerTabPanelHeader(radioBtnVal) {
 
 var selectedValue = radioBtnVal;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelUTInnerPopOverlayConfig');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelUTNoImage');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUTSubscribeOverlay');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelUTNoImage');
	panelCtrl.restoreUIState(panel);
 }
}
	
		
function updateDisplayedInnerTabPanelHeader2(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerUpSellConfig2');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell2');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoUpSell2');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomUpSell2');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelDefaultUpSell2');
	panelCtrl.restoreUIState(panel);
 }
}
</script>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>
							<hx:scriptCollector id="scriptCollector1" preRender="#{pc_PopOverlayDefaults.onPageLoadBegin}">
								<h:form styleClass="form" id="form1">
									<h:messages styleClass="messages" id="messages1"></h:messages>

									<h:panelGrid styleClass="panelGrid" id="grid7" cellpadding="2" cellspacing="2" columns="2">
										
										<odc:tabbedPanel slantActiveRight="4" showBackNextButton="false" slantInactiveRight="4" styleClass="tabbedPanel" width="630" showTabs="true" variableTabLength="false" height="350" id="tabbedPanel1">
											<odc:bfPanel id="bfpanel1" name="USA TODAY" showFinishCancelButton="false">

												<h:panelGrid styleClass="panelGrid" id="gridSubscribePathOverlayImageOptionGrid" cellpadding="2" columns="1">
													<f:facet name="header">
														<h:panelGroup styleClass="panelGroup" id="group1">
															<h:outputText styleClass="outputText_Med" id="textUpSellImageHeader" value="UT Brand Default Subscribe Path PopUp Overlay" style="font-weight: bold; font-size: 14pt"></h:outputText>
														</h:panelGroup>
													</f:facet>
												<h:selectOneRadio disabledClass="selectOneRadio_Disabled" enabledClass="selectOneRadio_Enabled" styleClass="selectOneRadio" id="radioUTOverlayImage" style="font-weight: bold" onclick="return func_1(this, event);" value="#{coverGraphicsHandler.utSubscribePathPopUpOverlayImageOption}">
														<f:selectItem itemLabel="Set To No Pop Up (Hide)"
															itemValue="none" />
														<f:selectItem itemLabel="Change PopUp Overlay Image"
															itemValue="custom" />
													</h:selectOneRadio>

												</h:panelGrid>



												<odc:tabbedPanel slantActiveRight="4" showBackNextButton="false" slantInactiveRight="4" styleClass="tabbedPanel_2" width="600" showTabs="false" variableTabLength="true" id="tabbedPanelUTInnerPopOverlayConfig" height="240" oninitialpanelshow="return func_3(this, event);">
													<odc:bfPanel id="bfpanelUTNoImage" name=""
														showFinishCancelButton="false">
																										<hx:jspPanel id="jspPanelInnerHeaderNoImage">
														<br>
														<br>
															<h:outputText styleClass="outputText_Med"
																id="textInnerHeaderNoImageLabel"
																value="No Pop Up Overlay will be displayed for any offer, unless set up at the keycode level."
																style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
															<br>
														<br>
														<br>
													</hx:jspPanel>
	
													</odc:bfPanel>
													<odc:bfPanel id="bfpanelCustomUTSubscribeOverlay" name="" showFinishCancelButton="false">
														<hx:panelLayout styleClass="panelLayout" id="layoutInnerUTCustomSubscribePathOverlayImageImageLayout" width="100%">
															<f:facet name="body">
																<hx:panelFormBox helpPosition="over" labelPosition="left" styleClass="panelFormBox" id="formBoxCustomImage" widthLabel="125">
																	<hx:formItem styleClass="formItem" id="formItemCustomUTSubscribeOverlayImageFormItem" label="Upload Image:" infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																		<hx:fileupload styleClass="fileupload" id="fileuploadUTImageUpload" size="40" accept="image/*" title="Custom Image Upload" value="#{coverGraphicsHandler.UTSubscribePathOverlayGraphicImage.imageContents}">
																			<hx:fileProp name="fileName" value="#{coverGraphicsHandler.UTSubscribePathOverlayGraphicImage.imageFileName}" />
																			<hx:fileProp name="contentType" value="#{coverGraphicsHandler.UTSubscribePathOverlayGraphicImage.imageFileType}" />
																		</hx:fileupload>
																		<h:message for="fileuploadUTImageUpload" id="errorMsgCustomFileUpload"></h:message>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem" id="formItemSubscribePathOverlayOnClickURL" label="On Click URL:" infoText="Specify the URL to go to when this image is clicked.">
																		<h:inputText styleClass="inputText" id="textUTSubscribePathOverlayOnClickURL" size="45" value="#{coverGraphicsHandler.UTSubscribePathOverlayGraphicImage.linkURL}"></h:inputText>
																	</hx:formItem>
																	<hx:formItem styleClass="formItem" id="formItemCustomUTSubscribePathOverlayImageAltText" label="Alternate Text:" infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																		<h:inputText styleClass="inputText" id="textUTSubscribePathOverlayImageAltText" size="45" value="#{coverGraphicsHandler.UTSubscribePathOverlayGraphicImage.alternateText}"></h:inputText>
																	</hx:formItem>
																</hx:panelFormBox>
															</f:facet>
															<f:facet name="left"></f:facet>
															<f:facet name="right"></f:facet>
															<f:facet name="bottom"></f:facet>
															<f:facet name="top"></f:facet>
														</hx:panelLayout>
													</odc:bfPanel>
													<f:facet name="back">
														<hx:commandExButton type="submit" value="&lt; Back" id="tabbedPanelInnerUpSellConfig_back" style="display:none"></hx:commandExButton>
													</f:facet>
													<f:facet name="next">
														<hx:commandExButton type="submit" value="Next &gt;" id="tabbedPanelInnerUpSellConfig_next" style="display:none"></hx:commandExButton>
													</f:facet>
													<f:facet name="finish">
														<hx:commandExButton type="submit" value="Finish" id="tabbedPanelInnerUpSellConfig_finish" style="display:none"></hx:commandExButton>
													</f:facet>
													<f:facet name="cancel">
														<hx:commandExButton type="submit" value="Cancel" id="tabbedPanelInnerUpSellConfig_cancel" style="display:none"></hx:commandExButton>
													</f:facet>
												</odc:tabbedPanel>








											</odc:bfPanel>
											<odc:bfPanel id="bfpanel2" name="SPORTS WEEKLY (not implemented)" showFinishCancelButton="false">




												<h:panelGrid styleClass="panelGrid" id="gridSWImageOptionGrid2" cellpadding="2" columns="1">
													<h:outputText styleClass="outputText" id="text2" value="Future - Not Implemented"></h:outputText>
													<f:facet name="header">
													</f:facet>

												</h:panelGrid>























												
											</odc:bfPanel>
											<f:facet name="back">
												<hx:commandExButton type="submit" value="&lt; Back" id="tabbedPanel1_back" style="display:none"></hx:commandExButton>
											</f:facet>
											<f:facet name="next">
												<hx:commandExButton type="submit" value="Next &gt;" id="tabbedPanel1_next" style="display:none"></hx:commandExButton>
											</f:facet>
											<f:facet name="finish">
												<hx:commandExButton type="submit" value="Finish" id="tabbedPanel1_finish" style="display:none"></hx:commandExButton>
											</f:facet>
											<f:facet name="cancel">
												<hx:commandExButton type="submit" value="Cancel" id="tabbedPanel1_cancel" style="display:none"></hx:commandExButton>
											</f:facet>
											<odc:buttonPanel alignContent="right" id="bp1">

												<hx:commandExButton type="submit"
													value="Publish These Campaigns"
													styleClass="commandExButton" id="buttonPublishCampaigns" action="#{pc_PopOverlayDefaults.doButtonPublishCampaignsAction}"></hx:commandExButton>

												<hx:commandExButton type="submit" value="Save" styleClass="commandExButton" id="buttonSaveChanges" action="#{pc_PopOverlayDefaults.doButtonSaveChangesAction}">

												</hx:commandExButton>


											</odc:buttonPanel>
										</odc:tabbedPanel>
										<f:facet name="header">
											<h:panelGroup styleClass="panelGroup" id="group21"
												style="height: 30px; width: 100%; text-align: left">
												<h:outputText styleClass="outputText_Large" id="text1"
													value="Customize Subscribe Path PopUp Overlay            "
													style="text-align: left"></h:outputText>
											</h:panelGroup>
										</f:facet>
										<hx:panelBox styleClass="panelBox" id="box1" valign="top">
										
											<h:panelGrid styleClass="panelGrid" id="gridCurrentSettings" columns="1" cellspacing="2" cellpadding="2" style="vertical-align: top">
								<f:facet name="header">
									<h:panelGroup styleClass="panelGroup3" id="grou94">
										<h:outputText styleClass="outputText_Med" id="textCurrentSettingLabel" value="Current Settings" style="text-decoration: underline overline; font-weight: bold;"></h:outputText>
									</h:panelGroup>
								</f:facet>
									<h:outputText styleClass="outputText" id="textCurrentUTOverlayImageLabel" value="UT Brand Default Overlay Graphic:" style="font-weight: bold"></h:outputText>

												<hx:graphicImageEx styleClass="graphicImageEx"
													id="imageUtDefaultRightPanel" title="Links to: #{utLandingPages.campaign.promotionSet.newSubscriptionOrderPopUpOverlay.imageLinkToURL}" value="#{utLandingPages.promotionSetHandler.newOrderPathPopOverlayPath}"></hx:graphicImageEx>
												<h:outputText styleClass="outputText"
													id="textNoUTOverlayImage"
													value="No Default PopUp Overlay Configured"
													style="color: maroon; font-weight: bold" rendered="false"></h:outputText>
												<h:outputText styleClass="outputText" id="textCurrentNavImageLabel" value="SW Brand Default Overlay Graphic:" style="font-weight: bold" rendered="false"></h:outputText>

							</h:panelGrid>
										
										
										
										</hx:panelBox>
									</h:panelGrid>


								</h:form>
								
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>

							</hx:scriptCollector>
						<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>
