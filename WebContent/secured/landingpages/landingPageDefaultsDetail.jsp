<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/landingpages/LandingPageDefaultsDetail.java" --%><%-- /jsf:pagecode --%>

<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%>
<%@taglib uri="http://www.ibm.com/jsf/rte" prefix="r"%><%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Landing Page Defaults</TITLE>
				<link rel="stylesheet" type="text/css" title="Style"
					href="../../theme/tabpanel.css">
				<link rel="stylesheet" type="text/css" title="Style"
					href="../../theme/rte_style.css">
<script language="JavaScript">
function func_1(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDisplayedInnerTabPanelHeader(thisObj);

}

function updateDisplayedInnerTabPanelHeader(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerFooterConfig4');

 //panelCtrl.restoreUIState(panel2);
 //panelCtrl.hideTab(panel3);
 //panelCtrl.disableTab(panel3);

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelFooterCurrent4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoFooter4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomFooter');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelFooterCurrent4');
	panelCtrl.restoreUIState(panel);
 }
}

function func_2(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDisplayedInnerTabPanelHeaderBottomLeft(thisObj);

}

function updateDisplayedInnerTabPanelHeaderBottomLeft(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerImage1Config4');

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelImage1Current4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoImage1');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomImage1');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelImage1Current4');
	panelCtrl.restoreUIState(panel);
 }
}

function func_3(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDisplayedInnerTabPanelHeaderBottomCenter(thisObj);

}

function updateDisplayedInnerTabPanelHeaderBottomCenter(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerImage2Config4');

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelImage2Current4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoImage2');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomImage2');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelImage2Current4');
	panelCtrl.restoreUIState(panel);
 }
}

function func_4(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDisplayedInnerTabPanelHeaderBottomRight(thisObj);

}

function updateDisplayedInnerTabPanelHeaderBottomRight(radioBtn) {
 
 var selectedValue = radioBtn.value;
 
 var panelCtrl = ODCRegistry.getClientControl('tabbedPanelInnerImage3Config4');

 if (selectedValue == null || selectedValue == "default") {
 	var panel = panelCtrl.getClientId('bfpanelImage3Current4');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "none") {
 	var panel = panelCtrl.getClientId('bfpanelNoImage3');
	panelCtrl.restoreUIState(panel);
 }
 else if (selectedValue == "custom") {
 	var panel = panelCtrl.getClientId('bfpanelCustomImage3');
	panelCtrl.restoreUIState(panel);
 }
 else {
 	var panel = panelCtrl.getClientId('bfpanelImage3Current4');
	panelCtrl.restoreUIState(panel);
 }
}
</script>					
			<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%>
									<hx:scriptCollector id="scriptCollector1" preRender="#{pc_LandingPageDefaultsDetail.onPageLoadBegin}"><h:form styleClass="form" id="formLandingPageDefaults">
											<h:messages styleClass="messages" id="messages1"></h:messages>
											<h:panelGrid styleClass="panelGrid" id="gridMainGrid"
												columns="2" cellpadding="2" cellspacing="2" style="text-align: left">
												<hx:panelBox styleClass="panelBox" id="boxPanelLeft"
													height="100%" valign="top">
														<odc:tabbedPanel slantActiveRight="4"
															showBackNextButton="false" slantInactiveRight="4"
															styleClass="tabbedPanel" width="700" showTabs="true"
															variableTabLength="false" height="350" id="tabbedPanel1">
															<odc:bfPanel id="bfpanel3" name="Footer Image"
																showFinishCancelButton="false">

																<h:panelGrid styleClass="panelGrid"
																	id="gridFooterImageOptionGrid8" cellpadding="2"
																	columns="1">
																	<f:facet name="header">
																		<h:panelGroup styleClass="panelGroup"
																			id="groupFooterImage1">
																			<h:outputText styleClass="outputText_Med"
																				id="textFooterImageHeader8"
																				value="Footer Image (Recommend no more than 218 pixel wide and 120 pixels high)"
																				style="font-weight: bold; font-size: 14pt"></h:outputText>
																		</h:panelGroup>
																	</f:facet>

																	<h:selectOneRadio
																		disabledClass="selectOneRadio_Disabled"
																		enabledClass="selectOneRadio_Enabled"
																		styleClass="selectOneRadio" id="radioFooterOpSelect"
																		value="#{welcomePageHandler.footerImageOption}"
																		style="font-weight: bold"
																		onclick="return func_1(this, event);">
																		<f:selectItem itemLabel="Current Image Selection"
																			itemValue="default" />
																		<f:selectItem itemLabel="No Image" itemValue="none" />
																		<f:selectItem itemLabel="Upload Image"
																			itemValue="custom" />
																	</h:selectOneRadio>

																</h:panelGrid>
																<odc:tabbedPanel slantActiveRight="4"
																	showBackNextButton="false" slantInactiveRight="4"
																	styleClass="tabbedPanel_2" width="100%"
																	showTabs="false" variableTabLength="true"
																	id="tabbedPanelInnerFooterConfig4">
																	<odc:bfPanel id="bfpanelFooterCurrent4" name=""
																		showFinishCancelButton="false">
																		<h:panelGrid styleClass="panelGrid"
																			id="gridFooterCustomImageDefaultImage4"
																			cellpadding="1" cellspacing="1" columns="2">
																			<f:facet name="footer">
																				<h:panelGroup styleClass="panelGroup"
																					id="groupFooter7">

																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageFooter" align="middle"
																						value="#{currentDefaultCampaign.promotionSetHandler.landingPageFooterImagePath}"
																						hspace="10" vspace="1" border="0"></hx:graphicImageEx>
																				</h:panelGroup>
																			</f:facet>

																			<hx:panelBox styleClass="panelBox"
																				id="boxCurrentFooterDefaultPanelBoxLeft4"
																				height="100%" valign="top">
																				<h:outputText styleClass="outputText_Med"
																					id="textCustomFooterImageDefaultLabel4"
																					value="Current Image Selection:"></h:outputText>
																			</hx:panelBox>


																		</h:panelGrid>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelNoFooter4" name=""
																		showFinishCancelButton="false">
																		<hx:jspPanel id="jspPanelInnerCustomFooterNoImage4">
																			<br>
																			<br>
																			<h:outputText styleClass="outputText_Med"
																				id="textInnerFooterNoImageLabel4"
																				value="No image will be displayed. "
																				style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																			<br>
																			<br>
																			<br>
																		</hx:jspPanel>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelCustomFooter" name=""
																		showFinishCancelButton="false">
																		<hx:panelLayout styleClass="panelLayout"
																			id="layoutInnerCustomFooterImageImageLayout4"
																			width="100%">
																			<f:facet name="body">
																				<hx:panelFormBox helpPosition="over"
																					labelPosition="left" styleClass="panelFormBox"
																					id="formBoxCustomFooterImage4" widthLabel="125">
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomFooterImageFormItem4"
																						label="Upload Image:"
																						infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																						<hx:fileupload styleClass="fileupload"
																							id="fileuploadCustomFooterImageUpload4" size="40"
																							accept="image/*" title="Custom Image Upload"
																							value="#{welcomePageHandler.footerImage.imageContents}">
																							<hx:fileProp name="fileName"
																								value="#{welcomePageHandler.footerImage.imageFileName}" />
																							<hx:fileProp name="contentType"
																								value="#{welcomePageHandler.footerImage.imageFileType}" />
																						</hx:fileupload>
																						<h:message
																							for="formItemCustomFooterImageFormItem4"
																							id="errorMsgCustomFooterFileUpload4"></h:message>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomFooterOnClickURL4"
																						label="On Click URL:"
																						infoText="Specify the URL to go to when this image is clicked.">
																						<h:inputText styleClass="inputText"
																							id="textCustomFooterOnClickURL4" size="45"></h:inputText>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomFooterImageAltText4"
																						label="Alternate Text:"
																						infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																						<h:inputText styleClass="inputText"
																							id="textCustomFooterImageAltText4" size="45"></h:inputText>
																					</hx:formItem>
																				</hx:panelFormBox>
																			</f:facet>
																			<f:facet name="left"></f:facet>
																			<f:facet name="right"></f:facet>
																			<f:facet name="bottom"></f:facet>
																			<f:facet name="top"></f:facet>
																		</hx:panelLayout>
																	</odc:bfPanel>
																	<f:facet name="back">
																		<hx:commandExButton type="submit" value="&lt; Back"
																			id="tabbedPanelInnerFooterConfig_back4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="next">
																		<hx:commandExButton type="submit" value="Next &gt;"
																			id="tabbedPanelInnerFooterConfig_next4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="finish">
																		<hx:commandExButton type="submit" value="Finish"
																			id="tabbedPanelInnerFooterConfig_finish4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="cancel">
																		<hx:commandExButton type="submit" value="Cancel"
																			id="tabbedPanelInnerFooterConfig_cancel4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																</odc:tabbedPanel>

															</odc:bfPanel>



															<odc:bfPanel id="bfpanel4" name="Bottom Left Image"
																showFinishCancelButton="false">


																<h:panelGrid styleClass="panelGrid"
																	id="gridImage1OptionGrid8" cellpadding="2" columns="1">
																	<f:facet name="header">
																		<h:panelGroup styleClass="panelGroup"
																			id="groupImage11">
																			<h:outputText styleClass="outputText_Med"
																				id="textImage1Header8"
																				value="Bottom Left Image (Recommend no more than 240 pixel wide and variable height)"
																				style="font-weight: bold; font-size: 12pt"></h:outputText>
																		</h:panelGroup>
																	</f:facet>

																	<h:selectOneRadio
																		disabledClass="selectOneRadio_Disabled"
																		enabledClass="selectOneRadio_Enabled"
																		styleClass="selectOneRadio" id="radioImage1OpSelect"
																		value="#{welcomePageHandler.image1ImageOption}"
																		style="font-weight: bold"
																		onclick="return func_2(this, event);">
																		<f:selectItem itemLabel="Current Image Selection"
																			itemValue="default" />
																		<f:selectItem itemLabel="No Image" itemValue="none" />
																		<f:selectItem itemLabel="Upload Image"
																			itemValue="custom" />
																	</h:selectOneRadio>

																</h:panelGrid>
																<odc:tabbedPanel slantActiveRight="4"
																	showBackNextButton="false" slantInactiveRight="4"
																	styleClass="tabbedPanel_2" width="100%"
																	showTabs="false" variableTabLength="true"
																	id="tabbedPanelInnerImage1Config4">
																	<odc:bfPanel id="bfpanelImage1Current4" name=""
																		showFinishCancelButton="false">
																		<h:panelGrid styleClass="panelGrid"
																			id="gridImage1CustomImageDefaultImage4"
																			cellpadding="1" cellspacing="1" columns="2">
																			<f:facet name="footer">
																				<h:panelGroup styleClass="panelGroup"
																					id="groupImage1">

																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageImage1" align="middle"
																						value="#{currentDefaultCampaign.promotionSetHandler.landingPagePromoImage1Path}"
																						hspace="10" vspace="1" border="0"></hx:graphicImageEx>
																				</h:panelGroup>
																			</f:facet>

																			<hx:panelBox styleClass="panelBox"
																				id="boxCurrentImage1DefaultPanelBoxLeft4"
																				height="100%" valign="top">
																				<h:outputText styleClass="outputText_Med"
																					id="textCustomImage1DefaultLabel4"
																					value="Current Image Selection:"></h:outputText>
																			</hx:panelBox>


																		</h:panelGrid>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelNoImage1" name=""
																		showFinishCancelButton="false">
																		<hx:jspPanel id="jspPanelInnerCustomImage1NoImage4">
																			<br>
																			<br>
																			<h:outputText styleClass="outputText_Med"
																				id="textInnerImage1NoImageLabel4"
																				value="No image will be displayed. "
																				style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																			<br>
																			<br>
																			<br>
																		</hx:jspPanel>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelCustomImage1" name=""
																		showFinishCancelButton="false">
																		<hx:panelLayout styleClass="panelLayout"
																			id="layoutInnerCustomImage1ImageLayout4" width="100%">
																			<f:facet name="body">
																				<hx:panelFormBox helpPosition="over"
																					labelPosition="left" styleClass="panelFormBox"
																					id="formBoxCustomImage1" widthLabel="125">
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage1FormItem4"
																						label="Upload Image:"
																						infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																						<hx:fileupload styleClass="fileupload"
																							id="fileuploadCustomImage1Upload4" size="40"
																							accept="image/*" title="Custom Image Upload"
																							value="#{welcomePageHandler.image1Image.imageContents}">
																							<hx:fileProp name="fileName"
																								value="#{welcomePageHandler.image1Image.imageFileName}" />
																							<hx:fileProp name="contentType"
																								value="#{welcomePageHandler.image1Image.imageFileType}" />
																						</hx:fileupload>
																						<h:message for="formItemCustomImage1FormItem4"
																							id="errorMsgCustomImage1FileUpload4"></h:message>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage1OnClickURL4"
																						label="On Click URL:"
																						infoText="Specify the URL to go to when this image is clicked.">
																						<h:inputText styleClass="inputText"
																							id="textCustomImage1OnClickURL4" size="45"></h:inputText>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage1AltText4"
																						label="Alternate Text:"
																						infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																						<h:inputText styleClass="inputText"
																							id="textCustomImage1AltText4" size="45"></h:inputText>
																					</hx:formItem>
																				</hx:panelFormBox>
																			</f:facet>
																			<f:facet name="left"></f:facet>
																			<f:facet name="right"></f:facet>
																			<f:facet name="bottom"></f:facet>
																			<f:facet name="top"></f:facet>
																		</hx:panelLayout>
																	</odc:bfPanel>
																	<f:facet name="back">
																		<hx:commandExButton type="submit" value="&lt; Back"
																			id="tabbedPanelInnerImage1Config_back4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="next">
																		<hx:commandExButton type="submit" value="Next &gt;"
																			id="tabbedPanelInnerImage1Config_next4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="finish">
																		<hx:commandExButton type="submit" value="Finish"
																			id="tabbedPanelInnerImage1Config_finish4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="cancel">
																		<hx:commandExButton type="submit" value="Cancel"
																			id="tabbedPanelInnerImage1Config_cancel4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																</odc:tabbedPanel>


															</odc:bfPanel>
															<odc:bfPanel id="bfpanel5" name="Bottom Center Image"
																showFinishCancelButton="false">


																<h:panelGrid styleClass="panelGrid"
																	id="gridImage2OptionGrid8" cellpadding="2" columns="1">
																	<f:facet name="header">
																		<h:panelGroup styleClass="panelGroup"
																			id="groupImage21">
																			<h:outputText styleClass="outputText_Med"
																				id="textImage2Header8"
																				value="Bottom Center Image (Recommend no more than 240 pixel wide and variable height)"
																				style="font-weight: bold; font-size: 12pt"></h:outputText>
																		</h:panelGroup>
																	</f:facet>

																	<h:selectOneRadio
																		disabledClass="selectOneRadio_Disabled"
																		enabledClass="selectOneRadio_Enabled"
																		styleClass="selectOneRadio" id="radioImage2OpSelect"
																		value="#{welcomePageHandler.image2ImageOption}"
																		style="font-weight: bold"
																		onclick="return func_3(this, event);">
																		<f:selectItem itemLabel="Current Image Selection"
																			itemValue="default" />
																		<f:selectItem itemLabel="No Image" itemValue="none" />
																		<f:selectItem itemLabel="Upload Image"
																			itemValue="custom" />
																	</h:selectOneRadio>

																</h:panelGrid>
																<odc:tabbedPanel slantActiveRight="4"
																	showBackNextButton="false" slantInactiveRight="4"
																	styleClass="tabbedPanel_2" width="100%"
																	showTabs="false" variableTabLength="true"
																	id="tabbedPanelInnerImage2Config4">
																	<odc:bfPanel id="bfpanelImage2Current4" name=""
																		showFinishCancelButton="false">
																		<h:panelGrid styleClass="panelGrid"
																			id="gridImage2CustomImageDefaultImage4"
																			cellpadding="1" cellspacing="1" columns="2">
																			<f:facet name="footer">
																				<h:panelGroup styleClass="panelGroup"
																					id="groupImage2">

																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageImage2" align="middle"
																						value="#{currentDefaultCampaign.promotionSetHandler.landingPagePromoImage2Path}"
																						hspace="10" vspace="1" border="0"></hx:graphicImageEx>
																				</h:panelGroup>
																			</f:facet>

																			<hx:panelBox styleClass="panelBox"
																				id="boxCurrentImage2DefaultPanelBoxLeft4"
																				height="100%" valign="top">
																				<h:outputText styleClass="outputText_Med"
																					id="textCustomImage2DefaultLabel4"
																					value="Current Image Selection:"></h:outputText>
																			</hx:panelBox>


																		</h:panelGrid>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelNoImage2" name=""
																		showFinishCancelButton="false">
																		<hx:jspPanel id="jspPanelInnerCustomImage2NoImage4">
																			<br>
																			<br>
																			<h:outputText styleClass="outputText_Med"
																				id="textInnerImage2NoImageLabel4"
																				value="No image will be displayed. "
																				style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																			<br>
																			<br>
																			<br>
																		</hx:jspPanel>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelCustomImage2" name=""
																		showFinishCancelButton="false">
																		<hx:panelLayout styleClass="panelLayout"
																			id="layoutInnerCustomImage2ImageLayout4" width="100%">
																			<f:facet name="body">
																				<hx:panelFormBox helpPosition="over"
																					labelPosition="left" styleClass="panelFormBox"
																					id="formBoxCustomImage2" widthLabel="125">
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage2FormItem4"
																						label="Upload Image:"
																						infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																						<hx:fileupload styleClass="fileupload"
																							id="fileuploadCustomImage2Upload4" size="40"
																							accept="image/*" title="Custom Image Upload"
																							value="#{welcomePageHandler.image2Image.imageContents}">
																							<hx:fileProp name="fileName"
																								value="#{welcomePageHandler.image2Image.imageFileName}" />
																							<hx:fileProp name="contentType"
																								value="#{welcomePageHandler.image2Image.imageFileType}" />
																						</hx:fileupload>
																						<h:message for="formItemCustomImage2FormItem4"
																							id="errorMsgCustomImage2FileUpload4"></h:message>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage2OnClickURL4"
																						label="On Click URL:"
																						infoText="Specify the URL to go to when this image is clicked.">
																						<h:inputText styleClass="inputText"
																							id="textCustomImage2OnClickURL4" size="45"></h:inputText>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage2AltText4"
																						label="Alternate Text:"
																						infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																						<h:inputText styleClass="inputText"
																							id="textCustomImage2AltText4" size="45"></h:inputText>
																					</hx:formItem>
																				</hx:panelFormBox>
																			</f:facet>
																			<f:facet name="left"></f:facet>
																			<f:facet name="right"></f:facet>
																			<f:facet name="bottom"></f:facet>
																			<f:facet name="top"></f:facet>
																		</hx:panelLayout>
																	</odc:bfPanel>
																	<f:facet name="back">
																		<hx:commandExButton type="submit" value="&lt; Back"
																			id="tabbedPanelInnerImage2Config_back4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="next">
																		<hx:commandExButton type="submit" value="Next &gt;"
																			id="tabbedPanelInnerImage2Config_next4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="finish">
																		<hx:commandExButton type="submit" value="Finish"
																			id="tabbedPanelInnerImage2Config_finish4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="cancel">
																		<hx:commandExButton type="submit" value="Cancel"
																			id="tabbedPanelInnerImage2Config_cancel4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																</odc:tabbedPanel>



															</odc:bfPanel>
															<odc:bfPanel id="bfpanel6" name="Bottom Right Image"
																showFinishCancelButton="false">


																<h:panelGrid styleClass="panelGrid"
																	id="gridImage3OptionGrid8" cellpadding="2" columns="1">
																	<f:facet name="header">
																		<h:panelGroup styleClass="panelGroup"
																			id="groupImage31">
																			<h:outputText styleClass="outputText_Med"
																				id="textImage3Header8"
																				value="Bottom Right Image (Recommend no more than 240 pixel wide and variable height)"
																				style="font-weight: bold; font-size: 12pt"></h:outputText>
																		</h:panelGroup>
																	</f:facet>

																	<h:selectOneRadio
																		disabledClass="selectOneRadio_Disabled"
																		enabledClass="selectOneRadio_Enabled"
																		styleClass="selectOneRadio" id="radioImage3OpSelect"
																		value="#{welcomePageHandler.image3ImageOption}"
																		style="font-weight: bold"
																		onclick="return func_4(this, event);">
																		<f:selectItem itemLabel="Current Image Selection"
																			itemValue="default" />
																		<f:selectItem itemLabel="No Image" itemValue="none" />
																		<f:selectItem itemLabel="Upload Image"
																			itemValue="custom" />
																	</h:selectOneRadio>

																</h:panelGrid>
																<odc:tabbedPanel slantActiveRight="4"
																	showBackNextButton="false" slantInactiveRight="4"
																	styleClass="tabbedPanel_2" width="100%"
																	showTabs="false" variableTabLength="true"
																	id="tabbedPanelInnerImage3Config4">
																	<odc:bfPanel id="bfpanelImage3Current4" name=""
																		showFinishCancelButton="false">
																		<h:panelGrid styleClass="panelGrid"
																			id="gridImage3CustomImageDefaultImage4"
																			cellpadding="1" cellspacing="1" columns="2">
																			<f:facet name="footer">
																				<h:panelGroup styleClass="panelGroup"
																					id="groupImage3">

																					<hx:graphicImageEx styleClass="graphicImageEx"
																						id="imageImage3" align="middle"
																						value="#{currentDefaultCampaign.promotionSetHandler.landingPagePromoImage3Path}"
																						hspace="10" vspace="1" border="0"></hx:graphicImageEx>
																				</h:panelGroup>
																			</f:facet>

																			<hx:panelBox styleClass="panelBox"
																				id="boxCurrentImage3DefaultPanelBoxLeft4"
																				height="100%" valign="top">
																				<h:outputText styleClass="outputText_Med"
																					id="textCustomImage3DefaultLabel4"
																					value="Current Image Selection:"></h:outputText>
																			</hx:panelBox>


																		</h:panelGrid>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelNoImage3" name=""
																		showFinishCancelButton="false">
																		<hx:jspPanel id="jspPanelInnerCustomImage3NoImage4">
																			<br>
																			<br>
																			<h:outputText styleClass="outputText_Med"
																				id="textInnerImage3NoImageLabel4"
																				value="No image will be displayed. "
																				style="margin-left: 10px; margin-right: 10px; font-weight: bold"></h:outputText>
																			<br>
																			<br>
																			<br>
																		</hx:jspPanel>
																	</odc:bfPanel>
																	<odc:bfPanel id="bfpanelCustomImage3" name=""
																		showFinishCancelButton="false">
																		<hx:panelLayout styleClass="panelLayout"
																			id="layoutInnerCustomImage3ImageLayout4" width="100%">
																			<f:facet name="body">
																				<hx:panelFormBox helpPosition="over"
																					labelPosition="left" styleClass="panelFormBox"
																					id="formBoxCustomImage3" widthLabel="125">
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage3FormItem4"
																						label="Upload Image:"
																						infoText="Upload a new image from your computer or network. It is recommended that you remove all spaces and special character from the file name first.">
																						<hx:fileupload styleClass="fileupload"
																							id="fileuploadCustomImage3Upload4" size="40"
																							accept="image/*" title="Custom Image Upload"
																							value="#{welcomePageHandler.image3Image.imageContents}">
																							<hx:fileProp name="fileName"
																								value="#{welcomePageHandler.image3Image.imageFileName}" />
																							<hx:fileProp name="contentType"
																								value="#{welcomePageHandler.image3Image.imageFileType}" />
																						</hx:fileupload>
																						<h:message for="formItemCustomImage3FormItem4"
																							id="errorMsgCustomImage3FileUpload4"></h:message>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage3OnClickURL4"
																						label="On Click URL:"
																						infoText="Specify the URL to go to when this image is clicked.">
																						<h:inputText styleClass="inputText"
																							id="textCustomImage3OnClickURL4" size="45"></h:inputText>
																					</hx:formItem>
																					<hx:formItem styleClass="formItem"
																						id="formItemCustomImage3AltText4"
																						label="Alternate Text:"
																						infoText="Enter Alt Text for this image. Displayed on mouse hover.">
																						<h:inputText styleClass="inputText"
																							id="textCustomImage3AltText4" size="45"></h:inputText>
																					</hx:formItem>
																				</hx:panelFormBox>
																			</f:facet>
																			<f:facet name="left"></f:facet>
																			<f:facet name="right"></f:facet>
																			<f:facet name="bottom"></f:facet>
																			<f:facet name="top"></f:facet>
																		</hx:panelLayout>
																	</odc:bfPanel>
																	<f:facet name="back">
																		<hx:commandExButton type="submit" value="&lt; Back"
																			id="tabbedPanelInnerImage3Config_back4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="next">
																		<hx:commandExButton type="submit" value="Next &gt;"
																			id="tabbedPanelInnerImage3Config_next4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="finish">
																		<hx:commandExButton type="submit" value="Finish"
																			id="tabbedPanelInnerImage3Config_finish4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																	<f:facet name="cancel">
																		<hx:commandExButton type="submit" value="Cancel"
																			id="tabbedPanelInnerImage3Config_cancel4"
																			style="display:none"></hx:commandExButton>
																	</f:facet>
																</odc:tabbedPanel>



															</odc:bfPanel>
															<f:facet name="back">
																<hx:commandExButton type="submit" value="&lt; Back"
																	id="tabbedPanel2_back" style="display:none"></hx:commandExButton>
															</f:facet>
															<f:facet name="next">
																<hx:commandExButton type="submit" value="Next &gt;"
																	id="tabbedPanel2_next" style="display:none"></hx:commandExButton>
															</f:facet>
															<f:facet name="finish">
																<hx:commandExButton type="submit" value="Finish"
																	id="tabbedPanel2_finish" style="display:none"></hx:commandExButton>
															</f:facet>
															<f:facet name="cancel">
																<hx:commandExButton type="submit" value="Cancel"
																	id="tabbedPanel2_cancel" style="display:none"></hx:commandExButton>
															</f:facet>
															<odc:buttonPanel alignContent="left" id="bp1">
																<hx:commandExButton type="submit"
																	value="Publish Campaign" styleClass="commandExButton"
																	id="buttonPublishCampaign"
																	action="#{pc_LandingPageDefaultsDetail.doButtonPublishCampaignAction}"></hx:commandExButton>
																<hx:commandExButton type="submit" value="Save Changes"
																	styleClass="commandExButton" id="buttonSaveChanges"
																	action="#{pc_LandingPageDefaultsDetail.doButtonSaveChangesAction}"></hx:commandExButton>
															</odc:buttonPanel>
														</odc:tabbedPanel>
													</hx:panelBox>
												<f:facet name="header">
													<h:panelGroup styleClass="panelGroup" id="group1A"
														style="width: 100%; text-align: left">
														<h:outputText styleClass="outputText_Large"
															id="textWelcomePage"
															value="Customize Welcome Page Images   "></h:outputText>

													</h:panelGroup>
												</f:facet>
											</h:panelGrid>
											<br>
										</h:form></hx:scriptCollector>
								<%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>