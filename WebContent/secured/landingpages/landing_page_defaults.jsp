<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/secured/landingpages/Landing_page_defaults.java" --%><%-- /jsf:pagecode --%>
<%-- tpl:insert page="/theme/primaryTemplate.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>

	<%-- tpl:insert page="/theme/JSP-C-02_blue.jtpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<%-- tpl:put name="headarea" --%>

			<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/usat_utility.js"></script>
			<%-- tpl:put name="headarea_1" --%>
				<TITLE>Landing Pages Defaults</TITLE>
			<%-- /tpl:put --%>
		<%-- /tpl:put --%>


<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<LINK rel="stylesheet" href="${pageContext.request.contextPath}/theme/C_master_blue.css" type="text/css">
<LINK rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/C_stylesheet_blue.css" title="Style">
</head>

<f:view>
	<body>
	<hx:scriptCollector id="scriptCollectorJSPC02blue">
		<!-- start header area -->
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><a
			href="${pageContext.request.contextPath}/index.faces"><span
			style="color: white; font-size: 24px; text-decoration: none;">Subscription
		Portal Admin Site</span></a> <a href="#navskip"><img
			src="${pageContext.request.contextPath}/theme/1x1.gif"
			alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
		<div id="sysDescriptionDivArea" class="topAreaRightMessagePlacement">
			<h:outputText styleClass="outputText" id="textSysDescriptionHeader"
			value="#{gloabalAppSettings.systemDescription}" escape="false"></h:outputText>
		</div>		
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<div class="topNav"><siteedit:navbar
			target="home,previous,parent,self"
			spec="/portaladmin/theme/nav_horizontal_text_head.jsp"
			topsibling="true" /></div>


		<!-- start left-hand navigation -->
		<table class="mainBox" border="0" cellpadding="0" width="100%"
			height="87%" cellspacing="0">
			<tbody>
				<tr>
					<td class="leftNavTD" align="left" valign="top">
					<div class="leftNavBox"><siteedit:navbar
						spec="/portaladmin/theme/nav_vertical_tree_left.jsp"
						targetlevel="1-5" onlychildren="true" navclass="leftNav"
						topsibling="true" /></div>
					<br>
					<hx:panelSection styleClass="panelSection-V2"
						id="sectionLeftNavCheckProdSection" initClosed="true"
						style="margin-bottom: 5px; margin-top: 5px"
						rendered="#{user.authenticated}">
						<hx:jspPanel id="jspPanelLeftNavCheckProdPanel">
							<table border="0" cellpadding="0" cellspacing="1">
								<tbody>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://service.usatoday.com/include/configReset/clearCache.jsp"
											styleClass="outputLinkEx" id="linkExLeftNavToClearProdCache"
											target="_blank">
											<h:outputText id="textLeftNavClearCacheLabel"
												styleClass="outputText" value="Cache Reset Page"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="http://service.usatoday.com/ping.jsp"
											id="linkExLeftNav1" target="_blank">
											<h:outputText id="textLeftNav1" styleClass="outputText"
												value="HTTP Ping"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"><hx:outputLinkEx
											styleClass="outputLinkEx"
											value="https://service.usatoday.com/ping.jsp"
											id="linkExLeftNav2" target="_blank">
											<h:outputText id="textLeftNav2" styleClass="outputText"
												value="HTTPS (SSL) Ping"></h:outputText>
										</hx:outputLinkEx> </span></td>
									</tr>
									<tr>
										<td><span class="leftNavDIV_L2"> <hx:outputLinkEx
											value="https://sitecatalyst.omniture.com"
											styleClass="outputLinkEx" id="linkExLeftNavToOmniture"
											target="_blank">
											<h:outputText id="textLeftNav4" styleClass="outputText"
												value="Omniture Reports"></h:outputText>
										</hx:outputLinkEx></span></td>
									</tr>
								</tbody>
							</table>
						</hx:jspPanel>
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelLeftNav5">
								<hx:graphicImageEx id="imageExLeftNavCheckProdCol"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_collapsed.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav11" styleClass="leftNav_Selected"
									value="Production Links"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelLeftNav4">
								<hx:graphicImageEx id="imageExLeftNavCheckProdExp"
									styleClass="graphicImageEx"
									value="/portaladmin/images/arrow_expanded.gif"
									align="absbottom" style="margin-left: 2px"></hx:graphicImageEx>
								<h:outputText id="textLeftNav10" styleClass="leftNav_Selected"
									value="Production Links" style="vertical-align: middle"></h:outputText>
							</hx:jspPanel>
						</f:facet>
					</hx:panelSection></td>
					<!-- end left-hand navigation -->

					<!-- start main content area -->
					<td class="mainContentWideTD" align="left" valign="top">
					<div class="mainContentWideBox"><siteedit:navtrail start=""
						end="" target="home,parent,ancestor,self" separator=":"
						spec="/portaladmin/theme/nav_horizontal_trail_head.jsp" /><a
						name="navskip"><img border="0"
						src="${pageContext.request.contextPath}/theme/1x1.gif" width="1"
						height="1" alt="Beginning of page content"></a><%-- tpl:put name="bodyarea" --%>


						<%-- tpl:put name="bodyarea_1" --%><hx:scriptCollector id="scriptCollector1">
									<br>
									<br>
									<h:form styleClass="form" id="form1">
										<table border="0" cellpadding="0" cellspacing="1" width="550">
											<tbody>
												<tr>
													<td class="headerClass" nowrap="nowrap">Default Renewal Rate Codes:</td>
													<td width="20">&nbsp;</td>
													<td class="headerClass">Default Keycodes:</td>
												</tr>
												<tr>
													<td valign="top">
													<table border="0" cellpadding="0" cellspacing="1">
														<tbody>
															<tr>
																<td><h:outputText styleClass="outputText"
																	id="textUTDefaultKeycodeLabel11" value="USA TODAY:"></h:outputText></td>
																<td><h:inputText styleClass="inputText"
																	id="textUTDefKeycode11" size="4"
																	readonly="true" value="#{portalApplicationSettings.UTDefaultRenewalRateCode}"></h:inputText></td>
																<td><hx:requestLink styleClass="requestLink"
																	id="linkChangeUTDefKeycode11" rendered="false">
																	<h:outputText id="text311" styleClass="outputText"
																		value="Change"></h:outputText>


																</hx:requestLink></td>
															</tr>
															<tr>
																<td><h:outputText styleClass="outputText"
																	id="textSWDefaultKeycodeLabel11" value="Sports Weekly:"></h:outputText></td>
																<td><h:inputText styleClass="inputText"
																	id="textSWDefaultKeycode11" size="4" readonly="true" value="#{portalApplicationSettings.sportsWeeklyDefaultRenewalRateCode}"></h:inputText></td>
																<td><hx:requestLink styleClass="requestLink"
																	id="linkChangeSWDefKeycode11" rendered="false">
																	<h:outputText id="text411" styleClass="outputText"
																		value="Change"></h:outputText>
																</hx:requestLink></td>
															</tr>
															<tr>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td></td>
																<td></td>
																<td></td>
															</tr>
														</tbody>
													</table>
													</td>
													<td></td>
													<td valign="top">
														<hx:dataTableEx border="0" cellpadding="2"
																			cellspacing="0" columnClasses="columnClass1"
																			headerClass="headerClass" footerClass="footerClass"
																			rowClasses="rowClass1, rowClass2"
																			styleClass="dataTableEx" id="productData" value="#{productHandler.products}" var="currentProd">
																			<hx:columnEx id="columnExProductName" align="left" nowrap="true">
																				<f:facet name="header">
																					<h:outputText id="text3" styleClass="outputText" value="Product"></h:outputText>
																				</f:facet>
																				<h:outputText styleClass="outputText"
																					id="textProdNameDT" value="#{currentProd.name} :"></h:outputText>
																			</hx:columnEx>
																			<hx:columnEx id="columnEx2" align="center" nowrap="true">
																				<f:facet name="header">
																				<h:outputText id="text444" styleClass="outputText" value="Default Offer Key Code"></h:outputText>
																				</f:facet>
																				<hx:outputLinkEx
																value="/portaladmin/secured/defaultKeycode/change.faces?product=#{currentProd.productCode}"
																styleClass="outputLinkEx" id="linkEx1" title="Click to Change">
																				<h:outputText styleClass="outputText" id="text10"
																					value="#{currentProd.defaultKeycode}"></h:outputText>
																					</hx:outputLinkEx>
																			</hx:columnEx>
																			<hx:columnEx id="columnEx3" align="center" nowrap="true">
																				<f:facet name="header">
																					<h:outputText value="Expired Offer Key Code"
																						styleClass="outputText" id="text11"></h:outputText>
																				</f:facet>
																				<hx:outputLinkEx
																					value="/portaladmin/secured/defaultKeycode/change.faces?product=#{currentProd.productCode}"
																					styleClass="outputLinkEx" id="linkEx11" title="Click to Change">
																				<h:outputText styleClass="outputText" id="text12"
																					value="#{currentProd.expiredOfferKeycode}"></h:outputText>
																					</hx:outputLinkEx>
																			</hx:columnEx>
																		</hx:dataTableEx>
													</td>
												</tr>
												<tr>
													<td valign="top">&nbsp;</td>
													<td></td>
													<td valign="top"></td>
												</tr>
												<tr>
													<td class="headerClass">Force EZPay Rate Codes:</td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td valign="top">
													<table border="0" cellpadding="1" cellspacing="1">
														<tbody>
															<tr>
																<td align="right"><h:outputText styleClass="outputText"
																	id="textUTDefaultKeycodeLabel111" value="USA TODAY:"></h:outputText></td>
																	<td><hx:graphicImageEx
																			styleClass="graphicImageEx"
																			id="imageExUTForceEZPayEnabledImage"
																			value="/portaladmin/images/Aqua-Ball-Green-16x16.png"
																			title="Enabled" hspace="3"
																			rendered="#{portalApplicationSettings.UTForceEZPAYOfferEnabled}"></hx:graphicImageEx>
																		<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageEx21"
																		value="/portaladmin/images/Aqua-Ball-Red-16x16.png"
																		title="Disabled" hspace="3"
																		rendered="#{not portalApplicationSettings.UTForceEZPAYOfferEnabled }"></hx:graphicImageEx></td>
																<td><h:inputText styleClass="inputText"
																		id="textUTDefKeycode111"
																		value="#{portalApplicationSettings.UTForceEZPAYOfferRateCode}"
																		size="4" readonly="true"></h:inputText></td>
																<td><hx:requestLink styleClass="requestLink"
																	id="linkChangeUTDefKeycode111" rendered="false">
																	<h:outputText id="text3111" styleClass="outputText"
																		value="Change"></h:outputText>


																</hx:requestLink></td>
															</tr>
															<tr>
																<td align="right"><h:outputText styleClass="outputText"
																	id="textSWDefaultKeycodeLabel111"
																	value="Sports Weekly:"></h:outputText></td>
																	<td><hx:graphicImageEx
																			styleClass="graphicImageEx" id="imageEx11"
																			value="/portaladmin/images/Aqua-Ball-Green-16x16.png"
																			title="Enabled" hspace="3"
																			rendered="#{portalApplicationSettings.sportsWeeklyForceEZPAYOfferEnabled}"></hx:graphicImageEx>
																		<hx:graphicImageEx styleClass="graphicImageEx"
																		id="imageEx2"
																		value="/portaladmin/images/Aqua-Ball-Red-16x16.png"
																		title="Disabled" hspace="3"
																		rendered="#{not portalApplicationSettings.sportsWeeklyForceEZPAYOfferEnabled }"></hx:graphicImageEx></td>
																<td><h:inputText styleClass="inputText"
																		id="textSWDefaultKeycode111" size="4"
																		readonly="true" value="#{portalApplicationSettings.sportsWeeklyForceEZPAYOfferRateCode}"></h:inputText></td>
																<td><hx:requestLink styleClass="requestLink"
																	id="linkChangeSWDefKeycode111" rendered="false">
																	<h:outputText id="text4111" styleClass="outputText"
																		value="Change"></h:outputText>
																</hx:requestLink></td>
															</tr>
															<tr>
																<td align="right"></td>
																	<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td align="right"></td>
																	<td></td>
																<td></td>
																<td></td>
															</tr>
														</tbody>
													</table>
													</td>
													<td></td>
													<td valign="top">
													
													
													</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											</tbody>
										</table>
												<hx:panelSection styleClass="panelSection" id="section1" initClosed="true" style="margin-top: 10px;">
																								<hx:dataTableEx border="0" cellpadding="2" cellspacing="1"
													columnClasses="columnClass1" headerClass="headerClass"
													footerClass="footerClass" rowClasses="rowClass1, rowClass2"
													styleClass="dataTableEx"
													id="tableExDefaultProductPromosOutter" value="#{productHandler.productHandlers}" var="varproductHandlers" style="margin-top: 10px">
													<hx:columnEx id="columnEx1" nowrap="true" valign="top" align="center">
														<f:facet name="header">
															<h:outputText id="text1" styleClass="outputText"
																value="Product"></h:outputText>
														</f:facet>
														<h:outputText styleClass="outputText"
															id="textProdNameDump" value="#{varproductHandlers.product.name}"></h:outputText>
													</hx:columnEx>
													<hx:columnEx id="columnEx4">
														<f:facet name="header">
															<h:outputText value="Default Configuration Settings"
																styleClass="outputText" id="text2"></h:outputText>
														</f:facet>
															<hx:dataTableEx border="0" cellpadding="2"
																cellspacing="0" columnClasses="columnClass4"
																headerClass="headerClass" footerClass="footerClass"
																rowClasses="rowClass1, rowClass3"
																styleClass="dataTableEx" id="tableEx1"
																var="varpromoConfigsForProduct"
																value="#{varproductHandlers.promoConfigsForProduct}">
																<hx:columnEx id="columnEx5">
																	<f:facet name="header">
																		<h:outputText id="text4" styleClass="outputText"
																			value="Promo Type"></h:outputText>
																	</f:facet>
																	<h:outputText styleClass="outputText" id="text5"
																		value="#{varpromoConfigsForProduct.type}"
																		style="font-size: 10px"></h:outputText>
																</hx:columnEx>
																<hx:columnEx id="columnEx6">
																	<f:facet name="header">
																		<h:outputText value="ImgName" styleClass="outputText"
																			id="text6"></h:outputText>
																	</f:facet>
																	<h:outputText styleClass="outputText" id="text9"
																		value="#{varpromoConfigsForProduct.name}"
																		style="font-size: 10px"></h:outputText>
																</hx:columnEx>
																<hx:columnEx id="columnEx7">
																	<f:facet name="header">
																		<h:outputText value="Fultfill Text"
																			styleClass="outputText" id="text7"></h:outputText>
																	</f:facet>
																	<h:outputText styleClass="outputText" id="text13"
																		value="#{varpromoConfigsForProduct.fulFillText}"
																		style="font-size: 10px"></h:outputText>
																</hx:columnEx>
																<hx:columnEx id="columnEx8">
																	<f:facet name="header">
																		<h:outputText value="Fulfill URL"
																			styleClass="outputText" id="text8"></h:outputText>
																	</f:facet>
																	<h:outputText styleClass="outputText" id="text14"
																		value="#{varpromoConfigsForProduct.fulFillUrl}"
																		style="font-size: 10px"></h:outputText>
																</hx:columnEx>
																<hx:columnEx id="columnEx9">
																	<f:facet name="header">
																		<h:outputText value="AltName" styleClass="outputText"
																			id="text15"></h:outputText>
																	</f:facet>
																	<h:outputText styleClass="outputText" id="text16"
																		value="#{varpromoConfigsForProduct.altName}"
																		style="font-size: 10px"></h:outputText>
																</hx:columnEx>
															</hx:dataTableEx>
														</hx:columnEx>
												</hx:dataTableEx>
												
													<f:facet name="closed">
														<hx:jspPanel id="jspPanel2">
															<hx:graphicImageEx id="imageEx3"
																styleClass="graphicImageEx" align="middle" value="/portaladmin/images/arrow_collapsed.gif"></hx:graphicImageEx>
															<h:outputText id="text18" styleClass="outputText"
																value="Default Configuration Dump By Product"></h:outputText>
														</hx:jspPanel>
													</f:facet>
													<f:facet name="opened">
														<hx:jspPanel id="jspPanel1">
															<hx:graphicImageEx id="imageEx1"
																styleClass="graphicImageEx" align="middle" value="/portaladmin/images/arrow_expanded.gif"></hx:graphicImageEx>
															<h:outputText id="text17" styleClass="outputText"
																value="Default Configuration Dump By Product"></h:outputText>
														</hx:jspPanel>
													</f:facet>
												</hx:panelSection>
											</h:form>
									<br>
											
										</hx:scriptCollector><%-- /tpl:put --%>
					<%-- /tpl:put --%></div>
					</td>
				</tr>
			</tbody>
		</table>
		<!-- end main content area -->
	</hx:scriptCollector>
	</body>
</f:view>
</html>
<%-- /tpl:insert --%>

<%-- /tpl:insert --%>