
	<% /* @copyright jsp */ %>

	<%@ page contentType="text/html; charset=utf-8" %>

	<%@ page import="java.util.*" %>
	<%@ page import="com.ibm.pvc.wps.docEditor.*" %>

	<% String loc = request.getParameter("locale"); %>
	<% String img = request.getParameter("images"); %>
	<% String editorName = request.getParameter("editorName"); %>
	<% ResourceBundle resourceBundle = LocaleHelper.getResourceBundle("com.ibm.pvc.wps.docEditor.nls.DocEditorNLS", loc); %>

	<% String langToUse = LocaleHelper.getLocale(loc).getLanguage(); %>
	<% String isBidi = request.getParameter("isBidi"); %>
	<% String tableDir = "LTR"; if (isBidi.equalsIgnoreCase("true")) tableDir = "RTL"; %>
	<% String alignDir = "left"; if (isBidi.equalsIgnoreCase("true")) alignDir = "right"; %>

	<% boolean isAccessible = request.getParameter("isAccessible") != null; %>
	 <%
		final String[] arrProtocol = {"("+resourceBundle.getString("Other")+")","http://","file://","gopher://","https://","mailto:","news:","telnet:","wais:"};
		final int selectedIndex = 1; 
		final int otherProtocolIndex = 0; 
	 %>

	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><%= resourceBundle.getString("InsertLink") %></title>
	<link rel="STYLESHEET" type="text/css" href="Styles.css">
	</head>
	<script language="javascript" src="script_editor.js"></script>                   
	<script language="javascript">
	function IBM_RTE_cancelClicked() {   
		window.close();
	}
	function IBM_RTE_okClicked(evt) {
		var txt_url = document.getElementById("url").value.trim();
		if(""==txt_url)
		{
			alert("<%= resourceBundle.getString("Link.error.blank") %>");
			setFocus();
			evt.preventDefault();
			return false;
		}
		this.opener.url_mozilla = txt_url;
		window.close();
		this.opener.createLink_Mozilla_1(this.opener.editorName, this.opener.prompttext,this.opener.url_mozilla);
		evt.preventDefault();
		return false;
	}
	//**************************
function isURLEntered(txt_url)
{
	var opt_protocol = document.getElementById('protocol');
	var arr_opt = opt_protocol.options;
	var isURLEntered = false;
	var count = 0;
	while(count < arr_opt.length)
	{
		if(txt_url.value == arr_opt[count].value)
		{
			isURLEntered = true;
			break;
		}
		count ++;
	}
	return isURLEntered;
}
function doOkEnabled(txt_url)
{
	var btn_submit = document.getElementById('submit');
	btn_submit.disabled = (txt_url == document.getElementById("selectedProtocol").value);
}
	//***************************
	function setFocus()
	{
		 document.getElementById("url").focus();
	}
	function protocolChange(protocolValue)
	{
		var url = document.getElementById("url").value;
		var selectedProtocol = document.getElementById("selectedProtocol").value;
		if(url.length == 0)
		{
			document.getElementById("url").value = protocolValue;
		}else
		{
			if(protocolValue == '<%=arrProtocol[otherProtocolIndex]%>')
			{
				 protocolValue = "";
			}
			var index = url.indexOf(selectedProtocol);
			if(index != -1)
			{
				url = url.substring(selectedProtocol.length,url.length);
				document.getElementById("url").value = protocolValue + url;
			}else
			{
				document.getElementById("url").value = protocolValue + url;
			}
		}
		 document.getElementById("selectedProtocol").value = protocolValue;
		 document.getElementById("url").focus();
		 doOkEnabled(document.getElementById("url").value);
	}
	var timer = null;
	var skipfocus = false;
	function focusOnMe() {
			if (!skipfocus) {
					window.focus();
			}
			timer = setTimeout('focusOnMe()', 100);
	}

	function resizeWindow() {
			var h = document.getElementById("thebody").offsetHeight;
			var w = document.getElementById("thebody").offsetWidth;
			window.resizeTo(w + 30, h + 60);
	}

	</script>
	<body dir="<%= tableDir %>" lang="<%= langToUse %>" onLoad="resizeWindow();setFocus();" leftMargin="0" rightMargin="0" topMargin="0" marginheight="0" marginwidth="0" style="margin:6px;">
	<form onsubmit="javascript:IBM_RTE_okClicked(event);">
     <div id="thebody">
	<img src="<%= img %>/link.gif" alt="" width="18" height="18" border="0" align="middle">
	<span class="portlet-section-header"><%= resourceBundle.getString("InsertALink") %></span>

	<hr class="portlet-separator">

	<label for="protocol"><%= resourceBundle.getString("LinkType") %></label><br>
	<select onfocus="skipfocus=true" onblur="skipfocus=false" size="1"  class="portlet-form-input-field" onChange="javascript:protocolChange(this.value);">
	 <%
		int i = 0;
		 while(i < arrProtocol.length)
		 {
				%>
				 <option value="<%=arrProtocol[i]%>" 
				 <% if(selectedIndex == i)
						{
							%>Selected<%
						}
					%>><%=arrProtocol[i]%></option>
				 <%
				i++;
		 }
	 %>	
	</select>

	<br>&nbsp;<br>
	<label for="url"><%= resourceBundle.getString("LinkURL") %></label><br>
	<input id="url" type="text" value="<%=arrProtocol[selectedIndex]%>" onfocus="skipfocus=true" onblur="skipfocus=false" size="30" class="portlet-form-input-field" onkeyup="doOkEnabled(this.value);"/>

	<br>&nbsp;<br>
	<hr class="portlet-separator">
	<input class="wpsButtonText" type="submit" value="<%= resourceBundle.getString("OK") %>" onmouseout="IBM_RTE_btn_mouseoout(id)" onmouseover="IBM_RTE_btn_mouseover(id)" onfocus="skipfocus=true" onblur="skipfocus=false" id="submit" disabled>
	<input class="wpsButtonText" type="button" value="<%= resourceBundle.getString("Cancel") %>" onmouseout="IBM_RTE_btn_mouseoout(id)" onmouseover="IBM_RTE_btn_mouseover(id)" onClick="javascript:IBM_RTE_cancelClicked();" onfocus="skipfocus=true" onblur="skipfocus=false" id="cancel">
	<input type="hidden" id="selectedProtocol" value="<%=arrProtocol[selectedIndex]%>"/>
	</div>
	</form>
	</body>
	</html>

