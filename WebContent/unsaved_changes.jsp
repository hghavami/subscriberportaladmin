<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/Unsaved_changes.java" --%><%-- /jsf:pagecode --%>
<f:view><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<html>
<head>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">
<script type="text/javascript" src="/portaladmin/scripts/prototype.js"></script>
<script type="text/javascript" src="/portaladmin/scripts/prototypeUtils.js"></script>
			<title>$Company Name: $page name</title>
		<link rel="stylesheet" href="/portaladmin/theme/C_master_blue.css" type="text/css">

<!--  Matching stylesheet for JSF components, use instead of 'stylesheet.css' or remove the reference -->
<link rel="stylesheet" href="/portaladmin/theme/C_stylesheet_blue.css" type="text/css">
	<script language="JavaScript">
	
function showHideSection(objectId) {
    if (document.getElementById(objectId) != null) {
        if (document.getElementById(objectId).style.display == "none") {
            document.getElementById(objectId).style.display = "inline";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_expanded.gif";
            }
            state = "inline";
        } else {
            document.getElementById(objectId).style.display = "none";
            if (document.getElementById(objectId+"Img")) {
                document.getElementById(objectId+"Img").src = "/portaladmin/images/arrow_collapsed.gif";
            }
            state = "none";
        }



    }

}

	
	</script>
	</head>

<body>
<!-- start header area -->
<div class="topAreaBox"></div>
<div class="topAreaLogo"><a href="/portaladmin/index.admin"><span style="color: white;font-size: 24px;text-decoration:none;">Subscription
Portal Admin Site</span></a>
<a href="#navskip"><img src="/portaladmin/theme/1x1.gif" alt="skip to page's main contents" border="0" width="1" height="1"></a></div>
<!-- end header area -->

<!-- start header navigation bar -->
<!-- Optional secondary header navigation area.  To use:
	 1.  Define a group in the Site Navigation Map
	 2.  Set the following navbar to use the group as its Destination Group -->
<siteedit:navbar spec="/portaladmin/theme/nav_vertical_list_head.jsp" group="" />
<!-- end header navigation bar bar -->

<!-- start main content area -->
<div class="mainWideBox"><a name="navskip"><img border="0" src="/portaladmin/theme/1x1.gif" width="1" height="1" alt="Beginning of page content"></a>
<br>
<table class="messagePortlet" border="0" cellpadding="0" cellspacing="0" valign="top" width="85%" summary="Inline Messages">
	<tr valign="top">
		<td class="messageTitle">
			<a href="javascript:showHideSection('com_ibm_ws_inlineMessages')" class="expand-task"><img id="com_ibm_ws_inlineMessagesImg" src="/portaladmin/images/arrow_expanded.gif" alt="show/hide" border="0" align="middle"> Messages</a>
		</td>
	</tr>
	<tbody id="com_ibm_ws_inlineMessages">
		<tr>
			<td class="complex-property">
				<span class="validation-warn-info">
				<img alt="Warning" align="baseline" height="16" width="16" src="/portaladmin/images/Warning.gif">Changes have been made to your campaigns. Click <a href="">Save </a> to apply changes to the database.</span><br>
			</td>
		</tr>
	</tbody>
</table>
<br>
<br>
        <table border="0" cellpadding="2" cellspacing="0" valign="top" width="100%" summary="Framing Table">
        <tbody>
          <tr valign="top"> 
            <td class="table-text-white">
             
            <p>Click the Save button to log out and update the master repository with your changes.  Click the Discard button to log out and discard your changes. Click Logout to continue logging out while preserving with your WorkSpace changes. </p>
            
              <p>
              <a href="javascript:showHideSection('saveTable')" class="expand-task">
              <img id="saveTableImg" src="images/arrow_collapsed.gif" alt="Expand or collapse" border="0" align="texttop" tabindex="1">
              Total changed documents: 1
              </a>
              </p>
              
            </td>
          </tr>
          </tbody>
        </table>


<table cellspacing="0" cellpadding="0" border="0" width="98%" class="wasPortlet">

  <tbody><tr>
    <th class="wpsPortletTitle">Save</th>
    <th class="wpsPortletTitleControls">            
        
    <a target="WAS_help" tabindex="100" href="/ibm/console/secure/help_console.jsp?helpfile=com.ibm.ws.console.core/nl/en/ucon_master_config.html "></a>
    <a href="javascript:showHidePortlet('wasUniPortlet')" tabindex="1">
    <img border="0" align="texttop" tabindex="1" alt="Expand or collapse" src="images/arrow_expanded.gif" id="wasUniPortletImg" title="Expand or collapse"/>
    </a>    
    </th>
  </tr>

  
  </tbody><tbody id="wasUniPortlet">
  <tr>   
  <td colspan="2" class="wpsPortletArea">
    
        <a name="important"/> 
        

        
	    <h1 id="title-bread-crumb">

        



        

Save

</h1>
   


   <p class="instruction-text">
      Save your workspace changes to the
      
   database</p>


        
       




<a name="main"/>


<table cellspacing="0" cellpadding="0" border="0" width="100%" summary="List layout table">

	<tbody>
		<tr>
			<td id="notabs" class="layout-manager">

    
        
    







































<form action="/ibm/console/syncworkspace.do" method="get">

     
     
<input type="hidden" value="true" name="logoff" tabindex="1"/>


        <table cellspacing="0" cellpadding="2" border="0" width="100%" summary="Framing Table" valign="top">
        <tbody>
          <tr valign="top"> 
            <td class="table-text-white">
             
            <p>Click the Save button to  update the database with your changes.  Click the Discard button to discard your changes. </p>
            
              <p>
              <a class="expand-task" href="javascript:showHideSection('saveTable')">
              </a></p></td>
          </tr>
          </tbody>
        </table>
        
        
      


        <table cellspacing="0" cellpadding="2" border="0" width="100%" summary="Framing Table" valign="top" id="saveTable" style="display: none;">
            <tbody><tr> 
                <td nowrap="" align="left" width="100%" valign="bottom" class="complex-property">                             
                    
                </td>
            </tr>
            </tbody></table>
            
            <br/>
                        
        <table cellspacing="0" cellpadding="2" border="0" width="100%" summary="Framing Table" valign="top">
                            
                <tbody><tr> 
                    <td valign="top" class="navigation-button-section"> 
                            <input type="submit" value="Save" id="navigation" class="buttons" name="saveaction" tabindex="1"/>
                            <input type="submit" value="Discard" id="navigation" class="buttons" name="discardaction" tabindex="1"/>
                    </td>
                </tr>
            </tbody></table>
            

</form>






    
                        </td>
               </tr>
        </tbody>
</table>

           

            
  </td>
 </tr>
  </tbody>
 </table>

</div>

<!-- end main content area -->
</body>
</html>
</f:view>
