// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Jan","Feb","Mar","Apr","Mai","Jūn","Jūl","Aug","Sep","Okt","Nov","Dec"],
        'months-format-wide':["janvāris","februāris","marts","aprīlis","maijs","jūnijs","jūlijs","augusts","septembris","oktobris","novembris","decembris"],
        'days-format-abbr':["Sv","P","O","T","C","Pk","S"],
        'days-format-wide':["svētdiena","pirmdiena","otrdiena","trešdiena","ceturtdiena","piektdiena","sestdiena"],
        'quarters-format-abbreviated':["C1","C2","C3","C4"],
        'eraNames':["pirms mūsu ēras","mūsu ērā"],
        'eraAbbr':["pmē","mē"],
        'eraNarrow':["p.m.ē.","m.ē."],
        'dateFormat-full': "EEEE, yyyy. 'gada' d. MMMM",
        'dateFormat-long': "yyyy. 'gada' d. MMMM",
        'dateFormat-medium': "yyyy.d.M",
        'dateFormat-short': "yy.d.M"
})
                        