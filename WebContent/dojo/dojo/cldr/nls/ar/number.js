// generated from ldml/main/*.xml, xpath: ldml/numbers
({
        'decimal':"٫",
        'group':"٬",
        'percentSign':"٪",
        'nativeZeroDigit':"٠",
        'decimalFormat':"#,##0.###;#,##0.###-",
        'currencyFormat':"¤ #,##0.00;¤ #,##0.00-"
})
