// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Jan","Shk","Mar","Pri","Maj","Qer","Kor","Gsh","Sht","Tet","Nën","Dhj"],
        'months-format-wide':["janar","shkurt","mars","prill","maj","qershor","korrik","gusht","shtator","tetor","nëntor","dhjetor"],
        'months-standAlone-narrow':["J","S","M","P","M","Q","K","G","S","T","N","D"],
        'days-format-abbr':["Die","Hën","Mar","Mër","Enj","Pre","Sht"],
        'days-format-wide':["e diel","e hënë","e martë","e mërkurë","e enjte","e premte","e shtunë"],
        'days-standAlone-narrow':["D","H","M","M","E","P","S"],
        'am':"PD",
        'pm':"MD",
        'eraAbbr':["p.e.r.","n.e.r."],
        'dateFormat-full': "EEEE, dd MMMM yyyy",
        'dateFormat-long': "dd MMMM yyyy",
        'dateFormat-medium': "yyyy-MM-dd",
        'dateFormat-short': "yy-MM-dd",
        'timeFormat-full': "h.mm.ss.a v",
        'timeFormat-long': "h.mm.ss.a z",
        'timeFormat-medium': "h:mm:ss.a",
        'timeFormat-short': "h.mm.a"
})
                        