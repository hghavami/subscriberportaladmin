// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Ean","Feabh","Márta","Aib","Beal","Meith","Iúil","Lún","MFómh","DFómh","Samh","Noll"],
        'months-format-wide':["Eanáir","Feabhra","Márta","Aibreán","Bealtaine","Meitheamh","Iúil","Lúnasa","Meán Fómhair","Deireadh Fómhair","Samhain","Nollaig"],
        'months-standAlone-narrow':["E","F","M","A","B","M","I","L","M","D","S","N"],
        'days-format-abbr':["Domh","Luan","Máirt","Céad","Déar","Aoine","Sath"],
        'days-format-wide':["Dé Domhnaigh","Dé Luain","Dé Máirt","Dé Céadaoin","Déardaoin","Dé hAoine","Dé Sathairn"],
        'quarters-format-abbreviated':["R1","R2","R3","R4"],
        'quarters-format-wide':["1ú ráithe","2ú ráithe","3ú ráithe","4ú ráithe"],
        'am':"a.m.",
        'pm':"p.m.",
        'eraAbbr':["RC","AD"]
})
                        