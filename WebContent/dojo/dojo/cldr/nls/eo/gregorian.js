// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","maj","jun","jul","aŭg","sep","okt","nov","dec"],
        'months-format-wide':["januaro","februaro","marto","aprilo","majo","junio","julio","aŭgusto","septembro","oktobro","novembro","decembro"],
        'days-format-abbr':["di","lu","ma","me","ĵa","ve","sa"],
        'days-format-wide':["dimanĉo","lundo","mardo","merkredo","ĵaŭdo","vendredo","sabato"],
        'am':"atm",
        'pm':"ptm",
        'eraAbbr':["aK","pK"],
        'dateFormat-full': "EEEE, d-'a' 'de' MMMM yyyy",
        'dateFormat-long': "yyyy-MMMM-dd",
        'dateFormat-medium': "yyyy-MMM-dd",
        'dateFormat-short': "yy-MM-dd",
        'timeFormat-full': "H-'a' 'horo' 'kaj' m:ss v"
})
                        