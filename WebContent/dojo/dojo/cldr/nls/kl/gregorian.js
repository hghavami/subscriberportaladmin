// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","maj","jun","jul","aug","sep","okt","nov","dec"],
        'months-format-wide':["januari","februari","martsi","aprili","maji","juni","juli","augustusi","septemberi","oktoberi","novemberi","decemberi"],
        'days-format-abbr':["sab","ata","mar","pin","sis","tal","arf"],
        'days-format-wide':["sabaat","ataasinngorneq","marlunngorneq","pingasunngorneq","sisamanngorneq","tallimanngorneq","arfininngorneq"]
})
                        