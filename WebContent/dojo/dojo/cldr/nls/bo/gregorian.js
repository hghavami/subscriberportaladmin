// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["ཟླ་༡","ཟླ་༢","ཟླ་༣","ཟླ་༤","ཟླ་༥","ཟླ་༦","ཟླ་༧","ཟླ་༨","ཟླ་༩","ཟླ་༡༠","ཟླ་༡༡","ཟླ་༡༢"],
        'months-format-wide':["ཟླ་བ་དང་པོ་","ཟླ་བ་གཉིས་པ་","ཟླ་བ་སུམ་པ་","ཟླ་བ་བཞི་པ་","ཟླ་བ་ལྔ་པ་","ཟླ་བ་དྲུག་པ་","ཟླ་བ་བདུན་པ་","ཟླ་བ་བརྒྱད་པ་","ཟླ་བ་དགུ་པ་","ཟླ་བ་བཅུ་པ་","ཟླ་བ་བཅུ་གཅིག་པ་","ཟླ་བ་བཅུ་གཉིས་པ་"],
        'months-standAlone-wide':[undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,"ཟླ་བ་བཅུ་པ་"],
        'days-format-abbr':["ཉི་མ་","ཟླ་བ་","མིག་དམར་","ཧླག་པ་","ཕུར་བུ་","སངས་","སྤེན་པ་"],
        'days-format-narrow':["ཉི","ཟླ","མི","ཧླ","ཕུ","ས","སྤེ"],
        'days-format-wide':["གཟའ་ཉི་མ་","གཟའ་ཟླ་བ་","གཟའ་མིག་དམར་","གཟའ་ཧླག་པ་","གཟའ་ཕུར་བུ་","གཟའ་སངས་","གཟའ་སྤེན་པ་"],
        'days-standAlone-narrow':["ཉི","ཟླ","མི","ཧླ","ཕུ","ས","སྤེ"],
        'days-standAlone-wide':["གཟའ་ཉི་མ་","གཟའ་ཟླ་བ་","གཟའ་མིག་དམར་","གཟའ་ཧླག་པ་","གཟའ་ཕུར་བུ་","གཟའ་སངས་","གཟའ་སྤེན་པ་"],
        'quarters-stand-alone-narrow':["1","2","3","4"],
        'am':"སྔ་དྲོ་",
        'pm':"ཕྱི་དྲོ་",
        'eraNames':["སྤྱི་ལོ་སྔོན།","སྤྱི་ལོ།"],
        'eraAbbr':["སྤྱི་ལོ་སྔོན།","སྤྱི་ལོ།"],
        'eraNarrow':["སྤྱི་ལོ་སྔོན།","སྤྱི་ལོ།"],
        'dateFormat-long': "སྦྱི་ལོ་yyyy MMMMའི་ཙེས་dད",
        'dateFormat-medium': "yyyy ལོ་འི་MMMཙེས་d",
        'field-year':"ལོ།",
        'field-month':"ཟླ་བ་",
        'field-day':"ཉིན།",
        'field-hour':"ཆུ་ཙོ་",
        'field-minute':"སྐར་མ།",
        'field-second':"སྐར་ཆ།",
        'field-zone':"དུས་ཚོད།"
})
                        