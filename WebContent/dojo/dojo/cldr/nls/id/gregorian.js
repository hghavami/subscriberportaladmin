// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nov","Des"],
        'months-format-wide':["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"],
        'days-format-abbr':["Min","Sen","Sel","Rab","Kam","Jum","Sab"],
        'days-format-wide':["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"]
})
                        