// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["thg 1","thg 2","thg 3","thg 4","thg 5","thg 6","thg 7","thg 8","thg 9","thg 10","thg 11","thg 12"],
        'months-format-wide':["tháng một","tháng hai","tháng ba","tháng tư","tháng năm","tháng sáu","tháng bảy","tháng tám","tháng chín","tháng mười","tháng mười một","tháng mười hai"],
        'days-format-abbr':["CN","Th 2","Th 3","Th 4","Th 5","Th 6","Th 7"],
        'days-format-wide':["Chủ nhật","Thứ hai","Thứ ba","Thứ tư","Thứ năm","Thứ sáu","Thứ bảy"],
        'am':"SA",
        'pm':"CH",
        'eraAbbr':["tr. CN","sau CN"],
        'dateFormat-full': "EEEE, 'ngày' dd MMMM 'năm' yyyy",
        'dateFormat-long': "'Ngày' dd 'tháng' M 'năm' yyyy",
        'dateFormat-medium': "dd-MM-yyyy",
        'dateFormat-short': "dd/MM/yyyy",
        'dateTimeFormat': "{0} {1}" 
})
                        