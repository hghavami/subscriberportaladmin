// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-islamic
({
        'months-format-abbr':["muharram","safar","rabi’ al-awwal","rabi’ al-akhir","jumada-l-ula","jumada-l-akhira","rajab","sha’ban","ramadan","shawwal","dhu-l-ga’da","dhu-l-hijja"],
        'months-format-wide':["muharram","safar","rabi’ al-awwal","rabi’ al-akhir","jumada-l-ula","jumada-l-akhira","rajab","sha’ban","ramadan","shawwal","dhu-l-ga’da","dhu-l-hijja"]
})
                        