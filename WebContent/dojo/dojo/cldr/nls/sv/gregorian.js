// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","maj","jun","jul","aug","sep","okt","nov","dec"],
        'months-format-wide':["januari","februari","mars","april","maj","juni","juli","augusti","september","oktober","november","december"],
        'months-standAlone-narrow':["J","F","M","A","M","J","J","A","S","O","N","D"],
        'days-format-abbr':["sön","mån","tis","ons","tors","fre","lör"],
        'days-format-wide':["söndag","måndag","tisdag","onsdag","torsdag","fredag","lördag"],
        'days-standAlone-narrow':["S","M","T","O","T","F","L"],
        'quarters-format-abbreviated':["K1","K2","K3","K4"],
        'quarters-format-wide':["1:a kvartalet","2:a kvartalet","3:e kvartalet","4:e kvartalet"],
        'am':"fm",
        'pm':"em",
        'eraNames':["före Kristus","efter Kristus"],
        'eraAbbr':["f.Kr.","e.Kr."],
        'dateFormat-long': "d MMMM yyyy",
        'dateFormat-medium': "d MMM yyyy",
        'dateFormat-short': "yyyy-MM-dd",
        'timeFormat-full': "'kl'. HH.mm.ss v",
        'timeFormat-long': "HH.mm.ss z",
        'timeFormat-medium': "HH.mm.ss",
        'timeFormat-short': "HH.mm",
        'dateTimeFormat': "{1} {0}" ,
        'dateTimeAvailableFormats':["HH.mm","HH.mm.ss","EEE d MMMM","d MMMM","d MMM","d/MM","d/M","hh:mm","hh:mm:ss","mm.ss","yy-MM","MMM -yy","yyyy-MM","MMM yyyy","QQQQ yyyy"],
        'field-era':"era",
        'field-year':"år",
        'field-month':"månad",
        'field-week':"vecka",
        'field-day':"dag",
        'field-weekday':"veckodag",
        'field-dayperiod':"tidsvisning",
        'field-hour':"timme",
        'field-minute':"minut",
        'field-second':"sekund",
        'field-zone':"tidszon"
})
                        