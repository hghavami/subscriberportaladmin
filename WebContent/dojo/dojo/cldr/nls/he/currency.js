// generated from ldml/main/*.xml, xpath: ldml/numbers/currencies
({
	AUD_displayName:"דולר אוסטרלי",
	CAD_displayName:"דולר קנדי",
	CHF_displayName:"פרנק שוויצרי",
	EUR_displayName:"אירו",
	EUR_symbol:"€",
	GBP_displayName:"לירה שטרלינג",
	GBP_symbol:"UK£",
	HKD_displayName:"דולר הונג קונגי",
	JPY_displayName:"ין יפני",
	JPY_symbol:"JP¥",
	USD_displayName:"דולר אמריקאי",
	USD_symbol:"US$"
})
                 