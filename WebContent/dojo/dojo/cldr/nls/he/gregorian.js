// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["ינו","פבר","מרץ","אפר","מאי","יונ","יול","אוג","ספט","אוק","נוב","דצמ"],
        'months-format-wide':["ינואר","פברואר","מרץ","אפריל","מאי","יוני","יולי","אוגוסט","ספטמבר","אוקטובר","נובמבר","דצמבר"],
        'months-standAlone-narrow':["1","2","3","4","5","6","7","8","9","10","11","12"],
        'days-format-abbr':["א","ב","ג","ד","ה","ו","ש"],
        'days-format-wide':["יום ראשון","יום שני","יום שלישי","יום רביעי","יום חמישי","יום שישי","שבת"],
        'days-standAlone-narrow':["א","ב","ג","ד","ה","ו","ש"],
        'quarters-format-abbreviated':["רבעון ראשון","רבעון שני","רבעון שלישי","רבעון רביעי"],
        'quarters-format-wide':["רבעון 1","רבעון 2","רבעון 3","רבעון 4"],
        'quarters-stand-alone-narrow':["1","2","3","4"],
        'am':"לפה\"צ",
        'pm':"אחה\"צ",
        'eraNames':["לפני הספירה","לספירה"],
        'eraAbbr':["לפנה״ס","לסה״נ"],
        'dateFormat-full': "EEEE d MMMM yyyy",
        'dateFormat-long': "d MMMM yyyy",
        'dateFormat-medium': "dd/MM/yyyy",
        'dateFormat-short': "dd/MM/yy",
        'dateTimeFormat': "{0} {1}" ,
        'dateTimeAvailableFormats':["E d","H","HH:mm","HH:mm:ss","E d MMM","d/M","mm:ss","MM/yy","MMM yy","Q yy","yyyy","MM/yyyy","MMMM yyyy"],
        'field-era':"תקופה",
        'field-year':"שנה",
        'field-month':"חודש",
        'field-week':"שבוע",
        'field-day':"יום",
        'field-weekday':"יום בשבוע",
        'field-hour':"שעה",
        'field-minute':"דקה",
        'field-second':"שנייה",
        'field-zone':"אזור"
})
                        