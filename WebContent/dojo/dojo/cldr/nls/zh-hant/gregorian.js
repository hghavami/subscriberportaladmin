// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'days-format-abbr':["週日","週一","週二","週三","週四","週五","週六"],
        'quarters-format-abbreviated':["1季","2季","3季","4季"],
        'quarters-format-wide':["第1季","第2季","第3季","第4季"],
        'quarters-stand-alone-abbreviated':["1季","2季","3季","4季"],
        'eraNames':["西元前","西元"],
        'dateFormat-medium': "yyyy/M/d",
        'dateFormat-short': "yyyy/M/d",
        'timeFormat-full': "ahh時mm分ss秒 v",
        'timeFormat-long': "ahh時mm分ss秒 z",
        'timeFormat-medium': "a h:mm:ss",
        'timeFormat-short': "a h:mm",
        'dateTimeAvailableFormats':"M/d",
        'field-era':"年代",
        'field-week':"週",
        'field-weekday':"週天",
        'field-hour':"小時",
        'field-minute':"分鐘",
        'field-second':"秒",
        'field-zone':"區域"
})
                        