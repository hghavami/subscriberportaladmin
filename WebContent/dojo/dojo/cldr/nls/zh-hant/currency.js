// generated from ldml/main/*.xml, xpath: ldml/numbers/currencies
({
	AUD_displayName:"澳幣",
	CAD_displayName:"加幣",
	CNY_displayName:"人民幣",
	EUR_displayName:"歐元",
	EUR_symbol:"EUR",
	GBP_displayName:"英鎊",
	GBP_symbol:"GBP",
	JPY_displayName:"日圓"
})
                 