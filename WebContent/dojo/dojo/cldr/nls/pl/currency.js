// generated from ldml/main/*.xml, xpath: ldml/numbers/currencies
({
	AUD_displayName:"dolar australijski",
	AUD_symbol:"$A",
	CAD_displayName:"dolar kanadyjski",
	CAD_symbol:"Can$",
	CHF_displayName:"frank szwajcarski",
	CHF_symbol:"SwF",
	CNY_displayName:"juan renminbi",
	CNY_symbol:"Y",
	EUR_displayName:"euro",
	EUR_symbol:"€",
	GBP_displayName:"funt szterling",
	HKD_displayName:"dolar hongkoński",
	HKD_symbol:"HK$",
	JPY_displayName:"jen japoński",
	JPY_symbol:"JP¥",
	USD_displayName:"dolar amerykański "
})
                 