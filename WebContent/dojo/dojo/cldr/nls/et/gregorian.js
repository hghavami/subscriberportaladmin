// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jaan","veebr","märts","apr","mai","juuni","juuli","aug","sept","okt","nov","dets"],
        'months-format-wide':["jaanuar","veebruar","märts","aprill","mai","juuni","juuli","august","september","oktoober","november","detsember"],
        'days-format-abbr':["P","E","T","K","N","R","L"],
        'days-format-wide':["pühapäev","esmaspäev","teisipäev","kolmapäev","neljapäev","reede","laupäev"],
        'eraAbbr':["e.m.a.","m.a.j."],
        'dateFormat-full': "EEEE, d, MMMM yyyy",
        'dateFormat-long': "d MMMM yyyy",
        'dateFormat-medium': "dd.MM.yyyy",
        'dateFormat-short': "dd.MM.yy",
        'timeFormat-full': "H:mm:ss v",
        'timeFormat-long': "H:mm:ss z",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm"
})
                        