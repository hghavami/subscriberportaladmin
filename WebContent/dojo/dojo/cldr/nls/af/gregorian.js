// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Aug","Sep","Okt","Nov","Des"],
        'months-format-wide':["Januarie","Februarie","Maart","April","Mei","Junie","Julie","Augustus","September","Oktober","November","Desember"],
        'days-format-abbr':["So","Ma","Di","Wo","Do","Vr","Sa"],
        'days-format-wide':["Sondag","Maandag","Dinsdag","Woensdag","Donderdag","Vrydag","Saterdag"],
        'am':"vm.",
        'pm':"nm.",
        'eraAbbr':["v.C.","n.C."],
        'timeFormat-full': "h:mm:ss a v",
        'timeFormat-long': "h:mm:ss a z"
})
                        