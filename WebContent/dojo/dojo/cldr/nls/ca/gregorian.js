// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["gen.","febr.","març","abr.","maig","juny","jul.","ag.","set.","oct.","nov.","des."],
        'months-format-wide':["gener","febrer","març","abril","maig","juny","juliol","agost","setembre","octubre","novembre","desembre"],
        'days-format-abbr':["dg.","dl.","dt.","dc.","dj.","dv.","ds."],
        'days-format-wide':["diumenge","dilluns","dimarts","dimecres","dijous","divendres","dissabte"],
        'quarters-format-abbreviated':["1T","2T","3T","4T"],
        'quarters-format-wide':["1r trimestre","2n trimestre","3r trimestre","4t trimestre"],
        'eraAbbr':["aC","dC"],
        'dateFormat-full': "EEEE d 'de' MMMM 'de' yyyy",
        'dateFormat-long': "d 'de' MMMM 'de' yyyy",
        'dateFormat-medium': "dd/MM/yyyy",
        'dateFormat-short': "dd/MM/yy",
        'timeFormat-full': "H:mm:ss v",
        'timeFormat-long': "H:mm:ss z",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm"
})
                        