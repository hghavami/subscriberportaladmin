// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","maí","jún","júl","ágú","sep","okt","nóv","des"],
        'months-format-wide':["janúar","febrúar","mars","apríl","maí","júní","júlí","ágúst","september","október","nóvember","desember"],
        'months-standAlone-narrow':["j","f","m","a","m","j","j","á","s","o","n","d"],
        'days-format-abbr':["sun","mán","þri","mið","fim","fös","lau"],
        'days-format-wide':["sunnudagur","mánudagur","þriðjudagur","miðvikudagur","fimmtudagur","föstudagur","laugardagur"],
        'days-standAlone-narrow':["s","m","þ","m","f","f","l"],
        'quarters-format-abbreviated':["F1","F2","F3","F4"],
        'quarters-format-wide':["1st fjórðungur","2nd fjórðungur","3rd fjórðungur","4th fjórðungur"],
        'dateFormat-full': "EEEE, d. MMMM yyyy",
        'dateFormat-long': "d. MMMM yyyy",
        'dateFormat-medium': "d.M.yyyy",
        'dateFormat-short': "d.M.yyyy"
})
                        