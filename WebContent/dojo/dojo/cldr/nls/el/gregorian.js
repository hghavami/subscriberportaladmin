// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Ιαν","Φεβ","Μαρ","Απρ","Μαϊ","Ιουν","Ιουλ","Αυγ","Σεπ","Οκτ","Νοε","Δεκ"],
        'months-format-narrow':["Ι","Φ","Μ","Α","Μ","Ι","Ι","Α","Σ","Ο","Ν","Δ"],
        'months-format-wide':["Ιανουαρίου","Φεβρουαρίου","Μαρτίου","Απριλίου","Μαΐου","Ιουνίου","Ιουλίου","Αυγούστου","Σεπτεμβρίου","Οκτωβρίου","Νοεμβρίου","Δεκεμβρίου"],
        'months-standAlone-abbr':["Ιαν","Φεβ","Μαρ","Απρ","Μαϊ","Ιουν","Ιουλ","Αυγ","Σεπ","Οκτ","Νοε","Δεκ"],
        'months-standAlone-narrow':["Ι","Φ","Μ","Α","Μ","Ι","Ι","Α","Σ","Ο","Ν","Δ"],
        'months-standAlone-wide':["Ιανουάριος","Φεβρουάριος","Μάρτιος","Απρίλιος","Μάιος","Ιούνιος","Ιούλιος","Αύγουστος","Σεπτέμβριος","Οκτώβριος","Νοέμβριος","Δεκέμβριος"],
        'days-format-abbr':["Κυρ","Δευ","Τρι","Τετ","Πεμ","Παρ","Σαβ"],
        'days-format-wide':["Κυριακή","Δευτέρα","Τρίτη","Τετάρτη","Πέμπτη","Παρασκευή","Σάββατο"],
        'days-standAlone-narrow':["Κ","Δ","Τ","Τ","Π","Π","Σ"],
        'quarters-format-abbreviated':["Τ1","Τ2","Τ3","Τ4"],
        'quarters-format-wide':["1ο τρίμηνο","2ο τρίμηνο","3ο τρίμηνο","4ο τρίμηνο"],
        'quarters-stand-alone-abbreviated':["Τ1","Τ2","Τ3","Τ4"],
        'am':"ΠΜ",
        'pm':"ΜΜ",
        'eraNames':["π.Χ.","μ.Χ."],
        'eraAbbr':["π.Χ.","μ.Χ."],
        'dateFormat-full': "EEEE, dd MMMM yyyy",
        'dateFormat-long': "dd MMMM yyyy",
        'dateFormat-medium': "dd MMM yyyy",
        'dateFormat-short': "dd/MM/yyyy",
        'timeFormat-full': "h:mm:ss a v",
        'timeFormat-long': "h:mm:ss a z",
        'timeFormat-medium': "h:mm:ss a",
        'timeFormat-short': "h:mm a",
        'dateTimeFormat': "{1} {0}" ,
        'dateTimeAvailableFormats':["E d","H","HH:mm","HH:mm:ss","E d MMM","d MMMM","dd MMMM","dd/MM","d/M","mm:ss","MM/yy","MMM yy","Q yy","QQQQ yy","yyyy","MM/yyyy","MMMM yyyy"],
        'field-era':"Περίοδος",
        'field-year':"Έτος",
        'field-month':"Μήνας",
        'field-week':"Εβδομάδα",
        'field-day':"Ημέρα",
        'field-weekday':"Ημέρα Εβδομάδας",
        'field-dayperiod':"ΠΜ/ΜΜ",
        'field-hour':"Ώρα",
        'field-minute':"Λεπτό",
        'field-second':"Δευτερόλεπτο",
        'field-zone':"Ζώνη"
})
                        