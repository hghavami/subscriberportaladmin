// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-wide':["جنوری","فروری","مارچ","اپریل","مئ","جون","جولائی","اگست","ستمبر","اکتوبر","نومبر","دسمبر"],
        'days-format-wide':["اتوار","پیر","منگل","بُدھ","جمعرات","جمعہ","ہفتہ"],
        'quarters-format-wide':["چوتھاي پہلاں","چوتھاي دوجا","چوتھاي تيجا","چوتھاي چوتھا"],
        'eraNames':["ايساپورو","سں"],
        'field-year':"ورھا",
        'field-month':"مہينا",
        'field-week':"ہفتہ",
        'field-day':"دئن",
        'field-weekday':"ہفتے دا دن",
        'field-hour':"گھنٹا",
        'field-minute':"منٹ",
        'field-zone':"ٹپہ"
})
                        