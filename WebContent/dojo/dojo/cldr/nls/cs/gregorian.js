// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-narrow':["l","ú","b","d","k","č","č","s","z","ř","l","p"],
        'months-format-wide':["ledna","února","března","dubna","května","června","července","srpna","září","října","listopadu","prosince"],
        'months-standAlone-abbr':["1.","2.","3.","4.","5.","6.","7.","8.","9.","10.","11.","12."],
        'months-standAlone-narrow':["l","ú","b","d","k","č","č","s","z","ř","l","p"],
        'months-standAlone-wide':["leden","únor","březen","duben","květen","červen","červenec","srpen","září","říjen","listopad","prosinec"],
        'days-format-abbr':["ne","po","út","st","čt","pá","so"],
        'days-format-wide':["neděle","pondělí","úterý","středa","čtvrtek","pátek","sobota"],
        'days-standAlone-narrow':["N","P","Ú","S","Č","P","S"],
        'quarters-format-wide':["1. čtvrtletí","2. čtvrtletí","3. čtvrtletí","4. čtvrtletí"],
        'am':"dop.",
        'pm':"odp.",
        'eraAbbr':["př.Kr.","po Kr."],
        'dateFormat-full': "EEEE, d. MMMM yyyy",
        'dateFormat-long': "d. MMMM yyyy",
        'dateFormat-medium': "d.M.yyyy",
        'dateFormat-short': "d.M.yy",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm"
})
                        