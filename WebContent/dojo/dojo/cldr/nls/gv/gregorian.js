// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["J-guer","T-arree","Mayrnt","Avrril","Boaldyn","M-souree","J-souree","Luanistyn","M-fouyir","J-fouyir","M.Houney","M.Nollick"],
        'months-format-wide':["Jerrey-geuree","Toshiaght-arree","Mayrnt","Averil","Boaldyn","Mean-souree","Jerrey-souree","Luanistyn","Mean-fouyir","Jerrey-fouyir","Mee Houney","Mee ny Nollick"],
        'days-format-abbr':["Jed","Jel","Jem","Jerc","Jerd","Jeh","Jes"],
        'days-format-wide':["Jedoonee","Jelhein","Jemayrt","Jercean","Jerdein","Jeheiney","Jesarn"],
        'am':"a.m.",
        'pm':"p.m.",
        'eraAbbr':["RC","AD"]
})
                        