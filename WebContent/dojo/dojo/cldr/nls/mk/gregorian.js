// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["јан.","фев.","мар.","апр.","мај.","јун.","јул.","авг.","септ.","окт.","ноем.","декем."],
        'months-format-wide':["јануари","февруари","март","април","мај","јуни","јули","август","септември","октомври","ноември","декември"],
        'months-standAlone-narrow':["ј","ф","м","а","м","ј","ј","а","с","о","н","д"],
        'days-format-abbr':["нед.","пон.","вт.","сре.","чет.","пет.","саб."],
        'days-format-wide':["недела","понеделник","вторник","среда","четврток","петок","сабота"],
        'days-standAlone-narrow':["н","п","в","с","ч","п","с"],
        'eraAbbr':["пр.н.е.","ае."],
        'dateFormat-full': "EEEE, dd MMMM yyyy",
        'dateFormat-long': "dd MMMM yyyy",
        'dateFormat-medium': "dd.M.yyyy",
        'dateFormat-short': "dd.M.yy"
})
                        