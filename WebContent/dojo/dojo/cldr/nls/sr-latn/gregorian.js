// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","maj","jun","jul","avg","sep","okt","nov","dec"],
        'months-format-wide':["januar","februar","mart","april","maj","jun","jul","avgust","septembar","oktobar","novembar","decembar"],
        'months-standAlone-narrow':["j","f","m","a","m","j","j","a","s","o","n","d"],
        'days-format-abbr':["ned","pon","uto","sre","čet","pet","sub"],
        'days-format-wide':["nedelja","ponedeljak","utorak","sreda","četvrtak","petak","subota"],
        'days-standAlone-narrow':["n","p","u","s","č","p","s"],
        'quarters-format-abbreviated':["K1","K2","K3","K4"],
        'quarters-format-wide':["Prvo tromesečje","Drugo tromesečje","Treće tromesečje","Četvrto tromesečje"],
        'quarters-stand-alone-wide':["Prvo tromesečje","Drugo tromesečje","Treće tromesečje","Četvrto tromesečje"],
        'eraNames':["Pre nove ere","Nove ere"],
        'eraAbbr':["p. n. e.","n. e"],
        'eraNarrow':["p.n.e.","n. e."],
        'timeFormat-full': "HH 'časova', mm 'minuta', ss 'sekundi' vvvv",
        'field-era':"era",
        'field-year':"godina",
        'field-month':"mesec",
        'field-week':"nedelja",
        'field-day':"dan",
        'field-weekday':"dan u nedelji",
        'field-dayperiod':"doba dana",
        'field-hour':"čas",
        'field-minute':"minut",
        'field-second':"sekund",
        'field-zone':"zona"
})
                        