// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","mai","jun","jul","aug","sep","okt","nov","des"],
        'months-format-wide':["januar","februar","mars","april","mai","juni","juli","august","september","oktober","november","desember"],
        'days-format-abbr':["su","må","ty","on","to","fr","la"],
        'days-format-wide':["sundag","måndag","tysdag","onsdag","torsdag","fredag","laurdag"],
        'days-standAlone-narrow':["S","M","T","O","T","F","L"],
        'quarters-format-abbreviated':["K1","K2","K3","K4"],
        'quarters-format-wide':["1. kvartal","2. kvartal","3. kvartal","4. kvartal"],
        'eraAbbr':["f.Kr.","e.Kr."],
        'dateFormat-full': "EEEE d. MMMM yyyy",
        'dateFormat-long': "d. MMMM yyyy",
        'dateFormat-medium': "d. MMM. yyyy",
        'dateFormat-short': "dd.MM.yy",
        'timeFormat-full': "'kl'. HH.mm.ss v",
        'timeFormat-long': "HH.mm.ss z",
        'timeFormat-medium': "HH.mm.ss",
        'timeFormat-short': "HH.mm",
        'field-dayperiod':"f.m./e.m.-val"
})
                        