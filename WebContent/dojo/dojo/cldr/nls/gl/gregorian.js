// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Xan","Feb","Mar","Abr","Mai","Xuñ","Xul","Ago","Set","Out","Nov","Dec"],
        'months-format-wide':["Xaneiro","Febreiro","Marzo","Abril","Maio","Xuño","Xullo","Agosto","Setembro","Outubro","Novembro","Decembro"],
        'days-format-abbr':["Dom","Lun","Mar","Mér","Xov","Ven","Sáb"],
        'days-format-wide':["Domingo","Luns","Martes","Mércores","Xoves","Venres","Sábado"]
})
                        