// generated from ldml/main/*.xml, xpath: ldml/numbers
({
        'nativeZeroDigit':"०",
        'decimalFormat':"#,##,##0.###",
        'percentFormat':"#,##,##0%",
        'currencyFormat':"¤ #,##,##0.00"
})
