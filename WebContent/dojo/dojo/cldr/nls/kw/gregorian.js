// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Gen","Whe","Mer","Ebr","Me","Efn","Gor","Est","Gwn","Hed","Du","Kev"],
        'months-format-wide':["Mys Genver","Mys Whevrel","Mys Merth","Mys Ebrel","Mys Me","Mys Efan","Mys Gortheren","Mye Est","Mys Gwyngala","Mys Hedra","Mys Du","Mys Kevardhu"],
        'days-format-abbr':["Sul","Lun","Mth","Mhr","Yow","Gwe","Sad"],
        'days-format-wide':["De Sul","De Lun","De Merth","De Merher","De Yow","De Gwener","De Sadorn"],
        'am':"a.m.",
        'pm':"p.m.",
        'eraAbbr':["RC","AD"]
})
                        