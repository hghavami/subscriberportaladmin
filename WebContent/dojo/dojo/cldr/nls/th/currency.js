// generated from ldml/main/*.xml, xpath: ldml/numbers/currencies
({
	AUD_displayName:"เหรียญออสเตรเลีย",
	AUD_symbol:"$A",
	CAD_displayName:"เหรียญคานาดา",
	CAD_symbol:"Can$",
	CHF_displayName:"ฟรังก์สวิส",
	CNY_displayName:"หยวนเหรินเหมินบี้",
	CNY_symbol:"￥",
	EUR_displayName:"ยูโร",
	GBP_displayName:"ปอนด์สเตอร์ลิงอังกฤษ",
	GBP_symbol:"\u00A3",
	HKD_displayName:"เหรียญฮ่องกง",
	HKD_symbol:"HK$",
	JPY_displayName:"เยน",
	USD_displayName:"ดอร์ล่าร์สหรัฐ"
})
                 