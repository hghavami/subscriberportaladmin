// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-buddhist
({
        'eraAbbr':["พ.ศ."],
        'dateFormat-full': "EEEEที่ d MMMM G yyyy",
        'dateFormat-long': "d MMMM yyyy",
        'dateFormat-medium': "d MMM yyyy",
        'dateFormat-short': "d/M/yyyy",
        'timeFormat-full': "H นาฬิกา m นาที ss วินาที",
        'timeFormat-long': "H นาฬิกา m นาที",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm",
        'dateTimeFormat': "{1}, {0}" 
})
                        