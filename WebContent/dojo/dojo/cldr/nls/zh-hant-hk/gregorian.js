// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'dateFormat-full': "yyyy年M月d日 EEEE",
        'dateFormat-long': "yyyy年M月d日",
        'dateFormat-medium': "yyyy年M月d日",
        'dateFormat-short': "yy年M月d日",
        'timeFormat-medium': "ahh:mm:ss",
        'timeFormat-short': "ah:mm"
})
                        