// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","maj","jun","jul","avg","sep","okt","nov","dec"],
        'months-format-wide':["januar","februar","marec","april","maj","junij","julij","avgust","september","oktober","november","december"],
        'months-standAlone-narrow':["j","f","m","a","m","j","j","a","s","o","n","d"],
        'days-format-abbr':["ned","pon","tor","sre","čet","pet","sob"],
        'days-format-wide':["nedelja","ponedeljek","torek","sreda","četrtek","petek","sobota"],
        'days-standAlone-narrow':["n","p","t","s","č","p","s"],
        'eraAbbr':["pr.n.š.","po Kr."],
        'dateFormat-full': "EEEE, dd. MMMM yyyy",
        'dateFormat-long': "dd. MMMM yyyy",
        'dateFormat-medium': "d.M.yyyy",
        'dateFormat-short': "d.M.yy",
        'timeFormat-full': "H:mm:ss v",
        'timeFormat-long': "H:mm:ss z",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm",
        'dateTimeAvailableFormats':"d MMMM"
})
                        