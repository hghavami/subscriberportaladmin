// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'dateFormat-long': "dd MMM yyyy",
        'dateFormat-short': "dd/MM/yy",
        'timeFormat-long': "a hh:mm:ss z",
        'timeFormat-short': "a hh:mm"
})
                        