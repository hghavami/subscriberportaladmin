// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mrt","apr","mei","jun","jul","aug","sep","okt","nov","dec"],
        'months-format-wide':["januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december"],
        'months-standAlone-narrow':["J","F","M","A","M","J","J","A","S","O","N","D"],
        'days-format-abbr':["zo","ma","di","wo","do","vr","za"],
        'days-format-wide':["zondag","maandag","dinsdag","woensdag","donderdag","vrijdag","zaterdag"],
        'days-standAlone-narrow':["Z","M","D","W","D","V","Z"],
        'quarters-format-abbreviated':["K1","K2","K3","K4"],
        'eraAbbr':["v. Chr.","n. Chr."],
        'dateFormat-full': "EEEE d MMMM yyyy",
        'dateFormat-long': "d MMMM yyyy",
        'dateFormat-medium': "d MMM yyyy",
        'dateFormat-short': "dd-MM-yy",
        'timeFormat-full': "HH:mm:ss v",
        'field-era':"Tijdperk",
        'field-year':"Jaar",
        'field-month':"Maand",
        'field-day':"Dag",
        'field-weekday':"Dag van de week",
        'field-hour':"Uur",
        'field-minute':"Minuut",
        'field-second':"Seconde"
})
                        