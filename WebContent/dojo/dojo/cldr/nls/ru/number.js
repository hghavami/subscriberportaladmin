// generated from ldml/main/*.xml, xpath: ldml/numbers
({
        'decimal':",",
        'group':" ",
        'scientificFormat':"#E0",
        'percentFormat':"#,##0 %",
        'currencyFormat':"#,##0.00 ¤"
})
