// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","mai","jun","jul","aug","sep","okt","nov","des"],
        'months-format-wide':["januar","februar","mars","apríl","mai","juni","juli","august","september","oktober","november","desember"],
        'days-format-abbr':["sun","mán","týs","mik","hós","frí","ley"],
        'days-format-wide':["sunnudagur","mánadagur","týsdagur","mikudagur","hósdagur","fríggjadagur","leygardagur"],
        'quarters-format-abbreviated':["K1","K2","K3","K4"],
        'quarters-format-wide':["1. kvartal","2. kvartal","3. kvartal","4. kvartal"],
        'dateFormat-full': "EEEE dd MMMM yyyy",
        'dateFormat-long': "d. MMM yyyy",
        'dateFormat-medium': "dd-MM-yyyy",
        'dateFormat-short': "dd-MM-yy"
})
                        