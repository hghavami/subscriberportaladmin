// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Sau","Vas","Kov","Bal","Geg","Bir","Lie","Rgp","Rgs","Spl","Lap","Grd"],
        'months-format-wide':["sausio","vasario","kovo","balandžio","gegužės","birželio","liepos","rugpjūčio","rugsėjo","spalio","lapkričio","gruodžio"],
        'months-standAlone-narrow':["S","V","K","B","G","B","L","R","R","S","L","G"],
        'months-standAlone-wide':["Sausis","Vasaris","Kovas","Balandis","Gegužė","Birželis","Liepa","Rugpjūtis","Rugsėjis","Spalis","Lapkritis","Gruodis"],
        'days-format-abbr':["Sk","Pr","An","Tr","Kt","Pn","Št"],
        'days-format-wide':["sekmadienis","pirmadienis","antradienis","trečiadienis","ketvirtadienis","penktadienis","šeštadienis"],
        'days-standAlone-narrow':["S","P","A","T","K","P","Š"],
        'quarters-format-abbreviated':["K1","K2","K3","K4"],
        'quarters-format-wide':["pirmas ketvirtis","antras ketvirtis","trečias ketvirtis","ketvirtas ketvirtis"],
        'am':"priešpiet",
        'pm':"popiet",
        'eraAbbr':["pr. Kr.","po Kr."],
        'dateFormat-full': "yyyy 'm'. MMMM d 'd'.,EEEE",
        'dateFormat-long': "yyyy 'm'. MMMM d 'd'.",
        'dateFormat-medium': "yyyy.MM.dd",
        'dateFormat-short': "yyyy-MM-dd",
        'field-era':"era",
        'field-year':"metai",
        'field-month':"mėnuo",
        'field-week':"savaitė",
        'field-day':"diena",
        'field-weekday':"savaitės diena",
        'field-dayperiod':"dienos metas",
        'field-hour':"valanda",
        'field-minute':"minutė",
        'field-second':"Sekundė",
        'field-zone':"juosta"
})
                        