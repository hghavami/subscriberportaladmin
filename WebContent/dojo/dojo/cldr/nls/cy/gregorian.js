// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Ion","Chwef","Mawrth","Ebrill","Mai","Meh","Gorff","Awst","Medi","Hyd","Tach","Rhag"],
        'months-format-wide':["Ionawr","Chwefror","Mawrth","Ebrill","Mai","Mehefin","Gorffenaf","Awst","Medi","Hydref","Tachwedd","Rhagfyr"],
        'months-standAlone-narrow':["I","C","M","E","M","M","G","A","M","H","T","R"],
        'days-format-abbr':["Sul","Llun","Maw","Mer","Iau","Gwen","Sad"],
        'days-format-wide':["Dydd Sul","Dydd Llun","Dydd Mawrth","Dydd Mercher","Dydd Iau","Dydd Gwener","Dydd Sadwrn"],
        'days-standAlone-narrow':["S","L","M","M","I","G","S"]
})
                        