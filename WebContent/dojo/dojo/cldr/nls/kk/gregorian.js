// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["қаң.","ақп.","нау.","сәу.","мам.","мау.","шіл.","там.","қыр.","қаз.","қар.","желт."],
        'months-format-wide':["қаңтар","ақпан","наурыз","сәуір","мамыр","маусым","шілде","тамыз","қыркүйек","қазан","қараша","желтоқсан"],
        'days-format-abbr':["жс.","дс.","сс.","ср.","бс.","жм.","сһ."],
        'days-format-wide':["жексені","дуйсенбі","сейсенбі","сәренбі","бейсенбі","жұма","сенбі"],
        'dateFormat-full': "EEEE, d MMMM yyyy 'ж'.",
        'dateFormat-long': "d MMMM yyyy 'ж'.",
        'dateFormat-medium': "dd.MM.yyyy",
        'dateFormat-short': "dd.MM.yy"
})
                        