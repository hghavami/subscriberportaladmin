// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Յնր","Փտր","Մրտ","Ապր","Մյս","Յնս","Յլս","Օգս","Սեպ","Հոկ","Նոյ","Դեկ"],
        'months-format-wide':["Յունուար","Փետրուար","Մարտ","Ապրիլ","Մայիս","Յունիս","Յուլիս","Օգոստոս","Սեպտեմբեր","Հոկտեմբեր","Նոյեմբեր","Դեկտեմբեր"],
        'days-format-abbr':["Կիր","Երկ","Երք","Չոր","Հնգ","Ուր","Շաբ"],
        'days-format-wide':["Կիրակի","Երկուշաբթի","Երեքշաբթի","Չորեքշաբթի","Հինգշաբթի","Ուրբաթ","Շաբաթ"],
        'am':"Առ․",
        'pm':"Եր․",
        'eraAbbr':["Ք․Ա․","Ք․Ե․"],
        'dateFormat-full': "EEEE, MMMM d, yyyy",
        'dateFormat-long': "MMMM dd, yyyy",
        'dateFormat-medium': "MMM d, yyyy",
        'dateFormat-short': "MM/dd/yy"
})
                        