// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-coptic
({
        'months-format-abbr':["Tout","Baba","Hator","Kiahk","Toba","Amshir","Baramhat","Baramouda","Bashans","Paona","Epep","Mesra","Nasie"],
        'months-standAlone-narrow':["1","2","3","4","5","6","7","8","9","10","11","12","13"],
        'months-format-wide':["Tout","Baba","Hator","Kiahk","Toba","Amshir","Baramhat","Baramouda","Bashans","Paona","Epep","Mesra","Nasie"],
        'months-format-abbr':["Tout","Baba","Hator","Kiahk","Toba","Amshir","Baramhat","Baramouda","Bashans","Paona","Epep","Mesra","Nasie"],
        'months-standAlone-narrow':["1","2","3","4","5","6","7","8","9","10","11","12","13"],
        'months-format-wide':["Tout","Baba","Hator","Kiahk","Toba","Amshir","Baramhat","Baramouda","Bashans","Paona","Epep","Mesra","Nasie"],
        'days-format-abbr':["1","2","3","4","5","6","7"],
        'days-standAlone-narrow':["1","2","3","4","5","6","7"],
        'days-format-wide':["1","2","3","4","5","6","7"],
        'days-format-abbr':["1","2","3","4","5","6","7"],
        'days-standAlone-narrow':["1","2","3","4","5","6","7"],
        'days-format-wide':["1","2","3","4","5","6","7"],
        'quarters-format-abbreviated':["Q1","Q2","Q3","Q4"],
        'quarters-stand-alone-narrow':["1","2","3","4"],
        'quarters-format-wide':["Q1","Q2","Q3","Q4"],
        'quarters-format-abbreviated':["Q1","Q2","Q3","Q4"],
        'quarters-stand-alone-narrow':["1","2","3","4"],
        'quarters-format-wide':["Q1","Q2","Q3","Q4"],
        'am':"AM",
        'pm':"PM",
        'eraNames':["ERA0","ERA1"],
        'eraAbbr':["ERA0","ERA1"],
        'eraNarrow':["ERA0","ERA1"],
        'dateFormat-full': "EEEE, yyyy MMMM dd",
        'dateFormat-long': "yyyy MMMM d",
        'dateFormat-medium': "yyyy MMM d",
        'dateFormat-short': "yyyy-MM-dd",
        'timeFormat-full': "HH:mm:ss v",
        'timeFormat-long': "HH:mm:ss z",
        'timeFormat-medium': "HH:mm:ss",
        'timeFormat-short': "HH:mm",
        'dateTimeFormat': "{1} {0}" ,
        'dateTimeAvailableFormats':["E d","H","HH:mm","HH:mm:ss","E MMM d","MMMM d","M-d","mm:ss","yy-MM","yy MMM","yy Q","yyyy"],
        'dateTimeFormats-appendItem-Day':"{0} ({2}: {1})",
        'dateTimeFormats-appendItem-Day-Of-Week':"{0} {1}",
        'dateTimeFormats-appendItem-Era':"{0} {1}",
        'dateTimeFormats-appendItem-Hour':"{0} ({2}: {1})",
        'dateTimeFormats-appendItem-Minute':"{0} ({2}: {1})",
        'dateTimeFormats-appendItem-Month':"{0} ({2}: {1})",
        'dateTimeFormats-appendItem-Quarter':"{0} ({2}: {1})",
        'dateTimeFormats-appendItem-Second':"{0} ({2}: {1})",
        'dateTimeFormats-appendItem-Timezone':"{0} {1}",
        'dateTimeFormats-appendItem-Week':"{0} ({2}: {1})",
        'dateTimeFormats-appendItem-Year':"{0} {1}"
})
                        