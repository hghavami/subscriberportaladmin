// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["១","២","៣","៤","៥","៦","៧","៨","៩","១០","១១","១២"],
        'months-format-wide':["មករា","កុម្ភៈ","មិនា","មេសា","ឧសភា","មិថុនា","កក្កដា","សីហា","កញ្ញា","តុលា","វិច្ឆិកា","ធ្នូ"],
        'days-format-abbr':["អា","ច","អ","ពុ","ព្រ","សុ","ស"],
        'quarters-format-abbreviated':["ត្រី១","ត្រី២","ត្រី៣","ត្រី៤"],
        'quarters-format-wide':["ត្រីមាសទី១","ត្រីមាសទី២","ត្រីមាសទី៣","ត្រីមាសទី៤"],
        'am':"ព្រឹក",
        'pm':"ល្ងាច",
        'eraNames':["មុន​គ្រិស្តសករាជ","គ្រិស្តសករាជ"],
        'eraAbbr':["មុន​គ.ស.","គ.ស."],
        'dateFormat-medium': "d MMM yyyy",
        'dateFormat-short': "d/M/yyyy",
        'timeFormat-full': "H ម៉ោង m នាទី ss វិនាទី​ v",
        'timeFormat-long': "H ម៉ោង m នាទី ss វិនាទី​z",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm",
        'dateTimeFormat': "{1}, {0}" 
})
                        