// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["jan","feb","mar","apr","máj","jún","júl","aug","sep","okt","nov","dec"],
        'months-format-wide':["január","február","marec","apríl","máj","jún","júl","august","september","október","november","december"],
        'months-standAlone-narrow':["j","f","m","a","m","j","j","a","s","o","n","d"],
        'days-format-abbr':["Ne","Po","Ut","St","Št","Pi","So"],
        'days-format-wide':["Nedeľa","Pondelok","Utorok","Streda","Štvrtok","Piatok","Sobota"],
        'days-standAlone-narrow':["N","P","U","S","Š","P","S"],
        'quarters-format-wide':["1. štvrťrok","2. štvrťrok","3. štvrťrok","4. štvrťrok"],
        'eraAbbr':["pred n.l.","n.l."],
        'dateFormat-full': "EEEE, d. MMMM yyyy",
        'dateFormat-long': "d. MMMM yyyy",
        'dateFormat-medium': "d.M.yyyy",
        'dateFormat-short': "d.M.yyyy",
        'timeFormat-full': "H:mm:ss v",
        'timeFormat-long': "H:mm:ss z",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm"
})
                        