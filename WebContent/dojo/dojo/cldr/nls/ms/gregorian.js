// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Jan","Feb","Mac","Apr","Mei","Jun","Jul","Ogos","Sep","Okt","Nov","Dis"],
        'months-format-wide':["Januari","Februari","Mac","April","Mei","Jun","Julai","Ogos","September","Oktober","November","Disember"],
        'days-format-abbr':["Ahd","Isn","Sel","Rab","Kha","Jum","Sab"],
        'days-format-wide':["Ahad","Isnin","Selasa","Rabu","Khamis","Jumaat","Sabtu"]
})
                        