// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-abbr':["Kob","Lab","Sad","Afr","Sha","Lix","Tod","Sid","Sag","Tob","KIT","LIT"],
        'months-format-wide':["Bisha Koobaad","Bisha Labaad","Bisha Saddexaad","Bisha Afraad","Bisha Shanaad","Bisha Lixaad","Bisha Todobaad","Bisha Sideedaad","Bisha Sagaalaad","Bisha Tobnaad","Bisha Kow iyo Tobnaad","Bisha Laba iyo Tobnaad"],
        'days-format-abbr':["Axa","Isn","Sal","Arb","Kha","Jim","Sab"],
        'days-format-wide':["Axad","Isniin","Salaaso","Arbaco","Khamiis","Jimco","Sabti"],
        'am':"sn",
        'pm':"gn",
        'eraAbbr':["Ciise ka hor","Ciise ka dib"],
        'dateFormat-full': "EEEE, MMMM dd, yyyy",
        'dateFormat-long': "dd MMMM yyyy",
        'dateFormat-medium': "dd-MMM-yyyy",
        'dateFormat-short': "dd/MM/yy",
        'timeFormat-full': "h:mm:ss a v",
        'timeFormat-long': "h:mm:ss a z",
        'timeFormat-medium': "h:mm:ss a",
        'timeFormat-short': "h:mm a"
})
                        