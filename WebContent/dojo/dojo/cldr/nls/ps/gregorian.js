// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-wide':["جنوري","فبروري","مارچ","اپریل","می","جون","جولای","اګست","سپتمبر","اکتوبر","نومبر","دسمبر"],
        'days-format-wide':["یکشنبه","دوشنبه","سه‌شنبه","چهارشنبه","پنجشنبه","جمعه","شنبه"],
        'am':"غ.م.",
        'pm':"غ.و.",
        'eraAbbr':["ق.م.","م."],
        'dateFormat-full': "EEEE د yyyy د MMMM d",
        'dateFormat-long': "د yyyy د MMMM d",
        'dateFormat-medium': "d MMM yyyy",
        'dateFormat-short': "yyyy/M/d",
        'timeFormat-full': "H:mm:ss (v)",
        'timeFormat-long': "H:mm:ss (z)",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm"
})
                        