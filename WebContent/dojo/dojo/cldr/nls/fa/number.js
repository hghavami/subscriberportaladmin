// generated from ldml/main/*.xml, xpath: ldml/numbers
({
        'decimal':"٫",
        'group':"٬",
        'list':"؛",
        'percentSign':"٪",
        'nativeZeroDigit':"۰",
        'minusSign':"−",
        'exponential':"×۱۰^",
        'decimalFormat':"#,##0.###;'‪'-#,##0.###'‬'",
        'percentFormat':"'‪'%#,##0'‬'",
        'currencyFormat':"#,##0.00 ¤;'‪'-#,##0.00'‬' ¤"
})
