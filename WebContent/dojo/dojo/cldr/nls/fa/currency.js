// generated from ldml/main/*.xml, xpath: ldml/numbers/currencies
({
	AUD_displayName:"دلار استرالیا",
	CAD_displayName:"دلار کانادا",
	CHF_displayName:"فرانک سوئیس",
	CNY_displayName:"رنمینبی یوآن چین",
	EUR_displayName:"یورو",
	GBP_displayName:"پوند استرلینگ بریتانیا",
	JPY_displayName:"ین ژاپن",
	USD_displayName:"دلار امریکا"
})
                 