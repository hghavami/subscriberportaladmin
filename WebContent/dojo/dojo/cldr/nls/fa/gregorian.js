// generated from ldml/main/*.xml, xpath: ldml/calendars/calendar-gregorian
({
        'months-format-wide':["ژانویهٔ","فوریهٔ","مارس","آوریل","مهٔ","ژوئن","ژوئیهٔ","اوت","سپتامبر","اکتبر","نوامبر","دسامبر"],
        'months-standAlone-abbr':["ژانویه","فوریه","مارس","آوریل","مه","ژوئن","ژوئیه","اوت","سپتامبر","اکتبر","نوامبر","دسامبر"],
        'months-standAlone-narrow':["ژ","ف","م","آ","م","ژ","ژ","ا","س","ا","ن","د"],
        'months-standAlone-wide':["ژانویه","فوریه","مارس","آوریل","مه","ژوئن","ژوئیه","اوت","سپتامبر","اکتبر","نوامبر","دسامبر"],
        'days-format-abbr':["یکشنبه","دوشنبه","سه‌شنبه","چهارشنبه","پنجشنبه","جمعه","شنبه"],
        'days-format-wide':["یکشنبه","دوشنبه","سه‌شنبه","چهارشنبه","پنجشنبه","جمعه","شنبه"],
        'days-standAlone-narrow':["ی","د","س","چ","پ","ج","ش"],
        'quarters-format-wide':["سه‌ماههٔ اول","سه‌ماههٔ دوم","سه‌ماههٔ سوم","سه‌ماههٔ چهارم"],
        'am':"قبل از ظهر",
        'pm':"بعد از ظهر",
        'eraNames':["قبل از میلاد","میلادی"],
        'eraAbbr':["ق.م.","م."],
        'dateFormat-full': "EEEE d MMMM yyyy GGGG",
        'dateFormat-long': "d MMMM yyyy",
        'dateFormat-medium': "yyyy/M/d",
        'dateFormat-short': "yy/M/d",
        'timeFormat-full': "H:mm:ss (vvvv)",
        'timeFormat-long': "H:mm:ss (zzzz)",
        'timeFormat-medium': "H:mm:ss",
        'timeFormat-short': "H:mm",
        'dateTimeFormat': "{0}، ساعت {1}" ,
        'dateTimeFormats-appendItem-Day-Of-Week':"{1} {0}",
        'field-year':"سال",
        'field-month':"ماه",
        'field-week':"هفته",
        'field-day':"روز",
        'field-weekday':"روز هفته",
        'field-hour':"ساعت",
        'field-minute':"دقیقه",
        'field-second':"ثانیه",
        'field-zone':"منطقهٔ زمانی"
})
                        