/*
 * Created on Aug 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode;

import java.util.Collection;

import pagecode.PageCodeBase;

import com.gannett.usatoday.adminportal.appConfig.PortalApplicationRuntimeConfigurations;
import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;
import com.gannett.usatoday.adminportal.users.PortalUserBO;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.context.FacesContext;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.util.constants.UsaTodayConstants;

import javax.faces.component.html.HtmlMessages;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Forgot_password extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formForgotPassword;
	protected HtmlOutputText textEmailPromptLabel;
	protected HtmlInputText textEmailAddress;
	protected HtmlMessage message1;
	protected HtmlCommandExButton buttonForgotPassword;
	protected HtmlMessages messages1;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	protected HtmlForm getFormForgotPassword() {
		if (formForgotPassword == null) {
			formForgotPassword = (HtmlForm) findComponentInRoot("formForgotPassword");
		}
		return formForgotPassword;
	}
	protected HtmlOutputText getTextEmailPromptLabel() {
		if (textEmailPromptLabel == null) {
			textEmailPromptLabel = (HtmlOutputText) findComponentInRoot("textEmailPromptLabel");
		}
		return textEmailPromptLabel;
	}
	protected HtmlInputText getTextEmailAddress() {
		if (textEmailAddress == null) {
			textEmailAddress = (HtmlInputText) findComponentInRoot("textEmailAddress");
		}
		return textEmailAddress;
	}
	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}
	protected HtmlCommandExButton getButtonForgotPassword() {
		if (buttonForgotPassword == null) {
			buttonForgotPassword = (HtmlCommandExButton) findComponentInRoot("buttonForgotPassword");
		}
		return buttonForgotPassword;
	}
	public String doButtonForgotPasswordAction() {
		// Type Java code that runs when the component is clicked

        FacesContext context = FacesContext.getCurrentInstance();

        String emailAddress = this.getTextEmailAddress().getValue().toString();
		try {
			
			Collection<PortalUserIntf> users = PortalUserBO.retrieveUsersByEmail(emailAddress);
			
			if (users.size() > 0) {
				PortalUserIntf user = users.iterator().next();
				
				String password = user.getPassword();

				// send the email
				SmtpMailSender mail = new SmtpMailSender();
				mail.setMessageSubject("Subscriber Portal Forgot Password Request");
				StringBuilder msg = new StringBuilder();
				msg.append("A password request was made using your email.\n");
				msg.append("Your user id is: ");
				msg.append(user.getUserID());
				msg.append("\nYour password is: ");
				msg.append(password);
				
				
				mail.setMessageText(msg.toString());
				mail.addTORecipient(emailAddress);
				
				mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);
				
				// usat.mail.smtp.host                               
				PortalApplicationRuntimeConfigurations settings = new PortalApplicationRuntimeConfigurations();
				PortalApplicationSettingIntf setting = settings.getSetting("usat.mail.smtp.host");
				String smtpServer = "";
				if (setting != null) {
					smtpServer = setting.getValue();
				}
				mail.setMailServerHost(smtpServer);
				
				mail.sendMessage();
				
			}
			else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No users found with the email address: " + emailAddress , null);
	        	context.addMessage(null, message);
	        	return "failure";
			}
			
		}
		catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Exception: " + e.getMessage() , null);
        	context.addMessage(null, message);
			e.printStackTrace();
		}        
		

		
		return "success";
	}
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
}