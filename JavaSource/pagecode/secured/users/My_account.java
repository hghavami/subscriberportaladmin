/**
 * 
 */
package pagecode.secured.users;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputSecret;
import com.ibm.faces.component.html.HtmlFormMessagesArea;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.gannett.usat.dataHandlers.UserHandler;
import javax.faces.component.html.HtmlMessages;

/**
 * @author aeast
 *
 */
public class My_account extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorUserAccount;
	protected HtmlForm formUserAccount;
	protected HtmlPanelFormBox formBoxAccountInfo;
	protected HtmlFormItem formItemFirstName;
	protected HtmlInputText textFirstName;
	protected HtmlFormItem formItemLastName;
	protected HtmlFormItem formItemPasswordConfirm;
	protected HtmlFormItem formItemPhone;
	protected HtmlFormItem formItemPassword;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlInputText textLastName;
	protected HtmlInputText textPhoneNumber;
	protected HtmlInputText textEmailAddress;
	protected HtmlInputSecret secretPassword;
	protected HtmlInputSecret secretPasswordConfirm;
	protected HtmlFormMessagesArea formMessagesArea1;
	protected HtmlCommandExButton buttonUpdateUser;
	protected UserHandler user;
	protected HtmlMessages messages1;
	protected HtmlFormItem formItemUserLoginID;
	protected HtmlInputText textLoginID;
	protected HtmlScriptCollector getScriptCollectorUserAccount() {
		if (scriptCollectorUserAccount == null) {
			scriptCollectorUserAccount = (HtmlScriptCollector) findComponentInRoot("scriptCollectorUserAccount");
		}
		return scriptCollectorUserAccount;
	}

	protected HtmlForm getFormUserAccount() {
		if (formUserAccount == null) {
			formUserAccount = (HtmlForm) findComponentInRoot("formUserAccount");
		}
		return formUserAccount;
	}

	protected HtmlPanelFormBox getFormBoxAccountInfo() {
		if (formBoxAccountInfo == null) {
			formBoxAccountInfo = (HtmlPanelFormBox) findComponentInRoot("formBoxAccountInfo");
		}
		return formBoxAccountInfo;
	}

	protected HtmlFormItem getFormItemFirstName() {
		if (formItemFirstName == null) {
			formItemFirstName = (HtmlFormItem) findComponentInRoot("formItemFirstName");
		}
		return formItemFirstName;
	}

	protected HtmlInputText getTextFirstName() {
		if (textFirstName == null) {
			textFirstName = (HtmlInputText) findComponentInRoot("textFirstName");
		}
		return textFirstName;
	}

	protected HtmlFormItem getFormItemLastName() {
		if (formItemLastName == null) {
			formItemLastName = (HtmlFormItem) findComponentInRoot("formItemLastName");
		}
		return formItemLastName;
	}

	protected HtmlFormItem getFormItemPasswordConfirm() {
		if (formItemPasswordConfirm == null) {
			formItemPasswordConfirm = (HtmlFormItem) findComponentInRoot("formItemPasswordConfirm");
		}
		return formItemPasswordConfirm;
	}

	protected HtmlFormItem getFormItemPhone() {
		if (formItemPhone == null) {
			formItemPhone = (HtmlFormItem) findComponentInRoot("formItemPhone");
		}
		return formItemPhone;
	}

	protected HtmlFormItem getFormItemPassword() {
		if (formItemPassword == null) {
			formItemPassword = (HtmlFormItem) findComponentInRoot("formItemPassword");
		}
		return formItemPassword;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlInputText getTextLastName() {
		if (textLastName == null) {
			textLastName = (HtmlInputText) findComponentInRoot("textLastName");
		}
		return textLastName;
	}

	protected HtmlInputText getTextPhoneNumber() {
		if (textPhoneNumber == null) {
			textPhoneNumber = (HtmlInputText) findComponentInRoot("textPhoneNumber");
		}
		return textPhoneNumber;
	}

	protected HtmlInputText getTextEmailAddress() {
		if (textEmailAddress == null) {
			textEmailAddress = (HtmlInputText) findComponentInRoot("textEmailAddress");
		}
		return textEmailAddress;
	}

	protected HtmlInputSecret getSecretPassword() {
		if (secretPassword == null) {
			secretPassword = (HtmlInputSecret) findComponentInRoot("secretPassword");
		}
		return secretPassword;
	}

	protected HtmlInputSecret getSecretPasswordConfirm() {
		if (secretPasswordConfirm == null) {
			secretPasswordConfirm = (HtmlInputSecret) findComponentInRoot("secretPasswordConfirm");
		}
		return secretPasswordConfirm;
	}

	protected HtmlFormMessagesArea getFormMessagesArea1() {
		if (formMessagesArea1 == null) {
			formMessagesArea1 = (HtmlFormMessagesArea) findComponentInRoot("formMessagesArea1");
		}
		return formMessagesArea1;
	}

	protected HtmlCommandExButton getButtonUpdateUser() {
		if (buttonUpdateUser == null) {
			buttonUpdateUser = (HtmlCommandExButton) findComponentInRoot("buttonUpdateUser");
		}
		return buttonUpdateUser;
	}

	public String doButtonUpdateUserAction() {
		// Type Java code that runs when the component is clicked

		UserHandler user = this.getUser();
		
		String responsePath = "success";
		if (!user.getPassword().equals(user.getUserEnteredpassword())) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Password does not match confirm password. Please re-enter passwords.", "Password does not match confirm password. Please re-enter passwords.");
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
		}
		
		else {
			try {
				user.getUser().save();
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Changes to your account have been saved.", "");
				getFacesContext().addMessage(null, message);
			}
			catch(Exception e) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Failed to update user.");
				getFacesContext().addMessage(null, message);
			}
		}
		
		return responsePath;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getFacesContext().getApplication()
					.createValueBinding("#{user}").getValue(getFacesContext());
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlFormItem getFormItemUserLoginID() {
		if (formItemUserLoginID == null) {
			formItemUserLoginID = (HtmlFormItem) findComponentInRoot("formItemUserLoginID");
		}
		return formItemUserLoginID;
	}

	protected HtmlInputText getTextLoginID() {
		if (textLoginID == null) {
			textLoginID = (HtmlInputText) findComponentInRoot("textLoginID");
		}
		return textLoginID;
	}

}