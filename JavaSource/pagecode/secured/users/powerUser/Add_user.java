/**
 * 
 */
package pagecode.secured.users.powerUser;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.util.constants.UsaTodayConstants;

import javax.faces.component.html.HtmlMessages;
import com.gannett.usat.dataHandlers.UserHandler;
import javax.faces.component.html.HtmlSelectManyCheckbox;

import org.apache.commons.lang.RandomStringUtils;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import com.gannett.usat.dataHandlers.AdminUsersHandler;

/**
 * @author aeast
 *
 */
public class Add_user extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formAddUserForm;
	protected HtmlPanelFormBox formBoxAddUser;
	protected HtmlFormItem formItem1UserRole;
	protected HtmlFormItem formItemUserID;
	protected HtmlFormItem formItemFirstName;
	protected HtmlFormItem formItemLastName;
	protected HtmlFormItem formItemPhone;
	protected HtmlFormItem formItemEmail;
	protected HtmlInputText textUserID;
	protected HtmlInputText textFirstName;
	protected HtmlInputText textLastName;
	protected HtmlInputText textPhone;
	protected HtmlInputText textEmailAddress;
	protected HtmlPanelGrid gridFooterGrid;
	protected HtmlCommandExButton buttonCancel;
	protected HtmlCommandExButton buttonSaveUser;
	protected HtmlMessages messages1;
	protected HtmlSelectManyCheckbox checkboxNewRoles;
	protected UserHandler newSubscriberPortalUser;
	protected HtmlFormItem formItemNewUserEnabled;
	protected HtmlSelectBooleanCheckbox checkboxNewUserEnabled;
	protected AdminUsersHandler adminSiteUsers;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormAddUserForm() {
		if (formAddUserForm == null) {
			formAddUserForm = (HtmlForm) findComponentInRoot("formAddUserForm");
		}
		return formAddUserForm;
	}

	protected HtmlPanelFormBox getFormBoxAddUser() {
		if (formBoxAddUser == null) {
			formBoxAddUser = (HtmlPanelFormBox) findComponentInRoot("formBoxAddUser");
		}
		return formBoxAddUser;
	}

	protected HtmlFormItem getFormItem1UserRole() {
		if (formItem1UserRole == null) {
			formItem1UserRole = (HtmlFormItem) findComponentInRoot("formItem1UserRole");
		}
		return formItem1UserRole;
	}

	protected HtmlFormItem getFormItemUserID() {
		if (formItemUserID == null) {
			formItemUserID = (HtmlFormItem) findComponentInRoot("formItemUserID");
		}
		return formItemUserID;
	}

	protected HtmlFormItem getFormItemFirstName() {
		if (formItemFirstName == null) {
			formItemFirstName = (HtmlFormItem) findComponentInRoot("formItemFirstName");
		}
		return formItemFirstName;
	}

	protected HtmlFormItem getFormItemLastName() {
		if (formItemLastName == null) {
			formItemLastName = (HtmlFormItem) findComponentInRoot("formItemLastName");
		}
		return formItemLastName;
	}

	protected HtmlFormItem getFormItemPhone() {
		if (formItemPhone == null) {
			formItemPhone = (HtmlFormItem) findComponentInRoot("formItemPhone");
		}
		return formItemPhone;
	}

	protected HtmlFormItem getFormItemEmail() {
		if (formItemEmail == null) {
			formItemEmail = (HtmlFormItem) findComponentInRoot("formItemEmail");
		}
		return formItemEmail;
	}

	protected HtmlInputText getTextUserID() {
		if (textUserID == null) {
			textUserID = (HtmlInputText) findComponentInRoot("textUserID");
		}
		return textUserID;
	}

	protected HtmlInputText getTextFirstName() {
		if (textFirstName == null) {
			textFirstName = (HtmlInputText) findComponentInRoot("textFirstName");
		}
		return textFirstName;
	}

	protected HtmlInputText getTextLastName() {
		if (textLastName == null) {
			textLastName = (HtmlInputText) findComponentInRoot("textLastName");
		}
		return textLastName;
	}

	protected HtmlInputText getTextPhone() {
		if (textPhone == null) {
			textPhone = (HtmlInputText) findComponentInRoot("textPhone");
		}
		return textPhone;
	}

	protected HtmlInputText getTextEmailAddress() {
		if (textEmailAddress == null) {
			textEmailAddress = (HtmlInputText) findComponentInRoot("textEmailAddress");
		}
		return textEmailAddress;
	}

	protected HtmlPanelGrid getGridFooterGrid() {
		if (gridFooterGrid == null) {
			gridFooterGrid = (HtmlPanelGrid) findComponentInRoot("gridFooterGrid");
		}
		return gridFooterGrid;
	}

	protected HtmlCommandExButton getButtonCancel() {
		if (buttonCancel == null) {
			buttonCancel = (HtmlCommandExButton) findComponentInRoot("buttonCancel");
		}
		return buttonCancel;
	}

	protected HtmlCommandExButton getButtonSaveUser() {
		if (buttonSaveUser == null) {
			buttonSaveUser = (HtmlCommandExButton) findComponentInRoot("buttonSaveUser");
		}
		return buttonSaveUser;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	public String doButtonSaveUserAction() {
		// Type Java code that runs when the component is clicked

		UserHandler user = this.getNewSubscriberPortalUser();
		
		String responsePath = "success";
		try {
			String newPwd = RandomStringUtils.randomAlphabetic(6);
			user.getUser().setNewPassword(newPwd);
			
			user.getUser().save();
			
			if (user.getUser().getEnabled()) {
				// send email to new user
				SmtpMailSender mail = new SmtpMailSender();
				mail.setMessageSubject("Subscriber Portal Admin Site User Created");
				mail.setMessageText("Your account to the subscriber portal administration site has been created. \n User ID: " + user.getUserID() + "\nPassword: " + newPwd +"\n");
				mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);
				mail.addTORecipient(user.getEmailAddress());
				mail.sendMessage();
			}
			
			// force the list of user to be reloaded
			this.getAdminSiteUsers().setUsers(null);

		}
		catch (Exception e) {
			System.out.println("Failed to save new user: " + e.getMessage());
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Failed to insert user.");
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
		}
		return responsePath;
	}

	public String doButtonCancelAction() {
		// Type Java code that runs when the component is clicked
	
		//this.getAdminSiteUsers().setUsers(null);
		
		return "success";
	}

	protected HtmlSelectManyCheckbox getCheckboxNewRoles() {
		if (checkboxNewRoles == null) {
			checkboxNewRoles = (HtmlSelectManyCheckbox) findComponentInRoot("checkboxNewRoles");
		}
		return checkboxNewRoles;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getNewSubscriberPortalUser() {
		if (newSubscriberPortalUser == null) {
			newSubscriberPortalUser = (UserHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{newSubscriberPortalUser}").getValue(
							getFacesContext());
		}
		return newSubscriberPortalUser;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewSubscriberPortalUser(
			UserHandler newSubscriberPortalUser) {
		this.newSubscriberPortalUser = newSubscriberPortalUser;
	}

	protected HtmlFormItem getFormItemNewUserEnabled() {
		if (formItemNewUserEnabled == null) {
			formItemNewUserEnabled = (HtmlFormItem) findComponentInRoot("formItemNewUserEnabled");
		}
		return formItemNewUserEnabled;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxNewUserEnabled() {
		if (checkboxNewUserEnabled == null) {
			checkboxNewUserEnabled = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxNewUserEnabled");
		}
		return checkboxNewUserEnabled;
	}

	/** 
	 * @managed-bean true
	 */
	protected AdminUsersHandler getAdminSiteUsers() {
		if (adminSiteUsers == null) {
			adminSiteUsers = (AdminUsersHandler) getFacesContext()
					.getApplication().createValueBinding("#{adminSiteUsers}")
					.getValue(getFacesContext());
		}
		return adminSiteUsers;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setAdminSiteUsers(AdminUsersHandler adminSiteUsers) {
		this.adminSiteUsers = adminSiteUsers;
	}

}