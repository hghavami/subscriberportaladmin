/**
 * 
 */
package pagecode.secured.users.powerUser;

import java.util.Collection;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlDataTableEx;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlCommandExRowEdit;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.util.constants.UsaTodayConstants;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectManyListbox;

import com.gannett.usat.dataHandlers.UserHandler;
import com.gannett.usat.dataHandlers.UserRolesReferenceHandler;
import javax.faces.component.html.HtmlSelectManyCheckbox;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;

import org.apache.commons.lang.RandomStringUtils;

import com.gannett.usat.dataHandlers.AdminUsersHandler;
import com.gannett.usatoday.adminportal.appConfig.PortalApplicationRuntimeConfigurations;
import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;

import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlFormMessagesArea;

/**
 * @author aeast
 *
 */
public class Administration extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlPanelSection sectionCurrentSystemUsers;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageEx2;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlOutputText text2;
	protected HtmlOutputText text1;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text3;
	protected HtmlDataTableEx tableExCurrentUsers;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text5;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text6;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText textLastName;
	protected HtmlOutputText textFirstName;
	protected HtmlOutputText textPhone;
	protected HtmlOutputText textEmailAddr;
	protected HtmlOutputText text7;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText textUserID;
	protected HtmlPanelGrid gridOptionsGrid;
	protected HtmlForm form1;
	protected HtmlInputRowSelect rowSelectedFlag;
	protected UIColumnEx columnEx6;
	protected HtmlPanelBox box1;
	protected HtmlCommandExButton buttonDeleteSelectedUsers;
	protected HtmlCommandExRowEdit rowEditUser;
	protected HtmlJspPanel jspPanel3;
	protected UIColumnEx columnEx7;
	protected HtmlPanelFormBox formBox1;
	protected HtmlFormItem formItemLastName;
	protected HtmlInputText textNewLastName;
	protected HtmlFormItem formItemFirstName;
	protected HtmlFormItem formItemPhone;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlInputText textNewFirstName;
	protected HtmlInputText textNewPhone;
	protected HtmlInputText textNewEmail;
	protected HtmlCommandExButton buttonClick;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx8;
	protected HtmlFormItem formItemNewUserRole;
	protected HtmlCommandExButton buttonResetPassword;
	protected HtmlSelectManyListbox listboxUpdateRoles;
	protected UserRolesReferenceHandler userRoleTypes;
	protected HtmlOutputText textAssignedRoles;
	protected HtmlSelectManyCheckbox checkboxNewRoles;
	protected HtmlFormItem formItemNewEnabledStatus;
	protected HtmlSelectBooleanCheckbox checkboxNewEnabledStatus;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx9;
	protected HtmlSelectBooleanCheckbox checkboxUserCurrentEnabled;
	protected AdminUsersHandler adminSiteUsers;
	protected HtmlMessages messages1;
	protected HtmlFormMessagesArea formMessagesArea1;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlPanelSection getSectionCurrentSystemUsers() {
		if (sectionCurrentSystemUsers == null) {
			sectionCurrentSystemUsers = (HtmlPanelSection) findComponentInRoot("sectionCurrentSystemUsers");
		}
		return sectionCurrentSystemUsers;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlDataTableEx getTableExCurrentUsers() {
		if (tableExCurrentUsers == null) {
			tableExCurrentUsers = (HtmlDataTableEx) findComponentInRoot("tableExCurrentUsers");
		}
		return tableExCurrentUsers;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getTextLastName() {
		if (textLastName == null) {
			textLastName = (HtmlOutputText) findComponentInRoot("textLastName");
		}
		return textLastName;
	}

	protected HtmlOutputText getTextFirstName() {
		if (textFirstName == null) {
			textFirstName = (HtmlOutputText) findComponentInRoot("textFirstName");
		}
		return textFirstName;
	}

	protected HtmlOutputText getTextPhone() {
		if (textPhone == null) {
			textPhone = (HtmlOutputText) findComponentInRoot("textPhone");
		}
		return textPhone;
	}

	protected HtmlOutputText getTextEmailAddr() {
		if (textEmailAddr == null) {
			textEmailAddr = (HtmlOutputText) findComponentInRoot("textEmailAddr");
		}
		return textEmailAddr;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getTextUserID() {
		if (textUserID == null) {
			textUserID = (HtmlOutputText) findComponentInRoot("textUserID");
		}
		return textUserID;
	}

	protected HtmlPanelGrid getGridOptionsGrid() {
		if (gridOptionsGrid == null) {
			gridOptionsGrid = (HtmlPanelGrid) findComponentInRoot("gridOptionsGrid");
		}
		return gridOptionsGrid;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlInputRowSelect getRowSelectedFlag() {
		if (rowSelectedFlag == null) {
			rowSelectedFlag = (HtmlInputRowSelect) findComponentInRoot("rowSelectedFlag");
		}
		return rowSelectedFlag;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlCommandExButton getButtonDeleteSelectedUsers() {
		if (buttonDeleteSelectedUsers == null) {
			buttonDeleteSelectedUsers = (HtmlCommandExButton) findComponentInRoot("buttonDeleteSelectedUsers");
		}
		return buttonDeleteSelectedUsers;
	}

	protected HtmlCommandExRowEdit getRowEditUser() {
		if (rowEditUser == null) {
			rowEditUser = (HtmlCommandExRowEdit) findComponentInRoot("rowEditUser");
		}
		return rowEditUser;
	}

	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlFormItem getFormItemLastName() {
		if (formItemLastName == null) {
			formItemLastName = (HtmlFormItem) findComponentInRoot("formItemLastName");
		}
		return formItemLastName;
	}

	protected HtmlInputText getTextNewLastName() {
		if (textNewLastName == null) {
			textNewLastName = (HtmlInputText) findComponentInRoot("textNewLastName");
		}
		return textNewLastName;
	}

	protected HtmlFormItem getFormItemFirstName() {
		if (formItemFirstName == null) {
			formItemFirstName = (HtmlFormItem) findComponentInRoot("formItemFirstName");
		}
		return formItemFirstName;
	}

	protected HtmlFormItem getFormItemPhone() {
		if (formItemPhone == null) {
			formItemPhone = (HtmlFormItem) findComponentInRoot("formItemPhone");
		}
		return formItemPhone;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlInputText getTextNewFirstName() {
		if (textNewFirstName == null) {
			textNewFirstName = (HtmlInputText) findComponentInRoot("textNewFirstName");
		}
		return textNewFirstName;
	}

	protected HtmlInputText getTextNewPhone() {
		if (textNewPhone == null) {
			textNewPhone = (HtmlInputText) findComponentInRoot("textNewPhone");
		}
		return textNewPhone;
	}

	protected HtmlInputText getTextNewEmail() {
		if (textNewEmail == null) {
			textNewEmail = (HtmlInputText) findComponentInRoot("textNewEmail");
		}
		return textNewEmail;
	}

	protected HtmlCommandExButton getButtonClick() {
		if (buttonClick == null) {
			buttonClick = (HtmlCommandExButton) findComponentInRoot("buttonClick");
		}
		return buttonClick;
	}

	public String doButtonClickAction() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		// return "success"; // global 
		// return "success"; // global 
		return "success";
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlFormItem getFormItemNewUserRole() {
		if (formItemNewUserRole == null) {
			formItemNewUserRole = (HtmlFormItem) findComponentInRoot("formItemNewUserRole");
		}
		return formItemNewUserRole;
	}

	protected HtmlCommandExButton getButtonResetPassword() {
		if (buttonResetPassword == null) {
			buttonResetPassword = (HtmlCommandExButton) findComponentInRoot("buttonResetPassword");
		}
		return buttonResetPassword;
	}

	public String doButtonResetPasswordAction() {
		// Type Java code that runs when the component is clicked
	
		//
		boolean hasErrors = false;
		int numProcessed = 0;

		PortalApplicationRuntimeConfigurations settings = new PortalApplicationRuntimeConfigurations();
		PortalApplicationSettingIntf setting = settings.getSetting("usat.mail.smtp.host");
		String smtpServer = "";
		if (setting != null) {
			smtpServer = setting.getValue();
		}
		
		for( UserHandler user : this.getAdminSiteUsers().getUsers()) {
			if (user.getUserSelected()) {
				// reset password, save, and send email.
				try {
					numProcessed++;
					String newPwd = RandomStringUtils.randomAlphabetic(6);
					user.setPassword(newPwd);
					user.getUser().save();
					// send email reminder
					SmtpMailSender mail = new SmtpMailSender();
					mail.setMessageSubject("Subscriber Portal Admin Site Password Reset");
					mail.setMessageText("Your password to the Subscription Portal Administration site was reset by an administrator.\n User ID: " + user.getUserID() + "\nPassword: " + newPwd +"\n");
					mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);
					mail.addTORecipient(user.getEmailAddress());

					// usat.mail.smtp.host                               
					mail.setMailServerHost(smtpServer);
					
					mail.sendMessage();
					
					user.setUserSelected(false);
					
				}
				catch (Exception e) {
					hasErrors = true;
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "User: " + user.getUserID());
					getFacesContext().addMessage(null, message);
				}
			}
		}
		if (hasErrors) {
			return "failure";
		}
		else {
			if (numProcessed > 0) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password was reset and an email sent to the user with the new password.", "");
				getFacesContext().addMessage(null, message);
			}
			else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No users were selected so no passwords were reset.", "");
				getFacesContext().addMessage(null, message);
			}
			return "success";
		}
	}

	protected HtmlSelectManyListbox getListboxUpdateRoles() {
		if (listboxUpdateRoles == null) {
			listboxUpdateRoles = (HtmlSelectManyListbox) findComponentInRoot("listboxUpdateRoles");
		}
		return listboxUpdateRoles;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserRolesReferenceHandler getUserRoleTypes() {
		if (userRoleTypes == null) {
			userRoleTypes = (UserRolesReferenceHandler) getFacesContext()
					.getApplication().createValueBinding("#{userRoleTypes}")
					.getValue(getFacesContext());
		}
		return userRoleTypes;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUserRoleTypes(UserRolesReferenceHandler userRoleTypes) {
		this.userRoleTypes = userRoleTypes;
	}

	protected HtmlOutputText getTextAssignedRoles() {
		if (textAssignedRoles == null) {
			textAssignedRoles = (HtmlOutputText) findComponentInRoot("textAssignedRoles");
		}
		return textAssignedRoles;
	}

	protected HtmlSelectManyCheckbox getCheckboxNewRoles() {
		if (checkboxNewRoles == null) {
			checkboxNewRoles = (HtmlSelectManyCheckbox) findComponentInRoot("checkboxNewRoles");
		}
		return checkboxNewRoles;
	}

	protected HtmlFormItem getFormItemNewEnabledStatus() {
		if (formItemNewEnabledStatus == null) {
			formItemNewEnabledStatus = (HtmlFormItem) findComponentInRoot("formItemNewEnabledStatus");
		}
		return formItemNewEnabledStatus;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxNewEnabledStatus() {
		if (checkboxNewEnabledStatus == null) {
			checkboxNewEnabledStatus = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxNewEnabledStatus");
		}
		return checkboxNewEnabledStatus;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx9() {
		if (columnEx9 == null) {
			columnEx9 = (UIColumnEx) findComponentInRoot("columnEx9");
		}
		return columnEx9;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxUserCurrentEnabled() {
		if (checkboxUserCurrentEnabled == null) {
			checkboxUserCurrentEnabled = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxUserCurrentEnabled");
		}
		return checkboxUserCurrentEnabled;
	}

	public String doRowEditUserAction() {
		// Type Java code that runs when the component is clicked
	
		Collection<UserHandler>users = this.getAdminSiteUsers().getUsers();
		
		
		for (UserHandler user : users) {
			try {
				user.getUser().save();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return "";
	}

	/** 
	 * @managed-bean true
	 */
	protected AdminUsersHandler getAdminSiteUsers() {
		if (adminSiteUsers == null) {
			adminSiteUsers = (AdminUsersHandler) getFacesContext()
					.getApplication().createValueBinding("#{adminSiteUsers}")
					.getValue(getFacesContext());
		}
		return adminSiteUsers;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setAdminSiteUsers(AdminUsersHandler adminSiteUsers) {
		this.adminSiteUsers = adminSiteUsers;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	public String doButtonDeleteSelectedUsersAction() {
		// Type Java code that runs when the component is clicked
	
		boolean hasErrors = false;
		
		for( UserHandler user : this.getAdminSiteUsers().getUsers()) {
			if (user.getUserSelected()) {
				// reset password, save, and send email.
				try {
					user.getUser().delete();
				}
				catch (Exception e) {
					hasErrors = true;
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "User: " + user.getUserID());
					getFacesContext().addMessage(null, message);
				}
			}
		}
		if (hasErrors) {
			return "failure";
		}
		else {
			// setting users to null will force them to be reloaded next page load.
			this.getAdminSiteUsers().setUsers(null);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected Users were deleted.", "");
			getFacesContext().addMessage(null, message);
			return "success";
		}
	}

	protected HtmlFormMessagesArea getFormMessagesArea1() {
		if (formMessagesArea1 == null) {
			formMessagesArea1 = (HtmlFormMessagesArea) findComponentInRoot("formMessagesArea1");
		}
		return formMessagesArea1;
	}

}