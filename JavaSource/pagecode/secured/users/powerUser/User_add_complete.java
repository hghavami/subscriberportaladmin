/**
 * 
 */
package pagecode.secured.users.powerUser;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlOutputText;

/**
 * @author aeast
 *
 */
public class User_add_complete extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlCommandExButton buttonAddAnother;
	protected HtmlOutputText textsuccessmsg;
	protected HtmlOutputText textInfo2;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlCommandExButton getButtonAddAnother() {
		if (buttonAddAnother == null) {
			buttonAddAnother = (HtmlCommandExButton) findComponentInRoot("buttonAddAnother");
		}
		return buttonAddAnother;
	}

	protected HtmlOutputText getTextsuccessmsg() {
		if (textsuccessmsg == null) {
			textsuccessmsg = (HtmlOutputText) findComponentInRoot("textsuccessmsg");
		}
		return textsuccessmsg;
	}

	protected HtmlOutputText getTextInfo2() {
		if (textInfo2 == null) {
			textInfo2 = (HtmlOutputText) findComponentInRoot("textInfo2");
		}
		return textInfo2;
	}

	public String doButtonAddAnotherAction() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		// return "success"; // global 
		// return "success"; // global 
		return "success";
	}

}