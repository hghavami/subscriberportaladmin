/**
 * 
 */
package pagecode.secured.defaultKeycode;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.portalConfig.PortalAppConfigHandler;
import com.gannett.usat.dataHandlers.products.DefaultKeycodeChangeHandler;
import com.gannett.usat.dataHandlers.products.ProductsHandler;
import com.gannett.usatoday.adminportal.campaigns.utils.UsatCampaignPublisher;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;

/**
 * @author aeast
 *
 */
public class Confirm_change extends PageCodeBase {

	protected HtmlPanelGrid gridChanges1;
	protected HtmlOutputText textOldKeycodeLabel;
	protected HtmlOutputText textOldKeycode;
	protected HtmlOutputText textNewKeycodeLabel;
	protected HtmlOutputText textNewKeycode;
	protected HtmlOutputText textOldEKeycodeLabel;
	protected HtmlOutputText textOldExpiredKeycode;
	protected HtmlOutputText textNewExpiredKeycodeLabel;
	protected HtmlOutputText textNewExpiredKeycode;
	protected HtmlOutputText textOldExpiredKeycodeLabel;
	protected HtmlPanelGroup group1;
	protected HtmlPanelGroup group2;
	protected HtmlOutputFormat format1;
	protected UIParameter param1;
	protected HtmlPanelGrid grid1;
	protected HtmlCommandExButton buttonSaveChanges;
	protected HtmlCommandExButton buttonDiscardChanges;
	protected DefaultKeycodeChangeHandler defaultKeycodeChangeHandler;
	protected PortalAppConfigHandler gloabalAppSettings;
	protected ProductsHandler productHandler;

	protected HtmlPanelGrid getGridChanges1() {
		if (gridChanges1 == null) {
			gridChanges1 = (HtmlPanelGrid) findComponentInRoot("gridChanges1");
		}
		return gridChanges1;
	}

	protected HtmlOutputText getTextOldKeycodeLabel() {
		if (textOldKeycodeLabel == null) {
			textOldKeycodeLabel = (HtmlOutputText) findComponentInRoot("textOldKeycodeLabel");
		}
		return textOldKeycodeLabel;
	}

	protected HtmlOutputText getTextOldKeycode() {
		if (textOldKeycode == null) {
			textOldKeycode = (HtmlOutputText) findComponentInRoot("textOldKeycode");
		}
		return textOldKeycode;
	}

	protected HtmlOutputText getTextNewKeycodeLabel() {
		if (textNewKeycodeLabel == null) {
			textNewKeycodeLabel = (HtmlOutputText) findComponentInRoot("textNewKeycodeLabel");
		}
		return textNewKeycodeLabel;
	}

	protected HtmlOutputText getTextNewKeycode() {
		if (textNewKeycode == null) {
			textNewKeycode = (HtmlOutputText) findComponentInRoot("textNewKeycode");
		}
		return textNewKeycode;
	}

	protected HtmlOutputText getTextOldEKeycodeLabel() {
		if (textOldEKeycodeLabel == null) {
			textOldEKeycodeLabel = (HtmlOutputText) findComponentInRoot("textOldEKeycodeLabel");
		}
		return textOldEKeycodeLabel;
	}

	protected HtmlOutputText getTextOldExpiredKeycode() {
		if (textOldExpiredKeycode == null) {
			textOldExpiredKeycode = (HtmlOutputText) findComponentInRoot("textOldExpiredKeycode");
		}
		return textOldExpiredKeycode;
	}

	protected HtmlOutputText getTextNewExpiredKeycodeLabel() {
		if (textNewExpiredKeycodeLabel == null) {
			textNewExpiredKeycodeLabel = (HtmlOutputText) findComponentInRoot("textNewExpiredKeycodeLabel");
		}
		return textNewExpiredKeycodeLabel;
	}

	protected HtmlOutputText getTextNewExpiredKeycode() {
		if (textNewExpiredKeycode == null) {
			textNewExpiredKeycode = (HtmlOutputText) findComponentInRoot("textNewExpiredKeycode");
		}
		return textNewExpiredKeycode;
	}

	protected HtmlOutputText getTextOldExpiredKeycodeLabel() {
		if (textOldExpiredKeycodeLabel == null) {
			textOldExpiredKeycodeLabel = (HtmlOutputText) findComponentInRoot("textOldExpiredKeycodeLabel");
		}
		return textOldExpiredKeycodeLabel;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlOutputFormat getFormat1() {
		if (format1 == null) {
			format1 = (HtmlOutputFormat) findComponentInRoot("format1");
		}
		return format1;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlCommandExButton getButtonSaveChanges() {
		if (buttonSaveChanges == null) {
			buttonSaveChanges = (HtmlCommandExButton) findComponentInRoot("buttonSaveChanges");
		}
		return buttonSaveChanges;
	}

	protected HtmlCommandExButton getButtonDiscardChanges() {
		if (buttonDiscardChanges == null) {
			buttonDiscardChanges = (HtmlCommandExButton) findComponentInRoot("buttonDiscardChanges");
		}
		return buttonDiscardChanges;
	}

	public String doButtonDiscardChangesAction() {
		// This is java code that runs when this action method is invoked
	
		// TODO: Return an outcome that corresponds to a navigation rule
		return "success";
	}

	public String doButtonSaveChangesAction() {
		// This is java code that runs when this action method is invoked
	
		String response = "success";
		String oldKeycode = "";
		String oldExpiredKeycode = "";
		try {
			DefaultKeycodeChangeHandler changeHandler = this.getDefaultKeycodeChangeHandler();
			USATProductBO product = changeHandler.getCurrentProduct().getProduct();

			oldKeycode = product.getDefaultKeycode();
			oldExpiredKeycode = product.getExpiredOfferKeycode();
			
			SubscriptionOfferIntf newOffer = changeHandler.getNewOffer();
			if (newOffer != null) {
				product.setDefaultKeyCode(newOffer.getKeyCode());
			}

			SubscriptionOfferIntf newExpiredOffer = changeHandler.getNewExpiredOffer();
			if (newExpiredOffer != null) {
				product.setExpiredOfferKeyCode(newExpiredOffer.getKeyCode());
			}

			try {
				product.saveToTestServers();
			}
			catch (Exception e) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to save changes to test servers: " + e.getMessage() , null);
				FacesContext.getCurrentInstance().addMessage(null, message);
				throw e;
			}
			
			try {
				product.saveToProductionServers();
			}
			catch (Exception e) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to save changes to production servers: " + e.getMessage() , null);
				FacesContext.getCurrentInstance().addMessage(null, message);
				// set test back to old keycode
				try {
					product.setDefaultKeyCode(oldKeycode);
					product.setExpiredOfferKeyCode(oldExpiredKeycode);
					product.saveToTestServers();
				}
				catch (Exception ee) {
					System.out.println("###################################");
					System.out.println("Unable to roll back changes to default keycode in test servers");
					ee.printStackTrace();
					System.out.println("###################################");
				}
				throw e;
			}
						
			this.getProductHandler().refresh();
			
			try {
				UsatCampaignPublisher publisher = new UsatCampaignPublisher();
				publisher.clearTestServerCache();
				publisher.clearProductionServerCache();
			}
			catch (Exception ee) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Saved changes but Failed to clear caches: " + ee.getMessage() , null);
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
		catch (Exception e) {
			response = "failure";
			e.printStackTrace();
		}
		return response;
	}

	/** 
	 * @managed-bean true
	 */
	protected DefaultKeycodeChangeHandler getDefaultKeycodeChangeHandler() {
		if (defaultKeycodeChangeHandler == null) {
			defaultKeycodeChangeHandler = (DefaultKeycodeChangeHandler) getManagedBean("defaultKeycodeChangeHandler");
		}
		return defaultKeycodeChangeHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setDefaultKeycodeChangeHandler(
			DefaultKeycodeChangeHandler defaultKeycodeChangeHandler) {
		this.defaultKeycodeChangeHandler = defaultKeycodeChangeHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected PortalAppConfigHandler getGloabalAppSettings() {
		if (gloabalAppSettings == null) {
			gloabalAppSettings = (PortalAppConfigHandler) getManagedBean("gloabalAppSettings");
		}
		return gloabalAppSettings;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGloabalAppSettings(
			PortalAppConfigHandler gloabalAppSettings) {
		this.gloabalAppSettings = gloabalAppSettings;
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}

}