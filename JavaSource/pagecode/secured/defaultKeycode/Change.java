/**
 * 
 */
package pagecode.secured.defaultKeycode;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.products.DefaultKeycodeChangeHandler;
import com.gannett.usat.dataHandlers.products.ProductHandler;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlPanelSection;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlAjaxRefreshSubmit;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;

import javax.faces.component.html.HtmlMessage;

/**
 * @author aeast
 *
 */
public class Change extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelFormBox formBoxNewKeycode;
	protected HtmlOutputSeparator separator1;
	protected HtmlFormItem formItemNewKeycode1;
	protected HtmlPanelGrid gridKeycode;
	protected DefaultKeycodeChangeHandler defaultKeycodeChangeHandler;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageEx2;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlPanelSection sectionCurrentTermsDetail;
	protected HtmlOutputText text2;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid gridInside11;
	protected HtmlOutputText textCurrentDefaultForPubLabel;
	protected HtmlInputText textCurrentDefaultKeycode;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text3;
	protected HtmlDataTableEx tableExCurrentOfferTerms;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text5;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text6;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlOutputText text9;
	protected HtmlOutputText text10;
	protected HtmlOutputText textCurrentExpiredDefaultForPubLabel;
	protected HtmlPanelGrid gridInside1111;
	protected HtmlInputText textCurrentDefaultExpiredKeycode;
	protected HtmlPanelSection sectionCurrentExpiredTermsDetail;
	protected HtmlJspPanel jspPanelExpired2;
	protected HtmlGraphicImageEx imageExExpired2;
	protected HtmlOutputText textExpired2;
	protected HtmlJspPanel jspPanelExpired1;
	protected HtmlGraphicImageEx imageExExpired1;
	protected HtmlOutputText textExpired1;
	protected HtmlDataTableEx tableExCurrentExpiredOfferTerms;
	protected UIColumnEx columnExExpired1;
	protected HtmlOutputText textExpired3;
	protected HtmlOutputText textExpired7;
	protected UIColumnEx columnExExpired2;
	protected HtmlOutputText textExpired4;
	protected HtmlOutputText textExpired8;
	protected UIColumnEx columnExExpired3;
	protected HtmlOutputText textExpired5;
	protected HtmlOutputText textExpired9;
	protected UIColumnEx columnExExpired4;
	protected HtmlOutputText textExpired6;
	protected HtmlOutputText textExpired10;
	protected HtmlInputText text12NewKeycode;
	protected HtmlFormItem formItemNewKeycode2;
	protected HtmlInputText text12;
	protected HtmlPanelGrid gridNewOfferGrid;
	protected HtmlPanelSection sectionNewTermsDetail;
	protected HtmlJspPanel jspPanelNew2;
	protected HtmlGraphicImageEx imageExNew2;
	protected HtmlOutputText textNew2;
	protected HtmlJspPanel jspPanelNew1;
	protected HtmlGraphicImageEx imageExNew1;
	protected HtmlDataTableEx tableExNewOfferTerms;
	protected UIColumnEx columnExNew1;
	protected HtmlOutputText textNew3;
	protected HtmlOutputText textNew7;
	protected UIColumnEx columnExNew2;
	protected HtmlOutputText textNew4;
	protected HtmlOutputText textNew8;
	protected UIColumnEx columnExNew3;
	protected HtmlOutputText textNew5;
	protected HtmlOutputText textNew9;
	protected UIColumnEx columnExNew4;
	protected HtmlOutputText textNew6;
	protected HtmlOutputText textNew10;
	protected HtmlPanelSection sectionNewExpiredTermsDetail;
	protected HtmlJspPanel jspPanelExpiredNew2;
	protected HtmlGraphicImageEx imageExExpiredNew2;
	protected HtmlOutputText textExpiredNew2;
	protected HtmlJspPanel jspPanelExpiredNew1;
	protected HtmlGraphicImageEx imageExExpiredNew1;
	protected HtmlOutputText textExpiredNew1;
	protected HtmlDataTableEx tableExNewExpiredOfferTerms;
	protected UIColumnEx columnExExpiredNew1;
	protected HtmlOutputText textExpiredNew3;
	protected HtmlOutputText textExpiredNew7;
	protected UIColumnEx columnExExpiredNew2;
	protected HtmlOutputText textExpiredNew4;
	protected HtmlOutputText textExpiredNew8;
	protected UIColumnEx columnExExpiredNew3;
	protected HtmlOutputText textExpiredNew5;
	protected HtmlOutputText textExpiredNew9;
	protected UIColumnEx columnExExpiredNew4;
	protected HtmlOutputText textExpiredNew6;
	protected HtmlOutputText textExpiredNew10;
	protected HtmlOutputText text13;
	protected HtmlAjaxRefreshSubmit ajaxRefreshSubmit1;
	protected HtmlBehavior behavior2;
	protected HtmlBehavior behavior1;
	protected HtmlCommandExButton buttonSaveChange;
	protected HtmlMessages messages1;
	protected HtmlOutputText text11;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlInputHelperAssist assist1;
	protected HtmlInputHelperAssist assist2;
	protected HtmlPanelBox box1;
	protected HtmlMessage message1;
	protected HtmlPanelBox box2;
	protected HtmlMessage message2;
	protected HtmlOutputText text15;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text16;
	protected HtmlOutputText text17;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text18;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelFormBox getFormBoxNewKeycode() {
		if (formBoxNewKeycode == null) {
			formBoxNewKeycode = (HtmlPanelFormBox) findComponentInRoot("formBoxNewKeycode");
		}
		return formBoxNewKeycode;
	}

	protected HtmlOutputSeparator getSeparator1() {
		if (separator1 == null) {
			separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
		}
		return separator1;
	}

	protected HtmlFormItem getFormItemNewKeycode1() {
		if (formItemNewKeycode1 == null) {
			formItemNewKeycode1 = (HtmlFormItem) findComponentInRoot("formItemNewKeycode1");
		}
		return formItemNewKeycode1;
	}

	protected HtmlPanelGrid getGridKeycode() {
		if (gridKeycode == null) {
			gridKeycode = (HtmlPanelGrid) findComponentInRoot("gridKeycode");
		}
		return gridKeycode;
	}

	/** 
	 * @managed-bean true
	 */
	protected DefaultKeycodeChangeHandler getDefaultKeycodeChangeHandler() {
		if (defaultKeycodeChangeHandler == null) {
			defaultKeycodeChangeHandler = (DefaultKeycodeChangeHandler) getManagedBean("defaultKeycodeChangeHandler");
		}
		return defaultKeycodeChangeHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setDefaultKeycodeChangeHandler(
			DefaultKeycodeChangeHandler defaultKeycodeChangeHandler) {
		this.defaultKeycodeChangeHandler = defaultKeycodeChangeHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	
		// void <method>(FacesContext facescontext)
		
		HttpServletRequest request = (HttpServletRequest)facescontext.getExternalContext().getRequest();
		
		String pub = request.getParameter("product");
		
		if (pub != null) {
			try {
				USATProductBO prod = USATProductBO.fetchProduct(pub);
				
				ProductHandler ph = new ProductHandler();
				ph.setProduct(prod);
				
				this.getDefaultKeycodeChangeHandler().setCurrentProduct(ph);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlPanelSection getSectionCurrentTermsDetail() {
		if (sectionCurrentTermsDetail == null) {
			sectionCurrentTermsDetail = (HtmlPanelSection) findComponentInRoot("sectionCurrentTermsDetail");
		}
		return sectionCurrentTermsDetail;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getGridInside11() {
		if (gridInside11 == null) {
			gridInside11 = (HtmlPanelGrid) findComponentInRoot("gridInside11");
		}
		return gridInside11;
	}

	protected HtmlOutputText getTextCurrentDefaultForPubLabel() {
		if (textCurrentDefaultForPubLabel == null) {
			textCurrentDefaultForPubLabel = (HtmlOutputText) findComponentInRoot("textCurrentDefaultForPubLabel");
		}
		return textCurrentDefaultForPubLabel;
	}

	protected HtmlInputText getTextCurrentDefaultKeycode() {
		if (textCurrentDefaultKeycode == null) {
			textCurrentDefaultKeycode = (HtmlInputText) findComponentInRoot("textCurrentDefaultKeycode");
		}
		return textCurrentDefaultKeycode;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlDataTableEx getTableExCurrentOfferTerms() {
		if (tableExCurrentOfferTerms == null) {
			tableExCurrentOfferTerms = (HtmlDataTableEx) findComponentInRoot("tableExCurrentOfferTerms");
		}
		return tableExCurrentOfferTerms;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlOutputText getTextCurrentExpiredDefaultForPubLabel() {
		if (textCurrentExpiredDefaultForPubLabel == null) {
			textCurrentExpiredDefaultForPubLabel = (HtmlOutputText) findComponentInRoot("textCurrentExpiredDefaultForPubLabel");
		}
		return textCurrentExpiredDefaultForPubLabel;
	}

	protected HtmlPanelGrid getGridInside1111() {
		if (gridInside1111 == null) {
			gridInside1111 = (HtmlPanelGrid) findComponentInRoot("gridInside1111");
		}
		return gridInside1111;
	}

	protected HtmlInputText getTextCurrentDefaultExpiredKeycode() {
		if (textCurrentDefaultExpiredKeycode == null) {
			textCurrentDefaultExpiredKeycode = (HtmlInputText) findComponentInRoot("textCurrentDefaultExpiredKeycode");
		}
		return textCurrentDefaultExpiredKeycode;
	}

	protected HtmlPanelSection getSectionCurrentExpiredTermsDetail() {
		if (sectionCurrentExpiredTermsDetail == null) {
			sectionCurrentExpiredTermsDetail = (HtmlPanelSection) findComponentInRoot("sectionCurrentExpiredTermsDetail");
		}
		return sectionCurrentExpiredTermsDetail;
	}

	protected HtmlJspPanel getJspPanelExpired2() {
		if (jspPanelExpired2 == null) {
			jspPanelExpired2 = (HtmlJspPanel) findComponentInRoot("jspPanelExpired2");
		}
		return jspPanelExpired2;
	}

	protected HtmlGraphicImageEx getImageExExpired2() {
		if (imageExExpired2 == null) {
			imageExExpired2 = (HtmlGraphicImageEx) findComponentInRoot("imageExExpired2");
		}
		return imageExExpired2;
	}

	protected HtmlOutputText getTextExpired2() {
		if (textExpired2 == null) {
			textExpired2 = (HtmlOutputText) findComponentInRoot("textExpired2");
		}
		return textExpired2;
	}

	protected HtmlJspPanel getJspPanelExpired1() {
		if (jspPanelExpired1 == null) {
			jspPanelExpired1 = (HtmlJspPanel) findComponentInRoot("jspPanelExpired1");
		}
		return jspPanelExpired1;
	}

	protected HtmlGraphicImageEx getImageExExpired1() {
		if (imageExExpired1 == null) {
			imageExExpired1 = (HtmlGraphicImageEx) findComponentInRoot("imageExExpired1");
		}
		return imageExExpired1;
	}

	protected HtmlOutputText getTextExpired1() {
		if (textExpired1 == null) {
			textExpired1 = (HtmlOutputText) findComponentInRoot("textExpired1");
		}
		return textExpired1;
	}

	protected HtmlDataTableEx getTableExCurrentExpiredOfferTerms() {
		if (tableExCurrentExpiredOfferTerms == null) {
			tableExCurrentExpiredOfferTerms = (HtmlDataTableEx) findComponentInRoot("tableExCurrentExpiredOfferTerms");
		}
		return tableExCurrentExpiredOfferTerms;
	}

	protected UIColumnEx getColumnExExpired1() {
		if (columnExExpired1 == null) {
			columnExExpired1 = (UIColumnEx) findComponentInRoot("columnExExpired1");
		}
		return columnExExpired1;
	}

	protected HtmlOutputText getTextExpired3() {
		if (textExpired3 == null) {
			textExpired3 = (HtmlOutputText) findComponentInRoot("textExpired3");
		}
		return textExpired3;
	}

	protected HtmlOutputText getTextExpired7() {
		if (textExpired7 == null) {
			textExpired7 = (HtmlOutputText) findComponentInRoot("textExpired7");
		}
		return textExpired7;
	}

	protected UIColumnEx getColumnExExpired2() {
		if (columnExExpired2 == null) {
			columnExExpired2 = (UIColumnEx) findComponentInRoot("columnExExpired2");
		}
		return columnExExpired2;
	}

	protected HtmlOutputText getTextExpired4() {
		if (textExpired4 == null) {
			textExpired4 = (HtmlOutputText) findComponentInRoot("textExpired4");
		}
		return textExpired4;
	}

	protected HtmlOutputText getTextExpired8() {
		if (textExpired8 == null) {
			textExpired8 = (HtmlOutputText) findComponentInRoot("textExpired8");
		}
		return textExpired8;
	}

	protected UIColumnEx getColumnExExpired3() {
		if (columnExExpired3 == null) {
			columnExExpired3 = (UIColumnEx) findComponentInRoot("columnExExpired3");
		}
		return columnExExpired3;
	}

	protected HtmlOutputText getTextExpired5() {
		if (textExpired5 == null) {
			textExpired5 = (HtmlOutputText) findComponentInRoot("textExpired5");
		}
		return textExpired5;
	}

	protected HtmlOutputText getTextExpired9() {
		if (textExpired9 == null) {
			textExpired9 = (HtmlOutputText) findComponentInRoot("textExpired9");
		}
		return textExpired9;
	}

	protected UIColumnEx getColumnExExpired4() {
		if (columnExExpired4 == null) {
			columnExExpired4 = (UIColumnEx) findComponentInRoot("columnExExpired4");
		}
		return columnExExpired4;
	}

	protected HtmlOutputText getTextExpired6() {
		if (textExpired6 == null) {
			textExpired6 = (HtmlOutputText) findComponentInRoot("textExpired6");
		}
		return textExpired6;
	}

	protected HtmlOutputText getTextExpired10() {
		if (textExpired10 == null) {
			textExpired10 = (HtmlOutputText) findComponentInRoot("textExpired10");
		}
		return textExpired10;
	}

	protected HtmlInputText getText12NewKeycode() {
		if (text12NewKeycode == null) {
			text12NewKeycode = (HtmlInputText) findComponentInRoot("text12NewKeycode");
		}
		return text12NewKeycode;
	}

	protected HtmlFormItem getFormItemNewKeycode2() {
		if (formItemNewKeycode2 == null) {
			formItemNewKeycode2 = (HtmlFormItem) findComponentInRoot("formItemNewKeycode2");
		}
		return formItemNewKeycode2;
	}

	protected HtmlInputText getText12() {
		if (text12 == null) {
			text12 = (HtmlInputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlPanelGrid getGridNewOfferGrid() {
		if (gridNewOfferGrid == null) {
			gridNewOfferGrid = (HtmlPanelGrid) findComponentInRoot("gridNewOfferGrid");
		}
		return gridNewOfferGrid;
	}

	protected HtmlPanelSection getSectionNewTermsDetail() {
		if (sectionNewTermsDetail == null) {
			sectionNewTermsDetail = (HtmlPanelSection) findComponentInRoot("sectionNewTermsDetail");
		}
		return sectionNewTermsDetail;
	}

	protected HtmlJspPanel getJspPanelNew2() {
		if (jspPanelNew2 == null) {
			jspPanelNew2 = (HtmlJspPanel) findComponentInRoot("jspPanelNew2");
		}
		return jspPanelNew2;
	}

	protected HtmlGraphicImageEx getImageExNew2() {
		if (imageExNew2 == null) {
			imageExNew2 = (HtmlGraphicImageEx) findComponentInRoot("imageExNew2");
		}
		return imageExNew2;
	}

	protected HtmlOutputText getTextNew2() {
		if (textNew2 == null) {
			textNew2 = (HtmlOutputText) findComponentInRoot("textNew2");
		}
		return textNew2;
	}

	protected HtmlJspPanel getJspPanelNew1() {
		if (jspPanelNew1 == null) {
			jspPanelNew1 = (HtmlJspPanel) findComponentInRoot("jspPanelNew1");
		}
		return jspPanelNew1;
	}

	protected HtmlGraphicImageEx getImageExNew1() {
		if (imageExNew1 == null) {
			imageExNew1 = (HtmlGraphicImageEx) findComponentInRoot("imageExNew1");
		}
		return imageExNew1;
	}

	protected HtmlDataTableEx getTableExNewOfferTerms() {
		if (tableExNewOfferTerms == null) {
			tableExNewOfferTerms = (HtmlDataTableEx) findComponentInRoot("tableExNewOfferTerms");
		}
		return tableExNewOfferTerms;
	}

	protected UIColumnEx getColumnExNew1() {
		if (columnExNew1 == null) {
			columnExNew1 = (UIColumnEx) findComponentInRoot("columnExNew1");
		}
		return columnExNew1;
	}

	protected HtmlOutputText getTextNew3() {
		if (textNew3 == null) {
			textNew3 = (HtmlOutputText) findComponentInRoot("textNew3");
		}
		return textNew3;
	}

	protected HtmlOutputText getTextNew7() {
		if (textNew7 == null) {
			textNew7 = (HtmlOutputText) findComponentInRoot("textNew7");
		}
		return textNew7;
	}

	protected UIColumnEx getColumnExNew2() {
		if (columnExNew2 == null) {
			columnExNew2 = (UIColumnEx) findComponentInRoot("columnExNew2");
		}
		return columnExNew2;
	}

	protected HtmlOutputText getTextNew4() {
		if (textNew4 == null) {
			textNew4 = (HtmlOutputText) findComponentInRoot("textNew4");
		}
		return textNew4;
	}

	protected HtmlOutputText getTextNew8() {
		if (textNew8 == null) {
			textNew8 = (HtmlOutputText) findComponentInRoot("textNew8");
		}
		return textNew8;
	}

	protected UIColumnEx getColumnExNew3() {
		if (columnExNew3 == null) {
			columnExNew3 = (UIColumnEx) findComponentInRoot("columnExNew3");
		}
		return columnExNew3;
	}

	protected HtmlOutputText getTextNew5() {
		if (textNew5 == null) {
			textNew5 = (HtmlOutputText) findComponentInRoot("textNew5");
		}
		return textNew5;
	}

	protected HtmlOutputText getTextNew9() {
		if (textNew9 == null) {
			textNew9 = (HtmlOutputText) findComponentInRoot("textNew9");
		}
		return textNew9;
	}

	protected UIColumnEx getColumnExNew4() {
		if (columnExNew4 == null) {
			columnExNew4 = (UIColumnEx) findComponentInRoot("columnExNew4");
		}
		return columnExNew4;
	}

	protected HtmlOutputText getTextNew6() {
		if (textNew6 == null) {
			textNew6 = (HtmlOutputText) findComponentInRoot("textNew6");
		}
		return textNew6;
	}

	protected HtmlOutputText getTextNew10() {
		if (textNew10 == null) {
			textNew10 = (HtmlOutputText) findComponentInRoot("textNew10");
		}
		return textNew10;
	}

	protected HtmlPanelSection getSectionNewExpiredTermsDetail() {
		if (sectionNewExpiredTermsDetail == null) {
			sectionNewExpiredTermsDetail = (HtmlPanelSection) findComponentInRoot("sectionNewExpiredTermsDetail");
		}
		return sectionNewExpiredTermsDetail;
	}

	protected HtmlJspPanel getJspPanelExpiredNew2() {
		if (jspPanelExpiredNew2 == null) {
			jspPanelExpiredNew2 = (HtmlJspPanel) findComponentInRoot("jspPanelExpiredNew2");
		}
		return jspPanelExpiredNew2;
	}

	protected HtmlGraphicImageEx getImageExExpiredNew2() {
		if (imageExExpiredNew2 == null) {
			imageExExpiredNew2 = (HtmlGraphicImageEx) findComponentInRoot("imageExExpiredNew2");
		}
		return imageExExpiredNew2;
	}

	protected HtmlOutputText getTextExpiredNew2() {
		if (textExpiredNew2 == null) {
			textExpiredNew2 = (HtmlOutputText) findComponentInRoot("textExpiredNew2");
		}
		return textExpiredNew2;
	}

	protected HtmlJspPanel getJspPanelExpiredNew1() {
		if (jspPanelExpiredNew1 == null) {
			jspPanelExpiredNew1 = (HtmlJspPanel) findComponentInRoot("jspPanelExpiredNew1");
		}
		return jspPanelExpiredNew1;
	}

	protected HtmlGraphicImageEx getImageExExpiredNew1() {
		if (imageExExpiredNew1 == null) {
			imageExExpiredNew1 = (HtmlGraphicImageEx) findComponentInRoot("imageExExpiredNew1");
		}
		return imageExExpiredNew1;
	}

	protected HtmlOutputText getTextExpiredNew1() {
		if (textExpiredNew1 == null) {
			textExpiredNew1 = (HtmlOutputText) findComponentInRoot("textExpiredNew1");
		}
		return textExpiredNew1;
	}

	protected HtmlDataTableEx getTableExNewExpiredOfferTerms() {
		if (tableExNewExpiredOfferTerms == null) {
			tableExNewExpiredOfferTerms = (HtmlDataTableEx) findComponentInRoot("tableExNewExpiredOfferTerms");
		}
		return tableExNewExpiredOfferTerms;
	}

	protected UIColumnEx getColumnExExpiredNew1() {
		if (columnExExpiredNew1 == null) {
			columnExExpiredNew1 = (UIColumnEx) findComponentInRoot("columnExExpiredNew1");
		}
		return columnExExpiredNew1;
	}

	protected HtmlOutputText getTextExpiredNew3() {
		if (textExpiredNew3 == null) {
			textExpiredNew3 = (HtmlOutputText) findComponentInRoot("textExpiredNew3");
		}
		return textExpiredNew3;
	}

	protected HtmlOutputText getTextExpiredNew7() {
		if (textExpiredNew7 == null) {
			textExpiredNew7 = (HtmlOutputText) findComponentInRoot("textExpiredNew7");
		}
		return textExpiredNew7;
	}

	protected UIColumnEx getColumnExExpiredNew2() {
		if (columnExExpiredNew2 == null) {
			columnExExpiredNew2 = (UIColumnEx) findComponentInRoot("columnExExpiredNew2");
		}
		return columnExExpiredNew2;
	}

	protected HtmlOutputText getTextExpiredNew4() {
		if (textExpiredNew4 == null) {
			textExpiredNew4 = (HtmlOutputText) findComponentInRoot("textExpiredNew4");
		}
		return textExpiredNew4;
	}

	protected HtmlOutputText getTextExpiredNew8() {
		if (textExpiredNew8 == null) {
			textExpiredNew8 = (HtmlOutputText) findComponentInRoot("textExpiredNew8");
		}
		return textExpiredNew8;
	}

	protected UIColumnEx getColumnExExpiredNew3() {
		if (columnExExpiredNew3 == null) {
			columnExExpiredNew3 = (UIColumnEx) findComponentInRoot("columnExExpiredNew3");
		}
		return columnExExpiredNew3;
	}

	protected HtmlOutputText getTextExpiredNew5() {
		if (textExpiredNew5 == null) {
			textExpiredNew5 = (HtmlOutputText) findComponentInRoot("textExpiredNew5");
		}
		return textExpiredNew5;
	}

	protected HtmlOutputText getTextExpiredNew9() {
		if (textExpiredNew9 == null) {
			textExpiredNew9 = (HtmlOutputText) findComponentInRoot("textExpiredNew9");
		}
		return textExpiredNew9;
	}

	protected UIColumnEx getColumnExExpiredNew4() {
		if (columnExExpiredNew4 == null) {
			columnExExpiredNew4 = (UIColumnEx) findComponentInRoot("columnExExpiredNew4");
		}
		return columnExExpiredNew4;
	}

	protected HtmlOutputText getTextExpiredNew6() {
		if (textExpiredNew6 == null) {
			textExpiredNew6 = (HtmlOutputText) findComponentInRoot("textExpiredNew6");
		}
		return textExpiredNew6;
	}

	protected HtmlOutputText getTextExpiredNew10() {
		if (textExpiredNew10 == null) {
			textExpiredNew10 = (HtmlOutputText) findComponentInRoot("textExpiredNew10");
		}
		return textExpiredNew10;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected HtmlAjaxRefreshSubmit getAjaxRefreshSubmit1() {
		if (ajaxRefreshSubmit1 == null) {
			ajaxRefreshSubmit1 = (HtmlAjaxRefreshSubmit) findComponentInRoot("ajaxRefreshSubmit1");
		}
		return ajaxRefreshSubmit1;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	protected HtmlCommandExButton getButtonSaveChange() {
		if (buttonSaveChange == null) {
			buttonSaveChange = (HtmlCommandExButton) findComponentInRoot("buttonSaveChange");
		}
		return buttonSaveChange;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	public String doButtonSaveChangeAction() {
		// This is java code that runs when this action method is invoked
		String response = "success";
		
		SubscriptionOfferIntf newOffer = this.getDefaultKeycodeChangeHandler().getNewOffer();
		if (newOffer == null && this.getDefaultKeycodeChangeHandler().getNewKeycode() != null && this.getDefaultKeycodeChangeHandler().getNewKeycode().length() == 5) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "New Default Offer Keycode: " + this.getDefaultKeycodeChangeHandler().getNewKeycode() , null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			response = "failure";
		}

		SubscriptionOfferIntf newExpiredOffer = this.getDefaultKeycodeChangeHandler().getNewExpiredOffer();
		if (newExpiredOffer == null && this.getDefaultKeycodeChangeHandler().getNewExpiredKeycode() != null && this.getDefaultKeycodeChangeHandler().getNewExpiredKeycode().length() == 5) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "New Default Expired Offer Keycode: " + this.getDefaultKeycodeChangeHandler().getNewExpiredKeycode() , null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			response = "failure";
		}

		return response;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}

	protected HtmlPanelBox getBox2() {
		if (box2 == null) {
			box2 = (HtmlPanelBox) findComponentInRoot("box2");
		}
		return box2;
	}

	protected HtmlMessage getMessage2() {
		if (message2 == null) {
			message2 = (HtmlMessage) findComponentInRoot("message2");
		}
		return message2;
	}

	public String doLink1Action() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		// return "success";
		return "success";
	}

	public String doLink11Action() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		// return "success";
		return "success";
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

}