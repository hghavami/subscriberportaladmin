/**
 * 
 */
package pagecode.secured.defaultKeycode;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.UIParameter;
import com.gannett.usat.dataHandlers.products.DefaultKeycodeChangeHandler;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

/**
 * @author aeast
 *
 */
public class Change_complete extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputFormat format1;
	protected UIParameter param1;
	protected DefaultKeycodeChangeHandler defaultKeycodeChangeHandler;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputText text1;
	protected HtmlOutputText text2;
	protected HtmlOutputLinkEx linkEx2;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputFormat getFormat1() {
		if (format1 == null) {
			format1 = (HtmlOutputFormat) findComponentInRoot("format1");
		}
		return format1;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	/** 
	 * @managed-bean true
	 */
	protected DefaultKeycodeChangeHandler getDefaultKeycodeChangeHandler() {
		if (defaultKeycodeChangeHandler == null) {
			defaultKeycodeChangeHandler = (DefaultKeycodeChangeHandler) getManagedBean("defaultKeycodeChangeHandler");
		}
		return defaultKeycodeChangeHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setDefaultKeycodeChangeHandler(
			DefaultKeycodeChangeHandler defaultKeycodeChangeHandler) {
		this.defaultKeycodeChangeHandler = defaultKeycodeChangeHandler;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputLinkEx getLinkEx2() {
		if (linkEx2 == null) {
			linkEx2 = (HtmlOutputLinkEx) findComponentInRoot("linkEx2");
		}
		return linkEx2;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	
		// void <method>(FacesContext facescontext)
		
		this.getDefaultKeycodeChangeHandler().setNewExpiredKeycode(null);
		this.getDefaultKeycodeChangeHandler().setNewKeycode(null);
	}

}