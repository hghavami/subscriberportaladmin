/*
 * Created on Aug 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode.secured;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.gannett.usat.dataHandlers.campaigns.CampaignStatisticsHandler;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.UINamingContainer;
import javax.faces.component.html.HtmlMessage;
import com.ibm.faces.component.html.HtmlBehaviorKeyPress;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.gannett.usat.dataHandlers.campaigns.BasicSearchHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Home extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorHomePage;
	protected HtmlOutputText textWelcomeText;
	protected HtmlForm formCampaignHome;
	protected HtmlPanelSection sectionCoverGraphics;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageExCoverGraphCol;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageExCovGraphicExp;
	protected HtmlOutputText text9;
	protected HtmlOutputText text8;
	protected HtmlPanelGrid gridLeftPanel;
	protected HtmlPanelGrid gridRightPanelGrid;
	protected HtmlJspPanel jspPanelUsatImgHeader;
	protected HtmlRequestLink linkUploadNewUT;
	protected HtmlOutputText text5;
	protected HtmlGraphicImageEx imageExUTCover;
	protected HtmlJspPanel jspPanel3;
	protected HtmlRequestLink linkSWCoverGraphicUpload;
	protected HtmlOutputText text6;
	protected HtmlPanelGrid gridInsideGrid;
	protected HtmlPanelBox boxleftBoxGraphic;
	protected HtmlPanelBox boxPanelBoxRightCovGraphic;
	protected HtmlGraphicImageEx imageExBWDefaultCover;
	protected HtmlScriptCollector scriptCollectorJSPC02blue;
	protected HtmlJspPanel jspPanelLeftNavCheckProdPanel;
	protected HtmlOutputLinkEx linkExLeftNav1;
	protected HtmlOutputText textLeftNav1;
	protected HtmlOutputLinkEx linkExLeftNav2;
	protected HtmlOutputText textLeftNav2;
	protected HtmlOutputLinkEx linkExLeftNavToOmniture;
	protected HtmlOutputText textLeftNav4;
	protected HtmlJspPanel jspPanelLeftNav5;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdCol;
	protected HtmlJspPanel jspPanelLeftNav4;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdExp;
	protected HtmlPanelSection sectionLeftNavCheckProdSection;
	protected HtmlOutputText textLeftNav11;
	protected HtmlOutputText textLeftNav10;
	protected HtmlOutputLinkEx linkExLeftNavToClearProdCache;
	protected HtmlOutputText textLeftNavClearCacheLabel;
	protected HtmlPanelSection sectionCampManagementSection;
	protected HtmlJspPanel jspPanelCampManagementPanel;
	protected HtmlJspPanel jspPanel11;
	protected HtmlGraphicImageEx imageExCampManCol;
	protected HtmlJspPanel jspPanel10;
	protected HtmlGraphicImageEx imageExCampManExp;
	protected HtmlOutputText text17;
	protected HtmlOutputText text16;
	protected HtmlOutputText textNumCampaignsLabel;
	protected HtmlGraphicImageEx imageExSearchCampsImageOpen;
	protected HtmlInputText textNumCampaigns;
	protected HtmlOutputLinkEx linkExToAdvancedSearch;
	protected UIColumnEx columnExCampCountDGridCol1;
	protected HtmlOutputText columnExCampCountDGridCol1Header;
	protected HtmlDataTableEx tableExCampaignCountsDGrid;
	protected UIColumnEx columnEx1;
	protected CampaignStatisticsHandler campaignStatisticsHandler;
	protected HtmlInputText text1;
	protected HtmlPanelBox box1;
	protected HtmlCommandExButton buttonRefreshStats;
	protected UINamingContainer subviewBasicSearch;
	protected HtmlScriptCollector scriptCollectorBasicSearchForm;
	protected HtmlForm formBasicSearchForm;
	protected HtmlOutputText textBasicSearchLabel;
	protected HtmlInputText textBasicSearchCriteria;
	protected HtmlOutputText textAdvSearchLabel;
	protected HtmlMessage message1;
	protected HtmlCommandExButton buttonBasicSearch;
	protected HtmlOutputLinkEx linkExAdvSearchLink;
	protected HtmlBehaviorKeyPress behaviorKeyPress1;
	protected HtmlInputHelperSetFocus setFocus1;
	protected BasicSearchHandler basicSearchCriteria;
	protected SearchResultsHandler currentSearchResults;
	protected HtmlScriptCollector getScriptCollectorHomePage() {
		if (scriptCollectorHomePage == null) {
			scriptCollectorHomePage = (HtmlScriptCollector) findComponentInRoot("scriptCollectorHomePage");
		}
		return scriptCollectorHomePage;
	}
	protected HtmlOutputText getTextWelcomeText() {
		if (textWelcomeText == null) {
			textWelcomeText = (HtmlOutputText) findComponentInRoot("textWelcomeText");
		}
		return textWelcomeText;
	}
	protected HtmlForm getFormCampaignHome() {
		if (formCampaignHome == null) {
			formCampaignHome = (HtmlForm) findComponentInRoot("formCampaignHome");
		}
		return formCampaignHome;
	}
	protected HtmlPanelSection getSectionCoverGraphics() {
		if (sectionCoverGraphics == null) {
			sectionCoverGraphics = (HtmlPanelSection) findComponentInRoot("sectionCoverGraphics");
		}
		return sectionCoverGraphics;
	}
	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}
	protected HtmlGraphicImageEx getImageExCoverGraphCol() {
		if (imageExCoverGraphCol == null) {
			imageExCoverGraphCol = (HtmlGraphicImageEx) findComponentInRoot("imageExCoverGraphCol");
		}
		return imageExCoverGraphCol;
	}
	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}
	protected HtmlGraphicImageEx getImageExCovGraphicExp() {
		if (imageExCovGraphicExp == null) {
			imageExCovGraphicExp = (HtmlGraphicImageEx) findComponentInRoot("imageExCovGraphicExp");
		}
		return imageExCovGraphicExp;
	}
	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}
	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}
	protected HtmlPanelGrid getGridLeftPanel() {
		if (gridLeftPanel == null) {
			gridLeftPanel = (HtmlPanelGrid) findComponentInRoot("gridLeftPanel");
		}
		return gridLeftPanel;
	}
	protected HtmlPanelGrid getGridRightPanelGrid() {
		if (gridRightPanelGrid == null) {
			gridRightPanelGrid = (HtmlPanelGrid) findComponentInRoot("gridRightPanelGrid");
		}
		return gridRightPanelGrid;
	}
	protected HtmlJspPanel getJspPanelUsatImgHeader() {
		if (jspPanelUsatImgHeader == null) {
			jspPanelUsatImgHeader = (HtmlJspPanel) findComponentInRoot("jspPanelUsatImgHeader");
		}
		return jspPanelUsatImgHeader;
	}
	protected HtmlRequestLink getLinkUploadNewUT() {
		if (linkUploadNewUT == null) {
			linkUploadNewUT = (HtmlRequestLink) findComponentInRoot("linkUploadNewUT");
		}
		return linkUploadNewUT;
	}
	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}
	protected HtmlGraphicImageEx getImageExUTCover() {
		if (imageExUTCover == null) {
			imageExUTCover = (HtmlGraphicImageEx) findComponentInRoot("imageExUTCover");
		}
		return imageExUTCover;
	}
	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}
	protected HtmlRequestLink getLinkSWCoverGraphicUpload() {
		if (linkSWCoverGraphicUpload == null) {
			linkSWCoverGraphicUpload = (HtmlRequestLink) findComponentInRoot("linkSWCoverGraphicUpload");
		}
		return linkSWCoverGraphicUpload;
	}
	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}
	protected HtmlPanelGrid getGridInsideGrid() {
		if (gridInsideGrid == null) {
			gridInsideGrid = (HtmlPanelGrid) findComponentInRoot("gridInsideGrid");
		}
		return gridInsideGrid;
	}
	protected HtmlPanelBox getBoxleftBoxGraphic() {
		if (boxleftBoxGraphic == null) {
			boxleftBoxGraphic = (HtmlPanelBox) findComponentInRoot("boxleftBoxGraphic");
		}
		return boxleftBoxGraphic;
	}
	protected HtmlPanelBox getBoxPanelBoxRightCovGraphic() {
		if (boxPanelBoxRightCovGraphic == null) {
			boxPanelBoxRightCovGraphic = (HtmlPanelBox) findComponentInRoot("boxPanelBoxRightCovGraphic");
		}
		return boxPanelBoxRightCovGraphic;
	}
	protected HtmlGraphicImageEx getImageExBWDefaultCover() {
		if (imageExBWDefaultCover == null) {
			imageExBWDefaultCover = (HtmlGraphicImageEx) findComponentInRoot("imageExBWDefaultCover");
		}
		return imageExBWDefaultCover;
	}
	protected HtmlScriptCollector getScriptCollectorJSPC02blue() {
		if (scriptCollectorJSPC02blue == null) {
			scriptCollectorJSPC02blue = (HtmlScriptCollector) findComponentInRoot("scriptCollectorJSPC02blue");
		}
		return scriptCollectorJSPC02blue;
	}
	protected HtmlJspPanel getJspPanelLeftNavCheckProdPanel() {
		if (jspPanelLeftNavCheckProdPanel == null) {
			jspPanelLeftNavCheckProdPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNavCheckProdPanel");
		}
		return jspPanelLeftNavCheckProdPanel;
	}
	protected HtmlOutputLinkEx getLinkExLeftNav1() {
		if (linkExLeftNav1 == null) {
			linkExLeftNav1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav1");
		}
		return linkExLeftNav1;
	}
	protected HtmlOutputText getTextLeftNav1() {
		if (textLeftNav1 == null) {
			textLeftNav1 = (HtmlOutputText) findComponentInRoot("textLeftNav1");
		}
		return textLeftNav1;
	}
	protected HtmlOutputLinkEx getLinkExLeftNav2() {
		if (linkExLeftNav2 == null) {
			linkExLeftNav2 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav2");
		}
		return linkExLeftNav2;
	}
	protected HtmlOutputText getTextLeftNav2() {
		if (textLeftNav2 == null) {
			textLeftNav2 = (HtmlOutputText) findComponentInRoot("textLeftNav2");
		}
		return textLeftNav2;
	}
	protected HtmlOutputLinkEx getLinkExLeftNavToOmniture() {
		if (linkExLeftNavToOmniture == null) {
			linkExLeftNavToOmniture = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToOmniture");
		}
		return linkExLeftNavToOmniture;
	}
	protected HtmlOutputText getTextLeftNav4() {
		if (textLeftNav4 == null) {
			textLeftNav4 = (HtmlOutputText) findComponentInRoot("textLeftNav4");
		}
		return textLeftNav4;
	}
	protected HtmlJspPanel getJspPanelLeftNav5() {
		if (jspPanelLeftNav5 == null) {
			jspPanelLeftNav5 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav5");
		}
		return jspPanelLeftNav5;
	}
	protected HtmlGraphicImageEx getImageExLeftNavCheckProdCol() {
		if (imageExLeftNavCheckProdCol == null) {
			imageExLeftNavCheckProdCol = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdCol");
		}
		return imageExLeftNavCheckProdCol;
	}
	protected HtmlJspPanel getJspPanelLeftNav4() {
		if (jspPanelLeftNav4 == null) {
			jspPanelLeftNav4 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav4");
		}
		return jspPanelLeftNav4;
	}
	protected HtmlGraphicImageEx getImageExLeftNavCheckProdExp() {
		if (imageExLeftNavCheckProdExp == null) {
			imageExLeftNavCheckProdExp = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdExp");
		}
		return imageExLeftNavCheckProdExp;
	}
	protected HtmlPanelSection getSectionLeftNavCheckProdSection() {
		if (sectionLeftNavCheckProdSection == null) {
			sectionLeftNavCheckProdSection = (HtmlPanelSection) findComponentInRoot("sectionLeftNavCheckProdSection");
		}
		return sectionLeftNavCheckProdSection;
	}
	protected HtmlOutputText getTextLeftNav11() {
		if (textLeftNav11 == null) {
			textLeftNav11 = (HtmlOutputText) findComponentInRoot("textLeftNav11");
		}
		return textLeftNav11;
	}
	protected HtmlOutputText getTextLeftNav10() {
		if (textLeftNav10 == null) {
			textLeftNav10 = (HtmlOutputText) findComponentInRoot("textLeftNav10");
		}
		return textLeftNav10;
	}
	protected HtmlOutputLinkEx getLinkExLeftNavToClearProdCache() {
		if (linkExLeftNavToClearProdCache == null) {
			linkExLeftNavToClearProdCache = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToClearProdCache");
		}
		return linkExLeftNavToClearProdCache;
	}
	protected HtmlOutputText getTextLeftNavClearCacheLabel() {
		if (textLeftNavClearCacheLabel == null) {
			textLeftNavClearCacheLabel = (HtmlOutputText) findComponentInRoot("textLeftNavClearCacheLabel");
		}
		return textLeftNavClearCacheLabel;
	}
	protected HtmlPanelSection getSectionCampManagementSection() {
		if (sectionCampManagementSection == null) {
			sectionCampManagementSection = (HtmlPanelSection) findComponentInRoot("sectionCampManagementSection");
		}
		return sectionCampManagementSection;
	}
	protected HtmlJspPanel getJspPanelCampManagementPanel() {
		if (jspPanelCampManagementPanel == null) {
			jspPanelCampManagementPanel = (HtmlJspPanel) findComponentInRoot("jspPanelCampManagementPanel");
		}
		return jspPanelCampManagementPanel;
	}
	protected HtmlJspPanel getJspPanel11() {
		if (jspPanel11 == null) {
			jspPanel11 = (HtmlJspPanel) findComponentInRoot("jspPanel11");
		}
		return jspPanel11;
	}
	protected HtmlGraphicImageEx getImageExCampManCol() {
		if (imageExCampManCol == null) {
			imageExCampManCol = (HtmlGraphicImageEx) findComponentInRoot("imageExCampManCol");
		}
		return imageExCampManCol;
	}
	protected HtmlJspPanel getJspPanel10() {
		if (jspPanel10 == null) {
			jspPanel10 = (HtmlJspPanel) findComponentInRoot("jspPanel10");
		}
		return jspPanel10;
	}
	protected HtmlGraphicImageEx getImageExCampManExp() {
		if (imageExCampManExp == null) {
			imageExCampManExp = (HtmlGraphicImageEx) findComponentInRoot("imageExCampManExp");
		}
		return imageExCampManExp;
	}
	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}
	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}
	protected HtmlOutputText getTextNumCampaignsLabel() {
		if (textNumCampaignsLabel == null) {
			textNumCampaignsLabel = (HtmlOutputText) findComponentInRoot("textNumCampaignsLabel");
		}
		return textNumCampaignsLabel;
	}
	protected HtmlGraphicImageEx getImageExSearchCampsImageOpen() {
		if (imageExSearchCampsImageOpen == null) {
			imageExSearchCampsImageOpen = (HtmlGraphicImageEx) findComponentInRoot("imageExSearchCampsImageOpen");
		}
		return imageExSearchCampsImageOpen;
	}
	protected HtmlInputText getTextNumCampaigns() {
		if (textNumCampaigns == null) {
			textNumCampaigns = (HtmlInputText) findComponentInRoot("textNumCampaigns");
		}
		return textNumCampaigns;
	}
	protected HtmlOutputLinkEx getLinkExToAdvancedSearch() {
		if (linkExToAdvancedSearch == null) {
			linkExToAdvancedSearch = (HtmlOutputLinkEx) findComponentInRoot("linkExToAdvancedSearch");
		}
		return linkExToAdvancedSearch;
	}
	public String doLinkUploadNewUTAction() {
		// Type Java code that runs when the component is clicked
	
		return "success";
	}
	public String doLinkSWCoverGraphicUploadAction() {
		// Type Java code that runs when the component is clicked
	
		return "success";
	}
	protected UIColumnEx getColumnExCampCountDGridCol1() {
		if (columnExCampCountDGridCol1 == null) {
			columnExCampCountDGridCol1 = (UIColumnEx) findComponentInRoot("columnExCampCountDGridCol1");
		}
		return columnExCampCountDGridCol1;
	}
	protected HtmlOutputText getColumnExCampCountDGridCol1Header() {
		if (columnExCampCountDGridCol1Header == null) {
			columnExCampCountDGridCol1Header = (HtmlOutputText) findComponentInRoot("columnExCampCountDGridCol1Header");
		}
		return columnExCampCountDGridCol1Header;
	}
	protected HtmlDataTableEx getTableExCampaignCountsDGrid() {
		if (tableExCampaignCountsDGrid == null) {
			tableExCampaignCountsDGrid = (HtmlDataTableEx) findComponentInRoot("tableExCampaignCountsDGrid");
		}
		return tableExCampaignCountsDGrid;
	}
	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignStatisticsHandler getCampaignStatisticsHandler() {
		if (campaignStatisticsHandler == null) {
			campaignStatisticsHandler = (CampaignStatisticsHandler) getManagedBean("campaignStatisticsHandler");
		}
		return campaignStatisticsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCampaignStatisticsHandler(
			CampaignStatisticsHandler campaignStatisticsHandler) {
		this.campaignStatisticsHandler = campaignStatisticsHandler;
	}
	protected HtmlInputText getText1() {
		if (text1 == null) {
			text1 = (HtmlInputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}
	protected HtmlCommandExButton getButtonRefreshStats() {
		if (buttonRefreshStats == null) {
			buttonRefreshStats = (HtmlCommandExButton) findComponentInRoot("buttonRefreshStats");
		}
		return buttonRefreshStats;
	}
	public String doButtonRefreshStatsAction() {
		// Type Java code that runs when the component is clicked
		this.getCampaignStatisticsHandler().refreshStatistics();
		return "";
	}
	protected UINamingContainer getSubviewBasicSearch() {
		if (subviewBasicSearch == null) {
			subviewBasicSearch = (UINamingContainer) findComponentInRoot("subviewBasicSearch");
		}
		return subviewBasicSearch;
	}
	protected HtmlScriptCollector getScriptCollectorBasicSearchForm() {
		if (scriptCollectorBasicSearchForm == null) {
			scriptCollectorBasicSearchForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorBasicSearchForm");
		}
		return scriptCollectorBasicSearchForm;
	}
	protected HtmlForm getFormBasicSearchForm() {
		if (formBasicSearchForm == null) {
			formBasicSearchForm = (HtmlForm) findComponentInRoot("formBasicSearchForm");
		}
		return formBasicSearchForm;
	}
	protected HtmlOutputText getTextBasicSearchLabel() {
		if (textBasicSearchLabel == null) {
			textBasicSearchLabel = (HtmlOutputText) findComponentInRoot("textBasicSearchLabel");
		}
		return textBasicSearchLabel;
	}
	protected HtmlInputText getTextBasicSearchCriteria() {
		if (textBasicSearchCriteria == null) {
			textBasicSearchCriteria = (HtmlInputText) findComponentInRoot("textBasicSearchCriteria");
		}
		return textBasicSearchCriteria;
	}
	protected HtmlOutputText getTextAdvSearchLabel() {
		if (textAdvSearchLabel == null) {
			textAdvSearchLabel = (HtmlOutputText) findComponentInRoot("textAdvSearchLabel");
		}
		return textAdvSearchLabel;
	}
	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}
	protected HtmlCommandExButton getButtonBasicSearch() {
		if (buttonBasicSearch == null) {
			buttonBasicSearch = (HtmlCommandExButton) findComponentInRoot("buttonBasicSearch");
		}
		return buttonBasicSearch;
	}
	protected HtmlOutputLinkEx getLinkExAdvSearchLink() {
		if (linkExAdvSearchLink == null) {
			linkExAdvSearchLink = (HtmlOutputLinkEx) findComponentInRoot("linkExAdvSearchLink");
		}
		return linkExAdvSearchLink;
	}
	protected HtmlBehaviorKeyPress getBehaviorKeyPress1() {
		if (behaviorKeyPress1 == null) {
			behaviorKeyPress1 = (HtmlBehaviorKeyPress) findComponentInRoot("behaviorKeyPress1");
		}
		return behaviorKeyPress1;
	}
	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}
	/** 
	 * @managed-bean true
	 */
	protected BasicSearchHandler getBasicSearchCriteria() {
		if (basicSearchCriteria == null) {
			basicSearchCriteria = (BasicSearchHandler) getManagedBean("basicSearchCriteria");
		}
		return basicSearchCriteria;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setBasicSearchCriteria(BasicSearchHandler basicSearchCriteria) {
		this.basicSearchCriteria = basicSearchCriteria;
	}
	public String doButtonBasicSearchAction() {
		// Type Java code that runs when the component is clicked
		String searchCriteria = this.getBasicSearchCriteria().getQueryString();

		String responsePath = "success";
		
		if (searchCriteria == null || searchCriteria.trim().length() == 0) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please specify search criteria", null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
		}
		else {

			try {
				this.getCurrentSearchResults().setSearchResults(UsatCampaignBO.fetchCampaignsMeetingGeneralCriteria(searchCriteria));
			}
			catch (Exception e) {
				e.printStackTrace();
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exception Performing Basic Search: " + e.getMessage(), "Please try again.");
				getFacesContext().addMessage(null, message);
				responsePath = "failure";
			}
		}
		return responsePath;
	}
	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getCurrentSearchResults() {
		if (currentSearchResults == null) {
			currentSearchResults = (SearchResultsHandler) getManagedBean("currentSearchResults");
		}
		return currentSearchResults;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCurrentSearchResults(
			SearchResultsHandler currentSearchResults) {
		this.currentSearchResults = currentSearchResults;
	}
}