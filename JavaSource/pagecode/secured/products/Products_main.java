/**
 * 
 */
package pagecode.secured.products;

import pagecode.PageCodeBase;
import com.gannett.usat.dataHandlers.products.ProductsHandler;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.UIColumnEx;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlForm;
import com.gannett.usat.dataHandlers.campaigns.CampaignStatisticsHandler;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlOutputLinkEx;

/**
 * @author aeast
 *
 */
public class Products_main extends PageCodeBase {

	protected ProductsHandler productHandler;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlDataTableEx tableEx1;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText textProductCodeLabel;
	protected HtmlOutputText textProductCode;
	protected HtmlOutputText text1;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text2;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlMessages messages1;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlPanelBox box1;
	protected HtmlCommandExButton buttonRefreshProducts;
	protected HtmlForm form1;
	protected CampaignStatisticsHandler campaignStatisticsHandler;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText text10;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText text12;
	protected HtmlOutputText text13;
	protected HtmlOutputText text14;
	protected HtmlPanelGrid grid1DefKeycodechange;
	protected HtmlOutputText text11;
	protected HtmlOutputText text15;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlPanelGrid grid2DefKeycodechange;
	protected HtmlOutputLinkEx linkEx111;
	protected HtmlOutputText text1511;

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlDataTableEx getTableEx1() {
		if (tableEx1 == null) {
			tableEx1 = (HtmlDataTableEx) findComponentInRoot("tableEx1");
		}
		return tableEx1;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getTextProductCodeLabel() {
		if (textProductCodeLabel == null) {
			textProductCodeLabel = (HtmlOutputText) findComponentInRoot("textProductCodeLabel");
		}
		return textProductCodeLabel;
	}

	protected HtmlOutputText getTextProductCode() {
		if (textProductCode == null) {
			textProductCode = (HtmlOutputText) findComponentInRoot("textProductCode");
		}
		return textProductCode;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlCommandExButton getButtonRefreshProducts() {
		if (buttonRefreshProducts == null) {
			buttonRefreshProducts = (HtmlCommandExButton) findComponentInRoot("buttonRefreshProducts");
		}
		return buttonRefreshProducts;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	public String doButtonRefreshProductsAction() {
		// Type Java code that runs when the component is clicked
	
		ProductsHandler ph = this.getProductHandler();
		ph.refresh();
		
		// refresh stats whenever we refresh products in case they've changed.
		this.getCampaignStatisticsHandler().refreshStatistics();
		
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Product List refreshed from database.", null);
		getFacesContext().addMessage(null, message);
		
		return "";
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignStatisticsHandler getCampaignStatisticsHandler() {
		if (campaignStatisticsHandler == null) {
			campaignStatisticsHandler = (CampaignStatisticsHandler) getManagedBean("campaignStatisticsHandler");
		}
		return campaignStatisticsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignStatisticsHandler(
			CampaignStatisticsHandler campaignStatisticsHandler) {
		this.campaignStatisticsHandler = campaignStatisticsHandler;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected HtmlPanelGrid getGrid1DefKeycodechange() {
		if (grid1DefKeycodechange == null) {
			grid1DefKeycodechange = (HtmlPanelGrid) findComponentInRoot("grid1DefKeycodechange");
		}
		return grid1DefKeycodechange;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlPanelGrid getGrid2DefKeycodechange() {
		if (grid2DefKeycodechange == null) {
			grid2DefKeycodechange = (HtmlPanelGrid) findComponentInRoot("grid2DefKeycodechange");
		}
		return grid2DefKeycodechange;
	}

	protected HtmlOutputLinkEx getLinkEx111() {
		if (linkEx111 == null) {
			linkEx111 = (HtmlOutputLinkEx) findComponentInRoot("linkEx111");
		}
		return linkEx111;
	}

	protected HtmlOutputText getText1511() {
		if (text1511 == null) {
			text1511 = (HtmlOutputText) findComponentInRoot("text1511");
		}
		return text1511;
	}

}