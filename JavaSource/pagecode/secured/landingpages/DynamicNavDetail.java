/**
 * 
 */
package pagecode.secured.landingpages;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItem;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneRadio;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.GenericFormHandler;
import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignPromotionSetHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.DynamicLinkHandler;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableHTMLPromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableImagePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.bf.component.html.HtmlButtonPanel;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

import javax.faces.component.html.HtmlMessages;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlFileupload;
import com.ibm.faces.component.html.HtmlPanelBox;
import javax.faces.component.html.HtmlMessage;
import com.gannett.usat.dataHandlers.campaigns.editPages.CoverGraphicsHandler;

/**
 * @author aeast
 *
 */
public class DynamicNavDetail extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected CampaignHandler currentDefaultCampaign;
	protected HtmlForm form1;
	protected HtmlPanelGrid gridProductGrid;
	protected HtmlOutputText textProductName;
	protected HtmlPanelLayout layout1;
	protected HtmlTabbedPanel tabbedPanelLinksEditorTabs;
	protected HtmlBfPanel bfpanelOEPagesLinks;
	protected HtmlCommandExButton tabbedPanel1_back;
	protected HtmlCommandExButton tabbedPanel1_next;
	protected HtmlCommandExButton tabbedPanel1_finish;
	protected HtmlCommandExButton tabbedPanel1_cancel;
	protected HtmlBfPanel bfpanelCSPagesLinks;
	protected HtmlPanelGrid gridOrderPageInnerGrid;
	protected HtmlDataTableEx tableExOrderPageNavLinks;
	protected UIColumnEx columnExDynamicLinks;
	protected HtmlOutputText textLinkcolHeader;
	protected HtmlJspPanel jspPanelOEInnerPanel1;
	protected HtmlOutputText textCurrentLinkeOE;
	protected HtmlPanelGroup group1;
	protected HtmlCommandExButton buttonModalOE2;
	protected HtmlBehavior behavior2;
	protected HtmlPanelDialog dialogOEAddLink;
	protected HtmlCommandExButton buttonModalOE1;
	protected HtmlPanelFormBox formBox1;
	protected HtmlFormItem formItemDisplayName;
	protected HtmlInputText text1;
	protected HtmlFormItem formItemOEUrl;
	protected HtmlInputText text3;
	protected HtmlCommandExButton buttonAddNewOELink;
	protected UIColumnEx columnEx1;
	protected HtmlCommandExButton buttonOEDeleteLinks;
	protected HtmlFormItem formItemdisplayOptoin;
	protected HtmlSelectOneRadio radioOpenInOptoin;
	protected UISelectItem selectItem1;
	protected UISelectItem selectItem2;
	protected GenericFormHandler genericFormBean;
	protected HtmlButtonPanel bp1;
	protected HtmlCommandExButton buttonSaveChanges;
	protected HtmlMessages messages1;
	protected HtmlOutputText text2;
	protected HtmlCommandExButton buttonPublishCampaign;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected HtmlPanelDialog dialogCSAddLink;
	protected HtmlPanelGroup group2;
	protected HtmlCommandExButton buttonModalCS2;
	protected HtmlBehavior behavior4;
	protected HtmlCommandExButton buttonModalCS1;
	protected HtmlBehavior behavior3;
	protected HtmlPanelFormBox formBox2;
	protected HtmlFormItem formItemCSUrl;
	protected HtmlInputText text33;
	protected HtmlFormItem formItemDisplayName2;
	protected HtmlInputText text11;
	protected HtmlFormItem formItemdisplayOptoin2;
	protected HtmlSelectOneRadio radioOpenInOptoin1;
	protected UISelectItem selectItem11;
	protected UISelectItem selectItem22;
	protected HtmlPanelGrid gridCSPageInnerGrid;
	protected HtmlCommandExButton buttonAddNewCSLink;
	protected HtmlDataTableEx tableExCSPageNavLinks;
	protected UIColumnEx columnExDynamicLinks1;
	protected HtmlOutputText textLinkcolHeader1;
	protected HtmlJspPanel jspPanelCSInnerPanel1;
	protected HtmlOutputText textCurrentLinkeCS;
	protected UIColumnEx columnEx11;
	protected HtmlCommandExButton buttonCSDeleteLinks;
	protected HtmlOutputLinkEx linkExback1;
	protected HtmlOutputText textBack1;
	protected GenericFormHandler genericFormBean2;
	protected HtmlBfPanel bfpanelNavImage;
	protected HtmlPanelGrid gridLeftNavImageOptionGrid8;
	protected HtmlPanelGroup groupLeftNavImage18;
	protected HtmlOutputText textLeftNavImageHeader8;
	protected HtmlSelectOneRadio radioLeftNavOpSelect;
	protected HtmlBfPanel bfpanelLeftNavCurrent4;
	protected HtmlPanelGrid gridCustomImageDefaultImage4;
	protected HtmlPanelGroup group7;
	protected HtmlGraphicImageEx imageLeftNav;
	protected HtmlOutputText textCustomLeftNavImageDefaultLabel4;
	protected HtmlJspPanel jspPanelInnerCustomrNoImage4;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout4;
	protected HtmlPanelFormBox formBoxCustomImage4;
	protected HtmlFormItem formItemCustomLeftNavImageFormItem4;
	protected HtmlFileupload fileuploadCustomImageUpload4;
	protected HtmlInputText textCustomLeftNavOnClickURL4;
	protected HtmlInputText textCustomLeftNavImageAltText4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel4;
	protected HtmlTabbedPanel tabbedPanelInnerLeftNavConfig4;
	protected HtmlPanelBox boxCurrentDefaultPanelBoxLeft4;
	protected HtmlBfPanel bfpanelNoLeftNav4;
	protected HtmlOutputText textInnerLeftNavNoImageLabel4;
	protected HtmlBfPanel bfpanelCustomLeftNav;
	protected HtmlMessage errorMsgCustomFileUpload4;
	protected HtmlFormItem formItemCustomLeftNavOnClickURL4;
	protected HtmlFormItem formItemCustomLeftNavImageAltText4;
	protected CoverGraphicsHandler coverGraphicsHandler;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getCurrentDefaultCampaign() {
		if (currentDefaultCampaign == null) {
			currentDefaultCampaign = (CampaignHandler) getManagedBean("currentDefaultCampaign");
		}
		return currentDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCurrentDefaultCampaign(
			CampaignHandler currentDefaultCampaign) {
		this.currentDefaultCampaign = currentDefaultCampaign;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelGrid getGridProductGrid() {
		if (gridProductGrid == null) {
			gridProductGrid = (HtmlPanelGrid) findComponentInRoot("gridProductGrid");
		}
		return gridProductGrid;
	}

	protected HtmlOutputText getTextProductName() {
		if (textProductName == null) {
			textProductName = (HtmlOutputText) findComponentInRoot("textProductName");
		}
		return textProductName;
	}

	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}

	protected HtmlTabbedPanel getTabbedPanelLinksEditorTabs() {
		if (tabbedPanelLinksEditorTabs == null) {
			tabbedPanelLinksEditorTabs = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelLinksEditorTabs");
		}
		return tabbedPanelLinksEditorTabs;
	}

	protected HtmlBfPanel getBfpanelOEPagesLinks() {
		if (bfpanelOEPagesLinks == null) {
			bfpanelOEPagesLinks = (HtmlBfPanel) findComponentInRoot("bfpanelOEPagesLinks");
		}
		return bfpanelOEPagesLinks;
	}

	protected HtmlCommandExButton getTabbedPanel1_back() {
		if (tabbedPanel1_back == null) {
			tabbedPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_back");
		}
		return tabbedPanel1_back;
	}

	protected HtmlCommandExButton getTabbedPanel1_next() {
		if (tabbedPanel1_next == null) {
			tabbedPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_next");
		}
		return tabbedPanel1_next;
	}

	protected HtmlCommandExButton getTabbedPanel1_finish() {
		if (tabbedPanel1_finish == null) {
			tabbedPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_finish");
		}
		return tabbedPanel1_finish;
	}

	protected HtmlCommandExButton getTabbedPanel1_cancel() {
		if (tabbedPanel1_cancel == null) {
			tabbedPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_cancel");
		}
		return tabbedPanel1_cancel;
	}

	protected HtmlBfPanel getBfpanelCSPagesLinks() {
		if (bfpanelCSPagesLinks == null) {
			bfpanelCSPagesLinks = (HtmlBfPanel) findComponentInRoot("bfpanelCSPagesLinks");
		}
		return bfpanelCSPagesLinks;
	}

	protected HtmlPanelGrid getGridOrderPageInnerGrid() {
		if (gridOrderPageInnerGrid == null) {
			gridOrderPageInnerGrid = (HtmlPanelGrid) findComponentInRoot("gridOrderPageInnerGrid");
		}
		return gridOrderPageInnerGrid;
	}

	protected HtmlDataTableEx getTableExOrderPageNavLinks() {
		if (tableExOrderPageNavLinks == null) {
			tableExOrderPageNavLinks = (HtmlDataTableEx) findComponentInRoot("tableExOrderPageNavLinks");
		}
		return tableExOrderPageNavLinks;
	}

	protected UIColumnEx getColumnExDynamicLinks() {
		if (columnExDynamicLinks == null) {
			columnExDynamicLinks = (UIColumnEx) findComponentInRoot("columnExDynamicLinks");
		}
		return columnExDynamicLinks;
	}

	protected HtmlOutputText getTextLinkcolHeader() {
		if (textLinkcolHeader == null) {
			textLinkcolHeader = (HtmlOutputText) findComponentInRoot("textLinkcolHeader");
		}
		return textLinkcolHeader;
	}

	protected HtmlJspPanel getJspPanelOEInnerPanel1() {
		if (jspPanelOEInnerPanel1 == null) {
			jspPanelOEInnerPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanelOEInnerPanel1");
		}
		return jspPanelOEInnerPanel1;
	}

	protected HtmlOutputText getTextCurrentLinkeOE() {
		if (textCurrentLinkeOE == null) {
			textCurrentLinkeOE = (HtmlOutputText) findComponentInRoot("textCurrentLinkeOE");
		}
		return textCurrentLinkeOE;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlCommandExButton getButtonModalOE2() {
		if (buttonModalOE2 == null) {
			buttonModalOE2 = (HtmlCommandExButton) findComponentInRoot("buttonModalOE2");
		}
		return buttonModalOE2;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlPanelDialog getDialogOEAddLink() {
		if (dialogOEAddLink == null) {
			dialogOEAddLink = (HtmlPanelDialog) findComponentInRoot("dialogOEAddLink");
		}
		return dialogOEAddLink;
	}

	protected HtmlCommandExButton getButtonModalOE1() {
		if (buttonModalOE1 == null) {
			buttonModalOE1 = (HtmlCommandExButton) findComponentInRoot("buttonModalOE1");
		}
		return buttonModalOE1;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlFormItem getFormItemDisplayName() {
		if (formItemDisplayName == null) {
			formItemDisplayName = (HtmlFormItem) findComponentInRoot("formItemDisplayName");
		}
		return formItemDisplayName;
	}

	protected HtmlInputText getText1() {
		if (text1 == null) {
			text1 = (HtmlInputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlFormItem getFormItemOEUrl() {
		if (formItemOEUrl == null) {
			formItemOEUrl = (HtmlFormItem) findComponentInRoot("formItemOEUrl");
		}
		return formItemOEUrl;
	}

	protected HtmlInputText getText3() {
		if (text3 == null) {
			text3 = (HtmlInputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlCommandExButton getButtonAddNewOELink() {
		if (buttonAddNewOELink == null) {
			buttonAddNewOELink = (HtmlCommandExButton) findComponentInRoot("buttonAddNewOELink");
		}
		return buttonAddNewOELink;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlCommandExButton getButtonOEDeleteLinks() {
		if (buttonOEDeleteLinks == null) {
			buttonOEDeleteLinks = (HtmlCommandExButton) findComponentInRoot("buttonOEDeleteLinks");
		}
		return buttonOEDeleteLinks;
	}

	protected HtmlFormItem getFormItemdisplayOptoin() {
		if (formItemdisplayOptoin == null) {
			formItemdisplayOptoin = (HtmlFormItem) findComponentInRoot("formItemdisplayOptoin");
		}
		return formItemdisplayOptoin;
	}

	protected HtmlSelectOneRadio getRadioOpenInOptoin() {
		if (radioOpenInOptoin == null) {
			radioOpenInOptoin = (HtmlSelectOneRadio) findComponentInRoot("radioOpenInOptoin");
		}
		return radioOpenInOptoin;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

	/** 
	 * @managed-bean true
	 */
	protected GenericFormHandler getGenericFormBean() {
		if (genericFormBean == null) {
			genericFormBean = (GenericFormHandler) getManagedBean("genericFormBean");
		}
		return genericFormBean;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGenericFormBean(GenericFormHandler genericFormBean) {
		this.genericFormBean = genericFormBean;
	}

	public String doButtonModalOE2Action() {
		GenericFormHandler formData = this.getGenericFormBean();
		
		CampaignHandler ch = this.getCurrentDefaultCampaign();
		
		try {
			
			if (formData.getValueString1() == null || formData.getValueString1().trim().length() == 0) {
				// faces message
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. Please enter the URL.", null);
				getFacesContext().addMessage(null, message);
				this.getMessages1().setStyle("color: red;");
				
				return "failure";
			}
			if (formData.getValueString2() == null || formData.getValueString2().trim().length() == 0) {

				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. Please enter the display name.", null);
				getFacesContext().addMessage(null, message);
				this.getMessages1().setStyle("color: red;");
				return "failure";
			}
			
			CampaignPromotionSetHandler ps = ch.getPromotionSetHandler();
			
			EditableHTMLPromotionIntf promo = ps.getSource().getDynamicNavigationOrderEntryHTML();
			if (promo == null){
				promo = new EditableHTMLPromotionBO();
				promo.setPromotionalHTML("");
				promo.setType(PromotionIntf.DYNAMIC_NAVIGATION_ORDER_ENTRY);
			}
			
			StringBuilder sb = new StringBuilder(promo.getPromotionalHTML());
			sb.append("<LI><A HREF=\"");
			sb.append(formData.getValueString1()).append("\"");
			if (formData.getValueString3() != null && formData.getValueString3().equalsIgnoreCase("_blank")){
				sb.append(" target=\"_blank\"");
			}
			sb.append(">");
			sb.append(formData.getValueString2());
			sb.append("</A></LI>");
			
			promo.setPromotionalHTML(sb.toString());
			ps.getSource().setDynamicNavigationOrderEntryHTML(promo);
			
			formData.setValueString1("");
			formData.setValueString2("");
			formData.setValueString3("_blank");
			
			FacesMessage message2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Link added. Don't forget to Save changes." , null);
			getFacesContext().addMessage(null, message2);
			
			this.getMessages1().setStyle("color: blue;");
			
		}
		catch (Exception e) {
			// faces message
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			this.getMessages1().setStyle("color: red;");
			return "failure";
		}
		return "success";
	}

	public String doButtonOEDeleteLinksAction() {
		// Type Java code that runs when the component is clicked
	
		try {
			DynamicLinkHandler dlh = (DynamicLinkHandler)this.getTableExOrderPageNavLinks().getRowData();
			
			Collection<DynamicLinkHandler> allLinks = this.getCurrentDefaultCampaign().getPromotionSetHandler().getOrderEntryDynamicLinks();
			
			if (allLinks.size() == 1) {
				allLinks.clear();
				this.currentDefaultCampaign.getPromotionSetHandler().getSource().setDynamicNavigationOrderEntryHTML(null);
			}
			else {
				EditableHTMLPromotionIntf promo = this.currentDefaultCampaign.getPromotionSetHandler().getSource().getDynamicNavigationOrderEntryHTML();
				
				StringBuilder sb = new StringBuilder();
				for(DynamicLinkHandler link : allLinks) {
					// if not the selected link
					if (!link.getLink().equalsIgnoreCase(dlh.getLink())) {
						sb.append(link.getLink());
					}
				}
				
				promo.setPromotionalHTML(sb.toString());

				FacesMessage message2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Link deleted. Don't forget to Save changes." , null);
				getFacesContext().addMessage(null, message2);
				
				this.getMessages1().setStyle("color: blue;");
				
			}
		}
		catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			this.getMessages1().setStyle("color: red;");
			return "failure";
		}
		return "success";
	}

	protected HtmlButtonPanel getBp1() {
		if (bp1 == null) {
			bp1 = (HtmlButtonPanel) findComponentInRoot("bp1");
		}
		return bp1;
	}

	protected HtmlCommandExButton getButtonSaveChanges() {
		if (buttonSaveChanges == null) {
			buttonSaveChanges = (HtmlCommandExButton) findComponentInRoot("buttonSaveChanges");
		}
		return buttonSaveChanges;
	}

	public String doButtonSaveChangesAction() {
		try {
			CampaignHandler ch = this.getCurrentDefaultCampaign();
			

			CoverGraphicsHandler graphicsEditor = this.getCoverGraphicsHandler();
			
			//ImageHandler newSwOverlayImage = graphicsEditor.getSwCoverImage();
			String navImageOption = graphicsEditor.getCurrentGraphicImageOption();
			ImageHandler newNavImage  = graphicsEditor.getCurrentImageGraphics();
			
			EditableImagePromotionIntf imagePromo =  null;
			
			if (navImageOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE) && newNavImage.getImageContents() != null && newNavImage.getImageFileName() != null) {
				 
				imagePromo = new EditableImagePromotionBO();
				imagePromo.setType(PromotionIntf.TEMPLATE_NAV_PROMO_150x90);
				imagePromo.setImageAltText(newNavImage.getAlternateText());
				imagePromo.setImageContents(newNavImage.getImageContents());
				imagePromo.setImageLinkToURL(newNavImage.getLinkURL());
				imagePromo.setImageFileType(newNavImage.getImageFileType());
				imagePromo.setImagePathString("/images/marketingimages/" + newNavImage.getImageFileName_NameOnly());
				
				ch.getCampaign().getPromotionSet().setTemplateNavigationPromoImage(imagePromo);
			
			}
			else {
				// if no image
				if (navImageOption.equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
					// clear any current pop up.
					ch.getCampaign().getPromotionSet().setTemplateNavigationPromoImage(null);
				}
				// othwerwise no changes so just leave it.
			}
		
			ch.getCampaign().save(false);
			
			// reset graphics editor
			graphicsEditor.setCurrentGraphicImageOption(CampaignEditsHandler.USE_DEFAULT_IMAGE);
			
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Campaign Changes Saved." , null);
			getFacesContext().addMessage(null, message);

			FacesMessage message2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Don't forget to publish the campaign if you want the changes visible in production." , null);
			getFacesContext().addMessage(null, message2);
			
			this.getMessages1().setStyle("color: blue;");
		}
		catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to save campaign. " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			this.getMessages1().setStyle("color: red;");
			return "failure";
		}
		return "success";
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlCommandExButton getButtonPublishCampaign() {
		if (buttonPublishCampaign == null) {
			buttonPublishCampaign = (HtmlCommandExButton) findComponentInRoot("buttonPublishCampaign");
		}
		return buttonPublishCampaign;
	}

	public String doButtonPublishCampaignAction() {
		String responseString = "success";

		CampaignHandler ch = this.getCurrentDefaultCampaign();
		ch.setCampaignSelected(true);
		
		Collection<CampaignHandler> defaultCampaigns = new ArrayList<CampaignHandler> ();

		defaultCampaigns.add(ch);
		
		this.getPublishingCampaignsHandler().setCollectionData(defaultCampaigns);
		
		return responseString;	
	}

	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getManagedBean("publishingCampaignsHandler");
		}
		return publishingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}

	protected HtmlPanelDialog getDialogCSAddLink() {
		if (dialogCSAddLink == null) {
			dialogCSAddLink = (HtmlPanelDialog) findComponentInRoot("dialogCSAddLink");
		}
		return dialogCSAddLink;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlCommandExButton getButtonModalCS2() {
		if (buttonModalCS2 == null) {
			buttonModalCS2 = (HtmlCommandExButton) findComponentInRoot("buttonModalCS2");
		}
		return buttonModalCS2;
	}

	protected HtmlBehavior getBehavior4() {
		if (behavior4 == null) {
			behavior4 = (HtmlBehavior) findComponentInRoot("behavior4");
		}
		return behavior4;
	}

	protected HtmlCommandExButton getButtonModalCS1() {
		if (buttonModalCS1 == null) {
			buttonModalCS1 = (HtmlCommandExButton) findComponentInRoot("buttonModalCS1");
		}
		return buttonModalCS1;
	}

	protected HtmlBehavior getBehavior3() {
		if (behavior3 == null) {
			behavior3 = (HtmlBehavior) findComponentInRoot("behavior3");
		}
		return behavior3;
	}

	protected HtmlPanelFormBox getFormBox2() {
		if (formBox2 == null) {
			formBox2 = (HtmlPanelFormBox) findComponentInRoot("formBox2");
		}
		return formBox2;
	}

	protected HtmlFormItem getFormItemCSUrl() {
		if (formItemCSUrl == null) {
			formItemCSUrl = (HtmlFormItem) findComponentInRoot("formItemCSUrl");
		}
		return formItemCSUrl;
	}

	protected HtmlInputText getText33() {
		if (text33 == null) {
			text33 = (HtmlInputText) findComponentInRoot("text33");
		}
		return text33;
	}

	protected HtmlFormItem getFormItemDisplayName2() {
		if (formItemDisplayName2 == null) {
			formItemDisplayName2 = (HtmlFormItem) findComponentInRoot("formItemDisplayName2");
		}
		return formItemDisplayName2;
	}

	protected HtmlInputText getText11() {
		if (text11 == null) {
			text11 = (HtmlInputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlFormItem getFormItemdisplayOptoin2() {
		if (formItemdisplayOptoin2 == null) {
			formItemdisplayOptoin2 = (HtmlFormItem) findComponentInRoot("formItemdisplayOptoin2");
		}
		return formItemdisplayOptoin2;
	}

	protected HtmlSelectOneRadio getRadioOpenInOptoin1() {
		if (radioOpenInOptoin1 == null) {
			radioOpenInOptoin1 = (HtmlSelectOneRadio) findComponentInRoot("radioOpenInOptoin1");
		}
		return radioOpenInOptoin1;
	}

	protected UISelectItem getSelectItem11() {
		if (selectItem11 == null) {
			selectItem11 = (UISelectItem) findComponentInRoot("selectItem11");
		}
		return selectItem11;
	}

	protected UISelectItem getSelectItem22() {
		if (selectItem22 == null) {
			selectItem22 = (UISelectItem) findComponentInRoot("selectItem22");
		}
		return selectItem22;
	}

	public String doButtonModalCS2Action() {
		GenericFormHandler formData = this.getGenericFormBean2();
		
		CampaignHandler ch = this.getCurrentDefaultCampaign();
		
		try {
			
			if (formData.getValueString1() == null || formData.getValueString1().trim().length() == 0) {
				// faces message
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. Please enter the URL.", null);
				getFacesContext().addMessage(null, message);
				this.getMessages1().setStyle("color: red;");
				
				return "failure";
			}
			if (formData.getValueString2() == null || formData.getValueString2().trim().length() == 0) {

				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. Please enter the display name.", null);
				getFacesContext().addMessage(null, message);
				this.getMessages1().setStyle("color: red;");
				return "failure";
			}
			
			CampaignPromotionSetHandler ps = ch.getPromotionSetHandler();
			
			EditableHTMLPromotionIntf promo = ps.getSource().getDynamicNavigationCustomerServiceHTML();
			if (promo == null){
				promo = new EditableHTMLPromotionBO();
				promo.setPromotionalHTML("");
				promo.setType(PromotionIntf.DYNAMIC_NAVIGATION_CUSTSERV);
			}
			
			StringBuilder sb = new StringBuilder(promo.getPromotionalHTML());
			sb.append("<LI><A HREF=\"");
			sb.append(formData.getValueString1()).append("\"");
			if (formData.getValueString3() != null && formData.getValueString3().equalsIgnoreCase("_blank")){
				sb.append(" target=\"_blank\"");
			}
			sb.append(">");
			sb.append(formData.getValueString2());
			sb.append("</A></LI>");
			
			promo.setPromotionalHTML(sb.toString());
			ps.getSource().setDynamicNavigationCustomerServiceHTML(promo);
			
			formData.setValueString1("");
			formData.setValueString2("");
			formData.setValueString3("_blank");
			
			FacesMessage message2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Link added. Don't forget to Save changes." , null);
			getFacesContext().addMessage(null, message2);
			
			this.getMessages1().setStyle("color: blue;");
		}
		catch (Exception e) {
			// faces message
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			this.getMessages1().setStyle("color: red;");
			return "failure";
		}
		return "success";
	}

	protected HtmlPanelGrid getGridCSPageInnerGrid() {
		if (gridCSPageInnerGrid == null) {
			gridCSPageInnerGrid = (HtmlPanelGrid) findComponentInRoot("gridCSPageInnerGrid");
		}
		return gridCSPageInnerGrid;
	}

	protected HtmlCommandExButton getButtonAddNewCSLink() {
		if (buttonAddNewCSLink == null) {
			buttonAddNewCSLink = (HtmlCommandExButton) findComponentInRoot("buttonAddNewCSLink");
		}
		return buttonAddNewCSLink;
	}

	protected HtmlDataTableEx getTableExCSPageNavLinks() {
		if (tableExCSPageNavLinks == null) {
			tableExCSPageNavLinks = (HtmlDataTableEx) findComponentInRoot("tableExCSPageNavLinks");
		}
		return tableExCSPageNavLinks;
	}

	protected UIColumnEx getColumnExDynamicLinks1() {
		if (columnExDynamicLinks1 == null) {
			columnExDynamicLinks1 = (UIColumnEx) findComponentInRoot("columnExDynamicLinks1");
		}
		return columnExDynamicLinks1;
	}

	protected HtmlOutputText getTextLinkcolHeader1() {
		if (textLinkcolHeader1 == null) {
			textLinkcolHeader1 = (HtmlOutputText) findComponentInRoot("textLinkcolHeader1");
		}
		return textLinkcolHeader1;
	}

	protected HtmlJspPanel getJspPanelCSInnerPanel1() {
		if (jspPanelCSInnerPanel1 == null) {
			jspPanelCSInnerPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanelCSInnerPanel1");
		}
		return jspPanelCSInnerPanel1;
	}

	protected HtmlOutputText getTextCurrentLinkeCS() {
		if (textCurrentLinkeCS == null) {
			textCurrentLinkeCS = (HtmlOutputText) findComponentInRoot("textCurrentLinkeCS");
		}
		return textCurrentLinkeCS;
	}

	protected UIColumnEx getColumnEx11() {
		if (columnEx11 == null) {
			columnEx11 = (UIColumnEx) findComponentInRoot("columnEx11");
		}
		return columnEx11;
	}

	protected HtmlCommandExButton getButtonCSDeleteLinks() {
		if (buttonCSDeleteLinks == null) {
			buttonCSDeleteLinks = (HtmlCommandExButton) findComponentInRoot("buttonCSDeleteLinks");
		}
		return buttonCSDeleteLinks;
	}

	public String doButtonCSDeleteLinksAction() {
		try {
			DynamicLinkHandler dlh = (DynamicLinkHandler)this.getTableExCSPageNavLinks().getRowData();
			
			Collection<DynamicLinkHandler> allLinks = this.getCurrentDefaultCampaign().getPromotionSetHandler().getCustServiceDynamicLinks();
			
			if (allLinks.size() == 1) {
				allLinks.clear();
				this.currentDefaultCampaign.getPromotionSetHandler().getSource().setDynamicNavigationCustomerServiceHTML(null);
			}
			else {
				EditableHTMLPromotionIntf promo = this.currentDefaultCampaign.getPromotionSetHandler().getSource().getDynamicNavigationCustomerServiceHTML();
				
				StringBuilder sb = new StringBuilder();
				for(DynamicLinkHandler link : allLinks) {
					// if not the selected link
					if (!link.getLink().equalsIgnoreCase(dlh.getLink())) {
						sb.append(link.getLink());
					}
				}
				
				promo.setPromotionalHTML(sb.toString());

				FacesMessage message2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Link deleted. Don't forget to Save changes." , null);
				getFacesContext().addMessage(null, message2);
				
				this.getMessages1().setStyle("color: blue;");
				
			}
		}
		catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to add link. " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			this.getMessages1().setStyle("color: red;");
			return "failure";
		}
		return "success";
	}

	protected HtmlOutputLinkEx getLinkExback1() {
		if (linkExback1 == null) {
			linkExback1 = (HtmlOutputLinkEx) findComponentInRoot("linkExback1");
		}
		return linkExback1;
	}

	protected HtmlOutputText getTextBack1() {
		if (textBack1 == null) {
			textBack1 = (HtmlOutputText) findComponentInRoot("textBack1");
		}
		return textBack1;
	}

	/** 
	 * @managed-bean true
	 */
	protected GenericFormHandler getGenericFormBean2() {
		if (genericFormBean2 == null) {
			genericFormBean2 = (GenericFormHandler) getManagedBean("genericFormBean2");
		}
		return genericFormBean2;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGenericFormBean2(GenericFormHandler genericFormBean2) {
		this.genericFormBean2 = genericFormBean2;
	}

	protected HtmlBfPanel getBfpanelNavImage() {
		if (bfpanelNavImage == null) {
			bfpanelNavImage = (HtmlBfPanel) findComponentInRoot("bfpanelNavImage");
		}
		return bfpanelNavImage;
	}

	protected HtmlPanelGrid getGridLeftNavImageOptionGrid8() {
		if (gridLeftNavImageOptionGrid8 == null) {
			gridLeftNavImageOptionGrid8 = (HtmlPanelGrid) findComponentInRoot("gridLeftNavImageOptionGrid8");
		}
		return gridLeftNavImageOptionGrid8;
	}

	protected HtmlPanelGroup getGroupLeftNavImage18() {
		if (groupLeftNavImage18 == null) {
			groupLeftNavImage18 = (HtmlPanelGroup) findComponentInRoot("groupLeftNavImage18");
		}
		return groupLeftNavImage18;
	}

	protected HtmlOutputText getTextLeftNavImageHeader8() {
		if (textLeftNavImageHeader8 == null) {
			textLeftNavImageHeader8 = (HtmlOutputText) findComponentInRoot("textLeftNavImageHeader8");
		}
		return textLeftNavImageHeader8;
	}

	protected HtmlSelectOneRadio getRadioLeftNavOpSelect() {
		if (radioLeftNavOpSelect == null) {
			radioLeftNavOpSelect = (HtmlSelectOneRadio) findComponentInRoot("radioLeftNavOpSelect");
		}
		return radioLeftNavOpSelect;
	}

	protected HtmlBfPanel getBfpanelLeftNavCurrent4() {
		if (bfpanelLeftNavCurrent4 == null) {
			bfpanelLeftNavCurrent4 = (HtmlBfPanel) findComponentInRoot("bfpanelLeftNavCurrent4");
		}
		return bfpanelLeftNavCurrent4;
	}

	protected HtmlPanelGrid getGridCustomImageDefaultImage4() {
		if (gridCustomImageDefaultImage4 == null) {
			gridCustomImageDefaultImage4 = (HtmlPanelGrid) findComponentInRoot("gridCustomImageDefaultImage4");
		}
		return gridCustomImageDefaultImage4;
	}

	protected HtmlPanelGroup getGroup7() {
		if (group7 == null) {
			group7 = (HtmlPanelGroup) findComponentInRoot("group7");
		}
		return group7;
	}

	protected HtmlGraphicImageEx getImageLeftNav() {
		if (imageLeftNav == null) {
			imageLeftNav = (HtmlGraphicImageEx) findComponentInRoot("imageLeftNav");
		}
		return imageLeftNav;
	}

	protected HtmlOutputText getTextCustomLeftNavImageDefaultLabel4() {
		if (textCustomLeftNavImageDefaultLabel4 == null) {
			textCustomLeftNavImageDefaultLabel4 = (HtmlOutputText) findComponentInRoot("textCustomLeftNavImageDefaultLabel4");
		}
		return textCustomLeftNavImageDefaultLabel4;
	}

	protected HtmlJspPanel getJspPanelInnerCustomrNoImage4() {
		if (jspPanelInnerCustomrNoImage4 == null) {
			jspPanelInnerCustomrNoImage4 = (HtmlJspPanel) findComponentInRoot("jspPanelInnerCustomrNoImage4");
		}
		return jspPanelInnerCustomrNoImage4;
	}

	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout4() {
		if (layoutInnerCustomUpSellImageImageLayout4 == null) {
			layoutInnerCustomUpSellImageImageLayout4 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout4");
		}
		return layoutInnerCustomUpSellImageImageLayout4;
	}

	protected HtmlPanelFormBox getFormBoxCustomImage4() {
		if (formBoxCustomImage4 == null) {
			formBoxCustomImage4 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage4");
		}
		return formBoxCustomImage4;
	}

	protected HtmlFormItem getFormItemCustomLeftNavImageFormItem4() {
		if (formItemCustomLeftNavImageFormItem4 == null) {
			formItemCustomLeftNavImageFormItem4 = (HtmlFormItem) findComponentInRoot("formItemCustomLeftNavImageFormItem4");
		}
		return formItemCustomLeftNavImageFormItem4;
	}

	protected HtmlFileupload getFileuploadCustomImageUpload4() {
		if (fileuploadCustomImageUpload4 == null) {
			fileuploadCustomImageUpload4 = (HtmlFileupload) findComponentInRoot("fileuploadCustomImageUpload4");
		}
		return fileuploadCustomImageUpload4;
	}

	protected HtmlInputText getTextCustomLeftNavOnClickURL4() {
		if (textCustomLeftNavOnClickURL4 == null) {
			textCustomLeftNavOnClickURL4 = (HtmlInputText) findComponentInRoot("textCustomLeftNavOnClickURL4");
		}
		return textCustomLeftNavOnClickURL4;
	}

	protected HtmlInputText getTextCustomLeftNavImageAltText4() {
		if (textCustomLeftNavImageAltText4 == null) {
			textCustomLeftNavImageAltText4 = (HtmlInputText) findComponentInRoot("textCustomLeftNavImageAltText4");
		}
		return textCustomLeftNavImageAltText4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back4() {
		if (tabbedPanelInnerUpSellConfig_back4 == null) {
			tabbedPanelInnerUpSellConfig_back4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back4");
		}
		return tabbedPanelInnerUpSellConfig_back4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next4() {
		if (tabbedPanelInnerUpSellConfig_next4 == null) {
			tabbedPanelInnerUpSellConfig_next4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next4");
		}
		return tabbedPanelInnerUpSellConfig_next4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish4() {
		if (tabbedPanelInnerUpSellConfig_finish4 == null) {
			tabbedPanelInnerUpSellConfig_finish4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish4");
		}
		return tabbedPanelInnerUpSellConfig_finish4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel4() {
		if (tabbedPanelInnerUpSellConfig_cancel4 == null) {
			tabbedPanelInnerUpSellConfig_cancel4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel4");
		}
		return tabbedPanelInnerUpSellConfig_cancel4;
	}

	protected HtmlTabbedPanel getTabbedPanelInnerLeftNavConfig4() {
		if (tabbedPanelInnerLeftNavConfig4 == null) {
			tabbedPanelInnerLeftNavConfig4 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelInnerLeftNavConfig4");
		}
		return tabbedPanelInnerLeftNavConfig4;
	}

	protected HtmlPanelBox getBoxCurrentDefaultPanelBoxLeft4() {
		if (boxCurrentDefaultPanelBoxLeft4 == null) {
			boxCurrentDefaultPanelBoxLeft4 = (HtmlPanelBox) findComponentInRoot("boxCurrentDefaultPanelBoxLeft4");
		}
		return boxCurrentDefaultPanelBoxLeft4;
	}

	protected HtmlBfPanel getBfpanelNoLeftNav4() {
		if (bfpanelNoLeftNav4 == null) {
			bfpanelNoLeftNav4 = (HtmlBfPanel) findComponentInRoot("bfpanelNoLeftNav4");
		}
		return bfpanelNoLeftNav4;
	}

	protected HtmlOutputText getTextInnerLeftNavNoImageLabel4() {
		if (textInnerLeftNavNoImageLabel4 == null) {
			textInnerLeftNavNoImageLabel4 = (HtmlOutputText) findComponentInRoot("textInnerLeftNavNoImageLabel4");
		}
		return textInnerLeftNavNoImageLabel4;
	}

	protected HtmlBfPanel getBfpanelCustomLeftNav() {
		if (bfpanelCustomLeftNav == null) {
			bfpanelCustomLeftNav = (HtmlBfPanel) findComponentInRoot("bfpanelCustomLeftNav");
		}
		return bfpanelCustomLeftNav;
	}

	protected HtmlMessage getErrorMsgCustomFileUpload4() {
		if (errorMsgCustomFileUpload4 == null) {
			errorMsgCustomFileUpload4 = (HtmlMessage) findComponentInRoot("errorMsgCustomFileUpload4");
		}
		return errorMsgCustomFileUpload4;
	}

	protected HtmlFormItem getFormItemCustomLeftNavOnClickURL4() {
		if (formItemCustomLeftNavOnClickURL4 == null) {
			formItemCustomLeftNavOnClickURL4 = (HtmlFormItem) findComponentInRoot("formItemCustomLeftNavOnClickURL4");
		}
		return formItemCustomLeftNavOnClickURL4;
	}

	protected HtmlFormItem getFormItemCustomLeftNavImageAltText4() {
		if (formItemCustomLeftNavImageAltText4 == null) {
			formItemCustomLeftNavImageAltText4 = (HtmlFormItem) findComponentInRoot("formItemCustomLeftNavImageAltText4");
		}
		return formItemCustomLeftNavImageAltText4;
	}

	/** 
	 * @managed-bean true
	 */
	protected CoverGraphicsHandler getCoverGraphicsHandler() {
		if (coverGraphicsHandler == null) {
			coverGraphicsHandler = (CoverGraphicsHandler) getManagedBean("coverGraphicsHandler");
		}
		return coverGraphicsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCoverGraphicsHandler(
			CoverGraphicsHandler coverGraphicsHandler) {
		this.coverGraphicsHandler = coverGraphicsHandler;
	}

}