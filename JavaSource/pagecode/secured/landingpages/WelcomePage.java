/*
 * Created on Aug 21, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode.secured.landingpages;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneRadio;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.WelcomePageHandler;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableHTMLPromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableImagePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.odc.jsf.components.components.rte.UIRichTextEditor;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WelcomePage extends PageCodeBase {

	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected CampaignEditsHandler campaignUtDefaultHandler ;
	protected CampaignEditsHandler campaignSwLandingPagesHandler;
	protected CampaignEditsHandler campaignEditsHandler;
	protected CampaignHandler utLandingPages;
	protected CampaignHandler swLandingPages;
	protected CampaignHandler campaign;
	protected HtmlMessages messages1;
	protected HtmlPanelGroup group3;
	protected HtmlGraphicImageEx imageUt400x200;
	protected HtmlPanelGroup group5;
	protected HtmlGraphicImageEx imageUt180x120;
	protected HtmlPanelGroup group6;
	protected HtmlGraphicImageEx imageUt180x90;
	protected HtmlPanelGroup group7;
	protected HtmlGraphicImageEx imageUt400x100;
	protected HtmlPanelGroup group8;
	protected HtmlGraphicImageEx imageSw400x200;
	protected HtmlPanelGroup group9;
	protected HtmlGraphicImageEx imageSw400x100;
	protected HtmlPanelGroup group10;
	protected HtmlGraphicImageEx imageSw180x90;
	protected HtmlPanelGroup group11;
	protected HtmlGraphicImageEx imageSw180x120;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel8;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel4;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel6;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel11;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel31;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel41;
	protected HtmlOutputText textInnerUpSellNoImageLabel11;
	protected HtmlOutputText textInnerUpSellNoImageLabel31;
	protected HtmlOutputText textInnerUpSellNoImageLabel41;
	protected HtmlOutputText textInnerUpSellNoImageLabel51;
	protected HtmlOutputText textInnerUpSellNoImageLabel6;
	protected HtmlOutputText textInnerUpSellNoImageLabel4;
	protected HtmlOutputText textInnerUpSellNoImageLabel;
	protected HtmlPanelGrid grid1;
	protected HtmlPanelGroup group2;
	protected HtmlPanelGroup group12;
	protected HtmlOutputText textCurrentSettingLabel;
	protected HtmlPanelGrid grid2;
	protected HtmlPanelGroup group13;
	protected HtmlPanelGroup group14;
	protected HtmlOutputText textCurrentHeaderImageLabel;
	protected HtmlOutputText textCurrentNavImageLabel;
	protected HtmlOutputText text1;
	protected HtmlOutputText text2;
	protected HtmlOutputText textCustomUpSellImageDefaultLabel51;
	protected HtmlPanelBox boxCurrentDefaultPanelBoxLeft51;
	protected HtmlOutputText textUpSellImageHeader;
	protected HtmlOutputText textUpSellImageHeader71;
	protected HtmlSelectOneRadio radioSw400x200;
	
	protected HtmlCommandExButton button1;
	protected HtmlSelectOneRadio radioSw400x100;
	protected HtmlSelectOneRadio radioSw180x90;
	protected HtmlSelectOneRadio radioSw180x120;
	protected HtmlSelectOneRadio radioUt400x200;
	protected HtmlSelectOneRadio radioUt400x100;
	protected HtmlSelectOneRadio radioUt180x90;
	protected HtmlSelectOneRadio radioUt180x120;
	protected HtmlOutputText textWelcomePage;
	protected HtmlOutputText textMockUpLinkLabel;
	protected HtmlOutputLinkEx linkExMockUpLink;
	protected HtmlCommandExButton buttonPublishChanges;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected UIRichTextEditor richTextEditor1;
	protected UIRichTextEditor richTextEditor2;
	protected WelcomePageHandler welcomePageHandler;
	protected HtmlBfPanel bfpanel1;
	protected HtmlOutputText textUpSellImageHeadera;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout;
	protected HtmlFormItem formItemCustomUpSellImageFormItem;
	protected HtmlFormItem formItemCustomUpSellOnClickURL;
	protected HtmlFormItem formItemCustomUpSellImageAltText;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel;
	protected HtmlOutputText textUpSellImageHeader4;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout4;
	protected HtmlPanelFormBox formBoxCustomImage4;
	protected HtmlFormItem formItemCustomUpSellImageFormItem4;
	protected HtmlFormItem formItemCustomUpSellOnClickURL4;
	protected HtmlFormItem formItemCustomUpSellImageAltText4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish4;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel4;
	protected HtmlOutputText textUpSellImageHeader6;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout6;
	protected HtmlPanelFormBox formBoxCustomImage6;
	protected HtmlFormItem formItemCustomUpSellImageFormItem6;
	protected HtmlFormItem formItemCustomUpSellOnClickURL6;
	protected HtmlFormItem formItemCustomUpSellImageAltText6;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back6;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next6;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish6;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel6;
	protected HtmlOutputText textUpSellImageHeader8;
	protected HtmlOutputText textInnerUpSellNoImageLabel8;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout8;
	protected HtmlPanelFormBox formBoxCustomImage8;
	protected HtmlFormItem formItemCustomUpSellImageFormItem8;
	protected HtmlFormItem formItemCustomUpSellOnClickURL8;
	protected HtmlFormItem formItemCustomUpSellImageAltText8;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back8;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next8;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish8;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel8;
	protected HtmlBfPanel bfpanel2;
	protected HtmlOutputText textUpSellImageHeader11a;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout11;
	protected HtmlPanelFormBox formBoxCustomImage11;
	protected HtmlFormItem formItemCustomUpSellImageFormItem11;
	protected HtmlFormItem formItemCustomUpSellOnClickURL11;
	protected HtmlFormItem formItemCustomUpSellImageAltText11;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back11;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next11;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish11;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel11;
	protected HtmlOutputText textUpSellImageHeader31;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout31;
	protected HtmlPanelFormBox formBoxCustomImage31;
	protected HtmlFormItem formItemCustomUpSellImageFormItem31;
	protected HtmlFormItem formItemCustomUpSellOnClickURL31;
	protected HtmlFormItem formItemCustomUpSellImageAltText31;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back31;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next31;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish31;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel31;
	protected HtmlOutputText textUpSellImageHeader41;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout41;
	protected HtmlPanelFormBox formBoxCustomImage41;
	protected HtmlFormItem formItemCustomUpSellImageFormItem41;
	protected HtmlFormItem formItemCustomUpSellOnClickURL41;
	protected HtmlFormItem formItemCustomUpSellImageAltText41;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back41;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next41;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish41;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel41;
	protected HtmlOutputText textUpSellImageHeader51;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout51;
	protected HtmlPanelFormBox formBoxCustomImage51;
	protected HtmlFormItem formItemCustomUpSellImageFormItem51;
	protected HtmlFormItem formItemCustomUpSellOnClickURL51;
	protected HtmlFormItem formItemCustomUpSellImageAltText51;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back51;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next51;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish51;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel51;
	protected HtmlTabbedPanel tabbedPanel2;
	public String doButtonPublishChangesAction() {
		
	String responseString = "success";
	CampaignHandler swcampaign = this.getSwLandingPages();
	CampaignHandler utcampaign = this.getUtLandingPages();
	
	
	Collection<CampaignHandler> defaultCampaigns = new ArrayList<CampaignHandler> ();

	defaultCampaigns.add(swcampaign);
	defaultCampaigns.add(utcampaign);
	
	
	// set up collection for publishing
	Collection<CampaignHandler> campaigns = defaultCampaigns;
	for(CampaignHandler ch : campaigns) {
			ch.setCampaignSelected(true);
	}
	this.getPublishingCampaignsHandler().setCollectionData(defaultCampaigns);
	
	return responseString;
	}
	
	
	
	public String doButtonSaveChangesAction() {
		 // Type Java code that runs when the component is clicked	
		boolean hasErrors = false;	
		CampaignHandler swcampaign = this.getSwLandingPages();
		CampaignHandler utcampaign = this.getUtLandingPages();
		/*
		WelcomePageHandler welcomePage = this.getWelcomePageHandler();
		
		ImageHandler newUt400x200Image  = welcomePage.getUt400x200Image();
		ImageHandler newUt400x100Image  = welcomePage.getUt400x100Image();
		ImageHandler newUt180x90Image  = welcomePage.getUt180x90Image();
		ImageHandler newUt180x120Image  = welcomePage.getUt180x120Image();
		
		ImageHandler newSw400x200Image  = welcomePage.getSw400x200Image();
		ImageHandler newSw400x100Image  = welcomePage.getSw400x100Image();
		ImageHandler newSw180x90Image  = welcomePage.getSw180x90Image();
		ImageHandler newSw180x120Image  = welcomePage.getSw180x120Image();
		
		try {		
			
			// set up new images  
			EditableHTMLPromotionIntf swWelcomePromoText =  null;
			EditableHTMLPromotionIntf utWelcomePromoText =  null;
			EditableImagePromotionIntf swWelcomePromo =  null;
			EditableImagePromotionIntf utWelcomePromo =  null;
		
			
			String swHeaderText = welcomePage.getSwHeaderText();
			if (swHeaderText == null) {		
				swHeaderText = null;					
			} else {
				swWelcomePromoText =  null;
				swWelcomePromoText = new EditableHTMLPromotionBO();
				swWelcomePromoText.setType(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
				swWelcomePromoText.setPromotionalHTML(swHeaderText);
				swcampaign.getCampaign().getPromotionSet().setPromoLandingPageText(swWelcomePromoText);
				swcampaign.getCampaign().save(false);
			}
			
			String utHeaderText = welcomePage.getUtHeaderText();
			if (utHeaderText == null) {		
				utHeaderText = null;					
			} else {
				utWelcomePromoText =  null;
				utWelcomePromoText = new EditableHTMLPromotionBO();
				utWelcomePromoText.setType(PromotionIntf.LANDING_PAGE_PROMO_TEXT);
				utWelcomePromoText.setPromotionalHTML(utHeaderText);
				utcampaign.getCampaign().getPromotionSet().setPromoLandingPageText(utWelcomePromoText);
				utcampaign.getCampaign().save(false);
			}
				
					
			
			if (!welcomePage.getSw400x200ImageOption().equalsIgnoreCase("default")) {
				swWelcomePromo =  null;
				swWelcomePromo = new EditableImagePromotionBO();
					
				swWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_400x200);				
				if (welcomePage.getSw400x200ImageOption().equalsIgnoreCase("custom")) {
				     // set up custom
					 if (newSw400x200Image.getImageContents() != null && newSw400x200Image.getImageFileType() != null) {
							swWelcomePromo.setName(newSw400x200Image.getImageFileName());
							swWelcomePromo.setImageAltText(newSw400x200Image.getAlternateText());
							swWelcomePromo.setImageContents(newSw400x200Image.getImageContents());
							swWelcomePromo.setImageLinkToURL(newSw400x200Image.getLinkURL());
							swWelcomePromo.setImageFileType(newSw400x200Image.getImageFileType());
							swWelcomePromo.setImagePathString("/images/marketingimages/" + newSw400x200Image.getImageFileName_NameOnly());
							welcomePage.setSw400x200ImageOption("custom");
					}
				}
				else if (getRadioSw400x200().getValue().equals("none")) {
							swWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
							welcomePage.setSw400x200ImageOption("none");
				}
				swcampaign.getCampaign().getPromotionSet().setLargePromoImageLandingPage(swWelcomePromo);
				swcampaign.getCampaign().save(false);
				
			}
			
			if (!welcomePage.getSw400x100ImageOption().equalsIgnoreCase("default")) {
				swWelcomePromo =  null;
				swWelcomePromo = new EditableImagePromotionBO();
				swWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_400x100);
				
				if (welcomePage.getSw400x100ImageOption().equalsIgnoreCase("custom")) {
					 if (newSw400x100Image.getImageContents() != null && newSw400x100Image.getImageFileType() != null) {
							swWelcomePromo.setName(newSw400x100Image.getImageFileName());
							swWelcomePromo.setImageAltText(newSw400x100Image.getAlternateText());
							swWelcomePromo.setImageContents(newSw400x100Image.getImageContents());
							swWelcomePromo.setImageLinkToURL(newSw400x100Image.getLinkURL());
							swWelcomePromo.setImageFileType(newSw400x100Image.getImageFileType());
							swWelcomePromo.setImagePathString("/images/marketingimages/" + newSw400x100Image.getImageFileName_NameOnly());
							welcomePage.setSw400x100ImageOption("custom");
								}
				}
				else if (getRadioSw400x100().getValue().equals("none")) {
					swWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
					welcomePage.setSw400x100ImageOption("none");
				}
				swcampaign.getCampaign().getPromotionSet().setSmallPromoImageLandingPage(swWelcomePromo);
				swcampaign.getCampaign().save(false);
				
			}
			
			
			if (!welcomePage.getSw180x90ImageOption().equalsIgnoreCase("default")) {
				swWelcomePromo =  null;
				swWelcomePromo = new EditableImagePromotionBO();
				swWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90);
			
				if (welcomePage.getSw180x90ImageOption().equalsIgnoreCase("custom")) {
					 if (newSw180x90Image.getImageContents() != null && newSw180x90Image.getImageFileType() != null) {
							swWelcomePromo.setName(newSw180x90Image.getImageFileName());
							swWelcomePromo.setImageAltText(newSw180x90Image.getAlternateText());
							swWelcomePromo.setImageContents(newSw180x90Image.getImageContents());
							swWelcomePromo.setImageLinkToURL(newSw180x90Image.getLinkURL());
							swWelcomePromo.setImageFileType(newSw180x90Image.getImageFileType());
							swWelcomePromo.setImagePathString("/images/marketingimages/" + newSw180x90Image.getImageFileName_NameOnly());
							welcomePage.setSw180x90ImageOption("custom");
						}
				}
				else if (getRadioSw180x90().getValue().equals("none")) {
					swWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
					welcomePage.setSw180x90ImageOption("none");
				}
				swcampaign.getCampaign().getPromotionSet().setRightColumnUpperPromoImageLandingPage(swWelcomePromo);
				swcampaign.getCampaign().save(false);
				
			}
			
		
				if (!welcomePage.getSw180x120ImageOption().equalsIgnoreCase("default")) {	
					swWelcomePromo = new EditableImagePromotionBO();
					swWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120);			
				
					if (welcomePage.getSw180x120ImageOption().equalsIgnoreCase("custom")) {
						 if (newSw180x120Image.getImageContents() != null && newSw180x120Image.getImageFileType() != null) {
								swWelcomePromo.setName(newSw180x120Image.getImageFileName());
								swWelcomePromo.setImageAltText(newSw180x120Image.getAlternateText());
								swWelcomePromo.setImageContents(newSw180x120Image.getImageContents());
								swWelcomePromo.setImageLinkToURL(newSw180x120Image.getLinkURL());
								swWelcomePromo.setImageFileType(newSw180x120Image.getImageFileType());
								swWelcomePromo.setImagePathString("/images/marketingimages/" + newSw180x120Image.getImageFileName_NameOnly());	
								welcomePage.setSw180x120ImageOption("custom");
							}
					}
					else if (getRadioSw180x120().getValue().equals("none")) {
						swWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
						welcomePage.setSw180x120ImageOption("none");
					}
					
					swcampaign.getCampaign().getPromotionSet().setRightColumnLowerPromoImageLandingPage(swWelcomePromo);
					swcampaign.getCampaign().save(false);
					
				}
		
			
			
			if (!welcomePage.getUt400x200ImageOption().equalsIgnoreCase("default")) {
				utWelcomePromo =  null;
				utWelcomePromo = new EditableImagePromotionBO();
					
				utWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_400x200);				
				if (welcomePage.getUt400x200ImageOption().equalsIgnoreCase("custom")) {
				     // set up custom
					 if (newUt400x200Image.getImageContents() != null && newUt400x200Image.getImageFileType() != null) {
							utWelcomePromo.setName(newUt400x200Image.getImageFileName());
							utWelcomePromo.setImageAltText(newUt400x200Image.getAlternateText());
							utWelcomePromo.setImageContents(newUt400x200Image.getImageContents());
							utWelcomePromo.setImageLinkToURL(newUt400x200Image.getLinkURL());
							utWelcomePromo.setImageFileType(newUt400x200Image.getImageFileType());
							utWelcomePromo.setImagePathString("/images/marketingimages/" + newUt400x200Image.getImageFileName_NameOnly());	
							welcomePage.setUt400x200ImageOption("custom");
					}
				}
				else if (getRadioUt400x200().getValue().equals("none")) {
							utWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
							welcomePage.setUt400x200ImageOption("none");
				}
				utcampaign.getCampaign().getPromotionSet().setLargePromoImageLandingPage(utWelcomePromo);
				utcampaign.getCampaign().save(false);
				
			}
			
			if (!welcomePage.getUt400x100ImageOption().equalsIgnoreCase("default")) {
				utWelcomePromo =  null;
				utWelcomePromo = new EditableImagePromotionBO();
				utWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_400x100);
				
				if (welcomePage.getUt400x100ImageOption().equalsIgnoreCase("custom")) {
					 if (newUt400x100Image.getImageContents() != null && newUt400x100Image.getImageFileType() != null) {
							utWelcomePromo.setName(newUt400x100Image.getImageFileName());
							utWelcomePromo.setImageAltText(newUt400x100Image.getAlternateText());
							utWelcomePromo.setImageContents(newUt400x100Image.getImageContents());
							utWelcomePromo.setImageLinkToURL(newUt400x100Image.getLinkURL());
							utWelcomePromo.setImageFileType(newUt400x100Image.getImageFileType());
							utWelcomePromo.setImagePathString("/images/marketingimages/" + newUt400x100Image.getImageFileName_NameOnly());
							welcomePage.setUt400x100ImageOption("custom");	
								}
				}
				else if (getRadioUt400x100().getValue().equals("none")) {
					utWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
					welcomePage.setUt400x100ImageOption("none");	
				}
				utcampaign.getCampaign().getPromotionSet().setSmallPromoImageLandingPage(utWelcomePromo);
				utcampaign.getCampaign().save(false);
				
			}
			
			
			if (!welcomePage.getUt180x90ImageOption().equalsIgnoreCase("default")) {
				utWelcomePromo =  null;
				utWelcomePromo = new EditableImagePromotionBO();
				utWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x90);
			
				if (welcomePage.getUt180x90ImageOption().equalsIgnoreCase("custom")) {
					//if (!utWelcomePromo.isShimImage()) {
						// set up custom
					 if (newUt180x90Image.getImageContents() != null && newUt180x90Image.getImageFileType() != null) {
							//newUt400x200Image.getImageFileName()
							//utWelcomePromo.setName(CampaignEditsHandler.USE_CUSTOM_IMAGE);
							
							utWelcomePromo.setName(newUt180x90Image.getImageFileName());
							utWelcomePromo.setImageAltText(newUt180x90Image.getAlternateText());
							utWelcomePromo.setImageContents(newUt180x90Image.getImageContents());
							utWelcomePromo.setImageLinkToURL(newUt180x90Image.getLinkURL());
							utWelcomePromo.setImageFileType(newUt180x90Image.getImageFileType());
							utWelcomePromo.setImagePathString("/images/marketingimages/" + newUt180x90Image.getImageFileName_NameOnly());
							welcomePage.setUt180x90ImageOption("custom");	
						}
				}
				else if (getRadioUt180x90().getValue().equals("none")) {
					utWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
					welcomePage.setUt180x90ImageOption("none");	
				}
				utcampaign.getCampaign().getPromotionSet().setRightColumnUpperPromoImageLandingPage(utWelcomePromo);
				utcampaign.getCampaign().save(false);
				
			}
			
			
		
				if (!welcomePage.getUt180x120ImageOption().equalsIgnoreCase("default")) {	
					utWelcomePromo = new EditableImagePromotionBO();
					utWelcomePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120);			
				
					if (welcomePage.getUt180x120ImageOption().equalsIgnoreCase("custom")) {
						 if (newUt180x120Image.getImageContents() != null && newUt180x120Image.getImageFileType() != null) {
								utWelcomePromo.setName(newUt180x120Image.getImageFileName());
								utWelcomePromo.setImageAltText(newUt180x120Image.getAlternateText());
								utWelcomePromo.setImageContents(newUt180x120Image.getImageContents());
								utWelcomePromo.setImageLinkToURL(newUt180x120Image.getLinkURL());
								utWelcomePromo.setImageFileType(newUt180x120Image.getImageFileType());
								utWelcomePromo.setImagePathString("/images/marketingimages/" + newUt180x120Image.getImageFileName_NameOnly());	
								welcomePage.setUt180x120ImageOption("custom");
							}
					}
					else if (getRadioUt180x120().getValue().equals("none")) {
						utWelcomePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
						welcomePage.setUt180x120ImageOption("none");
					}
					
					utcampaign.getCampaign().getPromotionSet().setRightColumnLowerPromoImageLandingPage(utWelcomePromo);
					utcampaign.getCampaign().save(false);
					
				}
			
			
			
	}
	catch (Exception e) {
		System.out.println(e.getMessage());
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "FAILED TO set Welcome Cover Graphics. Contact IT for support.", e.getMessage());
		getMessages1().setErrorStyle("color:Red");
		getMessages1().setShowDetail(true);
		getFacesContext().addMessage(null, message);
		hasErrors = true;
	}
		
		*/
		if (hasErrors) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "FAILED TO set Welcome Page Cover Graphics Values", "");
			getFacesContext().addMessage(null, message);
			return "failure";
		}
		else {	
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome Graphics successfully saved", "");
			getFacesContext().addMessage(null, message);
			return "success";
		}
		
		
	
	}
	
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlGraphicImageEx getImageUt400x200() {
		if (imageUt400x200 == null) {
			imageUt400x200 = (HtmlGraphicImageEx) findComponentInRoot("imageUt400x200");
		}
		return imageUt400x200;
	}

	protected HtmlPanelGroup getGroup5() {
		if (group5 == null) {
			group5 = (HtmlPanelGroup) findComponentInRoot("group5");
		}
		return group5;
	}

	protected HtmlGraphicImageEx getImageUt180x120() {
		if (imageUt180x120 == null) {
			imageUt180x120 = (HtmlGraphicImageEx) findComponentInRoot("imageUt180x120");
		}
		return imageUt180x120;
	}

	protected HtmlPanelGroup getGroup6() {
		if (group6 == null) {
			group6 = (HtmlPanelGroup) findComponentInRoot("group6");
		}
		return group6;
	}

	protected HtmlGraphicImageEx getImageUt180x90() {
		if (imageUt180x90 == null) {
			imageUt180x90 = (HtmlGraphicImageEx) findComponentInRoot("imageUt180x90");
		}
		return imageUt180x90;
	}

	protected HtmlPanelGroup getGroup7() {
		if (group7 == null) {
			group7 = (HtmlPanelGroup) findComponentInRoot("group7");
		}
		return group7;
	}

	protected HtmlGraphicImageEx getImageUt400x100() {
		if (imageUt400x100 == null) {
			imageUt400x100 = (HtmlGraphicImageEx) findComponentInRoot("imageUt400x100");
		}
		return imageUt400x100;
	}

	protected HtmlPanelGroup getGroup8() {
		if (group8 == null) {
			group8 = (HtmlPanelGroup) findComponentInRoot("group8");
		}
		return group8;
	}

	protected HtmlGraphicImageEx getImageSw400x200() {
		if (imageSw400x200 == null) {
			imageSw400x200 = (HtmlGraphicImageEx) findComponentInRoot("imageSw400x200");
		}
		return imageSw400x200;
	}

	protected HtmlPanelGroup getGroup9() {
		if (group9 == null) {
			group9 = (HtmlPanelGroup) findComponentInRoot("group9");
		}
		return group9;
	}

	protected HtmlGraphicImageEx getImageSw400x100() {
		if (imageSw400x100 == null) {
			imageSw400x100 = (HtmlGraphicImageEx) findComponentInRoot("imageSw400x100");
		}
		return imageSw400x100;
	}

	protected HtmlPanelGroup getGroup10() {
		if (group10 == null) {
			group10 = (HtmlPanelGroup) findComponentInRoot("group10");
		}
		return group10;
	}

	protected HtmlGraphicImageEx getImageSw180x90() {
		if (imageSw180x90 == null) {
			imageSw180x90 = (HtmlGraphicImageEx) findComponentInRoot("imageSw180x90");
		}
		return imageSw180x90;
	}

	protected HtmlPanelGroup getGroup11() {
		if (group11 == null) {
			group11 = (HtmlPanelGroup) findComponentInRoot("group11");
		}
		return group11;
	}

	protected HtmlGraphicImageEx getImageSw180x120() {
		if (imageSw180x120 == null) {
			imageSw180x120 = (HtmlGraphicImageEx) findComponentInRoot("imageSw180x120");
		}
		return imageSw180x120;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel8() {
		if (textCustomUpSellImageDefaultLabel8 == null) {
			textCustomUpSellImageDefaultLabel8 = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel8");
		}
		return textCustomUpSellImageDefaultLabel8;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel() {
		if (textCustomUpSellImageDefaultLabel == null) {
			textCustomUpSellImageDefaultLabel = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel");
		}
		return textCustomUpSellImageDefaultLabel;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel4() {
		if (textCustomUpSellImageDefaultLabel4 == null) {
			textCustomUpSellImageDefaultLabel4 = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel4");
		}
		return textCustomUpSellImageDefaultLabel4;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel6() {
		if (textCustomUpSellImageDefaultLabel6 == null) {
			textCustomUpSellImageDefaultLabel6 = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel6");
		}
		return textCustomUpSellImageDefaultLabel6;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel11() {
		if (textCustomUpSellImageDefaultLabel11 == null) {
			textCustomUpSellImageDefaultLabel11 = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel11");
		}
		return textCustomUpSellImageDefaultLabel11;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel31() {
		if (textCustomUpSellImageDefaultLabel31 == null) {
			textCustomUpSellImageDefaultLabel31 = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel31");
		}
		return textCustomUpSellImageDefaultLabel31;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel41() {
		if (textCustomUpSellImageDefaultLabel41 == null) {
			textCustomUpSellImageDefaultLabel41 = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel41");
		}
		return textCustomUpSellImageDefaultLabel41;
	}

	protected HtmlOutputText getTextInnerUpSellNoImageLabel11() {
		if (textInnerUpSellNoImageLabel11 == null) {
			textInnerUpSellNoImageLabel11 = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel11");
		}
		return textInnerUpSellNoImageLabel11;
	}

	protected HtmlOutputText getTextInnerUpSellNoImageLabel31() {
		if (textInnerUpSellNoImageLabel31 == null) {
			textInnerUpSellNoImageLabel31 = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel31");
		}
		return textInnerUpSellNoImageLabel31;
	}

	protected HtmlOutputText getTextInnerUpSellNoImageLabel41() {
		if (textInnerUpSellNoImageLabel41 == null) {
			textInnerUpSellNoImageLabel41 = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel41");
		}
		return textInnerUpSellNoImageLabel41;
	}

	protected HtmlOutputText getTextInnerUpSellNoImageLabel51() {
		if (textInnerUpSellNoImageLabel51 == null) {
			textInnerUpSellNoImageLabel51 = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel51");
		}
		return textInnerUpSellNoImageLabel51;
	}

	protected HtmlOutputText getTextInnerUpSellNoImageLabel6() {
		if (textInnerUpSellNoImageLabel6 == null) {
			textInnerUpSellNoImageLabel6 = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel6");
		}
		return textInnerUpSellNoImageLabel6;
	}

	protected HtmlOutputText getTextInnerUpSellNoImageLabel4() {
		if (textInnerUpSellNoImageLabel4 == null) {
			textInnerUpSellNoImageLabel4 = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel4");
		}
		return textInnerUpSellNoImageLabel4;
	}

	protected HtmlOutputText getTextInnerUpSellNoImageLabel() {
		if (textInnerUpSellNoImageLabel == null) {
			textInnerUpSellNoImageLabel = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel");
		}
		return textInnerUpSellNoImageLabel;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlPanelGroup getGroup12() {
		if (group12 == null) {
			group12 = (HtmlPanelGroup) findComponentInRoot("group12");
		}
		return group12;
	}

	protected HtmlOutputText getTextCurrentSettingLabel() {
		if (textCurrentSettingLabel == null) {
			textCurrentSettingLabel = (HtmlOutputText) findComponentInRoot("textCurrentSettingLabel");
		}
		return textCurrentSettingLabel;
	}

	protected HtmlPanelGrid getGrid2() {
		if (grid2 == null) {
			grid2 = (HtmlPanelGrid) findComponentInRoot("grid2");
		}
		return grid2;
	}

	protected HtmlPanelGroup getGroup13() {
		if (group13 == null) {
			group13 = (HtmlPanelGroup) findComponentInRoot("group13");
		}
		return group13;
	}

	protected HtmlPanelGroup getGroup14() {
		if (group14 == null) {
			group14 = (HtmlPanelGroup) findComponentInRoot("group14");
		}
		return group14;
	}

	protected HtmlOutputText getTextCurrentHeaderImageLabel() {
		if (textCurrentHeaderImageLabel == null) {
			textCurrentHeaderImageLabel = (HtmlOutputText) findComponentInRoot("textCurrentHeaderImageLabel");
		}
		return textCurrentHeaderImageLabel;
	}

	protected HtmlOutputText getTextCurrentNavImageLabel() {
		if (textCurrentNavImageLabel == null) {
			textCurrentNavImageLabel = (HtmlOutputText) findComponentInRoot("textCurrentNavImageLabel");
		}
		return textCurrentNavImageLabel;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getTextCustomUpSellImageDefaultLabel51() {
		if (textCustomUpSellImageDefaultLabel51 == null) {
			textCustomUpSellImageDefaultLabel51 = (HtmlOutputText) findComponentInRoot("textCustomUpSellImageDefaultLabel51");
		}
		return textCustomUpSellImageDefaultLabel51;
	}

	protected HtmlPanelBox getBoxCurrentDefaultPanelBoxLeft51() {
		if (boxCurrentDefaultPanelBoxLeft51 == null) {
			boxCurrentDefaultPanelBoxLeft51 = (HtmlPanelBox) findComponentInRoot("boxCurrentDefaultPanelBoxLeft51");
		}
		return boxCurrentDefaultPanelBoxLeft51;
	}

	protected HtmlOutputText getTextUpSellImageHeader() {
		if (textUpSellImageHeader == null) {
			textUpSellImageHeader = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader");
		}
		return textUpSellImageHeader;
	}

	protected HtmlOutputText getTextUpSellImageHeader71() {
		if (textUpSellImageHeader71 == null) {
			textUpSellImageHeader71 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader71");
		}
		return textUpSellImageHeader71;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}
	protected HtmlSelectOneRadio getRadioSw400x200() {
		if (radioSw400x200 == null) {
			radioSw400x200 = (HtmlSelectOneRadio) findComponentInRoot("radioSw400x200");
		}
		return radioSw400x200;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getSwLandingPages() {
		if (swLandingPages == null) {
			swLandingPages = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding("#{swLandingPages}")
					.getValue(getFacesContext());
		}
		return swLandingPages;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setSwLandingPages(CampaignHandler swLandingPages) {
		this.swLandingPages = swLandingPages;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getUtLandingPages() {
		if (utLandingPages == null) {
			utLandingPages = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding("#{utLandingPages}")
					.getValue(getFacesContext());
		}
		return utLandingPages;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUtLandingPages(CampaignHandler utLandingPages) {
		this.utLandingPages = utLandingPages;
	}
	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}
	protected HtmlSelectOneRadio getRadioSw400x100() {
		if (radioSw400x100 == null) {
			radioSw400x100 = (HtmlSelectOneRadio) findComponentInRoot("radioSw400x100");
		}
		return radioSw400x100;
	}
	protected HtmlSelectOneRadio getRadioSw180x90() {
		if (radioSw180x90 == null) {
			radioSw180x90 = (HtmlSelectOneRadio) findComponentInRoot("radioSw180x90");
		}
		return radioSw180x90;
	}
	protected HtmlSelectOneRadio getRadioSw180x120() {
		if (radioSw180x120 == null) {
			radioSw180x120 = (HtmlSelectOneRadio) findComponentInRoot("radioSw180x120");
		}
		return radioSw180x120;
	}
	protected HtmlSelectOneRadio getRadioUt400x200() {
		if (radioUt400x200 == null) {
			radioUt400x200 = (HtmlSelectOneRadio) findComponentInRoot("radioUt400x200");
		}
		return radioUt400x200;
	}
	protected HtmlSelectOneRadio getRadioUt400x100() {
		if (radioUt400x100 == null) {
			radioUt400x100 = (HtmlSelectOneRadio) findComponentInRoot("radioUt400x100");
		}
		return radioUt400x100;
	}
	protected HtmlSelectOneRadio getRadioUt180x90() {
		if (radioUt180x90 == null) {
			radioUt180x90 = (HtmlSelectOneRadio) findComponentInRoot("radioUt180x90");
		}
		return radioUt180x90;
	}
	protected HtmlSelectOneRadio getRadioUt180x120() {
		if (radioUt180x120 == null) {
			radioUt180x120 = (HtmlSelectOneRadio) findComponentInRoot("radioUt180x120");
		}
		return radioUt180x120;
	}
	protected HtmlOutputText getTextWelcomePage() {
		if (textWelcomePage == null) {
			textWelcomePage = (HtmlOutputText) findComponentInRoot("textWelcomePage");
		}
		return textWelcomePage;
	}
	protected HtmlOutputText getTextMockUpLinkLabel() {
		if (textMockUpLinkLabel == null) {
			textMockUpLinkLabel = (HtmlOutputText) findComponentInRoot("textMockUpLinkLabel");
		}
		return textMockUpLinkLabel;
	}
	protected HtmlOutputLinkEx getLinkExMockUpLink() {
		if (linkExMockUpLink == null) {
			linkExMockUpLink = (HtmlOutputLinkEx) findComponentInRoot("linkExMockUpLink");
		}
		return linkExMockUpLink;
	}
	protected HtmlCommandExButton getButtonPublishChanges() {
		if (buttonPublishChanges == null) {
			buttonPublishChanges = (HtmlCommandExButton) findComponentInRoot("buttonPublishChanges");
		}
		return buttonPublishChanges;
	}
	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{publishingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return publishingCampaignsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}
	protected UIRichTextEditor getRichTextEditor1() {
		if (richTextEditor1 == null) {
			richTextEditor1 = (UIRichTextEditor) findComponentInRoot("richTextEditor1");
		}
		return richTextEditor1;
	}



	protected UIRichTextEditor getRichTextEditor2() {
		if (richTextEditor2 == null) {
			richTextEditor2 = (UIRichTextEditor) findComponentInRoot("richTextEditor2");
		}
		return richTextEditor2;
	}



	/** 
	 * @managed-bean true
	 */
	protected WelcomePageHandler getWelcomePageHandler() {
		if (welcomePageHandler == null) {
			welcomePageHandler = (WelcomePageHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{welcomePageHandler}")
					.getValue(getFacesContext());
		}
		return welcomePageHandler;
	}



	/** 
	 * @managed-bean true
	 */
	protected void setWelcomePageHandler(WelcomePageHandler welcomePageHandler) {
		this.welcomePageHandler = welcomePageHandler;
	}



	protected HtmlBfPanel getBfpanel1() {
		if (bfpanel1 == null) {
			bfpanel1 = (HtmlBfPanel) findComponentInRoot("bfpanel1");
		}
		return bfpanel1;
	}



	protected HtmlOutputText getTextUpSellImageHeadera() {
		if (textUpSellImageHeadera == null) {
			textUpSellImageHeadera = (HtmlOutputText) findComponentInRoot("textUpSellImageHeadera");
		}
		return textUpSellImageHeadera;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout() {
		if (layoutInnerCustomUpSellImageImageLayout == null) {
			layoutInnerCustomUpSellImageImageLayout = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout");
		}
		return layoutInnerCustomUpSellImageImageLayout;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem() {
		if (formItemCustomUpSellImageFormItem == null) {
			formItemCustomUpSellImageFormItem = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem");
		}
		return formItemCustomUpSellImageFormItem;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL() {
		if (formItemCustomUpSellOnClickURL == null) {
			formItemCustomUpSellOnClickURL = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL");
		}
		return formItemCustomUpSellOnClickURL;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText() {
		if (formItemCustomUpSellImageAltText == null) {
			formItemCustomUpSellImageAltText = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText");
		}
		return formItemCustomUpSellImageAltText;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back() {
		if (tabbedPanelInnerUpSellConfig_back == null) {
			tabbedPanelInnerUpSellConfig_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back");
		}
		return tabbedPanelInnerUpSellConfig_back;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next() {
		if (tabbedPanelInnerUpSellConfig_next == null) {
			tabbedPanelInnerUpSellConfig_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next");
		}
		return tabbedPanelInnerUpSellConfig_next;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish() {
		if (tabbedPanelInnerUpSellConfig_finish == null) {
			tabbedPanelInnerUpSellConfig_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish");
		}
		return tabbedPanelInnerUpSellConfig_finish;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel() {
		if (tabbedPanelInnerUpSellConfig_cancel == null) {
			tabbedPanelInnerUpSellConfig_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel");
		}
		return tabbedPanelInnerUpSellConfig_cancel;
	}



	protected HtmlOutputText getTextUpSellImageHeader4() {
		if (textUpSellImageHeader4 == null) {
			textUpSellImageHeader4 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader4");
		}
		return textUpSellImageHeader4;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout4() {
		if (layoutInnerCustomUpSellImageImageLayout4 == null) {
			layoutInnerCustomUpSellImageImageLayout4 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout4");
		}
		return layoutInnerCustomUpSellImageImageLayout4;
	}



	protected HtmlPanelFormBox getFormBoxCustomImage4() {
		if (formBoxCustomImage4 == null) {
			formBoxCustomImage4 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage4");
		}
		return formBoxCustomImage4;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem4() {
		if (formItemCustomUpSellImageFormItem4 == null) {
			formItemCustomUpSellImageFormItem4 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem4");
		}
		return formItemCustomUpSellImageFormItem4;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL4() {
		if (formItemCustomUpSellOnClickURL4 == null) {
			formItemCustomUpSellOnClickURL4 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL4");
		}
		return formItemCustomUpSellOnClickURL4;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText4() {
		if (formItemCustomUpSellImageAltText4 == null) {
			formItemCustomUpSellImageAltText4 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText4");
		}
		return formItemCustomUpSellImageAltText4;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back4() {
		if (tabbedPanelInnerUpSellConfig_back4 == null) {
			tabbedPanelInnerUpSellConfig_back4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back4");
		}
		return tabbedPanelInnerUpSellConfig_back4;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next4() {
		if (tabbedPanelInnerUpSellConfig_next4 == null) {
			tabbedPanelInnerUpSellConfig_next4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next4");
		}
		return tabbedPanelInnerUpSellConfig_next4;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish4() {
		if (tabbedPanelInnerUpSellConfig_finish4 == null) {
			tabbedPanelInnerUpSellConfig_finish4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish4");
		}
		return tabbedPanelInnerUpSellConfig_finish4;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel4() {
		if (tabbedPanelInnerUpSellConfig_cancel4 == null) {
			tabbedPanelInnerUpSellConfig_cancel4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel4");
		}
		return tabbedPanelInnerUpSellConfig_cancel4;
	}



	protected HtmlOutputText getTextUpSellImageHeader6() {
		if (textUpSellImageHeader6 == null) {
			textUpSellImageHeader6 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader6");
		}
		return textUpSellImageHeader6;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout6() {
		if (layoutInnerCustomUpSellImageImageLayout6 == null) {
			layoutInnerCustomUpSellImageImageLayout6 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout6");
		}
		return layoutInnerCustomUpSellImageImageLayout6;
	}



	protected HtmlPanelFormBox getFormBoxCustomImage6() {
		if (formBoxCustomImage6 == null) {
			formBoxCustomImage6 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage6");
		}
		return formBoxCustomImage6;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem6() {
		if (formItemCustomUpSellImageFormItem6 == null) {
			formItemCustomUpSellImageFormItem6 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem6");
		}
		return formItemCustomUpSellImageFormItem6;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL6() {
		if (formItemCustomUpSellOnClickURL6 == null) {
			formItemCustomUpSellOnClickURL6 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL6");
		}
		return formItemCustomUpSellOnClickURL6;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText6() {
		if (formItemCustomUpSellImageAltText6 == null) {
			formItemCustomUpSellImageAltText6 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText6");
		}
		return formItemCustomUpSellImageAltText6;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back6() {
		if (tabbedPanelInnerUpSellConfig_back6 == null) {
			tabbedPanelInnerUpSellConfig_back6 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back6");
		}
		return tabbedPanelInnerUpSellConfig_back6;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next6() {
		if (tabbedPanelInnerUpSellConfig_next6 == null) {
			tabbedPanelInnerUpSellConfig_next6 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next6");
		}
		return tabbedPanelInnerUpSellConfig_next6;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish6() {
		if (tabbedPanelInnerUpSellConfig_finish6 == null) {
			tabbedPanelInnerUpSellConfig_finish6 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish6");
		}
		return tabbedPanelInnerUpSellConfig_finish6;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel6() {
		if (tabbedPanelInnerUpSellConfig_cancel6 == null) {
			tabbedPanelInnerUpSellConfig_cancel6 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel6");
		}
		return tabbedPanelInnerUpSellConfig_cancel6;
	}



	protected HtmlOutputText getTextUpSellImageHeader8() {
		if (textUpSellImageHeader8 == null) {
			textUpSellImageHeader8 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader8");
		}
		return textUpSellImageHeader8;
	}



	protected HtmlOutputText getTextInnerUpSellNoImageLabel8() {
		if (textInnerUpSellNoImageLabel8 == null) {
			textInnerUpSellNoImageLabel8 = (HtmlOutputText) findComponentInRoot("textInnerUpSellNoImageLabel8");
		}
		return textInnerUpSellNoImageLabel8;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout8() {
		if (layoutInnerCustomUpSellImageImageLayout8 == null) {
			layoutInnerCustomUpSellImageImageLayout8 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout8");
		}
		return layoutInnerCustomUpSellImageImageLayout8;
	}



	protected HtmlPanelFormBox getFormBoxCustomImage8() {
		if (formBoxCustomImage8 == null) {
			formBoxCustomImage8 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage8");
		}
		return formBoxCustomImage8;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem8() {
		if (formItemCustomUpSellImageFormItem8 == null) {
			formItemCustomUpSellImageFormItem8 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem8");
		}
		return formItemCustomUpSellImageFormItem8;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL8() {
		if (formItemCustomUpSellOnClickURL8 == null) {
			formItemCustomUpSellOnClickURL8 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL8");
		}
		return formItemCustomUpSellOnClickURL8;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText8() {
		if (formItemCustomUpSellImageAltText8 == null) {
			formItemCustomUpSellImageAltText8 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText8");
		}
		return formItemCustomUpSellImageAltText8;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back8() {
		if (tabbedPanelInnerUpSellConfig_back8 == null) {
			tabbedPanelInnerUpSellConfig_back8 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back8");
		}
		return tabbedPanelInnerUpSellConfig_back8;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next8() {
		if (tabbedPanelInnerUpSellConfig_next8 == null) {
			tabbedPanelInnerUpSellConfig_next8 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next8");
		}
		return tabbedPanelInnerUpSellConfig_next8;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish8() {
		if (tabbedPanelInnerUpSellConfig_finish8 == null) {
			tabbedPanelInnerUpSellConfig_finish8 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish8");
		}
		return tabbedPanelInnerUpSellConfig_finish8;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel8() {
		if (tabbedPanelInnerUpSellConfig_cancel8 == null) {
			tabbedPanelInnerUpSellConfig_cancel8 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel8");
		}
		return tabbedPanelInnerUpSellConfig_cancel8;
	}



	protected HtmlBfPanel getBfpanel2() {
		if (bfpanel2 == null) {
			bfpanel2 = (HtmlBfPanel) findComponentInRoot("bfpanel2");
		}
		return bfpanel2;
	}



	protected HtmlOutputText getTextUpSellImageHeader11a() {
		if (textUpSellImageHeader11a == null) {
			textUpSellImageHeader11a = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader11a");
		}
		return textUpSellImageHeader11a;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout11() {
		if (layoutInnerCustomUpSellImageImageLayout11 == null) {
			layoutInnerCustomUpSellImageImageLayout11 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout11");
		}
		return layoutInnerCustomUpSellImageImageLayout11;
	}



	protected HtmlPanelFormBox getFormBoxCustomImage11() {
		if (formBoxCustomImage11 == null) {
			formBoxCustomImage11 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage11");
		}
		return formBoxCustomImage11;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem11() {
		if (formItemCustomUpSellImageFormItem11 == null) {
			formItemCustomUpSellImageFormItem11 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem11");
		}
		return formItemCustomUpSellImageFormItem11;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL11() {
		if (formItemCustomUpSellOnClickURL11 == null) {
			formItemCustomUpSellOnClickURL11 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL11");
		}
		return formItemCustomUpSellOnClickURL11;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText11() {
		if (formItemCustomUpSellImageAltText11 == null) {
			formItemCustomUpSellImageAltText11 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText11");
		}
		return formItemCustomUpSellImageAltText11;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back11() {
		if (tabbedPanelInnerUpSellConfig_back11 == null) {
			tabbedPanelInnerUpSellConfig_back11 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back11");
		}
		return tabbedPanelInnerUpSellConfig_back11;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next11() {
		if (tabbedPanelInnerUpSellConfig_next11 == null) {
			tabbedPanelInnerUpSellConfig_next11 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next11");
		}
		return tabbedPanelInnerUpSellConfig_next11;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish11() {
		if (tabbedPanelInnerUpSellConfig_finish11 == null) {
			tabbedPanelInnerUpSellConfig_finish11 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish11");
		}
		return tabbedPanelInnerUpSellConfig_finish11;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel11() {
		if (tabbedPanelInnerUpSellConfig_cancel11 == null) {
			tabbedPanelInnerUpSellConfig_cancel11 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel11");
		}
		return tabbedPanelInnerUpSellConfig_cancel11;
	}



	protected HtmlOutputText getTextUpSellImageHeader31() {
		if (textUpSellImageHeader31 == null) {
			textUpSellImageHeader31 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader31");
		}
		return textUpSellImageHeader31;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout31() {
		if (layoutInnerCustomUpSellImageImageLayout31 == null) {
			layoutInnerCustomUpSellImageImageLayout31 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout31");
		}
		return layoutInnerCustomUpSellImageImageLayout31;
	}



	protected HtmlPanelFormBox getFormBoxCustomImage31() {
		if (formBoxCustomImage31 == null) {
			formBoxCustomImage31 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage31");
		}
		return formBoxCustomImage31;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem31() {
		if (formItemCustomUpSellImageFormItem31 == null) {
			formItemCustomUpSellImageFormItem31 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem31");
		}
		return formItemCustomUpSellImageFormItem31;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL31() {
		if (formItemCustomUpSellOnClickURL31 == null) {
			formItemCustomUpSellOnClickURL31 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL31");
		}
		return formItemCustomUpSellOnClickURL31;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText31() {
		if (formItemCustomUpSellImageAltText31 == null) {
			formItemCustomUpSellImageAltText31 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText31");
		}
		return formItemCustomUpSellImageAltText31;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back31() {
		if (tabbedPanelInnerUpSellConfig_back31 == null) {
			tabbedPanelInnerUpSellConfig_back31 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back31");
		}
		return tabbedPanelInnerUpSellConfig_back31;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next31() {
		if (tabbedPanelInnerUpSellConfig_next31 == null) {
			tabbedPanelInnerUpSellConfig_next31 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next31");
		}
		return tabbedPanelInnerUpSellConfig_next31;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish31() {
		if (tabbedPanelInnerUpSellConfig_finish31 == null) {
			tabbedPanelInnerUpSellConfig_finish31 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish31");
		}
		return tabbedPanelInnerUpSellConfig_finish31;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel31() {
		if (tabbedPanelInnerUpSellConfig_cancel31 == null) {
			tabbedPanelInnerUpSellConfig_cancel31 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel31");
		}
		return tabbedPanelInnerUpSellConfig_cancel31;
	}



	protected HtmlOutputText getTextUpSellImageHeader41() {
		if (textUpSellImageHeader41 == null) {
			textUpSellImageHeader41 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader41");
		}
		return textUpSellImageHeader41;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout41() {
		if (layoutInnerCustomUpSellImageImageLayout41 == null) {
			layoutInnerCustomUpSellImageImageLayout41 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout41");
		}
		return layoutInnerCustomUpSellImageImageLayout41;
	}



	protected HtmlPanelFormBox getFormBoxCustomImage41() {
		if (formBoxCustomImage41 == null) {
			formBoxCustomImage41 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage41");
		}
		return formBoxCustomImage41;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem41() {
		if (formItemCustomUpSellImageFormItem41 == null) {
			formItemCustomUpSellImageFormItem41 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem41");
		}
		return formItemCustomUpSellImageFormItem41;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL41() {
		if (formItemCustomUpSellOnClickURL41 == null) {
			formItemCustomUpSellOnClickURL41 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL41");
		}
		return formItemCustomUpSellOnClickURL41;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText41() {
		if (formItemCustomUpSellImageAltText41 == null) {
			formItemCustomUpSellImageAltText41 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText41");
		}
		return formItemCustomUpSellImageAltText41;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back41() {
		if (tabbedPanelInnerUpSellConfig_back41 == null) {
			tabbedPanelInnerUpSellConfig_back41 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back41");
		}
		return tabbedPanelInnerUpSellConfig_back41;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next41() {
		if (tabbedPanelInnerUpSellConfig_next41 == null) {
			tabbedPanelInnerUpSellConfig_next41 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next41");
		}
		return tabbedPanelInnerUpSellConfig_next41;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish41() {
		if (tabbedPanelInnerUpSellConfig_finish41 == null) {
			tabbedPanelInnerUpSellConfig_finish41 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish41");
		}
		return tabbedPanelInnerUpSellConfig_finish41;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel41() {
		if (tabbedPanelInnerUpSellConfig_cancel41 == null) {
			tabbedPanelInnerUpSellConfig_cancel41 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel41");
		}
		return tabbedPanelInnerUpSellConfig_cancel41;
	}



	protected HtmlOutputText getTextUpSellImageHeader51() {
		if (textUpSellImageHeader51 == null) {
			textUpSellImageHeader51 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader51");
		}
		return textUpSellImageHeader51;
	}



	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout51() {
		if (layoutInnerCustomUpSellImageImageLayout51 == null) {
			layoutInnerCustomUpSellImageImageLayout51 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout51");
		}
		return layoutInnerCustomUpSellImageImageLayout51;
	}



	protected HtmlPanelFormBox getFormBoxCustomImage51() {
		if (formBoxCustomImage51 == null) {
			formBoxCustomImage51 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage51");
		}
		return formBoxCustomImage51;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageFormItem51() {
		if (formItemCustomUpSellImageFormItem51 == null) {
			formItemCustomUpSellImageFormItem51 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem51");
		}
		return formItemCustomUpSellImageFormItem51;
	}



	protected HtmlFormItem getFormItemCustomUpSellOnClickURL51() {
		if (formItemCustomUpSellOnClickURL51 == null) {
			formItemCustomUpSellOnClickURL51 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL51");
		}
		return formItemCustomUpSellOnClickURL51;
	}



	protected HtmlFormItem getFormItemCustomUpSellImageAltText51() {
		if (formItemCustomUpSellImageAltText51 == null) {
			formItemCustomUpSellImageAltText51 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText51");
		}
		return formItemCustomUpSellImageAltText51;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back51() {
		if (tabbedPanelInnerUpSellConfig_back51 == null) {
			tabbedPanelInnerUpSellConfig_back51 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back51");
		}
		return tabbedPanelInnerUpSellConfig_back51;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next51() {
		if (tabbedPanelInnerUpSellConfig_next51 == null) {
			tabbedPanelInnerUpSellConfig_next51 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next51");
		}
		return tabbedPanelInnerUpSellConfig_next51;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish51() {
		if (tabbedPanelInnerUpSellConfig_finish51 == null) {
			tabbedPanelInnerUpSellConfig_finish51 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish51");
		}
		return tabbedPanelInnerUpSellConfig_finish51;
	}



	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel51() {
		if (tabbedPanelInnerUpSellConfig_cancel51 == null) {
			tabbedPanelInnerUpSellConfig_cancel51 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel51");
		}
		return tabbedPanelInnerUpSellConfig_cancel51;
	}



	protected HtmlTabbedPanel getTabbedPanel2() {
		if (tabbedPanel2 == null) {
			tabbedPanel2 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanel2");
		}
		return tabbedPanel2;
	}

}