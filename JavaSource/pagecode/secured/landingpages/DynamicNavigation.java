/**
 * 
 */
package pagecode.secured.landingpages;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlSelectOneMenu;
import com.gannett.usat.dataHandlers.products.ProductsHandler;
import javax.faces.component.UISelectItems;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import javax.faces.component.html.HtmlMessages;
import com.gannett.usat.dataHandlers.GenericFormHandler;

/**
 * @author aeast
 *
 */
public class DynamicNavigation extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelFormBox formBoxProductSelection;
	protected HtmlFormItem formItemProductSelector;
	protected HtmlSelectOneMenu menuProductSelectionMenu;
	protected ProductsHandler productHandler;
	protected UISelectItems selectItems1;
	protected HtmlCommandExButton buttonDynamicNextButton;
	protected CampaignHandler currentDefaultCampaign;
	protected HtmlMessages messages1;
	protected GenericFormHandler genericFormBean;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelFormBox getFormBoxProductSelection() {
		if (formBoxProductSelection == null) {
			formBoxProductSelection = (HtmlPanelFormBox) findComponentInRoot("formBoxProductSelection");
		}
		return formBoxProductSelection;
	}

	protected HtmlFormItem getFormItemProductSelector() {
		if (formItemProductSelector == null) {
			formItemProductSelector = (HtmlFormItem) findComponentInRoot("formItemProductSelector");
		}
		return formItemProductSelector;
	}

	protected HtmlSelectOneMenu getMenuProductSelectionMenu() {
		if (menuProductSelectionMenu == null) {
			menuProductSelectionMenu = (HtmlSelectOneMenu) findComponentInRoot("menuProductSelectionMenu");
		}
		return menuProductSelectionMenu;
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}

	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}

	protected HtmlCommandExButton getButtonDynamicNextButton() {
		if (buttonDynamicNextButton == null) {
			buttonDynamicNextButton = (HtmlCommandExButton) findComponentInRoot("buttonDynamicNextButton");
		}
		return buttonDynamicNextButton;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getCurrentDefaultCampaign() {
		if (currentDefaultCampaign == null) {
			currentDefaultCampaign = (CampaignHandler) getManagedBean("currentDefaultCampaign");
		}
		return currentDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCurrentDefaultCampaign(
			CampaignHandler currentDefaultCampaign) {
		this.currentDefaultCampaign = currentDefaultCampaign;
	}

	public String doButtonDynamicNextButtonAction() {

		String responseStr = "success";
		
		CampaignHandler ch = this.getCurrentDefaultCampaign();
		ProductsHandler ph = this.getProductHandler();
		
		String pubCode = this.getGenericFormBean().getValueString1();
		
		USATProductBO product = ph.getProductForPub(pubCode);
		
		try {
			UsatCampaignIntf campaign = UsatCampaignBO.fetchCampaignForPubAndKeycode(product.getProductCode(), PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
			if (campaign == null) {
				throw new Exception("Default campaign for pub: " + pubCode + " does not exist. IT must manually create it.");
			}
			
			ch.setCampaign(campaign);
			ch.setCampaignSelected(true);
			ch.setCampaignErrorMessage("");
			
			// clear this since used on subsequent page
			this.getGenericFormBean().setValueString1("");
		}
		catch (Exception e) {
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exception : " + e.getMessage() , null);
			this.getFacesContext().addMessage(null, m);
			e.printStackTrace();
			responseStr = "failure";
		}
		
		return responseStr;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	/** 
	 * @managed-bean true
	 */
	protected GenericFormHandler getGenericFormBean() {
		if (genericFormBean == null) {
			genericFormBean = (GenericFormHandler) getManagedBean("genericFormBean");
		}
		return genericFormBean;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGenericFormBean(GenericFormHandler genericFormBean) {
		this.genericFormBean = genericFormBean;
	}

}