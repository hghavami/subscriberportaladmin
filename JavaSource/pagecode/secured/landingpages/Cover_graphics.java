/**
 * 
 */
package pagecode.secured.landingpages;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CoverGraphicsHandler;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableImagePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlButtonPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFileupload;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

/**
 * @author swong
 *
 */
public class Cover_graphics extends PageCodeBase {

	protected HtmlOutputText text1;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected CampaignEditsHandler campaignUtDefaultHandler ;
	protected CampaignEditsHandler campaignSwLandingPagesHandler;
	protected CampaignEditsHandler campaignEditsHandler;
	protected CampaignHandler utLandingPages;
	protected CampaignHandler swLandingPages;
	protected CampaignHandler campaign;

	protected HtmlPanelGroup group23;
	
	protected HtmlPanelGroup group21;
	protected HtmlBfPanel bfpanel1;
	protected HtmlPanelGrid gridMainUpSellImageOptionGrid;
	protected HtmlPanelGroup group1;
	protected HtmlOutputText textUpSellImageHeader;
	protected HtmlMessages messages1;
	protected HtmlGraphicImageEx imageUtDefaultRightPanel;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel;
	protected HtmlPanelGrid gridMainUpSellImageOptionGrid2;
	protected HtmlPanelGroup group3;
	protected HtmlOutputText textUpSellImageHeader2;
	protected HtmlPanelLayout layoutInnerCustomUpSellImageImageLayout2;
	protected HtmlPanelFormBox formBoxCustomImage2;
	protected HtmlFormItem formItemCustomUpSellImageFormItem2;
	protected HtmlFileupload fileuploadSWCustomImageUpload;
	protected HtmlInputText textSWCustomUpSellOnClickURL;
	protected HtmlInputText textSWCustomUpSellImageAltText;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back2;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next2;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish2;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel2;
	protected HtmlCommandExButton tabbedPanel1_back;
	protected HtmlCommandExButton tabbedPanel1_next;
	protected HtmlCommandExButton tabbedPanel1_finish;
	protected HtmlCommandExButton tabbedPanel1_cancel;
	protected HtmlCommandExButton button1;
	protected HtmlTabbedPanel tabbedPanel1;
	protected HtmlTabbedPanel tabbedPanelInnerUpSellConfig;
	protected HtmlBfPanel bfpanelCustomUpSell;
	protected HtmlMessage errorMsgCustomFileUpload;
	protected HtmlFormItem formItemCustomUpSellOnClickURL;
	protected HtmlBfPanel bfpanel2;
	protected HtmlTabbedPanel tabbedPanelInnerUpSellConfig2;
	protected HtmlBfPanel bfpanelCustomUpSell2;
	protected HtmlMessage errorMsgCustomFileUpload2;
	protected HtmlFormItem formItemCustomUpSellOnClickURL2;
	protected HtmlFormItem formItemCustomUpSellImageAltText2;
	protected HtmlButtonPanel bp1;
	protected HtmlPanelBox box1;
	protected HtmlPanelGrid gridCurrentSettings;
	protected HtmlOutputText textCurrentSettingLabel;
	protected HtmlOutputText textCurrentHeaderImageLabel;
	protected HtmlOutputText textCurrentNavImageLabel;
	private String utCoverGraphicsImageName = CampaignEditsHandler.UT_COVER_IMAGE_NAME;
	private String swCoverGraphicsImageName = CampaignEditsHandler.SW_COVER_IMAGE_NAME;
	protected HtmlFileupload fileuploadSWImageUpload;
	protected HtmlPanelGroup grou94;
	protected HtmlGraphicImageEx imageSwDefaultRightPanel;
	protected HtmlPanelFormBox formBoxCustomImage;
	protected HtmlFormItem formItemCustomUpSellImageFormItem;
	protected HtmlFormItem formItemCustomUpSellImageAltText;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected CampaignHandlerReadOnly utDefaultCampaign;
	protected CampaignHandlerReadOnly swDefaultCampaign;
	protected CoverGraphicsHandler coverGraphicsHandler;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected HtmlCommandExButton buttonPublishChanges;
	protected HtmlPanelGrid grid7;
	protected HtmlScriptCollector scriptCollectorJSPC02blue;
	protected HtmlOutputText textSysDescriptionHeader;
	protected HtmlPanelSection sectionLeftNavCheckProdSection;
	protected HtmlJspPanel jspPanelLeftNavCheckProdPanel;
	protected HtmlOutputLinkEx linkExLeftNavToClearProdCache;
	protected HtmlOutputText textLeftNavClearCacheLabel;
	protected HtmlOutputLinkEx linkExLeftNav1;
	protected HtmlOutputText textLeftNav1;
	protected HtmlOutputLinkEx linkExLeftNav2;
	protected HtmlOutputText textLeftNav2;
	protected HtmlOutputLinkEx linkExLeftNavToOmniture;
	protected HtmlOutputText textLeftNav4;
	protected HtmlJspPanel jspPanelLeftNav5;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdCol;
	protected HtmlOutputText textLeftNav11;
	protected HtmlJspPanel jspPanelLeftNav4;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdExp;
	protected HtmlOutputText textLeftNav10;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlFileupload fileuploadUTImageUpload;
	protected HtmlInputText textUTCustomUpSellOnClickURL;
	protected HtmlInputText textUTCustomUpSellImageAltText;
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGroup getGroup23() {
		if (group23 == null) {
			group23 = (HtmlPanelGroup) findComponentInRoot("group23");
		}
		return group23;
	}
public String doButtonPublishChangesAction() {
		
	String responseString = "success";
	CampaignHandler swcampaign = this.getSwLandingPages();
	CampaignHandler utcampaign = this.getUtLandingPages();
	
	
	Collection<CampaignHandler> defaultCampaigns = new ArrayList<CampaignHandler> ();

	defaultCampaigns.add(swcampaign);
	defaultCampaigns.add(utcampaign);
	
	
	// set up collection for publishing
	Collection<CampaignHandler> campaigns = defaultCampaigns;
	for(CampaignHandler ch : campaigns) {
			ch.setCampaignSelected(true);
	}
	this.getPublishingCampaignsHandler().setCollectionData(defaultCampaigns);
	
	return responseString;
	}

	public String doButtonSaveChangesAction() {
		// Type Java code that runs when the component is clicked	
		boolean hasErrors = false;	
		CampaignHandler swcampaign = this.getSwLandingPages();
		CampaignHandler utcampaign = this.getUtLandingPages();
		
		//CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		//editor.setChanged(true);
		CoverGraphicsHandler graphicsEditor = this.getCoverGraphicsHandler();
		
		this.utCoverGraphicsImageName = CampaignEditsHandler.UT_COVER_IMAGE_NAME;
		this.swCoverGraphicsImageName = CampaignEditsHandler.SW_COVER_IMAGE_NAME;	
		
					
		ImageHandler newSwCoverImage = graphicsEditor.getSwCoverImage();			
		ImageHandler newUtCoverImage  = graphicsEditor.getUtCoverImage();
		
		try {		
		
			// set up new images  
			EditableImagePromotionIntf swCoverPromo =  null;
			EditableImagePromotionIntf utCoverPromo =  null;
			
			
			//set Ut
		 if (newUtCoverImage.getImageContents() != null && newUtCoverImage.getImageFileName() != null) {
			utCoverPromo = new EditableImagePromotionBO();
			utCoverPromo.setType(PromotionIntf.PRODUCT_IMAGE_1);
			utCoverPromo.setName(this.utCoverGraphicsImageName);
			utCoverPromo.setImageAltText(newUtCoverImage.getAlternateText());
			utCoverPromo.setImageContents(newUtCoverImage.getImageContents());
			utCoverPromo.setImageLinkToURL(newUtCoverImage.getLinkURL());
			utCoverPromo.setImageFileType(newUtCoverImage.getImageFileType());
			utCoverPromo.setImagePathString("/images/marketingimages/" + this.utCoverGraphicsImageName);
			utcampaign.getCampaign().getPromotionSet().setProductImage1(utCoverPromo);			
			utcampaign.getCampaign().save(false);			
		
			// refresh default campaign
			this.getUtDefaultCampaign().setSourceCampaign(utcampaign);
			
		}
		
		 if (newSwCoverImage.getImageContents() != null && newSwCoverImage.getImageFileName() != null) {
				
			//set SW
			swCoverPromo = new EditableImagePromotionBO();
			swCoverPromo.setType(PromotionIntf.PRODUCT_IMAGE_1);
			swCoverPromo.setName(this.swCoverGraphicsImageName);			
			swCoverPromo.setImageAltText(newSwCoverImage.getAlternateText());
			swCoverPromo.setImageContents(newSwCoverImage.getImageContents());
			swCoverPromo.setImageLinkToURL(newSwCoverImage.getLinkURL());
			swCoverPromo.setImageFileType(newSwCoverImage.getImageFileType());
			swCoverPromo.setImagePathString("/images/marketingimages/" + this.swCoverGraphicsImageName);
			swcampaign.getCampaign().getPromotionSet().setProductImage1(swCoverPromo);			
			swcampaign.getCampaign().save(false);			
		
			// refresh default campaign
			this.getSwDefaultCampaign().setSourceCampaign(swcampaign);
		 }	
			
			hasErrors = false;
			
			}
		catch (Exception e) {
			System.out.println(e.getMessage());
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "FAILED TO set Terms Page Cover Graphics. Contact IT for support.", e.getMessage());
			getMessages1().setErrorStyle("color:Red");
			getMessages1().setShowDetail(true);
			getFacesContext().addMessage(null, message);
			hasErrors = true;
		}
			
		if (hasErrors) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "FAILED TO set Terms Page Cover Graphics Values", "");
			getFacesContext().addMessage(null, message);
			return "failure";
		}
		else {	
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cover Graphics successfully saved", "");
			getFacesContext().addMessage(null, message);
			return "success";
		}
		//	}
					
	}


	protected HtmlPanelGroup getGroup21() {
		if (group21 == null) {
			group21 = (HtmlPanelGroup) findComponentInRoot("group21");
		}
		return group21;
	}

	protected HtmlBfPanel getBfpanel1() {
		if (bfpanel1 == null) {
			bfpanel1 = (HtmlBfPanel) findComponentInRoot("bfpanel1");
		}
		return bfpanel1;
	}

	protected HtmlPanelGrid getGridMainUpSellImageOptionGrid() {
		if (gridMainUpSellImageOptionGrid == null) {
			gridMainUpSellImageOptionGrid = (HtmlPanelGrid) findComponentInRoot("gridMainUpSellImageOptionGrid");
		}
		return gridMainUpSellImageOptionGrid;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlOutputText getTextUpSellImageHeader() {
		if (textUpSellImageHeader == null) {
			textUpSellImageHeader = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader");
		}
		return textUpSellImageHeader;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUtDefaultHandler(
			CampaignEditsHandler UtDefaultHandler) {
		this.campaignUtDefaultHandler = UtDefaultHandler;
	}

	
	
	protected HtmlGraphicImageEx getImageUtDefaultRightPanel() {
		if (imageUtDefaultRightPanel == null) {
			imageUtDefaultRightPanel = (HtmlGraphicImageEx) findComponentInRoot("imageUtDefaultRightPanel");
		}
		return imageUtDefaultRightPanel;
	}

	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout() {
		if (layoutInnerCustomUpSellImageImageLayout == null) {
			layoutInnerCustomUpSellImageImageLayout = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout");
		}
		return layoutInnerCustomUpSellImageImageLayout;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back() {
		if (tabbedPanelInnerUpSellConfig_back == null) {
			tabbedPanelInnerUpSellConfig_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back");
		}
		return tabbedPanelInnerUpSellConfig_back;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next() {
		if (tabbedPanelInnerUpSellConfig_next == null) {
			tabbedPanelInnerUpSellConfig_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next");
		}
		return tabbedPanelInnerUpSellConfig_next;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish() {
		if (tabbedPanelInnerUpSellConfig_finish == null) {
			tabbedPanelInnerUpSellConfig_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish");
		}
		return tabbedPanelInnerUpSellConfig_finish;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel() {
		if (tabbedPanelInnerUpSellConfig_cancel == null) {
			tabbedPanelInnerUpSellConfig_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel");
		}
		return tabbedPanelInnerUpSellConfig_cancel;
	}

	protected HtmlPanelGrid getGridMainUpSellImageOptionGrid2() {
		if (gridMainUpSellImageOptionGrid2 == null) {
			gridMainUpSellImageOptionGrid2 = (HtmlPanelGrid) findComponentInRoot("gridMainUpSellImageOptionGrid2");
		}
		return gridMainUpSellImageOptionGrid2;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlOutputText getTextUpSellImageHeader2() {
		if (textUpSellImageHeader2 == null) {
			textUpSellImageHeader2 = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader2");
		}
		return textUpSellImageHeader2;
	}

	protected HtmlPanelLayout getLayoutInnerCustomUpSellImageImageLayout2() {
		if (layoutInnerCustomUpSellImageImageLayout2 == null) {
			layoutInnerCustomUpSellImageImageLayout2 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomUpSellImageImageLayout2");
		}
		return layoutInnerCustomUpSellImageImageLayout2;
	}

	protected HtmlPanelFormBox getFormBoxCustomImage2() {
		if (formBoxCustomImage2 == null) {
			formBoxCustomImage2 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage2");
		}
		return formBoxCustomImage2;
	}

	protected HtmlFormItem getFormItemCustomUpSellImageFormItem2() {
		if (formItemCustomUpSellImageFormItem2 == null) {
			formItemCustomUpSellImageFormItem2 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem2");
		}
		return formItemCustomUpSellImageFormItem2;
	}

	protected HtmlFileupload getFileuploadSWImageUpload() {
		if (fileuploadSWImageUpload == null) {
			fileuploadSWImageUpload = (HtmlFileupload) findComponentInRoot("fileuploadSWImageUpload");
		}
		return fileuploadSWImageUpload;
	}

	protected HtmlInputText getTextSWCustomUpSellOnClickURL() {
		if (textSWCustomUpSellOnClickURL == null) {
			textSWCustomUpSellOnClickURL = (HtmlInputText) findComponentInRoot("textSWCustomUpSellOnClickURL");
		}
		return textSWCustomUpSellOnClickURL;
	}

	protected HtmlInputText getTextSWCustomUpSellImageAltText() {
		if (textSWCustomUpSellImageAltText == null) {
			textSWCustomUpSellImageAltText = (HtmlInputText) findComponentInRoot("textSWCustomUpSellImageAltText");
		}
		return textSWCustomUpSellImageAltText;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back2() {
		if (tabbedPanelInnerUpSellConfig_back2 == null) {
			tabbedPanelInnerUpSellConfig_back2 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back2");
		}
		return tabbedPanelInnerUpSellConfig_back2;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next2() {
		if (tabbedPanelInnerUpSellConfig_next2 == null) {
			tabbedPanelInnerUpSellConfig_next2 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next2");
		}
		return tabbedPanelInnerUpSellConfig_next2;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish2() {
		if (tabbedPanelInnerUpSellConfig_finish2 == null) {
			tabbedPanelInnerUpSellConfig_finish2 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish2");
		}
		return tabbedPanelInnerUpSellConfig_finish2;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel2() {
		if (tabbedPanelInnerUpSellConfig_cancel2 == null) {
			tabbedPanelInnerUpSellConfig_cancel2 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel2");
		}
		return tabbedPanelInnerUpSellConfig_cancel2;
	}

	protected HtmlCommandExButton getTabbedPanel1_back() {
		if (tabbedPanel1_back == null) {
			tabbedPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_back");
		}
		return tabbedPanel1_back;
	}

	protected HtmlCommandExButton getTabbedPanel1_next() {
		if (tabbedPanel1_next == null) {
			tabbedPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_next");
		}
		return tabbedPanel1_next;
	}

	protected HtmlCommandExButton getTabbedPanel1_finish() {
		if (tabbedPanel1_finish == null) {
			tabbedPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_finish");
		}
		return tabbedPanel1_finish;
	}

	protected HtmlCommandExButton getTabbedPanel1_cancel() {
		if (tabbedPanel1_cancel == null) {
			tabbedPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_cancel");
		}
		return tabbedPanel1_cancel;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlTabbedPanel getTabbedPanel1() {
		if (tabbedPanel1 == null) {
			tabbedPanel1 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanel1");
		}
		return tabbedPanel1;
	}

	protected HtmlTabbedPanel getTabbedPanelInnerUpSellConfig() {
		if (tabbedPanelInnerUpSellConfig == null) {
			tabbedPanelInnerUpSellConfig = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelInnerUpSellConfig");
		}
		return tabbedPanelInnerUpSellConfig;
	}

	protected HtmlBfPanel getBfpanelCustomUpSell() {
		if (bfpanelCustomUpSell == null) {
			bfpanelCustomUpSell = (HtmlBfPanel) findComponentInRoot("bfpanelCustomUpSell");
		}
		return bfpanelCustomUpSell;
	}

	protected HtmlMessage getErrorMsgCustomFileUpload() {
		if (errorMsgCustomFileUpload == null) {
			errorMsgCustomFileUpload = (HtmlMessage) findComponentInRoot("errorMsgCustomFileUpload");
		}
		return errorMsgCustomFileUpload;
	}

	protected HtmlFormItem getFormItemCustomUpSellOnClickURL() {
		if (formItemCustomUpSellOnClickURL == null) {
			formItemCustomUpSellOnClickURL = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL");
		}
		return formItemCustomUpSellOnClickURL;
	}

	protected HtmlBfPanel getBfpanel2() {
		if (bfpanel2 == null) {
			bfpanel2 = (HtmlBfPanel) findComponentInRoot("bfpanel2");
		}
		return bfpanel2;
	}

	protected HtmlTabbedPanel getTabbedPanelInnerUpSellConfig2() {
		if (tabbedPanelInnerUpSellConfig2 == null) {
			tabbedPanelInnerUpSellConfig2 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelInnerUpSellConfig2");
		}
		return tabbedPanelInnerUpSellConfig2;
	}

	protected HtmlBfPanel getBfpanelCustomUpSell2() {
		if (bfpanelCustomUpSell2 == null) {
			bfpanelCustomUpSell2 = (HtmlBfPanel) findComponentInRoot("bfpanelCustomUpSell2");
		}
		return bfpanelCustomUpSell2;
	}

	protected HtmlMessage getErrorMsgCustomFileUpload2() {
		if (errorMsgCustomFileUpload2 == null) {
			errorMsgCustomFileUpload2 = (HtmlMessage) findComponentInRoot("errorMsgCustomFileUpload2");
		}
		return errorMsgCustomFileUpload2;
	}

	protected HtmlFormItem getFormItemCustomUpSellOnClickURL2() {
		if (formItemCustomUpSellOnClickURL2 == null) {
			formItemCustomUpSellOnClickURL2 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellOnClickURL2");
		}
		return formItemCustomUpSellOnClickURL2;
	}

	protected HtmlFormItem getFormItemCustomUpSellImageAltText2() {
		if (formItemCustomUpSellImageAltText2 == null) {
			formItemCustomUpSellImageAltText2 = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText2");
		}
		return formItemCustomUpSellImageAltText2;
	}

	protected HtmlButtonPanel getBp1() {
		if (bp1 == null) {
			bp1 = (HtmlButtonPanel) findComponentInRoot("bp1");
		}
		return bp1;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlPanelGrid getGridCurrentSettings() {
		if (gridCurrentSettings == null) {
			gridCurrentSettings = (HtmlPanelGrid) findComponentInRoot("gridCurrentSettings");
		}
		return gridCurrentSettings;
	}

	protected HtmlOutputText getTextCurrentSettingLabel() {
		if (textCurrentSettingLabel == null) {
			textCurrentSettingLabel = (HtmlOutputText) findComponentInRoot("textCurrentSettingLabel");
		}
		return textCurrentSettingLabel;
	}

	protected HtmlOutputText getTextCurrentHeaderImageLabel() {
		if (textCurrentHeaderImageLabel == null) {
			textCurrentHeaderImageLabel = (HtmlOutputText) findComponentInRoot("textCurrentHeaderImageLabel");
		}
		return textCurrentHeaderImageLabel;
	}

	protected HtmlOutputText getTextCurrentNavImageLabel() {
		if (textCurrentNavImageLabel == null) {
			textCurrentNavImageLabel = (HtmlOutputText) findComponentInRoot("textCurrentNavImageLabel");
		}
		return textCurrentNavImageLabel;
	}

	protected HtmlPanelGroup getGrou94() {
		if (grou94 == null) {
			grou94 = (HtmlPanelGroup) findComponentInRoot("grou94");
		}
		return grou94;
	}

	protected HtmlGraphicImageEx getImageSwDefaultRightPanel() {
		if (imageSwDefaultRightPanel == null) {
			imageSwDefaultRightPanel = (HtmlGraphicImageEx) findComponentInRoot("imageSwDefaultRightPanel");
		}
		return imageSwDefaultRightPanel;
	}

	protected HtmlPanelFormBox getFormBoxCustomImage() {
		if (formBoxCustomImage == null) {
			formBoxCustomImage = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage");
		}
		return formBoxCustomImage;
	}

	protected HtmlFormItem getFormItemCustomUpSellImageFormItem() {
		if (formItemCustomUpSellImageFormItem == null) {
			formItemCustomUpSellImageFormItem = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageFormItem");
		}
		return formItemCustomUpSellImageFormItem;
	}

	protected HtmlFormItem getFormItemCustomUpSellImageAltText() {
		if (formItemCustomUpSellImageAltText == null) {
			formItemCustomUpSellImageAltText = (HtmlFormItem) findComponentInRoot("formItemCustomUpSellImageAltText");
		}
		return formItemCustomUpSellImageAltText;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getUtDefaultCampaign() {
		if (utDefaultCampaign == null) {
			utDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{utDefaultCampaign}").getValue(
							getFacesContext());
		}
		return utDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUtDefaultCampaign(
			CampaignHandlerReadOnly utDefaultCampaign) {
		this.utDefaultCampaign = utDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getSwDefaultCampaign() {
		if (swDefaultCampaign == null) {
			swDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{swDefaultCampaign}").getValue(
							getFacesContext());
		}
		return swDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSwDefaultCampaign(
			CampaignHandlerReadOnly swDefaultCampaign) {
		this.swDefaultCampaign = swDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getSwLandingPages() {
		if (swLandingPages == null) {
			swLandingPages = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding("#{swLandingPages}")
					.getValue(getFacesContext());
		}
		return swLandingPages;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSwLandingPages(CampaignHandler swLandingPages) {
		this.swLandingPages = swLandingPages;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getUtLandingPages() {
		if (utLandingPages == null) {
			utLandingPages = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding("#{utLandingPages}")
					.getValue(getFacesContext());
		}
		return utLandingPages;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUtLandingPages(CampaignHandler utLandingPages) {
		this.utLandingPages = utLandingPages;
	}

	/** 
	 * @managed-bean true
	 */
	protected CoverGraphicsHandler getCoverGraphicsHandler() {
		if (coverGraphicsHandler == null) {
			coverGraphicsHandler = (CoverGraphicsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{coverGraphicsHandler}").getValue(
							getFacesContext());
		}
		return coverGraphicsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCoverGraphicsHandler(
			CoverGraphicsHandler coverGraphicsHandler) {
		this.coverGraphicsHandler = coverGraphicsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{publishingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return publishingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}

	protected HtmlCommandExButton getButtonPublishChanges() {
		if (buttonPublishChanges == null) {
			buttonPublishChanges = (HtmlCommandExButton) findComponentInRoot("buttonPublishChanges");
		}
		return buttonPublishChanges;
	}

	protected HtmlPanelGrid getGrid7() {
		if (grid7 == null) {
			grid7 = (HtmlPanelGrid) findComponentInRoot("grid7");
		}
		return grid7;
	}

	protected HtmlScriptCollector getScriptCollectorJSPC02blue() {
		if (scriptCollectorJSPC02blue == null) {
			scriptCollectorJSPC02blue = (HtmlScriptCollector) findComponentInRoot("scriptCollectorJSPC02blue");
		}
		return scriptCollectorJSPC02blue;
	}

	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}

	protected HtmlPanelSection getSectionLeftNavCheckProdSection() {
		if (sectionLeftNavCheckProdSection == null) {
			sectionLeftNavCheckProdSection = (HtmlPanelSection) findComponentInRoot("sectionLeftNavCheckProdSection");
		}
		return sectionLeftNavCheckProdSection;
	}

	protected HtmlJspPanel getJspPanelLeftNavCheckProdPanel() {
		if (jspPanelLeftNavCheckProdPanel == null) {
			jspPanelLeftNavCheckProdPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNavCheckProdPanel");
		}
		return jspPanelLeftNavCheckProdPanel;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToClearProdCache() {
		if (linkExLeftNavToClearProdCache == null) {
			linkExLeftNavToClearProdCache = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToClearProdCache");
		}
		return linkExLeftNavToClearProdCache;
	}

	protected HtmlOutputText getTextLeftNavClearCacheLabel() {
		if (textLeftNavClearCacheLabel == null) {
			textLeftNavClearCacheLabel = (HtmlOutputText) findComponentInRoot("textLeftNavClearCacheLabel");
		}
		return textLeftNavClearCacheLabel;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav1() {
		if (linkExLeftNav1 == null) {
			linkExLeftNav1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav1");
		}
		return linkExLeftNav1;
	}

	protected HtmlOutputText getTextLeftNav1() {
		if (textLeftNav1 == null) {
			textLeftNav1 = (HtmlOutputText) findComponentInRoot("textLeftNav1");
		}
		return textLeftNav1;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav2() {
		if (linkExLeftNav2 == null) {
			linkExLeftNav2 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav2");
		}
		return linkExLeftNav2;
	}

	protected HtmlOutputText getTextLeftNav2() {
		if (textLeftNav2 == null) {
			textLeftNav2 = (HtmlOutputText) findComponentInRoot("textLeftNav2");
		}
		return textLeftNav2;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToOmniture() {
		if (linkExLeftNavToOmniture == null) {
			linkExLeftNavToOmniture = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToOmniture");
		}
		return linkExLeftNavToOmniture;
	}

	protected HtmlOutputText getTextLeftNav4() {
		if (textLeftNav4 == null) {
			textLeftNav4 = (HtmlOutputText) findComponentInRoot("textLeftNav4");
		}
		return textLeftNav4;
	}

	protected HtmlJspPanel getJspPanelLeftNav5() {
		if (jspPanelLeftNav5 == null) {
			jspPanelLeftNav5 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav5");
		}
		return jspPanelLeftNav5;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdCol() {
		if (imageExLeftNavCheckProdCol == null) {
			imageExLeftNavCheckProdCol = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdCol");
		}
		return imageExLeftNavCheckProdCol;
	}

	protected HtmlOutputText getTextLeftNav11() {
		if (textLeftNav11 == null) {
			textLeftNav11 = (HtmlOutputText) findComponentInRoot("textLeftNav11");
		}
		return textLeftNav11;
	}

	protected HtmlJspPanel getJspPanelLeftNav4() {
		if (jspPanelLeftNav4 == null) {
			jspPanelLeftNav4 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav4");
		}
		return jspPanelLeftNav4;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdExp() {
		if (imageExLeftNavCheckProdExp == null) {
			imageExLeftNavCheckProdExp = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdExp");
		}
		return imageExLeftNavCheckProdExp;
	}

	protected HtmlOutputText getTextLeftNav10() {
		if (textLeftNav10 == null) {
			textLeftNav10 = (HtmlOutputText) findComponentInRoot("textLeftNav10");
		}
		return textLeftNav10;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlFileupload getFileuploadUTImageUpload() {
		if (fileuploadUTImageUpload == null) {
			fileuploadUTImageUpload = (HtmlFileupload) findComponentInRoot("fileuploadUTImageUpload");
		}
		return fileuploadUTImageUpload;
	}

	protected HtmlInputText getTextUTCustomUpSellOnClickURL() {
		if (textUTCustomUpSellOnClickURL == null) {
			textUTCustomUpSellOnClickURL = (HtmlInputText) findComponentInRoot("textUTCustomUpSellOnClickURL");
		}
		return textUTCustomUpSellOnClickURL;
	}

	protected HtmlInputText getTextUTCustomUpSellImageAltText() {
		if (textUTCustomUpSellImageAltText == null) {
			textUTCustomUpSellImageAltText = (HtmlInputText) findComponentInRoot("textUTCustomUpSellImageAltText");
		}
		return textUTCustomUpSellImageAltText;
	}

}