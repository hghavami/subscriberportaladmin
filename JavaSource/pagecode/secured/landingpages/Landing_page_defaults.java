/**
 * 
 */
package pagecode.secured.landingpages;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.gannett.usat.dataHandlers.portalConfig.PortalAppConfigHandler;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlOutputLinkEx;

/**
 * @author aeast
 *
 */
public class Landing_page_defaults extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlOutputText textUTDefaultKeycodeLabel11;
	protected HtmlInputText textUTDefKeycode11;
	protected HtmlRequestLink linkChangeUTDefKeycode11;
	protected HtmlOutputText text311;
	protected HtmlOutputText textSWDefaultKeycodeLabel11;
	protected HtmlInputText textSWDefaultKeycode11;
	protected HtmlRequestLink linkChangeSWDefKeycode11;
	protected HtmlOutputText text411;
	protected HtmlOutputText textUTDefaultKeycodeLabel111;
	protected HtmlInputText textUTDefKeycode111;
	protected HtmlRequestLink linkChangeUTDefKeycode111;
	protected HtmlOutputText text3111;
	protected HtmlOutputText textSWDefaultKeycodeLabel111;
	protected HtmlInputText textSWDefaultKeycode111;
	protected HtmlRequestLink linkChangeSWDefKeycode111;
	protected HtmlOutputText text4111;
	protected HtmlGraphicImageEx imageEx2;
	protected PortalAppConfigHandler portalApplicationSettings;
	protected HtmlGraphicImageEx imageEx21;
	protected HtmlGraphicImageEx imageExUTForceEZPayEnabledImage;
	protected HtmlGraphicImageEx imageEx11;
	protected HtmlDataTableEx productData;
	protected UIColumnEx columnExProductName;
	protected HtmlOutputText text3;
	protected HtmlOutputText text444;
	protected HtmlOutputText text11;
	protected HtmlOutputText textProdNameDT;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text10;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text12;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlPanelSection section1;
	protected HtmlOutputText text18;
	protected HtmlOutputText text17;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text1;
	protected HtmlOutputText text2;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text4;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlOutputText text15;
	protected HtmlDataTableEx tableExDefaultProductPromosOutter;
	protected HtmlOutputText textProdNameDump;
	protected UIColumnEx columnEx4;
	protected HtmlDataTableEx tableEx1;
	protected HtmlOutputText text5;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText text13;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText text14;
	protected UIColumnEx columnEx9;
	protected HtmlOutputText text16;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputLinkEx linkEx11;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlOutputText getTextUTDefaultKeycodeLabel11() {
		if (textUTDefaultKeycodeLabel11 == null) {
			textUTDefaultKeycodeLabel11 = (HtmlOutputText) findComponentInRoot("textUTDefaultKeycodeLabel11");
		}
		return textUTDefaultKeycodeLabel11;
	}

	protected HtmlInputText getTextUTDefKeycode11() {
		if (textUTDefKeycode11 == null) {
			textUTDefKeycode11 = (HtmlInputText) findComponentInRoot("textUTDefKeycode11");
		}
		return textUTDefKeycode11;
	}

	protected HtmlRequestLink getLinkChangeUTDefKeycode11() {
		if (linkChangeUTDefKeycode11 == null) {
			linkChangeUTDefKeycode11 = (HtmlRequestLink) findComponentInRoot("linkChangeUTDefKeycode11");
		}
		return linkChangeUTDefKeycode11;
	}

	protected HtmlOutputText getText311() {
		if (text311 == null) {
			text311 = (HtmlOutputText) findComponentInRoot("text311");
		}
		return text311;
	}

	protected HtmlOutputText getTextSWDefaultKeycodeLabel11() {
		if (textSWDefaultKeycodeLabel11 == null) {
			textSWDefaultKeycodeLabel11 = (HtmlOutputText) findComponentInRoot("textSWDefaultKeycodeLabel11");
		}
		return textSWDefaultKeycodeLabel11;
	}

	protected HtmlInputText getTextSWDefaultKeycode11() {
		if (textSWDefaultKeycode11 == null) {
			textSWDefaultKeycode11 = (HtmlInputText) findComponentInRoot("textSWDefaultKeycode11");
		}
		return textSWDefaultKeycode11;
	}

	protected HtmlRequestLink getLinkChangeSWDefKeycode11() {
		if (linkChangeSWDefKeycode11 == null) {
			linkChangeSWDefKeycode11 = (HtmlRequestLink) findComponentInRoot("linkChangeSWDefKeycode11");
		}
		return linkChangeSWDefKeycode11;
	}

	protected HtmlOutputText getText411() {
		if (text411 == null) {
			text411 = (HtmlOutputText) findComponentInRoot("text411");
		}
		return text411;
	}

	protected HtmlOutputText getTextUTDefaultKeycodeLabel111() {
		if (textUTDefaultKeycodeLabel111 == null) {
			textUTDefaultKeycodeLabel111 = (HtmlOutputText) findComponentInRoot("textUTDefaultKeycodeLabel111");
		}
		return textUTDefaultKeycodeLabel111;
	}

	protected HtmlInputText getTextUTDefKeycode111() {
		if (textUTDefKeycode111 == null) {
			textUTDefKeycode111 = (HtmlInputText) findComponentInRoot("textUTDefKeycode111");
		}
		return textUTDefKeycode111;
	}

	protected HtmlRequestLink getLinkChangeUTDefKeycode111() {
		if (linkChangeUTDefKeycode111 == null) {
			linkChangeUTDefKeycode111 = (HtmlRequestLink) findComponentInRoot("linkChangeUTDefKeycode111");
		}
		return linkChangeUTDefKeycode111;
	}

	protected HtmlOutputText getText3111() {
		if (text3111 == null) {
			text3111 = (HtmlOutputText) findComponentInRoot("text3111");
		}
		return text3111;
	}

	protected HtmlOutputText getTextSWDefaultKeycodeLabel111() {
		if (textSWDefaultKeycodeLabel111 == null) {
			textSWDefaultKeycodeLabel111 = (HtmlOutputText) findComponentInRoot("textSWDefaultKeycodeLabel111");
		}
		return textSWDefaultKeycodeLabel111;
	}

	protected HtmlInputText getTextSWDefaultKeycode111() {
		if (textSWDefaultKeycode111 == null) {
			textSWDefaultKeycode111 = (HtmlInputText) findComponentInRoot("textSWDefaultKeycode111");
		}
		return textSWDefaultKeycode111;
	}

	protected HtmlRequestLink getLinkChangeSWDefKeycode111() {
		if (linkChangeSWDefKeycode111 == null) {
			linkChangeSWDefKeycode111 = (HtmlRequestLink) findComponentInRoot("linkChangeSWDefKeycode111");
		}
		return linkChangeSWDefKeycode111;
	}

	protected HtmlOutputText getText4111() {
		if (text4111 == null) {
			text4111 = (HtmlOutputText) findComponentInRoot("text4111");
		}
		return text4111;
	}

	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}

	/** 
	 * @managed-bean true
	 */
	protected PortalAppConfigHandler getPortalApplicationSettings() {
		if (portalApplicationSettings == null) {
			portalApplicationSettings = (PortalAppConfigHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{portalApplicationSettings}").getValue(
							getFacesContext());
		}
		return portalApplicationSettings;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPortalApplicationSettings(
			PortalAppConfigHandler portalApplicationSettings) {
		this.portalApplicationSettings = portalApplicationSettings;
	}

	protected HtmlGraphicImageEx getImageEx21() {
		if (imageEx21 == null) {
			imageEx21 = (HtmlGraphicImageEx) findComponentInRoot("imageEx21");
		}
		return imageEx21;
	}

	protected HtmlGraphicImageEx getImageExUTForceEZPayEnabledImage() {
		if (imageExUTForceEZPayEnabledImage == null) {
			imageExUTForceEZPayEnabledImage = (HtmlGraphicImageEx) findComponentInRoot("imageExUTForceEZPayEnabledImage");
		}
		return imageExUTForceEZPayEnabledImage;
	}

	protected HtmlGraphicImageEx getImageEx11() {
		if (imageEx11 == null) {
			imageEx11 = (HtmlGraphicImageEx) findComponentInRoot("imageEx11");
		}
		return imageEx11;
	}

	protected HtmlDataTableEx getProductData() {
		if (productData == null) {
			productData = (HtmlDataTableEx) findComponentInRoot("productData");
		}
		return productData;
	}

	protected UIColumnEx getColumnExProductName() {
		if (columnExProductName == null) {
			columnExProductName = (UIColumnEx) findComponentInRoot("columnExProductName");
		}
		return columnExProductName;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText444() {
		if (text444 == null) {
			text444 = (HtmlOutputText) findComponentInRoot("text444");
		}
		return text444;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlOutputText getTextProdNameDT() {
		if (textProdNameDT == null) {
			textProdNameDT = (HtmlOutputText) findComponentInRoot("textProdNameDT");
		}
		return textProdNameDT;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlPanelSection getSection1() {
		if (section1 == null) {
			section1 = (HtmlPanelSection) findComponentInRoot("section1");
		}
		return section1;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected HtmlDataTableEx getTableExDefaultProductPromosOutter() {
		if (tableExDefaultProductPromosOutter == null) {
			tableExDefaultProductPromosOutter = (HtmlDataTableEx) findComponentInRoot("tableExDefaultProductPromosOutter");
		}
		return tableExDefaultProductPromosOutter;
	}

	protected HtmlOutputText getTextProdNameDump() {
		if (textProdNameDump == null) {
			textProdNameDump = (HtmlOutputText) findComponentInRoot("textProdNameDump");
		}
		return textProdNameDump;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlDataTableEx getTableEx1() {
		if (tableEx1 == null) {
			tableEx1 = (HtmlDataTableEx) findComponentInRoot("tableEx1");
		}
		return tableEx1;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected UIColumnEx getColumnEx9() {
		if (columnEx9 == null) {
			columnEx9 = (UIColumnEx) findComponentInRoot("columnEx9");
		}
		return columnEx9;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlOutputLinkEx getLinkEx11() {
		if (linkEx11 == null) {
			linkEx11 = (HtmlOutputLinkEx) findComponentInRoot("linkEx11");
		}
		return linkEx11;
	}

}