/**
 * 
 */
package pagecode.secured.landingpages;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.WelcomePageHandler;
import com.gannett.usat.dataHandlers.products.ProductsHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableImagePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlButtonPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFileupload;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 *
 */
public class LandingPageDefaultsDetail extends PageCodeBase {

	protected CampaignHandler currentDefaultCampaign;
	protected HtmlScriptCollector scriptCollectorJSPC02blue;
	protected HtmlOutputText textSysDescriptionHeader;
	protected HtmlJspPanel jspPanelLeftNavCheckProdPanel;
	protected HtmlOutputLinkEx linkExLeftNavToClearProdCache;
	protected HtmlOutputText textLeftNavClearCacheLabel;
	protected HtmlOutputLinkEx linkExLeftNav1;
	protected HtmlOutputText textLeftNav1;
	protected HtmlOutputLinkEx linkExLeftNav2;
	protected HtmlOutputText textLeftNav2;
	protected HtmlOutputLinkEx linkExLeftNavToOmniture;
	protected HtmlOutputText textLeftNav4;
	protected HtmlJspPanel jspPanelLeftNav5;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdCol;
	protected HtmlJspPanel jspPanelLeftNav4;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdExp;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formLandingPageDefaults;
	protected HtmlMessages messages1;
	protected HtmlPanelBox boxPanelLeft;
	protected HtmlTabbedPanel tabbedPanel1;
	protected HtmlCommandExButton tabbedPanel2_back;
	protected HtmlCommandExButton tabbedPanel2_next;
	protected HtmlCommandExButton tabbedPanel2_finish;
	protected HtmlCommandExButton tabbedPanel2_cancel;
	protected HtmlPanelGroup group1A;
	protected HtmlOutputText textWelcomePage;
	protected HtmlPanelSection sectionLeftNavCheckProdSection;
	protected HtmlOutputText textLeftNav11;
	protected HtmlOutputText textLeftNav10;
	protected HtmlPanelGrid gridMainGrid;
	protected HtmlBfPanel bfpanel3;
	protected HtmlBfPanel bfpanel4;
	protected HtmlBfPanel bfpanel5;
	protected HtmlBfPanel bfpanel6;
	protected HtmlButtonPanel bp1;
	protected ProductsHandler productHandler;
	protected HtmlPanelGrid gridFooterImageOptionGrid8;
	protected HtmlPanelGroup groupFooterImage1;
	protected HtmlOutputText textFooterImageHeader8;
	protected HtmlSelectOneRadio radioFooterOpSelect;
	protected HtmlBfPanel bfpanelFooterCurrent4;
	protected HtmlJspPanel jspPanelInnerCustomFooterNoImage4;
	protected HtmlPanelLayout layoutInnerCustomFooterImageImageLayout4;
	protected HtmlPanelFormBox formBoxCustomFooterImage4;
	protected HtmlFormItem formItemCustomFooterImageFormItem4;
	protected HtmlFileupload fileuploadCustomFooterImageUpload4;
	protected HtmlInputText textCustomFooterOnClickURL4;
	protected HtmlInputText textCustomFooterImageAltText4;
	protected HtmlCommandExButton tabbedPanelInnerFooterConfig_back4;
	protected HtmlCommandExButton tabbedPanelInnerFooterConfig_next4;
	protected HtmlCommandExButton tabbedPanelInnerFooterConfig_finish4;
	protected HtmlCommandExButton tabbedPanelInnerFooterConfig_cancel4;
	protected HtmlTabbedPanel tabbedPanelInnerFooterConfig4;
	protected HtmlBfPanel bfpanelNoFooter4;
	protected HtmlOutputText textInnerFooterNoImageLabel4;
	protected HtmlBfPanel bfpanelCustomFooter;
	protected HtmlMessage errorMsgCustomFooterFileUpload4;
	protected HtmlFormItem formItemCustomFooterOnClickURL4;
	protected HtmlFormItem formItemCustomFooterImageAltText4;
	protected HtmlPanelGrid gridImage1OptionGrid8;
	protected HtmlPanelGroup groupImage11;
	protected HtmlOutputText textImage1Header8;
	protected HtmlSelectOneRadio radioImage1OpSelect;
	protected HtmlTabbedPanel tabbedPanelInnerImage1Config4;
	protected HtmlBfPanel bfpanelImage1Current4;
	protected HtmlPanelGrid gridImage1CustomImageDefaultImage4;
	protected HtmlGraphicImageEx imageImage1;
	protected HtmlPanelBox boxCurrentImage1DefaultPanelBoxLeft4;
	protected HtmlOutputText textCustomImage1DefaultLabel4;
	protected HtmlBfPanel bfpanelNoImage1;
	protected HtmlJspPanel jspPanelInnerCustomImage1NoImage4;
	protected HtmlOutputText textInnerImage1NoImageLabel4;
	protected HtmlBfPanel bfpanelCustomImage1;
	protected HtmlPanelLayout layoutInnerCustomImage1ImageLayout4;
	protected HtmlPanelFormBox formBoxCustomImage1;
	protected HtmlFormItem formItemCustomImage1FormItem4;
	protected HtmlFileupload fileuploadCustomImage1Upload4;
	protected HtmlMessage errorMsgCustomImage1FileUpload4;
	protected HtmlFormItem formItemCustomImage1OnClickURL4;
	protected HtmlInputText textCustomImage1OnClickURL4;
	protected HtmlFormItem formItemCustomImage1AltText4;
	protected HtmlInputText textCustomImage1AltText4;
	protected HtmlCommandExButton tabbedPanelInnerImage1Config_back4;
	protected HtmlCommandExButton tabbedPanelInnerImage1Config_next4;
	protected HtmlCommandExButton tabbedPanelInnerImage1Config_finish4;
	protected HtmlCommandExButton tabbedPanelInnerImage1Config_cancel4;
	protected HtmlPanelGroup groupImage2;
	protected HtmlPanelGrid gridImage2OptionGrid8;
	protected HtmlPanelGroup groupImage21;
	protected HtmlOutputText textImage2Header8;
	protected HtmlSelectOneRadio radioImage2OpSelect;
	protected HtmlTabbedPanel tabbedPanelInnerImage2Config4;
	protected HtmlBfPanel bfpanelImage2Current4;
	protected HtmlPanelGrid gridImage2CustomImageDefaultImage4;
	protected HtmlGraphicImageEx imageImage2;
	protected HtmlPanelBox boxCurrentImage2DefaultPanelBoxLeft4;
	protected HtmlOutputText textCustomImage2DefaultLabel4;
	protected HtmlBfPanel bfpanelNoImage2;
	protected HtmlJspPanel jspPanelInnerCustomImage2NoImage4;
	protected HtmlOutputText textInnerImage2NoImageLabel4;
	protected HtmlBfPanel bfpanelCustomImage2;
	protected HtmlPanelLayout layoutInnerCustomImage2ImageLayout4;
	protected HtmlPanelFormBox formBoxCustomImage2;
	protected HtmlFormItem formItemCustomImage2FormItem4;
	protected HtmlFileupload fileuploadCustomImage2Upload4;
	protected HtmlMessage errorMsgCustomImage2FileUpload4;
	protected HtmlFormItem formItemCustomImage2OnClickURL4;
	protected HtmlInputText textCustomImage2OnClickURL4;
	protected HtmlFormItem formItemCustomImage2AltText4;
	protected HtmlInputText textCustomImage2AltText4;
	protected HtmlCommandExButton tabbedPanelInnerImage2Config_back4;
	protected HtmlCommandExButton tabbedPanelInnerImage2Config_next4;
	protected HtmlCommandExButton tabbedPanelInnerImage2Config_finish4;
	protected HtmlCommandExButton tabbedPanelInnerImage2Config_cancel4;
	protected HtmlPanelGrid gridImage3OptionGrid8;
	protected HtmlPanelGroup groupImage31;
	protected HtmlOutputText textImage3Header8;
	protected HtmlSelectOneRadio radioImage3OpSelect;
	protected HtmlTabbedPanel tabbedPanelInnerImage3Config4;
	protected HtmlBfPanel bfpanelImage3Current4;
	protected HtmlPanelGrid gridImage3CustomImageDefaultImage4;
	protected HtmlPanelGroup groupImage3;
	protected HtmlGraphicImageEx imageImage3;
	protected HtmlPanelBox boxCurrentImage3DefaultPanelBoxLeft4;
	protected HtmlOutputText textCustomImage3DefaultLabel4;
	protected HtmlBfPanel bfpanelNoImage3;
	protected HtmlJspPanel jspPanelInnerCustomImage3NoImage4;
	protected HtmlOutputText textInnerImage3NoImageLabel4;
	protected HtmlBfPanel bfpanelCustomImage3;
	protected HtmlPanelLayout layoutInnerCustomImage3ImageLayout4;
	protected HtmlPanelFormBox formBoxCustomImage3;
	protected HtmlFormItem formItemCustomImage3FormItem4;
	protected HtmlFileupload fileuploadCustomImage3Upload4;
	protected HtmlMessage errorMsgCustomImage3FileUpload4;
	protected HtmlFormItem formItemCustomImage3OnClickURL4;
	protected HtmlInputText textCustomImage3OnClickURL4;
	protected HtmlFormItem formItemCustomImage3AltText4;
	protected HtmlInputText textCustomImage3AltText4;
	protected HtmlCommandExButton tabbedPanelInnerImage3Config_back4;
	protected HtmlCommandExButton tabbedPanelInnerImage3Config_next4;
	protected HtmlCommandExButton tabbedPanelInnerImage3Config_finish4;
	protected HtmlCommandExButton tabbedPanelInnerImage3Config_cancel4;
	protected HtmlCommandExButton buttonPublishCampaign;
	protected HtmlCommandExButton buttonSaveChanges;
	protected HtmlPanelGrid gridFooterCustomImageDefaultImage4;
	protected HtmlPanelGroup groupFooter7;
	protected HtmlGraphicImageEx imageFooter;
	protected HtmlOutputText textCustomFooterImageDefaultLabel4;
	protected HtmlPanelBox boxCurrentFooterDefaultPanelBoxLeft4;
	protected HtmlPanelGroup groupImage1;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected WelcomePageHandler welcomePageHandler;
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getCurrentDefaultCampaign() {
		if (currentDefaultCampaign == null) {
			currentDefaultCampaign = (CampaignHandler) getManagedBean("currentDefaultCampaign");
		}
		return currentDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCurrentDefaultCampaign(
			CampaignHandler currentDefaultCampaign) {
		this.currentDefaultCampaign = currentDefaultCampaign;
	}

	public String doLinkClearPromoTextAction() {
		
		this.getCurrentDefaultCampaign().getCampaign().getPromotionSet().setPromoLandingPageText(null);
		
		return "success";

	}

	protected HtmlScriptCollector getScriptCollectorJSPC02blue() {
		if (scriptCollectorJSPC02blue == null) {
			scriptCollectorJSPC02blue = (HtmlScriptCollector) findComponentInRoot("scriptCollectorJSPC02blue");
		}
		return scriptCollectorJSPC02blue;
	}

	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}

	protected HtmlJspPanel getJspPanelLeftNavCheckProdPanel() {
		if (jspPanelLeftNavCheckProdPanel == null) {
			jspPanelLeftNavCheckProdPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNavCheckProdPanel");
		}
		return jspPanelLeftNavCheckProdPanel;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToClearProdCache() {
		if (linkExLeftNavToClearProdCache == null) {
			linkExLeftNavToClearProdCache = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToClearProdCache");
		}
		return linkExLeftNavToClearProdCache;
	}

	protected HtmlOutputText getTextLeftNavClearCacheLabel() {
		if (textLeftNavClearCacheLabel == null) {
			textLeftNavClearCacheLabel = (HtmlOutputText) findComponentInRoot("textLeftNavClearCacheLabel");
		}
		return textLeftNavClearCacheLabel;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav1() {
		if (linkExLeftNav1 == null) {
			linkExLeftNav1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav1");
		}
		return linkExLeftNav1;
	}

	protected HtmlOutputText getTextLeftNav1() {
		if (textLeftNav1 == null) {
			textLeftNav1 = (HtmlOutputText) findComponentInRoot("textLeftNav1");
		}
		return textLeftNav1;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav2() {
		if (linkExLeftNav2 == null) {
			linkExLeftNav2 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav2");
		}
		return linkExLeftNav2;
	}

	protected HtmlOutputText getTextLeftNav2() {
		if (textLeftNav2 == null) {
			textLeftNav2 = (HtmlOutputText) findComponentInRoot("textLeftNav2");
		}
		return textLeftNav2;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToOmniture() {
		if (linkExLeftNavToOmniture == null) {
			linkExLeftNavToOmniture = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToOmniture");
		}
		return linkExLeftNavToOmniture;
	}

	protected HtmlOutputText getTextLeftNav4() {
		if (textLeftNav4 == null) {
			textLeftNav4 = (HtmlOutputText) findComponentInRoot("textLeftNav4");
		}
		return textLeftNav4;
	}

	protected HtmlJspPanel getJspPanelLeftNav5() {
		if (jspPanelLeftNav5 == null) {
			jspPanelLeftNav5 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav5");
		}
		return jspPanelLeftNav5;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdCol() {
		if (imageExLeftNavCheckProdCol == null) {
			imageExLeftNavCheckProdCol = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdCol");
		}
		return imageExLeftNavCheckProdCol;
	}

	protected HtmlJspPanel getJspPanelLeftNav4() {
		if (jspPanelLeftNav4 == null) {
			jspPanelLeftNav4 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav4");
		}
		return jspPanelLeftNav4;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdExp() {
		if (imageExLeftNavCheckProdExp == null) {
			imageExLeftNavCheckProdExp = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdExp");
		}
		return imageExLeftNavCheckProdExp;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormLandingPageDefaults() {
		if (formLandingPageDefaults == null) {
			formLandingPageDefaults = (HtmlForm) findComponentInRoot("formLandingPageDefaults");
		}
		return formLandingPageDefaults;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlPanelBox getBoxPanelLeft() {
		if (boxPanelLeft == null) {
			boxPanelLeft = (HtmlPanelBox) findComponentInRoot("boxPanelLeft");
		}
		return boxPanelLeft;
	}

	protected HtmlTabbedPanel getTabbedPanel1() {
		if (tabbedPanel1 == null) {
			tabbedPanel1 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanel1");
		}
		return tabbedPanel1;
	}

	protected HtmlCommandExButton getTabbedPanel2_back() {
		if (tabbedPanel2_back == null) {
			tabbedPanel2_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel2_back");
		}
		return tabbedPanel2_back;
	}

	protected HtmlCommandExButton getTabbedPanel2_next() {
		if (tabbedPanel2_next == null) {
			tabbedPanel2_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel2_next");
		}
		return tabbedPanel2_next;
	}

	protected HtmlCommandExButton getTabbedPanel2_finish() {
		if (tabbedPanel2_finish == null) {
			tabbedPanel2_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel2_finish");
		}
		return tabbedPanel2_finish;
	}

	protected HtmlCommandExButton getTabbedPanel2_cancel() {
		if (tabbedPanel2_cancel == null) {
			tabbedPanel2_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel2_cancel");
		}
		return tabbedPanel2_cancel;
	}

	protected HtmlPanelGroup getGroup1A() {
		if (group1A == null) {
			group1A = (HtmlPanelGroup) findComponentInRoot("group1A");
		}
		return group1A;
	}

	protected HtmlOutputText getTextWelcomePage() {
		if (textWelcomePage == null) {
			textWelcomePage = (HtmlOutputText) findComponentInRoot("textWelcomePage");
		}
		return textWelcomePage;
	}

	protected HtmlPanelSection getSectionLeftNavCheckProdSection() {
		if (sectionLeftNavCheckProdSection == null) {
			sectionLeftNavCheckProdSection = (HtmlPanelSection) findComponentInRoot("sectionLeftNavCheckProdSection");
		}
		return sectionLeftNavCheckProdSection;
	}

	protected HtmlOutputText getTextLeftNav11() {
		if (textLeftNav11 == null) {
			textLeftNav11 = (HtmlOutputText) findComponentInRoot("textLeftNav11");
		}
		return textLeftNav11;
	}

	protected HtmlOutputText getTextLeftNav10() {
		if (textLeftNav10 == null) {
			textLeftNav10 = (HtmlOutputText) findComponentInRoot("textLeftNav10");
		}
		return textLeftNav10;
	}

	protected HtmlPanelGrid getGridMainGrid() {
		if (gridMainGrid == null) {
			gridMainGrid = (HtmlPanelGrid) findComponentInRoot("gridMainGrid");
		}
		return gridMainGrid;
	}

	protected HtmlBfPanel getBfpanel3() {
		if (bfpanel3 == null) {
			bfpanel3 = (HtmlBfPanel) findComponentInRoot("bfpanel3");
		}
		return bfpanel3;
	}

	protected HtmlBfPanel getBfpanel4() {
		if (bfpanel4 == null) {
			bfpanel4 = (HtmlBfPanel) findComponentInRoot("bfpanel4");
		}
		return bfpanel4;
	}

	protected HtmlBfPanel getBfpanel5() {
		if (bfpanel5 == null) {
			bfpanel5 = (HtmlBfPanel) findComponentInRoot("bfpanel5");
		}
		return bfpanel5;
	}

	protected HtmlBfPanel getBfpanel6() {
		if (bfpanel6 == null) {
			bfpanel6 = (HtmlBfPanel) findComponentInRoot("bfpanel6");
		}
		return bfpanel6;
	}

	protected HtmlButtonPanel getBp1() {
		if (bp1 == null) {
			bp1 = (HtmlButtonPanel) findComponentInRoot("bp1");
		}
		return bp1;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	
		CampaignHandler ch = this.getCurrentDefaultCampaign();

		// welcome page changes are for UT pub only
		if (ch.getCampaign() == null || ! ch.getCampaign().getPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
			String pubCode = "UT";  // UT controls welcome page
			
			ProductsHandler ph = this.getProductHandler();
			USATProductBO product = ph.getProductForPub(pubCode);
			
			try {
				UsatCampaignIntf campaign = UsatCampaignBO.fetchCampaignForPubAndKeycode(product.getProductCode(), PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
				if (campaign == null) {
					throw new Exception("Default campaign for pub: " + pubCode + " does not exist. IT must manually create it.");
				}
				
				ch.setCampaign(campaign);
				ch.setCampaignSelected(true);
				ch.setCampaignErrorMessage("");
			}
			catch (Exception e) {
				FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exception : " + e.getMessage() , null);
				this.getFacesContext().addMessage(null, m);
				e.printStackTrace();
			}
		
		}
		
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}

	protected HtmlPanelGrid getGridFooterImageOptionGrid8() {
		if (gridFooterImageOptionGrid8 == null) {
			gridFooterImageOptionGrid8 = (HtmlPanelGrid) findComponentInRoot("gridFooterImageOptionGrid8");
		}
		return gridFooterImageOptionGrid8;
	}

	protected HtmlPanelGroup getGroupFooterImage1() {
		if (groupFooterImage1 == null) {
			groupFooterImage1 = (HtmlPanelGroup) findComponentInRoot("groupFooterImage1");
		}
		return groupFooterImage1;
	}

	protected HtmlOutputText getTextFooterImageHeader8() {
		if (textFooterImageHeader8 == null) {
			textFooterImageHeader8 = (HtmlOutputText) findComponentInRoot("textFooterImageHeader8");
		}
		return textFooterImageHeader8;
	}

	protected HtmlSelectOneRadio getRadioFooterOpSelect() {
		if (radioFooterOpSelect == null) {
			radioFooterOpSelect = (HtmlSelectOneRadio) findComponentInRoot("radioFooterOpSelect");
		}
		return radioFooterOpSelect;
	}

	protected HtmlBfPanel getBfpanelFooterCurrent4() {
		if (bfpanelFooterCurrent4 == null) {
			bfpanelFooterCurrent4 = (HtmlBfPanel) findComponentInRoot("bfpanelFooterCurrent4");
		}
		return bfpanelFooterCurrent4;
	}

	protected HtmlJspPanel getJspPanelInnerCustomFooterNoImage4() {
		if (jspPanelInnerCustomFooterNoImage4 == null) {
			jspPanelInnerCustomFooterNoImage4 = (HtmlJspPanel) findComponentInRoot("jspPanelInnerCustomFooterNoImage4");
		}
		return jspPanelInnerCustomFooterNoImage4;
	}

	protected HtmlPanelLayout getLayoutInnerCustomFooterImageImageLayout4() {
		if (layoutInnerCustomFooterImageImageLayout4 == null) {
			layoutInnerCustomFooterImageImageLayout4 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomFooterImageImageLayout4");
		}
		return layoutInnerCustomFooterImageImageLayout4;
	}

	protected HtmlPanelFormBox getFormBoxCustomFooterImage4() {
		if (formBoxCustomFooterImage4 == null) {
			formBoxCustomFooterImage4 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomFooterImage4");
		}
		return formBoxCustomFooterImage4;
	}

	protected HtmlFormItem getFormItemCustomFooterImageFormItem4() {
		if (formItemCustomFooterImageFormItem4 == null) {
			formItemCustomFooterImageFormItem4 = (HtmlFormItem) findComponentInRoot("formItemCustomFooterImageFormItem4");
		}
		return formItemCustomFooterImageFormItem4;
	}

	protected HtmlFileupload getFileuploadCustomFooterImageUpload4() {
		if (fileuploadCustomFooterImageUpload4 == null) {
			fileuploadCustomFooterImageUpload4 = (HtmlFileupload) findComponentInRoot("fileuploadCustomFooterImageUpload4");
		}
		return fileuploadCustomFooterImageUpload4;
	}

	protected HtmlInputText getTextCustomFooterOnClickURL4() {
		if (textCustomFooterOnClickURL4 == null) {
			textCustomFooterOnClickURL4 = (HtmlInputText) findComponentInRoot("textCustomFooterOnClickURL4");
		}
		return textCustomFooterOnClickURL4;
	}

	protected HtmlInputText getTextCustomFooterImageAltText4() {
		if (textCustomFooterImageAltText4 == null) {
			textCustomFooterImageAltText4 = (HtmlInputText) findComponentInRoot("textCustomFooterImageAltText4");
		}
		return textCustomFooterImageAltText4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerFooterConfig_back4() {
		if (tabbedPanelInnerFooterConfig_back4 == null) {
			tabbedPanelInnerFooterConfig_back4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerFooterConfig_back4");
		}
		return tabbedPanelInnerFooterConfig_back4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerFooterConfig_next4() {
		if (tabbedPanelInnerFooterConfig_next4 == null) {
			tabbedPanelInnerFooterConfig_next4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerFooterConfig_next4");
		}
		return tabbedPanelInnerFooterConfig_next4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerFooterConfig_finish4() {
		if (tabbedPanelInnerFooterConfig_finish4 == null) {
			tabbedPanelInnerFooterConfig_finish4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerFooterConfig_finish4");
		}
		return tabbedPanelInnerFooterConfig_finish4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerFooterConfig_cancel4() {
		if (tabbedPanelInnerFooterConfig_cancel4 == null) {
			tabbedPanelInnerFooterConfig_cancel4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerFooterConfig_cancel4");
		}
		return tabbedPanelInnerFooterConfig_cancel4;
	}

	protected HtmlTabbedPanel getTabbedPanelInnerFooterConfig4() {
		if (tabbedPanelInnerFooterConfig4 == null) {
			tabbedPanelInnerFooterConfig4 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelInnerFooterConfig4");
		}
		return tabbedPanelInnerFooterConfig4;
	}

	protected HtmlBfPanel getBfpanelNoFooter4() {
		if (bfpanelNoFooter4 == null) {
			bfpanelNoFooter4 = (HtmlBfPanel) findComponentInRoot("bfpanelNoFooter4");
		}
		return bfpanelNoFooter4;
	}

	protected HtmlOutputText getTextInnerFooterNoImageLabel4() {
		if (textInnerFooterNoImageLabel4 == null) {
			textInnerFooterNoImageLabel4 = (HtmlOutputText) findComponentInRoot("textInnerFooterNoImageLabel4");
		}
		return textInnerFooterNoImageLabel4;
	}

	protected HtmlBfPanel getBfpanelCustomFooter() {
		if (bfpanelCustomFooter == null) {
			bfpanelCustomFooter = (HtmlBfPanel) findComponentInRoot("bfpanelCustomFooter");
		}
		return bfpanelCustomFooter;
	}

	protected HtmlMessage getErrorMsgCustomFooterFileUpload4() {
		if (errorMsgCustomFooterFileUpload4 == null) {
			errorMsgCustomFooterFileUpload4 = (HtmlMessage) findComponentInRoot("errorMsgCustomFooterFileUpload4");
		}
		return errorMsgCustomFooterFileUpload4;
	}

	protected HtmlFormItem getFormItemCustomFooterOnClickURL4() {
		if (formItemCustomFooterOnClickURL4 == null) {
			formItemCustomFooterOnClickURL4 = (HtmlFormItem) findComponentInRoot("formItemCustomFooterOnClickURL4");
		}
		return formItemCustomFooterOnClickURL4;
	}

	protected HtmlFormItem getFormItemCustomFooterImageAltText4() {
		if (formItemCustomFooterImageAltText4 == null) {
			formItemCustomFooterImageAltText4 = (HtmlFormItem) findComponentInRoot("formItemCustomFooterImageAltText4");
		}
		return formItemCustomFooterImageAltText4;
	}

	protected HtmlPanelGrid getGridImage1OptionGrid8() {
		if (gridImage1OptionGrid8 == null) {
			gridImage1OptionGrid8 = (HtmlPanelGrid) findComponentInRoot("gridImage1OptionGrid8");
		}
		return gridImage1OptionGrid8;
	}

	protected HtmlPanelGroup getGroupImage11() {
		if (groupImage11 == null) {
			groupImage11 = (HtmlPanelGroup) findComponentInRoot("groupImage11");
		}
		return groupImage11;
	}

	protected HtmlOutputText getTextImage1Header8() {
		if (textImage1Header8 == null) {
			textImage1Header8 = (HtmlOutputText) findComponentInRoot("textImage1Header8");
		}
		return textImage1Header8;
	}

	protected HtmlSelectOneRadio getRadioImage1OpSelect() {
		if (radioImage1OpSelect == null) {
			radioImage1OpSelect = (HtmlSelectOneRadio) findComponentInRoot("radioImage1OpSelect");
		}
		return radioImage1OpSelect;
	}

	protected HtmlTabbedPanel getTabbedPanelInnerImage1Config4() {
		if (tabbedPanelInnerImage1Config4 == null) {
			tabbedPanelInnerImage1Config4 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelInnerImage1Config4");
		}
		return tabbedPanelInnerImage1Config4;
	}

	protected HtmlBfPanel getBfpanelImage1Current4() {
		if (bfpanelImage1Current4 == null) {
			bfpanelImage1Current4 = (HtmlBfPanel) findComponentInRoot("bfpanelImage1Current4");
		}
		return bfpanelImage1Current4;
	}

	protected HtmlPanelGrid getGridImage1CustomImageDefaultImage4() {
		if (gridImage1CustomImageDefaultImage4 == null) {
			gridImage1CustomImageDefaultImage4 = (HtmlPanelGrid) findComponentInRoot("gridImage1CustomImageDefaultImage4");
		}
		return gridImage1CustomImageDefaultImage4;
	}

	protected HtmlGraphicImageEx getImageImage1() {
		if (imageImage1 == null) {
			imageImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageImage1");
		}
		return imageImage1;
	}

	protected HtmlPanelBox getBoxCurrentImage1DefaultPanelBoxLeft4() {
		if (boxCurrentImage1DefaultPanelBoxLeft4 == null) {
			boxCurrentImage1DefaultPanelBoxLeft4 = (HtmlPanelBox) findComponentInRoot("boxCurrentImage1DefaultPanelBoxLeft4");
		}
		return boxCurrentImage1DefaultPanelBoxLeft4;
	}

	protected HtmlOutputText getTextCustomImage1DefaultLabel4() {
		if (textCustomImage1DefaultLabel4 == null) {
			textCustomImage1DefaultLabel4 = (HtmlOutputText) findComponentInRoot("textCustomImage1DefaultLabel4");
		}
		return textCustomImage1DefaultLabel4;
	}

	protected HtmlBfPanel getBfpanelNoImage1() {
		if (bfpanelNoImage1 == null) {
			bfpanelNoImage1 = (HtmlBfPanel) findComponentInRoot("bfpanelNoImage1");
		}
		return bfpanelNoImage1;
	}

	protected HtmlJspPanel getJspPanelInnerCustomImage1NoImage4() {
		if (jspPanelInnerCustomImage1NoImage4 == null) {
			jspPanelInnerCustomImage1NoImage4 = (HtmlJspPanel) findComponentInRoot("jspPanelInnerCustomImage1NoImage4");
		}
		return jspPanelInnerCustomImage1NoImage4;
	}

	protected HtmlOutputText getTextInnerImage1NoImageLabel4() {
		if (textInnerImage1NoImageLabel4 == null) {
			textInnerImage1NoImageLabel4 = (HtmlOutputText) findComponentInRoot("textInnerImage1NoImageLabel4");
		}
		return textInnerImage1NoImageLabel4;
	}

	protected HtmlBfPanel getBfpanelCustomImage1() {
		if (bfpanelCustomImage1 == null) {
			bfpanelCustomImage1 = (HtmlBfPanel) findComponentInRoot("bfpanelCustomImage1");
		}
		return bfpanelCustomImage1;
	}

	protected HtmlPanelLayout getLayoutInnerCustomImage1ImageLayout4() {
		if (layoutInnerCustomImage1ImageLayout4 == null) {
			layoutInnerCustomImage1ImageLayout4 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomImage1ImageLayout4");
		}
		return layoutInnerCustomImage1ImageLayout4;
	}

	protected HtmlPanelFormBox getFormBoxCustomImage1() {
		if (formBoxCustomImage1 == null) {
			formBoxCustomImage1 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage1");
		}
		return formBoxCustomImage1;
	}

	protected HtmlFormItem getFormItemCustomImage1FormItem4() {
		if (formItemCustomImage1FormItem4 == null) {
			formItemCustomImage1FormItem4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage1FormItem4");
		}
		return formItemCustomImage1FormItem4;
	}

	protected HtmlFileupload getFileuploadCustomImage1Upload4() {
		if (fileuploadCustomImage1Upload4 == null) {
			fileuploadCustomImage1Upload4 = (HtmlFileupload) findComponentInRoot("fileuploadCustomImage1Upload4");
		}
		return fileuploadCustomImage1Upload4;
	}

	protected HtmlMessage getErrorMsgCustomImage1FileUpload4() {
		if (errorMsgCustomImage1FileUpload4 == null) {
			errorMsgCustomImage1FileUpload4 = (HtmlMessage) findComponentInRoot("errorMsgCustomImage1FileUpload4");
		}
		return errorMsgCustomImage1FileUpload4;
	}

	protected HtmlFormItem getFormItemCustomImage1OnClickURL4() {
		if (formItemCustomImage1OnClickURL4 == null) {
			formItemCustomImage1OnClickURL4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage1OnClickURL4");
		}
		return formItemCustomImage1OnClickURL4;
	}

	protected HtmlInputText getTextCustomImage1OnClickURL4() {
		if (textCustomImage1OnClickURL4 == null) {
			textCustomImage1OnClickURL4 = (HtmlInputText) findComponentInRoot("textCustomImage1OnClickURL4");
		}
		return textCustomImage1OnClickURL4;
	}

	protected HtmlFormItem getFormItemCustomImage1AltText4() {
		if (formItemCustomImage1AltText4 == null) {
			formItemCustomImage1AltText4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage1AltText4");
		}
		return formItemCustomImage1AltText4;
	}

	protected HtmlInputText getTextCustomImage1AltText4() {
		if (textCustomImage1AltText4 == null) {
			textCustomImage1AltText4 = (HtmlInputText) findComponentInRoot("textCustomImage1AltText4");
		}
		return textCustomImage1AltText4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage1Config_back4() {
		if (tabbedPanelInnerImage1Config_back4 == null) {
			tabbedPanelInnerImage1Config_back4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage1Config_back4");
		}
		return tabbedPanelInnerImage1Config_back4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage1Config_next4() {
		if (tabbedPanelInnerImage1Config_next4 == null) {
			tabbedPanelInnerImage1Config_next4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage1Config_next4");
		}
		return tabbedPanelInnerImage1Config_next4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage1Config_finish4() {
		if (tabbedPanelInnerImage1Config_finish4 == null) {
			tabbedPanelInnerImage1Config_finish4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage1Config_finish4");
		}
		return tabbedPanelInnerImage1Config_finish4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage1Config_cancel4() {
		if (tabbedPanelInnerImage1Config_cancel4 == null) {
			tabbedPanelInnerImage1Config_cancel4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage1Config_cancel4");
		}
		return tabbedPanelInnerImage1Config_cancel4;
	}

	protected HtmlPanelGroup getGroupImage2() {
		if (groupImage2 == null) {
			groupImage2 = (HtmlPanelGroup) findComponentInRoot("groupImage2");
		}
		return groupImage2;
	}

	protected HtmlPanelGrid getGridImage2OptionGrid8() {
		if (gridImage2OptionGrid8 == null) {
			gridImage2OptionGrid8 = (HtmlPanelGrid) findComponentInRoot("gridImage2OptionGrid8");
		}
		return gridImage2OptionGrid8;
	}

	protected HtmlPanelGroup getGroupImage21() {
		if (groupImage21 == null) {
			groupImage21 = (HtmlPanelGroup) findComponentInRoot("groupImage21");
		}
		return groupImage21;
	}

	protected HtmlOutputText getTextImage2Header8() {
		if (textImage2Header8 == null) {
			textImage2Header8 = (HtmlOutputText) findComponentInRoot("textImage2Header8");
		}
		return textImage2Header8;
	}

	protected HtmlSelectOneRadio getRadioImage2OpSelect() {
		if (radioImage2OpSelect == null) {
			radioImage2OpSelect = (HtmlSelectOneRadio) findComponentInRoot("radioImage2OpSelect");
		}
		return radioImage2OpSelect;
	}

	protected HtmlTabbedPanel getTabbedPanelInnerImage2Config4() {
		if (tabbedPanelInnerImage2Config4 == null) {
			tabbedPanelInnerImage2Config4 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelInnerImage2Config4");
		}
		return tabbedPanelInnerImage2Config4;
	}

	protected HtmlBfPanel getBfpanelImage2Current4() {
		if (bfpanelImage2Current4 == null) {
			bfpanelImage2Current4 = (HtmlBfPanel) findComponentInRoot("bfpanelImage2Current4");
		}
		return bfpanelImage2Current4;
	}

	protected HtmlPanelGrid getGridImage2CustomImageDefaultImage4() {
		if (gridImage2CustomImageDefaultImage4 == null) {
			gridImage2CustomImageDefaultImage4 = (HtmlPanelGrid) findComponentInRoot("gridImage2CustomImageDefaultImage4");
		}
		return gridImage2CustomImageDefaultImage4;
	}

	protected HtmlGraphicImageEx getImageImage2() {
		if (imageImage2 == null) {
			imageImage2 = (HtmlGraphicImageEx) findComponentInRoot("imageImage2");
		}
		return imageImage2;
	}

	protected HtmlPanelBox getBoxCurrentImage2DefaultPanelBoxLeft4() {
		if (boxCurrentImage2DefaultPanelBoxLeft4 == null) {
			boxCurrentImage2DefaultPanelBoxLeft4 = (HtmlPanelBox) findComponentInRoot("boxCurrentImage2DefaultPanelBoxLeft4");
		}
		return boxCurrentImage2DefaultPanelBoxLeft4;
	}

	protected HtmlOutputText getTextCustomImage2DefaultLabel4() {
		if (textCustomImage2DefaultLabel4 == null) {
			textCustomImage2DefaultLabel4 = (HtmlOutputText) findComponentInRoot("textCustomImage2DefaultLabel4");
		}
		return textCustomImage2DefaultLabel4;
	}

	protected HtmlBfPanel getBfpanelNoImage2() {
		if (bfpanelNoImage2 == null) {
			bfpanelNoImage2 = (HtmlBfPanel) findComponentInRoot("bfpanelNoImage2");
		}
		return bfpanelNoImage2;
	}

	protected HtmlJspPanel getJspPanelInnerCustomImage2NoImage4() {
		if (jspPanelInnerCustomImage2NoImage4 == null) {
			jspPanelInnerCustomImage2NoImage4 = (HtmlJspPanel) findComponentInRoot("jspPanelInnerCustomImage2NoImage4");
		}
		return jspPanelInnerCustomImage2NoImage4;
	}

	protected HtmlOutputText getTextInnerImage2NoImageLabel4() {
		if (textInnerImage2NoImageLabel4 == null) {
			textInnerImage2NoImageLabel4 = (HtmlOutputText) findComponentInRoot("textInnerImage2NoImageLabel4");
		}
		return textInnerImage2NoImageLabel4;
	}

	protected HtmlBfPanel getBfpanelCustomImage2() {
		if (bfpanelCustomImage2 == null) {
			bfpanelCustomImage2 = (HtmlBfPanel) findComponentInRoot("bfpanelCustomImage2");
		}
		return bfpanelCustomImage2;
	}

	protected HtmlPanelLayout getLayoutInnerCustomImage2ImageLayout4() {
		if (layoutInnerCustomImage2ImageLayout4 == null) {
			layoutInnerCustomImage2ImageLayout4 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomImage2ImageLayout4");
		}
		return layoutInnerCustomImage2ImageLayout4;
	}

	protected HtmlPanelFormBox getFormBoxCustomImage2() {
		if (formBoxCustomImage2 == null) {
			formBoxCustomImage2 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage2");
		}
		return formBoxCustomImage2;
	}

	protected HtmlFormItem getFormItemCustomImage2FormItem4() {
		if (formItemCustomImage2FormItem4 == null) {
			formItemCustomImage2FormItem4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage2FormItem4");
		}
		return formItemCustomImage2FormItem4;
	}

	protected HtmlFileupload getFileuploadCustomImage2Upload4() {
		if (fileuploadCustomImage2Upload4 == null) {
			fileuploadCustomImage2Upload4 = (HtmlFileupload) findComponentInRoot("fileuploadCustomImage2Upload4");
		}
		return fileuploadCustomImage2Upload4;
	}

	protected HtmlMessage getErrorMsgCustomImage2FileUpload4() {
		if (errorMsgCustomImage2FileUpload4 == null) {
			errorMsgCustomImage2FileUpload4 = (HtmlMessage) findComponentInRoot("errorMsgCustomImage2FileUpload4");
		}
		return errorMsgCustomImage2FileUpload4;
	}

	protected HtmlFormItem getFormItemCustomImage2OnClickURL4() {
		if (formItemCustomImage2OnClickURL4 == null) {
			formItemCustomImage2OnClickURL4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage2OnClickURL4");
		}
		return formItemCustomImage2OnClickURL4;
	}

	protected HtmlInputText getTextCustomImage2OnClickURL4() {
		if (textCustomImage2OnClickURL4 == null) {
			textCustomImage2OnClickURL4 = (HtmlInputText) findComponentInRoot("textCustomImage2OnClickURL4");
		}
		return textCustomImage2OnClickURL4;
	}

	protected HtmlFormItem getFormItemCustomImage2AltText4() {
		if (formItemCustomImage2AltText4 == null) {
			formItemCustomImage2AltText4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage2AltText4");
		}
		return formItemCustomImage2AltText4;
	}

	protected HtmlInputText getTextCustomImage2AltText4() {
		if (textCustomImage2AltText4 == null) {
			textCustomImage2AltText4 = (HtmlInputText) findComponentInRoot("textCustomImage2AltText4");
		}
		return textCustomImage2AltText4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage2Config_back4() {
		if (tabbedPanelInnerImage2Config_back4 == null) {
			tabbedPanelInnerImage2Config_back4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage2Config_back4");
		}
		return tabbedPanelInnerImage2Config_back4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage2Config_next4() {
		if (tabbedPanelInnerImage2Config_next4 == null) {
			tabbedPanelInnerImage2Config_next4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage2Config_next4");
		}
		return tabbedPanelInnerImage2Config_next4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage2Config_finish4() {
		if (tabbedPanelInnerImage2Config_finish4 == null) {
			tabbedPanelInnerImage2Config_finish4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage2Config_finish4");
		}
		return tabbedPanelInnerImage2Config_finish4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage2Config_cancel4() {
		if (tabbedPanelInnerImage2Config_cancel4 == null) {
			tabbedPanelInnerImage2Config_cancel4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage2Config_cancel4");
		}
		return tabbedPanelInnerImage2Config_cancel4;
	}

	protected HtmlPanelGrid getGridImage3OptionGrid8() {
		if (gridImage3OptionGrid8 == null) {
			gridImage3OptionGrid8 = (HtmlPanelGrid) findComponentInRoot("gridImage3OptionGrid8");
		}
		return gridImage3OptionGrid8;
	}

	protected HtmlPanelGroup getGroupImage31() {
		if (groupImage31 == null) {
			groupImage31 = (HtmlPanelGroup) findComponentInRoot("groupImage31");
		}
		return groupImage31;
	}

	protected HtmlOutputText getTextImage3Header8() {
		if (textImage3Header8 == null) {
			textImage3Header8 = (HtmlOutputText) findComponentInRoot("textImage3Header8");
		}
		return textImage3Header8;
	}

	protected HtmlSelectOneRadio getRadioImage3OpSelect() {
		if (radioImage3OpSelect == null) {
			radioImage3OpSelect = (HtmlSelectOneRadio) findComponentInRoot("radioImage3OpSelect");
		}
		return radioImage3OpSelect;
	}

	protected HtmlTabbedPanel getTabbedPanelInnerImage3Config4() {
		if (tabbedPanelInnerImage3Config4 == null) {
			tabbedPanelInnerImage3Config4 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelInnerImage3Config4");
		}
		return tabbedPanelInnerImage3Config4;
	}

	protected HtmlBfPanel getBfpanelImage3Current4() {
		if (bfpanelImage3Current4 == null) {
			bfpanelImage3Current4 = (HtmlBfPanel) findComponentInRoot("bfpanelImage3Current4");
		}
		return bfpanelImage3Current4;
	}

	protected HtmlPanelGrid getGridImage3CustomImageDefaultImage4() {
		if (gridImage3CustomImageDefaultImage4 == null) {
			gridImage3CustomImageDefaultImage4 = (HtmlPanelGrid) findComponentInRoot("gridImage3CustomImageDefaultImage4");
		}
		return gridImage3CustomImageDefaultImage4;
	}

	protected HtmlPanelGroup getGroupImage3() {
		if (groupImage3 == null) {
			groupImage3 = (HtmlPanelGroup) findComponentInRoot("groupImage3");
		}
		return groupImage3;
	}

	protected HtmlGraphicImageEx getImageImage3() {
		if (imageImage3 == null) {
			imageImage3 = (HtmlGraphicImageEx) findComponentInRoot("imageImage3");
		}
		return imageImage3;
	}

	protected HtmlPanelBox getBoxCurrentImage3DefaultPanelBoxLeft4() {
		if (boxCurrentImage3DefaultPanelBoxLeft4 == null) {
			boxCurrentImage3DefaultPanelBoxLeft4 = (HtmlPanelBox) findComponentInRoot("boxCurrentImage3DefaultPanelBoxLeft4");
		}
		return boxCurrentImage3DefaultPanelBoxLeft4;
	}

	protected HtmlOutputText getTextCustomImage3DefaultLabel4() {
		if (textCustomImage3DefaultLabel4 == null) {
			textCustomImage3DefaultLabel4 = (HtmlOutputText) findComponentInRoot("textCustomImage3DefaultLabel4");
		}
		return textCustomImage3DefaultLabel4;
	}

	protected HtmlBfPanel getBfpanelNoImage3() {
		if (bfpanelNoImage3 == null) {
			bfpanelNoImage3 = (HtmlBfPanel) findComponentInRoot("bfpanelNoImage3");
		}
		return bfpanelNoImage3;
	}

	protected HtmlJspPanel getJspPanelInnerCustomImage3NoImage4() {
		if (jspPanelInnerCustomImage3NoImage4 == null) {
			jspPanelInnerCustomImage3NoImage4 = (HtmlJspPanel) findComponentInRoot("jspPanelInnerCustomImage3NoImage4");
		}
		return jspPanelInnerCustomImage3NoImage4;
	}

	protected HtmlOutputText getTextInnerImage3NoImageLabel4() {
		if (textInnerImage3NoImageLabel4 == null) {
			textInnerImage3NoImageLabel4 = (HtmlOutputText) findComponentInRoot("textInnerImage3NoImageLabel4");
		}
		return textInnerImage3NoImageLabel4;
	}

	protected HtmlBfPanel getBfpanelCustomImage3() {
		if (bfpanelCustomImage3 == null) {
			bfpanelCustomImage3 = (HtmlBfPanel) findComponentInRoot("bfpanelCustomImage3");
		}
		return bfpanelCustomImage3;
	}

	protected HtmlPanelLayout getLayoutInnerCustomImage3ImageLayout4() {
		if (layoutInnerCustomImage3ImageLayout4 == null) {
			layoutInnerCustomImage3ImageLayout4 = (HtmlPanelLayout) findComponentInRoot("layoutInnerCustomImage3ImageLayout4");
		}
		return layoutInnerCustomImage3ImageLayout4;
	}

	protected HtmlPanelFormBox getFormBoxCustomImage3() {
		if (formBoxCustomImage3 == null) {
			formBoxCustomImage3 = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage3");
		}
		return formBoxCustomImage3;
	}

	protected HtmlFormItem getFormItemCustomImage3FormItem4() {
		if (formItemCustomImage3FormItem4 == null) {
			formItemCustomImage3FormItem4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage3FormItem4");
		}
		return formItemCustomImage3FormItem4;
	}

	protected HtmlFileupload getFileuploadCustomImage3Upload4() {
		if (fileuploadCustomImage3Upload4 == null) {
			fileuploadCustomImage3Upload4 = (HtmlFileupload) findComponentInRoot("fileuploadCustomImage3Upload4");
		}
		return fileuploadCustomImage3Upload4;
	}

	protected HtmlMessage getErrorMsgCustomImage3FileUpload4() {
		if (errorMsgCustomImage3FileUpload4 == null) {
			errorMsgCustomImage3FileUpload4 = (HtmlMessage) findComponentInRoot("errorMsgCustomImage3FileUpload4");
		}
		return errorMsgCustomImage3FileUpload4;
	}

	protected HtmlFormItem getFormItemCustomImage3OnClickURL4() {
		if (formItemCustomImage3OnClickURL4 == null) {
			formItemCustomImage3OnClickURL4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage3OnClickURL4");
		}
		return formItemCustomImage3OnClickURL4;
	}

	protected HtmlInputText getTextCustomImage3OnClickURL4() {
		if (textCustomImage3OnClickURL4 == null) {
			textCustomImage3OnClickURL4 = (HtmlInputText) findComponentInRoot("textCustomImage3OnClickURL4");
		}
		return textCustomImage3OnClickURL4;
	}

	protected HtmlFormItem getFormItemCustomImage3AltText4() {
		if (formItemCustomImage3AltText4 == null) {
			formItemCustomImage3AltText4 = (HtmlFormItem) findComponentInRoot("formItemCustomImage3AltText4");
		}
		return formItemCustomImage3AltText4;
	}

	protected HtmlInputText getTextCustomImage3AltText4() {
		if (textCustomImage3AltText4 == null) {
			textCustomImage3AltText4 = (HtmlInputText) findComponentInRoot("textCustomImage3AltText4");
		}
		return textCustomImage3AltText4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage3Config_back4() {
		if (tabbedPanelInnerImage3Config_back4 == null) {
			tabbedPanelInnerImage3Config_back4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage3Config_back4");
		}
		return tabbedPanelInnerImage3Config_back4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage3Config_next4() {
		if (tabbedPanelInnerImage3Config_next4 == null) {
			tabbedPanelInnerImage3Config_next4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage3Config_next4");
		}
		return tabbedPanelInnerImage3Config_next4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage3Config_finish4() {
		if (tabbedPanelInnerImage3Config_finish4 == null) {
			tabbedPanelInnerImage3Config_finish4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage3Config_finish4");
		}
		return tabbedPanelInnerImage3Config_finish4;
	}

	protected HtmlCommandExButton getTabbedPanelInnerImage3Config_cancel4() {
		if (tabbedPanelInnerImage3Config_cancel4 == null) {
			tabbedPanelInnerImage3Config_cancel4 = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerImage3Config_cancel4");
		}
		return tabbedPanelInnerImage3Config_cancel4;
	}

	protected HtmlCommandExButton getButtonPublishCampaign() {
		if (buttonPublishCampaign == null) {
			buttonPublishCampaign = (HtmlCommandExButton) findComponentInRoot("buttonPublishCampaign");
		}
		return buttonPublishCampaign;
	}

	protected HtmlCommandExButton getButtonSaveChanges() {
		if (buttonSaveChanges == null) {
			buttonSaveChanges = (HtmlCommandExButton) findComponentInRoot("buttonSaveChanges");
		}
		return buttonSaveChanges;
	}

	public String doButtonSaveChangesAction() {
		try {
			CampaignHandler ch = this.getCurrentDefaultCampaign();
			

			WelcomePageHandler wph = this.getWelcomePageHandler();
			
			String imageOption = wph.getImage1ImageOption();
			ImageHandler newImage  = wph.getImage1Image();
			
			EditableImagePromotionIntf imagePromo =  null;
			
			if (imageOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE) && newImage.getImageContents() != null && newImage.getImageFileName() != null) {
				 
				imagePromo = new EditableImagePromotionBO();
				imagePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_1);
				imagePromo.setImageAltText(newImage.getAlternateText());
				imagePromo.setImageContents(newImage.getImageContents());
				imagePromo.setImageLinkToURL(newImage.getLinkURL());
				imagePromo.setImageFileType(newImage.getImageFileType());
				imagePromo.setImagePathString("/images/marketingimages/" + newImage.getImageFileName_NameOnly());
				
				ch.getCampaign().getPromotionSet().setLandingPagePromoImage1(imagePromo);
			
			}
			else {
				// if no image
				if (imageOption.equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
					// clear any current pop up.
					ch.getCampaign().getPromotionSet().setLandingPagePromoImage1(null);
				}
				// othwerwise no changes so just leave it.
			}

			
			// reset graphics editor
			wph.setImage1ImageOption(CampaignEditsHandler.USE_DEFAULT_IMAGE);

			// image 2
			imageOption = wph.getImage2ImageOption();
			 newImage  = wph.getImage2Image();
			
			imagePromo =  null;
			
			if (imageOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE) && newImage.getImageContents() != null && newImage.getImageFileName() != null) {
				 
				imagePromo = new EditableImagePromotionBO();
				imagePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_2);
				imagePromo.setImageAltText(newImage.getAlternateText());
				imagePromo.setImageContents(newImage.getImageContents());
				imagePromo.setImageLinkToURL(newImage.getLinkURL());
				imagePromo.setImageFileType(newImage.getImageFileType());
				imagePromo.setImagePathString("/images/marketingimages/" + newImage.getImageFileName_NameOnly());
				
				ch.getCampaign().getPromotionSet().setLandingPagePromoImage2(imagePromo);
			
			}
			else {
				// if no image
				if (imageOption.equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
					// clear any current pop up.
					ch.getCampaign().getPromotionSet().setLandingPagePromoImage2(null);
				}
				// othwerwise no changes so just leave it.
			}
			wph.setImage2ImageOption(CampaignEditsHandler.USE_DEFAULT_IMAGE);
			
			// image 3
			imageOption = wph.getImage3ImageOption();
			newImage  = wph.getImage3Image();
			
			imagePromo =  null;
			
			if (imageOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE) && newImage.getImageContents() != null && newImage.getImageFileName() != null) {
				 
				imagePromo = new EditableImagePromotionBO();
				imagePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_3);
				imagePromo.setImageAltText(newImage.getAlternateText());
				imagePromo.setImageContents(newImage.getImageContents());
				imagePromo.setImageLinkToURL(newImage.getLinkURL());
				imagePromo.setImageFileType(newImage.getImageFileType());
				imagePromo.setImagePathString("/images/marketingimages/" + newImage.getImageFileName_NameOnly());
				
				ch.getCampaign().getPromotionSet().setLandingPagePromoImage3(imagePromo);
			
			}
			else {
				// if no image
				if (imageOption.equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
					// clear any current pop up.
					ch.getCampaign().getPromotionSet().setLandingPagePromoImage3(null);
				}
				// othwerwise no changes so just leave it.
			}
			wph.setImage3ImageOption(CampaignEditsHandler.USE_DEFAULT_IMAGE);
			
			// footer image
			imageOption = wph.getFooterImageOption();
			newImage  = wph.getFooterImage();
			
			imagePromo =  null;
			
			if (imageOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE) && newImage.getImageContents() != null && newImage.getImageFileName() != null) {
				 
				imagePromo = new EditableImagePromotionBO();
				imagePromo.setType(PromotionIntf.LANDING_PAGE_PROMO_IMAGE_RIGHT_180x120);
				imagePromo.setImageAltText(newImage.getAlternateText());
				imagePromo.setImageContents(newImage.getImageContents());
				imagePromo.setImageLinkToURL(newImage.getLinkURL());
				imagePromo.setImageFileType(newImage.getImageFileType());
				imagePromo.setImagePathString("/images/marketingimages/" + newImage.getImageFileName_NameOnly());
				
				ch.getCampaign().getPromotionSet().setLandingPageFooterPromoImage(imagePromo);
			
			}
			else {
				// if no image
				if (imageOption.equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
					// clear any current pop up.
					ch.getCampaign().getPromotionSet().setLandingPageFooterPromoImage(null);
				}
				// othwerwise no changes so just leave it.
			}
			wph.setFooterImageOption(CampaignEditsHandler.USE_DEFAULT_IMAGE);
			
			
			ch.getCampaign().save(false);
			
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Campaign Changes Saved." , null);
			getFacesContext().addMessage(null, message);

			FacesMessage message2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Don't forget to publish the campaign if you want the changes visible in production." , null);
			getFacesContext().addMessage(null, message2);
			
			this.getMessages1().setStyle("color: blue;");
		}
		catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to save campaign. " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			this.getMessages1().setStyle("color: red;");
			return "failure";
		}
		return "success";
	}

	protected HtmlPanelGrid getGridFooterCustomImageDefaultImage4() {
		if (gridFooterCustomImageDefaultImage4 == null) {
			gridFooterCustomImageDefaultImage4 = (HtmlPanelGrid) findComponentInRoot("gridFooterCustomImageDefaultImage4");
		}
		return gridFooterCustomImageDefaultImage4;
	}

	protected HtmlPanelGroup getGroupFooter7() {
		if (groupFooter7 == null) {
			groupFooter7 = (HtmlPanelGroup) findComponentInRoot("groupFooter7");
		}
		return groupFooter7;
	}

	protected HtmlGraphicImageEx getImageFooter() {
		if (imageFooter == null) {
			imageFooter = (HtmlGraphicImageEx) findComponentInRoot("imageFooter");
		}
		return imageFooter;
	}

	protected HtmlOutputText getTextCustomFooterImageDefaultLabel4() {
		if (textCustomFooterImageDefaultLabel4 == null) {
			textCustomFooterImageDefaultLabel4 = (HtmlOutputText) findComponentInRoot("textCustomFooterImageDefaultLabel4");
		}
		return textCustomFooterImageDefaultLabel4;
	}

	protected HtmlPanelBox getBoxCurrentFooterDefaultPanelBoxLeft4() {
		if (boxCurrentFooterDefaultPanelBoxLeft4 == null) {
			boxCurrentFooterDefaultPanelBoxLeft4 = (HtmlPanelBox) findComponentInRoot("boxCurrentFooterDefaultPanelBoxLeft4");
		}
		return boxCurrentFooterDefaultPanelBoxLeft4;
	}

	protected HtmlPanelGroup getGroupImage1() {
		if (groupImage1 == null) {
			groupImage1 = (HtmlPanelGroup) findComponentInRoot("groupImage1");
		}
		return groupImage1;
	}

	public String doButtonPublishCampaignAction() {
		// Type Java code that runs when the component is clicked
			String responseString = "success";

		CampaignHandler ch = this.getCurrentDefaultCampaign();
		ch.setCampaignSelected(true);
		
		Collection<CampaignHandler> defaultCampaigns = new ArrayList<CampaignHandler> ();

		defaultCampaigns.add(ch);
		
		this.getPublishingCampaignsHandler().setCollectionData(defaultCampaigns);
		
		return responseString;	

	}

	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getManagedBean("publishingCampaignsHandler");
		}
		return publishingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected WelcomePageHandler getWelcomePageHandler() {
		if (welcomePageHandler == null) {
			welcomePageHandler = (WelcomePageHandler) getManagedBean("welcomePageHandler");
		}
		return welcomePageHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setWelcomePageHandler(WelcomePageHandler welcomePageHandler) {
		this.welcomePageHandler = welcomePageHandler;
	}

}