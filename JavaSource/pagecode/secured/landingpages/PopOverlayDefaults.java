/**
 * 
 */
package pagecode.secured.landingpages;

import java.util.ArrayList;
import java.util.Collection;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlFileupload;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlMessage;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.bf.component.html.HtmlButtonPanel;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

import javax.faces.component.html.HtmlSelectOneRadio;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CoverGraphicsHandler;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableImagePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import javax.faces.context.FacesContext;

/**
 * @author aeast
 *
 */
public class PopOverlayDefaults extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlMessages messages1;
	protected HtmlPanelGrid gridSubscribePathOverlayImageOptionGrid;
	protected HtmlPanelGroup group1;
	protected HtmlOutputText textUpSellImageHeader;
	protected HtmlPanelLayout layoutInnerUTCustomSubscribePathOverlayImageImageLayout;
	protected HtmlPanelFormBox formBoxCustomImage;
	protected HtmlFormItem formItemCustomUTSubscribeOverlayImageFormItem;
	protected HtmlFileupload fileuploadUTImageUpload;
	protected HtmlInputText textUTSubscribePathOverlayOnClickURL;
	protected HtmlInputText textUTSubscribePathOverlayImageAltText;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_back;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_next;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_finish;
	protected HtmlCommandExButton tabbedPanelInnerUpSellConfig_cancel;
	protected HtmlPanelGrid gridSWImageOptionGrid2;
	protected HtmlCommandExButton tabbedPanel1_back;
	protected HtmlCommandExButton tabbedPanel1_next;
	protected HtmlCommandExButton tabbedPanel1_finish;
	protected HtmlCommandExButton tabbedPanel1_cancel;
	protected HtmlCommandExButton buttonPublishChanges;
	protected HtmlPanelGroup group21;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid gridCurrentSettings;
	protected HtmlPanelGroup grou94;
	protected HtmlOutputText textCurrentSettingLabel;
	protected HtmlPanelGrid grid7;
	protected HtmlMessage errorMsgCustomFileUpload;
	protected HtmlFormItem formItemSubscribePathOverlayOnClickURL;
	protected HtmlFormItem formItemCustomUTSubscribePathOverlayImageAltText;
	protected HtmlCommandExButton buttonSaveChanges;
	protected HtmlPanelBox box1;
	protected HtmlOutputText textCurrentUTOverlayImageLabel;
	protected HtmlGraphicImageEx imageUtDefaultRightPanel;
	protected HtmlOutputText textCurrentNavImageLabel;
	protected HtmlOutputText text2;
	protected HtmlScriptCollector scriptCollectorJSPC02blue;
	protected HtmlJspPanel jspPanelLeftNavCheckProdPanel;
	protected HtmlOutputLinkEx linkExLeftNavToClearProdCache;
	protected HtmlOutputText textLeftNavClearCacheLabel;
	protected HtmlOutputLinkEx linkExLeftNav1;
	protected HtmlOutputText textLeftNav1;
	protected HtmlOutputLinkEx linkExLeftNav2;
	protected HtmlOutputText textLeftNav2;
	protected HtmlOutputLinkEx linkExLeftNavToOmniture;
	protected HtmlOutputText textLeftNav4;
	protected HtmlJspPanel jspPanelLeftNav5;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdCol;
	protected HtmlJspPanel jspPanelLeftNav4;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdExp;
	protected HtmlTabbedPanel tabbedPanel1;
	protected HtmlBfPanel bfpanel1;
	protected HtmlBfPanel bfpanelCustomUTSubscribeOverlay;
	protected HtmlPanelSection sectionLeftNavCheckProdSection;
	protected HtmlOutputText textLeftNav11;
	protected HtmlOutputText textLeftNav10;
	protected HtmlTabbedPanel tabbedPanelUTInnerPopOverlayConfig;
	protected HtmlBfPanel bfpanel2;
	protected HtmlButtonPanel bp1;
	protected HtmlSelectOneRadio radioUTOverlayImage;
	protected CampaignHandler utLandingPages;
	protected CoverGraphicsHandler coverGraphicsHandler;
	protected HtmlCommandExButton buttonPublishCampaigns;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected HtmlBfPanel bfpanelUTNoImage;
	protected HtmlJspPanel jspPanelInnerHeaderNoImage;
	protected HtmlOutputText textInnerHeaderNoImageLabel;
	protected HtmlOutputText textNoUTOverlayImage;
	protected HtmlOutputText textSysDescriptionHeader;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlPanelGrid getGridSubscribePathOverlayImageOptionGrid() {
		if (gridSubscribePathOverlayImageOptionGrid == null) {
			gridSubscribePathOverlayImageOptionGrid = (HtmlPanelGrid) findComponentInRoot("gridSubscribePathOverlayImageOptionGrid");
		}
		return gridSubscribePathOverlayImageOptionGrid;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlOutputText getTextUpSellImageHeader() {
		if (textUpSellImageHeader == null) {
			textUpSellImageHeader = (HtmlOutputText) findComponentInRoot("textUpSellImageHeader");
		}
		return textUpSellImageHeader;
	}

	protected HtmlPanelLayout getLayoutInnerUTCustomSubscribePathOverlayImageImageLayout() {
		if (layoutInnerUTCustomSubscribePathOverlayImageImageLayout == null) {
			layoutInnerUTCustomSubscribePathOverlayImageImageLayout = (HtmlPanelLayout) findComponentInRoot("layoutInnerUTCustomSubscribePathOverlayImageImageLayout");
		}
		return layoutInnerUTCustomSubscribePathOverlayImageImageLayout;
	}

	protected HtmlPanelFormBox getFormBoxCustomImage() {
		if (formBoxCustomImage == null) {
			formBoxCustomImage = (HtmlPanelFormBox) findComponentInRoot("formBoxCustomImage");
		}
		return formBoxCustomImage;
	}

	protected HtmlFormItem getFormItemCustomUTSubscribeOverlayImageFormItem() {
		if (formItemCustomUTSubscribeOverlayImageFormItem == null) {
			formItemCustomUTSubscribeOverlayImageFormItem = (HtmlFormItem) findComponentInRoot("formItemCustomUTSubscribeOverlayImageFormItem");
		}
		return formItemCustomUTSubscribeOverlayImageFormItem;
	}

	protected HtmlFileupload getFileuploadUTImageUpload() {
		if (fileuploadUTImageUpload == null) {
			fileuploadUTImageUpload = (HtmlFileupload) findComponentInRoot("fileuploadUTImageUpload");
		}
		return fileuploadUTImageUpload;
	}

	protected HtmlInputText getTextUTSubscribePathOverlayOnClickURL() {
		if (textUTSubscribePathOverlayOnClickURL == null) {
			textUTSubscribePathOverlayOnClickURL = (HtmlInputText) findComponentInRoot("textUTSubscribePathOverlayOnClickURL");
		}
		return textUTSubscribePathOverlayOnClickURL;
	}

	protected HtmlInputText getTextUTSubscribePathOverlayImageAltText() {
		if (textUTSubscribePathOverlayImageAltText == null) {
			textUTSubscribePathOverlayImageAltText = (HtmlInputText) findComponentInRoot("textUTSubscribePathOverlayImageAltText");
		}
		return textUTSubscribePathOverlayImageAltText;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_back() {
		if (tabbedPanelInnerUpSellConfig_back == null) {
			tabbedPanelInnerUpSellConfig_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_back");
		}
		return tabbedPanelInnerUpSellConfig_back;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_next() {
		if (tabbedPanelInnerUpSellConfig_next == null) {
			tabbedPanelInnerUpSellConfig_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_next");
		}
		return tabbedPanelInnerUpSellConfig_next;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_finish() {
		if (tabbedPanelInnerUpSellConfig_finish == null) {
			tabbedPanelInnerUpSellConfig_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_finish");
		}
		return tabbedPanelInnerUpSellConfig_finish;
	}

	protected HtmlCommandExButton getTabbedPanelInnerUpSellConfig_cancel() {
		if (tabbedPanelInnerUpSellConfig_cancel == null) {
			tabbedPanelInnerUpSellConfig_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerUpSellConfig_cancel");
		}
		return tabbedPanelInnerUpSellConfig_cancel;
	}

	protected HtmlPanelGrid getGridSWImageOptionGrid2() {
		if (gridSWImageOptionGrid2 == null) {
			gridSWImageOptionGrid2 = (HtmlPanelGrid) findComponentInRoot("gridSWImageOptionGrid2");
		}
		return gridSWImageOptionGrid2;
	}

	protected HtmlCommandExButton getTabbedPanel1_back() {
		if (tabbedPanel1_back == null) {
			tabbedPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_back");
		}
		return tabbedPanel1_back;
	}

	protected HtmlCommandExButton getTabbedPanel1_next() {
		if (tabbedPanel1_next == null) {
			tabbedPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_next");
		}
		return tabbedPanel1_next;
	}

	protected HtmlCommandExButton getTabbedPanel1_finish() {
		if (tabbedPanel1_finish == null) {
			tabbedPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_finish");
		}
		return tabbedPanel1_finish;
	}

	protected HtmlCommandExButton getTabbedPanel1_cancel() {
		if (tabbedPanel1_cancel == null) {
			tabbedPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_cancel");
		}
		return tabbedPanel1_cancel;
	}

	protected HtmlCommandExButton getButtonPublishChanges() {
		if (buttonPublishChanges == null) {
			buttonPublishChanges = (HtmlCommandExButton) findComponentInRoot("buttonPublishChanges");
		}
		return buttonPublishChanges;
	}

	protected HtmlPanelGroup getGroup21() {
		if (group21 == null) {
			group21 = (HtmlPanelGroup) findComponentInRoot("group21");
		}
		return group21;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getGridCurrentSettings() {
		if (gridCurrentSettings == null) {
			gridCurrentSettings = (HtmlPanelGrid) findComponentInRoot("gridCurrentSettings");
		}
		return gridCurrentSettings;
	}

	protected HtmlPanelGroup getGrou94() {
		if (grou94 == null) {
			grou94 = (HtmlPanelGroup) findComponentInRoot("grou94");
		}
		return grou94;
	}

	protected HtmlOutputText getTextCurrentSettingLabel() {
		if (textCurrentSettingLabel == null) {
			textCurrentSettingLabel = (HtmlOutputText) findComponentInRoot("textCurrentSettingLabel");
		}
		return textCurrentSettingLabel;
	}

	protected HtmlPanelGrid getGrid7() {
		if (grid7 == null) {
			grid7 = (HtmlPanelGrid) findComponentInRoot("grid7");
		}
		return grid7;
	}

	protected HtmlMessage getErrorMsgCustomFileUpload() {
		if (errorMsgCustomFileUpload == null) {
			errorMsgCustomFileUpload = (HtmlMessage) findComponentInRoot("errorMsgCustomFileUpload");
		}
		return errorMsgCustomFileUpload;
	}

	protected HtmlFormItem getFormItemSubscribePathOverlayOnClickURL() {
		if (formItemSubscribePathOverlayOnClickURL == null) {
			formItemSubscribePathOverlayOnClickURL = (HtmlFormItem) findComponentInRoot("formItemSubscribePathOverlayOnClickURL");
		}
		return formItemSubscribePathOverlayOnClickURL;
	}

	protected HtmlFormItem getFormItemCustomUTSubscribePathOverlayImageAltText() {
		if (formItemCustomUTSubscribePathOverlayImageAltText == null) {
			formItemCustomUTSubscribePathOverlayImageAltText = (HtmlFormItem) findComponentInRoot("formItemCustomUTSubscribePathOverlayImageAltText");
		}
		return formItemCustomUTSubscribePathOverlayImageAltText;
	}

	protected HtmlCommandExButton getButtonSaveChanges() {
		if (buttonSaveChanges == null) {
			buttonSaveChanges = (HtmlCommandExButton) findComponentInRoot("buttonSaveChanges");
		}
		return buttonSaveChanges;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlOutputText getTextCurrentUTOverlayImageLabel() {
		if (textCurrentUTOverlayImageLabel == null) {
			textCurrentUTOverlayImageLabel = (HtmlOutputText) findComponentInRoot("textCurrentUTOverlayImageLabel");
		}
		return textCurrentUTOverlayImageLabel;
	}

	protected HtmlGraphicImageEx getImageUtDefaultRightPanel() {
		if (imageUtDefaultRightPanel == null) {
			imageUtDefaultRightPanel = (HtmlGraphicImageEx) findComponentInRoot("imageUtDefaultRightPanel");
		}
		return imageUtDefaultRightPanel;
	}

	protected HtmlOutputText getTextCurrentNavImageLabel() {
		if (textCurrentNavImageLabel == null) {
			textCurrentNavImageLabel = (HtmlOutputText) findComponentInRoot("textCurrentNavImageLabel");
		}
		return textCurrentNavImageLabel;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlScriptCollector getScriptCollectorJSPC02blue() {
		if (scriptCollectorJSPC02blue == null) {
			scriptCollectorJSPC02blue = (HtmlScriptCollector) findComponentInRoot("scriptCollectorJSPC02blue");
		}
		return scriptCollectorJSPC02blue;
	}

	protected HtmlJspPanel getJspPanelLeftNavCheckProdPanel() {
		if (jspPanelLeftNavCheckProdPanel == null) {
			jspPanelLeftNavCheckProdPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNavCheckProdPanel");
		}
		return jspPanelLeftNavCheckProdPanel;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToClearProdCache() {
		if (linkExLeftNavToClearProdCache == null) {
			linkExLeftNavToClearProdCache = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToClearProdCache");
		}
		return linkExLeftNavToClearProdCache;
	}

	protected HtmlOutputText getTextLeftNavClearCacheLabel() {
		if (textLeftNavClearCacheLabel == null) {
			textLeftNavClearCacheLabel = (HtmlOutputText) findComponentInRoot("textLeftNavClearCacheLabel");
		}
		return textLeftNavClearCacheLabel;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav1() {
		if (linkExLeftNav1 == null) {
			linkExLeftNav1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav1");
		}
		return linkExLeftNav1;
	}

	protected HtmlOutputText getTextLeftNav1() {
		if (textLeftNav1 == null) {
			textLeftNav1 = (HtmlOutputText) findComponentInRoot("textLeftNav1");
		}
		return textLeftNav1;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav2() {
		if (linkExLeftNav2 == null) {
			linkExLeftNav2 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav2");
		}
		return linkExLeftNav2;
	}

	protected HtmlOutputText getTextLeftNav2() {
		if (textLeftNav2 == null) {
			textLeftNav2 = (HtmlOutputText) findComponentInRoot("textLeftNav2");
		}
		return textLeftNav2;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToOmniture() {
		if (linkExLeftNavToOmniture == null) {
			linkExLeftNavToOmniture = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToOmniture");
		}
		return linkExLeftNavToOmniture;
	}

	protected HtmlOutputText getTextLeftNav4() {
		if (textLeftNav4 == null) {
			textLeftNav4 = (HtmlOutputText) findComponentInRoot("textLeftNav4");
		}
		return textLeftNav4;
	}

	protected HtmlJspPanel getJspPanelLeftNav5() {
		if (jspPanelLeftNav5 == null) {
			jspPanelLeftNav5 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav5");
		}
		return jspPanelLeftNav5;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdCol() {
		if (imageExLeftNavCheckProdCol == null) {
			imageExLeftNavCheckProdCol = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdCol");
		}
		return imageExLeftNavCheckProdCol;
	}

	protected HtmlJspPanel getJspPanelLeftNav4() {
		if (jspPanelLeftNav4 == null) {
			jspPanelLeftNav4 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav4");
		}
		return jspPanelLeftNav4;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdExp() {
		if (imageExLeftNavCheckProdExp == null) {
			imageExLeftNavCheckProdExp = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdExp");
		}
		return imageExLeftNavCheckProdExp;
	}

	protected HtmlTabbedPanel getTabbedPanel1() {
		if (tabbedPanel1 == null) {
			tabbedPanel1 = (HtmlTabbedPanel) findComponentInRoot("tabbedPanel1");
		}
		return tabbedPanel1;
	}

	protected HtmlBfPanel getBfpanel1() {
		if (bfpanel1 == null) {
			bfpanel1 = (HtmlBfPanel) findComponentInRoot("bfpanel1");
		}
		return bfpanel1;
	}

	protected HtmlBfPanel getBfpanelCustomUTSubscribeOverlay() {
		if (bfpanelCustomUTSubscribeOverlay == null) {
			bfpanelCustomUTSubscribeOverlay = (HtmlBfPanel) findComponentInRoot("bfpanelCustomUTSubscribeOverlay");
		}
		return bfpanelCustomUTSubscribeOverlay;
	}

	protected HtmlPanelSection getSectionLeftNavCheckProdSection() {
		if (sectionLeftNavCheckProdSection == null) {
			sectionLeftNavCheckProdSection = (HtmlPanelSection) findComponentInRoot("sectionLeftNavCheckProdSection");
		}
		return sectionLeftNavCheckProdSection;
	}

	protected HtmlOutputText getTextLeftNav11() {
		if (textLeftNav11 == null) {
			textLeftNav11 = (HtmlOutputText) findComponentInRoot("textLeftNav11");
		}
		return textLeftNav11;
	}

	protected HtmlOutputText getTextLeftNav10() {
		if (textLeftNav10 == null) {
			textLeftNav10 = (HtmlOutputText) findComponentInRoot("textLeftNav10");
		}
		return textLeftNav10;
	}

	protected HtmlTabbedPanel getTabbedPanelUTInnerPopOverlayConfig() {
		if (tabbedPanelUTInnerPopOverlayConfig == null) {
			tabbedPanelUTInnerPopOverlayConfig = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelUTInnerPopOverlayConfig");
		}
		return tabbedPanelUTInnerPopOverlayConfig;
	}

	protected HtmlBfPanel getBfpanel2() {
		if (bfpanel2 == null) {
			bfpanel2 = (HtmlBfPanel) findComponentInRoot("bfpanel2");
		}
		return bfpanel2;
	}

	protected HtmlButtonPanel getBp1() {
		if (bp1 == null) {
			bp1 = (HtmlButtonPanel) findComponentInRoot("bp1");
		}
		return bp1;
	}

	protected HtmlSelectOneRadio getRadioUTOverlayImage() {
		if (radioUTOverlayImage == null) {
			radioUTOverlayImage = (HtmlSelectOneRadio) findComponentInRoot("radioUTOverlayImage");
		}
		return radioUTOverlayImage;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getUtLandingPages() {
		if (utLandingPages == null) {
			utLandingPages = (CampaignHandler) getManagedBean("utLandingPages");
		}
		return utLandingPages;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUtLandingPages(CampaignHandler utLandingPages) {
		this.utLandingPages = utLandingPages;
	}

	public String doButtonSaveChangesAction() {
		// Type Java code that runs when the component is clicked
		boolean hasErrors = false;	
	//	CampaignHandler swcampaign = this.getSwLandingPages();
		CampaignHandler utcampaign = this.getUtLandingPages();
		
		CoverGraphicsHandler graphicsEditor = this.getCoverGraphicsHandler();
							
		//ImageHandler newSwOverlayImage = graphicsEditor.getSwCoverImage();
		String utOverlayImageOption = graphicsEditor.getUtSubscribePathPopUpOverlayImageOption();
		ImageHandler newUtCoverImage  = graphicsEditor.getUTSubscribePathOverlayGraphicImage();
		
		try {		
		
			// set up new images  
			//EditableImagePromotionIntf swCoverPromo =  null;
			EditableImagePromotionIntf utCoverPromo =  null;
			
			
			//set Ut
			 if (utOverlayImageOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE) && newUtCoverImage.getImageContents() != null && newUtCoverImage.getImageFileName() != null) {
				utCoverPromo = new EditableImagePromotionBO();
				utCoverPromo.setType(PromotionIntf.ORDER_PATH_POP_OVERLAY);
				utCoverPromo.setImageAltText(newUtCoverImage.getAlternateText());
				utCoverPromo.setImageContents(newUtCoverImage.getImageContents());
				utCoverPromo.setImageLinkToURL(newUtCoverImage.getLinkURL());
				utCoverPromo.setImageFileType(newUtCoverImage.getImageFileType());
				utCoverPromo.setImagePathString("/images/marketingimages/" + newUtCoverImage.getImageFileName_NameOnly());
				
				utcampaign.getCampaign().getPromotionSet().setNewSubscriptionOrderPopUpOverlay(utCoverPromo);
				utcampaign.getCampaign().save(false);			
			
			}
			else {
				 // clear any current pop up.
				utcampaign.getCampaign().getPromotionSet().clearNewOrderPopOverlayConfigurations();
				utcampaign.getCampaign().save(false);
				this.getUtLandingPages().getCampaign().getPromotionSet().clearNewOrderPopOverlayConfigurations();
			}
		
			hasErrors = false;
			
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "FAILED TO set Subscribe Path PopUp Overlay Graphics. Contact IT for support.", e.getMessage());
			getMessages1().setErrorStyle("color:Red");
			getMessages1().setShowDetail(true);
			getFacesContext().addMessage(null, message);
			hasErrors = true;
		}
			
		if (hasErrors) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "FAILED TO set Subscribe Path PopUp Overlay Graphics Values", "");
			getFacesContext().addMessage(null, message);
			return "failure";
		}
		else {	
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Subscribe Path PopUp Overlay Graphics successfully saved", "");
			getFacesContext().addMessage(null, message);
			return "success";
		}
		
	}

	/** 
	 * @managed-bean true
	 */
	protected CoverGraphicsHandler getCoverGraphicsHandler() {
		if (coverGraphicsHandler == null) {
			coverGraphicsHandler = (CoverGraphicsHandler) getManagedBean("coverGraphicsHandler");
		}
		return coverGraphicsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCoverGraphicsHandler(
			CoverGraphicsHandler coverGraphicsHandler) {
		this.coverGraphicsHandler = coverGraphicsHandler;
	}

	protected HtmlCommandExButton getButtonPublishCampaigns() {
		if (buttonPublishCampaigns == null) {
			buttonPublishCampaigns = (HtmlCommandExButton) findComponentInRoot("buttonPublishCampaigns");
		}
		return buttonPublishCampaigns;
	}

	public String doButtonPublishCampaignsAction() {
		// Type Java code that runs when the component is clicked
		String responseString = "success";
		//CampaignHandler swcampaign = this.getSwLandingPages();
		CampaignHandler utcampaign = this.getUtLandingPages();
		
		
		Collection<CampaignHandler> defaultCampaigns = new ArrayList<CampaignHandler> ();

		//defaultCampaigns.add(swcampaign);
		defaultCampaigns.add(utcampaign);
		
		
		// set up collection for publishing
		Collection<CampaignHandler> campaigns = defaultCampaigns;
		for(CampaignHandler ch : campaigns) {
				ch.setCampaignSelected(true);
		}

		this.getPublishingCampaignsHandler().setCollectionData(defaultCampaigns);
		
		return responseString;
	}

	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getManagedBean("publishingCampaignsHandler");
		}
		return publishingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}

	protected HtmlBfPanel getBfpanelUTNoImage() {
		if (bfpanelUTNoImage == null) {
			bfpanelUTNoImage = (HtmlBfPanel) findComponentInRoot("bfpanelUTNoImage");
		}
		return bfpanelUTNoImage;
	}

	protected HtmlJspPanel getJspPanelInnerHeaderNoImage() {
		if (jspPanelInnerHeaderNoImage == null) {
			jspPanelInnerHeaderNoImage = (HtmlJspPanel) findComponentInRoot("jspPanelInnerHeaderNoImage");
		}
		return jspPanelInnerHeaderNoImage;
	}

	protected HtmlOutputText getTextInnerHeaderNoImageLabel() {
		if (textInnerHeaderNoImageLabel == null) {
			textInnerHeaderNoImageLabel = (HtmlOutputText) findComponentInRoot("textInnerHeaderNoImageLabel");
		}
		return textInnerHeaderNoImageLabel;
	}

	protected HtmlOutputText getTextNoUTOverlayImage() {
		if (textNoUTOverlayImage == null) {
			textNoUTOverlayImage = (HtmlOutputText) findComponentInRoot("textNoUTOverlayImage");
		}
		return textNoUTOverlayImage;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	
		// void <method>(FacesContext facescontext)
		//CampaignHandler utCamp = this.getUtLandingPages();
		
		//CoverGraphicsHandler ch = this.getCoverGraphicsHandler();
		
		//EditableImagePromotionIntf ei = utCamp.getCampaign().getPromotionSet().getNewSubscriptionOrderPopUpOverlay(); 
		//if (ei != null ) {
		//	if (!ei.isShimImage()) {
		//		ch.setUtSubscribePathPopUpOverlayImageOption(CampaignEditsHandler.USE_CUSTOM_IMAGE);
		//	}
		//}
	}

	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}


}