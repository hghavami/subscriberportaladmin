/*
 * Created on Aug 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode.secured.campaigns;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class BasicSearch extends PageCodeBase {

	protected UINamingContainer subviewBasicSearch;
	protected HtmlScriptCollector scriptCollectorBasicSearchForm;
	protected HtmlForm formBasicSearchForm;
	protected HtmlInputText textBasicSearchCriteria;
	protected HtmlMessage message1;
	protected HtmlOutputText textAdvSearchLabel;
	protected HtmlOutputLinkEx linkExAdvSearchLink;
	protected HtmlCommandExButton buttonBasicSearch;
	protected HtmlOutputText textBasicSearchLabel;
	protected UINamingContainer getSubviewBasicSearch() {
		if (subviewBasicSearch == null) {
			subviewBasicSearch = (UINamingContainer) findComponentInRoot("subviewBasicSearch");
		}
		return subviewBasicSearch;
	}
	protected HtmlScriptCollector getScriptCollectorBasicSearchForm() {
		if (scriptCollectorBasicSearchForm == null) {
			scriptCollectorBasicSearchForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorBasicSearchForm");
		}
		return scriptCollectorBasicSearchForm;
	}
	protected HtmlForm getFormBasicSearchForm() {
		if (formBasicSearchForm == null) {
			formBasicSearchForm = (HtmlForm) findComponentInRoot("formBasicSearchForm");
		}
		return formBasicSearchForm;
	}
	protected HtmlInputText getTextBasicSearchCriteria() {
		if (textBasicSearchCriteria == null) {
			textBasicSearchCriteria = (HtmlInputText) findComponentInRoot("textBasicSearchCriteria");
		}
		return textBasicSearchCriteria;
	}
	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}
	protected HtmlOutputText getTextAdvSearchLabel() {
		if (textAdvSearchLabel == null) {
			textAdvSearchLabel = (HtmlOutputText) findComponentInRoot("textAdvSearchLabel");
		}
		return textAdvSearchLabel;
	}
	protected HtmlOutputLinkEx getLinkExAdvSearchLink() {
		if (linkExAdvSearchLink == null) {
			linkExAdvSearchLink = (HtmlOutputLinkEx) findComponentInRoot("linkExAdvSearchLink");
		}
		return linkExAdvSearchLink;
	}
	protected HtmlCommandExButton getButtonBasicSearch() {
		if (buttonBasicSearch == null) {
			buttonBasicSearch = (HtmlCommandExButton) findComponentInRoot("buttonBasicSearch");
		}
		return buttonBasicSearch;
	}
	protected HtmlOutputText getTextBasicSearchLabel() {
		if (textBasicSearchLabel == null) {
			textBasicSearchLabel = (HtmlOutputText) findComponentInRoot("textBasicSearchLabel");
		}
		return textBasicSearchLabel;
	}
}