/**
 * 
 */
package pagecode.secured.campaigns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignPublisherFailure;
import com.gannett.usatoday.adminportal.campaigns.utils.UsatCampaignPublisher;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlOutputSelecticons;
import com.ibm.faces.component.html.HtmlPagerWeb;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlSortHeader;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.UISortHeader;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlProgressBar;

/**
 * @author aeast
 *
 */
public class Publish_campaigns extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorPublishingCollector;
	protected HtmlPanelSection sectionSelectedCampaignsForPublishing;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageExPublishingCCollapsed;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageExPublischCOpen;
	protected HtmlOutputText textCollapsedheaderText;
	protected HtmlOutputText textexpandedTextHeaderLabel;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected HtmlPanelBox boxHeaderBox;
	protected HtmlOutputSelecticons selecticons1;
	protected HtmlOutputText textNumRowsLabel;
	protected HtmlPanelBox boxFooterBox;
	protected HtmlOutputSelecticons selecticonsRowSelectionFooter;
	protected HtmlPagerWeb webBottomPager;
	protected HtmlInputRowSelect rowSelectSearchResults;
	protected HtmlOutputText text5;
	protected HtmlOutputText textCampGroupHeaderLabel;
	protected HtmlOutputText text6;
	protected HtmlOutputText textKeycodeHeaderLabel;
	protected HtmlOutputText text9;
	protected HtmlOutputText textProdVanityLinkText;
	protected HtmlOutputText text10;
	protected HtmlOutputText text11;
	protected HtmlDataTableEx tableExSearchResultsDataTable;
	protected HtmlPanelGrid gridNumRowsToShow;
	protected HtmlInputText textRowsPerPageValue;
	protected HtmlCommandExButton buttonUpdateRowToShow;
	protected HtmlPanelGrid gridBottomPagerGrid;
	protected UIColumnEx columnEx3;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText textCampaignName;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText textCampaignGroup;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textCampaignPub;
	protected UIColumnEx columnExKeycodeCol;
	protected HtmlOutputText textCampaignKeycode;
	protected UIColumnEx columnEx5;
	protected HtmlOutputLinkEx linkExVanityLink;
	protected UIColumnEx columnEx7;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText textCampaignStatus;
	protected HtmlOutputText textTestVanityLabel;
	protected HtmlOutputLinkEx linkExTestServerLink;
	protected HtmlForm formPublishingForm;
	protected HtmlCommandExButton button1;
	protected HtmlOutputText text1;
	protected UIColumnEx columnExLastErrMsg;
	protected HtmlOutputText textLastPublishMessage;
	protected HtmlMessages messages1;
	protected HtmlOutputLinkEx linkExBackToSearchPage;
	protected HtmlCommandExButton buttonPublishToTest;
	protected HtmlCommandExButton buttonPublishToProd;
	protected HtmlSortHeader sortHeaderCampaignName;
	protected HtmlSortHeader sortHeaderCampaignGroup;
	protected HtmlSortHeader sortHeaderKeycode;
	protected HtmlGraphicImageEx imageExSearchIcon;
	protected HtmlProgressBar barPubStatusBar;
	protected HtmlScriptCollector getScriptCollectorPublishingCollector() {
		if (scriptCollectorPublishingCollector == null) {
			scriptCollectorPublishingCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorPublishingCollector");
		}
		return scriptCollectorPublishingCollector;
	}

	protected HtmlPanelSection getSectionSelectedCampaignsForPublishing() {
		if (sectionSelectedCampaignsForPublishing == null) {
			sectionSelectedCampaignsForPublishing = (HtmlPanelSection) findComponentInRoot("sectionSelectedCampaignsForPublishing");
		}
		return sectionSelectedCampaignsForPublishing;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlGraphicImageEx getImageExPublishingCCollapsed() {
		if (imageExPublishingCCollapsed == null) {
			imageExPublishingCCollapsed = (HtmlGraphicImageEx) findComponentInRoot("imageExPublishingCCollapsed");
		}
		return imageExPublishingCCollapsed;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlGraphicImageEx getImageExPublischCOpen() {
		if (imageExPublischCOpen == null) {
			imageExPublischCOpen = (HtmlGraphicImageEx) findComponentInRoot("imageExPublischCOpen");
		}
		return imageExPublischCOpen;
	}

	protected HtmlOutputText getTextCollapsedheaderText() {
		if (textCollapsedheaderText == null) {
			textCollapsedheaderText = (HtmlOutputText) findComponentInRoot("textCollapsedheaderText");
		}
		return textCollapsedheaderText;
	}

	protected HtmlOutputText getTextexpandedTextHeaderLabel() {
		if (textexpandedTextHeaderLabel == null) {
			textexpandedTextHeaderLabel = (HtmlOutputText) findComponentInRoot("textexpandedTextHeaderLabel");
		}
		return textexpandedTextHeaderLabel;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{publishingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return publishingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}

	protected HtmlPanelBox getBoxHeaderBox() {
		if (boxHeaderBox == null) {
			boxHeaderBox = (HtmlPanelBox) findComponentInRoot("boxHeaderBox");
		}
		return boxHeaderBox;
	}

	protected HtmlOutputSelecticons getSelecticons1() {
		if (selecticons1 == null) {
			selecticons1 = (HtmlOutputSelecticons) findComponentInRoot("selecticons1");
		}
		return selecticons1;
	}

	protected HtmlOutputText getTextNumRowsLabel() {
		if (textNumRowsLabel == null) {
			textNumRowsLabel = (HtmlOutputText) findComponentInRoot("textNumRowsLabel");
		}
		return textNumRowsLabel;
	}

	protected HtmlPanelBox getBoxFooterBox() {
		if (boxFooterBox == null) {
			boxFooterBox = (HtmlPanelBox) findComponentInRoot("boxFooterBox");
		}
		return boxFooterBox;
	}

	protected HtmlOutputSelecticons getSelecticonsRowSelectionFooter() {
		if (selecticonsRowSelectionFooter == null) {
			selecticonsRowSelectionFooter = (HtmlOutputSelecticons) findComponentInRoot("selecticonsRowSelectionFooter");
		}
		return selecticonsRowSelectionFooter;
	}

	protected HtmlPagerWeb getWebBottomPager() {
		if (webBottomPager == null) {
			webBottomPager = (HtmlPagerWeb) findComponentInRoot("webBottomPager");
		}
		return webBottomPager;
	}

	protected HtmlInputRowSelect getRowSelectSearchResults() {
		if (rowSelectSearchResults == null) {
			rowSelectSearchResults = (HtmlInputRowSelect) findComponentInRoot("rowSelectSearchResults");
		}
		return rowSelectSearchResults;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getTextCampGroupHeaderLabel() {
		if (textCampGroupHeaderLabel == null) {
			textCampGroupHeaderLabel = (HtmlOutputText) findComponentInRoot("textCampGroupHeaderLabel");
		}
		return textCampGroupHeaderLabel;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getTextKeycodeHeaderLabel() {
		if (textKeycodeHeaderLabel == null) {
			textKeycodeHeaderLabel = (HtmlOutputText) findComponentInRoot("textKeycodeHeaderLabel");
		}
		return textKeycodeHeaderLabel;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlOutputText getTextProdVanityLinkText() {
		if (textProdVanityLinkText == null) {
			textProdVanityLinkText = (HtmlOutputText) findComponentInRoot("textProdVanityLinkText");
		}
		return textProdVanityLinkText;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlDataTableEx getTableExSearchResultsDataTable() {
		if (tableExSearchResultsDataTable == null) {
			tableExSearchResultsDataTable = (HtmlDataTableEx) findComponentInRoot("tableExSearchResultsDataTable");
		}
		return tableExSearchResultsDataTable;
	}

	protected HtmlPanelGrid getGridNumRowsToShow() {
		if (gridNumRowsToShow == null) {
			gridNumRowsToShow = (HtmlPanelGrid) findComponentInRoot("gridNumRowsToShow");
		}
		return gridNumRowsToShow;
	}

	protected HtmlInputText getTextRowsPerPageValue() {
		if (textRowsPerPageValue == null) {
			textRowsPerPageValue = (HtmlInputText) findComponentInRoot("textRowsPerPageValue");
		}
		return textRowsPerPageValue;
	}

	protected HtmlCommandExButton getButtonUpdateRowToShow() {
		if (buttonUpdateRowToShow == null) {
			buttonUpdateRowToShow = (HtmlCommandExButton) findComponentInRoot("buttonUpdateRowToShow");
		}
		return buttonUpdateRowToShow;
	}

	protected HtmlPanelGrid getGridBottomPagerGrid() {
		if (gridBottomPagerGrid == null) {
			gridBottomPagerGrid = (HtmlPanelGrid) findComponentInRoot("gridBottomPagerGrid");
		}
		return gridBottomPagerGrid;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getTextCampaignName() {
		if (textCampaignName == null) {
			textCampaignName = (HtmlOutputText) findComponentInRoot("textCampaignName");
		}
		return textCampaignName;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getTextCampaignGroup() {
		if (textCampaignGroup == null) {
			textCampaignGroup = (HtmlOutputText) findComponentInRoot("textCampaignGroup");
		}
		return textCampaignGroup;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getTextCampaignPub() {
		if (textCampaignPub == null) {
			textCampaignPub = (HtmlOutputText) findComponentInRoot("textCampaignPub");
		}
		return textCampaignPub;
	}

	protected UIColumnEx getColumnExKeycodeCol() {
		if (columnExKeycodeCol == null) {
			columnExKeycodeCol = (UIColumnEx) findComponentInRoot("columnExKeycodeCol");
		}
		return columnExKeycodeCol;
	}

	protected HtmlOutputText getTextCampaignKeycode() {
		if (textCampaignKeycode == null) {
			textCampaignKeycode = (HtmlOutputText) findComponentInRoot("textCampaignKeycode");
		}
		return textCampaignKeycode;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputLinkEx getLinkExVanityLink() {
		if (linkExVanityLink == null) {
			linkExVanityLink = (HtmlOutputLinkEx) findComponentInRoot("linkExVanityLink");
		}
		return linkExVanityLink;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlOutputText getTextCampaignStatus() {
		if (textCampaignStatus == null) {
			textCampaignStatus = (HtmlOutputText) findComponentInRoot("textCampaignStatus");
		}
		return textCampaignStatus;
	}

	protected HtmlOutputText getTextTestVanityLabel() {
		if (textTestVanityLabel == null) {
			textTestVanityLabel = (HtmlOutputText) findComponentInRoot("textTestVanityLabel");
		}
		return textTestVanityLabel;
	}

	protected HtmlOutputLinkEx getLinkExTestServerLink() {
		if (linkExTestServerLink == null) {
			linkExTestServerLink = (HtmlOutputLinkEx) findComponentInRoot("linkExTestServerLink");
		}
		return linkExTestServerLink;
	}

	protected HtmlForm getFormPublishingForm() {
		if (formPublishingForm == null) {
			formPublishingForm = (HtmlForm) findComponentInRoot("formPublishingForm");
		}
		return formPublishingForm;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected UIColumnEx getColumnExLastErrMsg() {
		if (columnExLastErrMsg == null) {
			columnExLastErrMsg = (UIColumnEx) findComponentInRoot("columnExLastErrMsg");
		}
		return columnExLastErrMsg;
	}

	protected HtmlOutputText getTextLastPublishMessage() {
		if (textLastPublishMessage == null) {
			textLastPublishMessage = (HtmlOutputText) findComponentInRoot("textLastPublishMessage");
		}
		return textLastPublishMessage;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputLinkEx getLinkExBackToSearchPage() {
		if (linkExBackToSearchPage == null) {
			linkExBackToSearchPage = (HtmlOutputLinkEx) findComponentInRoot("linkExBackToSearchPage");
		}
		return linkExBackToSearchPage;
	}

	protected HtmlCommandExButton getButtonPublishToTest() {
		if (buttonPublishToTest == null) {
			buttonPublishToTest = (HtmlCommandExButton) findComponentInRoot("buttonPublishToTest");
		}
		return buttonPublishToTest;
	}

	public String doButtonPublishToTestAction() {
		// Type Java code that runs when the component is clicked
	
		Collection<UsatCampaignIntf> selectedCampaigns = new ArrayList<UsatCampaignIntf>();
		
		ArrayList<CampaignHandler> noLongerValid = new ArrayList<CampaignHandler>();
		SearchResultsHandler coreCollection = this.getPublishingCampaignsHandler();
		for (CampaignHandler ch : coreCollection.getLastSearchResults()) {
			if (ch.getCampaignID() < 0) {
				// may have been deleted on another screen so flag for removal.
				noLongerValid.add(ch);
			}
			else if (ch.isCampaignSelected()) {
				// publish to test regardless of status.
				selectedCampaigns.add(ch.getCampaign());
			}
			ch.setCampaignErrorMessage("");
		}
		
		// remove any deleted campaigns from the list.
		if (noLongerValid.size() > 0) {
			Iterator<CampaignHandler> cItr = noLongerValid.iterator();
			while (cItr.hasNext()) {
				CampaignHandler ch = cItr.next();
				boolean removed = coreCollection.removeCampaignFromCollection(ch);
				if (removed) {
					FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Campaign, " + ch.getCampaignName() + ", was removed because it has been deleted.", null);
					this.getFacesContext().addMessage(null, m1);					
				}
			}
		}
		
		UsatCampaignPublisher publisher = new UsatCampaignPublisher();
		
		try {
			Collection<CampaignPublisherFailure> failures = publisher.publishCampaignsToTestServers(selectedCampaigns);
			
			if (failures != null && failures.size() > 0) {
				FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_WARN, failures.size() + " Failures during publishing.", null);
				this.getFacesContext().addMessage(null, m1);
			}
		}
		catch (Exception e) {
			FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed to publish at least one campaign to test: " + e.getMessage(), e.getMessage());
			this.getFacesContext().addMessage(null, m1);
		}
			
		return "";

	}

	protected HtmlCommandExButton getButtonPublishToProd() {
		if (buttonPublishToProd == null) {
			buttonPublishToProd = (HtmlCommandExButton) findComponentInRoot("buttonPublishToProd");
		}
		return buttonPublishToProd;
	}

	public String doButtonPublishToProdAction() {
		// Type Java code that runs when the component is clicked
		Collection<UsatCampaignIntf> selectedCampaigns = new ArrayList<UsatCampaignIntf>();
		
		ArrayList<CampaignHandler> noLongerValid = new ArrayList<CampaignHandler>();
		SearchResultsHandler coreCollection = this.getPublishingCampaignsHandler();
		
		for (CampaignHandler ch : coreCollection.getLastSearchResults()) {
			ch.setCampaignErrorMessage("");
			if (ch.getCampaignID() < 0) {
				// may have been deleted on another screen so flag for removal.
				noLongerValid.add(ch);
			}
			else {
				if (ch.isCampaignSelected()) {
					int cState = ch.getCampaignState();
					
					if (cState == UsatCampaignIntf.STAGED_STATE || cState == UsatCampaignIntf.REPUBLISH_STATE || cState == UsatCampaignIntf.PUBLISHED_STATE) {
						// only publish campaigns that are selected and in a state requiring publishing.
						selectedCampaigns.add(ch.getCampaign());
					}
					else {
						// if not already in production
						ch.setCampaignErrorMessage("Must be published to Test before production.");
					}
				}// end if selected
				
			} // end else
		}

		// remove any deleted campaigns from the list.
		if (noLongerValid.size() > 0) {
			Iterator<CampaignHandler> cItr = noLongerValid.iterator();
			while (cItr.hasNext()) {
				CampaignHandler ch = cItr.next();
				boolean removed = coreCollection.removeCampaignFromCollection(ch);
				if (removed) {
					FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Campaign, " + ch.getCampaignName() + ", was removed because it has been deleted.", null);
					this.getFacesContext().addMessage(null, m1);					
				}
			}
		}
		
		
		UsatCampaignPublisher publisher = new UsatCampaignPublisher();
		
		try {
			
			Collection<CampaignPublisherFailure> failures = publisher.publishCampaignsToProductionServers(selectedCampaigns);

			if (failures != null && failures.size() > 0) {
				FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_WARN, failures.size() + " Failures during publishing.", null);
				this.getFacesContext().addMessage(null, m1);				
			}
			else {
				FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Published " + selectedCampaigns.size() + " campaigns.", null);
				this.getFacesContext().addMessage(null, m1);								
			}
			
		}
		catch (Exception e) {
			FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed to publish at least one campaign to test: " + e.getMessage(), e.getMessage());
			this.getFacesContext().addMessage(null, m1);
		}
			
		return "";

	}

	protected HtmlSortHeader getSortHeaderCampaignName() {
		if (sortHeaderCampaignName == null) {
			sortHeaderCampaignName = (HtmlSortHeader) findComponentInRoot("sortHeaderCampaignName");
		}
		return sortHeaderCampaignName;
	}

	public String doSortHeaderCampaignNameAction() {
		// Type Java code that runs when the component is clicked
			String state = UISortHeader.getCurrentSortOrder();

		String order = "asc";
		
		if(state!=null) {

			
			if(state.equals(UISortHeader.SORT_BIDOWN)) {
				order = "desc";
			}

			this.getPublishingCampaignsHandler().setSortCol(SearchResultsHandler.SORTBY_CAMP_NAME);
			this.getPublishingCampaignsHandler().setSortOrder(order);
			// sort the data
		}
		
		return "";

	}

	protected HtmlSortHeader getSortHeaderCampaignGroup() {
		if (sortHeaderCampaignGroup == null) {
			sortHeaderCampaignGroup = (HtmlSortHeader) findComponentInRoot("sortHeaderCampaignGroup");
		}
		return sortHeaderCampaignGroup;
	}

	public String doSortHeaderCampaignGroupAction() {
		// Type Java code that runs when the component is clicked
	
		String state = UISortHeader.getCurrentSortOrder();

		String order = "asc";
		
		if(state!=null) {

			
			if(state.equals(UISortHeader.SORT_BIDOWN)) {
				order = "desc";
			}

			this.getPublishingCampaignsHandler().setSortCol(SearchResultsHandler.SORTBY_GROUP_NAME);
			this.getPublishingCampaignsHandler().setSortOrder(order);
			// sort the data
		}
		
		return "";

	}

	protected HtmlSortHeader getSortHeaderKeycode() {
		if (sortHeaderKeycode == null) {
			sortHeaderKeycode = (HtmlSortHeader) findComponentInRoot("sortHeaderKeycode");
		}
		return sortHeaderKeycode;
	}

	public String doSortHeaderKeycodeAction() {
		// Type Java code that runs when the component is clicked
	
		String state = UISortHeader.getCurrentSortOrder();

		String order = "asc";
		
		if(state!=null) {

			
			if(state.equals(UISortHeader.SORT_BIDOWN)) {
				order = "desc";
			}

			this.getPublishingCampaignsHandler().setSortCol(SearchResultsHandler.SORTBY_KEYCODE);
			this.getPublishingCampaignsHandler().setSortOrder(order);
			// sort the data
		}
		
		return "";

	}

	protected HtmlGraphicImageEx getImageExSearchIcon() {
		if (imageExSearchIcon == null) {
			imageExSearchIcon = (HtmlGraphicImageEx) findComponentInRoot("imageExSearchIcon");
		}
		return imageExSearchIcon;
	}

	protected HtmlProgressBar getBarPubStatusBar() {
		if (barPubStatusBar == null) {
			barPubStatusBar = (HtmlProgressBar) findComponentInRoot("barPubStatusBar");
		}
		return barPubStatusBar;
	}

}