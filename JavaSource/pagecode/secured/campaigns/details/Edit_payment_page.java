/**
 * 
 */
package pagecode.secured.campaigns.details;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectManyCheckbox;
import javax.faces.component.html.HtmlSelectOneRadio;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableCreditCardPromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableHTMLPromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableCreditCardPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlButtonPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import javax.faces.component.html.HtmlMessages;

/**
 * @author aeast
 *
 */
public class Edit_payment_page extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorCampaignDetails;
	protected HtmlForm formCampaignDetails;
	protected HtmlPanelGrid gridMainGrid;
	protected HtmlPanelBox boxPanelLeft;
	protected HtmlTabbedPanel tabbedPanelPaymentCustomizations;
	protected HtmlBfPanel bfpanelEZPayConfigsPanel;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlOutputText text1;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected HtmlScriptCollector scriptCollectorNoNavTemplateCollector;
	protected HtmlPanelLayout layoutEZPayConfigLayout1;
	protected HtmlInputTextarea textareaEZPayCustomText;
	protected HtmlSelectBooleanCheckbox checkboxIncludeEZPayOffer;
	protected HtmlPanelLayout layoutNorthEZPayLayout;
	protected HtmlOutputText textDefaultEZPayPromoText;
	protected HtmlSelectOneRadio radioDummyRadioButton;
	protected HtmlBfPanel bfpanelCreditCardsConfigPanel;
	protected HtmlCommandExButton tabbedPanelPaymentCustomizations_back;
	protected HtmlCommandExButton tabbedPanelPaymentCustomizations_next;
	protected HtmlCommandExButton tabbedPanelPaymentCustomizations_finish;
	protected HtmlCommandExButton tabbedPanelPaymentCustomizations_cancel;
	protected HtmlCommandExButton buttonApplyChanges;
	protected HtmlButtonPanel bpCustomButtonArea;
	protected HtmlPanelBox boxPanelRight;
	protected HtmlPanelGroup groupHeaderGroup;
	protected HtmlOutputText textPageHeaderText;
	protected HtmlGraphicImageEx imageExCampaignListImage;
	protected HtmlPanelDialog dialogSelectedCampaignsForEdit;
	protected HtmlDataTableEx tableExSelectedCampaignsForEditV2;
	protected HtmlPanelBox box1;
	protected HtmlOutputText textEditCampsDTHeader;
	protected HtmlInputRowSelect rowSelectRemoveCampaignFromEdit;
	protected UIParameter param1;
	protected HtmlOutputText textEditPubHeader;
	protected HtmlOutputText textEditKeycodeHeader;
	protected HtmlOutputText textEditNameHeader;
	protected HtmlOutputText textEditTypeColHeader;
	protected HtmlOutputText textEditLandingPageHeader;
	protected HtmlCommandExButton buttonRemoveSelectedCampaignsFromEditCollection;
	protected HtmlForm formForm2;
	protected UIColumnEx columnEx6;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text7;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText textEditCampaignType;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textEditCampaignLandingPageDes;
	protected HtmlPanelGroup groupEditCampaignsFooterGroup;
	protected HtmlOutputText textDefaultEZPayHeaderLabel;
	protected HtmlPanelBox boxleftSideHeader;
	protected HtmlPanelBox bodyHeaderEzPay;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText textPromoTextInfoTextLabel;
	protected HtmlOutputSeparator separator1;
	protected HtmlPanelLayout layoutPaymeyOptionsLayout;
	protected HtmlSelectManyCheckbox checkboxPaymentMethods;
	protected HtmlPanelFormBox formBoxCreditCardFormBox;
	protected HtmlFormItem formItemCreditCardChoices;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlGraphicImageEx imageEx2;
	protected HtmlPanelGrid gridRightSideCurrentSettings;
	protected HtmlOutputText textCurrentEZPayPromoTextLabel;
	protected HtmlOutputText textCurrentPaymentOptionsLabel;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlPanelGrid gridCreditCardImageGrid;
	protected HtmlGraphicImageEx imageEx4;
	protected HtmlGraphicImageEx imageEx5;
	protected HtmlGraphicImageEx imageEx6;
	protected HtmlGraphicImageEx imageEx7;
	protected HtmlPanelGroup group1;
	protected HtmlOutputText textCurrentSettingLabel;
	protected HtmlJspPanel jspPanelEZPayBodyInnerPanel;
	protected HtmlInputTextarea textareaCurrentEZPayCustomerText;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlMessages messages1;
	protected HtmlScriptCollector getScriptCollectorCampaignDetails() {
		if (scriptCollectorCampaignDetails == null) {
			scriptCollectorCampaignDetails = (HtmlScriptCollector) findComponentInRoot("scriptCollectorCampaignDetails");
		}
		return scriptCollectorCampaignDetails;
	}

	protected HtmlForm getFormCampaignDetails() {
		if (formCampaignDetails == null) {
			formCampaignDetails = (HtmlForm) findComponentInRoot("formCampaignDetails");
		}
		return formCampaignDetails;
	}

	protected HtmlPanelGrid getGridMainGrid() {
		if (gridMainGrid == null) {
			gridMainGrid = (HtmlPanelGrid) findComponentInRoot("gridMainGrid");
		}
		return gridMainGrid;
	}

	protected HtmlPanelBox getBoxPanelLeft() {
		if (boxPanelLeft == null) {
			boxPanelLeft = (HtmlPanelBox) findComponentInRoot("boxPanelLeft");
		}
		return boxPanelLeft;
	}

	protected HtmlTabbedPanel getTabbedPanelPaymentCustomizations() {
		if (tabbedPanelPaymentCustomizations == null) {
			tabbedPanelPaymentCustomizations = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelPaymentCustomizations");
		}
		return tabbedPanelPaymentCustomizations;
	}

	protected HtmlBfPanel getBfpanelEZPayConfigsPanel() {
		if (bfpanelEZPayConfigsPanel == null) {
			bfpanelEZPayConfigsPanel = (HtmlBfPanel) findComponentInRoot("bfpanelEZPayConfigsPanel");
		}
		return bfpanelEZPayConfigsPanel;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	protected HtmlScriptCollector getScriptCollectorNoNavTemplateCollector() {
		if (scriptCollectorNoNavTemplateCollector == null) {
			scriptCollectorNoNavTemplateCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorNoNavTemplateCollector");
		}
		return scriptCollectorNoNavTemplateCollector;
	}

	protected HtmlPanelLayout getLayoutEZPayConfigLayout1() {
		if (layoutEZPayConfigLayout1 == null) {
			layoutEZPayConfigLayout1 = (HtmlPanelLayout) findComponentInRoot("layoutEZPayConfigLayout1");
		}
		return layoutEZPayConfigLayout1;
	}

	protected HtmlInputTextarea getTextareaEZPayCustomText() {
		if (textareaEZPayCustomText == null) {
			textareaEZPayCustomText = (HtmlInputTextarea) findComponentInRoot("textareaEZPayCustomText");
		}
		return textareaEZPayCustomText;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxIncludeEZPayOffer() {
		if (checkboxIncludeEZPayOffer == null) {
			checkboxIncludeEZPayOffer = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxIncludeEZPayOffer");
		}
		return checkboxIncludeEZPayOffer;
	}

	protected HtmlPanelLayout getLayoutNorthEZPayLayout() {
		if (layoutNorthEZPayLayout == null) {
			layoutNorthEZPayLayout = (HtmlPanelLayout) findComponentInRoot("layoutNorthEZPayLayout");
		}
		return layoutNorthEZPayLayout;
	}

	protected HtmlOutputText getTextDefaultEZPayPromoText() {
		if (textDefaultEZPayPromoText == null) {
			textDefaultEZPayPromoText = (HtmlOutputText) findComponentInRoot("textDefaultEZPayPromoText");
		}
		return textDefaultEZPayPromoText;
	}

	protected HtmlSelectOneRadio getRadioDummyRadioButton() {
		if (radioDummyRadioButton == null) {
			radioDummyRadioButton = (HtmlSelectOneRadio) findComponentInRoot("radioDummyRadioButton");
		}
		return radioDummyRadioButton;
	}

	protected HtmlBfPanel getBfpanelCreditCardsConfigPanel() {
		if (bfpanelCreditCardsConfigPanel == null) {
			bfpanelCreditCardsConfigPanel = (HtmlBfPanel) findComponentInRoot("bfpanelCreditCardsConfigPanel");
		}
		return bfpanelCreditCardsConfigPanel;
	}

	protected HtmlCommandExButton getTabbedPanelPaymentCustomizations_back() {
		if (tabbedPanelPaymentCustomizations_back == null) {
			tabbedPanelPaymentCustomizations_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelPaymentCustomizations_back");
		}
		return tabbedPanelPaymentCustomizations_back;
	}

	protected HtmlCommandExButton getTabbedPanelPaymentCustomizations_next() {
		if (tabbedPanelPaymentCustomizations_next == null) {
			tabbedPanelPaymentCustomizations_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelPaymentCustomizations_next");
		}
		return tabbedPanelPaymentCustomizations_next;
	}

	protected HtmlCommandExButton getTabbedPanelPaymentCustomizations_finish() {
		if (tabbedPanelPaymentCustomizations_finish == null) {
			tabbedPanelPaymentCustomizations_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelPaymentCustomizations_finish");
		}
		return tabbedPanelPaymentCustomizations_finish;
	}

	protected HtmlCommandExButton getTabbedPanelPaymentCustomizations_cancel() {
		if (tabbedPanelPaymentCustomizations_cancel == null) {
			tabbedPanelPaymentCustomizations_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelPaymentCustomizations_cancel");
		}
		return tabbedPanelPaymentCustomizations_cancel;
	}

	protected HtmlCommandExButton getButtonApplyChanges() {
		if (buttonApplyChanges == null) {
			buttonApplyChanges = (HtmlCommandExButton) findComponentInRoot("buttonApplyChanges");
		}
		return buttonApplyChanges;
	}

	protected HtmlButtonPanel getBpCustomButtonArea() {
		if (bpCustomButtonArea == null) {
			bpCustomButtonArea = (HtmlButtonPanel) findComponentInRoot("bpCustomButtonArea");
		}
		return bpCustomButtonArea;
	}

	protected HtmlPanelBox getBoxPanelRight() {
		if (boxPanelRight == null) {
			boxPanelRight = (HtmlPanelBox) findComponentInRoot("boxPanelRight");
		}
		return boxPanelRight;
	}

	protected HtmlPanelGroup getGroupHeaderGroup() {
		if (groupHeaderGroup == null) {
			groupHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupHeaderGroup");
		}
		return groupHeaderGroup;
	}

	protected HtmlOutputText getTextPageHeaderText() {
		if (textPageHeaderText == null) {
			textPageHeaderText = (HtmlOutputText) findComponentInRoot("textPageHeaderText");
		}
		return textPageHeaderText;
	}

	protected HtmlGraphicImageEx getImageExCampaignListImage() {
		if (imageExCampaignListImage == null) {
			imageExCampaignListImage = (HtmlGraphicImageEx) findComponentInRoot("imageExCampaignListImage");
		}
		return imageExCampaignListImage;
	}

	protected HtmlPanelDialog getDialogSelectedCampaignsForEdit() {
		if (dialogSelectedCampaignsForEdit == null) {
			dialogSelectedCampaignsForEdit = (HtmlPanelDialog) findComponentInRoot("dialogSelectedCampaignsForEdit");
		}
		return dialogSelectedCampaignsForEdit;
	}

	protected HtmlDataTableEx getTableExSelectedCampaignsForEditV2() {
		if (tableExSelectedCampaignsForEditV2 == null) {
			tableExSelectedCampaignsForEditV2 = (HtmlDataTableEx) findComponentInRoot("tableExSelectedCampaignsForEditV2");
		}
		return tableExSelectedCampaignsForEditV2;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlOutputText getTextEditCampsDTHeader() {
		if (textEditCampsDTHeader == null) {
			textEditCampsDTHeader = (HtmlOutputText) findComponentInRoot("textEditCampsDTHeader");
		}
		return textEditCampsDTHeader;
	}

	protected HtmlInputRowSelect getRowSelectRemoveCampaignFromEdit() {
		if (rowSelectRemoveCampaignFromEdit == null) {
			rowSelectRemoveCampaignFromEdit = (HtmlInputRowSelect) findComponentInRoot("rowSelectRemoveCampaignFromEdit");
		}
		return rowSelectRemoveCampaignFromEdit;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected HtmlOutputText getTextEditPubHeader() {
		if (textEditPubHeader == null) {
			textEditPubHeader = (HtmlOutputText) findComponentInRoot("textEditPubHeader");
		}
		return textEditPubHeader;
	}

	protected HtmlOutputText getTextEditKeycodeHeader() {
		if (textEditKeycodeHeader == null) {
			textEditKeycodeHeader = (HtmlOutputText) findComponentInRoot("textEditKeycodeHeader");
		}
		return textEditKeycodeHeader;
	}

	protected HtmlOutputText getTextEditNameHeader() {
		if (textEditNameHeader == null) {
			textEditNameHeader = (HtmlOutputText) findComponentInRoot("textEditNameHeader");
		}
		return textEditNameHeader;
	}

	protected HtmlOutputText getTextEditTypeColHeader() {
		if (textEditTypeColHeader == null) {
			textEditTypeColHeader = (HtmlOutputText) findComponentInRoot("textEditTypeColHeader");
		}
		return textEditTypeColHeader;
	}

	protected HtmlOutputText getTextEditLandingPageHeader() {
		if (textEditLandingPageHeader == null) {
			textEditLandingPageHeader = (HtmlOutputText) findComponentInRoot("textEditLandingPageHeader");
		}
		return textEditLandingPageHeader;
	}

	protected HtmlCommandExButton getButtonRemoveSelectedCampaignsFromEditCollection() {
		if (buttonRemoveSelectedCampaignsFromEditCollection == null) {
			buttonRemoveSelectedCampaignsFromEditCollection = (HtmlCommandExButton) findComponentInRoot("buttonRemoveSelectedCampaignsFromEditCollection");
		}
		return buttonRemoveSelectedCampaignsFromEditCollection;
	}

	protected HtmlForm getFormForm2() {
		if (formForm2 == null) {
			formForm2 = (HtmlForm) findComponentInRoot("formForm2");
		}
		return formForm2;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getTextEditCampaignType() {
		if (textEditCampaignType == null) {
			textEditCampaignType = (HtmlOutputText) findComponentInRoot("textEditCampaignType");
		}
		return textEditCampaignType;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getTextEditCampaignLandingPageDes() {
		if (textEditCampaignLandingPageDes == null) {
			textEditCampaignLandingPageDes = (HtmlOutputText) findComponentInRoot("textEditCampaignLandingPageDes");
		}
		return textEditCampaignLandingPageDes;
	}

	protected HtmlPanelGroup getGroupEditCampaignsFooterGroup() {
		if (groupEditCampaignsFooterGroup == null) {
			groupEditCampaignsFooterGroup = (HtmlPanelGroup) findComponentInRoot("groupEditCampaignsFooterGroup");
		}
		return groupEditCampaignsFooterGroup;
	}

	protected HtmlOutputText getTextDefaultEZPayHeaderLabel() {
		if (textDefaultEZPayHeaderLabel == null) {
			textDefaultEZPayHeaderLabel = (HtmlOutputText) findComponentInRoot("textDefaultEZPayHeaderLabel");
		}
		return textDefaultEZPayHeaderLabel;
	}

	protected HtmlPanelBox getBoxleftSideHeader() {
		if (boxleftSideHeader == null) {
			boxleftSideHeader = (HtmlPanelBox) findComponentInRoot("boxleftSideHeader");
		}
		return boxleftSideHeader;
	}

	protected HtmlPanelBox getBodyHeaderEzPay() {
		if (bodyHeaderEzPay == null) {
			bodyHeaderEzPay = (HtmlPanelBox) findComponentInRoot("bodyHeaderEzPay");
		}
		return bodyHeaderEzPay;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getTextPromoTextInfoTextLabel() {
		if (textPromoTextInfoTextLabel == null) {
			textPromoTextInfoTextLabel = (HtmlOutputText) findComponentInRoot("textPromoTextInfoTextLabel");
		}
		return textPromoTextInfoTextLabel;
	}

	protected HtmlOutputSeparator getSeparator1() {
		if (separator1 == null) {
			separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
		}
		return separator1;
	}

	protected HtmlPanelLayout getLayoutPaymeyOptionsLayout() {
		if (layoutPaymeyOptionsLayout == null) {
			layoutPaymeyOptionsLayout = (HtmlPanelLayout) findComponentInRoot("layoutPaymeyOptionsLayout");
		}
		return layoutPaymeyOptionsLayout;
	}

	protected HtmlSelectManyCheckbox getCheckboxPaymentMethods() {
		if (checkboxPaymentMethods == null) {
			checkboxPaymentMethods = (HtmlSelectManyCheckbox) findComponentInRoot("checkboxPaymentMethods");
		}
		return checkboxPaymentMethods;
	}

	protected HtmlPanelFormBox getFormBoxCreditCardFormBox() {
		if (formBoxCreditCardFormBox == null) {
			formBoxCreditCardFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxCreditCardFormBox");
		}
		return formBoxCreditCardFormBox;
	}

	protected HtmlFormItem getFormItemCreditCardChoices() {
		if (formItemCreditCardChoices == null) {
			formItemCreditCardChoices = (HtmlFormItem) findComponentInRoot("formItemCreditCardChoices");
		}
		return formItemCreditCardChoices;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}

	protected HtmlPanelGrid getGridRightSideCurrentSettings() {
		if (gridRightSideCurrentSettings == null) {
			gridRightSideCurrentSettings = (HtmlPanelGrid) findComponentInRoot("gridRightSideCurrentSettings");
		}
		return gridRightSideCurrentSettings;
	}

	protected HtmlOutputText getTextCurrentEZPayPromoTextLabel() {
		if (textCurrentEZPayPromoTextLabel == null) {
			textCurrentEZPayPromoTextLabel = (HtmlOutputText) findComponentInRoot("textCurrentEZPayPromoTextLabel");
		}
		return textCurrentEZPayPromoTextLabel;
	}

	protected HtmlOutputText getTextCurrentPaymentOptionsLabel() {
		if (textCurrentPaymentOptionsLabel == null) {
			textCurrentPaymentOptionsLabel = (HtmlOutputText) findComponentInRoot("textCurrentPaymentOptionsLabel");
		}
		return textCurrentPaymentOptionsLabel;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlPanelGrid getGridCreditCardImageGrid() {
		if (gridCreditCardImageGrid == null) {
			gridCreditCardImageGrid = (HtmlPanelGrid) findComponentInRoot("gridCreditCardImageGrid");
		}
		return gridCreditCardImageGrid;
	}

	protected HtmlGraphicImageEx getImageEx4() {
		if (imageEx4 == null) {
			imageEx4 = (HtmlGraphicImageEx) findComponentInRoot("imageEx4");
		}
		return imageEx4;
	}

	protected HtmlGraphicImageEx getImageEx5() {
		if (imageEx5 == null) {
			imageEx5 = (HtmlGraphicImageEx) findComponentInRoot("imageEx5");
		}
		return imageEx5;
	}

	protected HtmlGraphicImageEx getImageEx6() {
		if (imageEx6 == null) {
			imageEx6 = (HtmlGraphicImageEx) findComponentInRoot("imageEx6");
		}
		return imageEx6;
	}

	protected HtmlGraphicImageEx getImageEx7() {
		if (imageEx7 == null) {
			imageEx7 = (HtmlGraphicImageEx) findComponentInRoot("imageEx7");
		}
		return imageEx7;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlOutputText getTextCurrentSettingLabel() {
		if (textCurrentSettingLabel == null) {
			textCurrentSettingLabel = (HtmlOutputText) findComponentInRoot("textCurrentSettingLabel");
		}
		return textCurrentSettingLabel;
	}

	protected HtmlJspPanel getJspPanelEZPayBodyInnerPanel() {
		if (jspPanelEZPayBodyInnerPanel == null) {
			jspPanelEZPayBodyInnerPanel = (HtmlJspPanel) findComponentInRoot("jspPanelEZPayBodyInnerPanel");
		}
		return jspPanelEZPayBodyInnerPanel;
	}

	protected HtmlInputTextarea getTextareaCurrentEZPayCustomerText() {
		if (textareaCurrentEZPayCustomerText == null) {
			textareaCurrentEZPayCustomerText = (HtmlInputTextarea) findComponentInRoot("textareaCurrentEZPayCustomerText");
		}
		return textareaCurrentEZPayCustomerText;
	}

	public String doButtonApplyChangesAction() {
		// Type Java code that runs when the component is clicked
	
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		editor.setChanged(true);
		
		EditCampaignDetailsHandler campaignsHandler = this.getEdittingCampaignsHandler();

		EditableHTMLPromotionIntf newEZPayPromoText = null;
		
		boolean useOverrideEZPayText = false;
		
		if (editor.isOverrideZPayPaymentPageText()) {
			useOverrideEZPayText = true;
			// update U/I current settings
			editor.setCurrentEZPayPaymentPageText(editor.getCustomEZPayPaymentPageText());
		}
		else {
			editor.setCurrentEZPayPaymentPageText(editor.getDefaultEZPAYPaymentPageText());
		}
		
		String[] paymentMethodArray = editor.getPaymentMethods();
		boolean overrideCreditCards = false;

		// only override if not all selected.
		if (paymentMethodArray != null && paymentMethodArray.length != 5) {
			overrideCreditCards = true;
		}
		
		// apply changes to campaigns
		for (CampaignHandler ch : campaignsHandler.getCampaigns()) {
			UsatCampaignIntf c = ch.getCampaign();
			
			// clear previous settings
			c.getPromotionSet().clearPaymentPageConfigurations();
			
			newEZPayPromoText = null;
			
			if (useOverrideEZPayText) {
				String overrideText = editor.getCustomEZPayPaymentPageText();
				if (overrideText != null && overrideText.trim().length() > 0) {
					// create new promo
					try {
						newEZPayPromoText = new EditableHTMLPromotionBO();
						newEZPayPromoText.setType(PromotionIntf.EZPAY_PROMO);
						newEZPayPromoText.setPromotionalHTML(overrideText);
					}
					catch (Exception e) {
						FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid EZ-PAY Promotional Text: " + e.getMessage() , null);
						this.getFacesContext().addMessage(null, m);
						e.printStackTrace();
						newEZPayPromoText = null;
					}
				}
				else {
					// Set it up so it inserts a comment instead of text
					try {
						newEZPayPromoText = new EditableHTMLPromotionBO();
						newEZPayPromoText.setType(PromotionIntf.EZPAY_PROMO);
						newEZPayPromoText.setPromotionalHTML(CampaignEditsHandler.OMIT_EZPAY_CUSTOM_TEXT);
					}
					catch (Exception e) {
						e.printStackTrace();
						newEZPayPromoText = null;
					}
				}
			}
			c.getPromotionSet().setPromoEZPayCustomText(newEZPayPromoText);
			
			if (overrideCreditCards) {
				EditableCreditCardPromotionIntf cCard = new EditableCreditCardPromotionBO();
	            try {
	                for (int i=0; i<paymentMethodArray.length; i++) {
	                    cCard.addCardToPromo(paymentMethodArray[i]);
	                }
	                c.getPromotionSet().setPromoCreditCard(cCard);
	            }
	            catch (UsatException ue) {
	                    // ignore
	            	ue.printStackTrace();
	            }
				
			}
		}

		
		
		return "";
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

}