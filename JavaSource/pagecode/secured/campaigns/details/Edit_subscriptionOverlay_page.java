/**
 * 
 */
package pagecode.secured.campaigns.details;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneRadio;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableImagePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlFileupload;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import javax.faces.component.html.HtmlMessage;

/**
 * @author aeast
 *
 */
public class Edit_subscriptionOverlay_page extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorCampaignDetails;
	protected HtmlMessages messages1;
	protected HtmlForm formCampaignDetails;
	protected HtmlDataTableEx tableExSelectedCampaignsForEditV2;
	protected HtmlPanelBox box1;
	protected HtmlOutputText textEditCampsDTHeader;
	protected HtmlInputRowSelect rowSelectRemoveCampaignFromEdit;
	protected UIParameter param1;
	protected HtmlOutputText textEditPubHeader;
	protected HtmlOutputText textEditKeycodeHeader;
	protected HtmlOutputText textEditNameHeader;
	protected HtmlOutputText textEditTypeColHeader;
	protected HtmlOutputText textEditLandingPageHeader;
	protected HtmlCommandExButton buttonRemoveSelectedCampaignsFromEditCollection;
	protected HtmlPanelDialog dialogSelectedCampaignsForEdit;
	protected UIColumnEx columnEx6;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text7;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText textEditCampaignType;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textEditCampaignLandingPageDes;
	protected HtmlPanelGroup groupEditCampaignsFooterGroup;
	protected HtmlPanelBox boxLeftBox;
	protected HtmlPanelLayout layoutHeaderImageLayout;
	protected HtmlPanelGrid gridHeaderImageDefaultImage;
	protected HtmlOutputText textHeaderImageDefaultLabel;
	protected HtmlJspPanel jspPanelInnerHeaderNoImage;
	protected HtmlPanelLayout layoutInnerHeaderImageCustomImageLayout;
	protected HtmlPanelFormBox formBoxHeaderCustomImage;
	protected HtmlFormItem formItemCustomImageFormItem;
	protected HtmlFileupload fileuploadPromoImageSubsribePopOverlay;
	protected HtmlInputText textCustomHeaderOnClickURL;
	protected HtmlInputText textCustomHeaderImageAltText;
	protected HtmlCommandExButton tabbedPanelInnerTabHeaderImage_back;
	protected HtmlCommandExButton tabbedPanelInnerTabHeaderImage_next;
	protected HtmlCommandExButton tabbedPanelInnerTabHeaderImage_finish;
	protected HtmlCommandExButton tabbedPanelInnerTabHeaderImage_cancel;
	protected HtmlPanelGrid gridMainHeaderImageOptionGrid;
	protected HtmlPanelGroup group1;
	protected HtmlOutputText textHeaderImageHeader;
	protected HtmlCommandExButton tabbedPanelHeaderNavTabs_back;
	protected HtmlCommandExButton tabbedPanelHeaderNavTabs_finish;
	protected HtmlCommandExButton tabbedPanelHeaderNavTabs_cancel;
	protected HtmlCommandExButton buttonApplyChanges;
	protected HtmlPanelGrid gridCurrentSettings;
	protected HtmlPanelGroup group3;
	protected HtmlOutputText textCurrentSettingLabel;
	protected HtmlPanelGroup groupHeaderGroup;
	protected HtmlOutputText textPageHeaderText;
	protected HtmlPanelGrid gridMainGrid;
	protected HtmlOutputSeparator separator1;
	protected HtmlGraphicImageEx imageExHeaderImageDefault;
	protected HtmlOutputText textInnerHeaderNoImageLabel;
	protected HtmlFormItem formItemCustomHeaderOnClickURL;
	protected HtmlFormItem formItemCustomHeaderImageAltText;
	protected HtmlSelectOneRadio radioOverlayImage;
	protected HtmlPanelBox boxRightBox;
	protected HtmlOutputText textCurrentHeaderImageLabel;
	protected HtmlGraphicImageEx imageExHeaderInMemoryVersion;
	protected HtmlGraphicImageEx imageExCampaignListImage;
	protected HtmlForm formForm2;
	protected HtmlBfPanel bfpanelHeaderImage;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected CampaignHandlerReadOnly defaultCampaignReadOnly;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlMessage pupUpFileUploadErrMsg;
	protected HtmlCommandExButton tabbedPanelHeaderNavTabs_next;
	protected CampaignHandler swLandingPages;

	protected HtmlScriptCollector getScriptCollectorCampaignDetails() {
		if (scriptCollectorCampaignDetails == null) {
			scriptCollectorCampaignDetails = (HtmlScriptCollector) findComponentInRoot("scriptCollectorCampaignDetails");
		}
		return scriptCollectorCampaignDetails;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlForm getFormCampaignDetails() {
		if (formCampaignDetails == null) {
			formCampaignDetails = (HtmlForm) findComponentInRoot("formCampaignDetails");
		}
		return formCampaignDetails;
	}

	protected HtmlDataTableEx getTableExSelectedCampaignsForEditV2() {
		if (tableExSelectedCampaignsForEditV2 == null) {
			tableExSelectedCampaignsForEditV2 = (HtmlDataTableEx) findComponentInRoot("tableExSelectedCampaignsForEditV2");
		}
		return tableExSelectedCampaignsForEditV2;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlOutputText getTextEditCampsDTHeader() {
		if (textEditCampsDTHeader == null) {
			textEditCampsDTHeader = (HtmlOutputText) findComponentInRoot("textEditCampsDTHeader");
		}
		return textEditCampsDTHeader;
	}

	protected HtmlInputRowSelect getRowSelectRemoveCampaignFromEdit() {
		if (rowSelectRemoveCampaignFromEdit == null) {
			rowSelectRemoveCampaignFromEdit = (HtmlInputRowSelect) findComponentInRoot("rowSelectRemoveCampaignFromEdit");
		}
		return rowSelectRemoveCampaignFromEdit;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected HtmlOutputText getTextEditPubHeader() {
		if (textEditPubHeader == null) {
			textEditPubHeader = (HtmlOutputText) findComponentInRoot("textEditPubHeader");
		}
		return textEditPubHeader;
	}

	protected HtmlOutputText getTextEditKeycodeHeader() {
		if (textEditKeycodeHeader == null) {
			textEditKeycodeHeader = (HtmlOutputText) findComponentInRoot("textEditKeycodeHeader");
		}
		return textEditKeycodeHeader;
	}

	protected HtmlOutputText getTextEditNameHeader() {
		if (textEditNameHeader == null) {
			textEditNameHeader = (HtmlOutputText) findComponentInRoot("textEditNameHeader");
		}
		return textEditNameHeader;
	}

	protected HtmlOutputText getTextEditTypeColHeader() {
		if (textEditTypeColHeader == null) {
			textEditTypeColHeader = (HtmlOutputText) findComponentInRoot("textEditTypeColHeader");
		}
		return textEditTypeColHeader;
	}

	protected HtmlOutputText getTextEditLandingPageHeader() {
		if (textEditLandingPageHeader == null) {
			textEditLandingPageHeader = (HtmlOutputText) findComponentInRoot("textEditLandingPageHeader");
		}
		return textEditLandingPageHeader;
	}

	protected HtmlCommandExButton getButtonRemoveSelectedCampaignsFromEditCollection() {
		if (buttonRemoveSelectedCampaignsFromEditCollection == null) {
			buttonRemoveSelectedCampaignsFromEditCollection = (HtmlCommandExButton) findComponentInRoot("buttonRemoveSelectedCampaignsFromEditCollection");
		}
		return buttonRemoveSelectedCampaignsFromEditCollection;
	}

	protected HtmlPanelDialog getDialogSelectedCampaignsForEdit() {
		if (dialogSelectedCampaignsForEdit == null) {
			dialogSelectedCampaignsForEdit = (HtmlPanelDialog) findComponentInRoot("dialogSelectedCampaignsForEdit");
		}
		return dialogSelectedCampaignsForEdit;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getTextEditCampaignType() {
		if (textEditCampaignType == null) {
			textEditCampaignType = (HtmlOutputText) findComponentInRoot("textEditCampaignType");
		}
		return textEditCampaignType;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getTextEditCampaignLandingPageDes() {
		if (textEditCampaignLandingPageDes == null) {
			textEditCampaignLandingPageDes = (HtmlOutputText) findComponentInRoot("textEditCampaignLandingPageDes");
		}
		return textEditCampaignLandingPageDes;
	}

	protected HtmlPanelGroup getGroupEditCampaignsFooterGroup() {
		if (groupEditCampaignsFooterGroup == null) {
			groupEditCampaignsFooterGroup = (HtmlPanelGroup) findComponentInRoot("groupEditCampaignsFooterGroup");
		}
		return groupEditCampaignsFooterGroup;
	}

	protected HtmlPanelBox getBoxLeftBox() {
		if (boxLeftBox == null) {
			boxLeftBox = (HtmlPanelBox) findComponentInRoot("boxLeftBox");
		}
		return boxLeftBox;
	}

	protected HtmlPanelLayout getLayoutHeaderImageLayout() {
		if (layoutHeaderImageLayout == null) {
			layoutHeaderImageLayout = (HtmlPanelLayout) findComponentInRoot("layoutHeaderImageLayout");
		}
		return layoutHeaderImageLayout;
	}

	protected HtmlPanelGrid getGridHeaderImageDefaultImage() {
		if (gridHeaderImageDefaultImage == null) {
			gridHeaderImageDefaultImage = (HtmlPanelGrid) findComponentInRoot("gridHeaderImageDefaultImage");
		}
		return gridHeaderImageDefaultImage;
	}

	protected HtmlOutputText getTextHeaderImageDefaultLabel() {
		if (textHeaderImageDefaultLabel == null) {
			textHeaderImageDefaultLabel = (HtmlOutputText) findComponentInRoot("textHeaderImageDefaultLabel");
		}
		return textHeaderImageDefaultLabel;
	}

	protected HtmlJspPanel getJspPanelInnerHeaderNoImage() {
		if (jspPanelInnerHeaderNoImage == null) {
			jspPanelInnerHeaderNoImage = (HtmlJspPanel) findComponentInRoot("jspPanelInnerHeaderNoImage");
		}
		return jspPanelInnerHeaderNoImage;
	}

	protected HtmlPanelLayout getLayoutInnerHeaderImageCustomImageLayout() {
		if (layoutInnerHeaderImageCustomImageLayout == null) {
			layoutInnerHeaderImageCustomImageLayout = (HtmlPanelLayout) findComponentInRoot("layoutInnerHeaderImageCustomImageLayout");
		}
		return layoutInnerHeaderImageCustomImageLayout;
	}

	protected HtmlPanelFormBox getFormBoxHeaderCustomImage() {
		if (formBoxHeaderCustomImage == null) {
			formBoxHeaderCustomImage = (HtmlPanelFormBox) findComponentInRoot("formBoxHeaderCustomImage");
		}
		return formBoxHeaderCustomImage;
	}

	protected HtmlFormItem getFormItemCustomImageFormItem() {
		if (formItemCustomImageFormItem == null) {
			formItemCustomImageFormItem = (HtmlFormItem) findComponentInRoot("formItemCustomImageFormItem");
		}
		return formItemCustomImageFormItem;
	}

	protected HtmlFileupload getFileuploadPromoImageSubsribePopOverlay() {
		if (fileuploadPromoImageSubsribePopOverlay == null) {
			fileuploadPromoImageSubsribePopOverlay = (HtmlFileupload) findComponentInRoot("fileuploadPromoImageSubsribePopOverlay");
		}
		return fileuploadPromoImageSubsribePopOverlay;
	}

	protected HtmlInputText getTextCustomHeaderOnClickURL() {
		if (textCustomHeaderOnClickURL == null) {
			textCustomHeaderOnClickURL = (HtmlInputText) findComponentInRoot("textCustomHeaderOnClickURL");
		}
		return textCustomHeaderOnClickURL;
	}

	protected HtmlInputText getTextCustomHeaderImageAltText() {
		if (textCustomHeaderImageAltText == null) {
			textCustomHeaderImageAltText = (HtmlInputText) findComponentInRoot("textCustomHeaderImageAltText");
		}
		return textCustomHeaderImageAltText;
	}

	protected HtmlCommandExButton getTabbedPanelInnerTabHeaderImage_back() {
		if (tabbedPanelInnerTabHeaderImage_back == null) {
			tabbedPanelInnerTabHeaderImage_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerTabHeaderImage_back");
		}
		return tabbedPanelInnerTabHeaderImage_back;
	}

	protected HtmlCommandExButton getTabbedPanelInnerTabHeaderImage_next() {
		if (tabbedPanelInnerTabHeaderImage_next == null) {
			tabbedPanelInnerTabHeaderImage_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerTabHeaderImage_next");
		}
		return tabbedPanelInnerTabHeaderImage_next;
	}

	protected HtmlCommandExButton getTabbedPanelInnerTabHeaderImage_finish() {
		if (tabbedPanelInnerTabHeaderImage_finish == null) {
			tabbedPanelInnerTabHeaderImage_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerTabHeaderImage_finish");
		}
		return tabbedPanelInnerTabHeaderImage_finish;
	}

	protected HtmlCommandExButton getTabbedPanelInnerTabHeaderImage_cancel() {
		if (tabbedPanelInnerTabHeaderImage_cancel == null) {
			tabbedPanelInnerTabHeaderImage_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelInnerTabHeaderImage_cancel");
		}
		return tabbedPanelInnerTabHeaderImage_cancel;
	}

	protected HtmlPanelGrid getGridMainHeaderImageOptionGrid() {
		if (gridMainHeaderImageOptionGrid == null) {
			gridMainHeaderImageOptionGrid = (HtmlPanelGrid) findComponentInRoot("gridMainHeaderImageOptionGrid");
		}
		return gridMainHeaderImageOptionGrid;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlOutputText getTextHeaderImageHeader() {
		if (textHeaderImageHeader == null) {
			textHeaderImageHeader = (HtmlOutputText) findComponentInRoot("textHeaderImageHeader");
		}
		return textHeaderImageHeader;
	}

	protected HtmlCommandExButton getTabbedPanelHeaderNavTabs_back() {
		if (tabbedPanelHeaderNavTabs_back == null) {
			tabbedPanelHeaderNavTabs_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelHeaderNavTabs_back");
		}
		return tabbedPanelHeaderNavTabs_back;
	}

	protected HtmlCommandExButton getTabbedPanelHeaderNavTabs_finish() {
		if (tabbedPanelHeaderNavTabs_finish == null) {
			tabbedPanelHeaderNavTabs_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelHeaderNavTabs_finish");
		}
		return tabbedPanelHeaderNavTabs_finish;
	}

	protected HtmlCommandExButton getTabbedPanelHeaderNavTabs_cancel() {
		if (tabbedPanelHeaderNavTabs_cancel == null) {
			tabbedPanelHeaderNavTabs_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelHeaderNavTabs_cancel");
		}
		return tabbedPanelHeaderNavTabs_cancel;
	}

	protected HtmlCommandExButton getButtonApplyChanges() {
		if (buttonApplyChanges == null) {
			buttonApplyChanges = (HtmlCommandExButton) findComponentInRoot("buttonApplyChanges");
		}
		return buttonApplyChanges;
	}

	protected HtmlPanelGrid getGridCurrentSettings() {
		if (gridCurrentSettings == null) {
			gridCurrentSettings = (HtmlPanelGrid) findComponentInRoot("gridCurrentSettings");
		}
		return gridCurrentSettings;
	}

	protected HtmlPanelGroup getGroup3() {
		if (group3 == null) {
			group3 = (HtmlPanelGroup) findComponentInRoot("group3");
		}
		return group3;
	}

	protected HtmlOutputText getTextCurrentSettingLabel() {
		if (textCurrentSettingLabel == null) {
			textCurrentSettingLabel = (HtmlOutputText) findComponentInRoot("textCurrentSettingLabel");
		}
		return textCurrentSettingLabel;
	}

	protected HtmlPanelGroup getGroupHeaderGroup() {
		if (groupHeaderGroup == null) {
			groupHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupHeaderGroup");
		}
		return groupHeaderGroup;
	}

	protected HtmlOutputText getTextPageHeaderText() {
		if (textPageHeaderText == null) {
			textPageHeaderText = (HtmlOutputText) findComponentInRoot("textPageHeaderText");
		}
		return textPageHeaderText;
	}

	protected HtmlPanelGrid getGridMainGrid() {
		if (gridMainGrid == null) {
			gridMainGrid = (HtmlPanelGrid) findComponentInRoot("gridMainGrid");
		}
		return gridMainGrid;
	}

	protected HtmlOutputSeparator getSeparator1() {
		if (separator1 == null) {
			separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
		}
		return separator1;
	}

	protected HtmlGraphicImageEx getImageExHeaderImageDefault() {
		if (imageExHeaderImageDefault == null) {
			imageExHeaderImageDefault = (HtmlGraphicImageEx) findComponentInRoot("imageExHeaderImageDefault");
		}
		return imageExHeaderImageDefault;
	}

	protected HtmlOutputText getTextInnerHeaderNoImageLabel() {
		if (textInnerHeaderNoImageLabel == null) {
			textInnerHeaderNoImageLabel = (HtmlOutputText) findComponentInRoot("textInnerHeaderNoImageLabel");
		}
		return textInnerHeaderNoImageLabel;
	}

	protected HtmlFormItem getFormItemCustomHeaderOnClickURL() {
		if (formItemCustomHeaderOnClickURL == null) {
			formItemCustomHeaderOnClickURL = (HtmlFormItem) findComponentInRoot("formItemCustomHeaderOnClickURL");
		}
		return formItemCustomHeaderOnClickURL;
	}

	protected HtmlFormItem getFormItemCustomHeaderImageAltText() {
		if (formItemCustomHeaderImageAltText == null) {
			formItemCustomHeaderImageAltText = (HtmlFormItem) findComponentInRoot("formItemCustomHeaderImageAltText");
		}
		return formItemCustomHeaderImageAltText;
	}

	protected HtmlSelectOneRadio getRadioOverlayImage() {
		if (radioOverlayImage == null) {
			radioOverlayImage = (HtmlSelectOneRadio) findComponentInRoot("radioOverlayImage");
		}
		return radioOverlayImage;
	}

	protected HtmlPanelBox getBoxRightBox() {
		if (boxRightBox == null) {
			boxRightBox = (HtmlPanelBox) findComponentInRoot("boxRightBox");
		}
		return boxRightBox;
	}

	protected HtmlOutputText getTextCurrentHeaderImageLabel() {
		if (textCurrentHeaderImageLabel == null) {
			textCurrentHeaderImageLabel = (HtmlOutputText) findComponentInRoot("textCurrentHeaderImageLabel");
		}
		return textCurrentHeaderImageLabel;
	}

	protected HtmlGraphicImageEx getImageExHeaderInMemoryVersion() {
		if (imageExHeaderInMemoryVersion == null) {
			imageExHeaderInMemoryVersion = (HtmlGraphicImageEx) findComponentInRoot("imageExHeaderInMemoryVersion");
		}
		return imageExHeaderInMemoryVersion;
	}

	protected HtmlGraphicImageEx getImageExCampaignListImage() {
		if (imageExCampaignListImage == null) {
			imageExCampaignListImage = (HtmlGraphicImageEx) findComponentInRoot("imageExCampaignListImage");
		}
		return imageExCampaignListImage;
	}

	protected HtmlForm getFormForm2() {
		if (formForm2 == null) {
			formForm2 = (HtmlForm) findComponentInRoot("formForm2");
		}
		return formForm2;
	}

	protected HtmlBfPanel getBfpanelHeaderImage() {
		if (bfpanelHeaderImage == null) {
			bfpanelHeaderImage = (HtmlBfPanel) findComponentInRoot("bfpanelHeaderImage");
		}
		return bfpanelHeaderImage;
	}

	public String doButtonApplyChangesAction() {
		// Type Java code that runs when the component is clicked
		//
		//  NOTE: May need to modify this to support configuring a different pop up for electronic versions of pub
		//
		
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		
		EditCampaignDetailsHandler campaignsHandler = this.getEdittingCampaignsHandler();

		ImageHandler newOrderPopUpOverlayImage = null;
		
		// following array should be added as a faces managed bean for use on cleaning up after a save.
		ArrayList<EditableImagePromotionIntf> oldImages = new ArrayList<EditableImagePromotionIntf>();
		
		boolean useOldPopUpOverlayImage = false;
		if (editor.getNewOrderPopUpOverlayImageOption().equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE)) {
			// new image possibly uploaded
			ImageHandler h = editor.getNewOrderPopUpOverlayCustomImage();
			if (h.getImageContents() != null && h.getImageFileName() != null) {
				if (editor.isImageNameReserved(h.getImageFileName())) {
					FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The image name: " + h.getImageFileName() + " is a reserved name. You must first rename your image and then upload it." , null);
					this.getFacesContext().addMessage(null, m);
					return "failure";
				}
				editor.setCurrentNewOrderPopUpOverlayImage(editor.getNewOrderPopUpOverlayCustomImage());
				newOrderPopUpOverlayImage = editor.getNewOrderPopUpOverlayCustomImage();
			}
			else {
				useOldPopUpOverlayImage = true;
			}
		}
		else if (editor.getNewOrderPopUpOverlayImageOption().equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
			editor.setCurrentNewOrderPopUpOverlayImage(null);
			newOrderPopUpOverlayImage = editor.getCurrentNewOrderPopUpOverlayImage();
		}
		else {
			editor.setCurrentNewOrderPopUpOverlayImage(editor.getDefaultNewOrderPopUpOverlayImage());
			newOrderPopUpOverlayImage = null;
		}

		// apply the change to the campaigns being editted
		for (CampaignHandler ch : campaignsHandler.getCampaigns()) {
			UsatCampaignIntf c = ch.getCampaign();
			
			EditablePromotionSetIntf promotionSet = c.getPromotionSet();
			
			// save reference to old configurations
			EditableImagePromotionIntf originalNewOrderPopUpOverlayImage = promotionSet.getNewSubscriptionOrderPopUpOverlay();
			if (originalNewOrderPopUpOverlayImage != null && !originalNewOrderPopUpOverlayImage.isShimImage() && !useOldPopUpOverlayImage){
				// add to list of images that may need to be cleaned up.
				oldImages.add(originalNewOrderPopUpOverlayImage);				
			}

			// clear any prevous settings
			promotionSet.clearNewOrderPopOverlayConfigurations();
			
			// set up new header image
			EditableImagePromotionIntf popUpOverlayImagePromo =  null;
			try {
				if (useOldPopUpOverlayImage) {
					popUpOverlayImagePromo = originalNewOrderPopUpOverlayImage;
				}
				else if (newOrderPopUpOverlayImage != null) {
				
					popUpOverlayImagePromo = new EditableImagePromotionBO();
					popUpOverlayImagePromo.setType(PromotionIntf.ORDER_PATH_POP_OVERLAY);
					if (!newOrderPopUpOverlayImage.isShimImage()) {
						// set up custom
						popUpOverlayImagePromo.setImageContents(newOrderPopUpOverlayImage.getImageContents());
						popUpOverlayImagePromo.setImageFileType(newOrderPopUpOverlayImage.getImageFileType());
						popUpOverlayImagePromo.setImagePathString("/images/marketingimages/" + newOrderPopUpOverlayImage.getImageFileName_NameOnly());
						popUpOverlayImagePromo.setImageLinkToURL(newOrderPopUpOverlayImage.getLinkURL());
						popUpOverlayImagePromo.setImageAltText(newOrderPopUpOverlayImage.getAlternateText());
					}
					else {
						// set up shim
						popUpOverlayImagePromo.setImagePathString(CampaignEditsHandler.PROD_SHIM_PATH);
						// set the text to hide the pop up.
						popUpOverlayImagePromo.setFulfillText("HIDE");
					}
					
				}
			}catch (Exception e){
				e.printStackTrace();
			}			
			
			// set the new object into the promotion set of this campaign
			promotionSet.setNewSubscriptionOrderPopUpOverlay(popUpOverlayImagePromo);
			if (popUpOverlayImagePromo == null) {
				// make sure the default radio button is selected on gui
				editor.setNewOrderPopUpOverlayImageOption(CampaignEditsHandler.USE_DEFAULT_IMAGE);
			}
			
			
		} // end for campaigns being editted

		editor.setChanged(true);
		
		return "";
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getManagedBean("campaignDetailEditsHandler");
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getDefaultCampaignReadOnly() {
		if (defaultCampaignReadOnly == null) {
			defaultCampaignReadOnly = (CampaignHandlerReadOnly) getManagedBean("defaultCampaignReadOnly");
		}
		return defaultCampaignReadOnly;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setDefaultCampaignReadOnly(
			CampaignHandlerReadOnly defaultCampaignReadOnly) {
		this.defaultCampaignReadOnly = defaultCampaignReadOnly;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getManagedBean("edittingCampaignsHandler");
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	protected HtmlMessage getPupUpFileUploadErrMsg() {
		if (pupUpFileUploadErrMsg == null) {
			pupUpFileUploadErrMsg = (HtmlMessage) findComponentInRoot("pupUpFileUploadErrMsg");
		}
		return pupUpFileUploadErrMsg;
	}

	protected HtmlCommandExButton getTabbedPanelHeaderNavTabs_next() {
		if (tabbedPanelHeaderNavTabs_next == null) {
			tabbedPanelHeaderNavTabs_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelHeaderNavTabs_next");
		}
		return tabbedPanelHeaderNavTabs_next;
	}

}