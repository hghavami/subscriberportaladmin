/**
 * 
 */
package pagecode.secured.campaigns.details;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlForm;
import com.ibm.odc.jsf.components.components.rte.UIRichTextEditor;
import com.ibm.faces.component.html.HtmlScriptCollector;

/**
 * @author aeast
 *
 */
public class TestPage extends PageCodeBase {

	protected HtmlForm form1;
	protected UIRichTextEditor richTextEditor1;
	protected HtmlScriptCollector scriptCollector1;

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected UIRichTextEditor getRichTextEditor1() {
		if (richTextEditor1 == null) {
			richTextEditor1 = (UIRichTextEditor) findComponentInRoot("richTextEditor1");
		}
		return richTextEditor1;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

}