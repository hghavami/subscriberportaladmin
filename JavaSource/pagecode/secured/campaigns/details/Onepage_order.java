/**
 * 
 */
package pagecode.secured.campaigns.details;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditableHTMLPromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditablePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlButtonPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.odc.jsf.components.components.rte.UIRichTextEditor;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

/**
 * @author aeast
 *
 */
public class Onepage_order extends PageCodeBase {

	protected HtmlOutputText textSysDescriptionHeader;
	protected HtmlOutputText text1;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlMessages messages1;
	protected HtmlPanelGroup groupHeaderGroup;
	protected HtmlOutputText textPageHeaderText;
	protected HtmlPanelLayout layoutInnerPromotionalText;
	protected HtmlOutputText textPromoTextMsg;
	protected HtmlPanelLayout layoutInnerTermsandConditionsLayout;
	protected HtmlOutputText textTermsConditionsTextMsg;
	protected HtmlCommandExButton tabbedPanelTermsTextAndImages_back;
	protected HtmlCommandExButton tabbedPanelTermsTextAndImages_next;
	protected HtmlCommandExButton tabbedPanelTermsTextAndImages_finish;
	protected HtmlCommandExButton tabbedPanelTermsTextAndImages_cancel;
	protected HtmlCommandExButton buttonApplyChanges;
	protected HtmlPanelGrid gridCurrentSettings;
	protected HtmlPanelGroup group1;
	protected HtmlOutputText textCurrentSettingLabel;
	protected HtmlOutputText textPreviewPromoText;
	protected HtmlOutputText textCurrentForceEZPayTermsLabel;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected HtmlForm form1;
	protected HtmlGraphicImageEx imageExCampaignListImage;
	protected HtmlPanelGrid gridCurrentPromoLabelGrid;
	protected HtmlGraphicImageEx imageExCurrentPromoText1DetailsImage;
	protected HtmlPanelGrid gridCurrentForceEZPayTermsGrid;
	protected HtmlOutputText textCurrentForceEZPayTermsText;
	protected HtmlTabbedPanel tabbedPanelTermsTextAndImages;
	protected HtmlBfPanel bfpanelHeaderPromoText;
	protected UIRichTextEditor richTextEditorTermsAndConditionsText;
	protected HtmlBfPanel bfpanelTermsAndConditions;
	protected HtmlButtonPanel bpCustomButtonPanel;
	protected UIRichTextEditor richTextEditor1;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlCommandLink linkClearPromoText;
	protected HtmlOutputText textClearPromoTextLabel;
	protected HtmlOutputText textDefaultPromotTextLabel;
	protected HtmlOutputText textDefaultTopPromoText;
	protected HtmlOutputSeparator separator1;
	protected HtmlOutputText textDefaultForceEZPayTermsLabel;
	protected HtmlOutputText textDefaultForceEZPayTermsText;
	protected HtmlCommandLink linkClearDisclaimerText;
	protected HtmlOutputText textClearDisclaimerLabel;
	protected HtmlBfPanel bfpanelForceEZPayTermsText;
	protected HtmlPanelLayout layoutInnerForceEZPayTermsLayout;
	protected HtmlCommandLink linkClearForceEZPayTermsText;
	protected HtmlOutputText textClearForceEZPayTermsLabel;
	protected HtmlOutputText textForceEZPayTermsTextMsg;
	protected HtmlOutputSeparator separator2;
	protected HtmlOutputText textDefaultDisclaimerTextLabel;
	protected HtmlOutputText textDefaultDisclaimerText;
	protected HtmlOutputText textCurrentTermsAndCondLabel;
	protected HtmlPanelGrid gridCurrentTermsConditionGrid;
	protected HtmlGraphicImageEx imageExCurrentDisclaimerTextDetailsImage;
	protected HtmlOutputText textCurrentTermsConditionsText;
	protected HtmlJspPanel jspPanel1;
	protected HtmlOutputText textPromoText1NumChars;
	protected HtmlOutputText textCurrentPromoText1HTML;
	protected HtmlPanelDialog dialogCurrentPromoTextDialogue;
	protected HtmlJspPanel jspPanelRawTermCondTextModalPanel;
	protected HtmlOutputText textTermsCondTextNumChars;
	protected HtmlOutputText textCurrentDisclaimerTextHTML;
	protected HtmlPanelDialog dialogCurrentTermsConditionTextDialgue;
	protected HtmlDataTableEx tableExSelectedCampaignsForEditV2;
	protected HtmlPanelBox box1;
	protected HtmlOutputText textEditCampsDTHeader;
	protected HtmlInputRowSelect rowSelectRemoveCampaignFromEdit;
	protected UIParameter param1;
	protected HtmlOutputText textEditPubHeader;
	protected HtmlOutputText textEditKeycodeHeader;
	protected HtmlOutputText textEditNameHeader;
	protected HtmlOutputText textEditLandingPageHeader;
	protected HtmlCommandExButton buttonRemoveSelectedCampaignsFromEditCollection;
	protected HtmlForm formForm2;
	protected HtmlPanelDialog dialogSelectedCampaignsForEdit;
	protected UIColumnEx columnEx6;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text7;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textEditCampaignLandingPageDes;
	protected HtmlPanelGroup groupEditCampaignsFooterGroup;
	protected HtmlInputTextarea textareaForceEZPayTermsText;
	protected HtmlPanelGrid gridForceEZPayTermsTextHeaderGrid;
	protected HtmlOutputText text2;
	protected HtmlBfPanel bfpanelOfferOverridesPanel;
	protected HtmlPanelGrid gridKeycodeOverrideGrid;
	protected HtmlSelectBooleanCheckbox checkbox1;
	protected HtmlOutputText textOverrideDefaultLabel;
	protected HtmlOutputText text4;
	protected HtmlPanelGrid gridKeycodeOverrideSetting;
	protected HtmlSelectBooleanCheckbox checkbox2;
	protected HtmlOutputText textKeycodeOverrideLabel;
	protected HtmlPanelGroup group2;
	protected HtmlOutputText textKeycodeOverrideGridHeaderLabel;
	protected HtmlJspPanel jspPanel22222;
	protected HtmlOutputText textCurrentPromoTextValue;
	protected HtmlInputTextarea textareaRawHtmlHeaderPromo;
	protected HtmlCommandExButton buttonApplyRawChanges;
	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlPanelGroup getGroupHeaderGroup() {
		if (groupHeaderGroup == null) {
			groupHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupHeaderGroup");
		}
		return groupHeaderGroup;
	}

	protected HtmlOutputText getTextPageHeaderText() {
		if (textPageHeaderText == null) {
			textPageHeaderText = (HtmlOutputText) findComponentInRoot("textPageHeaderText");
		}
		return textPageHeaderText;
	}

	protected HtmlPanelLayout getLayoutInnerPromotionalText() {
		if (layoutInnerPromotionalText == null) {
			layoutInnerPromotionalText = (HtmlPanelLayout) findComponentInRoot("layoutInnerPromotionalText");
		}
		return layoutInnerPromotionalText;
	}

	protected HtmlOutputText getTextPromoTextMsg() {
		if (textPromoTextMsg == null) {
			textPromoTextMsg = (HtmlOutputText) findComponentInRoot("textPromoTextMsg");
		}
		return textPromoTextMsg;
	}

	protected HtmlPanelLayout getLayoutInnerTermsandConditionsLayout() {
		if (layoutInnerTermsandConditionsLayout == null) {
			layoutInnerTermsandConditionsLayout = (HtmlPanelLayout) findComponentInRoot("layoutInnerTermsandConditionsLayout");
		}
		return layoutInnerTermsandConditionsLayout;
	}

	protected HtmlOutputText getTextTermsConditionsTextMsg() {
		if (textTermsConditionsTextMsg == null) {
			textTermsConditionsTextMsg = (HtmlOutputText) findComponentInRoot("textTermsConditionsTextMsg");
		}
		return textTermsConditionsTextMsg;
	}

	protected HtmlCommandExButton getTabbedPanelTermsTextAndImages_back() {
		if (tabbedPanelTermsTextAndImages_back == null) {
			tabbedPanelTermsTextAndImages_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanelTermsTextAndImages_back");
		}
		return tabbedPanelTermsTextAndImages_back;
	}

	protected HtmlCommandExButton getTabbedPanelTermsTextAndImages_next() {
		if (tabbedPanelTermsTextAndImages_next == null) {
			tabbedPanelTermsTextAndImages_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanelTermsTextAndImages_next");
		}
		return tabbedPanelTermsTextAndImages_next;
	}

	protected HtmlCommandExButton getTabbedPanelTermsTextAndImages_finish() {
		if (tabbedPanelTermsTextAndImages_finish == null) {
			tabbedPanelTermsTextAndImages_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanelTermsTextAndImages_finish");
		}
		return tabbedPanelTermsTextAndImages_finish;
	}

	protected HtmlCommandExButton getTabbedPanelTermsTextAndImages_cancel() {
		if (tabbedPanelTermsTextAndImages_cancel == null) {
			tabbedPanelTermsTextAndImages_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanelTermsTextAndImages_cancel");
		}
		return tabbedPanelTermsTextAndImages_cancel;
	}

	protected HtmlCommandExButton getButtonApplyChanges() {
		if (buttonApplyChanges == null) {
			buttonApplyChanges = (HtmlCommandExButton) findComponentInRoot("buttonApplyChanges");
		}
		return buttonApplyChanges;
	}

	protected HtmlPanelGrid getGridCurrentSettings() {
		if (gridCurrentSettings == null) {
			gridCurrentSettings = (HtmlPanelGrid) findComponentInRoot("gridCurrentSettings");
		}
		return gridCurrentSettings;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlOutputText getTextCurrentSettingLabel() {
		if (textCurrentSettingLabel == null) {
			textCurrentSettingLabel = (HtmlOutputText) findComponentInRoot("textCurrentSettingLabel");
		}
		return textCurrentSettingLabel;
	}

	protected HtmlOutputText getTextPreviewPromoText() {
		if (textPreviewPromoText == null) {
			textPreviewPromoText = (HtmlOutputText) findComponentInRoot("textPreviewPromoText");
		}
		return textPreviewPromoText;
	}

	protected HtmlOutputText getTextCurrentForceEZPayTermsLabel() {
		if (textCurrentForceEZPayTermsLabel == null) {
			textCurrentForceEZPayTermsLabel = (HtmlOutputText) findComponentInRoot("textCurrentForceEZPayTermsLabel");
		}
		return textCurrentForceEZPayTermsLabel;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlGraphicImageEx getImageExCampaignListImage() {
		if (imageExCampaignListImage == null) {
			imageExCampaignListImage = (HtmlGraphicImageEx) findComponentInRoot("imageExCampaignListImage");
		}
		return imageExCampaignListImage;
	}

	protected HtmlPanelGrid getGridCurrentPromoLabelGrid() {
		if (gridCurrentPromoLabelGrid == null) {
			gridCurrentPromoLabelGrid = (HtmlPanelGrid) findComponentInRoot("gridCurrentPromoLabelGrid");
		}
		return gridCurrentPromoLabelGrid;
	}

	protected HtmlGraphicImageEx getImageExCurrentPromoText1DetailsImage() {
		if (imageExCurrentPromoText1DetailsImage == null) {
			imageExCurrentPromoText1DetailsImage = (HtmlGraphicImageEx) findComponentInRoot("imageExCurrentPromoText1DetailsImage");
		}
		return imageExCurrentPromoText1DetailsImage;
	}

	protected HtmlPanelGrid getGridCurrentForceEZPayTermsGrid() {
		if (gridCurrentForceEZPayTermsGrid == null) {
			gridCurrentForceEZPayTermsGrid = (HtmlPanelGrid) findComponentInRoot("gridCurrentForceEZPayTermsGrid");
		}
		return gridCurrentForceEZPayTermsGrid;
	}

	protected HtmlOutputText getTextCurrentForceEZPayTermsText() {
		if (textCurrentForceEZPayTermsText == null) {
			textCurrentForceEZPayTermsText = (HtmlOutputText) findComponentInRoot("textCurrentForceEZPayTermsText");
		}
		return textCurrentForceEZPayTermsText;
	}

	protected HtmlTabbedPanel getTabbedPanelTermsTextAndImages() {
		if (tabbedPanelTermsTextAndImages == null) {
			tabbedPanelTermsTextAndImages = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelTermsTextAndImages");
		}
		return tabbedPanelTermsTextAndImages;
	}

	protected HtmlBfPanel getBfpanelHeaderPromoText() {
		if (bfpanelHeaderPromoText == null) {
			bfpanelHeaderPromoText = (HtmlBfPanel) findComponentInRoot("bfpanelHeaderPromoText");
		}
		return bfpanelHeaderPromoText;
	}

	protected UIRichTextEditor getRichTextEditorTermsAndConditionsText() {
		if (richTextEditorTermsAndConditionsText == null) {
			richTextEditorTermsAndConditionsText = (UIRichTextEditor) findComponentInRoot("richTextEditorTermsAndConditionsText");
		}
		return richTextEditorTermsAndConditionsText;
	}

	protected HtmlBfPanel getBfpanelTermsAndConditions() {
		if (bfpanelTermsAndConditions == null) {
			bfpanelTermsAndConditions = (HtmlBfPanel) findComponentInRoot("bfpanelTermsAndConditions");
		}
		return bfpanelTermsAndConditions;
	}

	protected HtmlButtonPanel getBpCustomButtonPanel() {
		if (bpCustomButtonPanel == null) {
			bpCustomButtonPanel = (HtmlButtonPanel) findComponentInRoot("bpCustomButtonPanel");
		}
		return bpCustomButtonPanel;
	}

	protected UIRichTextEditor getRichTextEditor1() {
		if (richTextEditor1 == null) {
			richTextEditor1 = (UIRichTextEditor) findComponentInRoot("richTextEditor1");
		}
		return richTextEditor1;
	}

	public String doButtonApplyChangesAction() {
		
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		editor.setChanged(true);
		
		EditCampaignDetailsHandler campaignsHandler = this.getEdittingCampaignsHandler();

		EditableHTMLPromotionIntf newPromoText1 = null;
		EditableHTMLPromotionIntf newDisclaimerText = null;
		EditableHTMLPromotionIntf newForceEZPayTermsText = null;
		
		// update U/I with current settings
		String customPromoText1 = editor.getCustomOnePagePromoText1();
		if (customPromoText1 != null && customPromoText1.trim().length() > 0) {
			editor.setCurrentOnePagePromoText1(customPromoText1);
			editor.setCurrentOnePagePromoText1Raw(customPromoText1);
		}
		else {
			editor.setCurrentOnePagePromoText1("");
			editor.setCurrentOnePagePromoText1Raw("");
		}
		String customDisclaimerText = editor.getCustomOnePageDisclaimerText();
		if (customDisclaimerText != null && customDisclaimerText.trim().length() > 0) {
			editor.setCurrentOnePageDisclaimerText(customDisclaimerText);
		}
		else {
			editor.setCurrentOnePageDisclaimerText("");
		}

		String customForceEZPayTermsText = editor.getCustomOnePageForceEZPayTerms();
		if (customForceEZPayTermsText != null && customForceEZPayTermsText.trim().length() > 0) {
			editor.setCurrentOnePageForceEZPayTermsText(customForceEZPayTermsText);
		}
		else {
			editor.setCurrentOnePageForceEZPayTermsText("");
		}

		// apply the change to the campaigns being editted
		for (CampaignHandler ch : campaignsHandler.getCampaigns()) {
			UsatCampaignIntf c = ch.getCampaign();
			
			EditablePromotionSetIntf promotionSet = c.getPromotionSet();
			
			// clear any prevous settings
			promotionSet.clearOnePageOrderConfigurations();
			
			newPromoText1 = null;
			newDisclaimerText = null;
			newForceEZPayTermsText = null;
			
			if (customPromoText1 != null && customPromoText1.trim().length() > 0) {
				try {
					newPromoText1 = new EditableHTMLPromotionBO();
					newPromoText1.setType(PromotionIntf.ONE_PAGE_ORDER_ENTRY_PROMO_TEXT_1);
					newPromoText1.setPromotionalHTML(customPromoText1.trim());
				}
				catch (Exception e) {
					FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Promotional Text: " + e.getMessage() , null);
					this.getFacesContext().addMessage(null, m);
					e.printStackTrace();
					newPromoText1 = null;
					break; // stop the loop
				}
			}
			promotionSet.setOnePagePromoSpot1Text(newPromoText1);
			
			if (customDisclaimerText != null && customDisclaimerText.trim().length() > 0) {
				try {
					newDisclaimerText = new EditableHTMLPromotionBO();
					newDisclaimerText.setType(PromotionIntf.ONE_PAGE_DISCLAIMER_TEXT);
					newDisclaimerText.setPromotionalHTML(customDisclaimerText.trim());
				}
				catch (Exception e) {
					FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Disclaimer Text: " + e.getMessage() , null);
					this.getFacesContext().addMessage(null, m);
					e.printStackTrace();
					newDisclaimerText = null;
					break; // stop the loop
				}
			}
			promotionSet.setOnePageDisclaimerText(newDisclaimerText);

			if (customForceEZPayTermsText != null && customForceEZPayTermsText.trim().length() > 0) {
				try {
					newForceEZPayTermsText = new EditableHTMLPromotionBO();
					newForceEZPayTermsText.setType(PromotionIntf.ONE_PAGE_FORCE_EZPAY_TERMS_TEXT);
					newForceEZPayTermsText.setPromotionalHTML(customForceEZPayTermsText.trim());
				}
				catch (Exception e) {
					FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Force EZ-PAY Terms Text: " + e.getMessage() , null);
					this.getFacesContext().addMessage(null, m);
					e.printStackTrace();
					newForceEZPayTermsText = null;
					break; // stop the loop
				}
			}
			promotionSet.setOnePageForceEZPayTermsText(newForceEZPayTermsText);

			EditablePromotionBO newSetting = null;
			if (editor.isOverrideDefaultAllowOfferOverride()) {
				// if overriding default
				try {
					newSetting = new EditablePromotionBO();
					newSetting.setType(PromotionIntf.ALLOW_OFFER_OVERRIDE);
					if (editor.isCustomAllowOfferOverrideSetting()) {
						// if allowing
						newSetting.setFulfillText("Y");
					}
					else {
						// not allowing
						newSetting.setFulfillText("N");
					}
				} // end try
				catch (Exception e) {
					FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Allow Offer Overrides Setting: " + e.getMessage() , null);
					this.getFacesContext().addMessage(null, m);
					e.printStackTrace();
					break; // stop the loop
				}
				promotionSet.setAllowOfferCodeOverrides(newSetting);
			}
			
		} // end for campaigns being editted
		
		return "";

	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getManagedBean("campaignDetailEditsHandler");
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getManagedBean("edittingCampaignsHandler");
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	protected HtmlCommandLink getLinkClearPromoText() {
		if (linkClearPromoText == null) {
			linkClearPromoText = (HtmlCommandLink) findComponentInRoot("linkClearPromoText");
		}
		return linkClearPromoText;
	}

	protected HtmlOutputText getTextClearPromoTextLabel() {
		if (textClearPromoTextLabel == null) {
			textClearPromoTextLabel = (HtmlOutputText) findComponentInRoot("textClearPromoTextLabel");
		}
		return textClearPromoTextLabel;
	}

	public String doLinkClearPromoTextAction() {
		// Type Java code that runs when the component is clicked
	
		this.getCampaignDetailEditsHandler().setCustomOnePagePromoText1(null);
		this.getCampaignDetailEditsHandler().setCurrentOnePagePromoText1("DEFAULT VALUE WILL BE USED");
		return "success";
	}

	protected HtmlOutputText getTextDefaultPromotTextLabel() {
		if (textDefaultPromotTextLabel == null) {
			textDefaultPromotTextLabel = (HtmlOutputText) findComponentInRoot("textDefaultPromotTextLabel");
		}
		return textDefaultPromotTextLabel;
	}

	protected HtmlOutputText getTextDefaultTopPromoText() {
		if (textDefaultTopPromoText == null) {
			textDefaultTopPromoText = (HtmlOutputText) findComponentInRoot("textDefaultTopPromoText");
		}
		return textDefaultTopPromoText;
	}

	protected HtmlOutputSeparator getSeparator1() {
		if (separator1 == null) {
			separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
		}
		return separator1;
	}

	protected HtmlOutputText getTextDefaultForceEZPayTermsLabel() {
		if (textDefaultForceEZPayTermsLabel == null) {
			textDefaultForceEZPayTermsLabel = (HtmlOutputText) findComponentInRoot("textDefaultForceEZPayTermsLabel");
		}
		return textDefaultForceEZPayTermsLabel;
	}

	protected HtmlOutputText getTextDefaultForceEZPayTermsText() {
		if (textDefaultForceEZPayTermsText == null) {
			textDefaultForceEZPayTermsText = (HtmlOutputText) findComponentInRoot("textDefaultForceEZPayTermsText");
		}
		return textDefaultForceEZPayTermsText;
	}

	protected HtmlCommandLink getLinkClearDisclaimerText() {
		if (linkClearDisclaimerText == null) {
			linkClearDisclaimerText = (HtmlCommandLink) findComponentInRoot("linkClearDisclaimerText");
		}
		return linkClearDisclaimerText;
	}

	protected HtmlOutputText getTextClearDisclaimerLabel() {
		if (textClearDisclaimerLabel == null) {
			textClearDisclaimerLabel = (HtmlOutputText) findComponentInRoot("textClearDisclaimerLabel");
		}
		return textClearDisclaimerLabel;
	}

	public String doLinkClearDisclaimerTextAction() {
		// Type Java code that runs when the component is clicked
	
		this.getCampaignDetailEditsHandler().setCustomOnePageDisclaimerText(null);
		this.getCampaignDetailEditsHandler().setCurrentOnePageDisclaimerText("DEFAULT VALUE WILL BE USED");
		return "";
	}

	protected HtmlBfPanel getBfpanelForceEZPayTermsText() {
		if (bfpanelForceEZPayTermsText == null) {
			bfpanelForceEZPayTermsText = (HtmlBfPanel) findComponentInRoot("bfpanelForceEZPayTermsText");
		}
		return bfpanelForceEZPayTermsText;
	}

	protected HtmlPanelLayout getLayoutInnerForceEZPayTermsLayout() {
		if (layoutInnerForceEZPayTermsLayout == null) {
			layoutInnerForceEZPayTermsLayout = (HtmlPanelLayout) findComponentInRoot("layoutInnerForceEZPayTermsLayout");
		}
		return layoutInnerForceEZPayTermsLayout;
	}

	protected HtmlCommandLink getLinkClearForceEZPayTermsText() {
		if (linkClearForceEZPayTermsText == null) {
			linkClearForceEZPayTermsText = (HtmlCommandLink) findComponentInRoot("linkClearForceEZPayTermsText");
		}
		return linkClearForceEZPayTermsText;
	}

	protected HtmlOutputText getTextClearForceEZPayTermsLabel() {
		if (textClearForceEZPayTermsLabel == null) {
			textClearForceEZPayTermsLabel = (HtmlOutputText) findComponentInRoot("textClearForceEZPayTermsLabel");
		}
		return textClearForceEZPayTermsLabel;
	}

	protected HtmlOutputText getTextForceEZPayTermsTextMsg() {
		if (textForceEZPayTermsTextMsg == null) {
			textForceEZPayTermsTextMsg = (HtmlOutputText) findComponentInRoot("textForceEZPayTermsTextMsg");
		}
		return textForceEZPayTermsTextMsg;
	}

	protected HtmlOutputSeparator getSeparator2() {
		if (separator2 == null) {
			separator2 = (HtmlOutputSeparator) findComponentInRoot("separator2");
		}
		return separator2;
	}

	protected HtmlOutputText getTextDefaultDisclaimerTextLabel() {
		if (textDefaultDisclaimerTextLabel == null) {
			textDefaultDisclaimerTextLabel = (HtmlOutputText) findComponentInRoot("textDefaultDisclaimerTextLabel");
		}
		return textDefaultDisclaimerTextLabel;
	}

	protected HtmlOutputText getTextDefaultDisclaimerText() {
		if (textDefaultDisclaimerText == null) {
			textDefaultDisclaimerText = (HtmlOutputText) findComponentInRoot("textDefaultDisclaimerText");
		}
		return textDefaultDisclaimerText;
	}

	protected HtmlOutputText getTextCurrentTermsAndCondLabel() {
		if (textCurrentTermsAndCondLabel == null) {
			textCurrentTermsAndCondLabel = (HtmlOutputText) findComponentInRoot("textCurrentTermsAndCondLabel");
		}
		return textCurrentTermsAndCondLabel;
	}

	protected HtmlPanelGrid getGridCurrentTermsConditionGrid() {
		if (gridCurrentTermsConditionGrid == null) {
			gridCurrentTermsConditionGrid = (HtmlPanelGrid) findComponentInRoot("gridCurrentTermsConditionGrid");
		}
		return gridCurrentTermsConditionGrid;
	}

	protected HtmlGraphicImageEx getImageExCurrentDisclaimerTextDetailsImage() {
		if (imageExCurrentDisclaimerTextDetailsImage == null) {
			imageExCurrentDisclaimerTextDetailsImage = (HtmlGraphicImageEx) findComponentInRoot("imageExCurrentDisclaimerTextDetailsImage");
		}
		return imageExCurrentDisclaimerTextDetailsImage;
	}

	protected HtmlOutputText getTextCurrentTermsConditionsText() {
		if (textCurrentTermsConditionsText == null) {
			textCurrentTermsConditionsText = (HtmlOutputText) findComponentInRoot("textCurrentTermsConditionsText");
		}
		return textCurrentTermsConditionsText;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlOutputText getTextPromoText1NumChars() {
		if (textPromoText1NumChars == null) {
			textPromoText1NumChars = (HtmlOutputText) findComponentInRoot("textPromoText1NumChars");
		}
		return textPromoText1NumChars;
	}

	protected HtmlOutputText getTextCurrentPromoText1HTML() {
		if (textCurrentPromoText1HTML == null) {
			textCurrentPromoText1HTML = (HtmlOutputText) findComponentInRoot("textCurrentPromoText1HTML");
		}
		return textCurrentPromoText1HTML;
	}

	protected HtmlPanelDialog getDialogCurrentPromoTextDialogue() {
		if (dialogCurrentPromoTextDialogue == null) {
			dialogCurrentPromoTextDialogue = (HtmlPanelDialog) findComponentInRoot("dialogCurrentPromoTextDialogue");
		}
		return dialogCurrentPromoTextDialogue;
	}

	protected HtmlJspPanel getJspPanelRawTermCondTextModalPanel() {
		if (jspPanelRawTermCondTextModalPanel == null) {
			jspPanelRawTermCondTextModalPanel = (HtmlJspPanel) findComponentInRoot("jspPanelRawTermCondTextModalPanel");
		}
		return jspPanelRawTermCondTextModalPanel;
	}

	protected HtmlOutputText getTextTermsCondTextNumChars() {
		if (textTermsCondTextNumChars == null) {
			textTermsCondTextNumChars = (HtmlOutputText) findComponentInRoot("textTermsCondTextNumChars");
		}
		return textTermsCondTextNumChars;
	}

	protected HtmlOutputText getTextCurrentDisclaimerTextHTML() {
		if (textCurrentDisclaimerTextHTML == null) {
			textCurrentDisclaimerTextHTML = (HtmlOutputText) findComponentInRoot("textCurrentDisclaimerTextHTML");
		}
		return textCurrentDisclaimerTextHTML;
	}

	protected HtmlPanelDialog getDialogCurrentTermsConditionTextDialgue() {
		if (dialogCurrentTermsConditionTextDialgue == null) {
			dialogCurrentTermsConditionTextDialgue = (HtmlPanelDialog) findComponentInRoot("dialogCurrentTermsConditionTextDialgue");
		}
		return dialogCurrentTermsConditionTextDialgue;
	}

	protected HtmlDataTableEx getTableExSelectedCampaignsForEditV2() {
		if (tableExSelectedCampaignsForEditV2 == null) {
			tableExSelectedCampaignsForEditV2 = (HtmlDataTableEx) findComponentInRoot("tableExSelectedCampaignsForEditV2");
		}
		return tableExSelectedCampaignsForEditV2;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlOutputText getTextEditCampsDTHeader() {
		if (textEditCampsDTHeader == null) {
			textEditCampsDTHeader = (HtmlOutputText) findComponentInRoot("textEditCampsDTHeader");
		}
		return textEditCampsDTHeader;
	}

	protected HtmlInputRowSelect getRowSelectRemoveCampaignFromEdit() {
		if (rowSelectRemoveCampaignFromEdit == null) {
			rowSelectRemoveCampaignFromEdit = (HtmlInputRowSelect) findComponentInRoot("rowSelectRemoveCampaignFromEdit");
		}
		return rowSelectRemoveCampaignFromEdit;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected HtmlOutputText getTextEditPubHeader() {
		if (textEditPubHeader == null) {
			textEditPubHeader = (HtmlOutputText) findComponentInRoot("textEditPubHeader");
		}
		return textEditPubHeader;
	}

	protected HtmlOutputText getTextEditKeycodeHeader() {
		if (textEditKeycodeHeader == null) {
			textEditKeycodeHeader = (HtmlOutputText) findComponentInRoot("textEditKeycodeHeader");
		}
		return textEditKeycodeHeader;
	}

	protected HtmlOutputText getTextEditNameHeader() {
		if (textEditNameHeader == null) {
			textEditNameHeader = (HtmlOutputText) findComponentInRoot("textEditNameHeader");
		}
		return textEditNameHeader;
	}

	protected HtmlOutputText getTextEditLandingPageHeader() {
		if (textEditLandingPageHeader == null) {
			textEditLandingPageHeader = (HtmlOutputText) findComponentInRoot("textEditLandingPageHeader");
		}
		return textEditLandingPageHeader;
	}

	protected HtmlCommandExButton getButtonRemoveSelectedCampaignsFromEditCollection() {
		if (buttonRemoveSelectedCampaignsFromEditCollection == null) {
			buttonRemoveSelectedCampaignsFromEditCollection = (HtmlCommandExButton) findComponentInRoot("buttonRemoveSelectedCampaignsFromEditCollection");
		}
		return buttonRemoveSelectedCampaignsFromEditCollection;
	}

	protected HtmlForm getFormForm2() {
		if (formForm2 == null) {
			formForm2 = (HtmlForm) findComponentInRoot("formForm2");
		}
		return formForm2;
	}

	protected HtmlPanelDialog getDialogSelectedCampaignsForEdit() {
		if (dialogSelectedCampaignsForEdit == null) {
			dialogSelectedCampaignsForEdit = (HtmlPanelDialog) findComponentInRoot("dialogSelectedCampaignsForEdit");
		}
		return dialogSelectedCampaignsForEdit;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getTextEditCampaignLandingPageDes() {
		if (textEditCampaignLandingPageDes == null) {
			textEditCampaignLandingPageDes = (HtmlOutputText) findComponentInRoot("textEditCampaignLandingPageDes");
		}
		return textEditCampaignLandingPageDes;
	}

	protected HtmlPanelGroup getGroupEditCampaignsFooterGroup() {
		if (groupEditCampaignsFooterGroup == null) {
			groupEditCampaignsFooterGroup = (HtmlPanelGroup) findComponentInRoot("groupEditCampaignsFooterGroup");
		}
		return groupEditCampaignsFooterGroup;
	}

	protected HtmlInputTextarea getTextareaForceEZPayTermsText() {
		if (textareaForceEZPayTermsText == null) {
			textareaForceEZPayTermsText = (HtmlInputTextarea) findComponentInRoot("textareaForceEZPayTermsText");
		}
		return textareaForceEZPayTermsText;
	}

	protected HtmlPanelGrid getGridForceEZPayTermsTextHeaderGrid() {
		if (gridForceEZPayTermsTextHeaderGrid == null) {
			gridForceEZPayTermsTextHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridForceEZPayTermsTextHeaderGrid");
		}
		return gridForceEZPayTermsTextHeaderGrid;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	public String doLinkClearForceEZPayTermsTextAction() {
		// Type Java code that runs when the component is clicked
		this.getCampaignDetailEditsHandler().setCustomOnePageForceEZPayTerms(null);
		this.getCampaignDetailEditsHandler().setCurrentOnePageForceEZPayTermsText("DEFAULT VALUE WILL BE USED");
		return "success";
	}

	protected HtmlBfPanel getBfpanelOfferOverridesPanel() {
		if (bfpanelOfferOverridesPanel == null) {
			bfpanelOfferOverridesPanel = (HtmlBfPanel) findComponentInRoot("bfpanelOfferOverridesPanel");
		}
		return bfpanelOfferOverridesPanel;
	}

	protected HtmlPanelGrid getGridKeycodeOverrideGrid() {
		if (gridKeycodeOverrideGrid == null) {
			gridKeycodeOverrideGrid = (HtmlPanelGrid) findComponentInRoot("gridKeycodeOverrideGrid");
		}
		return gridKeycodeOverrideGrid;
	}

	protected HtmlSelectBooleanCheckbox getCheckbox1() {
		if (checkbox1 == null) {
			checkbox1 = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkbox1");
		}
		return checkbox1;
	}

	protected HtmlOutputText getTextOverrideDefaultLabel() {
		if (textOverrideDefaultLabel == null) {
			textOverrideDefaultLabel = (HtmlOutputText) findComponentInRoot("textOverrideDefaultLabel");
		}
		return textOverrideDefaultLabel;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlPanelGrid getGridKeycodeOverrideSetting() {
		if (gridKeycodeOverrideSetting == null) {
			gridKeycodeOverrideSetting = (HtmlPanelGrid) findComponentInRoot("gridKeycodeOverrideSetting");
		}
		return gridKeycodeOverrideSetting;
	}

	protected HtmlSelectBooleanCheckbox getCheckbox2() {
		if (checkbox2 == null) {
			checkbox2 = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkbox2");
		}
		return checkbox2;
	}

	protected HtmlOutputText getTextKeycodeOverrideLabel() {
		if (textKeycodeOverrideLabel == null) {
			textKeycodeOverrideLabel = (HtmlOutputText) findComponentInRoot("textKeycodeOverrideLabel");
		}
		return textKeycodeOverrideLabel;
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlOutputText getTextKeycodeOverrideGridHeaderLabel() {
		if (textKeycodeOverrideGridHeaderLabel == null) {
			textKeycodeOverrideGridHeaderLabel = (HtmlOutputText) findComponentInRoot("textKeycodeOverrideGridHeaderLabel");
		}
		return textKeycodeOverrideGridHeaderLabel;
	}

	protected HtmlJspPanel getJspPanel22222() {
		if (jspPanel22222 == null) {
			jspPanel22222 = (HtmlJspPanel) findComponentInRoot("jspPanel22222");
		}
		return jspPanel22222;
	}

	protected HtmlOutputText getTextCurrentPromoTextValue() {
		if (textCurrentPromoTextValue == null) {
			textCurrentPromoTextValue = (HtmlOutputText) findComponentInRoot("textCurrentPromoTextValue");
		}
		return textCurrentPromoTextValue;
	}

	protected HtmlInputTextarea getTextareaRawHtmlHeaderPromo() {
		if (textareaRawHtmlHeaderPromo == null) {
			textareaRawHtmlHeaderPromo = (HtmlInputTextarea) findComponentInRoot("textareaRawHtmlHeaderPromo");
		}
		return textareaRawHtmlHeaderPromo;
	}

	protected HtmlCommandExButton getButtonApplyRawChanges() {
		if (buttonApplyRawChanges == null) {
			buttonApplyRawChanges = (HtmlCommandExButton) findComponentInRoot("buttonApplyRawChanges");
		}
		return buttonApplyRawChanges;
	}

	public String doButtonApplyRawChangesAction() {
		// This is java code that runs when this action method is invoked
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		
		//editor.setChanged(true);
		
		//EditCampaignDetailsHandler campaignsHandler = this.getEdittingCampaignsHandler();

		//EditableHTMLPromotionIntf newPromoText1 = null;
		
		// update U/I with current settings
		String rawText = this.getCampaignDetailEditsHandler().getCurrentOnePagePromoText1Raw();
		
		if (rawText != null && rawText.trim().length() > 0) {
			editor.setCustomOnePagePromoText1(rawText);
		}
		else {
			editor.setCustomOnePagePromoText1("");
		}

		return "success";
	}

}