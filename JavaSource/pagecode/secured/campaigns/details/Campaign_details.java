/**
 * 
 */
package pagecode.secured.campaigns.details;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignVanityHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlMessages;

/**
 * @author aeast
 *
 */
public class Campaign_details extends PageCodeBase {

	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected EditCampaignVanityHandler edittingCampaignsVanityHandler;
	protected HtmlScriptCollector scriptCollectorCampaignDetailsCollector;
	protected HtmlForm formCampaignDetails;
	protected HtmlPanelSection sectionCampaignsSection;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageExClosed;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageExOpened;
	protected HtmlOutputText textSelectedCampaignsLabel1;
	protected HtmlOutputText textSelectedCampaignsLabel;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text3;
	protected HtmlDataTableEx tableExCampaigns;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text5;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text6;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text2;
	protected HtmlOutputText text9;
	protected HtmlOutputLinkEx linkExBackToSearchPage;
	protected HtmlOutputText text10;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text12;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText text13;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText text14;
	protected UIColumnEx columnEx9;
	protected HtmlOutputText textCampaignType;
	protected HtmlOutputText textCampaignLandingPage;
	protected HtmlOutputText textCampaignCreator;
	protected HtmlOutputText textCreatedTime;
	protected HtmlOutputText textCampaignLastUpdater;
	protected HtmlOutputText text20;
	protected UIColumnEx columnEx10;
	protected HtmlOutputText textUpdateTime;
	protected HtmlPanelBox box1;
	protected HtmlPanelGrid gridEditCampaignDTHeaderGrid;
	protected HtmlOutputText textSelCampHeaderInfoText;
	protected HtmlGraphicImageEx imageExInfoImage1;
	protected HtmlOutputText text15;
	protected UIColumnEx columnEx12;
	protected HtmlOutputText textEditCampaignPubStatus;
	protected HtmlJspPanel jspPanelCampEditJumpPanel;
	protected CampaignHandler editSelectedCampaign;
	protected UIParameter param2;
	protected HtmlCommandExButton buttonPublishChanges;
	protected HtmlCommandExButton buttonClearAllChanges;
	protected CampaignHandler selectedItems;
	protected CampaignHandler editCampaignDetails;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected HtmlMessages messages1;
	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	
	protected EditCampaignVanityHandler getEdittingCampaignsVanityHandler() {
		if (edittingCampaignsVanityHandler == null) {
			edittingCampaignsVanityHandler = (EditCampaignVanityHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsVanityHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsVanityHandler;
	}

	
	
	
	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	protected HtmlScriptCollector getScriptCollectorCampaignDetailsCollector() {
		if (scriptCollectorCampaignDetailsCollector == null) {
			scriptCollectorCampaignDetailsCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorCampaignDetailsCollector");
		}
		return scriptCollectorCampaignDetailsCollector;
	}

	protected HtmlForm getFormCampaignDetails() {
		if (formCampaignDetails == null) {
			formCampaignDetails = (HtmlForm) findComponentInRoot("formCampaignDetails");
		}
		return formCampaignDetails;
	}

	protected HtmlPanelSection getSectionCampaignsSection() {
		if (sectionCampaignsSection == null) {
			sectionCampaignsSection = (HtmlPanelSection) findComponentInRoot("sectionCampaignsSection");
		}
		return sectionCampaignsSection;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlGraphicImageEx getImageExClosed() {
		if (imageExClosed == null) {
			imageExClosed = (HtmlGraphicImageEx) findComponentInRoot("imageExClosed");
		}
		return imageExClosed;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlGraphicImageEx getImageExOpened() {
		if (imageExOpened == null) {
			imageExOpened = (HtmlGraphicImageEx) findComponentInRoot("imageExOpened");
		}
		return imageExOpened;
	}

	protected HtmlOutputText getTextSelectedCampaignsLabel1() {
		if (textSelectedCampaignsLabel1 == null) {
			textSelectedCampaignsLabel1 = (HtmlOutputText) findComponentInRoot("textSelectedCampaignsLabel1");
		}
		return textSelectedCampaignsLabel1;
	}

	protected HtmlOutputText getTextSelectedCampaignsLabel() {
		if (textSelectedCampaignsLabel == null) {
			textSelectedCampaignsLabel = (HtmlOutputText) findComponentInRoot("textSelectedCampaignsLabel");
		}
		return textSelectedCampaignsLabel;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlDataTableEx getTableExCampaigns() {
		if (tableExCampaigns == null) {
			tableExCampaigns = (HtmlDataTableEx) findComponentInRoot("tableExCampaigns");
		}
		return tableExCampaigns;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlOutputLinkEx getLinkExBackToSearchPage() {
		if (linkExBackToSearchPage == null) {
			linkExBackToSearchPage = (HtmlOutputLinkEx) findComponentInRoot("linkExBackToSearchPage");
		}
		return linkExBackToSearchPage;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected UIColumnEx getColumnEx9() {
		if (columnEx9 == null) {
			columnEx9 = (UIColumnEx) findComponentInRoot("columnEx9");
		}
		return columnEx9;
	}

	protected HtmlOutputText getTextCampaignType() {
		if (textCampaignType == null) {
			textCampaignType = (HtmlOutputText) findComponentInRoot("textCampaignType");
		}
		return textCampaignType;
	}

	protected HtmlOutputText getTextCampaignLandingPage() {
		if (textCampaignLandingPage == null) {
			textCampaignLandingPage = (HtmlOutputText) findComponentInRoot("textCampaignLandingPage");
		}
		return textCampaignLandingPage;
	}

	protected HtmlOutputText getTextCampaignCreator() {
		if (textCampaignCreator == null) {
			textCampaignCreator = (HtmlOutputText) findComponentInRoot("textCampaignCreator");
		}
		return textCampaignCreator;
	}

	protected HtmlOutputText getTextCreatedTime() {
		if (textCreatedTime == null) {
			textCreatedTime = (HtmlOutputText) findComponentInRoot("textCreatedTime");
		}
		return textCreatedTime;
	}

	protected HtmlOutputText getTextCampaignLastUpdater() {
		if (textCampaignLastUpdater == null) {
			textCampaignLastUpdater = (HtmlOutputText) findComponentInRoot("textCampaignLastUpdater");
		}
		return textCampaignLastUpdater;
	}

	protected HtmlOutputText getText20() {
		if (text20 == null) {
			text20 = (HtmlOutputText) findComponentInRoot("text20");
		}
		return text20;
	}

	protected UIColumnEx getColumnEx10() {
		if (columnEx10 == null) {
			columnEx10 = (UIColumnEx) findComponentInRoot("columnEx10");
		}
		return columnEx10;
	}

	protected HtmlOutputText getTextUpdateTime() {
		if (textUpdateTime == null) {
			textUpdateTime = (HtmlOutputText) findComponentInRoot("textUpdateTime");
		}
		return textUpdateTime;
	}

	public String doRowAction1Action() {
		
	//Get CID parm of selected Campaign
	Object paramValue1 = getRequestParam().get("CID").toString();
		
	boolean hasErrors = false;
	
	//Collection to hold selected campaign
	ArrayList<CampaignHandler> editSelected = new ArrayList<CampaignHandler> ();
		
	String CID = " ";
		for( CampaignHandler campaign : this.getEdittingCampaignsHandler().getCampaigns()) {
			CID = String.valueOf(campaign.getCampaignID());
			
		
			if (paramValue1.equals(CID)) {
				//add campaign to Bean for use in the Standard Vanity page
				try {		
					editSelected.add(campaign);
					hasErrors = false;
				//	
				}
				//Catch if there are errors
				catch (Exception e) {
					hasErrors = true;
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Campaign: " + campaign.getCampaignName() + " not added successfully.  Contact IT support.");
					getFacesContext().addMessage(null, message);
				}
			}
		}
		if (hasErrors) {
			return "failure";
		}
		else {
			// forward to Standard Vanity page		
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "");
			getFacesContext().addMessage(null, message);
			this.getEdittingCampaignsVanityHandler().setCampaigns(editSelected);
			return "success";		
		}
		
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlPanelGrid getGridEditCampaignDTHeaderGrid() {
		if (gridEditCampaignDTHeaderGrid == null) {
			gridEditCampaignDTHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridEditCampaignDTHeaderGrid");
		}
		return gridEditCampaignDTHeaderGrid;
	}

	protected HtmlOutputText getTextSelCampHeaderInfoText() {
		if (textSelCampHeaderInfoText == null) {
			textSelCampHeaderInfoText = (HtmlOutputText) findComponentInRoot("textSelCampHeaderInfoText");
		}
		return textSelCampHeaderInfoText;
	}

	protected HtmlGraphicImageEx getImageExInfoImage1() {
		if (imageExInfoImage1 == null) {
			imageExInfoImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageExInfoImage1");
		}
		return imageExInfoImage1;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected UIColumnEx getColumnEx12() {
		if (columnEx12 == null) {
			columnEx12 = (UIColumnEx) findComponentInRoot("columnEx12");
		}
		return columnEx12;
	}

	protected HtmlOutputText getTextEditCampaignPubStatus() {
		if (textEditCampaignPubStatus == null) {
			textEditCampaignPubStatus = (HtmlOutputText) findComponentInRoot("textEditCampaignPubStatus");
		}
		return textEditCampaignPubStatus;
	}

	protected HtmlJspPanel getJspPanelCampEditJumpPanel() {
		if (jspPanelCampEditJumpPanel == null) {
			jspPanelCampEditJumpPanel = (HtmlJspPanel) findComponentInRoot("jspPanelCampEditJumpPanel");
		}
		return jspPanelCampEditJumpPanel;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getEditSelectedCampaign() {
		if (editSelectedCampaign == null) {
			editSelectedCampaign = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{editSelectedCampaign}").getValue(
							getFacesContext());
		}
		return editSelectedCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEditSelectedCampaign(CampaignHandler editSelectedCampaign) {
		this.editSelectedCampaign = editSelectedCampaign;
	}

	protected UIParameter getParam2() {
		if (param2 == null) {
			param2 = (UIParameter) findComponentInRoot("param2");
		}
		return param2;
	}

	protected HtmlCommandExButton getButtonPublishChanges() {
		if (buttonPublishChanges == null) {
			buttonPublishChanges = (HtmlCommandExButton) findComponentInRoot("buttonPublishChanges");
		}
		return buttonPublishChanges;
	}

	protected HtmlCommandExButton getButtonClearAllChanges() {
		if (buttonClearAllChanges == null) {
			buttonClearAllChanges = (HtmlCommandExButton) findComponentInRoot("buttonClearAllChanges");
		}
		return buttonClearAllChanges;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getSelectedItems() {
		if (selectedItems == null) {
			selectedItems = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding("#{selectedItems}")
					.getValue(getFacesContext());
		}
		return selectedItems;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSelectedItems(CampaignHandler selectedItems) {
		this.selectedItems = selectedItems;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandler getEditCampaignDetails() {
		if (editCampaignDetails == null) {
			editCampaignDetails = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{editCampaignDetails}").getValue(
							getFacesContext());
		}
		return editCampaignDetails;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEditCampaignDetails(CampaignHandler editCampaignDetails) {
		this.editCampaignDetails = editCampaignDetails;
	}

	public String doButtonPublishChangesAction() {
	
		String responseString = "success";
		
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		if (editor.isChanged()) {
			// add a message to save changes first
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You have unsaved changes. Please Save Changes or Discard Unsaved Changes Before Publishing.", "");
			getFacesContext().addMessage(null, message);
			responseString = "failure";
		}
		else {
			// set up collection for publishing
			Collection<CampaignHandler> campaigns = this.getEdittingCampaignsHandler().getCampaigns();
			for(CampaignHandler ch : campaigns) {
				ch.setCampaignSelected(true);
			}
			this.getPublishingCampaignsHandler().setCollectionData(this.getEdittingCampaignsHandler().getCampaigns());
		}
		return responseString;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{publishingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return publishingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}

	public String doButtonClearAllChangesAction() {
		// Type Java code that runs when the component is clicked
	
		String responseString = "success";
		
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		
		CampaignHandler sourceCampaign = editor.getSourceCampaign();
		if (editor.isChanged()) {
			
			EditCampaignDetailsHandler campaigns = this.getEdittingCampaignsHandler();
			ArrayList<CampaignHandler> reloadedCollection = new ArrayList<CampaignHandler>(campaigns.getCampaigns().size());
			
			boolean errors = false;
			String errorDetail = "";
			for (CampaignHandler ch : campaigns.getCampaigns()) {
				try {
					UsatCampaignIntf reloadedCampaign = UsatCampaignBO.fetchCampaign(ch.getCampaignID());
					if (reloadedCampaign != null) {
						CampaignHandler chTemp = new CampaignHandler();
						chTemp.setCampaign(reloadedCampaign);
						
						reloadedCollection.add(chTemp);

						if (ch.getCampaignID() == sourceCampaign.getCampaignID()) {
							sourceCampaign = chTemp;
						}
					}
				}
				catch (Exception e) {
					System.out.println("Failed to reload campaign (Discard changes on edit details screens): " + e.getMessage());
					errorDetail = e.getMessage();
					errors = true;
				}
			}
			
			campaigns.setCampaigns(reloadedCollection);
			editor.setChanged(false);
			// reset the source campaign to the new object
			editor.setSourceCampaign(sourceCampaign);
			
			if (!errors) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "All Changes since the last Save were cleared.", "");
				getFacesContext().addMessage(null, message);
			}
			else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unable to reload all campaigns. Recommend starting back at the Advanced Search Page. Error Detail: " + errorDetail, "");
				getFacesContext().addMessage(null, message);
			}
		}
		else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "No Changes were detected.", "");
			getFacesContext().addMessage(null, message);
		}
		return responseString;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

}