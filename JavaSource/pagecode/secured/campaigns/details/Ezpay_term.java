/**
 * 
 */
package pagecode.secured.campaigns.details;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItem;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneRadio;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usatoday.adminportal.campaigns.RelRateBO;
import com.gannett.usatoday.adminportal.campaigns.intf.RelRtCodeIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditablePromotionBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import javax.faces.component.UIParameter;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

/**
 * @author aeast
 *
 */
public class Ezpay_term extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlPanelGrid gridMainGrid;
	protected HtmlPanelGrid gridLeftGrid;
	protected HtmlPanelGrid gridRightGrid;
	protected HtmlForm form1;
	protected HtmlSelectOneRadio radioOverrideOptionsRadio;
	protected UISelectItem selectItem1;
	protected UISelectItem selectItem2;
	protected UISelectItem selectItem3;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlPanelFormBox formBox1;
	protected HtmlFormItem formItemRateCodeCheck;
	protected HtmlInputText textRateCodeToCheck;
	protected HtmlPanelGrid gridRateCodeInfoGrid;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlPanelGrid gridBottomeGrid;
	protected HtmlCommandExButton buttonCheckRate;
	protected HtmlPanelGroup group1;
	protected HtmlOutputText textOptionSelectionText;
	protected HtmlMessages messages1;
	protected HtmlCommandExButton buttonUseRate;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected HtmlPanelGroup groupDefaultForceEZPAYFooterGroup;
	protected HtmlCommandExButton button2;
	protected HtmlBehavior behavior2;
	protected HtmlBehavior behavior1;
	protected HtmlPanelDialog dialogDefaultForceEZPayDialog;
	protected HtmlCommandExButton button1;
	protected HtmlPanelLayout layout1;
	protected HtmlDataTableEx tableExDefaultForceEzpayOptionsTable;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text10;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text12;
	protected HtmlOutputText text13;
	protected HtmlOutputText text14;
	protected HtmlOutputText text15;
	protected HtmlOutputText text8;
	protected HtmlOutputLinkEx linkExShowDefaults;
	protected HtmlCommandExButton buttonApplySetting;
	protected HtmlPanelGroup group2;
	protected HtmlDataTableEx tableExSelectedCampaignsForEditV2;
	protected HtmlPanelBox box1;
	protected HtmlOutputText textEditCampsDTHeader;
	protected HtmlInputRowSelect rowSelectRemoveCampaignFromEdit;
	protected UIParameter param1;
	protected HtmlOutputText textEditPubHeader;
	protected HtmlOutputText textEditKeycodeHeader;
	protected HtmlOutputText textEditNameHeader;
	protected HtmlOutputText textEditLandingPageHeader;
	protected HtmlCommandExButton buttonRemoveSelectedCampaignsFromEditCollection;
	protected HtmlForm formForm2;
	protected HtmlPanelDialog dialogSelectedCampaignsForEdit;
	protected UIColumnEx columnEx6;
	protected UIColumnEx columnEx4;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText textEditCampaignLandingPageDes;
	protected HtmlPanelGroup groupEditCampaignsFooterGroup;
	protected HtmlGraphicImageEx imageExCampaignListImage;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlPanelGrid getGridMainGrid() {
		if (gridMainGrid == null) {
			gridMainGrid = (HtmlPanelGrid) findComponentInRoot("gridMainGrid");
		}
		return gridMainGrid;
	}

	protected HtmlPanelGrid getGridLeftGrid() {
		if (gridLeftGrid == null) {
			gridLeftGrid = (HtmlPanelGrid) findComponentInRoot("gridLeftGrid");
		}
		return gridLeftGrid;
	}

	protected HtmlPanelGrid getGridRightGrid() {
		if (gridRightGrid == null) {
			gridRightGrid = (HtmlPanelGrid) findComponentInRoot("gridRightGrid");
		}
		return gridRightGrid;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlSelectOneRadio getRadioOverrideOptionsRadio() {
		if (radioOverrideOptionsRadio == null) {
			radioOverrideOptionsRadio = (HtmlSelectOneRadio) findComponentInRoot("radioOverrideOptionsRadio");
		}
		return radioOverrideOptionsRadio;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

	protected UISelectItem getSelectItem3() {
		if (selectItem3 == null) {
			selectItem3 = (UISelectItem) findComponentInRoot("selectItem3");
		}
		return selectItem3;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getManagedBean("edittingCampaignsHandler");
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	protected HtmlPanelFormBox getFormBox1() {
		if (formBox1 == null) {
			formBox1 = (HtmlPanelFormBox) findComponentInRoot("formBox1");
		}
		return formBox1;
	}

	protected HtmlFormItem getFormItemRateCodeCheck() {
		if (formItemRateCodeCheck == null) {
			formItemRateCodeCheck = (HtmlFormItem) findComponentInRoot("formItemRateCodeCheck");
		}
		return formItemRateCodeCheck;
	}

	protected HtmlInputText getTextRateCodeToCheck() {
		if (textRateCodeToCheck == null) {
			textRateCodeToCheck = (HtmlInputText) findComponentInRoot("textRateCodeToCheck");
		}
		return textRateCodeToCheck;
	}

	protected HtmlPanelGrid getGridRateCodeInfoGrid() {
		if (gridRateCodeInfoGrid == null) {
			gridRateCodeInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridRateCodeInfoGrid");
		}
		return gridRateCodeInfoGrid;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlPanelGrid getGridBottomeGrid() {
		if (gridBottomeGrid == null) {
			gridBottomeGrid = (HtmlPanelGrid) findComponentInRoot("gridBottomeGrid");
		}
		return gridBottomeGrid;
	}

	protected HtmlCommandExButton getButtonCheckRate() {
		if (buttonCheckRate == null) {
			buttonCheckRate = (HtmlCommandExButton) findComponentInRoot("buttonCheckRate");
		}
		return buttonCheckRate;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlOutputText getTextOptionSelectionText() {
		if (textOptionSelectionText == null) {
			textOptionSelectionText = (HtmlOutputText) findComponentInRoot("textOptionSelectionText");
		}
		return textOptionSelectionText;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlCommandExButton getButtonUseRate() {
		if (buttonUseRate == null) {
			buttonUseRate = (HtmlCommandExButton) findComponentInRoot("buttonUseRate");
		}
		return buttonUseRate;
	}

	public String doButtonCheckRateAction() {
		// Type Java code that runs when the component is clicked
	
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		
		String pub = editor.getSourceCampaign().getPubcode();
		try {
			RelRateBO rate = RelRateBO.fetchRateCode(pub, editor.getForceEZPAYRateCodeToLookUp());
			
			if (rate != null) {
				editor.setLastRateLookedUp(rate);
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Found Rate " + editor.getForceEZPAYRateCodeToLookUp(), null);
				getFacesContext().addMessage("form1:textRateCodeToCheck", message);
				
			}
			else {
				editor.setLastRateLookedUp(null);
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "No Rate Found For Code: " + editor.getForceEZPAYRateCodeToLookUp(), null);
				getFacesContext().addMessage("form1:textRateCodeToCheck", message);
			}
			
		}
		catch (Exception e) {
			editor.setLastRateLookedUp(null);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
			getFacesContext().addMessage("form1:textRateCodeToCheck", message);
		}
		
		return "";
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getManagedBean("campaignDetailEditsHandler");
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	protected HtmlPanelGroup getGroupDefaultForceEZPAYFooterGroup() {
		if (groupDefaultForceEZPAYFooterGroup == null) {
			groupDefaultForceEZPAYFooterGroup = (HtmlPanelGroup) findComponentInRoot("groupDefaultForceEZPAYFooterGroup");
		}
		return groupDefaultForceEZPAYFooterGroup;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	protected HtmlPanelDialog getDialogDefaultForceEZPayDialog() {
		if (dialogDefaultForceEZPayDialog == null) {
			dialogDefaultForceEZPayDialog = (HtmlPanelDialog) findComponentInRoot("dialogDefaultForceEZPayDialog");
		}
		return dialogDefaultForceEZPayDialog;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}

	protected HtmlDataTableEx getTableExDefaultForceEzpayOptionsTable() {
		if (tableExDefaultForceEzpayOptionsTable == null) {
			tableExDefaultForceEzpayOptionsTable = (HtmlDataTableEx) findComponentInRoot("tableExDefaultForceEzpayOptionsTable");
		}
		return tableExDefaultForceEzpayOptionsTable;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputLinkEx getLinkExShowDefaults() {
		if (linkExShowDefaults == null) {
			linkExShowDefaults = (HtmlOutputLinkEx) findComponentInRoot("linkExShowDefaults");
		}
		return linkExShowDefaults;
	}

	public String doButtonUseRateAction() {
		// Type Java code that runs when the component is clicked
	
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		EditCampaignDetailsHandler campaignsHandler = this.getEdittingCampaignsHandler();

		String rateCode = null;
		String selectedOption = null;
		
		try {
			RelRtCodeIntf rate = editor.getLastRateLookedUp();
			
			// the button to get to this method is only shown if a rate exists
			if (rate != null) {
				rateCode = rate.getPiaRateCode();
				
				editor.setForceEZPAYOverrideRateOption(CampaignEditsHandler.USE_CUSTOM_VALUE);
				selectedOption = CampaignEditsHandler.USE_CUSTOM_VALUE;

			}
			else {
				editor.setLastRateLookedUp(null);
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "You must first check the rate, and then apply it to the campaigns.", null);
				getFacesContext().addMessage(null, message);
				return "failure";
			}
			
			editor.setChanged(true);
			
			// apply the change to the campaigns being editted
			for (CampaignHandler ch : campaignsHandler.getCampaigns()) {
				UsatCampaignIntf c = ch.getCampaign();
				
				EditablePromotionSetIntf promotionSet = c.getPromotionSet();

				// clear any prevous settings
				promotionSet.clearForceEZPAYOverrides();
				
				EditablePromotionBO promo = null;
				if (selectedOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_VALUE)) {
					promo = new EditablePromotionBO();
					promo.setType(PromotionIntf.FORCE_EZPAY_OFFER);
					promo.setFulfillText(selectedOption);
					promo.setFulfillUrl(rateCode);
					
				}
				else if (selectedOption.equalsIgnoreCase(CampaignEditsHandler.USE_HIDDEN_VALUE)) {
					promo = new EditablePromotionBO();
					promo.setType(PromotionIntf.FORCE_EZPAY_OFFER);
					promo.setFulfillText(selectedOption);
				}
				
				promotionSet.setForceEZPAY(promo);
			} // end for campaigns being editted
				
			
		}
		catch (Exception e) {
			editor.setLastRateLookedUp(null);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exception applying rate to campaigns: " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
		}
		
		
		// apply to each campaign
		
		// update radio button selection
		
		return "success";
	}

	protected HtmlCommandExButton getButtonApplySetting() {
		if (buttonApplySetting == null) {
			buttonApplySetting = (HtmlCommandExButton) findComponentInRoot("buttonApplySetting");
		}
		return buttonApplySetting;
	}

	public String doButtonApplySettingAction() {
		// Type Java code that runs when the component is clicked
	
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		EditCampaignDetailsHandler campaignsHandler = this.getEdittingCampaignsHandler();

		String rateCode = null;
		String selectedOption = editor.getForceEZPAYOverrideRateOption();
		
		try {
			
			if (selectedOption.equalsIgnoreCase(CampaignEditsHandler.USE_DEFAULT_VALUE)) {
				editor.setChanged(true);
				for (CampaignHandler ch : campaignsHandler.getCampaigns()) {
					
					UsatCampaignIntf c = ch.getCampaign();
					
					EditablePromotionSetIntf promotionSet = c.getPromotionSet();

					
					// clear any prevous settings
					promotionSet.clearForceEZPAYOverrides();
				}				
			}
			else if (selectedOption.equalsIgnoreCase(CampaignEditsHandler.USE_HIDDEN_VALUE)){
				editor.setChanged(true);
				for (CampaignHandler ch : campaignsHandler.getCampaigns()) {
					UsatCampaignIntf c = ch.getCampaign();
					
					EditablePromotionSetIntf promotionSet = c.getPromotionSet();

					// clear any prevous settings
					promotionSet.clearForceEZPAYOverrides();
					
					EditablePromotionBO promo = new EditablePromotionBO();
					promo.setType(PromotionIntf.FORCE_EZPAY_OFFER);
					promo.setFulfillText(selectedOption);

					promotionSet.setForceEZPAY(promo);
				}
			}
			else {
				// use custom override
				RelRtCodeIntf rate = editor.getLastRateLookedUp();
				
				// the button to get to this method is only shown if a rate exists
				if (rate != null) {
					rateCode = rate.getPiaRateCode();
					
				}
				else {
					editor.setLastRateLookedUp(null);
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "You must first check the rate, and then apply it to the campaigns.", null);
					getFacesContext().addMessage(null, message);
					return "failure";
				}
				editor.setChanged(true);
				// apply the change to the campaigns being editted
				for (CampaignHandler ch : campaignsHandler.getCampaigns()) {
					UsatCampaignIntf c = ch.getCampaign();
					
					EditablePromotionSetIntf promotionSet = c.getPromotionSet();

					// clear any prevous settings
					promotionSet.clearForceEZPAYOverrides();
					
					EditablePromotionBO promo = null;
					promo = new EditablePromotionBO();
					promo.setType(PromotionIntf.FORCE_EZPAY_OFFER);
					promo.setFulfillText(selectedOption);
					promo.setFulfillUrl(rateCode);
					
					promotionSet.setForceEZPAY(promo);
				} // end for campaigns being editted
					
			}
			
		}
		catch (Exception e) {
			editor.setLastRateLookedUp(null);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exception applying rate setings to campaigns: " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
		}
		
				
		return "success";
	}

	protected HtmlPanelGroup getGroup2() {
		if (group2 == null) {
			group2 = (HtmlPanelGroup) findComponentInRoot("group2");
		}
		return group2;
	}

	protected HtmlDataTableEx getTableExSelectedCampaignsForEditV2() {
		if (tableExSelectedCampaignsForEditV2 == null) {
			tableExSelectedCampaignsForEditV2 = (HtmlDataTableEx) findComponentInRoot("tableExSelectedCampaignsForEditV2");
		}
		return tableExSelectedCampaignsForEditV2;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlOutputText getTextEditCampsDTHeader() {
		if (textEditCampsDTHeader == null) {
			textEditCampsDTHeader = (HtmlOutputText) findComponentInRoot("textEditCampsDTHeader");
		}
		return textEditCampsDTHeader;
	}

	protected HtmlInputRowSelect getRowSelectRemoveCampaignFromEdit() {
		if (rowSelectRemoveCampaignFromEdit == null) {
			rowSelectRemoveCampaignFromEdit = (HtmlInputRowSelect) findComponentInRoot("rowSelectRemoveCampaignFromEdit");
		}
		return rowSelectRemoveCampaignFromEdit;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	protected HtmlOutputText getTextEditPubHeader() {
		if (textEditPubHeader == null) {
			textEditPubHeader = (HtmlOutputText) findComponentInRoot("textEditPubHeader");
		}
		return textEditPubHeader;
	}

	protected HtmlOutputText getTextEditKeycodeHeader() {
		if (textEditKeycodeHeader == null) {
			textEditKeycodeHeader = (HtmlOutputText) findComponentInRoot("textEditKeycodeHeader");
		}
		return textEditKeycodeHeader;
	}

	protected HtmlOutputText getTextEditNameHeader() {
		if (textEditNameHeader == null) {
			textEditNameHeader = (HtmlOutputText) findComponentInRoot("textEditNameHeader");
		}
		return textEditNameHeader;
	}

	protected HtmlOutputText getTextEditLandingPageHeader() {
		if (textEditLandingPageHeader == null) {
			textEditLandingPageHeader = (HtmlOutputText) findComponentInRoot("textEditLandingPageHeader");
		}
		return textEditLandingPageHeader;
	}

	protected HtmlCommandExButton getButtonRemoveSelectedCampaignsFromEditCollection() {
		if (buttonRemoveSelectedCampaignsFromEditCollection == null) {
			buttonRemoveSelectedCampaignsFromEditCollection = (HtmlCommandExButton) findComponentInRoot("buttonRemoveSelectedCampaignsFromEditCollection");
		}
		return buttonRemoveSelectedCampaignsFromEditCollection;
	}

	protected HtmlForm getFormForm2() {
		if (formForm2 == null) {
			formForm2 = (HtmlForm) findComponentInRoot("formForm2");
		}
		return formForm2;
	}

	protected HtmlPanelDialog getDialogSelectedCampaignsForEdit() {
		if (dialogSelectedCampaignsForEdit == null) {
			dialogSelectedCampaignsForEdit = (HtmlPanelDialog) findComponentInRoot("dialogSelectedCampaignsForEdit");
		}
		return dialogSelectedCampaignsForEdit;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getTextEditCampaignLandingPageDes() {
		if (textEditCampaignLandingPageDes == null) {
			textEditCampaignLandingPageDes = (HtmlOutputText) findComponentInRoot("textEditCampaignLandingPageDes");
		}
		return textEditCampaignLandingPageDes;
	}

	protected HtmlPanelGroup getGroupEditCampaignsFooterGroup() {
		if (groupEditCampaignsFooterGroup == null) {
			groupEditCampaignsFooterGroup = (HtmlPanelGroup) findComponentInRoot("groupEditCampaignsFooterGroup");
		}
		return groupEditCampaignsFooterGroup;
	}

	protected HtmlGraphicImageEx getImageExCampaignListImage() {
		if (imageExCampaignListImage == null) {
			imageExCampaignListImage = (HtmlGraphicImageEx) findComponentInRoot("imageExCampaignListImage");
		}
		return imageExCampaignListImage;
	}

}