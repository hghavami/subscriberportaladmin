/**
 * 
 */
package pagecode.secured.campaigns.details;

import pagecode.PageCodeBase;


import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignVanityHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.bf.component.html.HtmlBfPanel;
import com.ibm.faces.bf.component.html.HtmlTabbedPanel;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.bf.component.html.HtmlButtonPanel;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneMenu;
import com.gannett.usat.dataHandlers.UserHandler;
import javax.faces.component.UISelectItem;

/**
 * @author aeast
 *
 */
public class Standard_vanity extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected EditCampaignVanityHandler edittingCampaignsVanityHandler;

	protected HtmlForm formCampaignDetails;
	protected HtmlPanelFormBox formBoxCampaignDetails;
	protected HtmlFormItem formItemKeyCode;
	protected HtmlFormItem formItem2;
	protected HtmlFormItem formItemLandingPage;
	protected HtmlInputText textCampaignVanity;
	protected HtmlMessages messages1;
	protected HtmlJspPanel jspPanelTopPanel;
	protected HtmlOutputText textHeaderCampPub;
	protected HtmlInputText textCampaignPub;
	protected HtmlOutputText textCampaignName;
	protected HtmlInputText textCampName;
	protected HtmlInputText text3;
	protected HtmlBfPanel bfpanelSinglePageSettings;
	protected HtmlCommandExButton tabbedPanel1_back;
	protected HtmlCommandExButton tabbedPanel1_next;
	protected HtmlCommandExButton tabbedPanel1_finish;
	protected HtmlCommandExButton tabbedPanel1_cancel;
	protected HtmlTabbedPanel tabbedPanelVanityTabPanel;
	protected HtmlPanelLayout layout1;
	protected HtmlButtonPanel bpCustomButtons;
	protected HtmlPanelGrid gridFooterGrid;
	protected HtmlCommandExButton buttonApplyChanges;
	protected HtmlCommandExButton buttonCancelChanges;
	protected HtmlOutputText textPageInformationText;

	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected HtmlSelectOneMenu menu1;
	protected UserHandler user;
	protected UISelectItem selectItem1;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormCampaignDetails() {
		if (formCampaignDetails == null) {
			formCampaignDetails = (HtmlForm) findComponentInRoot("formCampaignDetails");
		}
		return formCampaignDetails;
	}

	protected HtmlPanelFormBox getFormBoxCampaignDetails() {
		if (formBoxCampaignDetails == null) {
			formBoxCampaignDetails = (HtmlPanelFormBox) findComponentInRoot("formBoxCampaignDetails");
		}
		return formBoxCampaignDetails;
	}

	protected HtmlFormItem getFormItemKeyCode() {
		if (formItemKeyCode == null) {
			formItemKeyCode = (HtmlFormItem) findComponentInRoot("formItemKeyCode");
		}
		return formItemKeyCode;
	}

	protected HtmlFormItem getFormItem2() {
		if (formItem2 == null) {
			formItem2 = (HtmlFormItem) findComponentInRoot("formItem2");
		}
		return formItem2;
	}

	protected HtmlFormItem getFormItemLandingPage() {
		if (formItemLandingPage == null) {
			formItemLandingPage = (HtmlFormItem) findComponentInRoot("formItemLandingPage");
		}
		return formItemLandingPage;
	}

	protected HtmlInputText getTextCampaignVanity() {
		if (textCampaignVanity == null) {
			textCampaignVanity = (HtmlInputText) findComponentInRoot("textCampaignVanity");
		}
		return textCampaignVanity;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlJspPanel getJspPanelTopPanel() {
		if (jspPanelTopPanel == null) {
			jspPanelTopPanel = (HtmlJspPanel) findComponentInRoot("jspPanelTopPanel");
		}
		return jspPanelTopPanel;
	}

	protected HtmlOutputText getTextHeaderCampPub() {
		if (textHeaderCampPub == null) {
			textHeaderCampPub = (HtmlOutputText) findComponentInRoot("textHeaderCampPub");
		}
		return textHeaderCampPub;
	}

	protected HtmlInputText getTextCampaignPub() {
		if (textCampaignPub == null) {
			textCampaignPub = (HtmlInputText) findComponentInRoot("textCampaignPub");
		}
		return textCampaignPub;
	}

	protected HtmlOutputText getTextCampaignName() {
		if (textCampaignName == null) {
			textCampaignName = (HtmlOutputText) findComponentInRoot("textCampaignName");
		}
		return textCampaignName;
	}

	protected HtmlInputText getTextCampName() {
		if (textCampName == null) {
			textCampName = (HtmlInputText) findComponentInRoot("textCampName");
		}
		return textCampName;
	}

	protected HtmlInputText getText3() {
		if (text3 == null) {
			text3 = (HtmlInputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlBfPanel getBfpanelSinglePageSettings() {
		if (bfpanelSinglePageSettings == null) {
			bfpanelSinglePageSettings = (HtmlBfPanel) findComponentInRoot("bfpanelSinglePageSettings");
		}
		return bfpanelSinglePageSettings;
	}

	protected HtmlCommandExButton getTabbedPanel1_back() {
		if (tabbedPanel1_back == null) {
			tabbedPanel1_back = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_back");
		}
		return tabbedPanel1_back;
	}

	protected HtmlCommandExButton getTabbedPanel1_next() {
		if (tabbedPanel1_next == null) {
			tabbedPanel1_next = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_next");
		}
		return tabbedPanel1_next;
	}

	protected HtmlCommandExButton getTabbedPanel1_finish() {
		if (tabbedPanel1_finish == null) {
			tabbedPanel1_finish = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_finish");
		}
		return tabbedPanel1_finish;
	}

	protected HtmlCommandExButton getTabbedPanel1_cancel() {
		if (tabbedPanel1_cancel == null) {
			tabbedPanel1_cancel = (HtmlCommandExButton) findComponentInRoot("tabbedPanel1_cancel");
		}
		return tabbedPanel1_cancel;
	}

	protected HtmlTabbedPanel getTabbedPanelVanityTabPanel() {
		if (tabbedPanelVanityTabPanel == null) {
			tabbedPanelVanityTabPanel = (HtmlTabbedPanel) findComponentInRoot("tabbedPanelVanityTabPanel");
		}
		return tabbedPanelVanityTabPanel;
	}

	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}

	protected HtmlButtonPanel getBpCustomButtons() {
		if (bpCustomButtons == null) {
			bpCustomButtons = (HtmlButtonPanel) findComponentInRoot("bpCustomButtons");
		}
		return bpCustomButtons;
	}

	protected HtmlPanelGrid getGridFooterGrid() {
		if (gridFooterGrid == null) {
			gridFooterGrid = (HtmlPanelGrid) findComponentInRoot("gridFooterGrid");
		}
		return gridFooterGrid;
	}

	protected HtmlCommandExButton getButtonApplyChanges() {
		if (buttonApplyChanges == null) {
			buttonApplyChanges = (HtmlCommandExButton) findComponentInRoot("buttonApplyChanges");
		}
		return buttonApplyChanges;
	}

	protected HtmlCommandExButton getButtonCancelChanges() {
		if (buttonCancelChanges == null) {
			buttonCancelChanges = (HtmlCommandExButton) findComponentInRoot("buttonCancelChanges");
		}
		return buttonCancelChanges;
	}
	
	/** 
	 * @managed-bean true
	 */
	
	protected EditCampaignVanityHandler getEdittingCampaignsVanityHandler() {
		if (edittingCampaignsVanityHandler == null) {
			edittingCampaignsVanityHandler = (EditCampaignVanityHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsVanityHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsVanityHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}
	
	public String doButtonApplyChangesAction() {
//		 Type Java code that runs when the component is clicked
	    boolean hasErrors = false;	    
	    
		//CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		//set Campaign as 'changed'
		//editor.setChanged(true);
	
		String msgDetail = "";
		//retrieve Campaign from bean and process
		for( CampaignHandler campaign : this.getEdittingCampaignsVanityHandler().getCampaigns()){
			
		    //Verify the campaign is valid
			if (campaign.getCampaign().getIsCampaignValid()) {  				
				hasErrors = false;		
				try {
					campaign.getCampaign().setUpdatedBy(this.getUser().getUserID());
					campaign.getCampaign().save(true);
				}
				catch (Exception e) {
					e.printStackTrace();
					hasErrors = true;
					msgDetail = e.getMessage();
				}
			} else {
				msgDetail = campaign.getCampaign().getErrorMessage();
				hasErrors = true;				
			}			
		}			
	
		if (hasErrors) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "The changes made are not Valid.  Please verify your changes. Detail: " + msgDetail, "");
			getFacesContext().addMessage(null, message);
			return "failure";
		}
		else {	
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected Campaign changes have been saved. You may still need to save your Promotional changes and publish your campaign.", "");
			getFacesContext().addMessage(null, message);
			return "success";
		}
	}

	
	public String doButtonCancelChangesAction() {		
		// return to the campaign details page 
		return "success";
	}

	protected HtmlOutputText getTextPageInformationText() {
		if (textPageInformationText == null) {
			textPageInformationText = (HtmlOutputText) findComponentInRoot("textPageInformationText");
		}
		return textPageInformationText;
	}

	protected HtmlSelectOneMenu getMenu1() {
		if (menu1 == null) {
			menu1 = (HtmlSelectOneMenu) findComponentInRoot("menu1");
		}
		return menu1;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

}