/**
 * 
 */
package pagecode.secured.campaigns.wizard;

import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignWizardHandler;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;

/**
 * @author aeast
 *
 */
public class Wizard_complete extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formWizardForm;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text7;
	protected HtmlOutputText text9;
	protected HtmlDataTableEx tableExNewCampaigns;
	protected UIColumnEx columnEx2;
	protected UIColumnEx columnEx3;
	protected UIColumnEx columnEx4;
	protected UIColumnEx columnEx6;
	protected UIColumnEx columnEx8;
	protected HtmlScriptCollector scriptCollectorNoNavTemplateCollector;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected HtmlOutputText text1;
	protected HtmlOutputText textHeaderText;
	protected CampaignWizardHandler newCampaignWizard;
	protected HtmlOutputText textPubCode;
	protected HtmlOutputText textKeyCode;
	protected HtmlOutputText textCampName;
	protected HtmlOutputText textCampGroupName;
	protected HtmlOutputText textCampStatus;
	protected HtmlOutputText textErrorReasonLabel;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText textErrorReason;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText textRecommendedActionLabel;
	protected HtmlOutputText textExceptionLabel;
	protected UIColumnEx columnEx9;
	protected HtmlOutputText textExeptionDetail;
	protected HtmlMessages messages1;
	protected HtmlOutputText textVanity;
	protected HtmlOutputText textAllCampaignStatusHeader;
	protected HtmlDataTableEx tableExNewCampaignstable;
	protected UIColumnEx columnEx10;
	protected HtmlOutputText text8;
	protected HtmlOutputText text10;
	protected UIColumnEx columnEx11;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx12;
	protected HtmlOutputText text12;
	protected UIColumnEx columnEx13;
	protected HtmlOutputText text13;
	protected UIColumnEx columnEx14;
	protected HtmlOutputText text14;
	protected UIColumnEx columnEx15;
	protected HtmlOutputText text15;
	protected HtmlOutputText text16;
	protected HtmlOutputText text17;
	protected HtmlOutputText text18;
	protected HtmlOutputText text19;
	protected HtmlOutputText text20;
	protected HtmlOutputLinkEx linkExProdLink;
	protected HtmlCommandExButton buttonAllFinished;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormWizardForm() {
		if (formWizardForm == null) {
			formWizardForm = (HtmlForm) findComponentInRoot("formWizardForm");
		}
		return formWizardForm;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlDataTableEx getTableExNewCampaigns() {
		if (tableExNewCampaigns == null) {
			tableExNewCampaigns = (HtmlDataTableEx) findComponentInRoot("tableExNewCampaigns");
		}
		return tableExNewCampaigns;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlScriptCollector getScriptCollectorNoNavTemplateCollector() {
		if (scriptCollectorNoNavTemplateCollector == null) {
			scriptCollectorNoNavTemplateCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorNoNavTemplateCollector");
		}
		return scriptCollectorNoNavTemplateCollector;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextHeaderText() {
		if (textHeaderText == null) {
			textHeaderText = (HtmlOutputText) findComponentInRoot("textHeaderText");
		}
		return textHeaderText;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignWizardHandler getNewCampaignWizard() {
		if (newCampaignWizard == null) {
			newCampaignWizard = (CampaignWizardHandler) getFacesContext()
					.getApplication()
					.createValueBinding("#{newCampaignWizard}").getValue(
							getFacesContext());
		}
		return newCampaignWizard;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewCampaignWizard(CampaignWizardHandler newCampaignWizard) {
		this.newCampaignWizard = newCampaignWizard;
	}

	protected HtmlOutputText getTextPubCode() {
		if (textPubCode == null) {
			textPubCode = (HtmlOutputText) findComponentInRoot("textPubCode");
		}
		return textPubCode;
	}

	protected HtmlOutputText getTextKeyCode() {
		if (textKeyCode == null) {
			textKeyCode = (HtmlOutputText) findComponentInRoot("textKeyCode");
		}
		return textKeyCode;
	}

	protected HtmlOutputText getTextCampName() {
		if (textCampName == null) {
			textCampName = (HtmlOutputText) findComponentInRoot("textCampName");
		}
		return textCampName;
	}

	protected HtmlOutputText getTextCampGroupName() {
		if (textCampGroupName == null) {
			textCampGroupName = (HtmlOutputText) findComponentInRoot("textCampGroupName");
		}
		return textCampGroupName;
	}

	protected HtmlOutputText getTextCampStatus() {
		if (textCampStatus == null) {
			textCampStatus = (HtmlOutputText) findComponentInRoot("textCampStatus");
		}
		return textCampStatus;
	}

	protected HtmlOutputText getTextErrorReasonLabel() {
		if (textErrorReasonLabel == null) {
			textErrorReasonLabel = (HtmlOutputText) findComponentInRoot("textErrorReasonLabel");
		}
		return textErrorReasonLabel;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getTextErrorReason() {
		if (textErrorReason == null) {
			textErrorReason = (HtmlOutputText) findComponentInRoot("textErrorReason");
		}
		return textErrorReason;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlOutputText getTextRecommendedActionLabel() {
		if (textRecommendedActionLabel == null) {
			textRecommendedActionLabel = (HtmlOutputText) findComponentInRoot("textRecommendedActionLabel");
		}
		return textRecommendedActionLabel;
	}

	protected HtmlOutputText getTextExceptionLabel() {
		if (textExceptionLabel == null) {
			textExceptionLabel = (HtmlOutputText) findComponentInRoot("textExceptionLabel");
		}
		return textExceptionLabel;
	}

	protected UIColumnEx getColumnEx9() {
		if (columnEx9 == null) {
			columnEx9 = (UIColumnEx) findComponentInRoot("columnEx9");
		}
		return columnEx9;
	}

	protected HtmlOutputText getTextExeptionDetail() {
		if (textExeptionDetail == null) {
			textExeptionDetail = (HtmlOutputText) findComponentInRoot("textExeptionDetail");
		}
		return textExeptionDetail;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getTextVanity() {
		if (textVanity == null) {
			textVanity = (HtmlOutputText) findComponentInRoot("textVanity");
		}
		return textVanity;
	}

	protected HtmlOutputText getTextAllCampaignStatusHeader() {
		if (textAllCampaignStatusHeader == null) {
			textAllCampaignStatusHeader = (HtmlOutputText) findComponentInRoot("textAllCampaignStatusHeader");
		}
		return textAllCampaignStatusHeader;
	}

	protected HtmlDataTableEx getTableExNewCampaignstable() {
		if (tableExNewCampaignstable == null) {
			tableExNewCampaignstable = (HtmlDataTableEx) findComponentInRoot("tableExNewCampaignstable");
		}
		return tableExNewCampaignstable;
	}

	protected UIColumnEx getColumnEx10() {
		if (columnEx10 == null) {
			columnEx10 = (UIColumnEx) findComponentInRoot("columnEx10");
		}
		return columnEx10;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected UIColumnEx getColumnEx11() {
		if (columnEx11 == null) {
			columnEx11 = (UIColumnEx) findComponentInRoot("columnEx11");
		}
		return columnEx11;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected UIColumnEx getColumnEx12() {
		if (columnEx12 == null) {
			columnEx12 = (UIColumnEx) findComponentInRoot("columnEx12");
		}
		return columnEx12;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected UIColumnEx getColumnEx13() {
		if (columnEx13 == null) {
			columnEx13 = (UIColumnEx) findComponentInRoot("columnEx13");
		}
		return columnEx13;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected UIColumnEx getColumnEx14() {
		if (columnEx14 == null) {
			columnEx14 = (UIColumnEx) findComponentInRoot("columnEx14");
		}
		return columnEx14;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected UIColumnEx getColumnEx15() {
		if (columnEx15 == null) {
			columnEx15 = (UIColumnEx) findComponentInRoot("columnEx15");
		}
		return columnEx15;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

	protected HtmlOutputText getText19() {
		if (text19 == null) {
			text19 = (HtmlOutputText) findComponentInRoot("text19");
		}
		return text19;
	}

	protected HtmlOutputText getText20() {
		if (text20 == null) {
			text20 = (HtmlOutputText) findComponentInRoot("text20");
		}
		return text20;
	}

	protected HtmlOutputLinkEx getLinkExProdLink() {
		if (linkExProdLink == null) {
			linkExProdLink = (HtmlOutputLinkEx) findComponentInRoot("linkExProdLink");
		}
		return linkExProdLink;
	}

	protected HtmlCommandExButton getButtonAllFinished() {
		if (buttonAllFinished == null) {
			buttonAllFinished = (HtmlCommandExButton) findComponentInRoot("buttonAllFinished");
		}
		return buttonAllFinished;
	}

	public String doButtonAllFinishedAction() {
		// Type Java code that runs when the component is clicked
	
		CampaignWizardHandler cw = this.getNewCampaignWizard();
		cw.reset();
		return "";
	}

}