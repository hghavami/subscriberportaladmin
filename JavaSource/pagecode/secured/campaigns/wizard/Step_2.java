/**
 * 
 */
package pagecode.secured.campaigns.wizard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectManyListbox;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.UserHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignGroupNameSuggestionsHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignWizardHandler;
import com.gannett.usat.dataHandlers.products.ProductsHandler;
import com.gannett.usat.dataHandlers.subscriptionOffers.SubscriptionOffersHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.KeyCodeIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperTypeahead;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.UsatException;

/**
 * @author aeast
 *
 */
public class Step_2 extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formWizardForm;
	protected HtmlPanelFormBox formBoxLeftBox;
	protected HtmlJspPanel jspPanel1;
	protected HtmlPanelLayout layout1;
	protected HtmlCommandExButton buttonAddKeycode;
	protected HtmlPanelGrid gridKeycodeMovers;
	protected HtmlCommandExButton buttonRemoveKeycodes;
	protected HtmlSelectManyListbox listboxAvailableKeycodes;
	protected HtmlSelectManyListbox listboxSelectedKeycodes;
	protected HtmlJspPanel jspPanel2;
	protected HtmlOutputText textKeycodeLabel;
	protected HtmlInputText textKeycodeFilter;
	protected HtmlCommandExButton buttonKeycodeFilter;
	protected HtmlOutputSeparator separator1;
	protected HtmlInputText textGroupName;
	protected HtmlFormItem formItemUTGrouping;
	protected HtmlFormItem formItemAddToGroupFlag;
	protected HtmlSelectBooleanCheckbox checkboxIncludeInGroup;
	protected HtmlPanelGrid gridFooterGrid;
	protected HtmlOutputSeparator separator2;
	protected HtmlPanelLayout layoutFooterLayout;
	protected HtmlOutputText textFiller1;
	protected HtmlCommandExButton buttonPrevStep;
	protected HtmlCommandExButton buttonClearKeycodeFilter;
	protected HtmlPanelFormBox formBoxRight;
	protected HtmlOutputText text1filler;
	protected HtmlGraphicImageEx imageExSWImage;
	protected HtmlGraphicImageEx imageExUTLogoSmall;
	protected HtmlCommandExButton buttonNextStep;
	protected HtmlScriptCollector scriptCollectorNoNavTemplateCollector;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected HtmlOutputText text1;
	protected HtmlInputHelperTypeahead typeaheadGroupNameSuggestions;
	protected HtmlMessages messages1;
	protected CampaignWizardHandler newCampaignWizard;
	protected SubscriptionOffersHandler availalbeKeyCodesHandler;
	protected UserHandler user;
	protected CampaignGroupNameSuggestionsHandler groupNameSuggestions;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected ProductsHandler productHandler;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormWizardForm() {
		if (formWizardForm == null) {
			formWizardForm = (HtmlForm) findComponentInRoot("formWizardForm");
		}
		return formWizardForm;
	}

	protected HtmlPanelFormBox getFormBoxLeftBox() {
		if (formBoxLeftBox == null) {
			formBoxLeftBox = (HtmlPanelFormBox) findComponentInRoot("formBoxLeftBox");
		}
		return formBoxLeftBox;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlPanelLayout getLayout1() {
		if (layout1 == null) {
			layout1 = (HtmlPanelLayout) findComponentInRoot("layout1");
		}
		return layout1;
	}

	protected HtmlCommandExButton getButtonAddKeycode() {
		if (buttonAddKeycode == null) {
			buttonAddKeycode = (HtmlCommandExButton) findComponentInRoot("buttonAddKeycode");
		}
		return buttonAddKeycode;
	}

	protected HtmlPanelGrid getGridKeycodeMovers() {
		if (gridKeycodeMovers == null) {
			gridKeycodeMovers = (HtmlPanelGrid) findComponentInRoot("gridKeycodeMovers");
		}
		return gridKeycodeMovers;
	}

	protected HtmlCommandExButton getButtonRemoveKeycodes() {
		if (buttonRemoveKeycodes == null) {
			buttonRemoveKeycodes = (HtmlCommandExButton) findComponentInRoot("buttonRemoveKeycodes");
		}
		return buttonRemoveKeycodes;
	}

	protected HtmlSelectManyListbox getListboxAvailableKeycodes() {
		if (listboxAvailableKeycodes == null) {
			listboxAvailableKeycodes = (HtmlSelectManyListbox) findComponentInRoot("listboxAvailableKeycodes");
		}
		return listboxAvailableKeycodes;
	}

	protected HtmlSelectManyListbox getListboxSelectedKeycodes() {
		if (listboxSelectedKeycodes == null) {
			listboxSelectedKeycodes = (HtmlSelectManyListbox) findComponentInRoot("listboxSelectedKeycodes");
		}
		return listboxSelectedKeycodes;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlOutputText getTextKeycodeLabel() {
		if (textKeycodeLabel == null) {
			textKeycodeLabel = (HtmlOutputText) findComponentInRoot("textKeycodeLabel");
		}
		return textKeycodeLabel;
	}

	protected HtmlInputText getTextKeycodeFilter() {
		if (textKeycodeFilter == null) {
			textKeycodeFilter = (HtmlInputText) findComponentInRoot("textKeycodeFilter");
		}
		return textKeycodeFilter;
	}

	protected HtmlCommandExButton getButtonKeycodeFilter() {
		if (buttonKeycodeFilter == null) {
			buttonKeycodeFilter = (HtmlCommandExButton) findComponentInRoot("buttonKeycodeFilter");
		}
		return buttonKeycodeFilter;
	}

	protected HtmlOutputSeparator getSeparator1() {
		if (separator1 == null) {
			separator1 = (HtmlOutputSeparator) findComponentInRoot("separator1");
		}
		return separator1;
	}

	protected HtmlInputText getTextGroupName() {
		if (textGroupName == null) {
			textGroupName = (HtmlInputText) findComponentInRoot("textGroupName");
		}
		return textGroupName;
	}

	protected HtmlFormItem getFormItemUTGrouping() {
		if (formItemUTGrouping == null) {
			formItemUTGrouping = (HtmlFormItem) findComponentInRoot("formItemUTGrouping");
		}
		return formItemUTGrouping;
	}

	protected HtmlFormItem getFormItemAddToGroupFlag() {
		if (formItemAddToGroupFlag == null) {
			formItemAddToGroupFlag = (HtmlFormItem) findComponentInRoot("formItemAddToGroupFlag");
		}
		return formItemAddToGroupFlag;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxIncludeInGroup() {
		if (checkboxIncludeInGroup == null) {
			checkboxIncludeInGroup = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxIncludeInGroup");
		}
		return checkboxIncludeInGroup;
	}

	protected HtmlPanelGrid getGridFooterGrid() {
		if (gridFooterGrid == null) {
			gridFooterGrid = (HtmlPanelGrid) findComponentInRoot("gridFooterGrid");
		}
		return gridFooterGrid;
	}

	public String doButtonPrevStepAction() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		return "success";
	}

	public String doButtonNextStepAction() {
		// Type Java code that runs when the component is clicked

		String pathString = "success";
		CampaignWizardHandler cw = this.getNewCampaignWizard();
		
		if (cw.isIncludeInGroup()) { 
			if (cw.getGroupName() == null || cw.getGroupName().trim().length() == 0) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "If you would like to include the new campaign(s) in a group, please enter a new or existing group name.", null);
				getFacesContext().addMessage(null, message);
				pathString = "failure";
				return pathString;
			}
			else {
				cw.setGroupName(cw.getGroupName().trim());
				// verify that the group name is not in use for a different pub
				try {
					Collection<UsatCampaignIntf> existingCampaigns = UsatCampaignBO.fetchCampaignsInGroup(cw.getGroupName());
					if (existingCampaigns != null && existingCampaigns.size() > 0) {
						UsatCampaignIntf c = existingCampaigns.iterator().next();
						if (!c.getPubCode().equalsIgnoreCase(cw.getPublication())) {
							USATProductBO prod = this.getProductHandler().getProductForPub(c.getPubCode());
							String pubStr = c.getPubCode();
							if (prod != null) {
								pubStr = prod.getName();
							}
							FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The group name you selected already exists for " + pubStr + ". Please choose another group name.", null);
							getFacesContext().addMessage(null, message);
							pathString = "failure";
							return pathString;
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Exception: " + e.getMessage(), null);
					getFacesContext().addMessage(null, message);
					pathString = "failure";
					return pathString;
				}
			}
			
		}
		
		if (cw.getSelectedKeyCodes().size() == 0) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "At least one keycode is required to conitnue.", null);
			getFacesContext().addMessage(null, message);
			pathString = "failure";		
			return pathString;
		}
		
		ArrayList<CampaignHandler> campaigns = cw.getNewCampaigns();
		// clear any old ones if they used the back button
		campaigns.clear();
		
		// create new campaign objects for each keycode
		for (KeyCodeIntf keyCode : cw.getSelectedKeyCodes()) {
			// for now create vanity campaigns the type will adjust on the next page.
			UsatCampaignBO newCampaign = UsatCampaignBO.createNewVanityCampaign();
			try {
				newCampaign.setCampaignGroup(cw.getGroupName());
				String tempName = keyCode.getKeyCode() + "-" + keyCode.getOfferDescription();
				if (tempName.length() > 60) {
					tempName = tempName.substring(0, 60);
				}
				newCampaign.setCampaignName(tempName);
				newCampaign.setCreatedBy(this.getUser().getUserID());
				newCampaign.setKeyCode(keyCode.getKeyCode());
				newCampaign.setPubCode(cw.getPublication());
				
				CampaignHandler ch = new CampaignHandler();
				ch.setCampaign(newCampaign);
				
				campaigns.add(ch);
			}
			catch (UsatException ue) {
				
			}
		}
		
		return pathString;
	}

	public String doButtonClearKeycodeFilterAction() {
		// Type Java code that runs when the component is clicked
	
		this.getNewCampaignWizard().setKeyCodeFilter(null);
		return "success";
	}

	protected HtmlOutputSeparator getSeparator2() {
		if (separator2 == null) {
			separator2 = (HtmlOutputSeparator) findComponentInRoot("separator2");
		}
		return separator2;
	}

	protected HtmlPanelLayout getLayoutFooterLayout() {
		if (layoutFooterLayout == null) {
			layoutFooterLayout = (HtmlPanelLayout) findComponentInRoot("layoutFooterLayout");
		}
		return layoutFooterLayout;
	}

	protected HtmlOutputText getTextFiller1() {
		if (textFiller1 == null) {
			textFiller1 = (HtmlOutputText) findComponentInRoot("textFiller1");
		}
		return textFiller1;
	}

	protected HtmlCommandExButton getButtonPrevStep() {
		if (buttonPrevStep == null) {
			buttonPrevStep = (HtmlCommandExButton) findComponentInRoot("buttonPrevStep");
		}
		return buttonPrevStep;
	}

	protected HtmlCommandExButton getButtonClearKeycodeFilter() {
		if (buttonClearKeycodeFilter == null) {
			buttonClearKeycodeFilter = (HtmlCommandExButton) findComponentInRoot("buttonClearKeycodeFilter");
		}
		return buttonClearKeycodeFilter;
	}

	protected HtmlPanelFormBox getFormBoxRight() {
		if (formBoxRight == null) {
			formBoxRight = (HtmlPanelFormBox) findComponentInRoot("formBoxRight");
		}
		return formBoxRight;
	}

	protected HtmlOutputText getText1filler() {
		if (text1filler == null) {
			text1filler = (HtmlOutputText) findComponentInRoot("text1filler");
		}
		return text1filler;
	}

	protected HtmlGraphicImageEx getImageExSWImage() {
		if (imageExSWImage == null) {
			imageExSWImage = (HtmlGraphicImageEx) findComponentInRoot("imageExSWImage");
		}
		return imageExSWImage;
	}

	protected HtmlGraphicImageEx getImageExUTLogoSmall() {
		if (imageExUTLogoSmall == null) {
			imageExUTLogoSmall = (HtmlGraphicImageEx) findComponentInRoot("imageExUTLogoSmall");
		}
		return imageExUTLogoSmall;
	}

	protected HtmlCommandExButton getButtonNextStep() {
		if (buttonNextStep == null) {
			buttonNextStep = (HtmlCommandExButton) findComponentInRoot("buttonNextStep");
		}
		return buttonNextStep;
	}

	protected HtmlScriptCollector getScriptCollectorNoNavTemplateCollector() {
		if (scriptCollectorNoNavTemplateCollector == null) {
			scriptCollectorNoNavTemplateCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorNoNavTemplateCollector");
		}
		return scriptCollectorNoNavTemplateCollector;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlInputHelperTypeahead getTypeaheadGroupNameSuggestions() {
		if (typeaheadGroupNameSuggestions == null) {
			typeaheadGroupNameSuggestions = (HtmlInputHelperTypeahead) findComponentInRoot("typeaheadGroupNameSuggestions");
		}
		return typeaheadGroupNameSuggestions;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignWizardHandler getNewCampaignWizard() {
		if (newCampaignWizard == null) {
			newCampaignWizard = (CampaignWizardHandler) getFacesContext()
					.getApplication()
					.createValueBinding("#{newCampaignWizard}").getValue(
							getFacesContext());
		}
		return newCampaignWizard;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewCampaignWizard(CampaignWizardHandler newCampaignWizard) {
		this.newCampaignWizard = newCampaignWizard;
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOffersHandler getAvailalbeKeyCodesHandler() {
		if (availalbeKeyCodesHandler == null) {
			availalbeKeyCodesHandler = (SubscriptionOffersHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{availalbeKeyCodesHandler}").getValue(
							getFacesContext());
		}
		return availalbeKeyCodesHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setAvailalbeKeyCodesHandler(
			SubscriptionOffersHandler availalbeKeyCodesHandler) {
		this.availalbeKeyCodesHandler = availalbeKeyCodesHandler;
	}

	public String doButtonAddKeycodeAction() {
		// Type Java code that runs when the component is clicked
	
		// load the selected keycodes array
		CampaignWizardHandler cw = this.getNewCampaignWizard();
		
		if (cw.getTempSelectedKeyCodesStrings() == null || cw.getTempSelectedKeyCodesStrings().length == 0) {
			// do nothing if nothing selected
			return "";
		}
		
		for (int i = 0; i < cw.getTempSelectedKeyCodesStrings().length; i++) {
			String kCodeStr = cw.getTempSelectedKeyCodesStrings()[i];
			kCodeStr = kCodeStr.substring(0, 5);
			boolean found = false;
			Iterator<KeyCodeIntf> kcItr = cw.getAvailableKeyCodes().iterator();
			while(!found && kcItr.hasNext()) {
				KeyCodeIntf kc = kcItr.next();
				if (kc.getKeyCode().equalsIgnoreCase(kCodeStr)) {
					found = true;
					cw.getSelectedKeyCodes().add(kc);
					kcItr.remove();
				}
			}
		}
		
		return "success";
	}

	public String doButtonRemoveKeycodesAction() {
		// Type Java code that runs when the component is clicked
	
		CampaignWizardHandler cw = this.getNewCampaignWizard();
		
		if (cw.getTempRemoveSelectedKeyCodeStrings() == null || cw.getTempRemoveSelectedKeyCodeStrings().length == 0) {
			// do nothing if nothing selected
			return "";
		}
		
		for (int i = 0; i < cw.getTempRemoveSelectedKeyCodeStrings().length; i++) {
			String kCodeStr = cw.getTempRemoveSelectedKeyCodeStrings()[i];
			kCodeStr = kCodeStr.substring(0, 5);
			boolean found = false;
			Iterator<KeyCodeIntf> kcItr = cw.getSelectedKeyCodes().iterator();
			while(!found && kcItr.hasNext()) {
				KeyCodeIntf kc = kcItr.next();
				if (kc.getKeyCode().equalsIgnoreCase(kCodeStr)) {
					found = true;
					cw.getAvailableKeyCodes().add(kc);
					kcItr.remove();
				}
			}
		}
		
		return "success";
	}

	public String doButtonKeycodeFilterAction() {
		// Type Java code that runs when the component is clicked
	
		// return "success"; // global 
		return "success";
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getFacesContext().getApplication()
					.createValueBinding("#{user}").getValue(getFacesContext());
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignGroupNameSuggestionsHandler getGroupNameSuggestions() {
		if (groupNameSuggestions == null) {
			groupNameSuggestions = (CampaignGroupNameSuggestionsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{groupNameSuggestions}").getValue(
							getFacesContext());
		}
		return groupNameSuggestions;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGroupNameSuggestions(
			CampaignGroupNameSuggestionsHandler groupNameSuggestions) {
		this.groupNameSuggestions = groupNameSuggestions;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}

}