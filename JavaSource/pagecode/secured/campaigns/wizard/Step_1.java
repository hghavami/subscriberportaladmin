/**
 * 
 */
package pagecode.secured.campaigns.wizard;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneRadio;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignGroupNameSuggestionsHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignWizardHandler;
import com.gannett.usat.dataHandlers.subscriptionOffers.SubscriptionOffersHandler;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.gannett.usat.dataHandlers.products.ProductsHandler;

/**
 * @author aeast
 *
 */
public class Step_1 extends PageCodeBase {

	protected HtmlGraphicImageEx imageExUTLogo;
	protected HtmlPanelFormBox formBoxNewCampaign;
	protected HtmlFormItem formItemPublication;
	protected HtmlScriptCollector scriptCollectorStep1;
	protected HtmlForm formStep1;
	protected HtmlSelectOneRadio radioPubChoice;
	protected HtmlPanelGrid grid1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlCommandExButton buttonNextStep;
	protected HtmlScriptCollector scriptCollectorNoNavTemplateCollector;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected HtmlOutputText text1;
	protected CampaignWizardHandler newCampaignWizard;
	protected SubscriptionOffersHandler availalbeKeyCodesHandler;
	protected CampaignGroupNameSuggestionsHandler groupNameSuggestions;
	protected UISelectItems selectItems1;
	protected ProductsHandler productHandler;
	protected HtmlGraphicImageEx getImageExUTLogo() {
		if (imageExUTLogo == null) {
			imageExUTLogo = (HtmlGraphicImageEx) findComponentInRoot("imageExUTLogo");
		}
		return imageExUTLogo;
	}

	protected HtmlPanelFormBox getFormBoxNewCampaign() {
		if (formBoxNewCampaign == null) {
			formBoxNewCampaign = (HtmlPanelFormBox) findComponentInRoot("formBoxNewCampaign");
		}
		return formBoxNewCampaign;
	}

	protected HtmlFormItem getFormItemPublication() {
		if (formItemPublication == null) {
			formItemPublication = (HtmlFormItem) findComponentInRoot("formItemPublication");
		}
		return formItemPublication;
	}

	protected HtmlScriptCollector getScriptCollectorStep1() {
		if (scriptCollectorStep1 == null) {
			scriptCollectorStep1 = (HtmlScriptCollector) findComponentInRoot("scriptCollectorStep1");
		}
		return scriptCollectorStep1;
	}

	protected HtmlForm getFormStep1() {
		if (formStep1 == null) {
			formStep1 = (HtmlForm) findComponentInRoot("formStep1");
		}
		return formStep1;
	}

	protected HtmlSelectOneRadio getRadioPubChoice() {
		if (radioPubChoice == null) {
			radioPubChoice = (HtmlSelectOneRadio) findComponentInRoot("radioPubChoice");
		}
		return radioPubChoice;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlCommandExButton getButtonNextStep() {
		if (buttonNextStep == null) {
			buttonNextStep = (HtmlCommandExButton) findComponentInRoot("buttonNextStep");
		}
		return buttonNextStep;
	}

	public String doButtonNextStepAction() {
		// Type Java code that runs when the component is clicked
	
		
		/*if (this.getNewCampaignWizard().isUTCampaign()) {
			this.getNewCampaignWizard().setAvailableKeyCodes(this.getAvailalbeKeyCodesHandler().getUtKeyCodes());
			this.getGroupNameSuggestions().setPubCode(UsaTodayConstants.UT_PUBCODE);
		}
		else {
			this.getNewCampaignWizard().setAvailableKeyCodes(this.getAvailalbeKeyCodesHandler().getSwKeyCodes());			
			this.getGroupNameSuggestions().setPubCode(UsaTodayConstants.SW_PUBCODE);
		}
		*/
		CampaignWizardHandler wizard = this.getNewCampaignWizard();
		
		ProductsHandler ph = this.getProductHandler();
		
		this.getGroupNameSuggestions().setPubCode(wizard.getPublication());
		wizard.setProduct(ph.getProductForPub(wizard.getPublication()));
		this.getAvailalbeKeyCodesHandler().setCurrentPub(wizard.getPublication());
		wizard.setAvailableKeyCodes(this.getAvailalbeKeyCodesHandler().getKeyCodesForPub(wizard.getPublication()));
		return "success";
	}

	protected HtmlScriptCollector getScriptCollectorNoNavTemplateCollector() {
		if (scriptCollectorNoNavTemplateCollector == null) {
			scriptCollectorNoNavTemplateCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorNoNavTemplateCollector");
		}
		return scriptCollectorNoNavTemplateCollector;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignWizardHandler getNewCampaignWizard() {
		if (newCampaignWizard == null) {
			newCampaignWizard = (CampaignWizardHandler) getFacesContext()
					.getApplication()
					.createValueBinding("#{newCampaignWizard}").getValue(
							getFacesContext());
		}
		return newCampaignWizard;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewCampaignWizard(CampaignWizardHandler newCampaignWizard) {
		this.newCampaignWizard = newCampaignWizard;
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOffersHandler getAvailalbeKeyCodesHandler() {
		if (availalbeKeyCodesHandler == null) {
			availalbeKeyCodesHandler = (SubscriptionOffersHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{availalbeKeyCodesHandler}").getValue(
							getFacesContext());
		}
		return availalbeKeyCodesHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setAvailalbeKeyCodesHandler(
			SubscriptionOffersHandler availalbeKeyCodesHandler) {
		this.availalbeKeyCodesHandler = availalbeKeyCodesHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignGroupNameSuggestionsHandler getGroupNameSuggestions() {
		if (groupNameSuggestions == null) {
			groupNameSuggestions = (CampaignGroupNameSuggestionsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{groupNameSuggestions}").getValue(
							getFacesContext());
		}
		return groupNameSuggestions;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGroupNameSuggestions(
			CampaignGroupNameSuggestionsHandler groupNameSuggestions) {
		this.groupNameSuggestions = groupNameSuggestions;
	}

	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}

}