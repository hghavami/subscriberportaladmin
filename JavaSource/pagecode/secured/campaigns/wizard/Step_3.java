/**
 * 
 */
package pagecode.secured.campaigns.wizard;

import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneMenu;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignWizardHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignFTPUtilities;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.gannett.usat.dataHandlers.products.ProductsHandler;
import javax.faces.component.UISelectItem;

/**
 * @author aeast
 *
 */
public class Step_3 extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorStep3WizardForm;
	protected HtmlPanelLayout layoutNewCampaignsArea;
	protected HtmlPanelLayout layoutFooterLayout;
	protected HtmlOutputText text12Filler;
	protected HtmlPanelGrid gridPrevNextButtons;
	protected HtmlCommandExButton buttonPrevStep;
	protected HtmlForm formWizardForm;
	protected HtmlOutputText textFiller11;
	protected HtmlCommandExButton buttonNextStep;
	protected HtmlDataTableEx tableExNewCampaigns;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text1;
	protected HtmlMessages messages1;
	protected HtmlOutputText text2;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textCampaignPub;
	protected HtmlOutputText textCampaignKeycode;
	protected HtmlOutputText textGroupNameColLabel;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText textGroupName;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx4;
	protected HtmlInputText textCampaignName;
	protected HtmlOutputText text5;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText textRedirectToPage;
	protected UIColumnEx columnEx6;
	protected HtmlSelectOneMenu menuCampaignType;
	protected HtmlScriptCollector scriptCollectorNoNavTemplateCollector;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected CampaignWizardHandler newCampaignWizard;
	protected HtmlPanelLayout layoutHeaderLayoutPanel;
	protected HtmlOutputText textHeaderText;
	protected HtmlPanelGrid gridCampaignImageGrid;
	protected HtmlGraphicImageEx imageExUTLogoStep3;
	protected HtmlGraphicImageEx imageExSWImageStep3;
	protected HtmlJspPanel jspPanelVanityDetailPanel;
	protected HtmlInputText textCampaignVanity;
	protected HtmlOutputText textVanityErrorMessage;
	protected ProductsHandler productHandler;
	protected HtmlOutputText textNewCampProdDesLabelHead1;
	protected HtmlOutputText textNewCampProdDescHead;
	protected UISelectItem selectItem1;
	protected HtmlScriptCollector getScriptCollectorStep3WizardForm() {
		if (scriptCollectorStep3WizardForm == null) {
			scriptCollectorStep3WizardForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorStep3WizardForm");
		}
		return scriptCollectorStep3WizardForm;
	}
	protected HtmlPanelLayout getLayoutNewCampaignsArea() {
		if (layoutNewCampaignsArea == null) {
			layoutNewCampaignsArea = (HtmlPanelLayout) findComponentInRoot("layoutNewCampaignsArea");
		}
		return layoutNewCampaignsArea;
	}
	protected HtmlPanelLayout getLayoutFooterLayout() {
		if (layoutFooterLayout == null) {
			layoutFooterLayout = (HtmlPanelLayout) findComponentInRoot("layoutFooterLayout");
		}
		return layoutFooterLayout;
	}
	protected HtmlOutputText getText12Filler() {
		if (text12Filler == null) {
			text12Filler = (HtmlOutputText) findComponentInRoot("text12Filler");
		}
		return text12Filler;
	}
	protected HtmlPanelGrid getGridPrevNextButtons() {
		if (gridPrevNextButtons == null) {
			gridPrevNextButtons = (HtmlPanelGrid) findComponentInRoot("gridPrevNextButtons");
		}
		return gridPrevNextButtons;
	}
	protected HtmlCommandExButton getButtonPrevStep() {
		if (buttonPrevStep == null) {
			buttonPrevStep = (HtmlCommandExButton) findComponentInRoot("buttonPrevStep");
		}
		return buttonPrevStep;
	}
	protected HtmlForm getFormWizardForm() {
		if (formWizardForm == null) {
			formWizardForm = (HtmlForm) findComponentInRoot("formWizardForm");
		}
		return formWizardForm;
	}
	protected HtmlOutputText getTextFiller11() {
		if (textFiller11 == null) {
			textFiller11 = (HtmlOutputText) findComponentInRoot("textFiller11");
		}
		return textFiller11;
	}
	protected HtmlCommandExButton getButtonNextStep() {
		if (buttonNextStep == null) {
			buttonNextStep = (HtmlCommandExButton) findComponentInRoot("buttonNextStep");
		}
		return buttonNextStep;
	}
	public String doButtonPrevStepAction() {
		// Type Java code that runs when the component is clicked
	
		return "success";
	}
	public String doButtonNextStepAction() {
		String responsePath = "success";

		CampaignWizardHandler cw = this.getNewCampaignWizard();
		
		ArrayList<CampaignHandler> newCampaigns = cw.getNewCampaigns();
		
		CampaignFTPUtilities ftpUtil = new CampaignFTPUtilities();
		
		HashMap<String, String> vanities = new HashMap<String, String>();
		
		for (CampaignHandler ch : newCampaigns) {
			String vanity = ch.getCampaign().getVanityURL();
			if (vanity == null || vanity.trim().length() == 0) {
				continue;
			}
			
			if (vanities.containsKey(vanity)) {
				FacesMessage msg = new FacesMessage("Vanity must by unique: " + vanity);
				ch.setCampaignErrorMessage("Vanity already specified.");
				this.getFacesContext().addMessage(null, msg);
				responsePath = "failure";
				continue;
			}
			else {
				// if not in the hash, then add it.
				vanities.put(vanity, vanity);
			}
			int useCount = 0;
			try {
				useCount = UsatCampaignBO.fetchCountOfCampaignsForVanity(vanity);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			boolean publishedAlready =  ftpUtil.isVanityInUse(vanity);
			if (useCount > 0 || publishedAlready) {
				String extraInfo1 = "";
				String extranInfo2 = "";
				if (useCount > 0) {
					extraInfo1 = "Vanity Reserved. Go to the Advanced Search and search by vanity URL to see which campaingn is using this vanity. ";
				}
				if (publishedAlready) {
					extranInfo2 = "Vanity In Use. <a href=\"http://service.usatoday.com/" + vanity + "\" target=_blank>Click</a> to see it in production.";
				}
				// add error to campaign handler only
				ch.setCampaignErrorMessage(extraInfo1 + extranInfo2);
				// return failure
				responsePath = "failure";
			}
		}
		return responsePath;
	}
	protected HtmlDataTableEx getTableExNewCampaigns() {
		if (tableExNewCampaigns == null) {
			tableExNewCampaigns = (HtmlDataTableEx) findComponentInRoot("tableExNewCampaigns");
		}
		return tableExNewCampaigns;
	}
	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}
	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}
	protected HtmlOutputText getTextCampaignPub() {
		if (textCampaignPub == null) {
			textCampaignPub = (HtmlOutputText) findComponentInRoot("textCampaignPub");
		}
		return textCampaignPub;
	}
	protected HtmlOutputText getTextCampaignKeycode() {
		if (textCampaignKeycode == null) {
			textCampaignKeycode = (HtmlOutputText) findComponentInRoot("textCampaignKeycode");
		}
		return textCampaignKeycode;
	}
	protected HtmlOutputText getTextGroupNameColLabel() {
		if (textGroupNameColLabel == null) {
			textGroupNameColLabel = (HtmlOutputText) findComponentInRoot("textGroupNameColLabel");
		}
		return textGroupNameColLabel;
	}
	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}
	protected HtmlOutputText getTextGroupName() {
		if (textGroupName == null) {
			textGroupName = (HtmlOutputText) findComponentInRoot("textGroupName");
		}
		return textGroupName;
	}
	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}
	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}
	protected HtmlInputText getTextCampaignName() {
		if (textCampaignName == null) {
			textCampaignName = (HtmlInputText) findComponentInRoot("textCampaignName");
		}
		return textCampaignName;
	}
	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}
	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}
	protected HtmlOutputText getTextRedirectToPage() {
		if (textRedirectToPage == null) {
			textRedirectToPage = (HtmlOutputText) findComponentInRoot("textRedirectToPage");
		}
		return textRedirectToPage;
	}
	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}
	protected HtmlSelectOneMenu getMenuCampaignType() {
		if (menuCampaignType == null) {
			menuCampaignType = (HtmlSelectOneMenu) findComponentInRoot("menuCampaignType");
		}
		return menuCampaignType;
	}
	protected HtmlScriptCollector getScriptCollectorNoNavTemplateCollector() {
		if (scriptCollectorNoNavTemplateCollector == null) {
			scriptCollectorNoNavTemplateCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorNoNavTemplateCollector");
		}
		return scriptCollectorNoNavTemplateCollector;
	}
	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}
	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignWizardHandler getNewCampaignWizard() {
		if (newCampaignWizard == null) {
			newCampaignWizard = (CampaignWizardHandler) getFacesContext()
					.getApplication()
					.createValueBinding("#{newCampaignWizard}").getValue(
							getFacesContext());
		}
		return newCampaignWizard;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setNewCampaignWizard(CampaignWizardHandler newCampaignWizard) {
		this.newCampaignWizard = newCampaignWizard;
	}
	protected HtmlPanelLayout getLayoutHeaderLayoutPanel() {
		if (layoutHeaderLayoutPanel == null) {
			layoutHeaderLayoutPanel = (HtmlPanelLayout) findComponentInRoot("layoutHeaderLayoutPanel");
		}
		return layoutHeaderLayoutPanel;
	}
	protected HtmlOutputText getTextHeaderText() {
		if (textHeaderText == null) {
			textHeaderText = (HtmlOutputText) findComponentInRoot("textHeaderText");
		}
		return textHeaderText;
	}
	protected HtmlPanelGrid getGridCampaignImageGrid() {
		if (gridCampaignImageGrid == null) {
			gridCampaignImageGrid = (HtmlPanelGrid) findComponentInRoot("gridCampaignImageGrid");
		}
		return gridCampaignImageGrid;
	}
	protected HtmlGraphicImageEx getImageExUTLogoStep3() {
		if (imageExUTLogoStep3 == null) {
			imageExUTLogoStep3 = (HtmlGraphicImageEx) findComponentInRoot("imageExUTLogoStep3");
		}
		return imageExUTLogoStep3;
	}
	protected HtmlGraphicImageEx getImageExSWImageStep3() {
		if (imageExSWImageStep3 == null) {
			imageExSWImageStep3 = (HtmlGraphicImageEx) findComponentInRoot("imageExSWImageStep3");
		}
		return imageExSWImageStep3;
	}
	protected HtmlJspPanel getJspPanelVanityDetailPanel() {
		if (jspPanelVanityDetailPanel == null) {
			jspPanelVanityDetailPanel = (HtmlJspPanel) findComponentInRoot("jspPanelVanityDetailPanel");
		}
		return jspPanelVanityDetailPanel;
	}
	protected HtmlInputText getTextCampaignVanity() {
		if (textCampaignVanity == null) {
			textCampaignVanity = (HtmlInputText) findComponentInRoot("textCampaignVanity");
		}
		return textCampaignVanity;
	}
	protected HtmlOutputText getTextVanityErrorMessage() {
		if (textVanityErrorMessage == null) {
			textVanityErrorMessage = (HtmlOutputText) findComponentInRoot("textVanityErrorMessage");
		}
		return textVanityErrorMessage;
	}
	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}
	protected HtmlOutputText getTextNewCampProdDesLabelHead1() {
		if (textNewCampProdDesLabelHead1 == null) {
			textNewCampProdDesLabelHead1 = (HtmlOutputText) findComponentInRoot("textNewCampProdDesLabelHead1");
		}
		return textNewCampProdDesLabelHead1;
	}
	protected HtmlOutputText getTextNewCampProdDescHead() {
		if (textNewCampProdDescHead == null) {
			textNewCampProdDescHead = (HtmlOutputText) findComponentInRoot("textNewCampProdDescHead");
		}
		return textNewCampProdDescHead;
	}
	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

}