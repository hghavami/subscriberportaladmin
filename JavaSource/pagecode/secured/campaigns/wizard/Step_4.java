/**
 * 
 */
package pagecode.secured.campaigns.wizard;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignWizardHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditablePromotionsSetBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignPublisherFailure;
import com.gannett.usatoday.adminportal.campaigns.utils.UsatCampaignPublisher;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.util.constants.UsaTodayConstants;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;

/**
 * @author aeast
 *
 */
public class Step_4 extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formWizardForm;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text2;
	protected HtmlOutputText text3;
	protected HtmlOutputText text4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlOutputText text9;
	protected HtmlDataTableEx tableExNewCampaigns;
	protected HtmlOutputText textPubcode1;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textKeycode1;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText textCampaignName1;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText textCampaignGroup1;
	protected UIColumnEx columnEx5;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText textVanityURL1;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText textCampaignStatusString1;
	protected HtmlOutputText textCampaignType;
	protected HtmlCommandExButton buttonSaveCampaigns;
	protected HtmlCommandExButton buttonPrevStep;
	protected HtmlOutputText textOptionsHeader;
	protected HtmlSelectBooleanCheckbox checkboxEditDetails;
	protected HtmlSelectBooleanCheckbox checkboxAutoPublish;
	protected HtmlOutputText textEditDetailLabel;
	protected HtmlOutputText textPublishLabel;
	protected HtmlOutputText textAddtlinfo;
	protected HtmlScriptCollector scriptCollectorNoNavTemplateCollector;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected HtmlOutputText text1;
	protected HtmlOutputText textHeaderText;
	protected HtmlOutputText textUnderText;
	protected CampaignWizardHandler newCampaignWizard;
	protected HtmlMessages messages1;
	protected CampaignHandlerReadOnly defaultCampaignReadOnly;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected CampaignHandlerReadOnly utDefaultCampaign;
	protected CampaignHandlerReadOnly swDefaultCampaign;
	protected HtmlOutputText text21;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormWizardForm() {
		if (formWizardForm == null) {
			formWizardForm = (HtmlForm) findComponentInRoot("formWizardForm");
		}
		return formWizardForm;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlDataTableEx getTableExNewCampaigns() {
		if (tableExNewCampaigns == null) {
			tableExNewCampaigns = (HtmlDataTableEx) findComponentInRoot("tableExNewCampaigns");
		}
		return tableExNewCampaigns;
	}

	protected HtmlOutputText getTextPubcode1() {
		if (textPubcode1 == null) {
			textPubcode1 = (HtmlOutputText) findComponentInRoot("textPubcode1");
		}
		return textPubcode1;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getTextKeycode1() {
		if (textKeycode1 == null) {
			textKeycode1 = (HtmlOutputText) findComponentInRoot("textKeycode1");
		}
		return textKeycode1;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getTextCampaignName1() {
		if (textCampaignName1 == null) {
			textCampaignName1 = (HtmlOutputText) findComponentInRoot("textCampaignName1");
		}
		return textCampaignName1;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getTextCampaignGroup1() {
		if (textCampaignGroup1 == null) {
			textCampaignGroup1 = (HtmlOutputText) findComponentInRoot("textCampaignGroup1");
		}
		return textCampaignGroup1;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getTextVanityURL1() {
		if (textVanityURL1 == null) {
			textVanityURL1 = (HtmlOutputText) findComponentInRoot("textVanityURL1");
		}
		return textVanityURL1;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlOutputText getTextCampaignStatusString1() {
		if (textCampaignStatusString1 == null) {
			textCampaignStatusString1 = (HtmlOutputText) findComponentInRoot("textCampaignStatusString1");
		}
		return textCampaignStatusString1;
	}

	protected HtmlOutputText getTextCampaignType() {
		if (textCampaignType == null) {
			textCampaignType = (HtmlOutputText) findComponentInRoot("textCampaignType");
		}
		return textCampaignType;
	}

	protected HtmlCommandExButton getButtonSaveCampaigns() {
		if (buttonSaveCampaigns == null) {
			buttonSaveCampaigns = (HtmlCommandExButton) findComponentInRoot("buttonSaveCampaigns");
		}
		return buttonSaveCampaigns;
	}

	protected HtmlCommandExButton getButtonPrevStep() {
		if (buttonPrevStep == null) {
			buttonPrevStep = (HtmlCommandExButton) findComponentInRoot("buttonPrevStep");
		}
		return buttonPrevStep;
	}

	public String doButtonPrevStepAction() {
		// Type Java code that runs when the component is clicked
	
		// return "success"; // global 
		 return "success"; // global 
		
	}

	public String doButtonSaveCampaignsAction() {
		// Type Java code that runs when the component is clicked
	
		// iterate over campaigns if campaigns in an exisiting group, must
		// set the promotion Set to the existing campaigns promotion set
		String responsePath = "successAndEditDetails";
		CampaignWizardHandler cw = this.getNewCampaignWizard();
		// clear any previous save attempt errors
		cw.setPublishingFailed(false);
		cw.setPublishingFailures(null);
		try {
			ArrayList<CampaignHandler> campaigns = cw.getNewCampaigns();
			
			boolean isInExistingGroup = false;
			Collection<UsatCampaignIntf> existingCampaigns = null;
			CampaignHandler ch = campaigns.get(0);
			String groupName = ch.getCampaignGroup();
			if (groupName != null && groupName.length() > 0) {
				existingCampaigns = UsatCampaignBO.fetchCampaignsInGroup(groupName);
				if (existingCampaigns.size() > 0) {
					isInExistingGroup = true;
				}
			}
			
			EditablePromotionSetIntf sourcePromotionSet = null;
			if (isInExistingGroup) {
				UsatCampaignIntf aCampaign = existingCampaigns.iterator().next();
				sourcePromotionSet = aCampaign.getPromotionSet();
				for (CampaignHandler tempCH : campaigns) {
					EditablePromotionsSetBO eSet = new EditablePromotionsSetBO(sourcePromotionSet);
					tempCH.getCampaign().setPromotionSet(eSet);
				}	
			}

			boolean saveFailed = false;
			// save the campaigns
			for (CampaignHandler tempCH : campaigns) {
				try {
					// clear any previous failure message
					tempCH.setCampaignErrorMessage(null);
					// save campaign and any protional items
					tempCH.getCampaign().save(false);
				}
				catch (Exception e) {
					tempCH.setCampaignErrorMessage(e.getMessage());
					if (!saveFailed) {
						// just want one of these messages
						StringBuilder msgStr = new StringBuilder();
						msgStr.append("Some campaigns failed to Save to the Extranet Admin database.");
						if (cw.isAutoPublish()) {
							msgStr.append(" No campaigns were published to test or production.");
						}
						FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, msgStr.toString(), null);
						getFacesContext().addMessage(null, message);
					}
					saveFailed = true;					
					responsePath = "failure";
				}
			}

			// if publishing campaigns
			// iterate over campaigns and do an automatic publish to test/prod
			boolean publishFailed = false;
			if (cw.isAutoPublish() && !saveFailed) {
				UsatCampaignPublisher publisher = new UsatCampaignPublisher();
				ArrayList<UsatCampaignIntf> newCampaigns = new ArrayList<UsatCampaignIntf>();
				for (CampaignHandler tempCH : campaigns) {
					newCampaigns.add(tempCH.getCampaign());
				}
				// publish to test then prod
				Collection<CampaignPublisherFailure> testErrors = null;
				Collection<CampaignPublisherFailure> prodErrors = null;
				try {
					
					testErrors =  publisher.publishCampaignsToTestServers(newCampaigns);
					// only publish to prod if no failures to test
					if (testErrors.size() == 0) {
						prodErrors = publisher.publishCampaignsToProductionServers(newCampaigns);
					}
				}
				catch (Exception e) {
					if (!publishFailed) {
						FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Some campaigns failed to publish.", null);
						getFacesContext().addMessage(null, message);
					}
					publishFailed = true;
					cw.setPublishingFailed(true);
				}
				if (testErrors != null && testErrors.size() > 0) {
					cw.setPublishingFailed(true);
					cw.setPublishingFailures(testErrors);
				}
				if (prodErrors != null && prodErrors.size()> 0) {
					cw.setPublishingFailed(true);
					cw.setPublishingFailures(prodErrors);
				}
				if (cw.isPublishingFailed()) {
					responsePath = "failure";
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Some or all campaigns failed to publish.", null);
					getFacesContext().addMessage(null, message);
				}
			}
			
			// if no errros and not editing campaigns/group go to campaign home page.
			if (!saveFailed && !publishFailed && !cw.isEditDetails()) {
				StringBuilder msgStr = new StringBuilder();
				msgStr.append("Your campaigns were saved ");
				if (cw.isAutoPublish()) {
					msgStr.append(" and published to test and production.");
				}
				else {
					msgStr.append(". They have not been published.");
				}
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, msgStr.toString(), null);
				getFacesContext().addMessage(null, message);
				responsePath = "saveNoEditDetails";
			}
			else {
				// set up everything to edit campaign details

				// set up the source campaign
				CampaignHandler source = campaigns.get(0);
				CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
				editor.setSourceCampaign(source);
				
				// set up default campaign object
				CampaignHandlerReadOnly defaultCampaign = this.getDefaultCampaignReadOnly();
				
				USATProductBO prod = cw.getProduct();
				
				if (prod.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
					CampaignHandler dch = new CampaignHandler();
					dch.setCampaign(this.getUtDefaultCampaign().getCampaign());
					defaultCampaign.setSourceCampaign(dch);
					editor.setDefaultCampaign(dch);
				}
				else {
					CampaignHandler dch = new CampaignHandler();
					dch.setCampaign(this.getSwDefaultCampaign().getCampaign());
					defaultCampaign.setSourceCampaign(dch);
					editor.setDefaultCampaign(dch);
				}
				
				// set the collection of campaigns to be editted into the following
				EditCampaignDetailsHandler editsHandler = this.getEdittingCampaignsHandler();
				
				// load the editable campaigns with the new ones and any existing group campaigns
				Collection<CampaignHandler> campaignsToEdit = new ArrayList<CampaignHandler>();
				// add the campaigns 
				campaignsToEdit.addAll(campaigns);
				if (isInExistingGroup) {
					editsHandler.setEdittingAGroup(true);
					if (existingCampaigns.size() > 0) {
						for (UsatCampaignIntf tempC : existingCampaigns) {
							CampaignHandler cHandler = new CampaignHandler();
							cHandler.setCampaign(tempC);
							campaignsToEdit.add(cHandler);
						}
					}
				}
				editsHandler.setCampaigns(campaignsToEdit); 
				
				// reset the campaign wizard before moving on
				cw.reset();
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Excepton occurred: " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
		}
		return responsePath; 
		
	}

	protected HtmlOutputText getTextOptionsHeader() {
		if (textOptionsHeader == null) {
			textOptionsHeader = (HtmlOutputText) findComponentInRoot("textOptionsHeader");
		}
		return textOptionsHeader;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxEditDetails() {
		if (checkboxEditDetails == null) {
			checkboxEditDetails = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxEditDetails");
		}
		return checkboxEditDetails;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxAutoPublish() {
		if (checkboxAutoPublish == null) {
			checkboxAutoPublish = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxAutoPublish");
		}
		return checkboxAutoPublish;
	}

	protected HtmlOutputText getTextEditDetailLabel() {
		if (textEditDetailLabel == null) {
			textEditDetailLabel = (HtmlOutputText) findComponentInRoot("textEditDetailLabel");
		}
		return textEditDetailLabel;
	}

	protected HtmlOutputText getTextPublishLabel() {
		if (textPublishLabel == null) {
			textPublishLabel = (HtmlOutputText) findComponentInRoot("textPublishLabel");
		}
		return textPublishLabel;
	}

	protected HtmlOutputText getTextAddtlinfo() {
		if (textAddtlinfo == null) {
			textAddtlinfo = (HtmlOutputText) findComponentInRoot("textAddtlinfo");
		}
		return textAddtlinfo;
	}

	protected HtmlScriptCollector getScriptCollectorNoNavTemplateCollector() {
		if (scriptCollectorNoNavTemplateCollector == null) {
			scriptCollectorNoNavTemplateCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorNoNavTemplateCollector");
		}
		return scriptCollectorNoNavTemplateCollector;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getTextHeaderText() {
		if (textHeaderText == null) {
			textHeaderText = (HtmlOutputText) findComponentInRoot("textHeaderText");
		}
		return textHeaderText;
	}

	protected HtmlOutputText getTextUnderText() {
		if (textUnderText == null) {
			textUnderText = (HtmlOutputText) findComponentInRoot("textUnderText");
		}
		return textUnderText;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignWizardHandler getNewCampaignWizard() {
		if (newCampaignWizard == null) {
			newCampaignWizard = (CampaignWizardHandler) getFacesContext()
					.getApplication()
					.createValueBinding("#{newCampaignWizard}").getValue(
							getFacesContext());
		}
		return newCampaignWizard;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewCampaignWizard(CampaignWizardHandler newCampaignWizard) {
		this.newCampaignWizard = newCampaignWizard;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getDefaultCampaignReadOnly() {
		if (defaultCampaignReadOnly == null) {
			defaultCampaignReadOnly = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication().createValueBinding(
							"#{defaultCampaignReadOnly}").getValue(
							getFacesContext());
		}
		return defaultCampaignReadOnly;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setDefaultCampaignReadOnly(
			CampaignHandlerReadOnly defaultCampaignReadOnly) {
		this.defaultCampaignReadOnly = defaultCampaignReadOnly;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getUtDefaultCampaign() {
		if (utDefaultCampaign == null) {
			utDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{utDefaultCampaign}").getValue(
							getFacesContext());
		}
		return utDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUtDefaultCampaign(
			CampaignHandlerReadOnly utDefaultCampaign) {
		this.utDefaultCampaign = utDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getSwDefaultCampaign() {
		if (swDefaultCampaign == null) {
			swDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{swDefaultCampaign}").getValue(
							getFacesContext());
		}
		return swDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSwDefaultCampaign(
			CampaignHandlerReadOnly swDefaultCampaign) {
		this.swDefaultCampaign = swDefaultCampaign;
	}

	protected HtmlOutputText getText21() {
		if (text21 == null) {
			text21 = (HtmlOutputText) findComponentInRoot("text21");
		}
		return text21;
	}

}