/**
 * 
 */
package pagecode.secured.campaigns.groups;

import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignGroupNameSuggestionsHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.ManageGroupsHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlInputHelperTypeahead;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlOutputSelecticons;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.util.constants.UsaTodayConstants;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

/**
 * @author aeast
 *
 */
public class Manage_group extends PageCodeBase {

	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlScriptCollector scriptCollectorManageGroups;
	protected HtmlForm formManageGroups;
	protected UIColumnEx columnExCampName;
	protected HtmlOutputText textCampNameHeaderLabel;
	protected HtmlDataTableEx tableExCampaignsToEditTable;
	protected HtmlPanelBox box1;
	protected HtmlOutputText textDataTableHeaderText;
	protected UIColumnEx columnExGroupNameCol;
	protected HtmlOutputText text2;
	protected UIColumnEx columnExPubCol;
	protected HtmlOutputText text3;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx4;
	protected HtmlInputRowSelect rowSelectCampaign;
	protected UIColumnEx columnEx5;
	protected HtmlPanelBox boxSelectionBoxesBottom;
	protected HtmlOutputSelecticons selecticonsBoxBottom;
	protected HtmlOutputSelecticons selecticonsBoxTop;
	protected HtmlOutputText textCampName;
	protected HtmlOutputText textGroupName;
	protected HtmlOutputText textPubCode;
	protected HtmlOutputText textKeycode;
	protected HtmlOutputText textProdVanityLinkText;
	protected HtmlOutputLinkEx linkExVanityLink;
	protected HtmlPanelFormBox formBoxGroupSettingsFormBox;
	protected HtmlMessages messagesErrrorMessages;
	protected ManageGroupsHandler campaignGroupSettingsHandler;
	protected HtmlInputHelperTypeahead typeaheadGroupName;
	protected CampaignGroupNameSuggestionsHandler groupNameSuggestions;
	protected HtmlPanelBox boxFooterBox;
	protected HtmlPanelGrid gridButtonGrid;
	protected HtmlCommandExButton buttonApplyGroupName;
	protected HtmlCommandExButton buttonClearGroupNames;
	protected HtmlInputText textNewGroupName;
	protected HtmlFormItem formItemNewGroupName;
	protected HtmlPanelGrid gridNameGrid;
	protected HtmlOutputText textChangedIndicator;
	protected HtmlOutputText textInfoLabel;
	protected HtmlOutputText textChangedIndicatorReadOnly;
	protected HtmlJspPanel jspPanelMessagesPanelSave;
	protected HtmlOutputText textSaveLinkLabel;
	protected HtmlRequestLink linkSaveGroupChanges;
	protected HtmlOutputLinkEx linkExBackToSearchPage;
	protected HtmlOutputText text9;
	protected HtmlGraphicImageEx imageExSearchIcon;
	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	protected HtmlScriptCollector getScriptCollectorManageGroups() {
		if (scriptCollectorManageGroups == null) {
			scriptCollectorManageGroups = (HtmlScriptCollector) findComponentInRoot("scriptCollectorManageGroups");
		}
		return scriptCollectorManageGroups;
	}

	protected HtmlForm getFormManageGroups() {
		if (formManageGroups == null) {
			formManageGroups = (HtmlForm) findComponentInRoot("formManageGroups");
		}
		return formManageGroups;
	}

	protected UIColumnEx getColumnExCampName() {
		if (columnExCampName == null) {
			columnExCampName = (UIColumnEx) findComponentInRoot("columnExCampName");
		}
		return columnExCampName;
	}

	protected HtmlOutputText getTextCampNameHeaderLabel() {
		if (textCampNameHeaderLabel == null) {
			textCampNameHeaderLabel = (HtmlOutputText) findComponentInRoot("textCampNameHeaderLabel");
		}
		return textCampNameHeaderLabel;
	}

	protected HtmlDataTableEx getTableExCampaignsToEditTable() {
		if (tableExCampaignsToEditTable == null) {
			tableExCampaignsToEditTable = (HtmlDataTableEx) findComponentInRoot("tableExCampaignsToEditTable");
		}
		return tableExCampaignsToEditTable;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlOutputText getTextDataTableHeaderText() {
		if (textDataTableHeaderText == null) {
			textDataTableHeaderText = (HtmlOutputText) findComponentInRoot("textDataTableHeaderText");
		}
		return textDataTableHeaderText;
	}

	protected UIColumnEx getColumnExGroupNameCol() {
		if (columnExGroupNameCol == null) {
			columnExGroupNameCol = (UIColumnEx) findComponentInRoot("columnExGroupNameCol");
		}
		return columnExGroupNameCol;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnExPubCol() {
		if (columnExPubCol == null) {
			columnExPubCol = (UIColumnEx) findComponentInRoot("columnExPubCol");
		}
		return columnExPubCol;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlInputRowSelect getRowSelectCampaign() {
		if (rowSelectCampaign == null) {
			rowSelectCampaign = (HtmlInputRowSelect) findComponentInRoot("rowSelectCampaign");
		}
		return rowSelectCampaign;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlPanelBox getBoxSelectionBoxesBottom() {
		if (boxSelectionBoxesBottom == null) {
			boxSelectionBoxesBottom = (HtmlPanelBox) findComponentInRoot("boxSelectionBoxesBottom");
		}
		return boxSelectionBoxesBottom;
	}

	protected HtmlOutputSelecticons getSelecticonsBoxBottom() {
		if (selecticonsBoxBottom == null) {
			selecticonsBoxBottom = (HtmlOutputSelecticons) findComponentInRoot("selecticonsBoxBottom");
		}
		return selecticonsBoxBottom;
	}

	protected HtmlOutputSelecticons getSelecticonsBoxTop() {
		if (selecticonsBoxTop == null) {
			selecticonsBoxTop = (HtmlOutputSelecticons) findComponentInRoot("selecticonsBoxTop");
		}
		return selecticonsBoxTop;
	}

	protected HtmlOutputText getTextCampName() {
		if (textCampName == null) {
			textCampName = (HtmlOutputText) findComponentInRoot("textCampName");
		}
		return textCampName;
	}

	protected HtmlOutputText getTextGroupName() {
		if (textGroupName == null) {
			textGroupName = (HtmlOutputText) findComponentInRoot("textGroupName");
		}
		return textGroupName;
	}

	protected HtmlOutputText getTextPubCode() {
		if (textPubCode == null) {
			textPubCode = (HtmlOutputText) findComponentInRoot("textPubCode");
		}
		return textPubCode;
	}

	protected HtmlOutputText getTextKeycode() {
		if (textKeycode == null) {
			textKeycode = (HtmlOutputText) findComponentInRoot("textKeycode");
		}
		return textKeycode;
	}

	protected HtmlOutputText getTextProdVanityLinkText() {
		if (textProdVanityLinkText == null) {
			textProdVanityLinkText = (HtmlOutputText) findComponentInRoot("textProdVanityLinkText");
		}
		return textProdVanityLinkText;
	}

	protected HtmlOutputLinkEx getLinkExVanityLink() {
		if (linkExVanityLink == null) {
			linkExVanityLink = (HtmlOutputLinkEx) findComponentInRoot("linkExVanityLink");
		}
		return linkExVanityLink;
	}

	protected HtmlPanelFormBox getFormBoxGroupSettingsFormBox() {
		if (formBoxGroupSettingsFormBox == null) {
			formBoxGroupSettingsFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxGroupSettingsFormBox");
		}
		return formBoxGroupSettingsFormBox;
	}

	protected HtmlMessages getMessagesErrrorMessages() {
		if (messagesErrrorMessages == null) {
			messagesErrrorMessages = (HtmlMessages) findComponentInRoot("messagesErrrorMessages");
		}
		return messagesErrrorMessages;
	}

	/** 
	 * @managed-bean true
	 */
	protected ManageGroupsHandler getCampaignGroupSettingsHandler() {
		if (campaignGroupSettingsHandler == null) {
			campaignGroupSettingsHandler = (ManageGroupsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignGroupSettingsHandler}").getValue(
							getFacesContext());
		}
		return campaignGroupSettingsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignGroupSettingsHandler(
			ManageGroupsHandler campaignGroupSettingsHandler) {
		this.campaignGroupSettingsHandler = campaignGroupSettingsHandler;
	}

	protected HtmlInputHelperTypeahead getTypeaheadGroupName() {
		if (typeaheadGroupName == null) {
			typeaheadGroupName = (HtmlInputHelperTypeahead) findComponentInRoot("typeaheadGroupName");
		}
		return typeaheadGroupName;
	}

	public String doButtonApplyGroupNameAction() {
		String responsePath = "success";
		
		Collection<CampaignHandler> campaigns = this.getEdittingCampaignsHandler().getCampaigns();
		
		String groupName = this.getCampaignGroupSettingsHandler().getGroupName();
		boolean isNewGroup = true;
		boolean isUTGroup = true;
		String existingPubCode = null;
		
		if (groupName.trim().length() == 0) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "You have entered an empty group name. Use the \"Remove Grouping\" button to clear the group name.", null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
			return responsePath;			
		}
		
		try {
			// process any existing campaigns in the group.
			Collection<UsatCampaignIntf> existingGroupMembers = UsatCampaignBO.fetchCampaignsInGroup(groupName);
			if (existingGroupMembers.size() > 0) {
				// existing group
				isNewGroup = false;
				existingPubCode = existingGroupMembers.iterator().next().getPubCode();
				if (existingPubCode.equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
					isUTGroup = false;
				}
			}
			
			// keep track of number of different pubs. all must be same pub.
			int numUT = 0;
			int numSW = 0;
			
			for (CampaignHandler ch : campaigns) {
				if (!ch.isCampaignSelected()) {
					continue;
				}
				if (ch.getPubcode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
					numUT++;
				}
				else {
					numSW++;
				}
				
				if (numSW > 0 && numUT > 0) {
					// error condition
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "You have selected campaigns belonging to multiple publications. Please select campaigns belonging to the same publication for grouping.", null);
					getFacesContext().addMessage(null, message);
					responsePath = "failure";
					return responsePath;			
					
				}
			} // end for first time
			
			if (!isNewGroup) {
				if (isUTGroup && (numSW > 0)) {
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The selected group name is pre-existing and is associated with USA TODAY campaigns. Choose a different group name.", null);
					getFacesContext().addMessage(null, message);
					responsePath = "failure";
					return responsePath;			
				}
				else if (!isUTGroup && (numUT > 0)) {
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The selected group name is pre-existing and is associated with Sports Weekly campaigns. Choose a different group name.", null);
					getFacesContext().addMessage(null, message);
					responsePath = "failure";
					return responsePath;									
				}
			}

			// if we get here all is good.
			// apply the group name to the campaigns
			for (CampaignHandler ch : campaigns) {
				if (ch.isCampaignSelected()) {
					ch.getCampaign().setCampaignGroup(groupName);
				}
			}
			
			if (numSW > 0 || numUT > 0) {
				this.getCampaignGroupSettingsHandler().setChanged(true);
			}
			else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "No Campaigns were updated.", null);
				getFacesContext().addMessage(null, message);				
			}
		}
		catch (Exception e){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Exception: " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
			return responsePath;			
		}
		return responsePath;
	}

	public String doButtonClearGroupNamesAction() {
		// Type Java code that runs when the component is clicked
		String responsePath = "success";
	
		Collection<CampaignHandler> campaigns = this.getEdittingCampaignsHandler().getCampaigns();
		
		try {
			
			// keep track of number of different pubs. all must be same pub.
			int numCleared = 0;
			
			for (CampaignHandler ch : campaigns) {
				if (ch.isCampaignSelected()) {
					numCleared++;
	
					// clear the group name.
					ch.getCampaign().setCampaignGroup("");
				}
			}
			
			if (numCleared > 0) {
				this.getCampaignGroupSettingsHandler().setChanged(true);
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The group name was cleared for " + numCleared + " campaigns. Click Save to to make the changes permanent.", null);
				getFacesContext().addMessage(null, message);
			}
			else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "No campaigns were processed.", null);
				getFacesContext().addMessage(null, message);
			}
			
		}
		catch (Exception e){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Exception: " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
			return responsePath;			
		}
		return responsePath;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// set to null so group suggestions include all pubs
		this.getGroupNameSuggestions().setPubCode(null);
	
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignGroupNameSuggestionsHandler getGroupNameSuggestions() {
		if (groupNameSuggestions == null) {
			groupNameSuggestions = (CampaignGroupNameSuggestionsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{groupNameSuggestions}").getValue(
							getFacesContext());
		}
		return groupNameSuggestions;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGroupNameSuggestions(
			CampaignGroupNameSuggestionsHandler groupNameSuggestions) {
		this.groupNameSuggestions = groupNameSuggestions;
	}

	protected HtmlPanelBox getBoxFooterBox() {
		if (boxFooterBox == null) {
			boxFooterBox = (HtmlPanelBox) findComponentInRoot("boxFooterBox");
		}
		return boxFooterBox;
	}

	protected HtmlPanelGrid getGridButtonGrid() {
		if (gridButtonGrid == null) {
			gridButtonGrid = (HtmlPanelGrid) findComponentInRoot("gridButtonGrid");
		}
		return gridButtonGrid;
	}

	protected HtmlCommandExButton getButtonApplyGroupName() {
		if (buttonApplyGroupName == null) {
			buttonApplyGroupName = (HtmlCommandExButton) findComponentInRoot("buttonApplyGroupName");
		}
		return buttonApplyGroupName;
	}

	protected HtmlCommandExButton getButtonClearGroupNames() {
		if (buttonClearGroupNames == null) {
			buttonClearGroupNames = (HtmlCommandExButton) findComponentInRoot("buttonClearGroupNames");
		}
		return buttonClearGroupNames;
	}

	protected HtmlInputText getTextNewGroupName() {
		if (textNewGroupName == null) {
			textNewGroupName = (HtmlInputText) findComponentInRoot("textNewGroupName");
		}
		return textNewGroupName;
	}

	protected HtmlFormItem getFormItemNewGroupName() {
		if (formItemNewGroupName == null) {
			formItemNewGroupName = (HtmlFormItem) findComponentInRoot("formItemNewGroupName");
		}
		return formItemNewGroupName;
	}

	protected HtmlPanelGrid getGridNameGrid() {
		if (gridNameGrid == null) {
			gridNameGrid = (HtmlPanelGrid) findComponentInRoot("gridNameGrid");
		}
		return gridNameGrid;
	}

	protected HtmlOutputText getTextChangedIndicator() {
		if (textChangedIndicator == null) {
			textChangedIndicator = (HtmlOutputText) findComponentInRoot("textChangedIndicator");
		}
		return textChangedIndicator;
	}

	protected HtmlOutputText getTextInfoLabel() {
		if (textInfoLabel == null) {
			textInfoLabel = (HtmlOutputText) findComponentInRoot("textInfoLabel");
		}
		return textInfoLabel;
	}

	protected HtmlOutputText getTextChangedIndicatorReadOnly() {
		if (textChangedIndicatorReadOnly == null) {
			textChangedIndicatorReadOnly = (HtmlOutputText) findComponentInRoot("textChangedIndicatorReadOnly");
		}
		return textChangedIndicatorReadOnly;
	}

	protected HtmlJspPanel getJspPanelMessagesPanelSave() {
		if (jspPanelMessagesPanelSave == null) {
			jspPanelMessagesPanelSave = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanelSave");
		}
		return jspPanelMessagesPanelSave;
	}

	protected HtmlOutputText getTextSaveLinkLabel() {
		if (textSaveLinkLabel == null) {
			textSaveLinkLabel = (HtmlOutputText) findComponentInRoot("textSaveLinkLabel");
		}
		return textSaveLinkLabel;
	}

	protected HtmlRequestLink getLinkSaveGroupChanges() {
		if (linkSaveGroupChanges == null) {
			linkSaveGroupChanges = (HtmlRequestLink) findComponentInRoot("linkSaveGroupChanges");
		}
		return linkSaveGroupChanges;
	}

	public String doLinkSaveGroupChangesAction() {
		Collection<CampaignHandler> campaigns = this.getEdittingCampaignsHandler().getCampaigns();
		try {
			for(CampaignHandler ch : campaigns) {
				UsatCampaignIntf c = ch.getCampaign();
				
				// save the campaign only (not promos)
				c.save(true);
				
			}
		}
		catch (Exception e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Exception: " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
		}
		return "success";
	}

	protected HtmlOutputLinkEx getLinkExBackToSearchPage() {
		if (linkExBackToSearchPage == null) {
			linkExBackToSearchPage = (HtmlOutputLinkEx) findComponentInRoot("linkExBackToSearchPage");
		}
		return linkExBackToSearchPage;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlGraphicImageEx getImageExSearchIcon() {
		if (imageExSearchIcon == null) {
			imageExSearchIcon = (HtmlGraphicImageEx) findComponentInRoot("imageExSearchIcon");
		}
		return imageExSearchIcon;
	}

}