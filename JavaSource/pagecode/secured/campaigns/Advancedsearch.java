/*
 * Created on Aug 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode.secured.campaigns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectOneRadio;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.AdvancedSearchHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.UISortHeader;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlOutputSelecticons;
import com.ibm.faces.component.html.HtmlOutputSeparator;
import com.ibm.faces.component.html.HtmlPagerWeb;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlSortHeader;
import com.usatoday.util.constants.UsaTodayConstants;
import javax.faces.component.UISelectItems;
import com.gannett.usat.dataHandlers.products.ProductsHandler;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Advancedsearch extends PageCodeBase {

	
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formAdvancedSearch;
	protected SearchResultsHandler currentSearchResults;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text5;
	protected HtmlDataTableEx tableExSearchResultsDataTable;
	protected HtmlMessages messages1;
	protected HtmlOutputText textCampaignName;
	protected HtmlOutputText text6;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textKeycodeHeaderLabel;
	protected UIColumnEx columnExKeycodeCol;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText textCampaignPub;
	protected HtmlOutputText textCampaignKeycode;
	protected HtmlSortHeader sortHeaderCName;
	protected HtmlInputRowSelect rowSelectSearchResults;
	protected UIColumnEx columnEx3;
	protected HtmlPanelBox boxFooterBox;
	protected HtmlOutputSelecticons selecticons1;
	protected HtmlPanelBox boxHeaderBox;
	protected HtmlOutputSelecticons selecticonsRowSelectionFooter;
	protected HtmlCommandExButton buttonDeleteSelected;
	protected HtmlCommandExButton buttonPublishCampaignsBottom;
	protected HtmlCommandExButton buttonPublishSelectedCampaigns;
	protected HtmlCommandExButton buttonDeleteSelected1;
	protected HtmlOutputSeparator separatorHR;
	protected HtmlOutputText text10;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText textCreatedDate;
	protected HtmlOutputText textCampaignStatus;
	protected HtmlCommandExButton buttonEditSelected;
	protected HtmlCommandExButton buttonEditSelectedBottom;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlOutputText textCriteriaLabelCampaignName;
	protected HtmlInputText textCampaignNameLike;
	protected HtmlOutputText textCriteriaLabelCampaignGroup;
	protected HtmlInputText textCampaignGroupLike;
	protected HtmlOutputText textCriteriaLabelKeycode;
	protected HtmlInputText textCampaignKeyCodeLike;
	protected HtmlOutputText textCriteriaLabelVanityURL;
	protected HtmlInputText textCampaignVanityLike;
	protected HtmlOutputText textCriteriaLabelCreatedDate;
	protected HtmlInputText textInsertTimeBegin;
	protected HtmlInputText textInsertTimeEnd;
	protected HtmlOutputText textCriteriaLabelPublications;
	protected HtmlSelectOneRadio radioPubSelection;
	protected HtmlOutputText textCriteriaLabelPublishingStatus;
	protected HtmlSelectOneRadio radioCampaignPubStatus;
	protected HtmlCommandExButton buttonSubmitSearch;
	protected HtmlOutputText textSeachResultsLabelText;
	protected HtmlPagerWeb webBottomPager;
	protected AdvancedSearchHandler advancedSearchCriteriaHandler;
	protected HtmlOutputText textSearchHelpStr;
	protected HtmlPanelGrid gridNumRowsToShow;
	protected HtmlInputText textRowsPerPageValue;
	protected HtmlOutputText textNumRowsLabel;
	protected HtmlCommandExButton buttonUpdateRowToShow;
	protected HtmlPanelGrid gridBottomPagerGrid;

	protected HtmlOutputText textProdVanityLinkText;

	protected HtmlOutputLinkEx linkExVanityLink;

	protected CampaignHandlerReadOnly defaultCampaignReadOnly;

	protected CampaignHandlerReadOnly swDefaultCampaign;

	protected CampaignHandlerReadOnly utDefaultCampaign;

	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected SearchResultsHandler publishingCampaignsHandler;
	protected SearchResultsHandler publishingCampaignsHandler1;
	protected HtmlCommandExButton buttonManageGroupSettings;
	protected HtmlCommandExButton buttonManageGroupSettingsBottom;
	protected UISelectItems selectItems1;
	protected ProductsHandler productHandler;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text1;
	protected HtmlOutputText textCampaignBrandingPub;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx10;
	protected HtmlOutputText text12;
	protected HtmlOutputText text7;
	protected UIColumnEx columnEx11;
	protected HtmlOutputText text13;
	protected HtmlOutputText text14;
	protected UIColumnEx columnEx12;
	protected HtmlOutputText text15;
	protected UIColumnEx columnEx13;
	protected HtmlOutputText text16;
	protected UIColumnEx columnEx14;
	protected HtmlOutputText text17;
	protected HtmlOutputText text18;
	protected HtmlOutputText text19;
	protected HtmlSortHeader sortHeaderByKeycode;
	protected HtmlSortHeader sortHeaderByCampGroup;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText textCampaignGroup;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	protected HtmlForm getFormAdvancedSearch() {
		if (formAdvancedSearch == null) {
			formAdvancedSearch = (HtmlForm) findComponentInRoot("formAdvancedSearch");
		}
		return formAdvancedSearch;
	}
	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getCurrentSearchResults() {
		if (currentSearchResults == null) {
			currentSearchResults = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{currentSearchResults}").getValue(
							getFacesContext());
		}
		return currentSearchResults;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCurrentSearchResults(
			SearchResultsHandler currentSearchResults) {
		this.currentSearchResults = currentSearchResults;
	}
	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}
	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}
	protected HtmlDataTableEx getTableExSearchResultsDataTable() {
		if (tableExSearchResultsDataTable == null) {
			tableExSearchResultsDataTable = (HtmlDataTableEx) findComponentInRoot("tableExSearchResultsDataTable");
		}
		return tableExSearchResultsDataTable;
	}
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
	protected HtmlOutputText getTextCampaignName() {
		if (textCampaignName == null) {
			textCampaignName = (HtmlOutputText) findComponentInRoot("textCampaignName");
		}
		return textCampaignName;
	}
	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}
	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}
	protected HtmlOutputText getTextKeycodeHeaderLabel() {
		if (textKeycodeHeaderLabel == null) {
			textKeycodeHeaderLabel = (HtmlOutputText) findComponentInRoot("textKeycodeHeaderLabel");
		}
		return textKeycodeHeaderLabel;
	}
	protected UIColumnEx getColumnExKeycodeCol() {
		if (columnExKeycodeCol == null) {
			columnExKeycodeCol = (UIColumnEx) findComponentInRoot("columnExKeycodeCol");
		}
		return columnExKeycodeCol;
	}
	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}
	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}
	protected HtmlOutputText getTextCampaignPub() {
		if (textCampaignPub == null) {
			textCampaignPub = (HtmlOutputText) findComponentInRoot("textCampaignPub");
		}
		return textCampaignPub;
	}
	protected HtmlOutputText getTextCampaignKeycode() {
		if (textCampaignKeycode == null) {
			textCampaignKeycode = (HtmlOutputText) findComponentInRoot("textCampaignKeycode");
		}
		return textCampaignKeycode;
	}
	protected HtmlSortHeader getSortHeaderCName() {
		if (sortHeaderCName == null) {
			sortHeaderCName = (HtmlSortHeader) findComponentInRoot("sortHeaderCName");
		}
		return sortHeaderCName;
	}
	public String doSortHeaderCNameAction() {
		// Type Java code that runs when the component is clicked
	
		// get the id of this sort header component
		//String sortHeaderId = UISortHeader.getSortHeaderId();
		
		//FacesContext context = FacesContext.getCurrentInstance();
		String state = UISortHeader.getCurrentSortOrder();

		String order = "asc";
		
		if(state!=null) {

			
			if(state.equals(UISortHeader.SORT_BIDOWN)) {
				order = "desc";
			}

			this.getCurrentSearchResults().setSortCol(SearchResultsHandler.SORTBY_CAMP_NAME);
			this.getCurrentSearchResults().setSortOrder(order);
		}
		
		return "";
	}
	public String doSortHeaderByKeycodeAction() {
		// Type Java code that runs when the component is clicked

		String state = UISortHeader.getCurrentSortOrder();

		String order = "asc";
		
		if(state!=null) {

			
			if(state.equals(UISortHeader.SORT_BIDOWN)) {
				order = "desc";
			}

			// sort the data
			this.getCurrentSearchResults().setSortCol(SearchResultsHandler.SORTBY_KEYCODE);
			this.getCurrentSearchResults().setSortOrder(order);
			
		}
	
		return "";
	}
	protected HtmlInputRowSelect getRowSelectSearchResults() {
		if (rowSelectSearchResults == null) {
			rowSelectSearchResults = (HtmlInputRowSelect) findComponentInRoot("rowSelectSearchResults");
		}
		return rowSelectSearchResults;
	}
	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}
	protected HtmlPanelBox getBoxFooterBox() {
		if (boxFooterBox == null) {
			boxFooterBox = (HtmlPanelBox) findComponentInRoot("boxFooterBox");
		}
		return boxFooterBox;
	}
	protected HtmlOutputSelecticons getSelecticons1() {
		if (selecticons1 == null) {
			selecticons1 = (HtmlOutputSelecticons) findComponentInRoot("selecticons1");
		}
		return selecticons1;
	}
	protected HtmlPanelBox getBoxHeaderBox() {
		if (boxHeaderBox == null) {
			boxHeaderBox = (HtmlPanelBox) findComponentInRoot("boxHeaderBox");
		}
		return boxHeaderBox;
	}
	protected HtmlOutputSelecticons getSelecticonsRowSelectionFooter() {
		if (selecticonsRowSelectionFooter == null) {
			selecticonsRowSelectionFooter = (HtmlOutputSelecticons) findComponentInRoot("selecticonsRowSelectionFooter");
		}
		return selecticonsRowSelectionFooter;
	}
	protected HtmlCommandExButton getButtonDeleteSelected() {
		if (buttonDeleteSelected == null) {
			buttonDeleteSelected = (HtmlCommandExButton) findComponentInRoot("buttonDeleteSelected");
		}
		return buttonDeleteSelected;
	}
	protected HtmlCommandExButton getButtonPublishCampaignsBottom() {
		if (buttonPublishCampaignsBottom == null) {
			buttonPublishCampaignsBottom = (HtmlCommandExButton) findComponentInRoot("buttonPublishCampaignsBottom");
		}
		return buttonPublishCampaignsBottom;
	}
	protected HtmlCommandExButton getButtonPublishSelectedCampaigns() {
		if (buttonPublishSelectedCampaigns == null) {
			buttonPublishSelectedCampaigns = (HtmlCommandExButton) findComponentInRoot("buttonPublishSelectedCampaigns");
		}
		return buttonPublishSelectedCampaigns;
	}
	protected HtmlCommandExButton getButtonDeleteSelected1() {
		if (buttonDeleteSelected1 == null) {
			buttonDeleteSelected1 = (HtmlCommandExButton) findComponentInRoot("buttonDeleteSelected1");
		}
		return buttonDeleteSelected1;
	}
	protected HtmlOutputSeparator getSeparatorHR() {
		if (separatorHR == null) {
			separatorHR = (HtmlOutputSeparator) findComponentInRoot("separatorHR");
		}
		return separatorHR;
	}
	public String doSortHeaderByCampGroupAction() {
		// Type Java code that runs when the component is clicked
	
		String state = UISortHeader.getCurrentSortOrder();

		String order = "asc";
		
		if(state!=null) {

			
			if(state.equals(UISortHeader.SORT_BIDOWN)) {
				order = "desc";
			}

			this.getCurrentSearchResults().setSortCol(SearchResultsHandler.SORTBY_GROUP_NAME);
			this.getCurrentSearchResults().setSortOrder(order);
			// sort the data
		}
		
		return "";
	}
	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}
	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}
	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}
	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}
	protected HtmlOutputText getTextCreatedDate() {
		if (textCreatedDate == null) {
			textCreatedDate = (HtmlOutputText) findComponentInRoot("textCreatedDate");
		}
		return textCreatedDate;
	}
	protected HtmlOutputText getTextCampaignStatus() {
		if (textCampaignStatus == null) {
			textCampaignStatus = (HtmlOutputText) findComponentInRoot("textCampaignStatus");
		}
		return textCampaignStatus;
	}
	public String doButtonSubmitSearchAction() {
		// Type Java code that runs when the component is clicked
	
		String responsePath = "success";
		try {
			AdvancedSearchHandler h = this.getAdvancedSearchCriteriaHandler();
			
			
			DateTime after = null;
			if (h.getInsertedAfter() != null) {
				after = new DateTime(h.getInsertedAfter());
			}
			DateTime before = null;
			if (h.getInsertedBefore() != null) {
				before = new DateTime(h.getInsertedBefore());
			}
			
			this.getCurrentSearchResults().setSearchResults(UsatCampaignBO.fetchCampaignsMeetingSpecificCriteria(h.getCampaignNameLike(), 
					h.getCampaignGroupLike(), h.getCampaignKeyCodeLike(), h.getCampaignVanityLike(), h.getPublication(), h.getPublishingStatus(), after, before));
			
			this.getTableExSearchResultsDataTable().setRows(this.getCurrentSearchResults().getNumRecordsPerPage());
		}
		catch (Exception e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exception Searching: " + e.getMessage(), "Please try again.");
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
		}
		
		return responsePath;
	}
	protected HtmlCommandExButton getButtonEditSelected() {
		if (buttonEditSelected == null) {
			buttonEditSelected = (HtmlCommandExButton) findComponentInRoot("buttonEditSelected");
		}
		return buttonEditSelected;
	}
	
	public String doButtonEditSelectedAction() {
	
		
		String responsePath = "success";
		ArrayList<CampaignHandler> selectedItems = new ArrayList<CampaignHandler>();

		boolean differentPubs = false;
		
		String pubCode = null;
		USATProductBO prod = null;
		
		HashMap<String, String> groupNames = new HashMap<String, String>();
		
		for (CampaignHandler campaign : this.getCurrentSearchResults().getLastSearchResults()) {

			if (campaign.isCampaignSelected()) {
				if (pubCode == null) {
					pubCode = campaign.getPubcode();
					prod = this.getProductHandler().getProductForPub(pubCode);
				}
				else {
					if (!campaign.getPubcode().equalsIgnoreCase(pubCode)) {
						differentPubs = true;
					}
				}
				String groupName = campaign.getCampaignGroup();
				// if campaign in a group
				if (groupName != null && groupName.trim().length() > 0) {
					if (!groupNames.containsKey(groupName)) {
						// add group name to list of group names
						groupNames.put(groupName, groupName);
					}
				}
				
				// unselect campaign
				campaign.setCampaignSelected(false);
				selectedItems.add(campaign);
			}
		}

		// check for all same pub
		if (selectedItems.size() == 0) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select at least one campaign to continue.", null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
			return responsePath;			
		}
		
		// check for all same pub
		if (differentPubs) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "You may not edit USA TODAY and Sports Weekly campaigns at the same time. Please choose either all USATODAY or all Sports Weekly campaigns.", "Please try again.");
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
			// clear selected items
			selectedItems.clear();
			return responsePath;			
		}

		// if multiple groups being editted then confirm them
		if (groupNames.size() > 0) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "You have selected a campaign belonging to one or more groups. All campaigns belonging to all " + groupNames.size() + " group(s) will be modified.", null);
			getFacesContext().addMessage(null, message);

			FacesMessage message2 = new FacesMessage(FacesMessage.SEVERITY_WARN, "To edit a subset of campaigns belonging to a group, you must first remove them from a their group.", null);
			getFacesContext().addMessage(null, message2);

			// generate a collection of all campaigns that will be affected.
			// start with the selected campaigns
			HashMap<Integer, CampaignHandler> fullSelection = new HashMap<Integer, CampaignHandler>();
			for (CampaignHandler c : selectedItems) {
				fullSelection.put(c.getCampaignID(), c);
			}
			// now add all campaigns in the db belonging to the same group.
			for (String groupName : groupNames.values()) {
				try {
					Collection<UsatCampaignIntf> groupCampaigns = UsatCampaignBO.fetchCampaignsInGroup(groupName);
					for (UsatCampaignIntf c : groupCampaigns) {
						if (!fullSelection.containsKey(c.getPrimaryKey())) {
							CampaignHandler newCH = new CampaignHandler();
							newCH.setCampaign(c);
							fullSelection.put(c.getPrimaryKey(), newCH);
						}					
					}
				}
				catch (Exception exp) {
					System.out.println("Exception pulling all campaigns for all groups. " + exp.getMessage());
					FacesMessage message3 = new FacesMessage(FacesMessage.SEVERITY_ERROR, exp.getMessage(), null);
					getFacesContext().addMessage(null, message3);
				}
			}// end for all groups
			
			selectedItems.clear();
			selectedItems.addAll(fullSelection.values());
			// if only one group go ahead and default a campaign to selected.
			if (groupNames.size() == 1) {
				selectedItems.get(0).setCampaignSelected(true);
			}
			responsePath = "confirm";
		}// end if multiple groups
		else {
			CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
			
			CampaignHandler dCamp = new CampaignHandler();
			
			if (prod.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
				dCamp.setCampaign(this.getUtDefaultCampaign().getCampaign());
			}
			else {
				dCamp.setCampaign(this.getSwDefaultCampaign().getCampaign());
			}
			this.getDefaultCampaignReadOnly().setSourceCampaign(dCamp);
			editor.setDefaultCampaign(dCamp);
			editor.setSourceCampaign(selectedItems.get(0));
		}
		
		this.getEdittingCampaignsHandler().setCampaigns(selectedItems);
		return responsePath;
	
	}
	protected HtmlCommandExButton getButtonEditSelectedBottom() {
		if (buttonEditSelectedBottom == null) {
			buttonEditSelectedBottom = (HtmlCommandExButton) findComponentInRoot("buttonEditSelectedBottom");
		}
		return buttonEditSelectedBottom;
	}
	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding("#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}
	protected HtmlOutputText getTextCriteriaLabelCampaignName() {
		if (textCriteriaLabelCampaignName == null) {
			textCriteriaLabelCampaignName = (HtmlOutputText) findComponentInRoot("textCriteriaLabelCampaignName");
		}
		return textCriteriaLabelCampaignName;
	}
	protected HtmlInputText getTextCampaignNameLike() {
		if (textCampaignNameLike == null) {
			textCampaignNameLike = (HtmlInputText) findComponentInRoot("textCampaignNameLike");
		}
		return textCampaignNameLike;
	}
	protected HtmlOutputText getTextCriteriaLabelCampaignGroup() {
		if (textCriteriaLabelCampaignGroup == null) {
			textCriteriaLabelCampaignGroup = (HtmlOutputText) findComponentInRoot("textCriteriaLabelCampaignGroup");
		}
		return textCriteriaLabelCampaignGroup;
	}
	protected HtmlInputText getTextCampaignGroupLike() {
		if (textCampaignGroupLike == null) {
			textCampaignGroupLike = (HtmlInputText) findComponentInRoot("textCampaignGroupLike");
		}
		return textCampaignGroupLike;
	}
	protected HtmlOutputText getTextCriteriaLabelKeycode() {
		if (textCriteriaLabelKeycode == null) {
			textCriteriaLabelKeycode = (HtmlOutputText) findComponentInRoot("textCriteriaLabelKeycode");
		}
		return textCriteriaLabelKeycode;
	}
	protected HtmlInputText getTextCampaignKeyCodeLike() {
		if (textCampaignKeyCodeLike == null) {
			textCampaignKeyCodeLike = (HtmlInputText) findComponentInRoot("textCampaignKeyCodeLike");
		}
		return textCampaignKeyCodeLike;
	}
	protected HtmlOutputText getTextCriteriaLabelVanityURL() {
		if (textCriteriaLabelVanityURL == null) {
			textCriteriaLabelVanityURL = (HtmlOutputText) findComponentInRoot("textCriteriaLabelVanityURL");
		}
		return textCriteriaLabelVanityURL;
	}
	protected HtmlInputText getTextCampaignVanityLike() {
		if (textCampaignVanityLike == null) {
			textCampaignVanityLike = (HtmlInputText) findComponentInRoot("textCampaignVanityLike");
		}
		return textCampaignVanityLike;
	}
	protected HtmlOutputText getTextCriteriaLabelCreatedDate() {
		if (textCriteriaLabelCreatedDate == null) {
			textCriteriaLabelCreatedDate = (HtmlOutputText) findComponentInRoot("textCriteriaLabelCreatedDate");
		}
		return textCriteriaLabelCreatedDate;
	}
	protected HtmlInputText getTextInsertTimeBegin() {
		if (textInsertTimeBegin == null) {
			textInsertTimeBegin = (HtmlInputText) findComponentInRoot("textInsertTimeBegin");
		}
		return textInsertTimeBegin;
	}
	protected HtmlInputText getTextInsertTimeEnd() {
		if (textInsertTimeEnd == null) {
			textInsertTimeEnd = (HtmlInputText) findComponentInRoot("textInsertTimeEnd");
		}
		return textInsertTimeEnd;
	}
	protected HtmlOutputText getTextCriteriaLabelPublications() {
		if (textCriteriaLabelPublications == null) {
			textCriteriaLabelPublications = (HtmlOutputText) findComponentInRoot("textCriteriaLabelPublications");
		}
		return textCriteriaLabelPublications;
	}
	protected HtmlSelectOneRadio getRadioPubSelection() {
		if (radioPubSelection == null) {
			radioPubSelection = (HtmlSelectOneRadio) findComponentInRoot("radioPubSelection");
		}
		return radioPubSelection;
	}
	protected HtmlOutputText getTextCriteriaLabelPublishingStatus() {
		if (textCriteriaLabelPublishingStatus == null) {
			textCriteriaLabelPublishingStatus = (HtmlOutputText) findComponentInRoot("textCriteriaLabelPublishingStatus");
		}
		return textCriteriaLabelPublishingStatus;
	}
	protected HtmlSelectOneRadio getRadioCampaignPubStatus() {
		if (radioCampaignPubStatus == null) {
			radioCampaignPubStatus = (HtmlSelectOneRadio) findComponentInRoot("radioCampaignPubStatus");
		}
		return radioCampaignPubStatus;
	}
	protected HtmlCommandExButton getButtonSubmitSearch() {
		if (buttonSubmitSearch == null) {
			buttonSubmitSearch = (HtmlCommandExButton) findComponentInRoot("buttonSubmitSearch");
		}
		return buttonSubmitSearch;
	}
	protected HtmlOutputText getTextSeachResultsLabelText() {
		if (textSeachResultsLabelText == null) {
			textSeachResultsLabelText = (HtmlOutputText) findComponentInRoot("textSeachResultsLabelText");
		}
		return textSeachResultsLabelText;
	}
	protected HtmlPagerWeb getWebBottomPager() {
		if (webBottomPager == null) {
			webBottomPager = (HtmlPagerWeb) findComponentInRoot("webBottomPager");
		}
		return webBottomPager;
	}
	/** 
	 * @managed-bean true
	 */
	protected AdvancedSearchHandler getAdvancedSearchCriteriaHandler() {
		if (advancedSearchCriteriaHandler == null) {
			advancedSearchCriteriaHandler = (AdvancedSearchHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{advancedSearchCriteriaHandler}").getValue(
							getFacesContext());
		}
		return advancedSearchCriteriaHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setAdvancedSearchCriteriaHandler(
			AdvancedSearchHandler advancedSearchCriteriaHandler) {
		this.advancedSearchCriteriaHandler = advancedSearchCriteriaHandler;
	}
	protected HtmlOutputText getTextSearchHelpStr() {
		if (textSearchHelpStr == null) {
			textSearchHelpStr = (HtmlOutputText) findComponentInRoot("textSearchHelpStr");
		}
		return textSearchHelpStr;
	}
	protected HtmlPanelGrid getGridNumRowsToShow() {
		if (gridNumRowsToShow == null) {
			gridNumRowsToShow = (HtmlPanelGrid) findComponentInRoot("gridNumRowsToShow");
		}
		return gridNumRowsToShow;
	}
	protected HtmlInputText getTextRowsPerPageValue() {
		if (textRowsPerPageValue == null) {
			textRowsPerPageValue = (HtmlInputText) findComponentInRoot("textRowsPerPageValue");
		}
		return textRowsPerPageValue;
	}
	protected HtmlOutputText getTextNumRowsLabel() {
		if (textNumRowsLabel == null) {
			textNumRowsLabel = (HtmlOutputText) findComponentInRoot("textNumRowsLabel");
		}
		return textNumRowsLabel;
	}
	protected HtmlCommandExButton getButtonUpdateRowToShow() {
		if (buttonUpdateRowToShow == null) {
			buttonUpdateRowToShow = (HtmlCommandExButton) findComponentInRoot("buttonUpdateRowToShow");
		}
		return buttonUpdateRowToShow;
	}
	
	public String doButtonDeleteSelectedCampaignsAction() {
		// Type Java code that runs when the component is clicked
	
		boolean hasErrors = false;			
		
		Collection<CampaignHandler> campaigns = this.getCurrentSearchResults().getLastSearchResults();
		ArrayList<UsatCampaignIntf> newCampaignList = new ArrayList<UsatCampaignIntf>();
		
		Iterator<CampaignHandler> campaignsItr = campaigns.iterator();
		while (campaignsItr.hasNext()) {
			CampaignHandler campaign = campaignsItr.next();
			if (campaign.isCampaignSelected()) {
				//delete campaign
				try {					
					campaign.getCampaign().delete();

					// remove campaign from search results.
					campaignsItr.remove();
				}
				catch (Exception e) {
					hasErrors = true;
					// add the errored item to the list of campaigns still avaialbe
					UsatCampaignIntf c = campaign.getCampaign();
					newCampaignList.add(c);
					// add informative message
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Campaign: " + campaign.getCampaignName() + " deleted");
					getFacesContext().addMessage(null, message);
				}
			}
			else {
				// add to new list of campaigns for redesplay (not deleted campaigns)
				UsatCampaignIntf c = campaign.getCampaign();
				newCampaignList.add(c);
			}
			 
		}
		
		// updaet the search results with the collection minus the removed items.
		this.getCurrentSearchResults().setSearchResults(newCampaignList);
		
		if (hasErrors) {
			return "failure";
		}
		else {
			// setting search results to null will force them to be reloaded next page load.
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected Campaigns were Deleted.", "");
			getFacesContext().addMessage(null, message);
			return "success";
		}
	}

	
	
	
	
	
	
	public String doButtonUpdateRowToShowAction() {
		// Type Java code that runs when the component is clicked
	
		// return "success"; // global 
		if (this.getCurrentSearchResults().getNumRecordsPerPage() > 0) {
			this.getTableExSearchResultsDataTable().setRows(this.getCurrentSearchResults().getNumRecordsPerPage());
		}
		else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Number of rows must be an integer greater than 1.", "Please try again.");
			getFacesContext().addMessage(null, message);			
		}
		
		return "";
	}
	protected HtmlPanelGrid getGridBottomPagerGrid() {
		if (gridBottomPagerGrid == null) {
			gridBottomPagerGrid = (HtmlPanelGrid) findComponentInRoot("gridBottomPagerGrid");
		}
		return gridBottomPagerGrid;
	}
	protected HtmlOutputText getTextProdVanityLinkText() {
		if (textProdVanityLinkText == null) {
			textProdVanityLinkText = (HtmlOutputText) findComponentInRoot("textProdVanityLinkText");
		}
		return textProdVanityLinkText;
	}
	protected HtmlOutputLinkEx getLinkExVanityLink() {
		if (linkExVanityLink == null) {
			linkExVanityLink = (HtmlOutputLinkEx) findComponentInRoot("linkExVanityLink");
		}
		return linkExVanityLink;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getDefaultCampaignReadOnly() {
		if (defaultCampaignReadOnly == null) {
			defaultCampaignReadOnly = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication().createValueBinding(
							"#{defaultCampaignReadOnly}").getValue(
							getFacesContext());
		}
		return defaultCampaignReadOnly;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setDefaultCampaignReadOnly(
			CampaignHandlerReadOnly defaultCampaignReadOnly) {
		this.defaultCampaignReadOnly = defaultCampaignReadOnly;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getSwDefaultCampaign() {
		if (swDefaultCampaign == null) {
			swDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{swDefaultCampaign}").getValue(
							getFacesContext());
		}
		return swDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setSwDefaultCampaign(
			CampaignHandlerReadOnly swDefaultCampaign) {
		this.swDefaultCampaign = swDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getUtDefaultCampaign() {
		if (utDefaultCampaign == null) {
			utDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{utDefaultCampaign}").getValue(
							getFacesContext());
		}
		return utDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUtDefaultCampaign(
			CampaignHandlerReadOnly utDefaultCampaign) {
		this.utDefaultCampaign = utDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}
	public String doButtonPublishSelectedCampaignsAction() {
		// Type Java code that runs when the component is clicked
	
		String responsePath = "success";
		ArrayList<CampaignHandler> selectedItems = new ArrayList<CampaignHandler>();
		
		int numSelected = 0;
		for (CampaignHandler campaign : this.getCurrentSearchResults().getLastSearchResults()) {

			if (campaign.isCampaignSelected()) {
				selectedItems.add(campaign);
				numSelected++;
			}
			
		}
		
		if (numSelected == 0) {
			// return error
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "No campaigns were selected to publish. Please check each campaign you'd like to publish.", "");
			this.getFacesContext().addMessage(null, m);
			responsePath = "failure";
		}
		else {
			this.getPublishingCampaignsHandler().setCollectionData(selectedItems);
		}
		
		return responsePath;
	}
	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler() {
		if (publishingCampaignsHandler == null) {
			publishingCampaignsHandler = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{publishingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return publishingCampaignsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler(
			SearchResultsHandler publishingCampaignsHandler) {
		this.publishingCampaignsHandler = publishingCampaignsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getPublishingCampaignsHandler1() {
		if (publishingCampaignsHandler1 == null) {
			publishingCampaignsHandler1 = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{publishingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return publishingCampaignsHandler1;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setPublishingCampaignsHandler1(
			SearchResultsHandler publishingCampaignsHandler1) {
		this.publishingCampaignsHandler1 = publishingCampaignsHandler1;
	}
	protected HtmlCommandExButton getButtonManageGroupSettings() {
		if (buttonManageGroupSettings == null) {
			buttonManageGroupSettings = (HtmlCommandExButton) findComponentInRoot("buttonManageGroupSettings");
		}
		return buttonManageGroupSettings;
	}
	public String doButtonManageGroupSettingsAction() {
		String responsePath = "success";
		ArrayList<CampaignHandler> selectedItems = new ArrayList<CampaignHandler>();

		for (CampaignHandler campaign : this.getCurrentSearchResults().getLastSearchResults()) {

			if (campaign.isCampaignSelected()) {
				// unselect campaign
				CampaignHandler newCH = new CampaignHandler();
				newCH.setCampaign(campaign.getCampaign());
				// default all campaigns to selected.
				newCH.setCampaignSelected(true);
				selectedItems.add(newCH);
			}
		}
		
		// check for all same pub
		if (selectedItems.size() == 0) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select at least one campaign to continue.", null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
			return responsePath;			
		}
		
		// set the collection of campaigns to edit
		this.getEdittingCampaignsHandler().setCampaigns(selectedItems);
		
		return responsePath;
	}
	protected HtmlCommandExButton getButtonManageGroupSettingsBottom() {
		if (buttonManageGroupSettingsBottom == null) {
			buttonManageGroupSettingsBottom = (HtmlCommandExButton) findComponentInRoot("buttonManageGroupSettingsBottom");
		}
		return buttonManageGroupSettingsBottom;
	}
	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}
	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getProductHandler() {
		if (productHandler == null) {
			productHandler = (ProductsHandler) getManagedBean("productHandler");
		}
		return productHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setProductHandler(ProductsHandler productHandler) {
		this.productHandler = productHandler;
	}
	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected HtmlOutputText getTextCampaignBrandingPub() {
		if (textCampaignBrandingPub == null) {
			textCampaignBrandingPub = (HtmlOutputText) findComponentInRoot("textCampaignBrandingPub");
		}
		return textCampaignBrandingPub;
	}
	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}
	protected UIColumnEx getColumnEx10() {
		if (columnEx10 == null) {
			columnEx10 = (UIColumnEx) findComponentInRoot("columnEx10");
		}
		return columnEx10;
	}
	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}
	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}
	protected UIColumnEx getColumnEx11() {
		if (columnEx11 == null) {
			columnEx11 = (UIColumnEx) findComponentInRoot("columnEx11");
		}
		return columnEx11;
	}
	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}
	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}
	protected UIColumnEx getColumnEx12() {
		if (columnEx12 == null) {
			columnEx12 = (UIColumnEx) findComponentInRoot("columnEx12");
		}
		return columnEx12;
	}
	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}
	protected UIColumnEx getColumnEx13() {
		if (columnEx13 == null) {
			columnEx13 = (UIColumnEx) findComponentInRoot("columnEx13");
		}
		return columnEx13;
	}
	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}
	protected UIColumnEx getColumnEx14() {
		if (columnEx14 == null) {
			columnEx14 = (UIColumnEx) findComponentInRoot("columnEx14");
		}
		return columnEx14;
	}
	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}
	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}
	protected HtmlOutputText getText19() {
		if (text19 == null) {
			text19 = (HtmlOutputText) findComponentInRoot("text19");
		}
		return text19;
	}
	protected HtmlSortHeader getSortHeaderByKeycode() {
		if (sortHeaderByKeycode == null) {
			sortHeaderByKeycode = (HtmlSortHeader) findComponentInRoot("sortHeaderByKeycode");
		}
		return sortHeaderByKeycode;
	}
	protected HtmlSortHeader getSortHeaderByCampGroup() {
		if (sortHeaderByCampGroup == null) {
			sortHeaderByCampGroup = (HtmlSortHeader) findComponentInRoot("sortHeaderByCampGroup");
		}
		return sortHeaderByCampGroup;
	}
	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}
	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}
	protected HtmlOutputText getTextCampaignGroup() {
		if (textCampaignGroup == null) {
			textCampaignGroup = (HtmlOutputText) findComponentInRoot("textCampaignGroup");
		}
		return textCampaignGroup;
	}
}