/**
 * 
 */
package pagecode.secured.campaigns;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 *
 */
public class Confirm_selection extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formConfirmSelection;
	protected HtmlPanelGrid gridMainLayoutGrid;
	protected HtmlMessages messages1;
	protected UIColumnEx columnExGroup;
	protected HtmlOutputText textGroupNameLabel;
	protected HtmlDataTableEx tableExSelectedCampaignsToEdit;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected CampaignEditsHandler campaignEditHandler;
	protected CampaignHandlerReadOnly defaultCampaignReadOnly;
	protected HtmlOutputText textLinkDescription;
	protected HtmlOutputLinkEx linkEx1BackToSearch;
	protected HtmlOutputText text1;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text2;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text3;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx4;
	protected HtmlInputRowSelect rowSelect1;
	protected UIColumnEx columnEx5;
	protected HtmlOutputText text5;
	protected HtmlOutputText textCampName;
	protected HtmlOutputText textPub;
	protected HtmlOutputText textKeycode;
	protected HtmlOutputText textPubStatus;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText textVanLabel;
	protected HtmlOutputLinkEx linkExVanLink;
	protected HtmlPanelBox box1;
	protected HtmlCommandExButton buttonConfirmSelection;
	protected CampaignHandlerReadOnly utDefaultCampaign;
	protected CampaignHandlerReadOnly swDefaultCampaign;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected HtmlOutputText textGroupName;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormConfirmSelection() {
		if (formConfirmSelection == null) {
			formConfirmSelection = (HtmlForm) findComponentInRoot("formConfirmSelection");
		}
		return formConfirmSelection;
	}

	protected HtmlPanelGrid getGridMainLayoutGrid() {
		if (gridMainLayoutGrid == null) {
			gridMainLayoutGrid = (HtmlPanelGrid) findComponentInRoot("gridMainLayoutGrid");
		}
		return gridMainLayoutGrid;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected UIColumnEx getColumnExGroup() {
		if (columnExGroup == null) {
			columnExGroup = (UIColumnEx) findComponentInRoot("columnExGroup");
		}
		return columnExGroup;
	}

	protected HtmlOutputText getTextGroupNameLabel() {
		if (textGroupNameLabel == null) {
			textGroupNameLabel = (HtmlOutputText) findComponentInRoot("textGroupNameLabel");
		}
		return textGroupNameLabel;
	}

	protected HtmlDataTableEx getTableExSelectedCampaignsToEdit() {
		if (tableExSelectedCampaignsToEdit == null) {
			tableExSelectedCampaignsToEdit = (HtmlDataTableEx) findComponentInRoot("tableExSelectedCampaignsToEdit");
		}
		return tableExSelectedCampaignsToEdit;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignEditHandler() {
		if (campaignEditHandler == null) {
			campaignEditHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignEditHandler}").getValue(
							getFacesContext());
		}
		return campaignEditHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignEditHandler(
			CampaignEditsHandler campaignEditHandler) {
		this.campaignEditHandler = campaignEditHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getDefaultCampaignReadOnly() {
		if (defaultCampaignReadOnly == null) {
			defaultCampaignReadOnly = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication().createValueBinding(
							"#{defaultCampaignReadOnly}").getValue(
							getFacesContext());
		}
		return defaultCampaignReadOnly;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setDefaultCampaignReadOnly(
			CampaignHandlerReadOnly defaultCampaignReadOnly) {
		this.defaultCampaignReadOnly = defaultCampaignReadOnly;
	}

	protected HtmlOutputText getTextLinkDescription() {
		if (textLinkDescription == null) {
			textLinkDescription = (HtmlOutputText) findComponentInRoot("textLinkDescription");
		}
		return textLinkDescription;
	}

	protected HtmlOutputLinkEx getLinkEx1BackToSearch() {
		if (linkEx1BackToSearch == null) {
			linkEx1BackToSearch = (HtmlOutputLinkEx) findComponentInRoot("linkEx1BackToSearch");
		}
		return linkEx1BackToSearch;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlInputRowSelect getRowSelect1() {
		if (rowSelect1 == null) {
			rowSelect1 = (HtmlInputRowSelect) findComponentInRoot("rowSelect1");
		}
		return rowSelect1;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getTextCampName() {
		if (textCampName == null) {
			textCampName = (HtmlOutputText) findComponentInRoot("textCampName");
		}
		return textCampName;
	}

	protected HtmlOutputText getTextPub() {
		if (textPub == null) {
			textPub = (HtmlOutputText) findComponentInRoot("textPub");
		}
		return textPub;
	}

	protected HtmlOutputText getTextKeycode() {
		if (textKeycode == null) {
			textKeycode = (HtmlOutputText) findComponentInRoot("textKeycode");
		}
		return textKeycode;
	}

	protected HtmlOutputText getTextPubStatus() {
		if (textPubStatus == null) {
			textPubStatus = (HtmlOutputText) findComponentInRoot("textPubStatus");
		}
		return textPubStatus;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getTextVanLabel() {
		if (textVanLabel == null) {
			textVanLabel = (HtmlOutputText) findComponentInRoot("textVanLabel");
		}
		return textVanLabel;
	}

	protected HtmlOutputLinkEx getLinkExVanLink() {
		if (linkExVanLink == null) {
			linkExVanLink = (HtmlOutputLinkEx) findComponentInRoot("linkExVanLink");
		}
		return linkExVanLink;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlCommandExButton getButtonConfirmSelection() {
		if (buttonConfirmSelection == null) {
			buttonConfirmSelection = (HtmlCommandExButton) findComponentInRoot("buttonConfirmSelection");
		}
		return buttonConfirmSelection;
	}

	public String doButtonConfirmSelectionAction() {
		// Type Java code that runs when the component is clicked
	
		EditCampaignDetailsHandler editHandler = this.getEdittingCampaignsHandler();
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		CampaignHandlerReadOnly defaultCamp = this.getDefaultCampaignReadOnly();
		
		boolean sourceCampaignSelected = false;
		try {
		for(CampaignHandler c : editHandler.getCampaigns()) {
			if (c.isCampaignSelected()) {
				sourceCampaignSelected = true;
				CampaignHandler defaultC = new CampaignHandler();
				if (c.getCampaign().getProduct().getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
					defaultC.setCampaign(this.getUtDefaultCampaign().getCampaign());
				}
				else {
					defaultC.setCampaign(this.getSwDefaultCampaign().getCampaign());
				}
				editor.setDefaultCampaign(defaultC);
				defaultCamp.setSourceCampaign(defaultC);
				editor.setSourceCampaign(c);
			}
		}
		}
		catch (Exception e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to process selection: " + e.getMessage(), null);
			getFacesContext().addMessage(null, message);
			return "failure";
		}
		
		if (!sourceCampaignSelected) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select the campaign to use as the basis for editting.", null);
			getFacesContext().addMessage(null, message);
			return "failure";
		}
		return "success";
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getUtDefaultCampaign() {
		if (utDefaultCampaign == null) {
			utDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{utDefaultCampaign}").getValue(
							getFacesContext());
		}
		return utDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUtDefaultCampaign(
			CampaignHandlerReadOnly utDefaultCampaign) {
		this.utDefaultCampaign = utDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getSwDefaultCampaign() {
		if (swDefaultCampaign == null) {
			swDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{swDefaultCampaign}").getValue(
							getFacesContext());
		}
		return swDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSwDefaultCampaign(
			CampaignHandlerReadOnly swDefaultCampaign) {
		this.swDefaultCampaign = swDefaultCampaign;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	protected HtmlOutputText getTextGroupName() {
		if (textGroupName == null) {
			textGroupName = (HtmlOutputText) findComponentInRoot("textGroupName");
		}
		return textGroupName;
	}

}