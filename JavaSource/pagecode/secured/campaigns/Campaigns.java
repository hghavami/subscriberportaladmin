/*
 * Created on Aug 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode.secured.campaigns;

import javax.faces.application.FacesMessage;
import javax.faces.component.UINamingContainer;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGroup;

import pagecode.PageCodeBase;

import com.gannett.usat.dataHandlers.campaigns.BasicSearchHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.CampaignStatisticsHandler;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.campaigns.SearchResultsHandler;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlBehaviorKeyPress;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlDataTableEx;
/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Campaigns extends PageCodeBase {

	protected HtmlOutputLinkEx linkEx1;
	protected HtmlInputText textBasicSearchCriteria;
	protected HtmlOutputText textAdvSearchLabel;
	protected HtmlMessage message1;
	protected HtmlOutputText textBasicSearchLabel;
	protected HtmlRequestLink linkChangeSWDefKeycode;
	protected HtmlOutputText text4;
	protected HtmlForm formBasicSearchForm;
	protected HtmlCommandExButton button1;
	protected HtmlCommandExButton button2;
	protected HtmlOutputText textNumExpiredCampaignsLabel;
	protected HtmlInputText textNumExpiredCampaigns;
	protected HtmlPanelDialog dialog1;
	protected HtmlPanelGroup group1;
	protected HtmlBehavior behavior2;
	protected HtmlBehavior behavior1;
	protected HtmlPanelSection section1;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageEx2;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlOutputText text2;
	protected HtmlPanelBox box1;
	protected HtmlScriptCollector scriptCollectorCampaignInfo;
	protected HtmlGraphicImageEx imageExCampaignsSectionExpanded;
	protected HtmlGraphicImageEx imageExCampaignsSectionCol;
	protected HtmlPanelSection sectionCampaignCountsSection;
	protected HtmlJspPanel jspPanelCampaignCountsJSPPanel;
	protected HtmlPanelSection sectionDefaultKeycodesSection;
	protected HtmlJspPanel jspPanel4;
	protected HtmlJspPanel jspPanel3;
	protected HtmlOutputText text5;
	protected HtmlGraphicImageEx imageExDefKeycodeExp;
	protected HtmlGraphicImageEx imageExDefKeycodesCol;
	protected HtmlJspPanel jspPanelDefaultKeycodesPanel;
	protected HtmlJspPanel jspPanel6;
	protected HtmlJspPanel jspPanel5;
	protected HtmlOutputText text7;
	protected HtmlOutputText text6;
	protected HtmlPanelSection sectionExpiredCampaignsSection;
	protected HtmlGraphicImageEx imageExExpCampsExp;
	protected HtmlGraphicImageEx imageExExpCampsCol;
	protected HtmlJspPanel jspPanelExpiredCampsPanel;
	protected HtmlOutputText textNumExpCampaignsLabel;
	protected HtmlInputText textNumExpCampaigns;
	protected HtmlMessages messagesCampDetailsErrors;
	protected HtmlForm formCampaignInfo;
	protected UINamingContainer subviewBasicSearch;
	protected HtmlScriptCollector scriptCollectorBasicSearchForm;
	protected HtmlCommandExButton buttonBasicSearch;
	protected HtmlOutputLinkEx linkExAdvSearchLink;
	protected BasicSearchHandler basicSearchCriteria;
	protected SearchResultsHandler currentSearchResults;
	protected CampaignStatisticsHandler campaignStatisticsHandler;
	protected HtmlOutputText textNumCampaignsLabel;
	protected HtmlInputText textNumCampaigns;
	protected HtmlOutputText textNewCampaignLinkLabel;
	protected HtmlOutputLinkEx linkExNewCampaignWizLink;
	protected CampaignHandlerReadOnly utDefaultCampaign;
	protected CampaignHandlerReadOnly swDefaultCampaign;
	protected CampaignHandlerReadOnly defaultCampaignReadOnly;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlBehaviorKeyPress behaviorKeyPress1;
	protected HtmlInputHelperSetFocus setFocus1;
	protected UIColumnEx columnExCampCountDGridCol1;
	protected HtmlOutputText columnExCampCountDGridCol1Header;
	protected HtmlDataTableEx tableExCampaignCountsDGrid;
	protected HtmlOutputText text8;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx1;
	protected HtmlCommandExButton buttonRefreshCampaignStats;
	protected HtmlDataTableEx productData;
	protected HtmlOutputText text3;
	protected HtmlScriptCollector scriptCollectorJSPC02blue;
	protected HtmlOutputText textSysDescriptionHeader;
	protected HtmlJspPanel jspPanelLeftNavCheckProdPanel;
	protected HtmlOutputLinkEx linkExLeftNavToClearProdCache;
	protected HtmlOutputText textLeftNavClearCacheLabel;
	protected HtmlOutputLinkEx linkExLeftNav1;
	protected HtmlOutputText textLeftNav1;
	protected HtmlOutputLinkEx linkExLeftNav2;
	protected HtmlOutputText textLeftNav2;
	protected HtmlOutputLinkEx linkExLeftNavToOmniture;
	protected HtmlOutputText textLeftNav4;
	protected HtmlJspPanel jspPanelLeftNav5;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdCol;
	protected HtmlJspPanel jspPanelLeftNav4;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdExp;
	protected UIColumnEx columnExProductName;
	protected HtmlPanelSection sectionLeftNavCheckProdSection;
	protected HtmlOutputText textLeftNav11;
	protected HtmlOutputText textLeftNav10;
	protected HtmlOutputText textProdNameDT;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text10;
	protected HtmlOutputText text444;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text12;
	protected HtmlOutputText text1;
	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}
	protected HtmlInputText getTextBasicSearchCriteria() {
		if (textBasicSearchCriteria == null) {
			textBasicSearchCriteria = (HtmlInputText) findComponentInRoot("textBasicSearchCriteria");
		}
		return textBasicSearchCriteria;
	}
	protected HtmlOutputText getTextAdvSearchLabel() {
		if (textAdvSearchLabel == null) {
			textAdvSearchLabel = (HtmlOutputText) findComponentInRoot("textAdvSearchLabel");
		}
		return textAdvSearchLabel;
	}
	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}
	protected HtmlOutputText getTextBasicSearchLabel() {
		if (textBasicSearchLabel == null) {
			textBasicSearchLabel = (HtmlOutputText) findComponentInRoot("textBasicSearchLabel");
		}
		return textBasicSearchLabel;
	}
	protected HtmlRequestLink getLinkChangeSWDefKeycode() {
		if (linkChangeSWDefKeycode == null) {
			linkChangeSWDefKeycode = (HtmlRequestLink) findComponentInRoot("linkChangeSWDefKeycode");
		}
		return linkChangeSWDefKeycode;
	}
	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}
	protected HtmlForm getFormBasicSearchForm() {
		if (formBasicSearchForm == null) {
			formBasicSearchForm = (HtmlForm) findComponentInRoot("formBasicSearchForm");
		}
		return formBasicSearchForm;
	}
	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}
	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}
	public String doLinkChangeUTDefKeycodeAction() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		 return "success";

	}
	protected HtmlOutputText getTextNumExpiredCampaignsLabel() {
		if (textNumExpiredCampaignsLabel == null) {
			textNumExpiredCampaignsLabel = (HtmlOutputText) findComponentInRoot("textNumExpiredCampaignsLabel");
		}
		return textNumExpiredCampaignsLabel;
	}
	protected HtmlInputText getTextNumExpiredCampaigns() {
		if (textNumExpiredCampaigns == null) {
			textNumExpiredCampaigns = (HtmlInputText) findComponentInRoot("textNumExpiredCampaigns");
		}
		return textNumExpiredCampaigns;
	}
	protected HtmlPanelDialog getDialog1() {
		if (dialog1 == null) {
			dialog1 = (HtmlPanelDialog) findComponentInRoot("dialog1");
		}
		return dialog1;
	}
	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}
	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}
	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}
	protected HtmlPanelSection getSection1() {
		if (section1 == null) {
			section1 = (HtmlPanelSection) findComponentInRoot("section1");
		}
		return section1;
	}
	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}
	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}
	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}
	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}
	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}
	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}
	protected HtmlScriptCollector getScriptCollectorCampaignInfo() {
		if (scriptCollectorCampaignInfo == null) {
			scriptCollectorCampaignInfo = (HtmlScriptCollector) findComponentInRoot("scriptCollectorCampaignInfo");
		}
		return scriptCollectorCampaignInfo;
	}
	protected HtmlGraphicImageEx getImageExCampaignsSectionExpanded() {
		if (imageExCampaignsSectionExpanded == null) {
			imageExCampaignsSectionExpanded = (HtmlGraphicImageEx) findComponentInRoot("imageExCampaignsSectionExpanded");
		}
		return imageExCampaignsSectionExpanded;
	}
	protected HtmlGraphicImageEx getImageExCampaignsSectionCol() {
		if (imageExCampaignsSectionCol == null) {
			imageExCampaignsSectionCol = (HtmlGraphicImageEx) findComponentInRoot("imageExCampaignsSectionCol");
		}
		return imageExCampaignsSectionCol;
	}
	protected HtmlPanelSection getSectionCampaignCountsSection() {
		if (sectionCampaignCountsSection == null) {
			sectionCampaignCountsSection = (HtmlPanelSection) findComponentInRoot("sectionCampaignCountsSection");
		}
		return sectionCampaignCountsSection;
	}
	protected HtmlJspPanel getJspPanelCampaignCountsJSPPanel() {
		if (jspPanelCampaignCountsJSPPanel == null) {
			jspPanelCampaignCountsJSPPanel = (HtmlJspPanel) findComponentInRoot("jspPanelCampaignCountsJSPPanel");
		}
		return jspPanelCampaignCountsJSPPanel;
	}
	protected HtmlPanelSection getSectionDefaultKeycodesSection() {
		if (sectionDefaultKeycodesSection == null) {
			sectionDefaultKeycodesSection = (HtmlPanelSection) findComponentInRoot("sectionDefaultKeycodesSection");
		}
		return sectionDefaultKeycodesSection;
	}
	protected HtmlJspPanel getJspPanel4() {
		if (jspPanel4 == null) {
			jspPanel4 = (HtmlJspPanel) findComponentInRoot("jspPanel4");
		}
		return jspPanel4;
	}
	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}
	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}
	protected HtmlGraphicImageEx getImageExDefKeycodeExp() {
		if (imageExDefKeycodeExp == null) {
			imageExDefKeycodeExp = (HtmlGraphicImageEx) findComponentInRoot("imageExDefKeycodeExp");
		}
		return imageExDefKeycodeExp;
	}
	protected HtmlGraphicImageEx getImageExDefKeycodesCol() {
		if (imageExDefKeycodesCol == null) {
			imageExDefKeycodesCol = (HtmlGraphicImageEx) findComponentInRoot("imageExDefKeycodesCol");
		}
		return imageExDefKeycodesCol;
	}
	protected HtmlJspPanel getJspPanelDefaultKeycodesPanel() {
		if (jspPanelDefaultKeycodesPanel == null) {
			jspPanelDefaultKeycodesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelDefaultKeycodesPanel");
		}
		return jspPanelDefaultKeycodesPanel;
	}
	protected HtmlJspPanel getJspPanel6() {
		if (jspPanel6 == null) {
			jspPanel6 = (HtmlJspPanel) findComponentInRoot("jspPanel6");
		}
		return jspPanel6;
	}
	protected HtmlJspPanel getJspPanel5() {
		if (jspPanel5 == null) {
			jspPanel5 = (HtmlJspPanel) findComponentInRoot("jspPanel5");
		}
		return jspPanel5;
	}
	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}
	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}
	protected HtmlPanelSection getSectionExpiredCampaignsSection() {
		if (sectionExpiredCampaignsSection == null) {
			sectionExpiredCampaignsSection = (HtmlPanelSection) findComponentInRoot("sectionExpiredCampaignsSection");
		}
		return sectionExpiredCampaignsSection;
	}
	protected HtmlGraphicImageEx getImageExExpCampsExp() {
		if (imageExExpCampsExp == null) {
			imageExExpCampsExp = (HtmlGraphicImageEx) findComponentInRoot("imageExExpCampsExp");
		}
		return imageExExpCampsExp;
	}
	protected HtmlGraphicImageEx getImageExExpCampsCol() {
		if (imageExExpCampsCol == null) {
			imageExExpCampsCol = (HtmlGraphicImageEx) findComponentInRoot("imageExExpCampsCol");
		}
		return imageExExpCampsCol;
	}
	protected HtmlJspPanel getJspPanelExpiredCampsPanel() {
		if (jspPanelExpiredCampsPanel == null) {
			jspPanelExpiredCampsPanel = (HtmlJspPanel) findComponentInRoot("jspPanelExpiredCampsPanel");
		}
		return jspPanelExpiredCampsPanel;
	}
	protected HtmlOutputText getTextNumExpCampaignsLabel() {
		if (textNumExpCampaignsLabel == null) {
			textNumExpCampaignsLabel = (HtmlOutputText) findComponentInRoot("textNumExpCampaignsLabel");
		}
		return textNumExpCampaignsLabel;
	}
	protected HtmlInputText getTextNumExpCampaigns() {
		if (textNumExpCampaigns == null) {
			textNumExpCampaigns = (HtmlInputText) findComponentInRoot("textNumExpCampaigns");
		}
		return textNumExpCampaigns;
	}
	protected HtmlMessages getMessagesCampDetailsErrors() {
		if (messagesCampDetailsErrors == null) {
			messagesCampDetailsErrors = (HtmlMessages) findComponentInRoot("messagesCampDetailsErrors");
		}
		return messagesCampDetailsErrors;
	}
	protected HtmlForm getFormCampaignInfo() {
		if (formCampaignInfo == null) {
			formCampaignInfo = (HtmlForm) findComponentInRoot("formCampaignInfo");
		}
		return formCampaignInfo;
	}
	protected UINamingContainer getSubviewBasicSearch() {
		if (subviewBasicSearch == null) {
			subviewBasicSearch = (UINamingContainer) findComponentInRoot("subviewBasicSearch");
		}
		return subviewBasicSearch;
	}
	protected HtmlScriptCollector getScriptCollectorBasicSearchForm() {
		if (scriptCollectorBasicSearchForm == null) {
			scriptCollectorBasicSearchForm = (HtmlScriptCollector) findComponentInRoot("scriptCollectorBasicSearchForm");
		}
		return scriptCollectorBasicSearchForm;
	}
	protected HtmlCommandExButton getButtonBasicSearch() {
		if (buttonBasicSearch == null) {
			buttonBasicSearch = (HtmlCommandExButton) findComponentInRoot("buttonBasicSearch");
		}
		return buttonBasicSearch;
	}
	protected HtmlOutputLinkEx getLinkExAdvSearchLink() {
		if (linkExAdvSearchLink == null) {
			linkExAdvSearchLink = (HtmlOutputLinkEx) findComponentInRoot("linkExAdvSearchLink");
		}
		return linkExAdvSearchLink;
	}
	public String doButtonBasicSearchAction() {
		// Type Java code that runs when the component is clicked
	
		String searchCriteria = this.getBasicSearchCriteria().getQueryString();

		String responsePath = "success";
		
		if (searchCriteria == null || searchCriteria.trim().length() == 0) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please specify search criteria", null);
			getFacesContext().addMessage(null, message);
			responsePath = "failure";
		}
		else {

			try {
				this.getCurrentSearchResults().setSearchResults(UsatCampaignBO.fetchCampaignsMeetingGeneralCriteria(searchCriteria));
			}
			catch (Exception e) {
				e.printStackTrace();
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Exception Performing Basic Search: " + e.getMessage(), "Please try again.");
				getFacesContext().addMessage(null, message);
				responsePath = "failure";
			}
		}
		return responsePath;
	}
	/** 
	 * @managed-bean true
	 */
	protected BasicSearchHandler getBasicSearchCriteria() {
		if (basicSearchCriteria == null) {
			basicSearchCriteria = (BasicSearchHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{basicSearchCriteria}").getValue(
							getFacesContext());
		}
		return basicSearchCriteria;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setBasicSearchCriteria(BasicSearchHandler basicSearchCriteria) {
		this.basicSearchCriteria = basicSearchCriteria;
	}
	/** 
	 * @managed-bean true
	 */
	protected SearchResultsHandler getCurrentSearchResults() {
		if (currentSearchResults == null) {
			currentSearchResults = (SearchResultsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{currentSearchResults}").getValue(
							getFacesContext());
		}
		return currentSearchResults;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCurrentSearchResults(
			SearchResultsHandler currentSearchResults) {
		this.currentSearchResults = currentSearchResults;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignStatisticsHandler getCampaignStatisticsHandler() {
		if (campaignStatisticsHandler == null) {
			campaignStatisticsHandler = (CampaignStatisticsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignStatisticsHandler}").getValue(
							getFacesContext());
		}
		return campaignStatisticsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCampaignStatisticsHandler(
			CampaignStatisticsHandler campaignStatisticsHandler) {
		this.campaignStatisticsHandler = campaignStatisticsHandler;
	}
	protected HtmlOutputText getTextNumCampaignsLabel() {
		if (textNumCampaignsLabel == null) {
			textNumCampaignsLabel = (HtmlOutputText) findComponentInRoot("textNumCampaignsLabel");
		}
		return textNumCampaignsLabel;
	}
	protected HtmlInputText getTextNumCampaigns() {
		if (textNumCampaigns == null) {
			textNumCampaigns = (HtmlInputText) findComponentInRoot("textNumCampaigns");
		}
		return textNumCampaigns;
	}
	protected HtmlOutputText getTextNewCampaignLinkLabel() {
		if (textNewCampaignLinkLabel == null) {
			textNewCampaignLinkLabel = (HtmlOutputText) findComponentInRoot("textNewCampaignLinkLabel");
		}
		return textNewCampaignLinkLabel;
	}
	protected HtmlOutputLinkEx getLinkExNewCampaignWizLink() {
		if (linkExNewCampaignWizLink == null) {
			linkExNewCampaignWizLink = (HtmlOutputLinkEx) findComponentInRoot("linkExNewCampaignWizLink");
		}
		return linkExNewCampaignWizLink;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getUtDefaultCampaign() {
		if (utDefaultCampaign == null) {
			utDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{utDefaultCampaign}").getValue(
							getFacesContext());
		}
		return utDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUtDefaultCampaign(
			CampaignHandlerReadOnly utDefaultCampaign) {
		this.utDefaultCampaign = utDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getSwDefaultCampaign() {
		if (swDefaultCampaign == null) {
			swDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{swDefaultCampaign}").getValue(
							getFacesContext());
		}
		return swDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setSwDefaultCampaign(
			CampaignHandlerReadOnly swDefaultCampaign) {
		this.swDefaultCampaign = swDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getDefaultCampaignReadOnly() {
		if (defaultCampaignReadOnly == null) {
			defaultCampaignReadOnly = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication().createValueBinding(
							"#{defaultCampaignReadOnly}").getValue(
							getFacesContext());
		}
		return defaultCampaignReadOnly;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setDefaultCampaignReadOnly(
			CampaignHandlerReadOnly defaultCampaignReadOnly) {
		this.defaultCampaignReadOnly = defaultCampaignReadOnly;
	}
	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}
	protected HtmlBehaviorKeyPress getBehaviorKeyPress1() {
		if (behaviorKeyPress1 == null) {
			behaviorKeyPress1 = (HtmlBehaviorKeyPress) findComponentInRoot("behaviorKeyPress1");
		}
		return behaviorKeyPress1;
	}
	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}
	protected UIColumnEx getColumnExCampCountDGridCol1() {
		if (columnExCampCountDGridCol1 == null) {
			columnExCampCountDGridCol1 = (UIColumnEx) findComponentInRoot("columnExCampCountDGridCol1");
		}
		return columnExCampCountDGridCol1;
	}
	protected HtmlOutputText getColumnExCampCountDGridCol1Header() {
		if (columnExCampCountDGridCol1Header == null) {
			columnExCampCountDGridCol1Header = (HtmlOutputText) findComponentInRoot("columnExCampCountDGridCol1Header");
		}
		return columnExCampCountDGridCol1Header;
	}
	protected HtmlDataTableEx getTableExCampaignCountsDGrid() {
		if (tableExCampaignCountsDGrid == null) {
			tableExCampaignCountsDGrid = (HtmlDataTableEx) findComponentInRoot("tableExCampaignCountsDGrid");
		}
		return tableExCampaignCountsDGrid;
	}
	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}
	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}
	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}
	protected HtmlCommandExButton getButtonRefreshCampaignStats() {
		if (buttonRefreshCampaignStats == null) {
			buttonRefreshCampaignStats = (HtmlCommandExButton) findComponentInRoot("buttonRefreshCampaignStats");
		}
		return buttonRefreshCampaignStats;
	}
	public String doButtonRefreshCampaignStatsAction() {
		// Type Java code that runs when the component is clicked

		this.getCampaignStatisticsHandler().refreshStatistics();
		
		return "";
	}
	protected HtmlDataTableEx getProductData() {
		if (productData == null) {
			productData = (HtmlDataTableEx) findComponentInRoot("productData");
		}
		return productData;
	}
	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}
	protected HtmlScriptCollector getScriptCollectorJSPC02blue() {
		if (scriptCollectorJSPC02blue == null) {
			scriptCollectorJSPC02blue = (HtmlScriptCollector) findComponentInRoot("scriptCollectorJSPC02blue");
		}
		return scriptCollectorJSPC02blue;
	}
	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}
	protected HtmlJspPanel getJspPanelLeftNavCheckProdPanel() {
		if (jspPanelLeftNavCheckProdPanel == null) {
			jspPanelLeftNavCheckProdPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNavCheckProdPanel");
		}
		return jspPanelLeftNavCheckProdPanel;
	}
	protected HtmlOutputLinkEx getLinkExLeftNavToClearProdCache() {
		if (linkExLeftNavToClearProdCache == null) {
			linkExLeftNavToClearProdCache = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToClearProdCache");
		}
		return linkExLeftNavToClearProdCache;
	}
	protected HtmlOutputText getTextLeftNavClearCacheLabel() {
		if (textLeftNavClearCacheLabel == null) {
			textLeftNavClearCacheLabel = (HtmlOutputText) findComponentInRoot("textLeftNavClearCacheLabel");
		}
		return textLeftNavClearCacheLabel;
	}
	protected HtmlOutputLinkEx getLinkExLeftNav1() {
		if (linkExLeftNav1 == null) {
			linkExLeftNav1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav1");
		}
		return linkExLeftNav1;
	}
	protected HtmlOutputText getTextLeftNav1() {
		if (textLeftNav1 == null) {
			textLeftNav1 = (HtmlOutputText) findComponentInRoot("textLeftNav1");
		}
		return textLeftNav1;
	}
	protected HtmlOutputLinkEx getLinkExLeftNav2() {
		if (linkExLeftNav2 == null) {
			linkExLeftNav2 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav2");
		}
		return linkExLeftNav2;
	}
	protected HtmlOutputText getTextLeftNav2() {
		if (textLeftNav2 == null) {
			textLeftNav2 = (HtmlOutputText) findComponentInRoot("textLeftNav2");
		}
		return textLeftNav2;
	}
	protected HtmlOutputLinkEx getLinkExLeftNavToOmniture() {
		if (linkExLeftNavToOmniture == null) {
			linkExLeftNavToOmniture = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToOmniture");
		}
		return linkExLeftNavToOmniture;
	}
	protected HtmlOutputText getTextLeftNav4() {
		if (textLeftNav4 == null) {
			textLeftNav4 = (HtmlOutputText) findComponentInRoot("textLeftNav4");
		}
		return textLeftNav4;
	}
	protected HtmlJspPanel getJspPanelLeftNav5() {
		if (jspPanelLeftNav5 == null) {
			jspPanelLeftNav5 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav5");
		}
		return jspPanelLeftNav5;
	}
	protected HtmlGraphicImageEx getImageExLeftNavCheckProdCol() {
		if (imageExLeftNavCheckProdCol == null) {
			imageExLeftNavCheckProdCol = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdCol");
		}
		return imageExLeftNavCheckProdCol;
	}
	protected HtmlJspPanel getJspPanelLeftNav4() {
		if (jspPanelLeftNav4 == null) {
			jspPanelLeftNav4 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav4");
		}
		return jspPanelLeftNav4;
	}
	protected HtmlGraphicImageEx getImageExLeftNavCheckProdExp() {
		if (imageExLeftNavCheckProdExp == null) {
			imageExLeftNavCheckProdExp = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdExp");
		}
		return imageExLeftNavCheckProdExp;
	}
	protected UIColumnEx getColumnExProductName() {
		if (columnExProductName == null) {
			columnExProductName = (UIColumnEx) findComponentInRoot("columnExProductName");
		}
		return columnExProductName;
	}
	protected HtmlPanelSection getSectionLeftNavCheckProdSection() {
		if (sectionLeftNavCheckProdSection == null) {
			sectionLeftNavCheckProdSection = (HtmlPanelSection) findComponentInRoot("sectionLeftNavCheckProdSection");
		}
		return sectionLeftNavCheckProdSection;
	}
	protected HtmlOutputText getTextLeftNav11() {
		if (textLeftNav11 == null) {
			textLeftNav11 = (HtmlOutputText) findComponentInRoot("textLeftNav11");
		}
		return textLeftNav11;
	}
	protected HtmlOutputText getTextLeftNav10() {
		if (textLeftNav10 == null) {
			textLeftNav10 = (HtmlOutputText) findComponentInRoot("textLeftNav10");
		}
		return textLeftNav10;
	}
	protected HtmlOutputText getTextProdNameDT() {
		if (textProdNameDT == null) {
			textProdNameDT = (HtmlOutputText) findComponentInRoot("textProdNameDT");
		}
		return textProdNameDT;
	}
	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}
	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}
	protected HtmlOutputText getText444() {
		if (text444 == null) {
			text444 = (HtmlOutputText) findComponentInRoot("text444");
		}
		return text444;
	}
	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}
	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}
	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}
	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}
}