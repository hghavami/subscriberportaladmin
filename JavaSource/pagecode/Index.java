/*
 * Created on Aug 8, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pagecode;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import com.gannett.usat.dataHandlers.UserHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.editPages.CoverGraphicsHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.users.PortalUserBO;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.util.constants.UsaTodayConstants;
/**
 * @author aeast
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Index extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText textuserNameLabel;
	protected HtmlOutputText textPasswordLabel;
	protected HtmlForm form1;
	protected HtmlInputSecret secretPassword;
	protected HtmlInputText textUserID;
	protected HtmlMessages messages1;
	protected UserHandler user;
	protected HtmlCommandExButton buttonLogin;
	protected HtmlOutputText textForgotPwdLabel;
	protected HtmlOutputLinkEx linkExForgotPwdLink;
	protected HtmlInputHelperSetFocus setFocus1;
	protected CampaignHandlerReadOnly utDefaultCampaign;
	protected CampaignHandlerReadOnly swDefaultCampaign;
	protected CampaignHandler utLandingPages;
	protected CampaignHandler swLandingPages;
	protected CampaignHandler utComingAttractions;
	protected CampaignHandler swComingAttractions;
	protected CoverGraphicsHandler coverGraphicsHandler;
	protected HtmlScriptCollector scriptCollectorJSPC02blue;
	protected HtmlJspPanel jspPanelLeftNavCheckProdPanel;
	protected HtmlOutputLinkEx linkExLeftNavToClearProdCache;
	protected HtmlOutputText textLeftNavClearCacheLabel;
	protected HtmlOutputLinkEx linkExLeftNav1;
	protected HtmlOutputText textLeftNav1;
	protected HtmlOutputLinkEx linkExLeftNav2;
	protected HtmlOutputText textLeftNav2;
	protected HtmlOutputLinkEx linkExLeftNavToOmniture;
	protected HtmlOutputText textLeftNav4;
	protected HtmlJspPanel jspPanelLeftNav5;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdCol;
	protected HtmlJspPanel jspPanelLeftNav4;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdExp;
	protected HtmlPanelSection sectionLeftNavCheckProdSection;
	protected HtmlOutputText textLeftNav11;
	protected HtmlOutputText textLeftNav10;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}
	protected HtmlOutputText getTextuserNameLabel() {
		if (textuserNameLabel == null) {
			textuserNameLabel = (HtmlOutputText) findComponentInRoot("textuserNameLabel");
		}
		return textuserNameLabel;
	}
	protected HtmlOutputText getTextPasswordLabel() {
		if (textPasswordLabel == null) {
			textPasswordLabel = (HtmlOutputText) findComponentInRoot("textPasswordLabel");
		}
		return textPasswordLabel;
	}
	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}
	protected HtmlInputSecret getSecretPassword() {
		if (secretPassword == null) {
			secretPassword = (HtmlInputSecret) findComponentInRoot("secretPassword");
		}
		return secretPassword;
	}
	protected HtmlInputText getTextUserID() {
		if (textUserID == null) {
			textUserID = (HtmlInputText) findComponentInRoot("textUserID");
		}
		return textUserID;
	}
	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}
	/** 
	 * @managed-bean true
	 */
	@SuppressWarnings("deprecation")
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getFacesContext().getApplication()
					.createValueBinding("#{user}").getValue(getFacesContext());
		}
		return user;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}
	protected HtmlCommandExButton getButtonLogin() {
		if (buttonLogin == null) {
			buttonLogin = (HtmlCommandExButton) findComponentInRoot("buttonLogin");
		}
		return buttonLogin;
	}
	public String doButtonLoginAction() {
		// Type Java code that runs when the component is clicked

		String responseString = "failure";
		PortalUserIntf authenticatedUser = null;
		try {
			if (UsaTodayConstants.debug) {
				System.out.println("Authenticating User: " + this.getUser().getUserEnteredUserID());
			}
			
			authenticatedUser = PortalUserBO.authenticateUser(this.getUser().getUserEnteredUserID(), this.getUser().getUserEnteredpassword());
			
			if (authenticatedUser == null) {
				// authentication failed
				
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The User ID/Password combination is invalid.", "Please try again.");
				getMessages1().setErrorStyle("color:Red");
				getFacesContext().addMessage(null, message);
				
			}
			else {
			
				this.getUser().setUser(authenticatedUser);
				this.getUser().setAuthenticated(true);
				// set up default campaigns "00000" keycode campaigns
				try {					
					UsatCampaignIntf c = UsatCampaignBO.fetchUTDefaultCampaign();				
				
					this.getUtLandingPages().setCampaign(c);
					this.getUtDefaultCampaign().setSourceCampaign(this.getUtLandingPages());
					this.getCoverGraphicsHandler().setDefaultUtCampaign(this.getUtLandingPages());
					
					UsatCampaignIntf swC = UsatCampaignBO.fetchSportsWeeklyDefaultCampaign();
					this.getSwLandingPages().setCampaign(swC);
					this.getSwDefaultCampaign().setSourceCampaign(this.getSwLandingPages());
					this.getCoverGraphicsHandler().setDefaultSwCampaign(this.getSwLandingPages());							
					
				}
				catch (Exception e) {
					System.out.println(e.getMessage());
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "FAILED TO LOAD Default Keycode Campaigns. Contact IT for support.", e.getMessage());
					getMessages1().setErrorStyle("color:Red");
					getMessages1().setShowDetail(true);
					getFacesContext().addMessage(null, message);
					
				}
				// successfule login
				responseString = "success";
			}
			
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "An unexpected error has occurred.", e.getMessage());
			getMessages1().setErrorStyle("color:Red");
			getMessages1().setShowDetail(true);
			getFacesContext().addMessage(null, message);
			
		}
		return responseString;
	}
	protected HtmlOutputText getTextForgotPwdLabel() {
		if (textForgotPwdLabel == null) {
			textForgotPwdLabel = (HtmlOutputText) findComponentInRoot("textForgotPwdLabel");
		}
		return textForgotPwdLabel;
	}
	protected HtmlOutputLinkEx getLinkExForgotPwdLink() {
		if (linkExForgotPwdLink == null) {
			linkExForgotPwdLink = (HtmlOutputLinkEx) findComponentInRoot("linkExForgotPwdLink");
		}
		return linkExForgotPwdLink;
	}
	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}
	
	/** 
	 * @managed-bean true
	 */
	@SuppressWarnings("deprecation")
	protected CampaignHandlerReadOnly getUtDefaultCampaign() {
		if (utDefaultCampaign == null) {
			utDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{utDefaultCampaign}").getValue(
							getFacesContext());
		}
		return utDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUtDefaultCampaign(
			CampaignHandlerReadOnly utDefaultCampaign) {
		this.utDefaultCampaign = utDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	@SuppressWarnings("deprecation")
	protected CampaignHandlerReadOnly getSwDefaultCampaign() {
		if (swDefaultCampaign == null) {
			swDefaultCampaign = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication()
					.createValueBinding("#{swDefaultCampaign}").getValue(
							getFacesContext());
		}
		return swDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setSwDefaultCampaign(
			CampaignHandlerReadOnly swDefaultCampaign) {
		this.swDefaultCampaign = swDefaultCampaign;
	}
	/** 
	 * @managed-bean true
	 */
	@SuppressWarnings("deprecation")
	protected CampaignHandler getUtComingAttractions() {
		if (utComingAttractions == null) {
			utComingAttractions = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{utComingAttractions}").getValue(
							getFacesContext());
		}
		return utComingAttractions;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUtComingAttractions(CampaignHandler utComingAttractions) {
		this.utComingAttractions = utComingAttractions;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setSwLandingPages(
			CampaignHandler swLandingPages) {
		this.swLandingPages = swLandingPages;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUtLandingPages(
			CampaignHandler utLandingPages) {
		this.utLandingPages = utLandingPages;
	}
	/** 
	 * @managed-bean true
	 */
	@SuppressWarnings("deprecation")
	protected CampaignHandler getUtLandingPages() {
		if (utLandingPages == null) {
			utLandingPages = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{utLandingPages}").getValue(
							getFacesContext());
		}
		return utLandingPages;
	}
	/** 
	 * @managed-bean true
	 */
	@SuppressWarnings("deprecation")
	protected CampaignHandler getSwLandingPages() {
		if (swLandingPages == null) {
			swLandingPages = (CampaignHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{swLandingPages}").getValue(
							getFacesContext());
		}
		return swLandingPages;
	}
		
	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	}	
	/** 
	 * @managed-bean true
	 */
	@SuppressWarnings("deprecation")
	protected CoverGraphicsHandler getCoverGraphicsHandler() {
		if (coverGraphicsHandler == null) {
			coverGraphicsHandler = (CoverGraphicsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{coverGraphicsHandler}").getValue(
							getFacesContext());
		}
		return coverGraphicsHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setCoverGraphicsHandler(
			CoverGraphicsHandler coverGraphicsHandler) {
		this.coverGraphicsHandler = coverGraphicsHandler;
	}
	protected HtmlScriptCollector getScriptCollectorJSPC02blue() {
		if (scriptCollectorJSPC02blue == null) {
			scriptCollectorJSPC02blue = (HtmlScriptCollector) findComponentInRoot("scriptCollectorJSPC02blue");
		}
		return scriptCollectorJSPC02blue;
	}
	protected HtmlJspPanel getJspPanelLeftNavCheckProdPanel() {
		if (jspPanelLeftNavCheckProdPanel == null) {
			jspPanelLeftNavCheckProdPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNavCheckProdPanel");
		}
		return jspPanelLeftNavCheckProdPanel;
	}
	protected HtmlOutputLinkEx getLinkExLeftNavToClearProdCache() {
		if (linkExLeftNavToClearProdCache == null) {
			linkExLeftNavToClearProdCache = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToClearProdCache");
		}
		return linkExLeftNavToClearProdCache;
	}
	protected HtmlOutputText getTextLeftNavClearCacheLabel() {
		if (textLeftNavClearCacheLabel == null) {
			textLeftNavClearCacheLabel = (HtmlOutputText) findComponentInRoot("textLeftNavClearCacheLabel");
		}
		return textLeftNavClearCacheLabel;
	}
	protected HtmlOutputLinkEx getLinkExLeftNav1() {
		if (linkExLeftNav1 == null) {
			linkExLeftNav1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav1");
		}
		return linkExLeftNav1;
	}
	protected HtmlOutputText getTextLeftNav1() {
		if (textLeftNav1 == null) {
			textLeftNav1 = (HtmlOutputText) findComponentInRoot("textLeftNav1");
		}
		return textLeftNav1;
	}
	protected HtmlOutputLinkEx getLinkExLeftNav2() {
		if (linkExLeftNav2 == null) {
			linkExLeftNav2 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav2");
		}
		return linkExLeftNav2;
	}
	protected HtmlOutputText getTextLeftNav2() {
		if (textLeftNav2 == null) {
			textLeftNav2 = (HtmlOutputText) findComponentInRoot("textLeftNav2");
		}
		return textLeftNav2;
	}
	protected HtmlOutputLinkEx getLinkExLeftNavToOmniture() {
		if (linkExLeftNavToOmniture == null) {
			linkExLeftNavToOmniture = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToOmniture");
		}
		return linkExLeftNavToOmniture;
	}
	protected HtmlOutputText getTextLeftNav4() {
		if (textLeftNav4 == null) {
			textLeftNav4 = (HtmlOutputText) findComponentInRoot("textLeftNav4");
		}
		return textLeftNav4;
	}
	protected HtmlJspPanel getJspPanelLeftNav5() {
		if (jspPanelLeftNav5 == null) {
			jspPanelLeftNav5 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav5");
		}
		return jspPanelLeftNav5;
	}
	protected HtmlGraphicImageEx getImageExLeftNavCheckProdCol() {
		if (imageExLeftNavCheckProdCol == null) {
			imageExLeftNavCheckProdCol = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdCol");
		}
		return imageExLeftNavCheckProdCol;
	}
	protected HtmlJspPanel getJspPanelLeftNav4() {
		if (jspPanelLeftNav4 == null) {
			jspPanelLeftNav4 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav4");
		}
		return jspPanelLeftNav4;
	}
	protected HtmlGraphicImageEx getImageExLeftNavCheckProdExp() {
		if (imageExLeftNavCheckProdExp == null) {
			imageExLeftNavCheckProdExp = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdExp");
		}
		return imageExLeftNavCheckProdExp;
	}
	protected HtmlPanelSection getSectionLeftNavCheckProdSection() {
		if (sectionLeftNavCheckProdSection == null) {
			sectionLeftNavCheckProdSection = (HtmlPanelSection) findComponentInRoot("sectionLeftNavCheckProdSection");
		}
		return sectionLeftNavCheckProdSection;
	}
	protected HtmlOutputText getTextLeftNav11() {
		if (textLeftNav11 == null) {
			textLeftNav11 = (HtmlOutputText) findComponentInRoot("textLeftNav11");
		}
		return textLeftNav11;
	}
	protected HtmlOutputText getTextLeftNav10() {
		if (textLeftNav10 == null) {
			textLeftNav10 = (HtmlOutputText) findComponentInRoot("textLeftNav10");
		}
		return textLeftNav10;
	}
}