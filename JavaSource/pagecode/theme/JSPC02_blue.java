/**
 * 
 */
package pagecode.theme;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlJspPanel;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.gannett.usat.dataHandlers.portalConfig.PortalAppConfigHandler;

/**
 * @author aeast
 *
 */
public class JSPC02_blue extends PageCodeBase {

	protected HtmlPanelSection sectionLeftNavCheckProdSection;
	protected HtmlJspPanel jspPanelLeftNavCheckProdPanel;
	protected HtmlOutputLinkEx linkExLeftNav1;
	protected HtmlOutputText textLeftNav1;
	protected HtmlOutputText textLeftNav2;
	protected HtmlOutputText textLeftNavClearCacheLabel;
	protected HtmlScriptCollector scriptCollectorJSPC02blue;
	protected HtmlOutputLinkEx linkExLeftNav2;
	protected HtmlOutputLinkEx linkExLeftNavToClearProdCache;
	protected HtmlJspPanel jspPanelLeftNav5;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdCol;
	protected HtmlOutputText textLeftNav11;
	protected HtmlJspPanel jspPanelLeftNav4;
	protected HtmlGraphicImageEx imageExLeftNavCheckProdExp;
	protected HtmlOutputText textLeftNav10;
	protected HtmlOutputLinkEx linkExLeftNavToOmniture;
	protected HtmlOutputText textLeftNav4;
	protected PortalAppConfigHandler portalApplicationSettings;
	protected PortalAppConfigHandler gloabalAppSettings;
	protected HtmlOutputText textSysDescriptionHeader;
	protected HtmlPanelSection getSectionLeftNavCheckProdSection() {
		if (sectionLeftNavCheckProdSection == null) {
			sectionLeftNavCheckProdSection = (HtmlPanelSection) findComponentInRoot("sectionLeftNavCheckProdSection");
		}
		return sectionLeftNavCheckProdSection;
	}

	protected HtmlJspPanel getJspPanelLeftNavCheckProdPanel() {
		if (jspPanelLeftNavCheckProdPanel == null) {
			jspPanelLeftNavCheckProdPanel = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNavCheckProdPanel");
		}
		return jspPanelLeftNavCheckProdPanel;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav1() {
		if (linkExLeftNav1 == null) {
			linkExLeftNav1 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav1");
		}
		return linkExLeftNav1;
	}

	protected HtmlOutputText getTextLeftNav1() {
		if (textLeftNav1 == null) {
			textLeftNav1 = (HtmlOutputText) findComponentInRoot("textLeftNav1");
		}
		return textLeftNav1;
	}

	protected HtmlOutputText getTextLeftNav2() {
		if (textLeftNav2 == null) {
			textLeftNav2 = (HtmlOutputText) findComponentInRoot("textLeftNav2");
		}
		return textLeftNav2;
	}

	protected HtmlOutputText getTextLeftNavClearCacheLabel() {
		if (textLeftNavClearCacheLabel == null) {
			textLeftNavClearCacheLabel = (HtmlOutputText) findComponentInRoot("textLeftNavClearCacheLabel");
		}
		return textLeftNavClearCacheLabel;
	}

	protected HtmlScriptCollector getScriptCollectorJSPC02blue() {
		if (scriptCollectorJSPC02blue == null) {
			scriptCollectorJSPC02blue = (HtmlScriptCollector) findComponentInRoot("scriptCollectorJSPC02blue");
		}
		return scriptCollectorJSPC02blue;
	}

	protected HtmlOutputLinkEx getLinkExLeftNav2() {
		if (linkExLeftNav2 == null) {
			linkExLeftNav2 = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNav2");
		}
		return linkExLeftNav2;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToClearProdCache() {
		if (linkExLeftNavToClearProdCache == null) {
			linkExLeftNavToClearProdCache = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToClearProdCache");
		}
		return linkExLeftNavToClearProdCache;
	}

	protected HtmlJspPanel getJspPanelLeftNav5() {
		if (jspPanelLeftNav5 == null) {
			jspPanelLeftNav5 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav5");
		}
		return jspPanelLeftNav5;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdCol() {
		if (imageExLeftNavCheckProdCol == null) {
			imageExLeftNavCheckProdCol = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdCol");
		}
		return imageExLeftNavCheckProdCol;
	}

	protected HtmlOutputText getTextLeftNav11() {
		if (textLeftNav11 == null) {
			textLeftNav11 = (HtmlOutputText) findComponentInRoot("textLeftNav11");
		}
		return textLeftNav11;
	}

	protected HtmlJspPanel getJspPanelLeftNav4() {
		if (jspPanelLeftNav4 == null) {
			jspPanelLeftNav4 = (HtmlJspPanel) findComponentInRoot("jspPanelLeftNav4");
		}
		return jspPanelLeftNav4;
	}

	protected HtmlGraphicImageEx getImageExLeftNavCheckProdExp() {
		if (imageExLeftNavCheckProdExp == null) {
			imageExLeftNavCheckProdExp = (HtmlGraphicImageEx) findComponentInRoot("imageExLeftNavCheckProdExp");
		}
		return imageExLeftNavCheckProdExp;
	}

	protected HtmlOutputText getTextLeftNav10() {
		if (textLeftNav10 == null) {
			textLeftNav10 = (HtmlOutputText) findComponentInRoot("textLeftNav10");
		}
		return textLeftNav10;
	}

	protected HtmlOutputLinkEx getLinkExLeftNavToOmniture() {
		if (linkExLeftNavToOmniture == null) {
			linkExLeftNavToOmniture = (HtmlOutputLinkEx) findComponentInRoot("linkExLeftNavToOmniture");
		}
		return linkExLeftNavToOmniture;
	}

	protected HtmlOutputText getTextLeftNav4() {
		if (textLeftNav4 == null) {
			textLeftNav4 = (HtmlOutputText) findComponentInRoot("textLeftNav4");
		}
		return textLeftNav4;
	}

	/** 
	 * @managed-bean true
	 */
	protected PortalAppConfigHandler getPortalApplicationSettings() {
		if (portalApplicationSettings == null) {
			portalApplicationSettings = (PortalAppConfigHandler) getManagedBean("portalApplicationSettings");
		}
		return portalApplicationSettings;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setPortalApplicationSettings(
			PortalAppConfigHandler portalApplicationSettings) {
		this.portalApplicationSettings = portalApplicationSettings;
	}

	/** 
	 * @managed-bean true
	 */
	protected PortalAppConfigHandler getGloabalAppSettings() {
		if (gloabalAppSettings == null) {
			gloabalAppSettings = (PortalAppConfigHandler) getManagedBean("gloabalAppSettings");
		}
		return gloabalAppSettings;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setGloabalAppSettings(
			PortalAppConfigHandler gloabalAppSettings) {
		this.gloabalAppSettings = gloabalAppSettings;
	}

	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}

}