/**
 * 
 */
package pagecode.theme;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.gannett.usat.dataHandlers.campaigns.editPages.CampaignEditsHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandlerReadOnly;
import com.gannett.usat.dataHandlers.campaigns.EditCampaignDetailsHandler;
import com.gannett.usat.dataHandlers.UserHandler;

/**
 * @author aeast
 *
 */
public class NoNav_EditCampaigns_JSPC03_blue extends PageCodeBase {

	protected HtmlGraphicImageEx imageExCampaignListImage;
	protected HtmlOutputText text1;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected CampaignEditsHandler campaignDetailEditsHandler;
	protected CampaignHandlerReadOnly defaultCampaignReadOnly;
	protected EditCampaignDetailsHandler edittingCampaignsHandler;
	protected HtmlOutputText textSysDescriptionHeader;
	protected UserHandler user;
	public String doLinkNoNavMessagesMasterSaveAction() {
		// Type Java code that runs when the component is clicked
	
		CampaignEditsHandler editor = this.getCampaignDetailEditsHandler();
		// Clean up any old images or vanities after saving.
		
		EditCampaignDetailsHandler campaigns = this.getEdittingCampaignsHandler();
		try {
			System.out.println("Master Save Invoked On " + campaigns.getCampaigns().size() + " campaigns.");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		boolean errors = false;
		for (CampaignHandler ch : campaigns.getCampaigns()){
			try {
				System.out.print("Saving " + ch.getCampaign().getCampaignName());
				ch.getCampaign().setUpdatedBy(this.getUser().getUserID());
				ch.getCampaign().save(false);
				System.out.println("...Done Saving. ");
			}
			catch (Exception e) {
				errors = true;
				FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to save Campaign: " + ch.getCampaignName() + ". Reason: " + e.getMessage(), null);
				this.getFacesContext().addMessage(null, m);
				e.printStackTrace();
			}
		}
		
		if (!errors) {
			FacesMessage m = new FacesMessage("All campaigns saved successfuly.", null);
			this.getFacesContext().addMessage(null, m);
			editor.setChanged(false);
		}
		return "success";
	}

	protected HtmlGraphicImageEx getImageExCampaignListImage() {
		if (imageExCampaignListImage == null) {
			imageExCampaignListImage = (HtmlGraphicImageEx) findComponentInRoot("imageExCampaignListImage");
		}
		return imageExCampaignListImage;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignEditsHandler getCampaignDetailEditsHandler() {
		if (campaignDetailEditsHandler == null) {
			campaignDetailEditsHandler = (CampaignEditsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{campaignDetailEditsHandler}").getValue(
							getFacesContext());
		}
		return campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCampaignDetailEditsHandler(
			CampaignEditsHandler campaignDetailEditsHandler) {
		this.campaignDetailEditsHandler = campaignDetailEditsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected CampaignHandlerReadOnly getDefaultCampaignReadOnly() {
		if (defaultCampaignReadOnly == null) {
			defaultCampaignReadOnly = (CampaignHandlerReadOnly) getFacesContext()
					.getApplication().createValueBinding(
							"#{defaultCampaignReadOnly}").getValue(
							getFacesContext());
		}
		return defaultCampaignReadOnly;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setDefaultCampaignReadOnly(
			CampaignHandlerReadOnly defaultCampaignReadOnly) {
		this.defaultCampaignReadOnly = defaultCampaignReadOnly;
	}

	/** 
	 * @managed-bean true
	 */
	protected EditCampaignDetailsHandler getEdittingCampaignsHandler() {
		if (edittingCampaignsHandler == null) {
			edittingCampaignsHandler = (EditCampaignDetailsHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEdittingCampaignsHandler(
			EditCampaignDetailsHandler edittingCampaignsHandler) {
		this.edittingCampaignsHandler = edittingCampaignsHandler;
	}

	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

}