/**
 * 
 */
package pagecode.theme;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlRequestLink;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlScriptCollector;

/**
 * @author aeast
 *
 */
public class PrimaryNoNave_JSPC03_blue extends PageCodeBase {

	protected HtmlOutputText text1;
	protected HtmlRequestLink linkNoNavMessagesMasterSave;
	protected HtmlJspPanel jspPanelMessagesPanel;
	protected HtmlScriptCollector scriptCollectorNoNavTemplateCollector;
	protected HtmlOutputText textSysDescriptionHeader;

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlRequestLink getLinkNoNavMessagesMasterSave() {
		if (linkNoNavMessagesMasterSave == null) {
			linkNoNavMessagesMasterSave = (HtmlRequestLink) findComponentInRoot("linkNoNavMessagesMasterSave");
		}
		return linkNoNavMessagesMasterSave;
	}

	protected HtmlJspPanel getJspPanelMessagesPanel() {
		if (jspPanelMessagesPanel == null) {
			jspPanelMessagesPanel = (HtmlJspPanel) findComponentInRoot("jspPanelMessagesPanel");
		}
		return jspPanelMessagesPanel;
	}

	public String doLinkNoNavMessagesMasterSaveAction() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		// return "success"; // global 
		// return "success"; // global
		System.out.println("Master Save Invoked"); 
		return "success";
	}

	protected HtmlScriptCollector getScriptCollectorNoNavTemplateCollector() {
		if (scriptCollectorNoNavTemplateCollector == null) {
			scriptCollectorNoNavTemplateCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorNoNavTemplateCollector");
		}
		return scriptCollectorNoNavTemplateCollector;
	}

	protected HtmlOutputText getTextSysDescriptionHeader() {
		if (textSysDescriptionHeader == null) {
			textSysDescriptionHeader = (HtmlOutputText) findComponentInRoot("textSysDescriptionHeader");
		}
		return textSysDescriptionHeader;
	}

}