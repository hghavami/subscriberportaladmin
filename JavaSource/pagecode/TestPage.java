/**
 * 
 */
package pagecode;

import com.gannett.usatoday.adminportal.junit.FTPUtilityTest;
import com.ibm.faces.component.html.HtmlScriptCollector;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlMessages;

/**
 * @author aeast
 *
 */
public class TestPage extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelFormBox formBoxFTPUtils;
	protected HtmlFormItem formItemImage;
	protected HtmlInputText textImageName;
	protected HtmlFormItem formItemVanity;
	protected HtmlInputText textVanity;
	protected HtmlCommandExButton buttonRunFTPTest;
	protected HtmlMessages messages1;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelFormBox getFormBoxFTPUtils() {
		if (formBoxFTPUtils == null) {
			formBoxFTPUtils = (HtmlPanelFormBox) findComponentInRoot("formBoxFTPUtils");
		}
		return formBoxFTPUtils;
	}

	protected HtmlFormItem getFormItemImage() {
		if (formItemImage == null) {
			formItemImage = (HtmlFormItem) findComponentInRoot("formItemImage");
		}
		return formItemImage;
	}

	protected HtmlInputText getTextImageName() {
		if (textImageName == null) {
			textImageName = (HtmlInputText) findComponentInRoot("textImageName");
		}
		return textImageName;
	}

	protected HtmlFormItem getFormItemVanity() {
		if (formItemVanity == null) {
			formItemVanity = (HtmlFormItem) findComponentInRoot("formItemVanity");
		}
		return formItemVanity;
	}

	protected HtmlInputText getTextVanity() {
		if (textVanity == null) {
			textVanity = (HtmlInputText) findComponentInRoot("textVanity");
		}
		return textVanity;
	}

	protected HtmlCommandExButton getButtonRunFTPTest() {
		if (buttonRunFTPTest == null) {
			buttonRunFTPTest = (HtmlCommandExButton) findComponentInRoot("buttonRunFTPTest");
		}
		return buttonRunFTPTest;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	public String doButtonRunFTPTestAction() {
		// Type Java code that runs when the component is clicked
	
		// TODO: Return outcome that corresponds to a navigation rule
		// return "success"; // global 
		// retun "success"; // global
		String vanity = this.getTextVanity().getValue().toString();
		String image = this.getTextImageName().getValue().toString();
		
		FTPUtilityTest test = new FTPUtilityTest();
		test.setImageName(image);
		test.setVanity(vanity);
		
		try {
			if (test.getVanity() != null) {
				boolean vanityExists = test.checkForVanity();
				StringBuilder messageText = new StringBuilder();
				messageText.append("Results of Vanity Check: ").append(vanityExists);
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, messageText.toString(), null);
				getFacesContext().addMessage(null, message);
			}
			if (test.getImageName() != null ) {
				boolean imageExists = test.checkForImage();
				StringBuilder messageText = new StringBuilder();
				messageText.append("Results of Image Check: ").append(imageExists);
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, messageText.toString(), null);
				getFacesContext().addMessage(null, message);
			}
		}
		catch (Exception e) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
				getFacesContext().addMessage(null, message);	
		}
		return "";
	}

}