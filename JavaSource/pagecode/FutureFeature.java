/**
 * 
 */
package pagecode;

import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;

/**
 * @author aeast
 *
 */
public class FutureFeature extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlOutputText textfuturetext;

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlOutputText getTextfuturetext() {
		if (textfuturetext == null) {
			textfuturetext = (HtmlOutputText) findComponentInRoot("textfuturetext");
		}
		return textfuturetext;
	}

}