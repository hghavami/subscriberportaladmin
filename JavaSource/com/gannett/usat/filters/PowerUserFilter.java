package com.gannett.usat.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gannett.usat.dataHandlers.UserHandler;
import com.gannett.usatoday.adminportal.users.PortalUserRoleEnum;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;

public class PowerUserFilter implements Filter {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public PowerUserFilter() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		boolean sendToNoAuthPage = true;
	    
		if (request instanceof HttpServletRequest) {
	        HttpServletRequest req = (HttpServletRequest)request;
	        
	        // only filter jsp or faces requests ignore all other
	        String requestedPage = req.getRequestURI();
	        if (! (requestedPage.endsWith(".faces") || requestedPage.endsWith(".jsp"))) {
	        	chain.doFilter(request, response);
	        	return;
	        }
	        
	        Object o = req.getSession().getAttribute("user");
	        UserHandler uh = null;
	        
	        if (o != null && o instanceof UserHandler) {
	            uh = (UserHandler)o;
	        }
	        
	        PortalUserIntf user = null;
	        if (uh != null) {
	            user = uh.getUser();
	        }
	        
	        if (user == null) {
	            sendToNoAuthPage = true;
	        }
	        else {
	            // check if user has only auto logged in.
	            if (user.hasAccess(PortalUserRoleEnum.POWER) || user.hasAccess(PortalUserRoleEnum.ADMINISTRATOR)) {
	            	sendToNoAuthPage = false;
	            }
	            else {
	            	sendToNoAuthPage = true;
	            }
	        }
	        
	        if (sendToNoAuthPage) {
	            // redirect to login page
	            HttpServletResponse res = (HttpServletResponse)response;
	            res.sendRedirect("/portaladmin/notAuthorized.faces");
	        }
	        else {
	            chain.doFilter(request, response);
	        }
	    }
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		
	}

}