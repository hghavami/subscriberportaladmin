package com.gannett.usat.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gannett.usat.dataHandlers.UserHandler;

/**
 * Servlet Filter implementation class AlreadyAuthenticatedFilter
 */
public class AlreadyAuthenticatedFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AlreadyAuthenticatedFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest)request;
		HttpSession session = req.getSession();
		UserHandler uh = (UserHandler)session.getAttribute("user");
		if (uh != null && uh.isAuthenticated()) {
			// already logged in so don't show login page
			HttpServletResponse res = (HttpServletResponse)response;
			res.sendRedirect("/portaladmin/secured/home.faces");
			//request.getRequestDispatcher("/secured/home.faces").forward(request, response);
			return;
		}
		
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
