package com.gannett.usat.filters;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gannett.usat.dataHandlers.UserHandler;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;

import java.io.IOException;

public class AuthenticationFilter implements Filter {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public AuthenticationFilter() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		boolean sendToLoginPage = true;
	    
		if (request instanceof HttpServletRequest) {
	        HttpServletRequest req = (HttpServletRequest)request;
	        
	        // only filter jsp or faces requests ignore all other
	        String requestedPage = req.getRequestURI();
	        if (! (requestedPage.endsWith(".faces") || requestedPage.endsWith(".jsp"))) {
	        	chain.doFilter(request, response);
	        	return;
	        }
	        
	        if (req.getSession().isNew()) {
	        	sendToLoginPage = true;
	        }
	        else {
		        Object o = req.getSession().getAttribute("user");
		        UserHandler uh = null;
		        
		        if (o != null && o instanceof UserHandler) {
		            uh = (UserHandler)o;
		        }
		        
		        PortalUserIntf user = null;
		        if (uh != null) {
		            user = uh.getUser();
		        }
		        
		        if (user == null) {
		            sendToLoginPage = true;
		        }
		        else {
		            // check if user has only auto logged in.
		            if (uh.isAuthenticated()) {
	                    sendToLoginPage = false;
		            }
		            else {
	                    sendToLoginPage = true;
		            }
		        }
	        }
	        
	        if (sendToLoginPage) {
	            // redirect to login page
	            FacesContext context = FacesContext.getCurrentInstance();
	            if (context != null) {
		            FacesMessage facesMsg = 
		                new FacesMessage(FacesMessage.SEVERITY_INFO, "Please login to continue.", null);
		            context.addMessage(null, facesMsg);
	            }
	            HttpServletResponse res = (HttpServletResponse)response;
	            res.sendRedirect("/portaladmin/index.faces");
	            //req.getRequestDispatcher("/index.faces").forward(request, response);
	        }
	        else {
	            chain.doFilter(request, response);
	        }
	    }
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	}

}