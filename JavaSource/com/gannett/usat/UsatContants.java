package com.gannett.usat;

import java.util.Collection;

import com.gannett.usatoday.adminportal.appConfig.PortalApplicationRuntimeConfigurations;
import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;
import com.gannett.usatoday.adminportal.products.USATProductBO;

public class UsatContants {
	
	private static final PortalApplicationRuntimeConfigurations appConfig = new PortalApplicationRuntimeConfigurations();
	
	
	public static String getProductionDomainHTTPUrlString() {
		return "http://" + UsatContants.appConfig.getProductionDomain().getValue();
	}

	public static String getProductionDomainHTTPSUrlString() {
		return "https://" + UsatContants.appConfig.getProductionDomain().getValue();
	}
	
	public static void reloadConfig() {
		UsatContants.appConfig.reloadSettings();
	}
	
	public static String getDefaultKeycodeForBrand(String brandPubCode) {
		USATProductBO prod = null;
		String code = null;
		try {
			prod = USATProductBO.fetchProduct(brandPubCode);
			code = prod.getDefaultKeycode();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return code;
	}
	
	public Collection<PortalApplicationSettingIntf> getForceEZPAYSettings() {
		Collection<PortalApplicationSettingIntf> settings = UsatContants.appConfig.getAllForceEZPAYSettings();
		
		return settings;
	}
	
}
