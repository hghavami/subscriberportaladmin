package com.gannett.usat.listeners;

import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;

import com.usatoday.util.constants.UsaTodayConstants;

public class SubscriberPortalAdminStartStopPhaseListener implements ServletContextListener {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public SubscriberPortalAdminStartStopPhaseListener() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(ServletContextEvent arg0)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("Subscriber Portal Admin Site Initializing.....");
		UsaTodayConstants.loadProperties();
		System.out.println("Subscriber Portal Admin Site Initialized.");
	}

	/* (non-Java-doc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(ServletContextEvent arg0)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// 
		System.out.println("Subscriber Portal Admin Site Shutting Down.");
	}

}