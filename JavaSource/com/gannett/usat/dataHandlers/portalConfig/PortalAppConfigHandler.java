package com.gannett.usat.dataHandlers.portalConfig;

import java.util.Collection;

import com.gannett.usatoday.adminportal.appConfig.PortalApplicationRuntimeConfigurations;
import com.gannett.usatoday.adminportal.appConfig.intf.PortalApplicationSettingIntf;

public class PortalAppConfigHandler {

	private PortalApplicationRuntimeConfigurations configs = null;
	
	public PortalAppConfigHandler() {
		super();
		configs = new PortalApplicationRuntimeConfigurations();
		
	}

	public String getUTDefaultRenewalRateCode() {
		String code = "";
		code = this.configs.getUTDefaultRenewalRateCode().getValue();
		return code;
	}

	
	public String getSportsWeeklyDefaultRenewalRateCode() {
		String code = "";
		code = this.configs.getSportsWeeklyDefaultRenewalRateCode().getValue();
		return code;
	}

	public String getUTForceEZPAYOfferRateCode() {
		String code = "";
		try {
			code = this.configs.getUTDefaultForceEZPayOfferRateCode().getValue();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	return code;
	}

	public String getSportsWeeklyForceEZPAYOfferRateCode() {
		String code = "";
		try {
			code = this.configs.getSportsWeeklyDefaultForceEZPayOfferRateCode().getValue();
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	return code;
	}

	public boolean getUTForceEZPAYOfferEnabled() {
		boolean programEnabled = false;
		PortalApplicationSettingIntf setting = this.configs.getUTForceEZPayOfferProgramEnabled();
		if (setting != null) {
			try {
				programEnabled = Boolean.parseBoolean(setting.getValue().trim());
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return programEnabled;
	}

	public boolean getSportsWeeklyForceEZPAYOfferEnabled() {
		boolean programEnabled = false;
		PortalApplicationSettingIntf setting = this.configs.getSportsWeeklyForceEZPayOfferProgramEnabled();
		if (setting != null) {
			try {
				programEnabled = Boolean.parseBoolean(setting.getValue().trim());
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return programEnabled;
	}
	
	public String getSystemDescription() {
		String sysDes = "Unknown";
		PortalApplicationSettingIntf setting = this.configs.getSubscriberPortalAdminSystemDescription();
		if (setting != null) {
			try {
				sysDes = setting.getValue().trim();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sysDes;
	}
	
	public Collection<PortalApplicationSettingIntf> getForceEZPAYApplicationSettings() {

		return this.configs.getAllForceEZPAYSettings();
	}

	public PortalApplicationRuntimeConfigurations getConfigs() {
		return configs;
	}
	
}
