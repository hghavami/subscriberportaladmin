package com.gannett.usat.dataHandlers;

import java.util.Date;

/**
 * 
 * @author aeast
 *	This class is used for request level submissions when you don't want to use any other managed bean
 *  It should only be used at Request scope.
 */
public class GenericFormHandler {

	private String valueString1 = null;
	private String valueString2 = null;
	private String valueString3 = null;
	
	private Date valueDate1 = null;
	private Date valueDate2 = null;
	
	private int valueInt1 = 0;
	
	private boolean valueBoolean1 = false;

	public String getValueString1() {
		return valueString1;
	}

	public void setValueString1(String valueString1) {
		this.valueString1 = valueString1;
	}

	public String getValueString2() {
		return valueString2;
	}

	public void setValueString2(String valueString2) {
		this.valueString2 = valueString2;
	}

	public String getValueString3() {
		return valueString3;
	}

	public void setValueString3(String valueString3) {
		this.valueString3 = valueString3;
	}

	public Date getValueDate1() {
		return valueDate1;
	}

	public void setValueDate1(Date valueDate1) {
		this.valueDate1 = valueDate1;
	}

	public Date getValueDate2() {
		return valueDate2;
	}

	public void setValueDate2(Date valueDate2) {
		this.valueDate2 = valueDate2;
	}

	public int getValueInt1() {
		return valueInt1;
	}

	public void setValueInt1(int valueInt1) {
		this.valueInt1 = valueInt1;
	}

	public boolean isValueBoolean1() {
		return valueBoolean1;
	}

	public void setValueBoolean1(boolean valueBoolean1) {
		this.valueBoolean1 = valueBoolean1;
	}
	
}
