package com.gannett.usat.dataHandlers;

import java.io.File;

public class ImageHandler {

	private byte[] imageContents = null;
	private String imageFileName = null;
	private String imageFileType = null;
	private String linkURL = null;
	private String alternateText = null;
	//private boolean existingImage = true;
	private boolean shimImage = false;
	
	public ImageHandler() {
	}

	public byte[] getImageContents() {
		return imageContents;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public String getImageFileName_NameOnly() {
		String shortName = "";
		if (imageFileName != null && imageFileName.length() > 0){
			int index = imageFileName.lastIndexOf(File.separatorChar);
			// if no file system seperator, check for web seperator
			if (index == -1) {
				index = imageFileName.lastIndexOf('/');
			}
			if (index > -1) {
				index++; // increment past last slash
				if (index < imageFileName.length()) {
					shortName = imageFileName.substring(index);
				}
				else {
					shortName = imageFileName;
				}
			}
			else {
				shortName = imageFileName;
			}
		}
		return shortName;
	}
	
	public String getImageFileType() {
		return imageFileType;
	}

	public void setImageContents(byte[] imageContents) {
		this.imageContents = imageContents;
	}

	public void setImageFileName(String newImageFileName) {
		if (newImageFileName != null && newImageFileName.indexOf("/shim.gif") > -1) {
			this.shimImage = true;
		}
		this.imageFileName = newImageFileName;
	}

	public void setImageFileType(String imageFileType) {
		this.imageFileType = imageFileType;
	}

	public String getAlternateText() {
		return alternateText;
	}

	public String getLinkURL() {
		return linkURL;
	}

	public void setAlternateText(String alternateText) {
		this.alternateText = alternateText;
	}

	public void setLinkURL(String linkURL) {
		this.linkURL = linkURL;
	}

	public boolean showBytes() {
		if (this.imageContents != null && this.imageContents.length > 0) {
			return true;
		}
		return false;
	}

	public boolean isShimImage() {
		return shimImage;
	}

	public void setShimImage(boolean shimImage) {
		this.shimImage = shimImage;
	}
}
