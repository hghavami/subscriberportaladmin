package com.gannett.usat.dataHandlers.products;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.dataHandlers.subscriptionOffers.SubscriptionTermViewHandler;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.integration.SubscriptionTermsTO;
import com.usatoday.util.constants.UsaTodayConstants;

public class DefaultKeycodeChangeHandler {
	private ProductHandler currentProduct = null;
	private String currentProductCode = "UT";
	private String previousKeycode = null;
	private String newKeycode = null;
	private String previousExpiredKeycode = null;
	private String newExpiredKeycode = null;
	public ProductHandler getCurrentProduct() {
		if (this.currentProduct == null) {
			try {
				
				ProductHandler ph = new ProductHandler();
				ph.setProduct(USATProductBO.fetchProduct(UsaTodayConstants.UT_PUBCODE));
				this.currentProduct = ph;
				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return currentProduct;
	}
	public void setCurrentProduct(ProductHandler currentProduct) {
		this.currentProduct = currentProduct;
	}
	public String getPreviousKeycode() {
		return previousKeycode;
	}
	public void setPreviousKeycode(String previousKeycode) {
		this.previousKeycode = previousKeycode;
	}
	public String getNewKeycode() {
		return newKeycode;
	}
	public void setNewKeycode(String new_Keycode) {
		if (new_Keycode != null) {
			new_Keycode = new_Keycode.toUpperCase();
		}
		this.newKeycode = new_Keycode;
	}
	public String getPreviousExpiredKeycode() {
		return previousExpiredKeycode;
	}
	public void setPreviousExpiredKeycode(String previousExpiredKeycode) {
		this.previousExpiredKeycode = previousExpiredKeycode;
	}
	public String getNewExpiredKeycode() {
		return newExpiredKeycode;
	}
	public void setNewExpiredKeycode(String new_ExpiredKeycode) {
		if (new_ExpiredKeycode != null) {
			new_ExpiredKeycode = new_ExpiredKeycode.toUpperCase();
		}
		this.newExpiredKeycode = new_ExpiredKeycode;
	}
	
	public SubscriptionOfferIntf getCurrentOffer() {
		SubscriptionOfferIntf offer = null;
		if (this.getCurrentProduct() != null){
			try {
				offer = SubscriptionOfferManager.getInstance().getOffer(this.currentProduct.getProduct().getDefaultKeycode(), this.currentProduct.getProduct().getProductCode());
			}
			catch (Exception e) {
				// 
				e.printStackTrace();
			}
		}
			
		return offer;
	}
	
	public Collection<SubscriptionTermViewHandler> getCurrentOfferTerms () {
		SubscriptionOfferIntf offer = this.getCurrentOffer();
		ArrayList<SubscriptionTermViewHandler> termsHandlerCol = new ArrayList<SubscriptionTermViewHandler>();
		
		if (offer != null) {
			for (SubscriptionTermsIntf term : offer.getTerms()) {
				SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
				termH.setTerm(term);
				
				termsHandlerCol.add(termH);
			}
		}
		return termsHandlerCol;
	}

	public Collection<SubscriptionTermViewHandler> getCurrentOfferRenewalTerms () {
		SubscriptionOfferIntf offer = this.getCurrentOffer();
		ArrayList<SubscriptionTermViewHandler> termsHandlerCol = new ArrayList<SubscriptionTermViewHandler>();
		
		if (offer != null) {
			for (SubscriptionTermsIntf term : offer.getRenewalTerms()) {
				SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
				termH.setTerm(term);
				
				termsHandlerCol.add(termH);
			}
		}
		return termsHandlerCol;
	}
	
	public SubscriptionOfferIntf getCurrentExpiredOffer() {
		SubscriptionOfferIntf offer = null;
		if (this.getCurrentProduct() != null){
			try {
				offer = SubscriptionOfferManager.getInstance().getOffer(this.currentProduct.getProduct().getExpiredOfferKeycode(), this.currentProduct.getProduct().getProductCode());
			}
			catch (Exception e) {
				// 
				e.printStackTrace();
			}
		}
			
		return offer;
	}

	public Collection<SubscriptionTermViewHandler> getCurrentExpiredOfferTerms () {
		SubscriptionOfferIntf offer = this.getCurrentExpiredOffer();
		ArrayList<SubscriptionTermViewHandler> termsHandlerCol = new ArrayList<SubscriptionTermViewHandler>();
		
		if (offer != null) {
			for (SubscriptionTermsIntf term : offer.getTerms()) {
				SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
				termH.setTerm(term);
				
				termsHandlerCol.add(termH);
			}
		}
		return termsHandlerCol;
	}

	public Collection<SubscriptionTermViewHandler> getCurrentExpiredOfferRenewalTerms () {
		SubscriptionOfferIntf offer = this.getCurrentExpiredOffer();
		ArrayList<SubscriptionTermViewHandler> termsHandlerCol = new ArrayList<SubscriptionTermViewHandler>();
		
		if (offer != null) {
			for (SubscriptionTermsIntf term : offer.getRenewalTerms()) {
				SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
				termH.setTerm(term);
				
				termsHandlerCol.add(termH);
			}
		}
		return termsHandlerCol;
	}
	
	public SubscriptionOfferIntf getNewOffer() {
		SubscriptionOfferIntf offer = null;
		if (this.getCurrentProduct() != null){
			try {
				offer = SubscriptionOfferManager.getInstance().getOffer(this.newKeycode, this.currentProduct.getProduct().getProductCode());
			}
			catch (Exception e) {
				// 				
			}
		}
			
		return offer;
	}
	
	public SubscriptionOfferIntf getNewExpiredOffer() {
		SubscriptionOfferIntf offer = null;
		if (this.getCurrentProduct() != null){
			try {
				offer = SubscriptionOfferManager.getInstance().getOffer(this.newExpiredKeycode, this.currentProduct.getProduct().getProductCode());
			}
			catch (Exception e) {
			}
		}
			
		return offer;
	}
	
	public Collection<SubscriptionTermViewHandler> getNewOfferTerms () {
		SubscriptionOfferIntf offer = this.getNewOffer();
		ArrayList<SubscriptionTermViewHandler> termsHandlerCol = new ArrayList<SubscriptionTermViewHandler>();
		
		if (offer != null) {
			for (SubscriptionTermsIntf term : offer.getTerms()) {
				SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
				termH.setTerm(term);
				
				termsHandlerCol.add(termH);
			}
		}
		else {
			SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
			SubscriptionTermsTO term  = new SubscriptionTermsTO("no keycode found", "", 0.0, "", "", "", "", "");
			termH.setTerm(term);
			termsHandlerCol.add(termH);
		}
		return termsHandlerCol;
	}
	
	public Collection<SubscriptionTermViewHandler> getNewExpiredOfferTerms () {
		SubscriptionOfferIntf offer = this.getNewExpiredOffer();
		ArrayList<SubscriptionTermViewHandler> termsHandlerCol = new ArrayList<SubscriptionTermViewHandler>();
		
		if (offer != null) {
			for (SubscriptionTermsIntf term : offer.getTerms()) {
				SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
				termH.setTerm(term);
				
				termsHandlerCol.add(termH);
			}
		}
		else {
			SubscriptionTermViewHandler termH = new SubscriptionTermViewHandler();
			SubscriptionTermsTO term  = new SubscriptionTermsTO("no keycode found", "", 0.0, "", "", "", "", "");
			termH.setTerm(term);
			termsHandlerCol.add(termH);
		}
		return termsHandlerCol;
	}
	
	
	public String getCurrentProductCode() {
		return currentProductCode;
	}
	public void setCurrentProductCode(String newCurrentProductCode) {
		if (!this.currentProductCode.equalsIgnoreCase(newCurrentProductCode)) {
			// update current product
			this.currentProductCode = newCurrentProductCode;
			
			try {
				USATProductBO prod = USATProductBO.fetchProduct(this.currentProductCode);
				this.getCurrentProduct().setProduct(prod);
			}
			catch (Exception e) {
				// 
				e.printStackTrace();
			}
		}
		
		
	}	
}
