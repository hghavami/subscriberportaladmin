package com.gannett.usat.dataHandlers.products;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.model.SelectItem;

import com.gannett.usatoday.adminportal.products.USATProductBO;

public class ProductsHandler {

	private Collection<USATProductBO> products = null;

	public Collection<USATProductBO> getProducts() {
		if (this.products == null) {
			try {
				this.products = USATProductBO.fetchProducts();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return products;
	}

	public void setProducts(Collection<USATProductBO> products) {
		this.products = products;
	}
	
	public Collection<SelectItem> getAvailableProductsSelectItems() {
		Collection<SelectItem> items = new ArrayList<SelectItem>();
		
		Collection<USATProductBO> productCol = this.getProducts();
		
		for(USATProductBO product : productCol) {
	        SelectItem si = new javax.faces.model.SelectItem();
	        si.setDescription(product.getDescription());
	        si.setLabel(product.getName());
	        si.setValue(product.getProductCode());
	        items.add(si);
		}
		return items;
	}

	public Collection<ProductHandler> getProductHandlers() {
		ArrayList<ProductHandler> handlers = new ArrayList<ProductHandler>();
		if (this.products == null) {
			try {
				this.products = USATProductBO.fetchProducts();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (USATProductBO prod : this.products) {
			ProductHandler ph = new ProductHandler();
			ph.setProduct(prod);
			handlers.add(ph);
		}
		return handlers;
	}
	
	public USATProductBO getProductForPub(String pubCode) {
		USATProductBO prod = null;
		
		if (this.products == null || this.products.size() == 0) {
			this.getProducts();
		}
		
		for (USATProductBO p : this.products) {
			if (p.getProductCode().equalsIgnoreCase(pubCode)) {
				prod = p;
			}
		}
		
		return prod;
	}
	
	public void refresh() {
		if (this.products != null) {
			this.products.clear();
			this.products = null;
		}
		this.getProducts();
	}
}
