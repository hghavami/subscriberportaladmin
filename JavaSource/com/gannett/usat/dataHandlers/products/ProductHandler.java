package com.gannett.usat.dataHandlers.products;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.PromotionDataHandler;
import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;

public class ProductHandler {

	USATProductBO product = null;

	public USATProductBO getProduct() {
		return product;
	}

	public void setProduct(USATProductBO product) {
		this.product = product;
	}
	
	public CampaignHandler getDefaultCampaignForProduct() {
		CampaignHandler ch = new CampaignHandler();
		
		if (this.product != null) {
			try {
				UsatCampaignIntf c = UsatCampaignBO.fetchCampaignForPubAndKeycode(this.product.getProductCode(),PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE);
				ch.setCampaign(c);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ch;
	}

	public Collection<PromotionDataHandler> getPromoConfigsForProduct() {
		CampaignHandler ch = this.getDefaultCampaignForProduct();
		
		ArrayList<PromotionDataHandler> promos = new ArrayList<PromotionDataHandler>();
		if (ch.getCampaign() != null) {
			try {
				EditablePromotionSetIntf pSet = ch.getCampaign().getPromotionSet();
				
				for (EditablePromotionIntf promo : pSet.getPromoConfigsCollection()) {
					PromotionDataHandler pdh = new PromotionDataHandler();
					pdh.setPromotionRow(promo);
					promos.add(pdh);
				}
				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return promos;
	}
}
