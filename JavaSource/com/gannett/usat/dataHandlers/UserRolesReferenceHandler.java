package com.gannett.usat.dataHandlers;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.model.SelectItem;

import com.gannett.usatoday.adminportal.users.PortalUserRoleEnum;

public class UserRolesReferenceHandler {

	public Collection<SelectItem> getAvailableRoles() {
		Collection<SelectItem> items = new ArrayList<SelectItem>();
		
		for(PortalUserRoleEnum role : PortalUserRoleEnum.values()) {
	        SelectItem si = new javax.faces.model.SelectItem();
	        si.setDescription(role.toString());
	        si.setLabel(role.toString());
	        si.setValue(role.getDbRepresentation());
	        items.add(si);
		}
		return items;
	}
}
