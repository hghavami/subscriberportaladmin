package com.gannett.usat.dataHandlers.campaigns;

import java.util.Comparator;

public class CampaignGroupNameComparator implements Comparator {

	public int compare(Object o1, Object o2) {
		String campaignGroupName1 = ( (CampaignHandler) o1 ).getCampaignGroup();

		String campaignGroupName2 = ( (CampaignHandler) o2 ).getCampaignGroup();

		//	uses compareTo method of String class to compare names of the employee

		return campaignGroupName1.compareTo(campaignGroupName2);
	}

}
