package com.gannett.usat.dataHandlers.campaigns;

import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;
import com.gannett.usatoday.adminportal.prodstats.ProdStatsBO;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.usatoday.UsatException;

public class CampaignStat {

	private USATProductBO product = null;
	
	private int numCampaigns = 0;
	private int numExpiredCampaigns = 0;
	private int totalNewStarts = 0;
	private int totalRenewals = 0;
	
	public CampaignStat(USATProductBO prod) throws UsatException {
		if (prod != null) {
			this.product = prod;
			this.numCampaigns = UsatCampaignBO.fetchCountOfCampaignsForPub(prod.getProductCode());
			this.totalNewStarts = ProdStatsBO.getCountOfNewStartsForProduct(prod.getProductCode());
			this.totalRenewals = ProdStatsBO.getCountOfRenewalsForProduct(prod.getProductCode());
		}
	}
	public USATProductBO getProduct() {
		return product;
	}
	public void setProduct(USATProductBO product) {
		this.product = product;
	}
	public int getNumCampaigns() {
		return numCampaigns;
	}
	public void setNumCampaigns(int numCampaigns) {
		this.numCampaigns = numCampaigns;
	}
	public int getNumExpiredCampaigns() {
		return numExpiredCampaigns;
	}
	public void setNumExpiredCampaigns(int numExpiredCampaigns) {
		this.numExpiredCampaigns = numExpiredCampaigns;
	}
	public int getTotalNewStarts() {
		return totalNewStarts;
	}
	public void setTotalNewStarts(int totalNewStarts) {
		this.totalNewStarts = totalNewStarts;
	}
	public int getTotalRenewals() {
		return totalRenewals;
	}
	public void setTotalRenewals(int totalRenewals) {
		this.totalRenewals = totalRenewals;
	}
	
}
