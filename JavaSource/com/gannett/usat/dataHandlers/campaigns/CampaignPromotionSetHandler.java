package com.gannett.usat.dataHandlers.campaigns;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usat.UsatContants;
import com.gannett.usat.dataHandlers.campaigns.editPages.DynamicLinkHandler;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
 
public class CampaignPromotionSetHandler {

	public static final String shimImagePath = "/portaladmin/images/marketingimages/shim.gif";
	public static final String noImagePath = "/portaladmin/images/noImageIndicator.gif";
		
	private EditablePromotionSetIntf source = null;
	
	public EditablePromotionSetIntf getSource() {
		return source;
	}

	public void setSource(EditablePromotionSetIntf source) {
		this.source = source;
	}
	
	public String getTemplateNavigationImagePath() {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getTemplateNavigationPromoImage();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}
	/**
	 * 
	 * @return Path to the current upsell image or to a blank image
	 */
	public String getCompletePageUpSellImagePath() {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getPromoImageComplete();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}
	
	public String getEZPayCustomPaymentPageText() {
		String text = "";
		
		if (this.source != null) {
			EditableHTMLPromotionIntf ezPayPromo = this.source.getPromoEZPayCustomText();
			if (ezPayPromo != null) {
				text =  ezPayPromo.getPromotionalHTML();
			}
		}
		return text;
	}

	public String getLandingPagePromoText() {
		String text = "";
		
		if (this.source != null) {
			EditableHTMLPromotionIntf promo = this.source.getPromoLandingPageText();
			if (promo != null) {
				text =  promo.getPromotionalHTML();
			}
		}
		return text;
	}

	public String getProductImage1Path () {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getProductImage1();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}
	
	public String getLandingPagePromoImage2Path () {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getLandingPagePromoImage2();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}

	public String getLandingPagePromoImage3Path () {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getLandingPagePromoImage3();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}

	
	public String getLandingPagePromoImage1Path () {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getLandingPagePromoImage1();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}

	public String getLandingPageFooterImagePath () {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getLandingPageFooterPromoImage();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}

	public String getNewOrderPathPopOverlayPath () {
		String path = CampaignPromotionSetHandler.noImagePath;
		
		if (this.source != null) {
			EditableImagePromotionIntf imagePromo = this.source.getNewSubscriptionOrderPopUpOverlay();
			if (imagePromo != null && !imagePromo.isShimImage()) {
				path = UsatContants.getProductionDomainHTTPUrlString() + imagePromo.getImagePathString();
			}
		}
		return path;
	}

	public String getOnePageOrderEntryPromoText1 () {
		String html = null;
		
		if (this.source != null) {
			EditableHTMLPromotionIntf htmlPromo = this.source.getOnePagePromoSpot1Text();
			if (htmlPromo != null && htmlPromo.getPromotionalHTML() != null) {
				html = htmlPromo.getPromotionalHTML();
			}
			else {
				html = "DEFAULT VALUE IS BLANK";
			}
		}
		return html;
	}

	public String getOnePageOrderEntryPromoForceEZPayTermsText () {
		String html = null;
		
		if (this.source != null) {
			EditableHTMLPromotionIntf htmlPromo = this.source.getOnePageForceEZPayTermsText();
			if (htmlPromo != null && htmlPromo.getPromotionalHTML() != null) {
				html = htmlPromo.getPromotionalHTML();
			}
			else {
				html = "DEFAULT VALUE IS BLANK";
			}
		}
		return html;
	}

	public String getForceEZPAYOverridePromoStatusString () {
		String status = "N/A";
		
		if (this.source != null) {
			EditablePromotionIntf promo = this.source.getForceEZPAY();
			if (promo != null && promo.getFulfillText() != null) {
				if (promo.getFulfillText().equalsIgnoreCase("HIDE")) {
					status = "Hidden";
				}
				else {
					status = promo.getFulfillUrl();
				}
			}
			
		}
		return status;
	}
	
	public String getOnePageOrderEntryDisclaimer () {
		String html = null;
		
		if (this.source != null) {
			EditableHTMLPromotionIntf htmlPromo = this.source.getOnePageDisclaimerText();
			if (htmlPromo != null && htmlPromo.getPromotionalHTML() != null) {
				html = htmlPromo.getPromotionalHTML();
			}
			else {
				html = "DEFAULT VALUE IS BLANK";
			}
		}
		return html;
	}

	public Collection<DynamicLinkHandler> getOrderEntryDynamicLinks () {
		ArrayList<DynamicLinkHandler> linksHTML = new ArrayList<DynamicLinkHandler>();
		
		if (this.source != null) {
			EditableHTMLPromotionIntf htmlPromo = this.source.getDynamicNavigationOrderEntryHTML();
			
			if ( (htmlPromo != null) && (htmlPromo.getPromotionalHTML().length() > 0)) {
				String source = htmlPromo.getPromotionalHTML();
				int length = source.length();
				
				// sample
				// <LI><A href="javascript:_shopUSAT()">Order back issues</A></LI>
				
				String delim = "</LI>";
				
				int lastEndOfLink = source.lastIndexOf(delim);
				int endOfLink = source.indexOf(delim);
				
				if (lastEndOfLink == endOfLink) {
					DynamicLinkHandler lh = new DynamicLinkHandler();
					lh.setLink(source);
					linksHTML.add(lh);
				}
				else {
					// iterate
					endOfLink += 5; // move five more to just beyond the > of the </LI>
					String tempStr = source.substring(0, endOfLink);
					DynamicLinkHandler lh = new DynamicLinkHandler();
					lh.setLink(tempStr);
					linksHTML.add(lh);
					int nextStart = endOfLink;
					while (endOfLink <= length) {
						endOfLink = source.indexOf(delim, endOfLink);
						if (endOfLink > 0) {
							endOfLink += 5;
							tempStr = source.substring(nextStart, endOfLink);
							lh = new DynamicLinkHandler();
							lh.setLink(tempStr);
							linksHTML.add(lh);
							nextStart = endOfLink;
						}
						else {
							break;
						}
					}
				}
			}
		}
		return linksHTML;
	}

	public Collection<DynamicLinkHandler> getCustServiceDynamicLinks () {
		ArrayList<DynamicLinkHandler> linksHTML = new ArrayList<DynamicLinkHandler>();
		
		if (this.source != null) {
			EditableHTMLPromotionIntf htmlPromo = this.source.getDynamicNavigationCustomerServiceHTML();
			
			if ( (htmlPromo != null) && (htmlPromo.getPromotionalHTML().length() > 0)) {
				String source = htmlPromo.getPromotionalHTML();
				int length = source.length();
				
				// sample
				// <LI><A href="javascript:_shopUSAT()">Order back issues</A></LI>
				
				String delim = "</LI>";
				
				int lastEndOfLink = source.lastIndexOf(delim);
				int endOfLink = source.indexOf(delim);
				
				if (lastEndOfLink == endOfLink) {
					DynamicLinkHandler lh = new DynamicLinkHandler();
					lh.setLink(source);
					linksHTML.add(lh);
				}
				else {
					// iterate
					endOfLink += 5; // move five more to just beyond the > of the </LI>
					String tempStr = source.substring(0, endOfLink);
					DynamicLinkHandler lh = new DynamicLinkHandler();
					lh.setLink(tempStr);
					linksHTML.add(lh);
					int nextStart = endOfLink;
					while (endOfLink <= length) {
						endOfLink = source.indexOf(delim, endOfLink);
						if (endOfLink > 0) {
							endOfLink += 5;
							tempStr = source.substring(nextStart, endOfLink);
							lh = new DynamicLinkHandler();
							lh.setLink(tempStr);
							linksHTML.add(lh);
							nextStart = endOfLink;
						}
						else {
							break;
						}
					}
				}
			}
		}
		return linksHTML;
	}

	public Collection<EditablePromotionIntf> getAllPromoConfigs() {
		
		if (this.source != null) {
			return this.source.getPromoConfigsCollection();
		}
		return null;
	}
}
