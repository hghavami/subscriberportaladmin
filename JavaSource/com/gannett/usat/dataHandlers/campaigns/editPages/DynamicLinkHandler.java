package com.gannett.usat.dataHandlers.campaigns.editPages;

public class DynamicLinkHandler {
	private String link = null;
	private boolean selected = false;
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
}
