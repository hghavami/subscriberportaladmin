package com.gannett.usat.dataHandlers.campaigns.editPages;

import com.gannett.usat.dataHandlers.ImageHandler;

public class WelcomePageHandler {
	
	private String promoText = null;
	
	private ImageHandler image1Image = new ImageHandler();   // used for file uploads
	private ImageHandler image2Image = new ImageHandler();   // used for file uploads
	private ImageHandler image3Image = new ImageHandler();   // used for file uploads
	private ImageHandler footerImage = new ImageHandler();   // used for file uploads

	private String image1ImageOption = "default";
	private String image2ImageOption = "default";
	private String image3ImageOption = "default";
	private String footerImageOption = "default";
	
	
	/** 
	 * @managed-bean true
	 */
	
	
	
	public String getFooterImageOption() {
			
			return this.footerImageOption;
	}
	
	public void setFooterImageOption(String newFooterImageOption) {
		
		this.footerImageOption = newFooterImageOption;
	
	}
	
	public String getImage2ImageOption() {
		
		return this.image2ImageOption;
	}
	
	public void setImage2ImageOption(String newImage2ImageOption) {
	
		this.image2ImageOption = newImage2ImageOption;
	
	}

	public String getImage1ImageOption() {
		return this.image1ImageOption;
	}
	public void setImage1ImageOption(String newImage1ImageOption) {
		this.image1ImageOption = newImage1ImageOption;
	}

	public String getImage3ImageOption() {
		return this.image3ImageOption;
	}
	public void setImage3ImageOption(String newImage3ImageOption) {
	
		this.image3ImageOption = newImage3ImageOption;
	
	}	
	
	public ImageHandler getImage1Image() {
		
		return this.image1Image;
	}
	
	public void setImage1Image(ImageHandler newImage1Image) {
		
		this.image1Image = newImage1Image;	
	}
	
	public ImageHandler getImage3Image() {
		
		return this.image3Image;
	}
	
	public void setImage3Image(ImageHandler newImage3Image) {
		
		this.image3Image = newImage3Image;	
	}
	
	
	public ImageHandler getImage2Image() {
		
		return this.image2Image;
	}
	
	public void setImage2Image(ImageHandler newImage2Image) {
		
		this.image2Image = newImage2Image;	
	}
	
	public ImageHandler getFooterImage() {
		
		return this.footerImage;
	}
	
	public void setFooterImage(ImageHandler newFooterImage) {
		
		this.footerImage = newFooterImage;	
	}

	public String getPromoText() {
		return promoText;
	}

	public void setPromoText(String newPromoText) {
		this.promoText = newPromoText;
	}	
		
}
