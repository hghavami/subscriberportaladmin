package com.gannett.usat.dataHandlers.campaigns.editPages;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignPromotionSetHandler;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditablePromotionsSetBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;

public class CoverGraphicsHandler {
	
	private ImageHandler termsPageUtCoverGraphics = new ImageHandler();   // used for file uploads
	private ImageHandler termsPageSwCoverGraphics = new ImageHandler(); // the current setting
	
	private UsatCampaignIntf campaignSw = null;
	private CampaignPromotionSetHandler sourceSwPromosHandler = null;
	 
	private UsatCampaignIntf campaignUt = null;
	private CampaignPromotionSetHandler sourceUtPromosHandler = null;
	
	    
//	 chosen option
	private String utCoverGraphicsImageOption = CampaignEditsHandler.UT_COVER_IMAGE_NAME;
	private String swCoverGraphicsImageOption = CampaignEditsHandler.SW_COVER_IMAGE_NAME;	
	
// Overlay PopUpImages
	private ImageHandler uTSubscribePathOverlayGraphicImage = new ImageHandler();   // used for file uploads
	private String utSubscribePathPopUpOverlayImageOption = CampaignEditsHandler.USE_DEFAULT_IMAGE;
	
	private CampaignHandler defaultUtCampaign = null;
	private CampaignHandler defaultSwCampaign = null;
	protected CampaignHandler utLandingPages;
	protected CampaignHandler swLandingPages;

	private ImageHandler currentImageGraphics = new ImageHandler(); // the current setting
	private String currentGraphicImageOption = CampaignEditsHandler.USE_DEFAULT_IMAGE;
	private CampaignHandler currentCampaign = null;
	
	
	/** 
	 * @managed-bean true
	 */
	protected void setUtLandingPages(CampaignHandler utLandingPages) {
		this.utLandingPages = utLandingPages;
	}
	
	public void setUtSourceCampaign(CampaignHandler c) {
		if (c != null) {
			this.campaignUt = c.getCampaign();
			
			EditablePromotionSetIntf copyOfPromos = new EditablePromotionsSetBO(c.getCampaign().getPromotionSet());
			CampaignPromotionSetHandler psh = new CampaignPromotionSetHandler();
			psh.setSource(copyOfPromos);
			
		//	EditableImagePromotionIntf ei = c.getCampaign().getPromotionSet().getNewSubscriptionOrderPopUpOverlay();
			
		//	if (ei != null && !ei.isShimImage()) {
		//		utSubscribePathPopUpOverlayImageOption = CampaignEditsHandler.USE_CUSTOM_IMAGE;
		//		this.uTSubscribePathOverlayGraphicImage.setAlternateText(ei.getImageAltText());
		//		this.uTSubscribePathOverlayGraphicImage.setImageContents(ei.getImageContents());
				//this.uTSubscribePathOverlayGraphicImage.setImageFileName(ei.getImagePathString());
		//		this.uTSubscribePathOverlayGraphicImage.setLinkURL(ei.getImageLinkToURL());
				
		//	}
			this.setUtSourcePromosHandler(psh);
			
		}
	}
	
	public void setSwSourceCampaign(CampaignHandler c) {
		if (c != null) {
			this.campaignSw = c.getCampaign();
			
			EditablePromotionSetIntf copyOfPromos = new EditablePromotionsSetBO(c.getCampaign().getPromotionSet());
			CampaignPromotionSetHandler psh = new CampaignPromotionSetHandler();
			psh.setSource(copyOfPromos);
			
			this.setSwSourcePromosHandler(psh);
			
		}
	}
	private void setSwSourcePromosHandler(
			CampaignPromotionSetHandler sourcePromosHandler) {
		this.sourceSwPromosHandler = sourcePromosHandler;
	}
	public void setUtCoverImage(ImageHandler newUtCoverImage) {
		
			this.termsPageUtCoverGraphics = newUtCoverImage;
		
	}
	private void setUtSourcePromosHandler(
			CampaignPromotionSetHandler sourcePromosHandler) {
		this.sourceUtPromosHandler = sourcePromosHandler;
	}
	public void setSwCoverImage(ImageHandler newSwCoverImage) {
		
			this.termsPageSwCoverGraphics = newSwCoverImage;
		
	}
	
	public CampaignHandler getDefaultUtCampaign() {
		return defaultUtCampaign;
	}
	
	public CampaignHandler getDefaultSwCampaign() {
		return defaultSwCampaign;
	}
	public void setDefaultUtCampaign(CampaignHandler defaultCampaign) {
		this.defaultUtCampaign = defaultCampaign;
	}

	public void setDefaultSwCampaign(CampaignHandler defaultCampaign) {
		this.defaultSwCampaign = defaultCampaign;
	}

	
	public ImageHandler getSwCoverImage() {
		
	//overGraphicsHandler editor = this.
		/*
		termsPageSwCoverGraphics = new ImageHandler();
		EditableImagePromotionIntf sourceImage = this.defaultSwCampaign.getCampaign().getPromotionSet().getPromoImageTerms();
	
			if (sourceImage != null) {
				termsPageSwCoverGraphics.setAlternateText(sourceImage.getImageAltText());
				termsPageSwCoverGraphics.setLinkURL(sourceImage.getImageLinkToURL());
				termsPageSwCoverGraphics.setImageFileName(sourceImage.getImagePathString());
				termsPageSwCoverGraphics.setImageContents(sourceImage.getImageContents());
				termsPageSwCoverGraphics.setImageFileType(sourceImage.getImageFileType());
				
			}
			else {
				termsPageSwCoverGraphics.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
			}
		*/
		return this.termsPageSwCoverGraphics;
	}
	
	

	public ImageHandler getUtCoverImage() {
	/*	termsPageUtCoverGraphics = new ImageHandler();
		EditableImagePromotionIntf sourceImage = this.defaultUtCampaign.getCampaign().getPromotionSet().getPromoImageTerms();
	
			if (sourceImage != null) {
				termsPageUtCoverGraphics.setAlternateText(sourceImage.getImageAltText());
				termsPageUtCoverGraphics.setLinkURL(sourceImage.getImageLinkToURL());
				termsPageUtCoverGraphics.setImageFileName(sourceImage.getImagePathString());
				termsPageUtCoverGraphics.setImageContents(sourceImage.getImageContents());
				termsPageUtCoverGraphics.setImageFileType(sourceImage.getImageFileType());
				
			}
			else {
				termsPageUtCoverGraphics.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
			}
		*/
		return this.termsPageUtCoverGraphics;
	}
	public ImageHandler getUTSubscribePathOverlayGraphicImage() {
		return uTSubscribePathOverlayGraphicImage;
	}
	public void setUTSubscribePathOverlayGraphicImage(ImageHandler subscribePathUTGraphic) {
		this.uTSubscribePathOverlayGraphicImage = subscribePathUTGraphic;
	}
	public String getUtSubscribePathPopUpOverlayImageOption() {
		return utSubscribePathPopUpOverlayImageOption;
	}
	public void setUtSubscribePathPopUpOverlayImageOption(
			String utSubscribePathPopUpOverlayImageOption) {
		this.utSubscribePathPopUpOverlayImageOption = utSubscribePathPopUpOverlayImageOption;
	}

	public boolean isShowCurrentUTSubscribePathOverlayImage() {
		if (this.utSubscribePathPopUpOverlayImageOption.equalsIgnoreCase(CampaignEditsHandler.USE_CUSTOM_IMAGE) && !this.uTSubscribePathOverlayGraphicImage.isShimImage() && this.uTSubscribePathOverlayGraphicImage.getImageContents() != null) {
			return true;
		}
		else {
			return false;
		}
	}

	public void setShowCurrentUTSubscribePathOverlayImage(boolean showCurrentImage) {
		; // ignore
	}

	public ImageHandler getCurrentImageGraphics() {
		return currentImageGraphics;
	}

	public void setCurrentImageGraphics(ImageHandler currentImageGraphics) {
		this.currentImageGraphics = currentImageGraphics;
	}

	public CampaignHandler getCurrentCampaign() {
		return currentCampaign;
	}

	public void setCurrentCampaign(CampaignHandler currentCampaign) {
		this.currentCampaign = currentCampaign;
	}

	public String getCurrentGraphicImageOption() {
		return currentGraphicImageOption;
	}

	public void setCurrentGraphicImageOption(String currentGraphicImageOption) {
		this.currentGraphicImageOption = currentGraphicImageOption;
	}
	
}
