package com.gannett.usat.dataHandlers.campaigns.editPages;

import java.io.File;
import java.util.ArrayList;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignHandler;
import com.gannett.usat.dataHandlers.campaigns.CampaignPromotionSetHandler;
import com.gannett.usatoday.adminportal.campaigns.intf.RelRtCodeIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableCreditCardPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableHTMLPromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.products.promotions.PromotionManager;
import com.usatoday.businessObjects.products.promotions.PromotionSet;

/**
 * This class handles the settings on the campaign edit details pages.
 * Changes should be set on the set of edittable campaigns found in the 
 * EditCampaignDetailsHandler class.
 * 
 * @author aeast
 *
 */
public class CampaignEditsHandler {
	public static final String UT_COVER_IMAGE_NAME = "usat_covers.jpg";
	public static final String SW_COVER_IMAGE_NAME = "bbw_covers.jpg";
		
	public static final String USE_DEFAULT_IMAGE = "default";
	public static final String NO_IMAGE = "none";
	public static final String USE_CUSTOM_IMAGE = "custom";
	
	// used with Force EZPAY Overrides
	public static final String USE_DEFAULT_VALUE = "default";
	public static final String USE_CUSTOM_VALUE = "OVRD";
	public static final String USE_HIDDEN_VALUE = "HIDE";
	
	public static final String PROD_SHIM_PATH = "/images/shim.gif";
	public static final String OMIT_EZPAY_CUSTOM_TEXT = "<!-- EZ Pay Offer Ommitted -->";

	public static final String[] DEFAULT_PAYMENT_METHODS = {EditableCreditCardPromotionIntf.AMEX,EditableCreditCardPromotionIntf.VISA,EditableCreditCardPromotionIntf.MC,EditableCreditCardPromotionIntf.DISCOVERY, EditableCreditCardPromotionIntf.DINERS};
	
	private boolean changed  = false;
	
	private CampaignHandler sourceCampaign = null;
	private CampaignHandler defaultCampaign = null;
	
	// New Order PopOverlay Image data
	private ImageHandler newOrderPopUpOverlayCustomImage = new ImageHandler();   // used for file uploads
	private ImageHandler currentNewOrderPopUpOverlayImage = new ImageHandler(); // the current setting
	private ImageHandler defaultNewOrderPopUpOverlayImage = null; // the default campaign image
	private String newOrderPopUpOverlayImageOption = CampaignEditsHandler.USE_DEFAULT_IMAGE;
	
	// Payment Page Data
	private boolean overrideZPayPaymentPageText = false; 
	private String customEZPayPaymentPageText = null;
	private String currentEZPayPaymentPageText = null;
	private String[] paymentMethods = CampaignEditsHandler.DEFAULT_PAYMENT_METHODS;

	// Product Data
	private ImageHandler productImage1 = new ImageHandler();   // used for file uploads
	private ImageHandler currentProductImage1 = new ImageHandler(); // the current setting
	private ImageHandler defaultProductImage1 = null; // the default campaign image
	private String productImage1Option = CampaignEditsHandler.USE_DEFAULT_IMAGE;
	private String currentTermsAndConditionsText = null;
	private String customTermsAndConditionsText = null;
	
	// One Page Order Entry Page Data
	private String customOnePagePromoText1 = null;
	private String currentOnePagePromoText1 = null;	
	private String currentOnePagePromoText1Raw = null;	
	private String customOnePageDisclaimerText = null;
	private String currentOnePageDisclaimerText = null;
	private String customOnePageForceEZPayTermsText = null;
	private String currentOnePageForceEZPayTermsText = null;
	private boolean overrideDefaultAllowOfferOverride = false;  // if true, use the keycode setting
	private boolean customAllowOfferOverrideSetting = false;  // true if allow overrides otherwise false
	private boolean defaultAllowOverrideSetting = false;
	
	// FORCE EZPAY Override
	private String forceEZPAYRateCodeToLookUp = null;
	private RelRtCodeIntf forceEZPAYRatelastRateLookedUp =null;
	private String forceEZPAYOverrideRateOption = CampaignEditsHandler.USE_DEFAULT_VALUE;  // 'default', 'custom', 'hide'
	
	
	///////////////////////////////
	public CampaignEditsHandler() {
		super();
	}

	public CampaignHandler getSourceCampaign() {
		return sourceCampaign;
	}
	
	public void setSourceCampaign(CampaignHandler sourceCampaign) {
		
		this.resetHandler();
		
		this.sourceCampaign = sourceCampaign;
		
		EditablePromotionSetIntf promotions = this.sourceCampaign.getCampaign().getPromotionSet();
		

		EditableImagePromotionIntf tempImage = null;
		EditableHTMLPromotionIntf tempHTML = null;
		
		// initialize current new order subscribe overlay image
		tempImage = promotions.getNewSubscriptionOrderPopUpOverlay();
		if (tempImage != null) {
			if (tempImage.getImagePathString().indexOf("shim.gif") > -1) {
				this.newOrderPopUpOverlayImageOption = CampaignEditsHandler.NO_IMAGE;
			}
			else {
				this.newOrderPopUpOverlayImageOption = CampaignEditsHandler.USE_CUSTOM_IMAGE;
				this.currentNewOrderPopUpOverlayImage = new ImageHandler();
				this.currentNewOrderPopUpOverlayImage.setAlternateText(tempImage.getImageAltText());
				this.currentNewOrderPopUpOverlayImage.setImageFileName(tempImage.getImagePathString());
				this.currentNewOrderPopUpOverlayImage.setLinkURL(tempImage.getImageLinkToURL());
				this.currentNewOrderPopUpOverlayImage.setImageContents(tempImage.getImageContents());
				this.currentNewOrderPopUpOverlayImage.setImageFileType(tempImage.getImageFileType());
			}
		}
		
		// initialize the current payment page settings
		tempHTML = promotions.getPromoEZPayCustomText();
		if (tempHTML != null) {
			
			String value = tempHTML.getPromotionalHTML();
			if (CampaignEditsHandler.OMIT_EZPAY_CUSTOM_TEXT.equalsIgnoreCase(value)) {
				this.currentEZPayPaymentPageText = "";
			}
			else {
				this.currentEZPayPaymentPageText = tempHTML.getPromotionalHTML();
				this.customEZPayPaymentPageText = tempHTML.getPromotionalHTML(); // want the current value to display in entry field too.
			}
			this.overrideZPayPaymentPageText = true;
		}
		
		EditableCreditCardPromotionIntf ccPromo = promotions.getPromoCreditCard();
		if (ccPromo != null) {
			ArrayList<String> ccCards = new ArrayList<String>();
			if (ccPromo.acceptsAmEx()) {
				ccCards.add(EditableCreditCardPromotionIntf.AMEX);
			}
			if (ccPromo.acceptsVisa()) {
				ccCards.add(EditableCreditCardPromotionIntf.VISA);
			}
			if (ccPromo.acceptsMasterCard()){
				ccCards.add(EditableCreditCardPromotionIntf.MC);
			}
			if (ccPromo.acceptsDiscovery()) {
				ccCards.add(EditableCreditCardPromotionIntf.DISCOVERY);
			}
			if (ccPromo.acceptsDiners()) {
				ccCards.add(EditableCreditCardPromotionIntf.DINERS);
			}
			this.paymentMethods = new String[ccCards.size()];
			int i = 0;
			for (String m : ccCards) {
				this.paymentMethods[i] = m;
				i++;
			}
		}

		// initialize the product image
		tempImage = promotions.getProductImage1();
		if (tempImage != null) {
			if (tempImage.getImagePathString().indexOf("shim.gif") > -1) {
				this.productImage1Option = CampaignEditsHandler.NO_IMAGE;
			}
			else {
				this.productImage1Option = CampaignEditsHandler.USE_CUSTOM_IMAGE;
				this.currentProductImage1 = new ImageHandler();
				this.currentProductImage1.setAlternateText(tempImage.getImageAltText());
				this.currentProductImage1.setImageFileName(tempImage.getImagePathString());
				this.currentProductImage1.setLinkURL(tempImage.getImageLinkToURL());
				this.currentProductImage1.setImageContents(tempImage.getImageContents());
				this.currentProductImage1.setImageFileType(tempImage.getImageFileType());
			}
		}


		
		tempHTML = promotions.getTermsAndConditionsText();
		if (tempHTML != null) {
			this.currentTermsAndConditionsText = tempHTML.getPromotionalHTML();
			this.customTermsAndConditionsText = tempHTML.getPromotionalHTML();
		}		
		 
		// initialize the one page order promotions
		tempHTML = promotions.getOnePagePromoSpot1Text();
		if (tempHTML != null) {
			this.currentOnePagePromoText1 = tempHTML.getPromotionalHTML();
			this.currentOnePagePromoText1Raw = tempHTML.getPromotionalHTML();
		}		
		else {
			this.currentOnePagePromoText1 = "DEFAULT VALUE WILL BE USED";
			this.currentOnePagePromoText1Raw = "";
		}

		tempHTML = promotions.getOnePageDisclaimerText();
		if (tempHTML != null) {
			this.currentOnePageDisclaimerText = tempHTML.getPromotionalHTML();
			this.customOnePageDisclaimerText = tempHTML.getPromotionalHTML();
		}		
		else {
			this.currentOnePageDisclaimerText = "DEFAULT VALUE WILL BE USED";
		}
		
		tempHTML = promotions.getOnePageForceEZPayTermsText();
		if (tempHTML != null) {
			this.currentOnePageForceEZPayTermsText = tempHTML.getPromotionalHTML();
			this.customOnePageForceEZPayTermsText = tempHTML.getPromotionalHTML();
		}		
		else {
			this.currentOnePageForceEZPayTermsText = "DEFAULT VALUE WILL BE USED";
		}
		
		EditablePromotionIntf tempPromo  = promotions.getAllowOfferCodeOverrides();
		if (tempPromo != null) {
			this.overrideDefaultAllowOfferOverride = true;
			if (tempPromo.getFulfillText().equalsIgnoreCase("Y")) {
				this.customAllowOfferOverrideSetting = true;
			}
		}
		
		
		PromotionIntf defaultPromoSetting = null;
		try {
			defaultPromoSetting = promotions.getDefaultPubPromotionSet().getAllowOfferOverrides();
		}
		catch (Exception e) {
			defaultPromoSetting = null;
		}
		this.defaultAllowOverrideSetting = false;
		if (defaultPromoSetting != null) {
			// if nothing set at default then we don't allow it
			if (defaultPromoSetting.getFulfillText().equalsIgnoreCase("Y")) {
				this.defaultAllowOverrideSetting = true;
			}
		}
		
		// initialize force EZPay option page
		tempPromo = promotions.getForceEZPAY();
		if (tempPromo != null) {
			if (tempPromo.getFulfillText()!= null && tempPromo.getFulfillText().equalsIgnoreCase(CampaignEditsHandler.USE_HIDDEN_VALUE)){
				this.forceEZPAYOverrideRateOption = CampaignEditsHandler.USE_HIDDEN_VALUE;
				this.forceEZPAYRateCodeToLookUp = null;
			}
			else {
				this.forceEZPAYOverrideRateOption = CampaignEditsHandler.USE_CUSTOM_VALUE;
				this.forceEZPAYRateCodeToLookUp = tempPromo.getFulfillUrl();
			}
		}
		
	}
	
	public void resetHandler() {
		this.changed = false;
		this.sourceCampaign = null;
		
	// New Order PopUp Overlay data
		this.newOrderPopUpOverlayCustomImage = new ImageHandler();   // used for file uploads
		this.currentNewOrderPopUpOverlayImage = new ImageHandler(); // the current setting
		this.defaultNewOrderPopUpOverlayImage = null; // the default campaign image
		this.newOrderPopUpOverlayImageOption = CampaignEditsHandler.USE_DEFAULT_IMAGE;

	
	// Payment Page Data
		this.overrideZPayPaymentPageText = false; 
		this.customEZPayPaymentPageText = null;
		this.currentEZPayPaymentPageText = null;
		this.paymentMethods = CampaignEditsHandler.DEFAULT_PAYMENT_METHODS;

	// Product Data
		this.customTermsAndConditionsText = null;
		this.currentTermsAndConditionsText = null;
		this.productImage1 = new ImageHandler();   // used for file uploads
		this.currentProductImage1 = new ImageHandler(); // the current setting
		this.defaultProductImage1 = null; // the default campaign image
		this.productImage1Option = CampaignEditsHandler.USE_DEFAULT_IMAGE;

	// One Page Order Entry Data
		this.currentOnePageDisclaimerText = "DEFAULT VALAUE WILL BE USED";
		this.currentOnePagePromoText1 = "DEFAULT VALAUE WILL BE USED";
		this.currentOnePagePromoText1Raw = "";
		this.customOnePageDisclaimerText = null;
		this.customOnePagePromoText1 = null;
		this.currentOnePageForceEZPayTermsText = "DEFAULT VALAUE WILL BE USED";
		this.customOnePageForceEZPayTermsText = null;
		this.customAllowOfferOverrideSetting = false;
		this.defaultAllowOverrideSetting = true;
		this.overrideDefaultAllowOfferOverride = false;
		
	// FORCE EZPAY
		this.forceEZPAYOverrideRateOption = CampaignEditsHandler.USE_DEFAULT_VALUE;
		this.forceEZPAYRateCodeToLookUp = null;
		this.forceEZPAYRatelastRateLookedUp = null;
	}
	
	public boolean isChanged() {
		return changed;
	}
	public boolean getChanged() {
		return changed;
	}
	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	public CampaignHandler getDefaultCampaign() {
		return defaultCampaign;
	}
	public void setDefaultCampaign(CampaignHandler defaultCampaign) {
		this.defaultCampaign = defaultCampaign;
	}


	///////////////////////////////////////////////////////
	//  Payment Page settings/ovverides
	///////////////////////////////////////////////////////
	public String getCurrentEZPayPaymentPageText() {
		if (this.currentEZPayPaymentPageText == null) {
			if (!this.overrideZPayPaymentPageText) { 
				this.currentEZPayPaymentPageText = this.getDefaultEZPAYPaymentPageText();
			}
			else {
				this.currentEZPayPaymentPageText = "";
			}
		}
		return currentEZPayPaymentPageText;
	}

	public String getCustomEZPayPaymentPageText() {
		return customEZPayPaymentPageText;
	}

	public boolean isOverrideZPayPaymentPageText() {
		return overrideZPayPaymentPageText;
	}

	public void setCurrentEZPayPaymentPageText(String newCurrentEZPayPaymentPageText) {
		if (newCurrentEZPayPaymentPageText == null) {
			this.currentEZPayPaymentPageText = this.getDefaultEZPAYPaymentPageText();
		}
		else {
			this.currentEZPayPaymentPageText = newCurrentEZPayPaymentPageText;
		}
	}

	public void setCustomEZPayPaymentPageText(String customEZPayPaymentPageText) {
		this.customEZPayPaymentPageText = customEZPayPaymentPageText;
	}

	public void setOverrideZPayPaymentPageText(
			boolean overrideZPayPaymentPageText) {
		this.overrideZPayPaymentPageText = overrideZPayPaymentPageText;
	}
	public String getDefaultEZPAYPaymentPageText () { 
		String defaultText = "";
		if (this.defaultCampaign != null && this.defaultCampaign.getCampaign() != null) {
			EditableHTMLPromotionIntf textEZPay = this.defaultCampaign.getCampaign().getPromotionSet().getPromoEZPayCustomText();
			if (textEZPay != null) {
				defaultText = textEZPay.getPromotionalHTML();
				if (defaultText == null || defaultText.trim().length() == 0) {
					defaultText = "";
				}
			}			
		}		
		return defaultText;
	}
	public void setDefaultEZPAYPaymentPageText (String whatever) {
		; // do nothing
	}

	public String[] getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(String[] paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public boolean getAcceptsAmexEx() {
		boolean acceptsCard = false;
		if (this.paymentMethods != null) {
			for(int i =0; i < this.paymentMethods.length; i++) {
				if (this.paymentMethods[i].equalsIgnoreCase(EditableCreditCardPromotionIntf.AMEX)) {
					acceptsCard = true;
					break;
				}
			}
		}
		return acceptsCard;
	}
	public boolean getAcceptsVisa() {
		boolean acceptsCard = false;
		if (this.paymentMethods != null) {
			for(int i =0; i < this.paymentMethods.length; i++) {
				if (this.paymentMethods[i].equalsIgnoreCase(EditableCreditCardPromotionIntf.VISA)) {
					acceptsCard = true;
					break;
				}
			}
		}
		return acceptsCard;
	}
	public boolean getAcceptsMasterCard() {
		boolean acceptsCard = false;
		if (this.paymentMethods != null) {
			for(int i =0; i < this.paymentMethods.length; i++) {
				if (this.paymentMethods[i].equalsIgnoreCase(EditableCreditCardPromotionIntf.MC)) {
					acceptsCard = true;
					break;
				}
			}
		}
		return acceptsCard;
	}
	public boolean getAcceptsDiscovery() {
		boolean acceptsCard = false;
		if (this.paymentMethods != null) {
			for(int i =0; i < this.paymentMethods.length; i++) {
				if (this.paymentMethods[i].equalsIgnoreCase(EditableCreditCardPromotionIntf.DISCOVERY)) {
					acceptsCard = true;
					break;
				}
			}
		}
		return acceptsCard;
	}
	public boolean getAcceptsDiners() {
		boolean acceptsCard = false;
		if (this.paymentMethods != null) {
			for(int i =0; i < this.paymentMethods.length; i++) {
				if (this.paymentMethods[i].equalsIgnoreCase(EditableCreditCardPromotionIntf.DINERS)) {
					acceptsCard = true;
					break;
				}
			}
		}
		return acceptsCard;
	}

	///////////////////////////////////////////////////////////////
	// Product methods
	//////////////////////////////////////////////////////////////

	public String getProductImage1Option() {
		
		return productImage1Option;
	}

	public void setProductImage1Optionn(String newProductImage1Option) {
		this.productImage1Option = newProductImage1Option;
	}

	public String getCurrentTermsAndConditionsText() {
		if (this.currentTermsAndConditionsText == null) {
			this.currentTermsAndConditionsText = "";
		}
		return currentTermsAndConditionsText;		
	}
	public int getCurrentTermsAndConditionsTextLength() {
		int length = 0;
		if (this.currentTermsAndConditionsText != null) {
			length = this.currentTermsAndConditionsText.length();
		}		
		return length;
	}
	public void setCurrentTermsAndConditionsText(
			String newCurrentTermsAndConditionsText) {
			this.currentTermsAndConditionsText = newCurrentTermsAndConditionsText;
	}

	public ImageHandler getCurrentProductImage1() {
		if (this.productImage1Option.equalsIgnoreCase(CampaignEditsHandler.USE_DEFAULT_IMAGE)) {
			return this.getDefaultProductImage1();
		}
		else if (this.productImage1Option.equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
			// set to shim.gif
			if (this.currentProductImage1 == null) {
				this.currentProductImage1 = new ImageHandler();
				this.currentProductImage1.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
			}
			
		}
		else {
			// current image not set up
			if ( this.currentProductImage1 == null || this.currentProductImage1.getImageFileName() == null) {
				// set up from source campaign again.
				EditableImagePromotionIntf tempImage = this.sourceCampaign.getCampaign().getPromotionSet().getProductImage1();
				if (tempImage != null) {
					this.currentProductImage1 = new ImageHandler();
					this.currentProductImage1.setAlternateText(tempImage.getImageAltText());
					this.currentProductImage1.setImageFileName(tempImage.getImagePathString());
					this.currentProductImage1.setLinkURL(tempImage.getImageLinkToURL());
					this.currentProductImage1.setImageContents(tempImage.getImageContents());
					this.currentProductImage1.setImageFileType(tempImage.getImageFileType());
				}
			}
		}

		return currentProductImage1;
	}

	public void setCurrentProductImage1(ImageHandler newCurrentProductImage1) {
		if (newCurrentProductImage1 == null) {
			// set to shim.gif
			this.currentProductImage1 = new ImageHandler();
			this.currentProductImage1.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
		}
		else {
			this.currentProductImage1 = newCurrentProductImage1;
		}
	}
	
	public String getCustomTermsAndConditionsText() {
		return customTermsAndConditionsText;
	}

	public void setCustomTermsAndConditionsText(
			String customTermsAndConditionsText) {
		this.customTermsAndConditionsText = customTermsAndConditionsText;
	}

	public ImageHandler getProductImage1() {
		return productImage1;
	}

	public void setProductImage1(ImageHandler newProductImage1) {
		this.productImage1 = newProductImage1;
	}

	public ImageHandler getDefaultProductImage1() {
		if (defaultProductImage1 == null && defaultCampaign != null) {
			defaultProductImage1 = new ImageHandler();
			EditableImagePromotionIntf sourceImage = defaultCampaign.getCampaign().getPromotionSet().getProductImage1();
			if (sourceImage != null) {
				defaultProductImage1.setAlternateText(sourceImage.getImageAltText());
				defaultProductImage1.setLinkURL(sourceImage.getImageLinkToURL());
				defaultProductImage1.setImageFileName(sourceImage.getImagePathString());
				defaultProductImage1.setImageContents(sourceImage.getImageContents());
				defaultProductImage1.setImageFileType(sourceImage.getImageFileType());
			}
			else {
				defaultProductImage1.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
			}
		}
		else if(defaultProductImage1 == null) {
			defaultProductImage1 = new ImageHandler();
			defaultProductImage1.setImageFileName(CampaignPromotionSetHandler.shimImagePath);			
		}
		return defaultProductImage1;
	}

	public void setDefaultTermsPageImage(ImageHandler defaultProductImage1) {
		this.defaultProductImage1 = defaultProductImage1;
	}

	/**
	 *  Helper method
	 * @param name
	 * @return
	 */
	public boolean isImageNameReserved(String name) {
		boolean nameReserved = false;
		if (name != null) {
			String trimmedName = name.trim();
			File f = new File(trimmedName);
			
			trimmedName = f.getName();
			
			if (trimmedName.equalsIgnoreCase(CampaignEditsHandler.UT_COVER_IMAGE_NAME) ||
					trimmedName.equalsIgnoreCase(CampaignEditsHandler.SW_COVER_IMAGE_NAME)) {
				nameReserved = true;
			}
		}
		else {
			nameReserved = false;
		}
		return nameReserved;
	}
	
	///////////////////////////////////////////////
	///   New Order PopUp Overlay Image Configuration
	///////////////////////////////////////////////
	
	public String getNewOrderPopUpOverlayImageOption() {
		return newOrderPopUpOverlayImageOption;
	}

	public void setNewOrderPopUpOverlayImageOption(String newOrderPopUpOverlayImageOption) {
		this.newOrderPopUpOverlayImageOption = newOrderPopUpOverlayImageOption;
	}

	public ImageHandler getCurrentNewOrderPopUpOverlayImage() {
		if (this.newOrderPopUpOverlayImageOption.equalsIgnoreCase(CampaignEditsHandler.USE_DEFAULT_IMAGE)) {
			return this.getDefaultNewOrderPopUpOverlayImage();
		}
		else if (this.newOrderPopUpOverlayImageOption.equalsIgnoreCase(CampaignEditsHandler.NO_IMAGE)) {
			// set to shim.gif
			if (this.currentNewOrderPopUpOverlayImage == null) {
				this.currentNewOrderPopUpOverlayImage = new ImageHandler();
				this.currentNewOrderPopUpOverlayImage.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
			}
			
		}
		else {
			// current image not set up
			if ( this.currentNewOrderPopUpOverlayImage == null || this.currentNewOrderPopUpOverlayImage.getImageFileName() == null) {
				// set up from source campaign again.
				EditableImagePromotionIntf tempImage = this.sourceCampaign.getCampaign().getPromotionSet().getNewSubscriptionOrderPopUpOverlay();
				if (tempImage != null) {
					this.currentNewOrderPopUpOverlayImage = new ImageHandler();
					this.currentNewOrderPopUpOverlayImage.setAlternateText(tempImage.getImageAltText());
					this.currentNewOrderPopUpOverlayImage.setImageFileName(tempImage.getImagePathString());
					this.currentNewOrderPopUpOverlayImage.setLinkURL(tempImage.getImageLinkToURL());
					this.currentNewOrderPopUpOverlayImage.setImageContents(tempImage.getImageContents());
					this.currentNewOrderPopUpOverlayImage.setImageFileType(tempImage.getImageFileType());
				}
			}
		}

		return currentNewOrderPopUpOverlayImage;
	}

	public void setCurrentNewOrderPopUpOverlayImage(ImageHandler newCurrentOverlayImage) {
		if (newCurrentOverlayImage == null) {
			// set to shim.gif
			this.currentNewOrderPopUpOverlayImage = new ImageHandler();
			this.currentNewOrderPopUpOverlayImage.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
		}
		else {
			this.currentNewOrderPopUpOverlayImage = newCurrentOverlayImage;
		}
	}
	
	public ImageHandler getDefaultNewOrderPopUpOverlayImage() {
		if (defaultNewOrderPopUpOverlayImage == null && defaultCampaign != null) {
			defaultNewOrderPopUpOverlayImage = new ImageHandler();
			EditableImagePromotionIntf sourceImage = defaultCampaign.getCampaign().getPromotionSet().getNewSubscriptionOrderPopUpOverlay();
			if (sourceImage != null) {
				defaultNewOrderPopUpOverlayImage.setAlternateText(sourceImage.getImageAltText());
				defaultNewOrderPopUpOverlayImage.setLinkURL(sourceImage.getImageLinkToURL());
				defaultNewOrderPopUpOverlayImage.setImageFileName(sourceImage.getImagePathString());
				defaultNewOrderPopUpOverlayImage.setImageContents(sourceImage.getImageContents());
				defaultNewOrderPopUpOverlayImage.setImageFileType(sourceImage.getImageFileType());
			}
			else {
				defaultNewOrderPopUpOverlayImage.setImageFileName(CampaignPromotionSetHandler.shimImagePath);
			}
		}
		else if(defaultNewOrderPopUpOverlayImage == null) {
			defaultNewOrderPopUpOverlayImage = new ImageHandler();
			defaultNewOrderPopUpOverlayImage.setImageFileName(CampaignPromotionSetHandler.shimImagePath);			
		}
		return defaultNewOrderPopUpOverlayImage;
	}

	public void setDefaultNewOrderPopUpOverlayImage(ImageHandler defaultNewOrderPopUpOverlayImage) {
		this.defaultNewOrderPopUpOverlayImage = defaultNewOrderPopUpOverlayImage;
	}
	
	public ImageHandler getNewOrderPopUpOverlayCustomImage() {
		return newOrderPopUpOverlayCustomImage;
	}

	public void setNewOrderPopUpOverlayCustomImage(ImageHandler newOrderOverlayCustomImage) {
		this.newOrderPopUpOverlayCustomImage = newOrderOverlayCustomImage;
	}	
    // end new order path overlay image

	
	///////////////////////////////////////////////////////////////
	// One page  order page methods
	//////////////////////////////////////////////////////////////

	public String getCurrentOnePageForceEZPayTermsText() {
		if (currentOnePageForceEZPayTermsText == null) {
			this.currentOnePageForceEZPayTermsText = "";
		}
		return currentOnePageForceEZPayTermsText;
	}

	public String getDefaultOnePageForceEZPayTermsText() {
		String  defaultText = null;
		try {
			// this is only overridden at the pub level 
			PromotionSet defaultSet = PromotionManager.getInstance().getPromotionsForOffer(this.sourceCampaign.getPubcode(), PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE); 
			
			if (defaultSet != null && defaultSet.getPromoOnePageForceEZPayCustomTermsText() != null) {
				String theText = defaultSet.getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
				if (theText.length() > 0) {
					defaultText = defaultSet.getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
				}
				else {
					defaultText ="DEFAULT VALUE IS BLANK";
				}
			}
			//if (defaultText == null) {
			//	defaultSet = this.sourceCampaign.getCampaign().getPromotionSet().getDefaultBrandingPubPromotionSet();
			//	String theText = defaultSet.getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
			//	if (theText.length() > 0) {
			//		defaultText = defaultSet.getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
			//	}
			//	else {
			//		defaultText ="DEFAULT VALUE IS BLANK";
			//	}
			//}
			
		}
		catch (NullPointerException ne) {
			// ignore these
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if (defaultText == null) {
			defaultText ="DEFAULT VALUE IS BLANK";
		}
		return defaultText;
	}

	public String getDefaultOnePagePromoText1() {
		String  defaultText = null;
		try {
			// this is only overridden at the pub level 
			PromotionSet defaultSet = PromotionManager.getInstance().getPromotionsForOffer(this.sourceCampaign.getPubcode(), PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE); 
			
			if (defaultSet != null && defaultSet.getPromoOnePageText1() != null) {
				String theText = defaultSet.getPromoOnePageText1().getPromotionalHTML();
				if (theText.length() > 0) {
					defaultText = theText;
				}
				else {
					defaultText ="DEFAULT VALUE IS BLANK";
				}
			}
			//if (defaultText == null) {
			//	defaultSet = this.sourceCampaign.getCampaign().getPromotionSet().getDefaultBrandingPubPromotionSet();
			//	String theText = defaultSet.getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
			//	if (theText.length() > 0) {
			//		defaultText = defaultSet.getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
			//	}
			//	else {
			//		defaultText ="DEFAULT VALUE IS BLANK";
			//	}
			//}
			
		}
		catch (NullPointerException ne) {
			// ignore these
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if (defaultText == null) {
			defaultText ="DEFAULT VALUE IS BLANK";
		}
		return defaultText;
	}
	
	public void setCurrentOnePageForceEZPayTermsText(String currentOnePageForceEZPayTermsText) {
		this.currentOnePageForceEZPayTermsText = currentOnePageForceEZPayTermsText;
	}
	
	public int getCurrentOnePageForceEZPayTermsLength() {
		int length = 0;
		if (this.currentOnePageForceEZPayTermsText != null) {
			length = this.currentOnePageForceEZPayTermsText.length();
		}
		return length;
	}

	public String getCustomOnePageForceEZPayTerms() {
		return customOnePageForceEZPayTermsText;
	}

	public void setCustomOnePageForceEZPayTerms(
			String customOnePageForceEZPayTerms) {
		this.customOnePageForceEZPayTermsText = customOnePageForceEZPayTerms;
	}

	//
	public String getCurrentOnePagePromoText1() {
		if (currentOnePagePromoText1 == null) {
			this.currentOnePagePromoText1 = "";
		}
		return currentOnePagePromoText1;
	}

	public void setCurrentOnePagePromoText1(String currentOnePagePromoText1) {
		this.currentOnePagePromoText1 = currentOnePagePromoText1;
	}
	
	public int getCurrentOnePagePromoText1Length() {
		int length = 0;
		if (this.currentOnePagePromoText1 != null) {
			length = this.currentOnePagePromoText1.length();
		}
		return length;
	}
	
	public String getCurrentOnePagePromoText1Raw() {
		if (currentOnePagePromoText1Raw == null) {
			this.currentOnePagePromoText1Raw = "";
		}
		return currentOnePagePromoText1Raw;
	}

	public void setCurrentOnePagePromoText1Raw(String currentOnePagePromoText1Raw) {
		this.currentOnePagePromoText1Raw = currentOnePagePromoText1Raw;
	}
	public String getCustomOnePagePromoText1() {
		return customOnePagePromoText1;
	}

	public void setCustomOnePagePromoText1(
			String customOnePagePromoText1) {
		this.customOnePagePromoText1 = customOnePagePromoText1;
	}

	//
	public String getCurrentOnePageDisclaimerText() {
		if (currentOnePageDisclaimerText == null) {
			this.currentOnePageDisclaimerText = "";
		}
		return currentOnePageDisclaimerText;
	}

	public void setCurrentOnePageDisclaimerText(String currentOnePageDisclaimerText) {
		this.currentOnePageDisclaimerText = currentOnePageDisclaimerText;
	}
	
	public int getCurrentOnePageDisclaimerTextLength() {
		int length = 0;
		if (this.currentOnePageDisclaimerText != null) {
			length = this.currentOnePageDisclaimerText.length();
		}
		return length;
	}

	public String getCustomOnePageDisclaimerText() {
		return customOnePageDisclaimerText;
	}

	public void setCustomOnePageDisclaimerText(
			String customOnePageDisclaimerText) {
		this.customOnePageDisclaimerText = customOnePageDisclaimerText;
	}
	
	
	// End One page order methods
	

	// FORCE EZPAY methods
	public boolean isShowForceEZPAYRateCodeResults() {
		boolean showResults = false;
		if (this.forceEZPAYRatelastRateLookedUp != null) {
			showResults = true;
		}
		return showResults;
	}

	public String getForceEZPAYRateCodeToLookUp() {
		return forceEZPAYRateCodeToLookUp;
	}

	public void setForceEZPAYRateCodeToLookUp(String rateCodeToLookUp) {
		if (rateCodeToLookUp != null) {
			rateCodeToLookUp = rateCodeToLookUp.toUpperCase();
		}
		this.forceEZPAYRateCodeToLookUp = rateCodeToLookUp;
	}

	public RelRtCodeIntf getLastRateLookedUp() {
		return forceEZPAYRatelastRateLookedUp;
	}

	public void setLastRateLookedUp(RelRtCodeIntf lastRateLookedUp) {
		this.forceEZPAYRatelastRateLookedUp = lastRateLookedUp;
	}

	public String getForceEZPAYOverrideRateOption() {
		return forceEZPAYOverrideRateOption;
	}

	public void setForceEZPAYOverrideRateOption(String overrideRateOption) {
		this.forceEZPAYOverrideRateOption = overrideRateOption;
	}
	
	public boolean isShowForceEZPayUseRateButton () {
		if (this.forceEZPAYRateCodeToLookUp != null && this.forceEZPAYRateCodeToLookUp.length() > 0 && this.forceEZPAYRatelastRateLookedUp != null) {
			return true;
		}
		return false;
	}
	
	public String getForceEZPAYUseButtonLabel () {
		if (this.forceEZPAYRatelastRateLookedUp != null) {
			return "Use Rate " + this.forceEZPAYRatelastRateLookedUp.getPiaRateCode() + " As Override";
		}
		return "";
	}

	public void setOverrideDefaultAllowOfferOverride(
			boolean overrideDefaultAllowOfferOverride) {
		this.overrideDefaultAllowOfferOverride = overrideDefaultAllowOfferOverride;
	}

	public boolean isOverrideDefaultAllowOfferOverride() {
		return overrideDefaultAllowOfferOverride;
	}

	public void setCustomAllowOfferOverrideSetting(
			boolean customAllowOfferOverrideSetting) {
		this.customAllowOfferOverrideSetting = customAllowOfferOverrideSetting;
	}

	public boolean isCustomAllowOfferOverrideSetting() {
		return customAllowOfferOverrideSetting;
	}

	public String getDefaultAllowOverrideOfferDisplayString() {
		String defaultSetting = "'Enter Offer Code' Overrides are NOT Allowed by Default";
		if (defaultAllowOverrideSetting) {
			defaultSetting = "'Enter Offer Code' Overrides ARE Allowed by Default";
		}
		return defaultSetting;
	}
	public boolean isDefaultAllowOverrideSetting() {
		return defaultAllowOverrideSetting;
	}

}
 