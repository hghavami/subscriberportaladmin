package com.gannett.usat.dataHandlers.campaigns;

import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionIntf;

public class PromotionDataHandler {
	private EditablePromotionIntf promotionRow = null;

	public EditablePromotionIntf getPromotionRow() {
		return promotionRow;
	}

	public void setPromotionRow(EditablePromotionIntf promotionRow) {
		this.promotionRow = promotionRow;
	}
	
	public String getType() {
		if (this.promotionRow != null) {
			return this.promotionRow.getType();
		}
		return "";
	}

	public String getName() {
		if (this.promotionRow != null) {
			return this.promotionRow.getName();
		}
		return "";
	}
	
	public String getAltName() {
		if (this.promotionRow != null) {
			return this.promotionRow.getAltName();
		}
		return "";
	}

	public String getFulFillText() {
		if (this.promotionRow != null) {
			return this.promotionRow.getFulfillText();
		}
		return "";
	}

	public String getFulFillUrl() {
		if (this.promotionRow != null) {
			return this.promotionRow.getFulfillUrl();
		}
		return "";
	}
	
}
