package com.gannett.usat.dataHandlers.campaigns;

import java.util.Collection;

//import com.usatoday.UsatException;





/**
 * 
 * @author aeast
 *
 * This class is used as the handler to access the set of campaigns that the user is working
 * with currently. It will usually contain a single group of campaigns, but there is no
 * restriction that campaigns must be in the same group to be modified the same.
 * 
 * Changes made through the site will be applied to every campaign in this collection.
 */
public class EditCampaignVanityHandler {
	private CampaignHandler selectedCampaign = null;
	private Collection<CampaignHandler> campaigns = null;

	
//	private UsatCampaignIntf campaign = null;
	
//	private String campaignGroupID = null;
//	private String campaignPubID = null;
	
	/** 
	 * @managed-bean true
	 */
	/*
	protected EditCampaignVanityHandler getEdittingCampaignsVanityHandler() {
		if (edittingCampaignsVanityHandler == null) {
			edittingCampaignsVanityHandler = (EditCampaignVanityHandler) getFacesContext()
					.getApplication().createValueBinding(
							"#{edittingCampaignsVanityHandler}").getValue(
							getFacesContext());
		}
		return edittingCampaignsVanityHandler;
	}


	*/
	
	
	
	
//	
	public EditCampaignVanityHandler() {
		super();
	}
	
	// following is used for when user appears in a collection and can be selected from a 
	// row selection
//	private boolean campaignSelected = false; 
	
	

	//private Collection<CampaignHandler> campaigns = null;

	private boolean edittingAGroup = false;
	
	
	public Collection<CampaignHandler> getCampaigns() {	
		
		if (this.campaigns != null && this.campaigns.size() > 0) {
			return campaigns;
		}
		return campaigns;
		
	}

	public void setCampaigns(Collection<CampaignHandler> campaigns) {
		this.campaigns = campaigns;
	}

	public boolean getEdittingAGroup() {
		return edittingAGroup;
	}

	public void setEdittingAGroup(boolean edittingAGroup) {
		this.edittingAGroup = edittingAGroup;
	}

	public String getDataTableHeaderString() {
		String text = "";
		if (this.edittingAGroup) {
			if (this.campaigns != null && this.campaigns.size() > 0) {
				text = "You are editting " + this.campaigns.size() + " campaigns in the group named: " + this.campaigns.iterator().next().getCampaignGroup();
			}
			else {
				text = "No Campaigns in the group. Please select at least one campaign.";
			}
		}
		else {
			// not a group edit
			if (this.campaigns != null && this.campaigns.size() > 0) {
				text = "You are editting " + this.campaigns.size() + " campaigns.";
			}
			else {
				text = "No Campaigns selected for edit. Please select at least one campaign.";
			}
		}
		return text;
	}
}
