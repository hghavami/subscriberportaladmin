package com.gannett.usat.dataHandlers.campaigns;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import com.gannett.usatoday.adminportal.campaigns.UsatCampaignBO;

public class CampaignGroupNameSuggestionsHandler extends AbstractMap {

	private String pubCode = null;
	
	public CampaignGroupNameSuggestionsHandler() {
	}

	@Override 
	public Set entrySet() {
		
		return null;
	}

	@Override
	public Object get(Object key) {
		
		String filter = null;
		Collection<String> suggestions = null;
		
		try {
			filter = (String) key;
		
			if (this.pubCode == null) {
				// return a collection of suggestions for the specified key (filter);
				suggestions = UsatCampaignBO.fetchAllCampaignGroupNames(filter);				
			}
			else {
				// return a collection of suggestions for the specified key (filter) and pub
				suggestions = UsatCampaignBO.fetchAllCampaignGroupNamesForPub(filter, this.pubCode);
			}
			
		}
		catch (Exception e) {
			System.out.println("CampaignGroupNameSuggestionsHandler(): Failed to generate suggestions. " + e.getMessage());
			e.printStackTrace();
		}
		
		if (suggestions == null) {
			suggestions = new ArrayList<String>();
		}
		
		return suggestions;
	}

	public String getPubCode() {
		return pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}

}
