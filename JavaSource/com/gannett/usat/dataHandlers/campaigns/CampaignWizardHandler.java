package com.gannett.usat.dataHandlers.campaigns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.faces.model.SelectItem;

import com.gannett.usatoday.adminportal.campaigns.intf.KeyCodeIntf;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignPublisherFailure;
import com.gannett.usatoday.adminportal.products.USATProductBO;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.util.constants.UsaTodayConstants;

public class CampaignWizardHandler {

	// used on step 1 
	private USATProductBO product = null;
	private String publication = UsaTodayConstants.UT_PUBCODE;
	private boolean isUTCampaignBrand = true;

	
	// used on step 2
	private Collection<KeyCodeIntf> availableKeyCodes = null;
	private Collection<KeyCodeIntf> selectedKeyCodes = null;
	private String[] tempSelectedKeyCodesStrings = null;
	private String[] tempRemoveSelectedKeyCodeStrings = null;
	private String groupName = "";
	private boolean includeInGroup = false;
	private String keyCodeFilter = null;
	
	// used on step 3 - no attributes needed
	
	// used on step 4
	private boolean autoPublish = false;
	private boolean editDetails = true;
	private boolean publishingFailed = false;
	private Collection<CampaignPublisherFailure> publishingFailures = null; 
	
	// collection of the new campaign objects
	private ArrayList<CampaignHandler> newCampaigns = null;
	
	
	public CampaignWizardHandler() {
		super();
		
		this.newCampaigns = new ArrayList<CampaignHandler>();
		this.selectedKeyCodes = new ArrayList<KeyCodeIntf>();
	}


	/**
	 * 
	 * @return The collection of newly configured campaigns
	 */
	public ArrayList<CampaignHandler> getNewCampaigns() {
		return newCampaigns;
	}


	public void setNewCampaigns(ArrayList<CampaignHandler> newCampaigns) {
		this.newCampaigns = newCampaigns;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getPublication() {
		return publication;
	}


	public void setPublication(String publication) {

		this.publication = publication;
		
		if (this.selectedKeyCodes != null) {
			this.selectedKeyCodes.clear();
		}
		
		
	}
	
	public boolean isUTCampaign() {
		return isUTCampaignBrand;
	}


	public void setUTCampaign(boolean isUTCampaign) {
		this.isUTCampaignBrand = isUTCampaign;
	}
	
	public boolean isIncludeInGroup() {
		return includeInGroup;
	}

	public void setIncludeInGroup(boolean includeInGroup) {
		this.includeInGroup = includeInGroup;
	}

	public Collection<SelectItem> getKeyCodesForSelectedPub() {
		ArrayList<SelectItem> items = new ArrayList<SelectItem>();

		Collection<KeyCodeIntf> keycodes = this.availableKeyCodes;
		if (keycodes instanceof ArrayList) {
			ArrayList<KeyCodeIntf> kCodes = (ArrayList<KeyCodeIntf>)keycodes;
			Collections.sort(kCodes);
		}
		
		boolean applyFilter = false;
		String filterString = null;
		if (this.keyCodeFilter != null && this.keyCodeFilter.trim().length() > 0) {
			this.keyCodeFilter = this.keyCodeFilter.trim();
			filterString = this.keyCodeFilter.toUpperCase();
			applyFilter = true;
		}
		for(KeyCodeIntf keyCode : keycodes) {
			boolean addItem = false;
			
			if (this.product.getBrandingPubCode().equalsIgnoreCase(keyCode.getPubCode())) {
				// if working with a default product check that it isnt the default keycode
				if (this.product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)){
					if (PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE.equalsIgnoreCase(keyCode.getKeyCode())) {
						continue;
					}
				}
				if (this.product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)){
					if (PromotionIntf.DEFAULT_CAMPAIGN_KEYCODE.equalsIgnoreCase(keyCode.getKeyCode())) {
						continue;
					}
				}
			}
			if (applyFilter) {
				if(keyCode.getKeyCode().startsWith(filterString)){
					addItem = true;
				}
			}
			else {
				addItem = true;
			}
			if (addItem) {
		        SelectItem si = new javax.faces.model.SelectItem();
		        si.setDescription(keyCode.getKeyCode() + "-" + keyCode.getOfferDescription());
		        si.setLabel(keyCode.getKeyCode());
		        si.setValue(keyCode.getKeyCode());
		        items.add(si);
			}
		}
		
		return items;
	}

	public Collection<SelectItem> getSelectedKeyCodesForNewCampaigns() {
		Collection<SelectItem> items = new ArrayList<SelectItem>();

		if (this.selectedKeyCodes == null) {
			return items;
		}
		
		if (this.selectedKeyCodes instanceof ArrayList) {
			ArrayList<KeyCodeIntf> kCodes = (ArrayList<KeyCodeIntf>)this.selectedKeyCodes;
			Collections.sort(kCodes);
		}		
		
		for(KeyCodeIntf keyCode : this.selectedKeyCodes) {
	        SelectItem si = new javax.faces.model.SelectItem();
	        si.setDescription(keyCode.getKeyCode() + "-" + keyCode.getOfferDescription());
	        si.setLabel(keyCode.getKeyCode());
	        si.setValue(keyCode.getKeyCode());
	        items.add(si);
		}
		return items;
	}


	public Collection<KeyCodeIntf> getAvailableKeyCodes() {
		return availableKeyCodes;
	}


	public void setAvailableKeyCodes(Collection<KeyCodeIntf> availableKeyCodes) {
		this.availableKeyCodes = availableKeyCodes;
	}


	public Collection<KeyCodeIntf> getSelectedKeyCodes() {
		return selectedKeyCodes;
	}


	public void setSelectedKeyCodes(Collection<SelectItem> selectedKeyCodes) {
		
	}
 
	public String[] getTempSelectedKeyCodesStrings() {
		return tempSelectedKeyCodesStrings;
	}


	public void setTempSelectedKeyCodesStrings(String[] tempSelectedKeyCodesStrings) {
		this.tempSelectedKeyCodesStrings = tempSelectedKeyCodesStrings;
		
	}


	public String[] getTempRemoveSelectedKeyCodeStrings() {
		return tempRemoveSelectedKeyCodeStrings;
	}


	public void setTempRemoveSelectedKeyCodeStrings(
			String[] tempRemoveSelectedKeyCodeStrings) {
		this.tempRemoveSelectedKeyCodeStrings = tempRemoveSelectedKeyCodeStrings;
	}


	public String getKeyCodeFilter() {
		return keyCodeFilter;
	}


	public void setKeyCodeFilter(String keyCodeFilter) {
		this.keyCodeFilter = keyCodeFilter;
	}


	public boolean isAutoPublish() {
		return autoPublish;
	}


	public boolean isEditDetails() {
		return editDetails;
	}


	public void setAutoPublish(boolean autoPublish) {
		this.autoPublish = autoPublish;
	}


	public void setEditDetails(boolean editDetails) {
		this.editDetails = editDetails;
	}


	public boolean isPublishingFailed() {
		return publishingFailed;
	}


	public Collection<CampaignPublisherFailure> getPublishingFailures() {
		return publishingFailures;
	}


	public void setPublishingFailed(boolean publishingFailed) {
		this.publishingFailed = publishingFailed;
	}


	public void setPublishingFailures(
			Collection<CampaignPublisherFailure> publishingFailures) {
		this.publishingFailures = publishingFailures;
	}

	/**
	 * clears out the session object for another use
	 *
	 */
	public void reset() {
		this.newCampaigns.clear();
		this.selectedKeyCodes.clear();
		if (this.publishingFailures != null) {
			this.publishingFailures.clear();
		}
		this.publishingFailed = false;
		this.autoPublish = false;
		this.isUTCampaignBrand = true;
		this.editDetails = true;
		this.includeInGroup = false;
		this.groupName = null;
		this.keyCodeFilter = null;
		this.publication = UsaTodayConstants.UT_PUBCODE;
		this.tempRemoveSelectedKeyCodeStrings = null;
		this.tempSelectedKeyCodesStrings = null;
	}

	public USATProductBO getProduct() {
		return product;
	}


	public void setProduct(USATProductBO product) {
		this.product = product;
		try {
			if (this.product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.SW_PUBCODE)) {
				this.isUTCampaignBrand = false;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			this.isUTCampaignBrand = true;
		}
	}
}
