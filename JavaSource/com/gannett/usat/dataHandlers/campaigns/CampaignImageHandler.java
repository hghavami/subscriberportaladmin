package com.gannett.usat.dataHandlers.campaigns;

import com.gannett.usat.dataHandlers.ImageHandler;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditableImagePromotionIntf;

public class CampaignImageHandler extends ImageHandler {

	public static final String USE_DEFAULT_IMAGE = "default";
	public static final String NO_IMAGE = "none";
	public static final String USE_CUSTOM_IMAGE = "custom";
	
	private String imageOption = CampaignImageHandler.USE_DEFAULT_IMAGE;
	
	
	public CampaignImageHandler() {
		super();
	}
	
	public CampaignImageHandler(EditableImagePromotionIntf source) {
		
	}
	public String getImageOption() {
		return imageOption;
	}
	public void setImageOption(String imageOpt) {
		if (imageOpt == null) {
			this.imageOption = CampaignImageHandler.USE_DEFAULT_IMAGE;
		}
		else {
			this.imageOption = imageOpt;
		}
	}
	public boolean isNoImage() {
		if (this.imageOption.equalsIgnoreCase(CampaignImageHandler.NO_IMAGE)) {
			return true;
		}
		return false;
	}
	public void setNoImage(boolean isNoImage) {
		; // ignore
	}
	public boolean isCustomImage() {
		if (this.imageOption.equalsIgnoreCase(CampaignImageHandler.USE_CUSTOM_IMAGE)) {
			return true;
		}
		return false;
	}
	public void setCustomImage(boolean isCustomImage) {
		; // ignore
	}
	public boolean isDefaultImage() {
		if (this.imageOption.equalsIgnoreCase(CampaignImageHandler.USE_DEFAULT_IMAGE)) {
			return true;
		}
		return false;
	}
	public void setDefaultImage(boolean isDefaultImage) {
		; // ignore
	}
	
}
