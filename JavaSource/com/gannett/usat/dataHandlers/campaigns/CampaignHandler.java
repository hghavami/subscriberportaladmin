/*
 * Created on Aug 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.gannett.usat.dataHandlers.campaigns;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;

import com.gannett.usatoday.adminportal.campaigns.intf.PublishingServerIntf;
import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.utils.CampaignPublisherFailure;
import com.gannett.usatoday.adminportal.campaigns.utils.PublishingServerCache;

/** 
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CampaignHandler {

	private UsatCampaignIntf campaign = null;
	
	private boolean campaignSelected = false;
    private String campaignErrorMessage = null;
    

	/**
	 * 
	 */
	public CampaignHandler() {
		super();
	}

	public boolean getIsGiftCampaign() {
		//if (this.campaign != null) {
			//return this.campaign.
		//}
		return false;
	}

	public void setIsGiftCampaign(boolean isGiftCampaign) {
		// TODO: Add this to campaign object
		;
	}

	/**
	 * @return Returns the campaignID.
	 */
	public int getCampaignID() {
		if (this.campaign != null) {
			return this.campaign.getPrimaryKey();
		}
		return -1;
	}
	/**
	 * @param campaignID The campaignID to set.
	 */
	public void setCampaignID(int campaignID) {
		; // no op can't set id through ui
	}
	/**
	 * @return Returns the campaignName.
	 */
	public String getCampaignName() {
		if (this.campaign != null) {
			return this.campaign.getCampaignName();
		}
		return "";
	}
	/**
	 * @param campaignName The campaignName to set.
	 */
	public void setCampaignName(String campaignName) {
		if (this.campaign != null) {
			try {
				this.campaign.setCampaignName(campaignName);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * @return Returns the campaignState.
	 */
	public int getCampaignState() {
		if (this.campaign != null) {
			return this.campaign.getCampaignState();
		}
		return 0;
	}
	/**
	 * @param campaignState The campaignState to set.
	 */
	public void setCampaignState(int campaignState) {
		if (this.campaign != null) {
			try {
				this.campaign.setCampaignState(campaignState);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * @return Returns the campaignType.
	 */
	public int getCampaignType() {
		if (this.campaign != null) {
			return this.campaign.getType();
		}
		return 0;
	}
	
	public String getCampaignTypeAsString () {
		if (this.campaign != null) {
			return String.valueOf(this.campaign.getType());
		}
		
		return "0";
	}
	
	public String getCampaignTypeDescription() {
		String description = "";
		if (this.campaign != null) {
			switch (this.campaign.getType()) {
			case UsatCampaignIntf.BASIC_VANITY_URL:
				description = "Basic Vanity";
				break;
			case UsatCampaignIntf.BASIC_SPLASH:
				description = "Splash Page";
				break;
			default:
				break;
			}
		}
		return description;
	}
	
	public void setCampaignTypeAsString (String type) {
		try {
			if (this.campaign != null) {
				try {
					this.campaign.setType(Integer.valueOf(type));
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		catch (Exception e) {
			;
		}
	}
	
	public String getCampaignLandingPageDescription() {
		// conver to a proxy lookup or something
		if (this.campaign != null) {
			if (this.campaign.getRedirectToPage().equalsIgnoreCase("TERMS")) {
				return "Order Page";
			}
			else { 
				return "Subscribe Page";
			}
		}
		return "";
	}
	/**
	 * @param campaignType The campaignType to set.
	 */
	public void setCampaignType(int campaignType) {
		//this.campaignType = campaignType;
	}
	/**
	 * @return Returns the createdBy.
	 */
	public String getCreatedBy() {
		if (this.campaign != null) {
			return this.campaign.getCreatedBy();
		}
		return "unknown";
	}
	/**
	 * @param createdBy The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		//this.createdBy = createdBy;
	}
	/**
	 * @return Returns the createdTime.
	 */
	public Date getCreatedTime() {
		if (this.campaign != null) {
			return this.campaign.getCreatedTime().toDate();
		}		
		return null;
	}
	/**
	 * @param createdTime The createdTime to set.
	 */
	public void setCreatedTime(Timestamp createdTime) {
		//this.createdTime = createdTime;
	}
	/**
	 * @return Returns the keycode.
	 */
	public String getKeycode() {
		if (this.campaign != null) {
			return this.campaign.getKeyCode();
		}		
		return "";
	}
	/**
	 * @param keycode The keycode to set.
	 */
	public void setKeycode(String keycode) {
		try {
			this.campaign.setKeyCode(keycode);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	/**
	 * @return Returns the pubcode.
	 */
	public String getPubcode() {
		if (this.campaign != null) {
			return this.campaign.getPubCode();
		}		
		return "";
	}
	/**
	 * @param pubcode The pubcode to set.
	 */
	public void setPubcode(String pubcode) {
		//this.pubcode = pubcode;
	}
	/**
	 * @return Returns the redirectToPage.
	 */
	public String getRedirectToPage() {
		if (this.campaign != null) {
			return this.campaign.getRedirectToPage();
		}		
		return "";
	}
	/**
	 * @param redirectToPage The redirectToPage to set.
	 */
	public void setRedirectToPage(String redirectToPage) {
		if (redirectToPage != null) {
			try {
				this.campaign.setRedirectToPage(redirectToPage);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * @return Returns the redirectURL.
	 */
	public String getRedirectURL() {
		if (this.campaign != null) {
			return this.campaign.getRedirectURL();
		}		
		return "";
	}
	/**
	 * @param redirectURL The redirectURL to set.
	 */
	public void setRedirectURL(String redirectURL) {
		//this.redirectURL = redirectURL;
	}
	/**
	 * @return Returns the updatedBy.
	 */
	public String getUpdatedBy() {
		if (this.campaign != null) {
			return this.campaign.getUpdatedBy();
		}		
		return "";
	}
	/**
	 * @param updatedBy The updatedBy to set.
	 */
	public void setUpdatedBy(String updatedBy) {
		//this.updatedBy = updatedBy;
	}
	/**
	 * @return Returns the updatedTime.
	 */
	public Date getUpdatedTime() {
		if (this.campaign != null) {
			return this.campaign.getUpdatedTime().toDate();
		}		
		return null;
	}
	/**
	 * @param updatedTime The updatedTime to set.
	 */
	public void setUpdatedTime(Timestamp updatedTime) {
		//this.updatedTime = updatedTime;
	}
	/**
	 * @return Returns the vanityURL.
	 */
	public String getVanityURL() {
		if (this.campaign != null) {
			return this.campaign.getVanityURL();
		}		
		return "";
	}
	/**
	 * @param vanityURL The vanityURL to set.
	 */
	public void setVanityURL(String vanityURL) {
		try {
			this.campaign.setVanityURL(vanityURL);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getCampaignGroup() {
		if (this.campaign != null) {
			return this.campaign.getGroupName();
		}		
		return "";
	}

	public void setCampaignGroup(String campaignGroup) {
		try {
			this.campaign.setCampaignGroup(campaignGroup);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
			
	}

	public String getCampaignStatusString() {
		if (this.campaign != null) {
			return this.campaign.getCampaignStateDescription();
		}
		return "unknown";
	}

	public void setCampaignStatusString(String campaignStatusString) {
		//this.campaignStatusString = campaignStatusString;
	}
	
	public String getTargetAudience() {
		if (this.campaign != null) {

			if (this.campaign.getType() < 2 ) {
				//if (this.isGiftCampaign) {
				//	return "Gift Sub";
				//}
				//else {
					return "New Sub";
				//}
			}
			else {
				return "Renewal";
			}
		}
		return "New Sub";
	}

	public UsatCampaignIntf getCampaign() {
		
		return campaign;
	}

	public void setCampaign(UsatCampaignIntf campaign) {
		this.campaign = campaign;
	}

	public boolean isCampaignSelected() {
		return campaignSelected;
	}

	public void setCampaignSelected(boolean campaignSelected) {
		this.campaignSelected = campaignSelected;
	}

	public String getCampaignErrorMessage() {
		return campaignErrorMessage;
	}

	public void setCampaignErrorMessage(String campaignErrorMessage) {
		this.campaignErrorMessage = campaignErrorMessage;
	}
	
	public String getFullVanityURL() {
		String vURL = "";
		if (this.campaign != null) {
			PublishingServerCache psCache = new PublishingServerCache();
			
			PublishingServerIntf ps = psCache.getAProductionServer();
			if (this.campaign.getVanityURL() != null && this.campaign.getVanityURL().length() > 0) {
				
				if (ps != null) {
					vURL = ps.getPublicHostURL() + "/" + this.campaign.getVanityURL();
				}
				else {
					vURL = "http://service.usatoday.com/" + this.campaign.getVanityURL();					
				}
			}
			else {
				// no vanity so just use regular link

				if (ps != null) {
					if (this.campaign.getRedirectToPage().equalsIgnoreCase("WELCOME")) {
						vURL = ps.getRedirectURL() + "?pub=" + this.campaign.getPubCode() + "&keycode=" + this.campaign.getKeyCode();					
					}
					else {
						vURL = ps.getRedirectURL1() + "?pub=" + this.campaign.getPubCode() + "&keycode=" + this.campaign.getKeyCode();					
					}
				}
				else {
					String pagePart = "/subscriptions/order/checkout.faces";
					vURL = "https://service.ustatoday.com" + pagePart + "?pub=" + this.campaign.getPubCode() + "&keycode=" + this.campaign.getKeyCode();
				}
			}
		}
		return vURL;
	}
 
	public String getFullVanityTestURL() {
		String vURL = "";
		if (this.campaign != null) {
			PublishingServerCache psCache = new PublishingServerCache();
			
			PublishingServerIntf ps = psCache.getATestServer();
			if (this.campaign.getVanityURL() != null && this.campaign.getVanityURL().length() > 0) {
				
				if (ps != null) {
					vURL = ps.getPublicHostURL() + "/" + this.campaign.getVanityURL();
				}
				else {
					vURL = "Unable to get test server";					
				}
			}
			else {
				// no vanity so just use regular link

				if (ps != null) {
					if (this.campaign.getRedirectToPage().equalsIgnoreCase("WELCOME")) {
						vURL = ps.getRedirectURL() + "?pub=" + this.campaign.getPubCode() + "&keycode=" + this.campaign.getKeyCode();					
					}
					else {
						vURL = ps.getRedirectURL1() + "?pub=" + this.campaign.getPubCode() + "&keycode=" + this.campaign.getKeyCode();					
					}
				}
				else {
					vURL = "Unable to get test server";					
				}
			}
		}
		return vURL;
	}
	
	public CampaignPromotionSetHandler getPromotionSetHandler() {
		CampaignPromotionSetHandler sh = new CampaignPromotionSetHandler();
		if (this.campaign != null) {
			sh.setSource(this.campaign.getPromotionSet());
		}
		return sh;
	}
	
	public String getFullVanityURLText() {
		String text = "NO VANITY";
		if (this.campaign != null) { 
			String vanity = this.campaign.getVanityURL();
			if (vanity != null && vanity.length() > 0) {
				text = vanity;
			}
		}
		return text;
	}
	
	public Collection<CampaignPublisherFailure> getPublishFailures() {
		Collection<CampaignPublisherFailure> failures = null;
		if (this.campaign != null) {
			failures = this.campaign.getPublishFailures();
		}
		return failures;
	}
}
