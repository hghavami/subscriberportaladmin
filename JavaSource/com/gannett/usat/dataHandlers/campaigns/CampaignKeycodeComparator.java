package com.gannett.usat.dataHandlers.campaigns;

import java.util.Comparator;

public class CampaignKeycodeComparator implements Comparator {

	public int compare(Object o1, Object o2) {
		String campaignKeycode1 = ( (CampaignHandler) o1 ).getKeycode();

		String campaignKeycode2 = ( (CampaignHandler) o2 ).getKeycode();

		//	uses compareTo method of String class to compare names of the employee

		return campaignKeycode1.compareTo(campaignKeycode2);

	}

}
