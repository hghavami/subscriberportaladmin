package com.gannett.usat.dataHandlers.campaigns;

import java.util.Comparator;

public class CampaignNameComparator implements Comparator {

	public int compare(Object o1, Object o2) {
		String campaignName1 = ( (CampaignHandler) o1 ).getCampaignName();

		String campaignName2 = ( (CampaignHandler) o2 ).getCampaignName();

		//	uses compareTo method of String class to compare names of the employee

		return campaignName1.compareTo(campaignName2);

	}

}
