package com.gannett.usat.dataHandlers.campaigns;

import java.util.Collection;

//import com.usatoday.UsatException;





/**
 * 
 * @author aeast
 *
 * This class is used as the handler to access the set of campaigns that the user is working
 * with currently. It will usually contain a single group of campaigns, but there is no
 * restriction that campaigns must be in the same group to be modified the same.
 * 
 * Changes made through the site will be applied to every campaign in this collection.
 */
public class EditCampaignDetailsHandler {
	private Collection<CampaignHandler> campaigns = null;
	private boolean edittingAGroup = false;
	
	
	public EditCampaignDetailsHandler() {
		super();
	}
	
	public Collection<CampaignHandler> getCampaigns() {	
		
		if (this.campaigns != null && this.campaigns.size() > 0) {
			return campaigns;
		}
		return campaigns;
		
	}

	public void setCampaigns(Collection<CampaignHandler> campaigns) {
		this.campaigns = campaigns;
	}

	public boolean getEdittingAGroup() {
		return edittingAGroup;
	}

	public void setEdittingAGroup(boolean edittingAGroup) {
		this.edittingAGroup = edittingAGroup;
	}

	public String getDataTableHeaderString() {
		String text = "";
		if (this.edittingAGroup) {
			if (this.campaigns != null && this.campaigns.size() > 0) {
				text = "You are editting " + this.campaigns.size() + " campaigns in the group named: " + this.campaigns.iterator().next().getCampaignGroup();
			}
			else {
				text = "No Campaigns in the group. Please select at least one campaign.";
			}
		}
		else {
			// not a group edit
			if (this.campaigns != null && this.campaigns.size() > 0) {
				text = "You are editting " + this.campaigns.size() + " campaigns.";
			}
			else {
				text = "No Campaigns selected for edit. Please select at least one campaign.";
			}
		}
		return text;
	}

}
