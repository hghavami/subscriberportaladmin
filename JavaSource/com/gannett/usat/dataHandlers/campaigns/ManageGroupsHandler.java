package com.gannett.usat.dataHandlers.campaigns;

public class ManageGroupsHandler {
	private String groupName = null;
	private boolean changed = false;
	
	
	public boolean isChanged() {
		return changed;
	}
	public void setChanged(boolean changed) {
		this.changed = changed;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}
