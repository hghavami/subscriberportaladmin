package com.gannett.usat.dataHandlers.campaigns;

import org.joda.time.DateTime;

import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;
import com.gannett.usatoday.adminportal.campaigns.promotions.EditablePromotionsSetBO;
import com.gannett.usatoday.adminportal.campaigns.promotions.intf.EditablePromotionSetIntf;

public class CampaignHandlerReadOnly {

	private UsatCampaignIntf campaign = null;
	    
    private CampaignPromotionSetHandler sourcePromosHandler = null;
    
	public String getCampaignGroup() {
		return campaign.getCampaignName();
	}
	public int getCampaignID() {
		return campaign.getPrimaryKey();
	}
	public String getCampaignName() {
		return campaign.getCampaignName();
	}
	public int getCampaignState() {
		return campaign.getCampaignState();
	}
	public String getCampaignStatusString() {
		return campaign.getCampaignStateDescription();
	}
	public int getCampaignType() {
		return campaign.getType();
	}
	public String getCreatedBy() {
		return campaign.getCreatedBy();
	}
	public DateTime getCreatedTime() {
		return campaign.getCreatedTime();
	}
	public boolean isGiftCampaign() {
		// TODO: Future support for gift preference
		return false;
	}
	public String getKeycode() {
		return campaign.getKeyCode();
	}
	public String getPubcode() {
		return campaign.getPubCode();
	}
	public String getRedirectToPage() {
		return campaign.getRedirectToPage();
	}
	public String getRedirectURL() {
		return campaign.getRedirectURL();
	}
	public String getUpdatedBy() {
		return campaign.getUpdatedBy();
	}
	public DateTime getUpdatedTime() {
		return campaign.getUpdatedTime();
	}
	public String getVanityURL() {
		return campaign.getVanityURL();
	}
    
	public void setSourceCampaign(CampaignHandler c) {
		if (c != null) {
			this.campaign = c.getCampaign();
			
			EditablePromotionSetIntf copyOfPromos = new EditablePromotionsSetBO(c.getCampaign().getPromotionSet());
			CampaignPromotionSetHandler psh = new CampaignPromotionSetHandler();
			psh.setSource(copyOfPromos);
			
			this.setSourcePromosHandler(psh);
			
		}
	}
	public CampaignPromotionSetHandler getSourcePromosHandler() {
		return sourcePromosHandler;
	}
	private void setSourcePromosHandler(
			CampaignPromotionSetHandler sourcePromosHandler) {
		this.sourcePromosHandler = sourcePromosHandler;
	}
	public UsatCampaignIntf getCampaign() {
		return campaign;
	}
	
}
