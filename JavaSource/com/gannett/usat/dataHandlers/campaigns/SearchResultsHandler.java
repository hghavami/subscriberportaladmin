/*
 * Created on Aug 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.gannett.usat.dataHandlers.campaigns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import com.gannett.usatoday.adminportal.campaigns.intf.UsatCampaignIntf;

/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SearchResultsHandler {

	public static final int SORTBY_CAMP_NAME = 0;
	public static final int SORTBY_KEYCODE = 1;
	public static final int SORTBY_GROUP_NAME = 2;
	
	private Collection<CampaignHandler> searchResults = null;
	private int sortCol = SearchResultsHandler.SORTBY_CAMP_NAME;
	private String sortOrder = "asc";
	
	private int numRecordsPerPage = 25;
	/**
	 * 
	 */
	public SearchResultsHandler() {
		super();
		
	}

	@SuppressWarnings("unchecked")
	public Collection<CampaignHandler> getLastSearchResults() {
		ArrayList<CampaignHandler> sortedResults = new ArrayList<CampaignHandler>(this.searchResults);
		switch (this.sortCol) {
		case SearchResultsHandler.SORTBY_GROUP_NAME:
			Collections.sort(sortedResults, new CampaignGroupNameComparator());
			if (!this.getSortOrder().equalsIgnoreCase("asc")) {
				Collections.reverse(sortedResults);
			}
			break;
		case SearchResultsHandler.SORTBY_KEYCODE:
			Collections.sort(sortedResults, new CampaignKeycodeComparator());
			if (!this.getSortOrder().equalsIgnoreCase("asc")) {
				Collections.reverse(sortedResults);
			}
			break;
		case SearchResultsHandler.SORTBY_CAMP_NAME:
		default:
			Collections.sort(sortedResults, new CampaignNameComparator());
			if (!this.getSortOrder().equalsIgnoreCase("asc")) {
				Collections.reverse(sortedResults);
			}
			break;
		}
		return sortedResults;
	}
	
	public void setSearchResults(Collection<UsatCampaignIntf> results) {
		if (results != null && results.size() > 0) {
			if (this.searchResults != null) {
				this.searchResults.clear();
			}
			else {
				this.searchResults = new ArrayList<CampaignHandler>();
			}
			// create handlers for each campaign
			for (UsatCampaignIntf c : results) {
				CampaignHandler ch = new CampaignHandler();
				ch.setCampaign(c);
				this.searchResults.add(ch);
			}
		}
		else {
			if (this.searchResults != null) {
				this.searchResults.clear();
			}
		}
	}
	
	public boolean removeCampaignFromCollection(CampaignHandler ch) {
		if (this.searchResults != null && this.searchResults.size() > 0) {
			return this.searchResults.remove(ch);
		}
		return false;
	}

	/**
	 * Following used to set the collection of data for duplicating collection
	 * Specifically for publishing campaigns.
	 * 
	 * @param results
	 */
	public void setCollectionData(Collection<CampaignHandler> results) {
		if (results != null && results.size() > 0) {
			if (this.searchResults != null) {
				this.searchResults.clear();
			}
			this.searchResults = new ArrayList<CampaignHandler>(results);
		}
		else {
			if (this.searchResults != null) {
				this.searchResults.clear();
			}
		}
	}
	
	public boolean getHasSearchResults() {
		if (searchResults != null && searchResults.size() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void setHasSearchResults(boolean hasResults) {
		;
	}

	public int getSortCol() {
		return sortCol;
	}

	public void setSortCol(int sortCol) {
		this.sortCol = sortCol;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public String getLastSeachResultsString() {
		StringBuilder status = new StringBuilder("Search Results: ");
		if (this.searchResults == null || this.searchResults.size() == 0) {
			status.append( "(0)");
		}
		else {
			status.append(this.searchResults.size()).append(" matching results...");
		}
		return status.toString();
	}

	public int getNumRecordsPerPage() {
		return numRecordsPerPage;
	}

	public void setNumRecordsPerPage(int numRecordsPerPage) {
		this.numRecordsPerPage = numRecordsPerPage;
	}
	
	public int getNumberOfCampaignsInCollection() {
		int numRecords = 0;
		if (this.searchResults != null) {
			numRecords = this.searchResults.size();
		}
		return numRecords;
	}
}
