package com.gannett.usat.dataHandlers.campaigns;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.products.USATProductBO;

public class CampaignStatisticsHandler {

	private java.util.HashMap<String, CampaignStat> statsMap = new java.util.HashMap<String, CampaignStat>();
	
	private int totalCampaigns = 0;
	private int totalExpiredCampaigns = 0;
	
	// production counts
	private int totalNewStarts = 0;
	private int totalRenewals = 0;
	
	public CampaignStatisticsHandler() {
		super();
		this.refreshStatistics();		
	}
	public int getTotalCampaigns() {
		return totalCampaigns;
	}
	public int getTotalExpiredCampaigns() {
		return totalExpiredCampaigns;
	}
	public void setTotalCampaigns(int totalCampaigns) {
		this.totalCampaigns = totalCampaigns;
	}
	public void setTotalExpiredCampaigns(int totalExpiredCampaigns) {
		this.totalExpiredCampaigns = totalExpiredCampaigns;
	}

	public void refreshStatistics() {
		try {
			totalCampaigns = 0;
			totalNewStarts = 0;
			totalRenewals = 0;
			totalExpiredCampaigns = 0;
			
			Collection<USATProductBO> products = USATProductBO.fetchProducts();
			
			for (USATProductBO prod : products) {

				CampaignStat stat = new CampaignStat(prod);
				
				// Campaign statistics
				totalCampaigns += stat.getNumCampaigns();
				
				// Product Transaction Counts
				totalNewStarts += stat.getTotalNewStarts();
				
				totalRenewals += stat.getTotalRenewals();
				this.statsMap.put(prod.getProductCode(), stat);
			}
		}
		catch (Exception exp) {
			exp.printStackTrace();
		}
	}
	public int getTotalNewStarts() {
		return totalNewStarts;
	}
	public void setTotalNewStarts(int totalNewStarts) {
		this.totalNewStarts = totalNewStarts;
	}
	public int getTotalRenewals() {
		return totalRenewals;
	}
	public void setTotalRenewals(int totalRenewals) {
		this.totalRenewals = totalRenewals;
	}
	public int getTotalNewStartsandRenewal() {
		return this.getTotalRenewals() + this.getTotalNewStarts();
	}
	
	public Collection<CampaignStat> getCampaignStatsCollection() {
		ArrayList<CampaignStat> statArray = new ArrayList<CampaignStat>(this.statsMap.values());
		return statArray;
	}
}
