/*
 * Created on Aug 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.gannett.usat.dataHandlers.campaigns;

import java.util.Date;

/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AdvancedSearchHandler {

	private String campaignNameLike = null;
	private String campaignGroupLike = null;
	private String campaignKeyCodeLike = null;
	private String campaignVanityLike = null;
	private String publication = "ALL";
	private String publishingStatus = "ALL";
	private Date   insertedAfter = null;
	private Date   insertedBefore = null;
	
	
	/**
	 * 
	 */
	public AdvancedSearchHandler() {
		super();
		
	}


	public String getCampaignGroupLike() {
		return campaignGroupLike;
	}


	public String getCampaignKeyCodeLike() {
		return campaignKeyCodeLike;
	}


	public String getCampaignNameLike() {
		return campaignNameLike;
	}


	public String getCampaignVanityLike() {
		return campaignVanityLike;
	}


	public Date getInsertedAfter() {
		return insertedAfter;
	}


	public Date getInsertedBefore() {
		return insertedBefore;
	}


	public String getPublication() {
		return publication;
	}


	public String getPublishingStatus() {
		return publishingStatus;
	}


	public void setCampaignGroupLike(String campaignGroupLike) {
		this.campaignGroupLike = campaignGroupLike;
	}


	public void setCampaignKeyCodeLike(String campaignKeyCodeLike) {
		this.campaignKeyCodeLike = campaignKeyCodeLike;
	}


	public void setCampaignNameLike(String campaignNameLike) {
		this.campaignNameLike = campaignNameLike;
	}


	public void setCampaignVanityLike(String campaignVanityLike) {
		this.campaignVanityLike = campaignVanityLike;
	}


	public void setInsertedAfter(Date insertedAfter) {
		this.insertedAfter = insertedAfter;
	}


	public void setInsertedBefore(Date insertedBefore) {
		this.insertedBefore = insertedBefore;
	}


	public void setPublication(String publication) {
		this.publication = publication;
	}


	public void setPublishingStatus(String publishingStatus) {
		this.publishingStatus = publishingStatus;
	}

}
