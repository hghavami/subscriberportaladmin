/*
 * Created on Aug 8, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.gannett.usat.dataHandlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.gannett.usatoday.adminportal.users.PortalUserBO;
import com.gannett.usatoday.adminportal.users.PortalUserRoleEnum;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;
import com.usatoday.UsatException;

/**
 * @author aeast
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UserHandler {

	private PortalUserIntf user = null;
	
	private String userEnteredUserID = null;
	private String userEnteredpassword = null;
	
	// following is used for when user appears in a collection and can be selected from a 
	// row selection
	private boolean userSelected = false; 
	
	// following flag set when user logs in
	private boolean authenticated = false;
	
	/**
	 * 
	 */
	public UserHandler() {
		super();
		this.user = new PortalUserBO();
	}

	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		String pwd = "";
		if (this.user != null) {
			pwd = new String(user.getPassword());
		}
		return pwd;
	}
	/**
	 * @param password The password to set.
	 */
	public void setPassword(String password) {
		try {
			if (this.user != null && password != null && password.trim().length() > 0) {
				this.user.setNewPassword(password);
			}
		}
		catch (UsatException ue) {
			System.out.println("Failed to set password on business object. " + ue.getMessage());
		}
	}
	/**
	 * @return Returns the userID.
	 */
	public String getUserID() {
		if (this.user != null) {
			return this.user.getUserID();
		}
		return "";
	}
	/**
	 * @param userID The userID to set.
	 */
	public void setUserID(String userID) {
			// only set user id if it is a new user.
			if (this.user != null && (this.user.getUserID() == null || this.user.getUserID().length() == 0)) {
				try {
					this.user.setUserID(userID);
				}
				catch(Exception e) {
					System.out.println("Failed to set the user id: " + userID);
				}
			}
	}
	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		if (this.user != null) {
			return this.user.getEmailAddress();
		}
		return "";
	}
	/**
	 * @param emailAddress The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		try {
			if (this.user != null) {
				this.user.setEmailAddress(emailAddress);
			}
		}
		catch (UsatException ue) {
			System.out.println("Failed to set email address on business object. " + ue.getMessage());
		}
	}

	public String getFirstName() {
		if (this.user != null) {
			return this.user.getFirstName();
		}
		return "";
	}

	public void setFirstName(String firstName) {
		try {
			if (this.user != null) {
				this.user.setFirstName(firstName);
			}
		}
		catch (Exception ue) {
			System.out.println("Failed to set first name on business object. " + ue.getMessage());
		}
	}

	public String getLastName() {
		if (this.user != null) {
			return this.user.getLastName();
		}
		return "";
	}

	public void setLastName(String lastName) {
		try {
			if (this.user != null) {
				this.user.setLastName(lastName);
			}
		}
		catch (Exception ue) {
			System.out.println("Failed to set last name on business object. " + ue.getMessage());
		}
	}

	public String getPhone() {
		if (this.user != null) {
			return this.user.getPhone();
		}
		return "";
	}

	public void setPhone(String phone) {
		try {
			if (this.user != null) {
				this.user.setPhone(phone);
			}
		}
		catch (Exception ue) {
			System.out.println("Failed to set phone on business object. " + ue.getMessage());
		}
	}

	public Collection<PortalUserRoleEnum> getAssignedRoles() {
		if (this.user != null) {
			return this.user.getAssignedRoles();
		}
		return null;
	}

	public String[] getAssignedRolesAsArray() {
		String roles[] = null;
		if (this.user != null) {
			roles = new String[this.user.getAssignedRoles().size()];
			Iterator<PortalUserRoleEnum> roleItr = this.user.getAssignedRoles().iterator();
			for (int i = 0; i < this.user.getAssignedRoles().size(); i++) {
				PortalUserRoleEnum role = roleItr.next();
				roles[i] = String.valueOf(role.getDbRepresentation());
			}
		}
		return roles;
	}

	public void setAssignedRolesAsArray(String[] newRoles) {
		if (newRoles != null && this.user != null) {
			ArrayList<PortalUserRoleEnum> roleCol = new ArrayList<PortalUserRoleEnum>();
			for (int i = 0; i < newRoles.length; i++) {
				if (newRoles[i].equalsIgnoreCase("2")) {
					roleCol.add(PortalUserRoleEnum.NORMAL);
				}
				else if(newRoles[i].equalsIgnoreCase("3")) {
					roleCol.add(PortalUserRoleEnum.POWER);
				}
				else if (newRoles[i].equalsIgnoreCase("4")) {
					roleCol.add(PortalUserRoleEnum.LIMITED);
				}
			}
			this.user.setAssignedRoles(roleCol);
		}
	}
	
	
	public String getAssignedRolesAsString() {
		String rolesStr = "";
		if (this.user != null) {
			StringBuffer rolesStrBuf = new StringBuffer();
			for(PortalUserRoleEnum role : this.user.getAssignedRoles()) {
				rolesStrBuf.append(role.toString()).append(", ");
			}
			rolesStr = rolesStrBuf.toString().trim();
			rolesStr = rolesStr.substring(0, (rolesStr.length()-1));
		}
		return rolesStr;
	}

	public void setAssignedRolesAsString(String roles) {
		; // no op helper method only
	}
	
	public void setAssignedRoles(Collection<PortalUserRoleEnum> newRoles) {
		if (this.user != null) {
			this.user.setAssignedRoles(newRoles);
		}
	}
	
	public PortalUserIntf getUser() {
		return user;
	}

	public void setUser(PortalUserIntf user) {
		this.user = user;
	}

	public String getUserEnteredpassword() {
		return userEnteredpassword; 
	}

	public void setUserEnteredpassword(String userEnteredpassword) {
		this.userEnteredpassword = userEnteredpassword;
	}

	public String getUserEnteredUserID() {
		return userEnteredUserID;
	}

	public void setUserEnteredUserID(String userEnteredUserID) {
		this.userEnteredUserID = userEnteredUserID;
	}
	
	public boolean getEnabled() {
		boolean userEnabled = false;
		if (this.user != null) {
			userEnabled = this.user.getEnabled();
		}
		return userEnabled;
	}
	
	public void setEnabled(boolean isEnabled) {
		if (this.user != null) {
			this.user.setEnabled(isEnabled);
		}
	}

	public boolean getUserSelected() {
		return userSelected;
	}

	public void setUserSelected(boolean userSelected) {
		this.userSelected = userSelected;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}
		
	public boolean getIslimitedAccessOnly() {
		if (this.user.hasAccess(PortalUserRoleEnum.NORMAL) ||
				this.user.hasAccess(PortalUserRoleEnum.POWER) ||
				this.user.hasAccess(PortalUserRoleEnum.ADMINISTRATOR)) {
				return false;
			}
			return true;
	}
	
	public String getDisabledButtonStyle() {
		if (getIslimitedAccessOnly()) {
			return "commandExButton_Disabled";
		}
		return "";
	}
}
