package com.gannett.usat.dataHandlers.subscriptionOffers;

import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;

public class SubscriptionTermViewHandler {

	private SubscriptionTermsIntf term = null;


	public SubscriptionTermsIntf getTerm() {
		return term;
		
	}

	public void setTerm(SubscriptionTermsIntf term) {
		this.term = term;
	}
	
	public String getRequiresEzpay() {
		String returnVal = "false";
		try {
			if (this.term != null && this.term.getPubCode().length() > 0) {
				returnVal = this.term.requiresEZPAY() ? "true" : "false";
			}
			else {
				returnVal = "";
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return returnVal;
	}
	
	public String getTermAsString() {
		String des = "";
		if (this.term != null) {
			des = this.term.getTermAsString();
		}
		return des;
	}
	public String getDescription() {
		String des = "";
		if (this.term != null) {
			des = this.term.getDescription();
		}
		return des;
	}
	public String getRateCode() {
		String des = "";
		if (this.term != null) {
			des = this.term.getPiaRateCode();
		}
		return des;
	}
	public String getDurationInWeeks() {
		String dur = "";
		if (this.term != null) {
			dur = this.term.getDurationInWeeks();
		}
		return dur;
	}
	public String getParentOfferKeycode() {
		String kCode = "";
		if (this.term !=null && this.term.getParentOffer() != null) {
			kCode = this.term.getParentOffer().getKeyCode();
		}
		return kCode;
	}
}
