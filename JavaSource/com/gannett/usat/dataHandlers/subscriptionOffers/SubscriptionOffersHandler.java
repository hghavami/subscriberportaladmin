package com.gannett.usat.dataHandlers.subscriptionOffers;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.model.SelectItem;

import com.gannett.usatoday.adminportal.campaigns.KeyCodeBO;
import com.gannett.usatoday.adminportal.campaigns.intf.KeyCodeIntf;
import com.gannett.usatoday.adminportal.products.USATProductBO;

public class SubscriptionOffersHandler {

	private String currentPub = null;
	
	private java.util.HashMap<String, Collection<KeyCodeIntf>> keycodeMap = null;
	
	public SubscriptionOffersHandler() {
		super();
		this.keycodeMap = new java.util.HashMap<String, Collection<KeyCodeIntf>>();
		
		try {
			// query product table for list of products, then query for keycodes.

			Collection<USATProductBO> products = USATProductBO.fetchProducts();
			
			for (USATProductBO prod : products) {
				Collection<KeyCodeIntf> codes = KeyCodeBO.fetchKeycodesForPub(prod.getProductCode());
				this.keycodeMap.put(prod.getProductCode(), codes);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	// generic
	public Collection<KeyCodeIntf> getKeyCodesForPub(String pub) {
		Collection<KeyCodeIntf> codes = null;
		
		codes = this.keycodeMap.get(pub);
		
		return codes;
	}

	public Collection<SelectItem> getAvailableKeyCodeSelectItemsForPub(String pub) {
		Collection<SelectItem> items = new ArrayList<SelectItem>();
		
		Collection<KeyCodeIntf> codes = this.getKeyCodesForPub(pub);
		
		if (codes != null){
			for(KeyCodeIntf keyCode : codes) {
		        SelectItem si = new javax.faces.model.SelectItem();
		        si.setDescription(keyCode.getKeyCode());
		        si.setLabel(keyCode.getKeyCode() + "-" + keyCode.getOfferDescription());
		        si.setValue(keyCode.getKeyCode());
		        items.add(si);
			}
		}
		return items;
	}

	public Collection<SelectItem> getAvailableKeyCodeSelectItemsForCurrentPub() {
		Collection<SelectItem> items = new ArrayList<SelectItem>();
		
		Collection<KeyCodeIntf> codes = this.getKeyCodesForPub(this.currentPub);
		
		if (codes != null){
			for(KeyCodeIntf keyCode : codes) {
		        SelectItem si = new javax.faces.model.SelectItem();
		        si.setDescription(keyCode.getKeyCode());
		        si.setLabel(keyCode.getKeyCode() + "-" + keyCode.getOfferDescription());
		        si.setValue(keyCode.getKeyCode());
		        items.add(si);
			}
		}
		return items;
	}
	
	
	public String getCurrentPub() {
		return currentPub;
	}

	public void setCurrentPub(String currentPub) {
		this.currentPub = currentPub;
	}

}
