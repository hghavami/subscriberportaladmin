package com.gannett.usat.dataHandlers;

import java.util.ArrayList;
import java.util.Collection;

import com.gannett.usatoday.adminportal.users.PortalUserBO;
import com.gannett.usatoday.adminportal.users.intf.PortalUserIntf;

public class AdminUsersHandler {

	private UserHandler requestingUser = null;
	private Collection<UserHandler> users = null;

	public AdminUsersHandler() {
		super();
	}

	public Collection<UserHandler> getUsers() {
		if (this.users == null) {
			try {
				// load the users
				if (this.requestingUser != null) {
					Collection<PortalUserIntf> userBos =PortalUserBO.retrieveAllUsers(this.requestingUser.getUser()); 
					this.users = new ArrayList<UserHandler>();
					for (PortalUserIntf pu : userBos) {
						UserHandler aUser = new UserHandler();
						aUser.setUser(pu);
						this.users.add(aUser);
					}
					
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return users;
	}

	public void setUsers(Collection<UserHandler> users) {
		this.users = users;
	}

	public UserHandler getRequestingUser() {
		return requestingUser;
	}

	public void setRequestingUser(UserHandler requestingUser) {
		this.requestingUser = requestingUser;
	}
	
}
